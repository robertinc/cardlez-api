# Build Go
FROM golang:alpine AS builder
# Install Git for go get
RUN set -eux; \
    apk add --no-cache --virtual git
ENV GO_WORKDIR $GOPATH/src/gitlab.com/robertinc/cardlez-api/
RUN go version
RUN mkdir -p src/gitlab.com/robertinc
RUN git config --global url."https://gitlab+deploy-token-13521:sqsdhs-xekNqUtJkz7DE@gitlab.com/".insteadOf "https://gitlab.com/"
RUN mkdir /root/.ssh && echo "StrictHostKeyChecking no " > /root/.ssh/config
ADD . ${GO_WORKDIR}
WORKDIR ${GO_WORKDIR}
RUN go get
RUN go install

# Minimize Docker Size
FROM alpine:latest
RUN set -eux; \
    apk add --no-cache --virtual ca-certificates
COPY --from=builder /go/bin/cardlez-api .
COPY --from=builder /go/src/gitlab.com/robertinc/cardlez-api/config.json .
CMD ["./cardlez-api"]
EXPOSE 8080