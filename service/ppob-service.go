package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"time"

	"gitlab.com/robertinc/cardlez-api/models"
	"go.uber.org/zap"
)

type PPOBService struct {
	Auth  string
	URL   string
	Route string
	Body  map[string]interface{}
}
type PulsaResponse struct {
	Data             models.PPOB `json:"Data"`
	IsSuccess        bool        `json:"IsSuccess"`
	ErrorCode        int         `json:"ErrorCode"`
	ErrorDescription string      `json:"ErrorDescription"`
}
type PLNPrepaidResponse struct {
	Data             models.PLNPrepaid `json:"Data"`
	IsSuccess        bool              `json:"IsSuccess"`
	ErrorCode        int               `json:"ErrorCode"`
	ErrorDescription string            `json:"ErrorDescription"`
}
type CheckPPOBResponse struct {
	Data             models.CheckPPOB `json:"Data"`
	IsSuccess        bool             `json:"IsSuccess"`
	ErrorCode        int              `json:"ErrorCode"`
	ErrorDescription string           `json:"ErrorDescription"`
}

func NewPPOBService() *PPOBService {
	configExt := models.LoadConfiguration()
	return &PPOBService{
		Auth: configExt.PPOBAuth,
		URL:  configExt.PPOBURL,
	}
}

func (s *PPOBService) SendPulsa(route string, body map[string]interface{}) (*models.PPOB, error) {
	b, err := json.Marshal(body)
	if err != nil {
		log.Fatal(err)
	}
	u, _ := url.ParseRequestURI(s.URL)
	u.Path = route
	urlStr := s.URL + u.Path
	client := &http.Client{}
	r, _ := http.NewRequest("POST", urlStr, bytes.NewBuffer(b)) // URL-encoded payload
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", "Bearer "+s.Auth)

	logger := Logger(context.TODO())
	logger.Info("Request",
		zap.String("request", fmt.Sprintf("%v", r)),
	)
	resp, err := client.Do(r)
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	logger.Info("Response Error",
		zap.String("response", fmt.Sprintf("%v", string(bodyBytes))),
	)
	var pulsaResponse PulsaResponse
	err = json.Unmarshal(bodyBytes, &pulsaResponse)
	if pulsaResponse.ErrorCode == 400 {
		return &pulsaResponse.Data, fmt.Errorf(pulsaResponse.ErrorDescription)
	}
	return &pulsaResponse.Data, nil
}
func (s *PPOBService) SendPLN(route string, body map[string]interface{}) (*models.PLNPrepaid, error) {
	b, err := json.Marshal(body)
	if err != nil {
		log.Fatal(err)
	}
	u, _ := url.ParseRequestURI(s.URL)
	u.Path = route
	urlStr := s.URL + u.Path
	client := &http.Client{}
	r, _ := http.NewRequest("POST", urlStr, bytes.NewBuffer(b)) // URL-encoded payload
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", "Bearer "+s.Auth)

	logger := Logger(context.TODO())
	logger.Info("Request",
		zap.String("request", fmt.Sprintf("%v", r)),
	)
	resp, err := client.Do(r)
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	logger.Info("Response",
		zap.String("response", fmt.Sprintf("%v", string(bodyBytes))),
	)
	var plnPrepaidResponse PLNPrepaidResponse
	err = json.Unmarshal(bodyBytes, &plnPrepaidResponse)
	if plnPrepaidResponse.ErrorCode == 400 {
		return &plnPrepaidResponse.Data, fmt.Errorf(plnPrepaidResponse.ErrorDescription)
	}
	return &plnPrepaidResponse.Data, nil
}

func (s *PPOBService) CheckPPOB(route string, params map[string]string) (*models.PPOB, error) {
	u, _ := url.ParseRequestURI(s.URL)
	u.Path = route
	urlStr := s.URL + u.Path
	client := &http.Client{}

	r, _ := http.NewRequest("GET", urlStr, nil) // URL-encoded payload
	if len(params) > 0 {
		qp := r.URL.Query()
		keys := reflect.ValueOf(params).MapKeys()
		for _, key := range keys {
			qp.Add(key.String(), params[key.String()])
		}
		r.URL.RawQuery = qp.Encode()
	}
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", "Bearer "+s.Auth)

	logger := Logger(context.TODO())
	logger.Info("Request",
		zap.String("request", fmt.Sprintf("%v", r)),
	)
	resp, err := client.Do(r)
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	logger.Info("Response",
		zap.String("response", fmt.Sprintf("%v", string(bodyBytes))),
	)
	var checkPPOB CheckPPOBResponse
	var pulsaResponse models.PPOB
	err = json.Unmarshal(bodyBytes, &checkPPOB)
	if checkPPOB.ErrorCode == 400 {
		return nil, fmt.Errorf(checkPPOB.ErrorDescription)
	}
	pulsaResponse.StatusCode = checkPPOB.Data.Status
	pulsaResponse.ResponseReference = checkPPOB.Data.ReffNo
	pulsaResponse.TransactionDate, _ = time.Parse("02-01-2006", checkPPOB.Data.TransactionDate)
	return &pulsaResponse, nil
}
