package service

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/nexmo-community/nexmo-go"
	"gitlab.com/robertinc/cardlez-api/models"
)

type SMSService struct {
	UserKey string
	PassKey string
	NoHP    string
	Pesan   string
}
type SMSResponse struct {
	Response xml.Name `xml:"response"`
	Message  struct {
		To      string  `xml:"to"`
		Status  int     `xml:"status"`
		Text    string  `xml:"text"`
		Balance float64 `xml:"balance"`
	} `xml:"message"`
}

func NewSMSService() *SMSService {
	user, pass := models.SMSGatewayCredential()
	return &SMSService{UserKey: user, PassKey: pass}
}

//for cardlez - ZENZIVA
func (s *SMSService) RegularSendVerificationCode(message string, handphone string) (bool, error) {
	s.Pesan = message
	s.NoHP = handphone
	//url := fmt.Sprintf("https://alpha.zenziva.net/apps/smsapi.php?userkey=%v&passkey=%v&nohp=%v&pesan=%v",
	url := fmt.Sprintf("https://reguler.zenziva.net/apps/smsapi.php?userkey=%v&passkey=%v&nohp=%v&pesan=%v",
		s.UserKey,
		s.PassKey,
		s.NoHP,
		s.Pesan,
	)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Cache-Control", "no-cache")
	response, err := http.DefaultClient.Do(req)
	fmt.Println(url)
	if err != nil {
		return false, fmt.Errorf("GET error: %v", err)
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Status error: %v", response.StatusCode)
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return false, fmt.Errorf("Read body: %v", err)
	}
	var smsResponse SMSResponse
	fmt.Println(string(data))
	err = xml.Unmarshal(data, &smsResponse)
	if err != nil {
		return false, err
	}
	if smsResponse.Message.Status != 0 {
		return false, fmt.Errorf(smsResponse.Message.Text)
	}
	return true, nil
}
func (s *SMSService) RegularSendOTP(otp string, handphone string) (bool, error) {
	s.Pesan = otp
	s.NoHP = handphone
	url := fmt.Sprintf("https://reguler.zenziva.net/apps/smsotp.php?userkey=%v&passkey=%v&nohp=%v&kode_otp=%v",
		s.UserKey,
		s.PassKey,
		s.NoHP,
		s.Pesan,
	)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Cache-Control", "no-cache")
	response, err := http.DefaultClient.Do(req)
	fmt.Println(url)
	if err != nil {
		return false, fmt.Errorf("GET error: %v", err)
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Status error: %v", response.StatusCode)
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return false, fmt.Errorf("Read body: %v", err)
	}
	var smsResponse SMSResponse
	fmt.Println(string(data))
	err = xml.Unmarshal(data, &smsResponse)
	if err != nil {
		return false, err
	}
	if smsResponse.Message.Status != 0 {
		return false, fmt.Errorf(smsResponse.Message.Text)
	}
	return true, nil
}

//for client - ZENZIVA
func (s *SMSService) SendVerificationCode(message string, handphone string) (bool, error) {
	s.Pesan = message
	s.NoHP = handphone
	url := fmt.Sprintf("https://alpha.zenziva.net/apps/smsapi.php?userkey=%v&passkey=%v&nohp=%v&pesan=%v",
		s.UserKey,
		s.PassKey,
		s.NoHP,
		s.Pesan,
	)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Cache-Control", "no-cache")
	response, err := http.DefaultClient.Do(req)
	fmt.Println(url)
	if err != nil {
		return false, fmt.Errorf("GET error: %v", err)
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Status error: %v", response.StatusCode)
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return false, fmt.Errorf("Read body: %v", err)
	}
	var smsResponse SMSResponse
	fmt.Println(string(data))
	err = xml.Unmarshal(data, &smsResponse)
	if err != nil {
		return false, err
	}
	if smsResponse.Message.Status != 1 {
		return false, fmt.Errorf(smsResponse.Message.Text)
	}
	return true, nil
}
func (s *SMSService) SendOTP(otp string, handphone string) (bool, error) {
	s.Pesan = otp
	s.NoHP = handphone
	//url := fmt.Sprintf("https://alpha.zenziva.net/apps/smsotp.php?userkey=%v&passkey=%v&nohp=%v&kode_otp=%v",
	url := fmt.Sprintf("https://alpha.zenziva.net/apps/smsapi.php?userkey=%v&passkey=%v&nohp=%v&pesan=%v&type=otp",
		s.UserKey,
		s.PassKey,
		s.NoHP,
		s.Pesan,
	)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Cache-Control", "no-cache")
	response, err := http.DefaultClient.Do(req)
	fmt.Println(url)
	if err != nil {
		return false, fmt.Errorf("GET error: %v", err)
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Status error: %v", response.StatusCode)
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return false, fmt.Errorf("Read body: %v", err)
	}
	var smsResponse SMSResponse
	fmt.Println(string(data))
	err = xml.Unmarshal(data, &smsResponse)
	if err != nil {
		return false, err
	}
	if smsResponse.Message.Status != 1 {
		return false, fmt.Errorf(smsResponse.Message.Text)
	}
	return true, nil
}

//Nexmo
func (s *SMSService) SendVerificationCodeNexmo(message string, handphone string, apikey string, apisecret string) (bool, string, error) {
	s.UserKey = apikey
	s.PassKey = apisecret
	s.Pesan = message
	s.NoHP = handphone
	//must 62
	if string(handphone[0:1]) == "0" {
		stringCutFirst := string(handphone[1:len(handphone)])
		s.NoHP = "62" + stringCutFirst
	}

	// Auth
	auth := nexmo.NewAuthSet()
	auth.SetAPISecret(s.UserKey, s.PassKey)

	// Init Nexmo
	client := nexmo.NewClient(http.DefaultClient, auth)

	smsContent := nexmo.SendSMSRequest{
		From: "nxsms",
		To:   s.NoHP,
		Text: s.Pesan,
	}

	smsResponse, _, err := client.SMS.SendSMS(smsContent)
	if err != nil {
		return false, smsResponse.Messages[0].Status, err
	}
	fmt.Println("Status:", smsResponse.Messages[0].Status)

	if smsResponse.Messages[0].Status != "0" {
		return false, smsResponse.Messages[0].Status, fmt.Errorf("Error message from nexmo: " + smsResponse.Messages[0].ErrorText)
	}

	return true, smsResponse.Messages[0].Status, nil
}

//for cardlez Console - ZENZIVA
func (s *SMSService) RegularSendVerificationCodeConsole(message string, handphone string) (bool, error) {
	s.Pesan = message
	s.NoHP = handphone
	url := "https://console.zenziva.net/reguler/api/sendsms/"

	r, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte(`
		{
			"userkey": "`+s.UserKey+`",
			"passkey": "`+s.PassKey+`",
			"to": "`+s.NoHP+`",
			"message": "`+s.Pesan+`"
		}		
		`)),
	)
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	r.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(r)
	if err != nil {
		return false, fmt.Errorf("Error: %v", err)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodybytesStr := fmt.Sprintf("%v", string(bodyBytes))
	fmt.Println(bodybytesStr)
	if resp.StatusCode != 201 {
		smsResponseConsole := &models.SMSZenzivaResponseFailed{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	} else {
		smsResponseConsole := &models.SMSZenzivaResponseSuccess{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	}
}
func (s *SMSService) RegularSendOTPConsole(otp string, handphone string) (bool, error) {
	s.Pesan = otp
	s.NoHP = handphone
	url := "https://console.zenziva.net/reguler/api/sendOTP/"

	r, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte(`
		{
			"userkey": "`+s.UserKey+`",
			"passkey": "`+s.PassKey+`",
			"to": "`+s.NoHP+`",
			"kode_otp": "`+s.Pesan+`"
		}		
		`)),
	)
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	r.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(r)
	if err != nil {
		return false, fmt.Errorf("Error: %v", err)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodybytesStr := fmt.Sprintf("%v", string(bodyBytes))
	fmt.Println(bodybytesStr)
	if resp.StatusCode != 201 {
		smsResponseConsole := &models.SMSZenzivaResponseFailed{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	} else {
		smsResponseConsole := &models.SMSZenzivaResponseSuccess{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	}
}

//for client Console - ZENZIVA
func (s *SMSService) SendVerificationCodeConsole(message string, handphone string) (bool, error) {
	s.Pesan = message
	s.NoHP = handphone
	url := "https://console.zenziva.net/masking/api/sendsms/"

	r, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte(`
		{
			"userkey": "`+s.UserKey+`",
			"passkey": "`+s.PassKey+`",
			"to": "`+s.NoHP+`",
			"message": "`+s.Pesan+`"
		}		
		`)),
	)
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	r.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(r)
	if err != nil {
		return false, fmt.Errorf("Error: %v", err)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodybytesStr := fmt.Sprintf("%v", string(bodyBytes))
	fmt.Println(bodybytesStr)
	if resp.StatusCode != 201 {
		smsResponseConsole := &models.SMSZenzivaResponseFailed{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	} else {
		smsResponseConsole := &models.SMSZenzivaResponseSuccess{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	}
}
func (s *SMSService) SendOTPConsole(otp string, handphone string) (bool, error) {
	s.Pesan = otp
	s.NoHP = handphone
	url := "https://console.zenziva.net/masking/api/sendOTP/"

	r, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte(`
		{
			"userkey": "`+s.UserKey+`",
			"passkey": "`+s.PassKey+`",
			"to": "`+s.NoHP+`",
			"message": "`+s.Pesan+`"
		}		
		`)),
	)
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	r.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(r)
	if err != nil {
		return false, fmt.Errorf("Error: %v", err)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodybytesStr := fmt.Sprintf("%v", string(bodyBytes))
	fmt.Println(bodybytesStr)
	if resp.StatusCode != 201 {
		smsResponseConsole := &models.SMSZenzivaResponseFailed{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	} else {
		smsResponseConsole := &models.SMSZenzivaResponseSuccess{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	}
}

//for cardlez Console - ZENZIVA WHATSAPP
func (s *SMSService) RegularSendWhatsAppConsole(message string, handphone string) (bool, error) {
	s.Pesan = message
	s.NoHP = handphone
	url := "https://console.zenziva.net/wareguler/api/sendWA/"

	r, _ := http.NewRequest("POST",
		url,
		bytes.NewBuffer([]byte(`
		{
			"userkey": "`+s.UserKey+`",
			"passkey": "`+s.PassKey+`",
			"to": "`+s.NoHP+`",
			"message": "`+s.Pesan+`"
		}		
		`)),
	)
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	r.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(r)
	if err != nil {
		return false, fmt.Errorf("Error: %v", err)
	}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodybytesStr := fmt.Sprintf("%v", string(bodyBytes))
	fmt.Println(bodybytesStr)
	if resp.StatusCode != 201 {
		smsResponseConsole := &models.SMSZenzivaResponseFailed{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	} else {
		smsResponseConsole := &models.SMSZenzivaResponseSuccess{}
		err = json.Unmarshal(bodyBytes, smsResponseConsole)
		if err != nil {
			return false, errors.New(err.Error())
		}
		if smsResponseConsole.Status != "1" {
			return false, fmt.Errorf(smsResponseConsole.Text)
		}
		return true, nil
	}
}
