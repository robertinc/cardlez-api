package service

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

var logger zap.Logger

func InitLogger() {
	w := zapcore.AddSync(&lumberjack.Logger{
		Filename:   models.LoadConfiguration().LogFile,
		LocalTime:  true,
		MaxSize:    100, // megabytes
		MaxBackups: 500,
		MaxAge:     30,   //days
		Compress:   true, // disabled by default
	})

	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.TimeKey = "timestamp"
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderCfg.CallerKey = "caller"
	encoderCfg.EncodeCaller = zapcore.FullCallerEncoder
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		w,
		zap.InfoLevel,
	)
	logger = *zap.New(core, zap.AddCaller())
}

type correlationIdType int

const (
	requestIdKey correlationIdType = iota
	sessionIdKey
)

// WithRqId returns a context which knows its request ID
func WithRqId(ctx context.Context, rqId string) context.Context {
	return context.WithValue(ctx, requestIdKey, rqId)
}

// WithSessionId returns a context which knows its session ID
func WithSessionId(ctx context.Context, sessionId string) context.Context {
	return context.WithValue(ctx, sessionIdKey, sessionId)
}

// Logger returns a zap logger with as much context as possible
func Logger(ctx context.Context) zap.Logger {
	newLogger := logger
	if ctx != nil {
		if ctxRqId, ok := ctx.Value(requestIdKey).(string); ok {
			newLogger = *newLogger.With(zap.String("rqId", ctxRqId))
		}
		if ctxSessionId, ok := ctx.Value(sessionIdKey).(string); ok {
			newLogger = *newLogger.With(zap.String("sessionId", ctxSessionId))
		}
	}
	return newLogger
}
