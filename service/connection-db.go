package service

import (
	"database/sql"
	"log"

	"gitlab.com/robertinc/cardlez-api/models"
)

var dbConn *sql.DB

func OpenDB() (*sql.DB, error) {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}

	dbConn = conn

	dbConn.SetMaxOpenConns(5)
	dbConn.SetMaxIdleConns(1)
	dbConn.SetConnMaxLifetime(0)
	return dbConn, err
}

func OpenDBPermata() (*sql.DB, error) {
	conn, err := sql.Open("sqlserver", models.BuildConnStringV2(2))
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}

	dbConn = conn

	dbConn.SetMaxOpenConns(5)
	dbConn.SetMaxIdleConns(1)
	dbConn.SetConnMaxLifetime(0)
	return dbConn, err
}
