package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	redis "github.com/go-redis/redis/v8"
	"gitlab.com/robertinc/cardlez-api/models"
)

type AuthService struct {
	appName             *string
	signedSecret        *string
	expiredTimeInSecond *time.Duration
	//log                 *logging.Logger
}

func NewAuthService() *AuthService {
	appname, secret, expire := models.GetJWTCredential()
	return &AuthService{&appname, &secret, &expire}
}

func (a *AuthService) SignJWT(ctx context.Context, user *models.CompanyUser) (*string, error) {
	expired := time.Now().Add(*a.expiredTimeInSecond).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id": base64.StdEncoding.EncodeToString([]byte(user.ID)),
		//"created_at": user.CreatedAt,
		"exp":       expired,
		"iss":       *a.appName,
		"isAdmin":   user.IsAdmin,
		"userID":    user.ID,
		"userEmail": user.UserEmail,
		"claimDate": user.ClaimDate,
	})

	client := BuildConnRedis(0)
	//Change Redis Here
	//client.Set(user.ID, expired, 0)
	client.Set(ctx, user.ID, expired, 0)
	tokenString, err := token.SignedString([]byte(*a.signedSecret))
	return &tokenString, err
}

func (a *AuthService) ValidateJWT(tokenString *string) (*jwt.Token, error) {
	token, err := jwt.Parse(*tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("	unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(*a.signedSecret), nil
	})

	return token, err
}
func BuildConnRedis(db int) *redis.Client {
	addr, pass := models.BuildConnStringRedis()
	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass, // no password set
		DB:       db,   // use default DB
	})
}

// CONTEXT
func AddContext(ctx context.Context, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

type ContextKey string

func (c ContextKey) String() string {
	return string(c)
}

var (
	CtxUserID  = ContextKey("user_id")
	CtxIsAuth  = ContextKey("is_authorized")
	CtxIsAdmin = ContextKey("is_admin")
)
