package service

import (
	"crypto/tls"
	"fmt"

	"gitlab.com/robertinc/cardlez-api/models"
	gomail "gopkg.in/gomail.v2"
)

type EmailService struct {
	EmailSMTP 		string
	Port      		int
	Username  		string
	Password  		string
	Email     		string
	Pesan     		string
	MailSender     	string
}

func NewEmailService() *EmailService {
	emailSMTP, username, password, port, mailSender := models.GetEmailSMTP()
	return &EmailService{EmailSMTP: emailSMTP, Username: username, Password: password, Port: port, MailSender: mailSender}
}

func (s *EmailService) SendVerificationCode(message string, email string, subject string) (bool, error) {
	m := gomail.NewMessage()
	s.Email = email
	s.Pesan = message
	m.SetHeader("From", s.MailSender)
	m.SetHeader("To", s.Email)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", s.Pesan)

	d := gomail.NewDialer(s.EmailSMTP, s.Port, s.Username, s.Password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	err := d.DialAndSend(m)
	if err != nil {
		return false, fmt.Errorf("GET error: ", err)
	}

	return true, nil
}

func (s *EmailService) SendVerificationCodeWithCC(message string, email string, subject string, cc string, bcc string) (bool, error) {
	m := gomail.NewMessage()
	s.Email = email
	s.Pesan = message
	m.SetHeader("From", s.MailSender)
	m.SetHeader("To", s.Email)
	m.SetHeader("Subject", subject)
	if cc != "" {
		m.SetHeader("Cc", cc)
	}
	if bcc != "" {
		m.SetHeader("Bcc", bcc)
	}
	m.SetBody("text/html", s.Pesan)

	d := gomail.NewDialer(s.EmailSMTP, s.Port, s.Username, s.Password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	err := d.DialAndSend(m)
	if err != nil {
		return false, fmt.Errorf("GET error: ", err)
	}

	return true, nil
}
