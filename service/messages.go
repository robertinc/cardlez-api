package service

const (
	PostMethodSupported = "only post method is allowed"
	CredentialsError    = "credentials error"
	TokenError          = "token error"
	UnauthorizedAccess  = "unauthorized access"
)
