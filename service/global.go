package service

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/beevik/guid"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/robertinc/cardlez-api/enum"
	"gitlab.com/robertinc/cardlez-api/models"
)

func IsValidGuid(input string) bool {
	if len(input) == 0 {
		return false
	}
	if input == "00000000-0000-0000-0000-000000000000" {
		return false
	}
	return guid.IsGuid(input)
}
func NewEmptyGuid() string {
	return "00000000-0000-0000-0000-000000000000"
}

func UploadFile() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}

		file, handle, err := r.FormFile("file")
		path := r.PostFormValue("path")
		if path == "" {
			jsonResponse(w, http.StatusMultiStatus, fmt.Sprintf(`{"message": "The path is not valid."}`))
		}
		filename := r.PostFormValue("filename")
		if filename == "" {
			filename = handle.Filename
		}
		if err != nil {
			fmt.Fprintf(w, "%v", err)
			return
		}
		defer file.Close()

		mimeType := handle.Header.Get("Content-Type")
		switch mimeType {
		case "image/jpeg":
			saveFile(w, file, handle, path, filename+".jpg")
		case "image/png":
			saveFile(w, file, handle, path, filename+".png")
		case "application/pdf":
			saveFile(w, file, handle, path, filename+".pdf")
		default:
			jsonResponse(w, http.StatusMultiStatus, fmt.Sprintf(`{"message": "The format file is not valid."}`))
		}
	})
}
func License(res bool, message string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}
		jsonResponse(w, http.StatusOK, fmt.Sprintf(`{"message": "%s", "status": %v}`, message, res))
	})
}
func saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader, path string, filename string) {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	rootPath := models.GetFilePath()
	CreateDirIfNotExist(rootPath + path)
	// err = ioutil.WriteFile(rootPath+path+"/"+handle.Filename, data, 0666)
	err = ioutil.WriteFile(rootPath+path+"/"+filename, data, 0666)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	jsonResponse(w, http.StatusCreated, fmt.Sprintf(`{"message": "File Uploaded"}`))
}
func jsonResponse(w http.ResponseWriter, code int, message string) {
	// w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	fmt.Fprint(w, message)
}
func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			panic(err)
		}
	}
}

func EncodeCursor(i *string) graphql.ID {
	return graphql.ID(base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("cursor%s", *i))))
}

func DecodeCursor(after *string) (*string, error) {
	var decodedValue string
	if after != nil {
		b, err := base64.StdEncoding.DecodeString(*after)
		if err != nil {
			return nil, err
		}
		i := strings.TrimPrefix(string(b), "cursor")
		decodedValue = i
	}
	return &decodedValue, nil
}
func RandomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(RandInt(1000, 9999))
	}
	return string(bytes)
}

func RandInt(min int, max int) int {
	return min + rand.Intn(max-min)
}
func NewNullString(s string) sql.NullString {
	// if s == nil {
	// 	return sql.NullString{}
	// }
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
func NewNullStringPointer(s *string) sql.NullString {
	if s == nil {
		return sql.NullString{}
	}
	if len(*s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: *s,
		Valid:  true,
	}
}
func SubstringDate(date string) string {
	if len(date) == 14 {
		return date[0:4] + "-" + date[4:6] + "-" + date[6:8] + " " + date[8:10] + ":" + date[10:12] + ":" + date[12:14]
	}
	return date[0:4] + "-" + date[4:6] + "-" + date[6:8]
}
func RemoveDashDate(date string) string {
	return strings.Replace(date, "-", "", -1)
}
func GetEnums(Type string) []models.Enum {
	var e enum.Enum
	Type = strings.ToLower(Type)
	Type = strings.Replace(Type, "_", "-", -1)
	var listEnum = []models.Enum{}
	e.LoadEnum()
	for _, tipe := range e {
		if tipe.TypeName == Type {
			for _, obj := range tipe.Types {
				listEnum = append(listEnum, models.Enum{
					Type:        tipe.TypeName,
					Key:         obj.Key,
					Value:       obj.Value,
					Description: obj.Description,
				})
			}
		}
	}
	return listEnum
}
func GetEnumByKey(key string, enums []models.Enum) *models.Enum {

	for _, n := range enums {
		if n.Key == key {
			return &n
		}
	}
	return nil
}
func GetEnumByValue(val string, enums []models.Enum) *models.Enum {
	for _, n := range enums {
		if n.Value == val {
			return &n
		}
	}
	return nil
}
func ArrayContains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func CountDivMul(t1, t2 time.Time) int {
	d1 := t1.Unix() / 86400 * 86400
	return int((t2.Unix() - d1) / 86400)
}
