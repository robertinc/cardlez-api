package enum

import (
	"encoding/json"
	"fmt"
	"os"
)

type Enum []struct {
	TypeName string `json:"typeName"`
	Types    []struct {
		Key         string `json:"key"`
		Value       string `json:"value"`
		Description string `json:"description"`
	} `json:"types"`
}

func (e *Enum) LoadEnum() {
	file, err := os.Open("enum.json")
	defer file.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(file)
	jsonParser.Decode(&e)
}
