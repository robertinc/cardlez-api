# Cardlez-API CORE Module

## Description
- This is contain only logic for core business
- This package will be called from cardlez-api-ext project
- If this package updated, then it would be pushed to all client server and re-build the cardlez-api-ext with newer cardlez-api, so ensure your change compatible with cardlez-api-ext (with testing to each cardlez-api-ext of course)
- If updates not compatible, then discuss with other cardlez developer as well
