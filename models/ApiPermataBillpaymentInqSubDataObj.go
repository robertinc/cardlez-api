package models

type ApiPermataBillpaymentInqSubDataObj struct {
	MsgRsHdr        ApiPermataFundTransferInqDataObjMsgRsHdr
	BillInquiryInfo ApiPermataBillInquiryInfo
	BillRefNo       string
	InstCode        string
}
