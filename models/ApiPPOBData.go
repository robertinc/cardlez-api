package models

type ApiPPOBData struct {
	IsAuthorized     bool
	RejectionMessage string
	Token            string
	ExpiredIn        int32

	IsSuccess        bool
	ErrorCode        int32
	ErrorDescription string
	ClientID         string
	SessionID        string
	TransactionCode  string
	Data             string
	DataObject       ApiPPOBDataObject
	DataBiller       APIBillpayment

	BillNumber          string
	UserReferenceNumber string
	InqReferenceNumber  string
	TransactionNumber   string
	Saldo               float64
	Nominal             float64
	Note                string
	TimeStamp           string
}
