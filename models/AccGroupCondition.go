package models

type AccGroupCondition struct {
	AccGroupCondition_ID           string
	AccGroupCondition_No           string
	Minimum_Balance                float64
	Deposit_Period                 int16
	Minimum_Deposit                float64
	Maximum_Deposit                float64
	Withdrawal_Period              int16
	Minimum_Withdrawal             float64
	Maximum_Withdrawal             float64
	IsDebit_Restrict               bool
	Interest_Post_Period           int16
	Inactive_Months                int16
	Rev_No                         int32
	Rev_Remarks                    string
	Record_Status                  int16
	User_Insert                    string
	Date_Insert                    string
	User_Author                    string
	Date_Author                    string
	User_Update                    string
	Date_Update                    string
	Maximum_Transfer_Antar_Nasabah float64
	Maximum_Transfer_Antar_Bank    float64
	OverbookingDailyLimit          int64
	TransferDailyLimit             int64
	OverbookingMinAmount           float64
	TransferMinAmount              float64
	OverbookingMaxDailyAmountTotal float64
	TransferMaxDailyAmountTotal    float64
}
