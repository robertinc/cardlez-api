package models

type AccountingReport struct {
	PeriodStart  string
	PeriodEnd    string
	AccountNo    string
	AccountType  string
	Transactions []Transaction
}
