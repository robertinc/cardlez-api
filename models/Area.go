package models

type Area struct {
	Area_ID			string
	KodePos       	string
	Kelurahan       string
	Kecamatan     	string
	Kota     		string
	Propinsi    	string
	District_ID    	string
}