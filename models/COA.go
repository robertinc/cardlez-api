package models

type COA struct {
	ID            string
	Code          string
	Description   string
	ParentCOAID   string
	ParentCOAName string
	COACategory   string
}
