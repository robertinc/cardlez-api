package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MemberCoupon struct {
	ID               string
	Code             string
	MemberID         string
	PromoCouponID    string
	PromoCode        string
	PromoAmount      float64
	ExchangeDate     string
	ExpiryDate       string
	IsRedeem         bool
	RedeemDate       string
	RedeemTransRefNo string
}
