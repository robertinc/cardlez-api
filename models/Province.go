package models

type Province struct {
	Province_ID 	string
	Province_No   	string
	Province_Name 	string
}
