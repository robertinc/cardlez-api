package models

import (
	"database/sql"
)

type TopUpBillPayment struct {
	ID              string
	Code            string
	DebitAccountID  string
	Amount          float64
	FeeAmount       float64
	DebitDate       string
	CustomerNo      string
	CustomerName    string
	CoreReffNo      sql.NullString
	BillerCode      string
	PlnToken        string
	RecordStatus    int
	UserInsert      string
	DateInsert      string
	UserUpdate      sql.NullString
	DateUpdate      sql.NullString
	TransactionCode string
}
