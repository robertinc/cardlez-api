package models

type Product struct {
	ID                  string
	Code                string
	ProductName         string
	CompanyID           string
	AccGroupConditionID string
	RecordStatus        int32
	PrefixCode          string
	ProductType         int32
	MinDepositAmount    float64
}
