package models

type ApiFundTransfer struct {
	Data             ApiFundTransferData
	IsSuccess        bool
	ErrorCode        int32
	ErrorDescription string
	HTTPStatusCode   string
}
