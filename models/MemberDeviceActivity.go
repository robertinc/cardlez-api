package models

import (
	"time"

	_ "github.com/denisenkom/go-mssqldb"
)

type MemberDeviceActivity struct {
	ID              string
	MemberID        string
	ActivityType    int32
	ActivityDate    time.Time
	ActivityRemarks string
	Imei            string
}
