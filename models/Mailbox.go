package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type Mailbox struct {
	ID            string
	MemberID      string
	Code          string
	MailDate      string
	MailSubject   string
	MailDesc      string
	MailImages    string
	PublishStatus int32
	MailDirection int32
	ReadStatus 	  int32
}
