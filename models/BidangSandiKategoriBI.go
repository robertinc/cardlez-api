package models

type BidangSandiKategoriBI struct {
	BidangSandiKategoriBIID string
	BidangSandiID           string
	NoHeadKategori          string
	KodeKategori            string
	BidangSandiNo           string
	BidangSandi             string
}
