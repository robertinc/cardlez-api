package models

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

type CompanyUser struct {
	ID                string
	Code              string
	UserName          string
	UserEmail         string `json:"email"`
	UserPass          string `json:"password"`
	Imei              string `json:"imei"`
	IsAdmin           bool
	IsActivation      bool
	Token             string
	CompanyUserRoleID string
	CompanyID         string
	TokenEmail        string
	RecordStatus      int
	ClaimDate         string
	IsCheckHPRegis    bool
	CaptchaToken      string
	IsLocked          bool
}

func (user *CompanyUser) ComparePassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(user.UserPass), []byte(password))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
	// if user.UserPass == password {
	// 	return true
	// } else {
	// 	return false
	// }
}
