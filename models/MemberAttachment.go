package models

type MemberAttachment struct {
	ID             string
	MemberID       string
	AttachmentType int32
	AttachmentName string
	AttachmentExt  string
	UserInsert     string
	DateInsert     string
	FileBase64Str  string
}
