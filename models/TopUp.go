package models

type TopUp struct {
	ID               string
	Code             string
	BillerCode       string
	CustomerNumber   string
	SourceAccountNo  string
	CustomerName     string
	FeeAmount        float64
	Amount           float64
	DenomCode        string
	Status           string
	Message          string
	ReffNo           string
	CoreReffNo       string
	PaymentReffNo    string
	CustomerId       string
	AdminFee         float64
	IsAdminFeeExist  bool
	IsFeeAmountExist bool
	AmountTotal      float64
	BillInquiryInfo  []BillInquiryInfo
	AdditionalInfo   []TopUpAdditionalInfo
}
