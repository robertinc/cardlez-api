package models

type JournalDetail struct {
	ID              string
	JournalID       string
	COAID           string
	Debit           float64
	Credit          float64
	Notes           string
	ReferenceNumber string
}
