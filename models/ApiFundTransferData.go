package models

type ApiFundTransferData struct {
	IsSuccess        bool
	ErrorCode        int32
	ErrorDescription string
	ClientID         string
	SessionID        string
	Data             string
}
