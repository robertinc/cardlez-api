package models

type ReportPaymentCity struct {
	City   string
	Amount float64
}
