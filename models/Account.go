package models

type Account struct {
	ID              string
	Code            string
	AccountName     string
	AccountCode     string
	AccountName2    string
	RecordStatus    int32
	MemberType      int32
	ProductName     string
	ProductID       string
	CompanyID       string
	MemberName      string
	MemberID        string
	MemberBranchID  string
	Email           string
	Phone           string
	Balance         float64
	ClosingBalance  float64
	BlockingBalance float64
	BalanceMerchant float64
	IsPrimary       bool
	ProductType     int32
	AccountType     int32
	CIF             string
	DateInsert      string
	MemberCode      string
	AccountDetail   AccountDetail
}

type AccountFilter struct {
	NoKTP *string
	NoHP  *string
	Name  *string
}
