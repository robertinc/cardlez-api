package models

type MessageLog struct {
	ID              string
	TransactionDate string
	Message         string
	Status          string
	Tipe            string
	Sender          string
	UserInsert      string
}
