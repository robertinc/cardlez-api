package models

type PromoCouponDestination struct {
	ID          	string
	PromoCouponID   string
	MemberID 		string
}
