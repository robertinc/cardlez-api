package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MemberPaymentDetail struct {
	ID              string
	MemberPaymentID string
	Amount          float64
	Note            string
}
