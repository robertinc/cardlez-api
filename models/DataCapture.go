package models

type DataCapture struct {
	ID                   string
	Code                 string
	Description          string
	TransactionDate      string
	DataCaptureAmountsID []string
	DataCaptureAmount    []DataCaptureAmount
	CompanyUserID        string
}
