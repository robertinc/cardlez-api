package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MemberPayment struct {
	ID                  string
	Code                string
	DebitAccountID      string
	CurrencyID          string
	Amount              float64
	DebitDate           string
	TransferType        string
	CreditAccountID     string
	CreditDate          string
	Note                string
	POSTransactionRefNo string
	POSName             string
	POSTransactionDate  string
	RevNo               int32
	RecordStatus        int16
	MemberPaymentDetail []MemberPaymentDetail
	DebitAccountName    string
	CreditAccountName   string
}
