package models

type Bank struct {
	ID           string
	Code         string
	BankName     string
	TransferCode string
}
