package models

type LoanProduct struct {
	LoanProductID            string
	LoanProductNo            string
	ProductName              string
	InterestCalcType         int32
	InterestDayBasisID       string
	InterestPercentage       float64
	CalcBase                 int32
	SchemeType               int32
	ScheduleType             int32
	CollectionType           int32
	RoundingType             int32
	PenaltyTerminationAmount float64
	MinPenaltyTermination    int32
	PenaltyAmount            float64
	MinLoanAmount            float64
	RecordStatus             int32
	CompanyBranchID          string
	UserInsert               string
	DateInsert               string
	UserUpdate               string
	DateUpdate               string
	Deleted                  bool
	GracePeroid              int32
}
