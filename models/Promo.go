package models

type Promo struct {
	ID          string
	Image       string
	Description string
}
