package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MemberLogin struct {
	ID             string
	MemberBranchID string
	EmailAddress   string
	LoginPass      string
	Phone          string
	RecordStatus   int64
}
