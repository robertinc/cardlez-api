package models

type Company struct {
	ID          	string
	Code        	string
	CompanyName 	string
	CompanyType 	int32
	Mnemonic    	string
	BaseURL     	string
	ImageLogo   	string
	BankCode    	string
	BankName     	string
	AccountName   	string
	AccountNo   	string
}
