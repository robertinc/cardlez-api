package models

type PermataFundTransfer struct {
	ID                 string
	AccountNoOrigin    string
	AccountNameOrigin  string
	AccountNoDest      string
	AccountNameDest    string
	BankID             string
	BankName           string
	Email              string
	TrxDesc            string
	TrxDesc2           string
	AccountDestPhoneNo string
	DatiII             string
	TkiFlag            string
	Amount             float64
	Charge             string
	RecordStatus       string
	StatusDesc         string
	UserInsert         string
	DateInsert         string
	UserUpdate         string
	DateUpdate         string
	ReffNo             string
}
