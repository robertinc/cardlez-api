package models

type ReportWithdrawal struct {
	SaldoSimpananMobile  float64
	SaldoVAPermata       float64
	SaldoRekeningBiller  float64
	SaldoYangBisaDitarik float64
	SaldoRekeningInvelli float64
	TotalKepemilikanDana float64
}
