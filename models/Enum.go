package models

type Enum struct {
	Type        string
	Key         string
	Description string
	Value       string
}
