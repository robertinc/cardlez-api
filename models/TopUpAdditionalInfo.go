package models

type TopUpAdditionalInfo struct {
	ReffNo         string
	Kwh            string
	Token          string
	Name           string
	CustomerNo     string
	TarifDaya      string
	Periode        string
	JumlahTagihan  string
	Amount         string
	NoMeter        string
	RpBayar        string
	Materai        string
	PPN            string
	PPJ            string
	Angsuran       string
	RpToken        string
	AdminBank      string
	StandMeter     string
	Period         string
	ReffNoBPJS     string
	Fee            string
	Bill           string
	BillerReffNo   string
	MemberTotal    string
	CustomerName   string
	FinalTotalBill string
	Retribution    string
	Penalty        string
	WaterUsage     string
	WaterBill      string
	AdminBill      string
	TotalBill      string
}
