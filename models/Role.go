package models

type Role struct {
	ID       string
	Code     string
	RoleName string
}
