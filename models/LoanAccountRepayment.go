package models

type LoanAccountRepayment struct {
	ID                     string
	LoanAccountID          string
	RepaymentType          int32
	RepaymentDate          string
	RepaymentActualDate    string
	RepaymentInterest      float64
	RepaymentPrincipal     float64
	RepaymentAmount        float64
	RepaymentInterestPaid  float64
	RepaymentPrincipalPaid float64
	OutStandingBakiDebet   float64
	TellerId               string
	IsPaid                 bool
	AmountPaid             float64
	PaymentTxnID           string
	RecordStatus           int32
	UserInsert             string
	DateInsert             string
	UserUpdate             string
	DateUpdate             string
	LoanCode               string
	ProductName            string
	MemberName             string
	AccountNo              string
}
