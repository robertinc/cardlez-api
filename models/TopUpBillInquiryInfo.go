package models

type BillInquiryInfo struct {
	BillAmountSign string
	BillReff       string
	BillAmount     string
	BillCurrency   string
	TarifDaya      string
	MeterNo        string
	CustomerNo     string
	Name           string
	Periode        string
	TotalTagihan   string
	TotalPeriod    string
	CustomerName   string
	MemberTotal    string
	NumberOfMonth  string
	Penalty        string
	Retribution    string
	WaterBill      string
	WaterUsage     string
	AdminBill      string
	StandMeter     string
	Fine1TotalBill string
}
