package models

type Category struct {
	ID   string
	Name string
}
