package models

type ApiPermataFundTransferInqDataObjOlXferInqRs struct {
	MsgRsHdr  ApiPermataFundTransferInqDataObjMsgRsHdr
	XferInfo  ApiPermataFundTransferInqDataObjXferInfo
	TrxReffNo string
}
