package models

type RoleMenu struct {
	ID            string
	CompanyRoleID string
	MenuID        string
	Role          string
	Menu          string
	AccessLevel   int32
	ParentMenuID  string
}
