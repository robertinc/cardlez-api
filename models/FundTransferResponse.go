package models

type FundTransferResponse struct {
	CardlezResponse
	Status                 string `json:"status"`
	TransferStatus         string `json:"transaction_status"`
	ReffNo                 string `json:"core_reff_no"`
	SourceAccountNo        string `json:"source_account_no"`
	BeneficiaryBankCode    string `json:"beneficiary_bank_code"`
	BeneficiaryBankName    string `json:"beneficiary_bank_name"`
	BeneficiaryAccountNo   string `json:"beneficiary_account_no"`
	BeneficiaryAccountName string `json:"beneficiary_account_name"`
	BankTransactionID      string `json:"bank_transaction_id"`
}

//CardlezResponse for response
type CardlezResponse struct {
	Status         string `json:"status"`
	Message        string `json:"message"`
	TraceNo        string `json:"trace_no"`
	HTTPStatusCode string
	Errors         map[string]interface{} `json:"errors"`
}
