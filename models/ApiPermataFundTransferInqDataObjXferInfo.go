package models

type ApiPermataFundTransferInqDataObjXferInfo struct {
	ToAccount         string
	ToAccountFullName string
	BankId            string
	BankName          string
}
