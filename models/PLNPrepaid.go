package models

import "time"

type PLNPrepaid struct {
	ID                    string
	TransactionDate       time.Time `json:"TransactionDate"`
	RequestAccountNo      string
	RequestCustomerNumber string
	RequestAmount         float64

	ResponseMeterNo       string    `json:"MeterNo"`
	ResponsePLNCustomerNo string    `json:"PLNCustomerNo"`
	ResponseCustomerName  string    `json:"CustomerName"`
	ResponseBillPeriod    string    `json:"BillPeriod"`
	ResponseNumberOfBill  string    `json:"NumberOfBill"`
	ResponseUsage         string    `json:"Usage"`
	ResponseTrxType       string    `json:"TrxType"`
	ResponseBillAmount    float64   `json:"BillAmount"`
	ResponseTarif         float64   `json:"Tarif"`
	ResponseAdminFee      float64   `json:"AdminFee"`
	ResponseTotalBayar    float64   `json:"TotalBayar"`
	ResponseKodeToken     string    `json:"KodeToken"`
	ResponseDate          time.Time `json:"Date"`
	ResponseKWH           string    `json:"KWH"`
	ResponseType          string    `json:"Type"`
	ResponseFiller        string    `json:"Filler"`
	ResponseType2         string    `json:"Type2"`
	ResponseRawMessage    string    `json:"RawMessage"`
	ResponseReference     string    `json:"Reference"`

	AdminAmount   float64
	PartnerAmount float64
	InvelliAmount float64

	StatusCode               string `json:"StatusCode"`
	CompanyID                string
	NomorTransaksi           string
	NomorTransaksiSettlement string
	PostingDate              string `json:"PostingDate"`
	StrukNo                  string `json:"StrukNo"`
	MessagePrinter           string
	TransStatus              int
	UserInsert               string
	DateInsert               string
	UserUpdate               string
	DateUpdate               string
	UserAuthorize            string
	DateAuthorize            string
}
