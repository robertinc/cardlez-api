package models

type AccGroupConditionDetail struct {
	AccGroupConditionDetail_ID string
	AccGroupCondition_ID       string
	Minimum_Saldo              float64
	Maximum_Saldo              float64
	Interest_Rate              float64
	User_Insert                string
	Date_Insert                string
	User_Update                string
	Date_Update                string
	MinDepositAmount           float64
	Tenor                      int32
}
