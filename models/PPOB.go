package models

import "time"

type PPOB struct {
	ID                 string
	TransactionDate    time.Time `json:"TransactionDate"`
	RequestAccountNo   string
	RequestPhoneNumber string
	RequestAmount      float64

	ResponsePhoneNumber string  `json:"PhoneNo"`
	ResponseAmount      float64 `json:"Amount"`
	ResponseReference   string  `json:"Reference"`
	ResponseRawMessage  string  `json:"RawMessage"`

	AdminAmount   float64
	PartnerAmount float64
	InvelliAmount float64

	StatusCode     string `json:"StatusCode"`
	CompanyID      string
	MessagePrinter string
	TransStatus    int
	UserInsert     string
	DateInsert     string
	UserUpdate     string
	DateUpdate     string
	UserAuthorize  string
	DateAuthorize  string

	NomorTransaksi           string
	NomorTransaksiSettlement string
	Operator string
}

type CheckPPOB struct {
	Status          string `json:"status"`
	ReffNo          string `json:"noRef"`
	TransactionDate string `json:"tanggal"`
	Serial          string `json:"serial"`
}
