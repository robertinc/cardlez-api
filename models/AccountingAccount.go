package models

type AccountingAccount struct {
	ID          string
	Code        string
	COAName     string
	Amount      float64
	AccountName string
}
