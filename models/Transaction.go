package models

//Transaction Model
type Transaction struct {
	JournalID           string
	PostingDate         string
	PostingDateCurrency string
	Description         string
	DebitAmount         float64
	CreditAmount        float64
	DateInsert          string
}
