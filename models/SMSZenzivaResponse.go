package models

type SMSZenzivaResponseSuccess struct {
	//MessageID string  `json:"messageId"` bikin error
	To     string  `json:"to"`
	Status string  `json:"status"`
	Text   string  `json:"text"`
	Cost   float64 `json:"cost"`
}
type SMSZenzivaResponseFailed struct {
	//MessageID string `json:"messageId"`
	To     string `json:"to"`
	Status string `json:"status"`
	Text   string `json:"text"`
}
