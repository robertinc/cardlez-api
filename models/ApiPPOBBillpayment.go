package models

import (
	"database/sql"
)

type ApiPPOBBillPayment struct {
	ID                     string
	Code                   string
	DebitAccountID         string
	Amount                 float64
	FeeAmount              float64
	DebitDate              string
	CustomerNo             string
	CustomerName           string
	CoreReffNo             sql.NullString
	BillerCode             string
	PlnToken               sql.NullString
	SessionIDInquiry       string
	SessionIDPayment       string
	InqReferenceNumber     string
	TransactionCode        string
	InquiryTransactionCode string

	PLNCustomerNo   string
	PLNCustomerName string
	BillPeriod      string
	Usage           string
	TrxType         string
	MeterNo         string
	PaymentDate     string
	KWH             string
	Type            string
	Message         string

	RecordStatus int
	UserInsert   string
	DateInsert   string
	UserUpdate   sql.NullString
	DateUpdate   sql.NullString
}
