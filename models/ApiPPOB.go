package models

type ApiPPOB struct {
	DataObject       ApiPPOBDataObject
	Data             ApiPPOBData
	DataBiller       APIBillpayment
	IsSuccess        bool
	ErrorCode        int32
	ErrorDescription string

	TransactionCodeSubmit        string
	InquiryTransactionCodeSubmit string
	DataSubmit                   string
	PhoneNumber                  string
	BillNumber                   string
	SessionIDInquiry             string
	SessionIDPayment             string
	InqReferenceNumber           string

	//account information
	SourceAccountNo string
	CustomerNo      string
	CustomerName    string

	Amount        float64
	BillPaymentID string
	JournalCode   string
}
