package models

type PermataOVO struct {
	ID                 string
	BillName           string
	NumberOfRecord     string
	BillReff1          string
	BillCurrency1      string
	BillAmountSign1    string
	BillReff2          string
	BillCurrency2      string
	BillAmountSign2    string
	RecordStatus       string
	StatusDesc         string
	UserInsert         string
	DateInsert         string
	UserUpdate         string
	DateUpdate         string
	BillType           string
	InstCode           string
	BillNumber         string
	TrxAmount          string
	Currency           string
	UserId             string
	DebAccNumber       string
	DebAccName         string
	DebAccCur          string
	BillRefNo          string
	AccountNoPermata   string
	AccountNamePermata string
	AdminFee           float64
	TotalAmount        string
	BillAmount1        string
	BillAmount2        string
}
