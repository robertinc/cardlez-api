package models

type DepositContract struct {
	ID                         string
	Code                       string
	DepositsID                 string
	RetailProductID            string
	DepositsAmount             float64
	StartDate                  string
	InterestPercentage         float64
	RolloverMethod             int32
	InterestPaymentDue         bool
	RolloverMonth              int32
	PostingForwardType         int32
	CreditAccountID            string
	Remarks                    string
	BaseRateKey				   float64
	RecordStatus               int32
	UserInsert                 string
	DateInsert                 string
	UserUpdate                 string
	DateUpdate                 string
	DepositContractCollections []DepositContractCollection
	TypeAro                    int32
	PeriodeBunga               int32
}
