package models

type ReportPaymentProvince struct {
	Province string
	Amount   float64
}
