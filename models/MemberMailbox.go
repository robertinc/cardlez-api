package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MemberMailbox struct {
	ID            string
	MemberID      string
	Code          string
	MailboxID     string
	MailboxCode   string
	MailDate      string
	MailSubject   string
	MailDesc      string
	MailImages    string
	PublishStatus int32
	MailDirection int32
	ReadStatus    int32
}
