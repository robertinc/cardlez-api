package models

import (
	"database/sql"
)

type JournalMapping struct {
	ID              string
	Code            sql.NullString
	TransactionName string
	Remarks         string
	DefaultValue    string
}
