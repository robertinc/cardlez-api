package models

import (
	"database/sql"
)

type Journal struct {
	ID               string
	Code             sql.NullString
	PostingDate      string
	Description      sql.NullString
	DebitAmount      float64
	CreditAmount     float64
	COA              sql.NullString
	COADescription   sql.NullString
	TransactionID    string
	JournalDetails   []JournalDetail
	JournalMappingID string
	AccountNo        string
	AccountName      string
	SaldoAwal        float64
	SaldoAkhir       float64
	ReferenceNumber  string
	JournalDetailID  string
}
