package models

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type PromoCoupon struct {
	ID                 string
	Code               string
	PromoImages        string
	PromoTitle         string
	PromoDesc          string
	StartDate          string
	EndDate            string
	LoyaltyID          string
	PoinAmount         int32
	PromoAmount        float64
	IsPoin             bool
	AmountIsPercentage bool
	MaxPromoAmount     float64
	MaxPromoUsage      int32
	DestinationType    int32
	Category           string
	MinPromoAmount     float64
}
type MemberPromoResult struct {
	PromoCouponx  []*PromoCoupon
	MemberCouponx []*MemberCoupon
}
