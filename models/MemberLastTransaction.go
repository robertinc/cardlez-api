package models

import (
	"time"

	_ "github.com/denisenkom/go-mssqldb"
)

type MemberLastTransaction struct {
	DateInsert time.Time
	Amount     float64
	Notes      string
}
