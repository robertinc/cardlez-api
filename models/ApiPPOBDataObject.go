package models

type ApiPPOBDataObject struct {
	PhoneNumber         string
	Amount              float64
	Message             string
	InqReferenceNumber  string
	UserReferenceNumber string
	TimeStamp           string

	// PLN PostPaid
	PLNCustomerNo string
	CustomerName  string
	BillPeriod    string
	Usage         string
	TrxType       string

	// PLN PrePaid
	MeterNo     string
	Type        string
	KodeToken   string
	PaymentDate string
	KWH         string
}
