package models

type Deposit struct {
	ID                     string
	Code                   string
	CustomerID             string
	DepositsName           string
	CurrencyID             string
	CompanyUserID          string
	SourceFund             int32
	DebitAccountID         string
	DestinationFund        int32
	InterestCapitalisation int32
	Remarks                string
	DepositType            int32
	RecordStatus           int32
	UserInsert             string
	DateInsert             string
	UserUpdate             string
	DateUpdate             string
	CustomerName           string
	CustomerCode           string
	CustomerNoInduk        string
	DepositContracts       []DepositContract
}
