package models

import (
	"database/sql"

	_ "github.com/denisenkom/go-mssqldb"
)

type MemberBranch struct {
	ID             string
	MemberID       string
	BusinessType   int64
	CompanyName    sql.NullString
	BankID         string
	CompanyAddress sql.NullString
	Province       sql.NullString
	City           sql.NullString
	MemberLogins   []MemberLogin
}
