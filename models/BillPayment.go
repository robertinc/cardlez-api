package models

type BillPayment struct {
	ID              string
	Code            string
	Description     string
	PartnerAmount   float64
	AdminAmount     float64
	InvelliAmount   float64
	AgentAmount     float64
	TransactionCode string
	Operator        string
	BasePrice       float64
	PurchaseAmount  float64
	SellPrice       float64
	BillerCode      string
	Nominal         float64
	// Field Base
	InquiryTransactionCode string
	Prefix                 string
	PrefixList             []string
}
