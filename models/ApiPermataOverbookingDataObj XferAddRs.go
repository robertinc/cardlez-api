package models

type ApiPermataOverbookingDataObjXferAddRs struct {
	MsgRsHdr  ApiPermataFundTransferInqDataObjMsgRsHdr
	TrxReffNo string
}
