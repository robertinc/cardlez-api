package models

type TransactionLog struct {
	ID           string
	URLSender    string
	ModuleName   string
	URLReceiver  string
	Request      string
	Response     string
	RecordStatus string
	StatusDesc   string
	UserInsert   string
	DateInsert   string
	UserUpdate   string
	DateUpdate   string
}
