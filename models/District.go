package models

type District struct {
	District_ID 	string
	District_No   	string
	District_Name 	string
	LBU_Code 	string
	Province_ID 	string
	Postal_Code_Prefix 	string
}
