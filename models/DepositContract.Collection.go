package models

type DepositContractCollection struct {
	ID                 string
	DepositsContractID string
	CollectionDate     string
	CollectionAmount   float64
	IsPaid             bool
	UserInsert         string
	DateInsert         string
	UserUpdate         string
	DateUpdate         string
	TransferDate       string
	IsTransfer         bool
	Remark             string
	InterestAmount	   float64
}
