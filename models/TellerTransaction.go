package models

import (
	"database/sql"
)

type TellerTransaction struct {
	ID                   string
	Code                 string
	CompanyID            string
	AccountSourceID      string
	AccountDestinationID string
	CustomerSourceName   sql.NullString
	TransType            int32
	TransAmount          float64
	Note                 sql.NullString
	RefNo                string
	TellerID             string
	TxnCodeDb            string
	TransactionDate      string
	RecordStatus         int32
	AgentName            string
	MemberPhone          string
	Pemohon              string
	DateUpdate           string
}
