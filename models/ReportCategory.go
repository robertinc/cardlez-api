package models

type ReportCategory struct {
	Category string
	Amount   float64
}
