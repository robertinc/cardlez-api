package models

type SyncMutationTransaction struct {
	ID         string
	DateStart  string
	DateFinish string
	Type       int32
	Status     int32
	DateInsert string
	UserInsert string
	DateUpdate string
	UserUpdate string
	//20200616
	StartDateRequest  string
	FinishDateRequest string
}
