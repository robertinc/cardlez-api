package models

type DataCaptureAmount struct {
	ID              string
	TransactionType string
	IsSaving        bool
	ProductName     string
	DCAmount        float64
	COAID           string
	ReferenceNumber string
}
