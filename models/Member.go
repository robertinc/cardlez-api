package models

import (
	"database/sql"

	_ "github.com/denisenkom/go-mssqldb"
)

type Member struct {
	ID                    string
	Code                  sql.NullString
	Phone                 sql.NullString
	Handphone             sql.NullString
	Email                 sql.NullString
	MemberName            sql.NullString
	MemberType            int64
	IsLocked              sql.NullBool
	LoginPIN              sql.NullString
	IdentityType          int64
	Gender                int64
	Nationality           int64
	MaritalStatus         int64
	BankID                string
	RecordStatus          int64
	VirtualAccountNo      sql.NullString
	ProfileImage          sql.NullString
	CategoryID            sql.NullString
	Category              Category
	MemberBranches        []MemberBranch
	PlayerID              sql.NullString
	MemberCoupons         []MemberCoupon
	IdentityNumber        sql.NullString
	Address               sql.NullString
	City                  sql.NullString
	Province              sql.NullString
	GoldStatus            int64
	BirthPlace            string
	BirthDate             string
	MotherMaiden          string
	VerificationExpiry    string
	IdentityUpload        string
	NPWPUpload            string
	VerificationCodeEmail string
	FotoKTPDiriUpload     string
	LoginPass             string
	UserUpdate            string
	UserInsert            string
	DateUpdate            string
	DateInsert            string
	UserAuthor            string
	DateAuthor            string
	Imei                  string
	DateActivatedByMember string
	// Added new field 2019-08-15
	NoPaspor            sql.NullString
	NoAkteLahir         sql.NullString
	SIUP                sql.NullString
	TDP                 sql.NullString
	Agama               sql.NullString
	Akte                sql.NullString
	JumlahTagungan      sql.NullString
	PendapatanPerbulan  sql.NullString
	NamaPasangan        sql.NullString
	NoIdentitasPasangan sql.NullString
	RTRW                sql.NullString
	Kelurahan           sql.NullString
	KodePos             sql.NullString
	Kecamatan           sql.NullString
	NoNPWP              sql.NullString
	// added new 20190906
	NoInduk              sql.NullString
	AlamatRumah2         sql.NullString
	AlamatTempatKerja1   sql.NullString
	AlamatTempatKerja2   sql.NullString
	PropinsiTempatKerja  sql.NullString
	KabupatenTempatKerja sql.NullString
	KecamatanTempatKerja sql.NullString
	KelurahanTempatKerja sql.NullString
	KodePosTempatKerja   sql.NullString
	FileNameFotoKTP      sql.NullString
	FileNameFotoSelfie   sql.NullString
	BankAccountName      sql.NullString
	BankAccountNo        sql.NullString
	BankCode             sql.NullString
	Grade                sql.NullString
	ReferralMemberID     sql.NullString
	LimitID              sql.NullString
	LimitAmount          float64
	IsUpload             sql.NullBool
	//Peruri 20200602
	PeruriEmailAddress       sql.NullString
	IsNeedVerificationPeruri sql.NullBool
	FilePDFName              string
	Base64PDF                string
	OrderID                  string
	TokenOTP                 string
	//memberdetail from core
	ProvinsiID          string
	KotaID              string
	KodePekerjaan       string
	NamaKodePekerjaan   string
	KodeBidangUsaha     string
	NamaKodeBidangUsaha string
	NamaPerusahaan      string
}
