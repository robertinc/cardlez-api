package models

type Menu struct {
	ID           string
	ParentMenuID string
	MenuName     string
	Icon         string
	Link         string
	Sequence     int32
}
