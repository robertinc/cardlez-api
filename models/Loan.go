package models

type Loan struct {
	AccountNo               string
	CustomerName            string
	ProductCode             string
	ProductDesc             string
	BranchCode              string
	BranchName              string
	StartDate               string
	EndDate                 string
	LoanFacility            string
	PlafondAmount           float64
	InstallmentAmount       float64
	OutstandingAmount       float64
	AOCode                  string
	AOName                  string
	Status                  string
	LoanStartAmount         float64
	CollectibilityScore     int32
	CollectibilityScoreDesc string
	MemberReferralCode      string
	MemberReferralName      string
	RevRemarks              string

	//core
	LoanID             string
	Code               string
	CustomerID         string
	LoanProductID      string
	TenorMonth         int32
	LoanAmount         float64
	InterestPercentage float64
	RepaymentMethod    int32
	AccountID          string
	UserInsert         string
	DateInsert         string
	UserAuthor         string
	DateAuthor         string
	AuthorName         string
	RecordStatus       int32
	IsLiquidated       bool
	NominalWajib       float64
	LoanAccountID      string
	//excel
	CustomerNoInduk      string
	CustomerCode         string
	NamaKelompok         string
	NominalAngsuranPokok float64
	NominalAngsuranBunga float64
	AlamatRumah          string
	Provinsi             string
	Kota                 string
	Kecamatan            string
	Kelurahan            string
	KodePos              string
	NomorHandhpone       string
	Pekerjaan            string
	TglLahir             string
	//signing document
	FilePDFName string
	Base64PDF   string
	OrderID     string
	TokenOTP    string
	//upload pdf file
	SLIKPDF          string
	CreditScoringPDF string

	LoanAccountRepaymentFromCore []*LoanAccountRepayment
}
