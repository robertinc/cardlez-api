package models

type ReportPaymentArea struct {
	ID     string
	Area   string
	Amount float64
}
