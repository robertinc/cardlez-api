package models

type AccountDetail struct {
	ProductCode         string
	ProductDesc         string
	AccountNo           string
	BilyetNo            string
	DepositAmount       float64
	BranchName          string
	BranchCode          string
	StartDate           string
	MatureDate          string
	Tenor               string
	DepositRate         float64
	InterestGrossAmount float64
	RolloverType        string
	InterestNetAmount   float64
}
