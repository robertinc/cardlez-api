package models

type InformationClause struct {
	ID           string
	SequenceNo   int32
	HeadInfo     string
	DetailInfo   string
	GroupInfo    string
	RecordStatus int32
	UserInsert   string
	DateInsert   string
	UserUpdate   string
	DateUpdate   string
}
