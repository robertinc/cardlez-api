package models

type AccountCoreParameter struct {
	ID              string
	AccountName     string
	AccountName2    string
	AccountNo		string
	Type 			string
	CustomerID		string
	IsPrimary		bool
}