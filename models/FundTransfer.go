package models

import (
	"database/sql"
)

type FundTransfer struct {
	ID                string
	Code              string
	TransferDirection bool
	DebitCompanyID    string
	CreditCompanyID   string
	DebitAccountID    string
	CurrencyID        string
	Amount            float64
	DebitDate         string
	BankCode          string
	BankAccountName   string
	TransferType      int32
	CreditAccountID   string
	CreditAccountName string
	CreditDate        string
	Note              string
	RevNo             int32
	RecordStatus      int32
	IsTab             bool
	Remarks           sql.NullString
	IsTabCredit       bool
	MemberID          string
	BankID            string
	BankName          string
	MemberName        string
	Handphone         string
	AccountCode       string
	ReffNo            string
	BankTransactionID string

	NonMember              bool
	AccountNoOrigin        string
	AccountNoDestination   string
	AccountNameDestination string
	ProductIDOrigin        string
	ProductIDDestination   string
	HandphoneTujuan        string

	IsSuccess                bool
	Description              string
	TransactionDate          string
	Token                    sql.NullString
	SessionIDInquiry         string
	NomorTransaksiSettlement string
	DateUpdate               string
	FeeAmount                float64
	RecordStatusSync      	 int32
	Counter      	 		 int32
}
