package models

type AuditLog struct {
	ID             string
	UserID         string
	Token          string
	ModuleName     string
	UserInsert     string
	DateInsert     string
	UserUpdate     string
	DateUpdate     string
	Remarks        string
	UserIDName     string
	UserInsertName string
}
