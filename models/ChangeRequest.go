package models

type ChangeRequest struct {
	ID                 string
	MemberID           string
	Requester          string
	CurrentValue       string
	ChangeRequestValue string
	Type               int32
	RecordStatus       int32
	UserInsert         string
	DateInsert         string
	UserUpdate         string
	DateUpdate         string
}
