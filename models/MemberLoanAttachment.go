package models

type MemberLoanAttachment struct {
	ID            string
	LoanID        string
	MemberID      string
	MemberCode    string
	Description   string
	FileName      string
	UserInsert    string
	Type          int32
	FileBase64Str string
}
