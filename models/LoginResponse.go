package models

type Response struct {
	Code  int    `json:"code"`
	Error string `json:"error,omitempty"`
}
type LoginResponse struct {
	*Response
	AccessToken  string `json:"access_token,omitempty"`
	UserID       string `json:"userID"`
	Email        string `json:"email"`
	CaptchaToken string `json:"captchaToken"`
}
