package models

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"time"
)

// Config : Represent config.json
type Config struct {
	Database []struct {
		Type     string `json:"type"`
		Host     string `json:"host"`
		Instance string `json:"instance"`
		User     string `json:"user"`
		Password string `json:"password"`
		Port     string `json:"port"`
		DBName   string `json:"dbName"`
	} `json:"database"`
	ServerPort string
	Credential struct {
		JWTSecret    string
		JWTExpiresIn string
	}
	AppName        string
	LogFile        string `json:"logFile"`
	FilePath       string
	BackupPath     string `json:"backupPath"`
	EnableGraphiQL bool   `json:"enableGraphiQL"`
	SMSGateway     struct {
		UserKey string
		PassKey string
	}
	TimeoutSecond int    `json:"timeout_second"`
	EmailSMTP     string `json:"emailSMTP"`
	PortSMTP      int    `json:"portSMTP,string"`
	EmailUsername string `json:"emailUsername"`
	EmailPassword string `json:"emailPassword"`
	MailSenderFrom  string `json:"mailSenderFrom"`
	PPOBAuth      string `json:"ppobAuth"`
	PPOBURL       string `json:"ppobURL"`
}

var configuration Config

func LoadConfiguration() Config {
	var config Config
	configFile, err := os.Open("config.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

func BuildConnString() string {
	config := LoadConfiguration()
	query := url.Values{}
	query.Add("database", config.Database[0].DBName)
	query.Add("packet size", "4096")

	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(config.Database[0].User, config.Database[0].Password),
		Host:   fmt.Sprintf("%s:%s", config.Database[0].Host, config.Database[0].Port),
		// Path:  instance, // if connecting to an instance instead of a port
		RawQuery: query.Encode(),
	}
	return u.String()
	// return "server=" + config.Database[0].Host +
	// 	";database=" + config.Database[0].DBName +
	// 	";user id=" + config.Database[0].User +
	// 	";port=" + config.Database[0].Port +
	// 	";password=" + config.Database[0].Password
}

func BuildConnStringV2(order int) string {
	config := LoadConfiguration()
	query := url.Values{}
	query.Add("database", config.Database[order].DBName)
	query.Add("packet size", "4096")

	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(config.Database[order].User, config.Database[order].Password),
		Host:   fmt.Sprintf("%s:%s", config.Database[order].Host, config.Database[order].Port),
		// Path:  instance, // if connecting to an instance instead of a port
		RawQuery: query.Encode(),
	}
	return u.String()
	// return "server=" + config.Database[0].Host +
	// 	";database=" + config.Database[0].DBName +
	// 	";user id=" + config.Database[0].User +
	// 	";port=" + config.Database[0].Port +
	// 	";password=" + config.Database[0].Password
}

func BuildConnStringRedis() (string, string) {
	config := LoadConfiguration()
	return config.Database[1].Host + ":" + config.Database[1].Port,
		config.Database[1].Password
}
func GetServerPort() string {
	config := LoadConfiguration()
	return config.ServerPort
}

func GetJWTCredential() (string, string, time.Duration) {
	config := LoadConfiguration()
	dur := ParseDuration(config.Credential.JWTExpiresIn)
	return config.AppName, config.Credential.JWTSecret, dur
}

func ParseDuration(str string) time.Duration {
	durationRegex := regexp.MustCompile(`P(?P<years>\d+Y)?(?P<months>\d+M)?(?P<days>\d+D)?T?(?P<hours>\d+H)?(?P<minutes>\d+M)?(?P<seconds>\d+S)?`)
	matches := durationRegex.FindStringSubmatch(str)

	years := ParseInt64(matches[1])
	months := ParseInt64(matches[2])
	days := ParseInt64(matches[3])
	hours := ParseInt64(matches[4])
	minutes := ParseInt64(matches[5])
	seconds := ParseInt64(matches[6])

	hour := int64(time.Hour)
	minute := int64(time.Minute)
	second := int64(time.Second)
	return time.Duration(years*24*365*hour + months*30*24*hour + days*24*hour + hours*hour + minutes*minute + seconds*second)
}

func ParseInt64(value string) int64 {
	if len(value) == 0 {
		return 0
	}
	parsed, err := strconv.Atoi(value[:len(value)-1])
	if err != nil {
		return 0
	}
	return int64(parsed)
}
func IsGraphiQLEnabled() bool {
	config := LoadConfiguration()
	return config.EnableGraphiQL
}
func SMSGatewayCredential() (string, string) {
	config := LoadConfiguration()
	return config.SMSGateway.UserKey, config.SMSGateway.PassKey
}

func GetFilePath() string {
	config := LoadConfiguration()
	return config.FilePath
}

func Timeout() int {
	config := LoadConfiguration()
	return config.TimeoutSecond
}

func GetEmailSMTP() (string, string, string, int, string) {
	config := LoadConfiguration()
	return config.EmailSMTP, config.EmailUsername, config.EmailPassword, config.PortSMTP, config.MailSenderFrom
}
