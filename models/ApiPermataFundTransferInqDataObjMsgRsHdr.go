package models

type ApiPermataFundTransferInqDataObjMsgRsHdr struct {
	ResponseTimestamp string
	CustRefID         string
	StatusCode        string
	StatusDesc        string
}
