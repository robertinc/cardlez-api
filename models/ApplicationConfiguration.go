package models

type ApplicationConfiguration struct {
	ID          string
	ConfigKey   string
	ConfigValue string
}
