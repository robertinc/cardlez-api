package models

type MemberDetail struct {
	ID                        string
	MemberID                  string
	NamaLengkapTanpaSingkatan string
	StatusPendidikan          string
	NamaIbuKandung            string
	NamaKontakDarurat         string
	NomorKontakDarurat        string
	HubunganKontakDarurat     string
	Provinsi                  string
	Kabupaten                 string
	Kecamatan                 string
	Kelurahan                 string
	KodePos                   string
	Phone                     string
	ProvinsiTempatKerja       string
	KabupatenTempatKerja      string
	KecamatanTempatKerja      string
	KelurahanTempatKerja      string
	KodePosTempatKerja        string
	KodePekerjaan             string
	NamaTempatKerja           string
	KodeBidangUsaha           string
	PenghasilanKotorPerTahun  string
	KodeSumberPenghasilan     string
	MaritalStatus             string
	JumlahTanggungan          string
	NoIdentitasPasangan       string
	NamaPasangan              string
	TanggalLahirPasangan      string
	PisahHarta                string
	AreaID                    string
	AreaIDTempatKerja         string
	FileNameFotoKTP           string
	FileNameFotoKTP64         string
	FileNameFotoSelfie        string
	FileNameFotoSelfie64      string
	//
	FileVideo64 string
	//
	StatusPendidikanSTR     string
	ProvinsiSTR             string
	KabupatenSTR            string
	ProvinsiTempatKerjaSTR  string
	KabupatenTempatKerjaSTR string
	DokumenTabungan         string
	DokumenTabungan64       string
}
