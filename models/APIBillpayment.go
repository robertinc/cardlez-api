package models

type APIBillpayment struct {
	//OVO
	BillNumber          string
	TransactionNumber   string
	Saldo               float64
	Nominal             float64
	Note                string
	InqReferenceNumber  string
	UserReferenceNumber string
}
