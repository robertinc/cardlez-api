package models

type FinalJournalReport struct {
	CoaNo                 string
	CoaName               string
	NoRekening            string
	NamaRekening          string
	DebitMovement         float64
	CreditMovement        float64
	SelisihDebitMovement  float64
	SelisihCreditMovement float64
}
