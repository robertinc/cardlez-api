package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/product"
	"gitlab.com/robertinc/cardlez-api/business-service/product/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type ProductUsecase struct {
	ProductRepository product.ProductRepository
	contextTimeout    time.Duration
}

func NewProductUsecase() product.ProductUsecase {
	return &ProductUsecase{
		ProductRepository: repository.NewSqlProductRepository(),
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func NewProductUsecaseV2(repo product.ProductRepository) product.ProductUsecase {
	return &ProductUsecase{
		ProductRepository: repo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *ProductUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.Product) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listProduct := a.ProductRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listProduct
}
func (a *ProductUsecase) FetchBase(c context.Context, search string, keyword string) (error, []*models.Product) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listProduct := a.ProductRepository.FetchBase(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listProduct
}

func (a *ProductUsecase) Update(c context.Context, b *models.Product) (*models.Product, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	product, err := a.ProductRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return product, nil
}
func (a *ProductUsecase) Store(c context.Context, b *models.Product) (*models.Product, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	product, err := a.ProductRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return product, nil
}
func (a *ProductUsecase) GetRetailProductByID(c context.Context, id string) (*models.Product, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	product, err := a.ProductRepository.GetRetailProductByID(ctx, id)

	if err != nil {
		return nil, err
	}
	return product, nil
}
