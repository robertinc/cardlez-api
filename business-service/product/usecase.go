package product

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ProductUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Account, string, error)
	//GetByID(ctx context.Context, id *string) *models.Account
	Update(ctx context.Context, ar *models.Product) (*models.Product, error)
	// GetByTitle(ctx context.Context, title string) (*models.Account, error)
	Store(context.Context, *models.Product) (*models.Product, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Product)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Product)
	GetRetailProductByID(ctx context.Context, id string) (*models.Product, error)
}
