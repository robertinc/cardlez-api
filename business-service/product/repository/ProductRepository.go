package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/product"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlProductRepository struct {
	Conn *sql.DB
}

func NewSqlProductRepository() product.ProductRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlProductRepository{conn}
}

func NewSqlProductRepositoryV2(Conn *sql.DB) product.ProductRepository {
	conn := Conn
	return &sqlProductRepository{conn}
}
func (sj *sqlProductRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.Product) {
	query := `
	SELECT  
		Cast(a.ID as varchar(36)) as ID,
		a.Code,
		ProductName, 
		a.RecordStatus,
		Cast(a.CompanyID as varchar(36)) as CompanyID,
		Cast(a.AccGroupConditionID as varchar(36)) as AccGroupConditionID,
		PrefixCode,
		a.ProductType
	FROM dbo.RetailProduct a
		`
	if search != "ALL" && search != "" {
		search = "a." + search
		query += ` where ` + search + ` like @p0`
	}

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Product, 0)
	for rows.Next() {
		j := new(models.Product)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.ProductName,
			&j.RecordStatus,
			&j.CompanyID,
			&j.AccGroupConditionID,
			&j.PrefixCode,
			&j.ProductType,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlProductRepository) FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Product) {
	query := `
	SELECT  
		Cast(a.ID as varchar(36)) as ID,
		a.Code,
		ProductName, 
		a.RecordStatus,
		Cast(a.CompanyID as varchar(36)) as CompanyID,
		Cast(a.AccGroupConditionID as varchar(36)) as AccGroupConditionID,
		PrefixCode,
		a.ProductType,
		ISNULL(a.MinDepositAmount,0) AS MinDepositAmount
	FROM dbo.RetailProduct a
		`
	if search != "ALL" && search != "" {
		search = "a." + search
		query += ` where ` + search + ` like @p0`
	}

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Product, 0)
	for rows.Next() {
		j := new(models.Product)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.ProductName,
			&j.RecordStatus,
			&j.CompanyID,
			&j.AccGroupConditionID,
			&j.PrefixCode,
			&j.ProductType,
			&j.MinDepositAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlProductRepository) Update(ctx context.Context, a *models.Product) (*models.Product, error) {

	updateQuery := `
		Update RetailProduct
		Set Code = @p1, ProductName = @p2
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.ProductName),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlProductRepository) Store(ctx context.Context, a *models.Product) (*models.Product, error) {
	a.ID = guid.New().StringUpper()
	productQuery := `
	INSERT INTO RetailProduct (
		id, 
		code, 
		productName, 
		InterestCalcType, 
		InterestPayDate, 
		AccGroupConditionID,
		CompanyID
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		0, 
		0, 
		'9026C4E2-3FCB-4E2F-8EF3-6FFF41C0BE45', 
		'15C55F75-85AD-4431-B1A6-DFBC7516925F' 
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", productQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, productQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.ProductName),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlProductRepository) GetRetailProductByCode(ctx context.Context, code string) (*models.Product, error) {
	var Product models.Product
	query := `
	select 
		cast(ID as varchar(36)) as ID,
		Code,
		cast(AccGroupConditionID as varchar(36)) as AccGroupConditionID,
		cast(CompanyID as varchar(36)) as CompanyID,
		ProductName,
		RecordStatus,
		ProductType
	from 
		dbo.RetailProduct 
	where 
		Code = @p0`
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&Product.ID,
		&Product.Code,
		&Product.AccGroupConditionID,
		&Product.CompanyID,
		&Product.ProductName,
		&Product.RecordStatus,
		&Product.ProductType,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", Product)),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Product)),
	)
	return &Product, nil
}
func (sj *sqlProductRepository) GetByProductType(ctx context.Context, productType int32) (*models.Product, error) {
	var Product models.Product

	logger := service.Logger(ctx)
	query := `
		select 
			cast(ID as varchar(36)) as ID,
			Code,
			cast(AccGroupConditionID as varchar(36)) as AccGroupConditionID,
			cast(CompanyID as varchar(36)) as CompanyID,
			ProductName,
			RecordStatus,
			ProductType
        from 
			dbo.RetailProduct 
		where 
		ProductType = @p0`
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("productType", fmt.Sprintf("%v", productType)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", productType)).Scan(
		&Product.ID,
		&Product.Code,
		&Product.AccGroupConditionID,
		&Product.CompanyID,
		&Product.ProductName,
		&Product.RecordStatus,
		&Product.ProductType,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Product)),
	)
	return &Product, nil
}
func (sj *sqlProductRepository) GetRetailProductByID(ctx context.Context, id string) (*models.Product, error) {
	var Product models.Product
	query := `
	select 
		cast(ID as varchar(36)) as ID,
		Code,
		cast(AccGroupConditionID as varchar(36)) as AccGroupConditionID,
		cast(CompanyID as varchar(36)) as CompanyID,
		ProductName,
		RecordStatus,
		ProductType
	from 
		dbo.RetailProduct 
	where 
	ID = @p0`
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&Product.ID,
		&Product.Code,
		&Product.AccGroupConditionID,
		&Product.CompanyID,
		&Product.ProductName,
		&Product.RecordStatus,
		&Product.ProductType,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", Product)),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Product)),
	)
	return &Product, nil
}
