package product

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ProductRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Product)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Product)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	// GetByID(ctx context.Context, id *string) *models.Journal
	Update(ctx context.Context, account *models.Product) (*models.Product, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Product) (*models.Product, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	GetRetailProductByCode(ctx context.Context, code string) (*models.Product, error)
	GetByProductType(ctx context.Context, productType int32) (*models.Product, error)
	GetRetailProductByID(ctx context.Context, id string) (*models.Product, error)
}
