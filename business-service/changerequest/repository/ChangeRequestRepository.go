package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/changerequest"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlChangeRequestRepository struct {
	Conn *sql.DB
}

func NewSqlChangeRequestRepository() changerequest.ChangeRequestRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlChangeRequestRepository{conn}
}

func NewSqlChangeRequestRepositoryV2(Conn *sql.DB) changerequest.ChangeRequestRepository {
	conn := Conn
	return &sqlChangeRequestRepository{conn}
}

func (sj *sqlChangeRequestRepository) Fetch(ctx context.Context) ([]*models.ChangeRequest, error) {
	query := `
	SELECT 
		IsNull(Cast(ID as varchar(36)),'') as ID,
		IsNull(Cast(MemberID as varchar(36)),'') as MemberID,
		IsNull(Requester, '') as Requester,
		IsNull(CurrentValue, '') as CurrentValue,
		IsNull(ChangeRequestValue, '') as ChangeRequestValue,
		IsNull(Type, 0) as Type,
		IsNull(RecordStatus, 0) as RecordStatus,
		IsNull(UserInsert,'') as UserInsert,
		IsNull(Cast(DateInsert as varchar(50)),'') as DateInsert,
		IsNull(UserUpdate,'') as UserUpdate,
		IsNull(Cast(DateUpdate as varchar(50)),'') as DateUpdate
	FROM ChangeRequest
	order by DateInsert desc `
	// if search != "ALL" && search != "" {
	// 	query += ` where ` + search + ` like @p0`
	// }
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		//zap.String("keyword", keyword),
	)
	// rows, err := sj.Conn.Query(query,
	// 	sql.Named("p0", "%"+keyword+"%"))
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.ChangeRequest, 0)
	for rows.Next() {
		j := new(models.ChangeRequest)
		err = rows.Scan(
			&j.ID,
			&j.MemberID,
			&j.Requester,
			&j.CurrentValue,
			&j.ChangeRequestValue,
			&j.Type,
			&j.RecordStatus,
			&j.UserInsert,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlChangeRequestRepository) Store(ctx context.Context, b *models.ChangeRequest) (*models.ChangeRequest, error) {

	if b.ID == "" {
		b.ID = guid.New().StringUpper()
	}
	b.UserInsert = ""
	if ctx.Value(service.CtxUserID) != nil {
		b.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	Query := `
		Insert Into ChangeRequest(
			ID
			,MemberID
			,ChangeRequestValue
			,Type
			,RecordStatus
			,UserInsert
			,DateInsert
			,UserUpdate
			,DateUpdate
			,Requester
			,CurrentValue
		)
		Values(
			@p0,
			@p1,
			@p2,
			@p3,
			@p4,
			@p5,
			getdate(), 
			null, 
			null,
			@p6,
			@p7
		)
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", b.ID),
		sql.Named("p1", b.MemberID),
		sql.Named("p2", b.ChangeRequestValue),
		sql.Named("p3", b.Type),
		sql.Named("p4", b.RecordStatus),
		sql.Named("p5", b.UserInsert),
		sql.Named("p6", b.Requester),
		sql.Named("p7", b.CurrentValue),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
func (sj *sqlChangeRequestRepository) StoreByDate(ctx context.Context, b *models.ChangeRequest, companyDate string) (*models.ChangeRequest, error) {

	if b.ID == "" {
		b.ID = guid.New().StringUpper()
	}
	b.UserInsert = ""
	if ctx.Value(service.CtxUserID) != nil {
		b.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	Query := `
		Insert Into ChangeRequest(
			ID
			,MemberID
			,ChangeRequestValue
			,Type
			,RecordStatus
			,UserInsert
			,DateInsert
			,UserUpdate
			,DateUpdate
			,Requester
			,CurrentValue
		)
		Values(
			@p0,
			@p1,
			@p2,
			@p3,
			@p4,
			@p5,
			@date, 
			null, 
			null,
			@p6,
			@p7
		)
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", b.ID),
		sql.Named("p1", b.MemberID),
		sql.Named("p2", b.ChangeRequestValue),
		sql.Named("p3", b.Type),
		sql.Named("p4", b.RecordStatus),
		sql.Named("p5", b.UserInsert),
		sql.Named("p6", b.Requester),
		sql.Named("p7", b.CurrentValue),
		sql.Named("date", companyDate),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}

func (sj *sqlChangeRequestRepository) GetByID(ctx context.Context, ID string) (*models.ChangeRequest, error) {

	var j models.ChangeRequest
	logger := service.Logger(ctx)
	logger.Info("Query ChangeRequest",
		zap.String("id", ID),
	)
	err := sj.Conn.QueryRow(`
		SELECT 
			IsNull(Cast(ID as varchar(36)),'') as ID,
			IsNull(Cast(MemberID as varchar(36)),'') as MemberID,
			IsNull(Requester, '') as Requester,
			IsNull(CurrentValue, '') as CurrentValue,
			IsNull(ChangeRequestValue, '') as ChangeRequestValue,
			IsNull(Type, 0) as Type,
			IsNull(RecordStatus, 0) as RecordStatus,
			IsNull(UserInsert,'') as UserInsert,
			IsNull(Cast(DateInsert as varchar(50)),'') as DateInsert,
			IsNull(UserUpdate,'') as UserUpdate,
			IsNull(Cast(DateUpdate as varchar(50)),'') as DateUpdate
		FROM ChangeRequest
		where 
			ID = @p0`,
		sql.Named("p0", ID)).Scan(
		&j.ID,
		&j.MemberID,
		&j.Requester,
		&j.CurrentValue,
		&j.ChangeRequestValue,
		&j.Type,
		&j.RecordStatus,
		&j.UserInsert,
		&j.DateInsert,
		&j.UserUpdate,
		&j.DateUpdate,
	)

	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}

func (sj *sqlChangeRequestRepository) ApproveRequest(ctx context.Context, ID string) (bool, error) {
	UserUpdate := ""
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	bankQuery := `Update ChangeRequest Set RecordStatus = 2, DateUpdate = getdate(), UserUpdate = @p1 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", ID),
		sql.Named("p1", UserUpdate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", ID)),
	)
	return true, nil
}
func (sj *sqlChangeRequestRepository) ApproveRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error) {
	UserUpdate := ""
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	bankQuery := `Update ChangeRequest Set RecordStatus = 2, DateUpdate = @date, UserUpdate = @p1 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", ID),
		sql.Named("p1", UserUpdate),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", ID)),
	)
	return true, nil
}

func (sj *sqlChangeRequestRepository) DeclineRequest(ctx context.Context, ID string) (bool, error) {
	UserUpdate := ""
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	bankQuery := `Update ChangeRequest Set RecordStatus = 3, DateUpdate = getdate(), UserUpdate = @p1 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", ID),
		sql.Named("p1", UserUpdate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", ID)),
	)
	return true, nil
}
func (sj *sqlChangeRequestRepository) DeclineRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error) {
	UserUpdate := ""
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	bankQuery := `Update ChangeRequest Set RecordStatus = 3, DateUpdate = @date, UserUpdate = @p1 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", ID),
		sql.Named("p1", UserUpdate),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", ID)),
	)
	return true, nil
}

func (sj *sqlChangeRequestRepository) CheckAvailableChangeRequest(ctx context.Context, MemberID string, Type int32) (bool, error) {
	availableRequest := false
	query := `
	SELECT 
		IsNull(Cast(ID as varchar(36)),'') as ID,
		IsNull(Cast(MemberID as varchar(36)),'') as MemberID,
		IsNull(Requester, '') as Requester,
		IsNull(CurrentValue, '') as CurrentValue,
		IsNull(ChangeRequestValue, '') as ChangeRequestValue,
		IsNull(Type, 0) as Type,
		IsNull(RecordStatus, 0) as RecordStatus,
		IsNull(UserInsert,'') as UserInsert,
		IsNull(Cast(DateInsert as varchar(50)),'') as DateInsert,
		IsNull(UserUpdate,'') as UserUpdate,
		IsNull(Cast(DateUpdate as varchar(50)),'') as DateUpdate
	FROM ChangeRequest
	where MemberID = @p0 and Type = @p1 and RecordStatus = 1
	order by DateInsert desc `
	// if search != "ALL" && search != "" {
	// 	query += ` where ` + search + ` like @p0`
	// }
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		//zap.String("keyword", keyword),
	)
	// rows, err := sj.Conn.Query(query,
	// 	sql.Named("p0", "%"+keyword+"%"))
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", MemberID),
		sql.Named("p1", Type),
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return availableRequest, err
	}
	defer rows.Close()

	result := make([]*models.ChangeRequest, 0)
	for rows.Next() {
		j := new(models.ChangeRequest)
		err = rows.Scan(
			&j.ID,
			&j.MemberID,
			&j.Requester,
			&j.CurrentValue,
			&j.ChangeRequestValue,
			&j.Type,
			&j.RecordStatus,
			&j.UserInsert,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return availableRequest, err
		}
		result = append(result, j)
	}
	logger.Info("Response",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	if len(result) == 0 {
		availableRequest = true
	}
	return availableRequest, nil
}
