package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/changerequest"
	"gitlab.com/robertinc/cardlez-api/business-service/changerequest/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type ChangeRequestUsecase struct {
	ChangeRequestRepository changerequest.ChangeRequestRepository
	contextTimeout          time.Duration
}

func NewChangeRequestUsecase() changerequest.ChangeRequestUsecase {
	return &ChangeRequestUsecase{
		ChangeRequestRepository: repository.NewSqlChangeRequestRepository(),
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func NewChangeRequestUsecaseV2(changerequestRepo changerequest.ChangeRequestRepository) changerequest.ChangeRequestUsecase {
	return &ChangeRequestUsecase{
		ChangeRequestRepository: changerequestRepo,
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func (a *ChangeRequestUsecase) Fetch(c context.Context) ([]*models.ChangeRequest, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listChangeRequest, err := a.ChangeRequestRepository.Fetch(ctx)
	if err != nil {
		return nil, err
	}
	return listChangeRequest, nil
}

func (a *ChangeRequestUsecase) Store(ctx context.Context, b *models.ChangeRequest) (*models.ChangeRequest, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	ChangeRequest, err := a.ChangeRequestRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}
	return ChangeRequest, nil
}
func (a *ChangeRequestUsecase) StoreByDate(ctx context.Context, b *models.ChangeRequest, companyDate string) (*models.ChangeRequest, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	ChangeRequest, err := a.ChangeRequestRepository.StoreByDate(ctx, b, companyDate)
	if err != nil {
		return nil, err
	}
	return ChangeRequest, nil
}

func (a *ChangeRequestUsecase) GetByID(ctx context.Context, ID string) (*models.ChangeRequest, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	ChangeRequest, err := a.ChangeRequestRepository.GetByID(ctx, ID)
	if err != nil {
		return nil, err
	}
	return ChangeRequest, nil
}

func (a *ChangeRequestUsecase) ApproveRequest(ctx context.Context, ID string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	approve, err := a.ChangeRequestRepository.ApproveRequest(ctx, ID)
	if err != nil {
		return false, err
	}
	return approve, nil
}
func (a *ChangeRequestUsecase) ApproveRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	approve, err := a.ChangeRequestRepository.ApproveRequestByDate(ctx, ID, companyDate)
	if err != nil {
		return false, err
	}
	return approve, nil
}

func (a *ChangeRequestUsecase) DeclineRequest(ctx context.Context, ID string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	decline, err := a.ChangeRequestRepository.DeclineRequest(ctx, ID)
	if err != nil {
		return false, err
	}
	return decline, nil
}
func (a *ChangeRequestUsecase) DeclineRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	decline, err := a.ChangeRequestRepository.DeclineRequestByDate(ctx, ID, companyDate)
	if err != nil {
		return false, err
	}
	return decline, nil
}

func (a *ChangeRequestUsecase) CheckAvailableChangeRequest(ctx context.Context, MemberID string, Type int32) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	available, err := a.ChangeRequestRepository.CheckAvailableChangeRequest(ctx, MemberID, Type)
	if err != nil {
		return false, err
	}
	return available, nil
}
