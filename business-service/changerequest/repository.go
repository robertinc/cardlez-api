package changerequest

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ChangeRequestRepository interface {
	Fetch(ctx context.Context) ([]*models.ChangeRequest, error)
	Store(ctx context.Context, b *models.ChangeRequest) (*models.ChangeRequest, error)
	StoreByDate(ctx context.Context, b *models.ChangeRequest, companyDate string) (*models.ChangeRequest, error)
	GetByID(ctx context.Context, ID string) (*models.ChangeRequest, error)
	ApproveRequest(ctx context.Context, ID string) (bool, error)
	ApproveRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error)
	DeclineRequest(ctx context.Context, ID string) (bool, error)
	DeclineRequestByDate(ctx context.Context, ID string, companyDate string) (bool, error)
	CheckAvailableChangeRequest(ctx context.Context, MemberID string, Type int32) (bool, error)
}
