package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/promo"
	"gitlab.com/robertinc/cardlez-api/business-service/promo/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoUsecase struct {
	PromoRepository promo.PromoRepository
	contextTimeout  time.Duration
}

func NewPromoUsecase() promo.PromoUsecase {
	return &PromoUsecase{
		PromoRepository: repository.NewSqlPromoRepository(),
		contextTimeout:  time.Second * time.Duration(models.Timeout()),
	}
}
func NewPromoUsecaseV2(repo promo.PromoRepository) promo.PromoUsecase {
	return &PromoUsecase{
		PromoRepository: repo,
		contextTimeout:  time.Second * time.Duration(models.Timeout()),
	}
}

func (a *PromoUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.Promo) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listPromo := a.PromoRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listPromo
}

func (a *PromoUsecase) Update(c context.Context, b *models.Promo) (*models.Promo, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	promo, err := a.PromoRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return promo, nil
}
func (a *PromoUsecase) Store(c context.Context, b *models.Promo) (*models.Promo, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	promo, err := a.PromoRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return promo, nil
}
