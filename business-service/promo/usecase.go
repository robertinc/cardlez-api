package promo

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Account, string, error)
	//GetByID(ctx context.Context, id *string) *models.Account
	Update(ctx context.Context, ar *models.Promo) (*models.Promo, error)
	// GetByTitle(ctx context.Context, title string) (*models.Account, error)
	Store(context.Context, *models.Promo) (*models.Promo, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Promo)
}
