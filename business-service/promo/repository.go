package promo

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Promo)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	// GetByID(ctx context.Context, id *string) *models.Journal
	Update(ctx context.Context, account *models.Promo) (*models.Promo, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Promo) (*models.Promo, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
