package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/promo"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlPromoRepository struct {
	Conn *sql.DB
}

func NewSqlPromoRepository() promo.PromoRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlPromoRepository{conn}
}

func NewSqlPromoRepositoryV2(Conn *sql.DB) promo.PromoRepository {
	conn := Conn
	return &sqlPromoRepository{conn}
}
func (sj *sqlPromoRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.Promo) {
	query := `
	SELECT  
		Cast(a.ID as varchar(36)) as ID,
		a.Image,
		a.Description
	FROM dbo.Promo a
		`
	if search != "ALL" && search != "" {
		if search == "NOTEXPIRED" {
			query += ` where ENDDATE < GETDATE() `
		} else {
			search = "a." + search
			query += ` where ` + search + ` like @p0`
		}
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.Promo, 0)
	for rows.Next() {
		j := new(models.Promo)
		err = rows.Scan(
			&j.ID,
			&j.Image,
			&j.Description,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		logger.Info("Response result",
			zap.String("res", fmt.Sprintf("%v", result)),
		)
	}
	return nil, result
}

func (sj *sqlPromoRepository) Update(ctx context.Context, a *models.Promo) (*models.Promo, error) {

	updateQuery := `
		Update Promo
		Set Image = @p1, Description = @p2
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Image),
		sql.Named("p2", a.Description),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlPromoRepository) Store(ctx context.Context, a *models.Promo) (*models.Promo, error) {
	a.ID = guid.New().StringUpper()
	promoQuery := `
	INSERT INTO Promo (
		id, 
		image, 
		description
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Image),
		sql.Named("p2", a.Description),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
