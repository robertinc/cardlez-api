package datacapture

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type DataCaptureRepository interface {
	Fetch(ctx context.Context, search string, keyword string) ([]*models.DataCapture, error)
	FetchDataCaptureAmount(ctx context.Context, dataCaptureID string) ([]*models.DataCaptureAmount, error)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.DataCapture, error)
	//GetByID(ctx context.Context, id *string) *models.DataCapture
	Update(ctx context.Context, DataCapture *models.DataCapture) (*models.DataCapture, error)
	// GetByTitle(ctx context.Context, title string) (*models.DataCapture, error)
	Store(ctx context.Context, a *models.DataCapture) (*models.DataCapture, error)
	UpdateBase(ctx context.Context, ar *models.DataCapture) (*models.DataCapture, error)
	StoreBase(context.Context, *models.DataCapture) (*models.DataCapture, error)
	GetCOAFromAccount(ctx context.Context, accountID string) (*models.COA, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
