package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/beevik/guid"
	datacapture "gitlab.com/robertinc/cardlez-api/business-service/data-capture"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlDataCaptureRepository struct {
	Conn *sql.DB
}

func NewSqlDataCaptureRepository() datacapture.DataCaptureRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlDataCaptureRepository{conn}
}

func NewSqlDataCaptureRepositoryV2(Conn *sql.DB) datacapture.DataCaptureRepository {
	conn := Conn
	return &sqlDataCaptureRepository{conn}
}
func (sj *sqlDataCaptureRepository) Fetch(ctx context.Context, search string, keyword string) ([]*models.DataCapture, error) {
	logger := service.Logger(ctx)
	query := `
	SELECT 
		CAST(ID as varchar(36)) as ID,
		Isnull(Code, '') as Code,
		Isnull(Note, '') as Note,
		Isnull(TransactionDate, '')
	FROM DataCapture`
	result := make([]*models.DataCapture, 0)
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	}
	logger.Info("Query Account",
		zap.String("query", query),
		zap.String("keyword", keyword),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.DataCapture)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.TransactionDate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlDataCaptureRepository) FetchDataCaptureAmount(ctx context.Context, dataCaptureID string) ([]*models.DataCaptureAmount, error) {
	query := `
	SELECT 
		CAST(dca.ID as varchar(36)) as ID,
		dca.IsTabungan,
		IsNull(dca.DCAmount, 0),
		Case When a.ID is null then 
			case when c.ID is null then 'N/A' else
			c.Code+ ' - ' + c.Description end
		Else a.Code+ ' - ' + a.AccountName End As Product,
		dca.TransType,
		CAST(dca.TrHdAccountID as varchar(36)) as TrHdAccountID
	FROM 
		dbo.DataCaptureAmount dca
		LEFT JOIN Account a on dca.TrHdAccountID = a.ID
		LEFT JOIN COA c on dca.TrHdAccountID = c.ID
	WHERE dca.DataCaptureID = @p0`
	result := make([]*models.DataCaptureAmount, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("dataCaptureID", fmt.Sprintf("%v", dataCaptureID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", dataCaptureID))

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.DataCaptureAmount)
		err = rows.Scan(
			&j.ID,
			&j.IsSaving,
			&j.DCAmount,
			&j.ProductName,
			&j.TransactionType,
			&j.COAID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlDataCaptureRepository) Store(ctx context.Context, a *models.DataCapture) (*models.DataCapture, error) {
	a.ID = guid.New().StringUpper()
	dcQuery := `
		declare @@companyID uniqueidentifier = (select CompanyID from CompanyUser where ID = @p3)
		INSERT INTO DataCapture (
			ID, 
			Code, 
			CompanyID, 
			CompanyUserID, 
			TransactionDate, 
			Note,
			RevNo,
			RecordStatus,
			DateInsert
			) 
		VALUES (@p0, @code, @@companyID, @p3, @p2, @p1, 1, 2, GETDATE())`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", dcQuery)),
		zap.String("description", fmt.Sprintf("%v", a.Description)),
		zap.String("transactionDate", fmt.Sprintf("%v", a.TransactionDate)),
		zap.String("companyUserID", fmt.Sprintf("%v", a.CompanyUserID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
	)
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Description),
		sql.Named("p2", a.TransactionDate),
		sql.Named("p3", a.CompanyUserID),
		sql.Named("code", a.Code),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		trx.Rollback()
		return nil, err
	}
	// Insert DC Amount
	for _, dcAmount := range a.DataCaptureAmount {
		var trHDAcc string
		if dcAmount.IsSaving == true {
			trHDAcc = dcAmount.ReferenceNumber
		} else {
			trHDAcc = dcAmount.COAID
		}
		dcAmount.ID = guid.New().StringUpper()
		dcaQuery := `
			INSERT INTO DataCaptureAmount (
				ID, 
				IsTabungan,
				DataCaptureID,
				TrHDAccountID,
				TransType,
				DCAmount,
				DateInsert
			) VALUES (
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				GETDATE()
			)
		`
		logger.Info("Query",
			zap.String("query", fmt.Sprintf("%v", dcaQuery)),
			zap.String("id", fmt.Sprintf("%v", dcAmount.ID)),
			zap.String("isSaving", fmt.Sprintf("%v", dcAmount.IsSaving)),
			zap.String("dataCaptureID", fmt.Sprintf("%v", a.ID)),
			zap.String("trHDAcc", fmt.Sprintf("%v", trHDAcc)),
			zap.String("transactionType", fmt.Sprintf("%v", dcAmount.TransactionType)),
			zap.String("dcAmount", fmt.Sprintf("%v", dcAmount.DCAmount)),
		)
		_, err = sj.Conn.ExecContext(ctx, dcaQuery,
			sql.Named("p0", dcAmount.ID),
			sql.Named("p1", dcAmount.IsSaving),
			sql.Named("p2", a.ID),
			sql.Named("p3", trHDAcc),
			sql.Named("p4", dcAmount.TransactionType),
			sql.Named("p5", dcAmount.DCAmount),
		)

		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}
	}
	trx.Commit()
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlDataCaptureRepository) StoreBase(ctx context.Context, a *models.DataCapture) (*models.DataCapture, error) {
	a.ID = guid.New().StringUpper()
	layout := "2006-01-02 15:04:05"
	t := time.Now()
	compDateConvert, err := time.Parse(layout, a.TransactionDate)
	if err != nil {
		return nil, err
	}
	companyDate := time.Date(compDateConvert.Year(), compDateConvert.Month(), compDateConvert.Day(), t.Hour(), t.Minute(), t.Second(), 0, compDateConvert.Location()).Format(layout)

	dcQuery := `
		declare @@companyID uniqueidentifier = (select CompanyID from CompanyUser where ID = @p3)
		INSERT INTO DataCapture (
			ID, 
			Code, 
			CompanyID, 
			CompanyUserID, 
			TransactionDate, 
			Note,
			RevNo,
			RecordStatus,
			DateInsert
			) 
		VALUES (@p0, @code, @@companyID, @p3, @p2, @p1, 1, 2, @p4)`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", dcQuery)),
		zap.String("description", fmt.Sprintf("%v", a.Description)),
		zap.String("transactionDate", fmt.Sprintf("%v", a.TransactionDate)),
		zap.String("companyUserID", fmt.Sprintf("%v", a.CompanyUserID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
	)
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Description),
		sql.Named("p2", a.TransactionDate),
		sql.Named("p3", a.CompanyUserID),
		sql.Named("code", a.Code),
		sql.Named("p4", companyDate),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		trx.Rollback()
		return nil, err
	}
	// Insert DC Amount
	for _, dcAmount := range a.DataCaptureAmount {
		var trHDAcc string
		if dcAmount.IsSaving == true {
			trHDAcc = dcAmount.ReferenceNumber
		} else {
			trHDAcc = dcAmount.COAID
		}
		dcAmount.ID = guid.New().StringUpper()
		dcaQuery := `
			INSERT INTO DataCaptureAmount (
				ID, 
				IsTabungan,
				DataCaptureID,
				TrHDAccountID,
				TransType,
				DCAmount,
				DateInsert
			) VALUES (
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				@p6
			)
		`
		logger.Info("Query",
			zap.String("query", fmt.Sprintf("%v", dcaQuery)),
			zap.String("id", fmt.Sprintf("%v", dcAmount.ID)),
			zap.String("isSaving", fmt.Sprintf("%v", dcAmount.IsSaving)),
			zap.String("dataCaptureID", fmt.Sprintf("%v", a.ID)),
			zap.String("trHDAcc", fmt.Sprintf("%v", trHDAcc)),
			zap.String("transactionType", fmt.Sprintf("%v", dcAmount.TransactionType)),
			zap.String("dcAmount", fmt.Sprintf("%v", dcAmount.DCAmount)),
		)
		_, err = sj.Conn.ExecContext(ctx, dcaQuery,
			sql.Named("p0", dcAmount.ID),
			sql.Named("p1", dcAmount.IsSaving),
			sql.Named("p2", a.ID),
			sql.Named("p3", trHDAcc),
			sql.Named("p4", dcAmount.TransactionType),
			sql.Named("p5", dcAmount.DCAmount),
			sql.Named("p6", companyDate),
		)

		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}
	}
	trx.Commit()
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlDataCaptureRepository) GetCOAFromAccount(ctx context.Context, accountID string) (*models.COA, error) {
	var COA models.COA
	query := `
		select 
			cast(c.ID as varchar(36)) as ID, c.Code, c.Description
		from 
			COA c
			Inner Join RetailProduct rp on rp.ID = c.ProductID
			Inner Join Account a on a.RetailProductID = rp.ID
		where a.ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", accountID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", accountID)).Scan(
		&COA.ID,
		&COA.Code,
		&COA.Description,
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", COA)),
	)
	return &COA, err
}
func (sj *sqlDataCaptureRepository) Update(ctx context.Context, a *models.DataCapture) (*models.DataCapture, error) {
	var param []string
	dcQuery := `Update DataCapture Set `
	if a.Description != "" {
		param = append(param, "Note = @p1")
	}
	if a.TransactionDate != "" {
		param = append(param, "TransactionDate = @p2")
	}
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	dcQuery += strings.Join(param, ",")
	dcQuery += ` Where ID = @p0`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Description),
		sql.Named("p2", a.TransactionDate),
	)

	if err != nil {
		trx.Rollback()
		return nil, err
	}
	// Insert DC Amount
	for _, dcAmount := range a.DataCaptureAmount {
		dcaQuery := `
			INSERT INTO DataCaptureAmount (
				ID, 
				IsTabungan,
				DataCaptureID,
				TrHDAccountID,
				TransType,
				DCAmount,
				DateInsert
			) VALUES (
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				GETDATE()
			)
		`
		_, err = sj.Conn.ExecContext(ctx, dcaQuery,
			sql.Named("p0", dcAmount.ID),
			sql.Named("p1", dcAmount.IsSaving),
			sql.Named("p2", a.ID),
			sql.Named("p3", dcAmount.COAID),
			sql.Named("p4", dcAmount.TransactionType),
			sql.Named("p5", dcAmount.DCAmount),
		)

		if err != nil {
			trx.Rollback()
			return nil, err
		}
	}
	trx.Commit()
	return a, nil
}
func (sj *sqlDataCaptureRepository) UpdateBase(ctx context.Context, a *models.DataCapture) (*models.DataCapture, error) {
	var param []string
	layout := "2006-01-02 15:04:05"
	t := time.Now()
	compDateConvert, err := time.Parse(layout, a.TransactionDate)
	if err != nil {
		return nil, err
	}
	companyDate := time.Date(compDateConvert.Year(), compDateConvert.Month(), compDateConvert.Day(), t.Hour(), t.Minute(), t.Second(), 0, compDateConvert.Location()).Format(layout)

	dcQuery := `Update DataCapture Set `
	if a.Description != "" {
		param = append(param, "Note = @p1")
	}
	if a.TransactionDate != "" {
		param = append(param, "TransactionDate = @p2")
	}
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	dcQuery += strings.Join(param, ",")
	dcQuery += ` Where ID = @p0`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Description),
		sql.Named("p2", companyDate),
	)

	if err != nil {
		trx.Rollback()
		return nil, err
	}
	// Insert DC Amount
	for _, dcAmount := range a.DataCaptureAmount {
		dcaQuery := `
			INSERT INTO DataCaptureAmount (
				ID, 
				IsTabungan,
				DataCaptureID,
				TrHDAccountID,
				TransType,
				DCAmount,
				DateInsert
			) VALUES (
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				@p6
			)
		`
		_, err = sj.Conn.ExecContext(ctx, dcaQuery,
			sql.Named("p0", dcAmount.ID),
			sql.Named("p1", dcAmount.IsSaving),
			sql.Named("p2", a.ID),
			sql.Named("p3", dcAmount.COAID),
			sql.Named("p4", dcAmount.TransactionType),
			sql.Named("p5", companyDate),
		)

		if err != nil {
			trx.Rollback()
			return nil, err
		}
	}
	trx.Commit()
	return a, nil
}
