package usecase

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/beevik/guid"

	companyuserrepo "gitlab.com/robertinc/cardlez-api/business-service/company-user/repository"
	datacapture "gitlab.com/robertinc/cardlez-api/business-service/data-capture"
	"gitlab.com/robertinc/cardlez-api/business-service/data-capture/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	memberrepo "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"

	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
)

type DataCaptureUsecase struct {
	DataCaptureRepository datacapture.DataCaptureRepository
	contextTimeout        time.Duration
}

func NewDataCaptureUsecase() datacapture.DataCaptureUsecase {
	return &DataCaptureUsecase{
		DataCaptureRepository: repository.NewSqlDataCaptureRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}
func NewDataCaptureUsecaseV2(dcRepo datacapture.DataCaptureRepository) datacapture.DataCaptureUsecase {
	return &DataCaptureUsecase{
		DataCaptureRepository: dcRepo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func (a *DataCaptureUsecase) Fetch(c context.Context, search string, keyword string) ([]*models.DataCapture, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listDataCapture, err := a.DataCaptureRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return nil, err
	}
	return listDataCapture, nil
}

func (a *DataCaptureUsecase) FetchDataCaptureAmount(c context.Context, dataCaptureID string) ([]*models.DataCaptureAmount, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listDataCapture, err := a.DataCaptureRepository.FetchDataCaptureAmount(ctx, dataCaptureID)
	if err != nil {
		return nil, err
	}
	return listDataCapture, nil
}
func (a *DataCaptureUsecase) Store(c context.Context, b *models.DataCapture) (*models.DataCapture, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	if b.Code == "" {
		uc := memberrepo.NewSqlMemberRepository()
		err, code := uc.GenerateCode(ctx, "DCP")
		if err != nil {
			return nil, err
		}
		b.Code = code
	}

	//check user role
	if ctx.Value(service.CtxUserID) == nil {
		return nil, errors.New("User ID Insert not found.")
	}
	userID := ctx.Value(service.CtxUserID).(string)
	companyUser := companyuserrepo.NewSqlCompanyUserRepository().GetByID(ctx, &userID)
	if companyUser == nil {
		return nil, errors.New("Company User not found.")
	}

	for i, dca := range b.DataCaptureAmount {
		if dca.COAID == "" && dca.ReferenceNumber != "" {
			COA, err := a.DataCaptureRepository.GetCOAFromAccount(ctx, dca.ReferenceNumber)
			if err != nil {
				return nil, err
			}
			b.DataCaptureAmount[i].COAID = COA.ID
		}
		//validation 100.01.02.04 - Bank - Giro - Permata VA tidak boleh credit jika bukan role superadmin
		if dca.COAID == "17DEC61D-7E76-4D69-8A38-B9722B511159" && dca.TransactionType == "1" {
			if companyUser.CompanyUserRoleID != "E33BD2C9-E558-472A-B9AB-59F313653AD6" {
				return nil, errors.New("Mohon maaf. Role anda tidak diizinkan untuk melakukan transaksi ini. Mohon Diperbaiki.")
			}
		}
	}
	DC, err := a.DataCaptureRepository.Store(ctx, b)

	//Insert To Journal
	if len(b.DataCaptureAmount) > 0 {
		err = a.InsertJournal(ctx, b)
		if err != nil {
			return nil, err
		}
	}
	return DC, nil
}

func (a *DataCaptureUsecase) Update(c context.Context, b *models.DataCapture) (*models.DataCapture, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	datacapture, err := a.DataCaptureRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	//Insert To Journal
	if len(b.DataCaptureAmount) > 0 {
		err = a.InsertJournal(ctx, b)
		if err != nil {
			return nil, err
		}
	}
	return datacapture, nil
}
func (a *DataCaptureUsecase) StoreBase(c context.Context, b *models.DataCapture) (*models.DataCapture, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	if b.Code == "" {
		uc := memberrepo.NewSqlMemberRepository()
		err, code := uc.GenerateCode(ctx, "DCP")
		if err != nil {
			return nil, err
		}
		b.Code = code
	}

	//check user role
	if ctx.Value(service.CtxUserID) == nil {
		return nil, errors.New("User ID Insert not found.")
	}
	userID := ctx.Value(service.CtxUserID).(string)
	companyUser := companyuserrepo.NewSqlCompanyUserRepository().GetByID(ctx, &userID)
	if companyUser == nil {
		return nil, errors.New("Company User not found.")
	}

	for i, dca := range b.DataCaptureAmount {
		if dca.COAID == "" && dca.ReferenceNumber != "" {
			COA, err := a.DataCaptureRepository.GetCOAFromAccount(ctx, dca.ReferenceNumber)
			if err != nil {
				return nil, err
			}
			b.DataCaptureAmount[i].COAID = COA.ID
		}
		//validation 100.01.02.04 - Bank - Giro - Permata VA tidak boleh credit jika bukan role superadmin
		if dca.COAID == "17DEC61D-7E76-4D69-8A38-B9722B511159" && dca.TransactionType == "1" {
			if companyUser.CompanyUserRoleID != "E33BD2C9-E558-472A-B9AB-59F313653AD6" {
				return nil, errors.New("Mohon maaf. Role anda tidak diizinkan untuk melakukan transaksi ini. Mohon Diperbaiki.")
			}
		}
	}

	DC, err := a.DataCaptureRepository.StoreBase(ctx, b)

	//Insert To Journal
	if len(b.DataCaptureAmount) > 0 {
		err = a.InsertJournalBase(ctx, b)
		if err != nil {
			return nil, err
		}
	}
	return DC, nil
}

func (a *DataCaptureUsecase) UpdateBase(c context.Context, b *models.DataCapture) (*models.DataCapture, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	datacapture, err := a.DataCaptureRepository.UpdateBase(ctx, b)

	if err != nil {
		return nil, err
	}
	//Insert To Journal
	if len(b.DataCaptureAmount) > 0 {
		err = a.InsertJournalBase(ctx, b)
		if err != nil {
			return nil, err
		}
	}
	return datacapture, nil
}
func (a *DataCaptureUsecase) InsertJournal(ctx context.Context, b *models.DataCapture) error {
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)

	journalID := guid.New().StringUpper()
	journal := &models.Journal{
		ID:            journalID,
		TransactionID: b.ID,
		PostingDate:   b.TransactionDate,
		Description:   NewNullString(b.Description),
		Code:          NewNullString(b.Code),
	}
	for _, dca := range b.DataCaptureAmount {
		journalDetailID := guid.New().StringUpper()
		var debit float64
		var credit float64
		if dca.TransactionType == "1" {
			debit = dca.DCAmount
			credit = 0
		}
		if dca.TransactionType == "2" {
			debit = 0
			credit = dca.DCAmount
		}

		journalDetail := &models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           dca.COAID,
			Debit:           debit,
			Credit:          credit,
			Notes:           b.Description,
			ReferenceNumber: dca.ReferenceNumber,
		}
		journal.JournalDetails = append(journal.JournalDetails, *journalDetail)
	}
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err := JournalUsecase.Store(ctx, journal)

	if err != nil {
		return err
	}
	return nil
}
func (a *DataCaptureUsecase) InsertJournalBase(ctx context.Context, b *models.DataCapture) error {
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)

	journalID := guid.New().StringUpper()
	journal := &models.Journal{
		ID:            journalID,
		TransactionID: b.ID,
		PostingDate:   b.TransactionDate,
		Description:   NewNullString(b.Description),
		Code:          NewNullString(b.Code),
	}
	for _, dca := range b.DataCaptureAmount {
		journalDetailID := guid.New().StringUpper()
		var debit float64
		var credit float64
		if dca.TransactionType == "1" {
			debit = dca.DCAmount
			credit = 0
		}
		if dca.TransactionType == "2" {
			debit = 0
			credit = dca.DCAmount
		}

		journalDetail := &models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           dca.COAID,
			Debit:           debit,
			Credit:          credit,
			Notes:           b.Description,
			ReferenceNumber: dca.ReferenceNumber,
		}
		journal.JournalDetails = append(journal.JournalDetails, *journalDetail)
	}
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err := JournalUsecase.StoreBase(ctx, journal)

	if err != nil {
		return err
	}
	return nil
}
func NewNullString(s string) sql.NullString {
	// if s == nil {
	// 	return sql.NullString{}
	// }
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
