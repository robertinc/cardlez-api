package account

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AccountUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Account, string, error)
	GetByID(ctx context.Context, id string) (*models.Account, error)
	Update(ctx context.Context, ar *models.Account) (*models.Account, error)
	UpdateStatusByMemberId(ctx context.Context, memberID *string, recordStatus *int32) (bool, error)
	// GetByTitle(ctx context.Context, title string) (*models.Account, error)
	Store(context.Context, *models.Account) (*models.Account, error)
	StoreFromCore(context.Context, []*models.AccountCoreParameter) (bool, error)
	StoreFromActivation(context.Context, *models.Account) (*models.Account, error)
	StoreFromActivationByDate(ctx context.Context, acc *models.Account, companyDate string) (*models.Account, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, search string, keyword string, filter *models.AccountFilter) (error, []*models.Account)
	GetByCode(ctx context.Context, code *string) (*models.Account, error)
	GetByRetailProductCode(ctx context.Context, code string, memberid string) (*models.Account, error)
	GetByMemberID(ctx context.Context, memberID string) (*models.Account, error)
	GetByMemberHandPhone(ctx context.Context, memberHandPhone string) (*models.Account, error)
	GetByMemberIDBase(ctx context.Context, memberID string, accType string) (*models.Account, error)
}
