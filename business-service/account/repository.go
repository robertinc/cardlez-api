package account

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AccountRepository interface {
	Fetch(ctx context.Context, search string, keyword string, filter *models.AccountFilter) (error, []*models.Account)
	Update(ctx context.Context, account *models.Account) (*models.Account, error)
	UpdateStatusByMemberId(ctx context.Context, memberID *string, recordStatus *int32) (bool, error)
	Store(ctx context.Context, account *models.Account) (*models.Account, error)
	StoreByDate(ctx context.Context, account *models.Account, companyDate string) (*models.Account, error)
	StoreMdwByDate(ctx context.Context, account *models.Account, companyDate string) (*models.Account, error)
	GetByCode(ctx context.Context, code *string) (*models.Account, error)
	GetByID(ctx context.Context, id *string) (*models.Account, error)
	GetByRetailProductCode(ctx context.Context, code string, memberid string) (*models.Account, error)
	GetByMemberID(ctx context.Context, memberID *string) (*models.Account, error)
	GetByMemberHandPhone(ctx context.Context, memberHandPhone *string) (*models.Account, error)
	GetByMemberIDBase(ctx context.Context, memberID *string, accType *string) (*models.Account, error)
}
