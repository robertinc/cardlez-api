package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/account"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlAccountRepository struct {
	Conn *sql.DB
}

func NewSqlAccountRepository() account.AccountRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlAccountRepository{conn}
}
func NewSqlAccountRepositoryV2(Conn *sql.DB) account.AccountRepository {
	conn := Conn
	return &sqlAccountRepository{conn}
}
func (sj *sqlAccountRepository) GetByCode(ctx context.Context, code *string) (*models.Account, error) {

	logger := service.Logger(ctx)
	var j models.Account
	logger.Info("Query Account",
		zap.String("code", service.NewNullString(*code).String),
	)
	err := sj.Conn.QueryRow(`
	SELECT   
	Cast(a.ID as varchar(36)) as ID,
	a.Code,
	a.AccountName,
	a.RecordStatus,
	IsNull(Cast(a.RetailProductID as varchar(36)), '') as ProductID,
	Cast(a.MemberID as varchar(36)) as MemberID,
	m.EmailAddress,
	Isnull(a.IsPrimary,0) as IsPrimary
	FROM dbo.Account a
		Inner Join Member m on a.MemberID = m.ID
	where 
		a.Code = @p0`, sql.Named("p0", code)).Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.RecordStatus,
		&j.ProductID,
		&j.MemberID,
		&j.Email,
		&j.IsPrimary,
	)

	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlAccountRepository) GetByID(ctx context.Context, id *string) (*models.Account, error) {

	var j models.Account
	logger := service.Logger(ctx)
	logger.Info("Query Account",
		zap.String("id", service.NewNullString(*id).String),
	)
	err := sj.Conn.QueryRow(`
		SELECT   
			Cast(a.ID as varchar(36)) as ID,
			a.Code,
			a.AccountName,
			a.RecordStatus,
			IsNull(Cast(RetailProductID as varchar(36)), '') as ProductID,
			Cast(MemberID as varchar(36)) as MemberID,
			Isnull(IsPrimary,0) as IsPrimary,
			Isnull(m.MemberName,'') as MemberName,
			Isnull(m.Handphone,'') as Handphone
		FROM 
			dbo.Account a
			LEFT JOIN Member m on m.ID = a.MemberID 
		where 
			a.ID = @p0`, sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.RecordStatus,
		&j.ProductID,
		&j.MemberID,
		&j.IsPrimary,
		&j.MemberName,
		&j.Phone,
	)

	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlAccountRepository) Fetch(ctx context.Context, search string, keyword string, filter *models.AccountFilter) (error, []*models.Account) {

	query := `
	SELECT  
		Cast(a.ID as varchar(36)) as ID,
		a.Code,
		AccountName, 
		a.RecordStatus,
		IsNull(rp.ProductName, '') as ProductName,
		IsNull(Cast(a.RetailProductID as varchar(36)), '') as ProductID,
		Coalesce(m.MemberName, mb.CompanyName,'') as MemberName,
		Isnull(m.EmailAddress, '') as Email,
		Isnull(m.Phone, '') as Phone,
		Coalesce(m.MemberType, m2.MemberType, 0),
		Cast(Coalesce(m.ID, m2.ID, '00000000-0000-0000-0000-000000000000') as varchar(36)) as MemberID,
		Isnull(Cast(mb.ID as varchar(36)), '')  as MemberBranchID,
		Isnull(vltc.saldo,0)  as Balance,
		Isnull(vltm.saldo,0)  as BalanceMerchant,
		Isnull(a.IsPrimary,0) as IsPrimary,
		Isnull(a.AccountName2,'') as AccountName2,
		Isnull(rp.ProductType,0) as ProductType
		
	FROM dbo.Account a
		LEFT JOIN dbo.RetailProduct rp on a.RetailProductID = rp.ID
		LEFT JOIN Member m on m.ID = a.MemberID
		Left Join MemberBranch mb on mb.ID = a.MemberID
		Left Join Member m2 on mb.MemberID = m2.ID
		Left Join VwListTabunganCustomer vltc on a.id=vltc.accountid
		left Join VwListTabunganMerchant vltm on a.id=vltm.accountid
		`
	if search != "ALL" && search != "" {
		if search == "PRODUCTNAME" || search == "PRODUCTCODE" {
			if search == "PRODUCTCODE" {
				search = "rp.code"
			} else {

				search = "rp." + search
			}
		} else if search == "MEMBERBRANCHID" {
			search = "mb.ID"
		} else {
			search = "a." + search
		}
		query += ` where ` + search + ` like @p0`
	} else {
		query += ` where 1=1`
	}
	name := ""
	noHP := ""
	noKTP := ""
	if filter != nil {
		if filter.NoKTP != nil {
			query += ` and IsNull(m.IdentityNumber, '') like @filterNoKTP`
			noKTP = *filter.NoKTP
		}
		if filter.NoHP != nil {
			query += ` and IsNull(m.Handphone, '') like @filterNoHP`
			noHP = *filter.NoHP
		}
		if filter.Name != nil {
			query += ` and IsNull(m.MemberName, '') like @filterName`
			name = *filter.Name
		}
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("filterName", fmt.Sprintf("%v", name)),
		zap.String("filterNoHP", fmt.Sprintf("%v", noHP)),
		zap.String("filterNoKTP", fmt.Sprintf("%v", noKTP)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
		sql.Named("filterName", "%"+name+"%"),
		sql.Named("filterNoHP", "%"+noHP+"%"),
		sql.Named("filterNoKTP", "%"+noKTP+"%"),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	defer rows.Close()

	result := make([]*models.Account, 0)
	for rows.Next() {
		j := new(models.Account)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.AccountName,
			&j.RecordStatus,
			&j.ProductName,
			&j.ProductID,
			&j.MemberName,
			&j.Email,
			&j.Phone,
			&j.MemberType,
			&j.MemberID,
			&j.MemberBranchID,
			&j.Balance,
			&j.BalanceMerchant,
			&j.IsPrimary,
			&j.AccountName2,
			&j.ProductType,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	//
	return nil, result
}

func (sj *sqlAccountRepository) Update(ctx context.Context, a *models.Account) (*models.Account, error) {

	updateQuery := `
	Update Account 
	Set MemberID = @p1, RetailProductID = @p2, AccountName = @p3, IsPrimary = @p4, RecordStatus = @p5
	Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("memberID", fmt.Sprintf("%v", a.MemberID)),
		zap.String("retailProductID", fmt.Sprintf("%v", a.ProductID)),
		zap.String("accountName", fmt.Sprintf("%v", a.AccountName)),
		zap.String("isPrimary", fmt.Sprintf("%v", a.IsPrimary)),
		zap.String("RecordStatus", fmt.Sprintf("%v", a.RecordStatus)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.ProductID),
		sql.Named("p3", a.AccountName),
		sql.Named("p4", a.IsPrimary),
		sql.Named("p5", a.RecordStatus),
	)

	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlAccountRepository) UpdateStatusByMemberId(ctx context.Context, memberID *string, recordStatus *int32) (bool, error) {

	updateQuery := `
	Update Account 
	Set RecordStatus = @p1
	Where MemberID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
		zap.String("RecordStatus", fmt.Sprintf("%v", recordStatus)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", memberID),
		sql.Named("p1", recordStatus),
	)

	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlAccountRepository) Store(ctx context.Context, a *models.Account) (*models.Account, error) {

	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	updateQuery := `
		Insert Into Account(
			ID
			,Code
			,MemberID
			,RetailProductID
			,CompanyID
			,CurrencyID
			,AccountName
			,AccountName2
			,AccountCharacteristicID
			,AccountMiscLiabTypeID
			,AccountDepoCharID
			,RevNo
			,RevRemarks
			,RecordStatus
			,UserInsert
			,DateInsert
			,UserAuthor
			,DateAuthor
			,UserUpdate
			,DateUpdate
			,IsPrimary
			,AccountCode
		)
		Values(
			@id,
			@p0,
			@p1,
			@p2,
			@companyID,
			'00000000-0000-0000-0000-000000000000',
			@p3,
			@p5,
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			null,
			null,
			0,
			null,
			getdate(),
			null, null, null, null,@p4,
			@accountCode
		)
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("Code", fmt.Sprintf("%v", a.Code)),
		zap.String("MemberID", fmt.Sprintf("%v", a.MemberID)),
		zap.String("RetailProductID", fmt.Sprintf("%v", a.ProductID)),
		zap.String("AccountName", fmt.Sprintf("%v", a.AccountName)),
		zap.String("AccountName2", fmt.Sprintf("%v", a.AccountName2)),
		zap.String("IsPrimary", fmt.Sprintf("%v", a.IsPrimary)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.ProductID),
		sql.Named("p3", a.AccountName),
		sql.Named("p4", a.IsPrimary),
		sql.Named("p5", a.AccountName2),
		sql.Named("accountCode", a.AccountCode),
		sql.Named("companyID", a.CompanyID),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlAccountRepository) StoreByDate(ctx context.Context, a *models.Account, companyDate string) (*models.Account, error) {

	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	updateQuery := `
		Insert Into Account(
			ID
			,Code
			,MemberID
			,RetailProductID
			,CompanyID
			,CurrencyID
			,AccountName
			,AccountName2
			,AccountCharacteristicID
			,AccountMiscLiabTypeID
			,AccountDepoCharID
			,RevNo
			,RevRemarks
			,RecordStatus
			,UserInsert
			,DateInsert
			,UserAuthor
			,DateAuthor
			,UserUpdate
			,DateUpdate
			,IsPrimary
			,AccountCode
		)
		Values(
			@id,
			@p0,
			@p1,
			@p2,
			@companyID,
			'00000000-0000-0000-0000-000000000000',
			@p3,
			@p5,
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			null,
			null,
			0,
			null,
			@date,
			null, null, null, null,@p4,
			@accountCode
		)
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("Code", fmt.Sprintf("%v", a.Code)),
		zap.String("MemberID", fmt.Sprintf("%v", a.MemberID)),
		zap.String("RetailProductID", fmt.Sprintf("%v", a.ProductID)),
		zap.String("AccountName", fmt.Sprintf("%v", a.AccountName)),
		zap.String("AccountName2", fmt.Sprintf("%v", a.AccountName2)),
		zap.String("IsPrimary", fmt.Sprintf("%v", a.IsPrimary)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.ProductID),
		sql.Named("p3", a.AccountName),
		sql.Named("p4", a.IsPrimary),
		sql.Named("p5", a.AccountName2),
		sql.Named("accountCode", a.AccountCode),
		sql.Named("companyID", a.CompanyID),
		sql.Named("date", companyDate),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlAccountRepository) StoreMdwByDate(ctx context.Context, a *models.Account, companyDate string) (*models.Account, error) {

	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	updateQuery := `
		Insert Into Account(
			ID
			,Code
			,MemberID
			,RetailProductID
			,CompanyID
			,CurrencyID
			,AccountName
			,AccountName2
			,AccountCharacteristicID
			,AccountMiscLiabTypeID
			,AccountDepoCharID
			,RevNo
			,RevRemarks
			,RecordStatus
			,UserInsert
			,DateInsert
			,UserAuthor
			,DateAuthor
			,UserUpdate
			,DateUpdate
			,IsPrimary
			,AccountCode
		)
		Values(
			@id,
			@p0,
			@p1,
			@p2,
			@companyID,
			'00000000-0000-0000-0000-000000000000',
			@p3,
			@p5,
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			'00000000-0000-0000-0000-000000000000',
			null,
			null,
			@status,
			null,
			@date,
			null, null, null, null,@p4,
			@accountCode
		)
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("Code", fmt.Sprintf("%v", a.Code)),
		zap.String("MemberID", fmt.Sprintf("%v", a.MemberID)),
		zap.String("RetailProductID", fmt.Sprintf("%v", a.ProductID)),
		zap.String("AccountName", fmt.Sprintf("%v", a.AccountName)),
		zap.String("AccountName2", fmt.Sprintf("%v", a.AccountName2)),
		zap.String("IsPrimary", fmt.Sprintf("%v", a.IsPrimary)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.ProductID),
		sql.Named("p3", a.AccountName),
		sql.Named("p4", a.IsPrimary),
		sql.Named("p5", a.AccountName2),
		sql.Named("accountCode", a.AccountCode),
		sql.Named("companyID", a.CompanyID),
		sql.Named("date", companyDate),
		sql.Named("status", a.RecordStatus),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlAccountRepository) GetByRetailProductCode(ctx context.Context, code string, memberid string) (*models.Account, error) {

	var j models.Account
	query := `
		SELECT  
			Cast(a.ID as varchar(36)) as ID,
			a.Code,
			AccountName, 
			a.RecordStatus,
			IsNull(rp.ProductName, '') as ProductName,
			IsNull(Cast(a.RetailProductID as varchar(36)), '') as ProductID,
			Coalesce(m.MemberName, mb.CompanyName,'') as MemberName,
			Isnull(m.EmailAddress, '') as Email,
			Isnull(m.Phone, '') as Phone,
			Isnull(m.MemberType, m2.MemberType),
			Cast(Isnull(m.ID, m2.ID) as varchar(36)) as MemberID,
			Isnull(Cast(mb.ID as varchar(36)), '')  as MemberBranchID,
			Isnull(vltc.saldo,0)  as Balance,
			Isnull(vltm.saldo,0)  as BalanceMerchant,
			Isnull(a.IsPrimary,0) as IsPrimary,
			Isnull(a.AccountName2,'') as AccountName2,
			Isnull(rp.ProductType,0) as ProductType
		FROM dbo.Account a
			LEFT JOIN dbo.RetailProduct rp on a.RetailProductID = rp.ID
			LEFT JOIN Member m on m.ID = a.MemberID
			Left Join MemberBranch mb on mb.ID = a.MemberID
			Left Join Member m2 on mb.MemberID = m2.ID
			Left Join VwListTabunganCustomer vltc on a.id=vltc.accountid
			left Join VwListTabunganMerchant vltm on a.id=vltm.accountid
		WHERE rp.Code = @p0 AND a.MemberID = @p1`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("code", code),
		zap.String("memberid", memberid),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code),
		sql.Named("p1", memberid),
	).Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.RecordStatus,
		&j.ProductName,
		&j.ProductID,
		&j.MemberName,
		&j.Email,
		&j.Phone,
		&j.MemberType,
		&j.MemberID,
		&j.MemberBranchID,
		&j.Balance,
		&j.BalanceMerchant,
		&j.IsPrimary,
		&j.AccountName2,
		&j.ProductType,
	)

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlAccountRepository) GetByMemberID(ctx context.Context, memberID *string) (*models.Account, error) {

	var j models.Account
	logger := service.Logger(ctx)
	logger.Info("Query Account",
		zap.String("memberID", service.NewNullString(*memberID).String),
	)
	row := sj.Conn.QueryRow(`
		SELECT   
			Cast(a.ID as varchar(36)) as ID,
			a.Code,
			a.AccountName,
			a.RecordStatus,
			IsNull(Cast(RetailProductID as varchar(36)), '') as ProductID,
			Cast(MemberID as varchar(36)) as MemberID,
			Isnull(IsPrimary,0) as IsPrimary,
			Isnull(m.MemberName,'') as MemberName,
			Isnull(m.Handphone,'') as Handphone,
			IsNull(Cast(CompanyID as varchar(36)), '') as CompanyID
		FROM 
			dbo.Account a
			LEFT JOIN Member m on m.ID = a.MemberID 
		where 
			m.ID = @p0 and IsPrimary=1`, sql.Named("p0", memberID))
	err := row.Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.RecordStatus,
		&j.ProductID,
		&j.MemberID,
		&j.IsPrimary,
		&j.MemberName,
		&j.Phone,
		&j.CompanyID,
	)
	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlAccountRepository) GetByMemberHandPhone(ctx context.Context, memberHandPhone *string) (*models.Account, error) {

	var j models.Account
	logger := service.Logger(ctx)
	logger.Info("Query Account",
		zap.String("memberHandPhone", service.NewNullString(*memberHandPhone).String),
	)
	row := sj.Conn.QueryRow(`
		SELECT   
			Cast(a.ID as varchar(36)) as ID,
			a.Code,
			a.AccountName,
			a.AccountName2,
			a.RecordStatus,
			IsNull(Cast(RetailProductID as varchar(36)), '') as ProductID,
			Cast(MemberID as varchar(36)) as MemberID,
			Isnull(IsPrimary,0) as IsPrimary,
			Isnull(m.MemberName,'') as MemberName,
			Isnull(m.Handphone,'') as Handphone,
			IsNull(Cast(CompanyID as varchar(36)), '') as CompanyID
		FROM 
			dbo.Account a
			LEFT JOIN Member m on m.ID = a.MemberID 
		where 
			m.Handphone = @p0 and IsPrimary=1`, sql.Named("p0", memberHandPhone))
	err := row.Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.AccountName2,
		&j.RecordStatus,
		&j.ProductID,
		&j.MemberID,
		&j.IsPrimary,
		&j.MemberName,
		&j.Phone,
		&j.CompanyID,
	)
	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlAccountRepository) GetByMemberIDBase(ctx context.Context, memberID *string, accType *string) (*models.Account, error) {

	var j models.Account
	logger := service.Logger(ctx)
	logger.Info("Query Account",
		zap.String("memberID", service.NewNullString(*memberID).String),
	)
	row := sj.Conn.QueryRow(`
		SELECT   
			Cast(ID as varchar(36)) as ID,
			Code,
			AccountName,
			RecordStatus,
			IsNull(Cast(RetailProductID as varchar(36)), '') as ProductID,
			Cast(MemberID as varchar(36)) as MemberID,
			Isnull(IsPrimary,0) as IsPrimary,
			IsNull(Cast(CompanyID as varchar(36)), '') as CompanyID
		FROM 
			dbo.Account
		where 
			MemberID = @p0 and AccountCode=@p1`, sql.Named("p0", memberID), sql.Named("p1", accType))
	err := row.Scan(
		&j.ID,
		&j.Code,
		&j.AccountName,
		&j.RecordStatus,
		&j.ProductID,
		&j.MemberID,
		&j.IsPrimary,
		&j.CompanyID,
	)
	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
