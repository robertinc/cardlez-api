package usecase

import (
	"context"
	"errors"
	"strconv"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/account"
	"gitlab.com/robertinc/cardlez-api/business-service/account/repository"

	// memberrepository "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"

	cCompany "gitlab.com/robertinc/cardlez-api/business-service/company"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	cProduct "gitlab.com/robertinc/cardlez-api/business-service/product"
)

type AccountUsecase struct {
	AccountRepository account.AccountRepository
	contextTimeout    time.Duration
}

func NewAccountUsecase() account.AccountUsecase {
	return &AccountUsecase{
		AccountRepository: repository.NewSqlAccountRepository(),
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func NewAccountUsecaseV2(
	accountRepo account.AccountRepository) account.AccountUsecase {
	return &AccountUsecase{
		AccountRepository: accountRepo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *AccountUsecase) Fetch(c context.Context, search string, keyword string, filter *models.AccountFilter) (error, []*models.Account) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listAccount := a.AccountRepository.Fetch(ctx, search, keyword, filter)
	if err != nil {
		return err, nil
	}
	return nil, listAccount
}
func (a *AccountUsecase) Update(c context.Context, b *models.Account) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) UpdateStatusByMemberId(c context.Context, memberID *string, recordStatus *int32) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.UpdateStatusByMemberId(ctx, memberID, recordStatus)

	if err != nil {
		return false, err
	}
	return account, nil
}

func (a *AccountUsecase) Store(c context.Context, b *models.Account) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//productRepo := productrepository.NewSqlProductRepository()
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ID", b.ProductID)
	if err != nil {
		return nil, err
	}
	//  memberRepo := memberrepository.NewSqlMemberRepository()
	//  err, codeAccount := memberRepo.GenerateCode(ctx, products[0].PrefixCode)
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	err, codeAccount := memberRepo.GenerateCode(ctx, products[0].PrefixCode)
	if err != nil {
		return nil, err
	}
	b.Code = codeAccount
	account, err := a.AccountRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) StoreFromCore(c context.Context, b []*models.AccountCoreParameter) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	for _, account := range b {
		productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
		err, products := productRepo.Fetch(ctx, "PrefixCode", account.Type)
		if err != nil {
			return false, errors.New("Product Code is Unknown : " + account.Type)
		}
		companyRepo := ctx.Value("CompanyRepository").(cCompany.CompanyRepository)
		company, err := companyRepo.GetByCode(ctx, "0001")
		if err != nil {
			return false, errors.New("Company Code not found 0001")
		}
		// isPrimary := false
		// if account.Type == "33" {
		// 	isPrimary = true
		// }

		Account := &models.Account{
			ID:           account.ID,
			Code:         account.AccountNo,
			MemberID:     account.CustomerID,
			ProductID:    products[0].ID,
			AccountName:  account.AccountName,
			AccountName2: account.AccountName2,
			AccountCode:  account.Type,
			CompanyID:    company.ID,
			IsPrimary:    account.IsPrimary,
		}
		_, err = a.AccountRepository.Store(ctx, Account)
		if err != nil {
			return false, err
		}
	}
	return true, nil
}
func (a *AccountUsecase) StoreFromActivation(c context.Context, b *models.Account) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "PrefixCode", strconv.Itoa(int(b.ProductType)))
	if err != nil {
		return nil, errors.New("Product Code is Unknown : " + strconv.Itoa(int(b.ProductType)))
	}
	companyRepo := ctx.Value("CompanyRepository").(cCompany.CompanyRepository)
	company, err := companyRepo.GetByCode(ctx, "0001")
	if err != nil {
		return nil, errors.New("Company Code not found 0001")
	}

	b.ProductID = products[0].ID
	b.CompanyID = company.ID

	account, err := a.AccountRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}

	return account, nil
}
func (a *AccountUsecase) StoreFromActivationByDate(c context.Context, b *models.Account, companyDate string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "PrefixCode", strconv.Itoa(int(b.ProductType)))
	if err != nil {
		return nil, errors.New("Product Code is Unknown : " + strconv.Itoa(int(b.ProductType)))
	}
	companyRepo := ctx.Value("CompanyRepository").(cCompany.CompanyRepository)
	company, err := companyRepo.GetByCode(ctx, "0001")
	if err != nil {
		return nil, errors.New("Company Code not found 0001")
	}

	b.ProductID = products[0].ID
	b.CompanyID = company.ID

	account, err := a.AccountRepository.StoreByDate(ctx, b, companyDate)
	if err != nil {
		return nil, err
	}

	return account, nil
}
func (a *AccountUsecase) GetByCode(c context.Context, code *string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByCode(ctx, code)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) GetByID(c context.Context, id string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByID(ctx, &id)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) GetByRetailProductCode(c context.Context, code string, memberid string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByRetailProductCode(ctx, code, memberid)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) GetByMemberID(c context.Context, memberID string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByMemberID(ctx, &memberID)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) GetByMemberHandPhone(c context.Context, memberHandPhone string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByMemberHandPhone(ctx, &memberHandPhone)

	if err != nil {
		return nil, err
	}
	return account, nil
}
func (a *AccountUsecase) GetByMemberIDBase(c context.Context, memberID string, accType string) (*models.Account, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	account, err := a.AccountRepository.GetByMemberIDBase(ctx, &memberID, &accType)

	if err != nil {
		return nil, err
	}
	return account, nil
}
