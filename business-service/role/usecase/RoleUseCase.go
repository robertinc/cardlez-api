package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/role"
	"gitlab.com/robertinc/cardlez-api/business-service/role/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleUsecase struct {
	RoleRepository role.RoleRepository
	contextTimeout time.Duration
}

func NewRoleUsecase() role.RoleUsecase {
	return &RoleUsecase{
		RoleRepository: repository.NewSqlRoleRepository(),
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func NewRoleUsecaseV2(repo role.RoleRepository) role.RoleUsecase {
	return &RoleUsecase{
		RoleRepository: repo,
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func (a *RoleUsecase) Fetch(c context.Context) (error, []*models.Role) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listRole := a.RoleRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listRole
}

func (a *RoleUsecase) GetByID(c context.Context, id *string) *models.Role {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listRole := a.RoleRepository.GetByID(ctx, id)

	return listRole
}

func (a *RoleUsecase) GetByName(c context.Context, name *string) (*models.Role, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listRole, err := a.RoleRepository.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return listRole, nil
}

func (a *RoleUsecase) Store(c context.Context, b *models.Role) (*models.Role, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	role, err := a.RoleRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return role, nil
}

func (a *RoleUsecase) Update(c context.Context, b *models.Role) (*models.Role, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	role, err := a.RoleRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return role, nil
}

func (a *RoleUsecase) Delete(c context.Context, b *models.Role) (*models.Role, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	role, err := a.RoleRepository.Delete(ctx, b)

	if err != nil {
		return nil, err
	}
	return role, nil
}
