package role

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleUsecase interface {
	GetByID(ctx context.Context, id *string) *models.Role
	Update(ctx context.Context, ar *models.Role) (*models.Role, error)
	Delete(ctx context.Context, ar *models.Role) (*models.Role, error)
	Store(context.Context, *models.Role) (*models.Role, error)
	Fetch(ctx context.Context) (error, []*models.Role)
	GetByName(ctx context.Context, name *string) (*models.Role, error)
}
