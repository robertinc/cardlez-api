package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/role"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlRoleRepository struct {
	Conn *sql.DB
}

func NewSqlRoleRepository() role.RoleRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlRoleRepository{conn}
}

func NewSqlRoleRepositoryV2(Conn *sql.DB) role.RoleRepository {
	conn := Conn
	return &sqlRoleRepository{conn}
}

func (sj *sqlRoleRepository) Fetch(ctx context.Context) (error, []*models.Role) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		RoleName
	FROM CompanyUserRole
	WHERE RecordStatus = 1`
	result := make([]*models.Role, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Role)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.RoleName,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlRoleRepository) GetByID(ctx context.Context, id *string) *models.Role {
	var role models.Role
	logger := service.Logger(ctx)
	query := `
	select 
		cast(ID as varchar(36)) ID, 
		Code, 
		RoleName
	from 
		CompanyUserRole 
	where 
		ID = @p0`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&role.ID,
		&role.Code,
		&role.RoleName)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &role
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", role)),
	)
	return &role
}

func (sj *sqlRoleRepository) Store(ctx context.Context, a *models.Role) (*models.Role, error) {
	a.ID = guid.New().StringUpper()
	roleQuery := `INSERT INTO CompanyUserRole (id, code, roleName, recordStatus) VALUES (@p0, @p1, @p2, 1)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", roleQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
		zap.String("roleName", fmt.Sprintf("%v", a.RoleName)),
	)
	_, err := sj.Conn.ExecContext(ctx, roleQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.RoleName),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlRoleRepository) Update(ctx context.Context, a *models.Role) (*models.Role, error) {

	roleQuery := `Update CompanyUserRole Set Code = @p1, RoleName = @p2 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", roleQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
		zap.String("roleName", fmt.Sprintf("%v", a.RoleName)),
	)
	_, err := sj.Conn.ExecContext(ctx, roleQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.RoleName),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlRoleRepository) Delete(ctx context.Context, a *models.Role) (*models.Role, error) {

	roleQuery := `
		Update CompanyUserRole 
		Set 
			RecordStatus = 0,
			Code = 'Inactive',
			RoleName = 'Inactive'
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", roleQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, roleQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlRoleRepository) GetByName(ctx context.Context, name *string) (*models.Role, error) {
	var role models.Role
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code, 
			RoleName
		from 
			CompanyUserRole 
		where 
			RoleName = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("roleName", fmt.Sprintf("%v", role.RoleName)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", name)).Scan(
		&role.ID,
		&role.Code,
		&role.RoleName)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", role)),
	)
	return &role, nil
}
