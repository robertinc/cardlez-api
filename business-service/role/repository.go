package role

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleRepository interface {
	Fetch(ctx context.Context) (error, []*models.Role)
	GetByID(ctx context.Context, id *string) *models.Role
	Update(ctx context.Context, role *models.Role) (*models.Role, error)
	Delete(ctx context.Context, role *models.Role) (*models.Role, error)
	Store(ctx context.Context, a *models.Role) (*models.Role, error)
	GetByName(ctx context.Context, name *string) (*models.Role, error)
}
