package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/company"
	companyuser "gitlab.com/robertinc/cardlez-api/business-service/company-user"
	companyuserrepo "gitlab.com/robertinc/cardlez-api/business-service/company-user/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/company/repository"
	member "gitlab.com/robertinc/cardlez-api/business-service/member"
	memberrepo "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type CompanyUsecase struct {
	CompanyRepository     company.CompanyRepository
	CompanyUserRepository companyuser.CompanyUserRepository
	MemberRepository      member.MemberRepository
	contextTimeout        time.Duration
}

func NewCompanyUsecase() company.CompanyUsecase {
	return &CompanyUsecase{
		CompanyRepository:     repository.NewSqlCompanyRepository(),
		CompanyUserRepository: companyuserrepo.NewSqlCompanyUserRepository(),
		MemberRepository:      memberrepo.NewSqlMemberRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewCompanyUsecaseV2(companyRepo company.CompanyRepository, companyUserRepo companyuser.CompanyUserRepository,
	memberRepo member.MemberRepository) company.CompanyUsecase {
	return &CompanyUsecase{
		CompanyRepository:     companyRepo,
		CompanyUserRepository: companyUserRepo,
		MemberRepository:      memberRepo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func (a *CompanyUsecase) Fetch(c context.Context) (error, []*models.Company) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listCompany := a.CompanyRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listCompany
}

func (a *CompanyUsecase) FetchMultiCompany(c context.Context) (error, []*models.Company) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listCompany := a.CompanyRepository.FetchMultiCompany(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listCompany
}

func (a *CompanyUsecase) GetByID(c context.Context, id *string) *models.Company {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompany := a.CompanyRepository.GetByID(ctx, id)

	return listCompany
}

func (a *CompanyUsecase) GetByName(c context.Context, name *string) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompany, err := a.CompanyRepository.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return listCompany, nil
}
func (a *CompanyUsecase) GetByCode(c context.Context, code string) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompany, err := a.CompanyRepository.GetByCode(ctx, code)
	if err != nil {
		return nil, err
	}
	return listCompany, nil
}

func (a *CompanyUsecase) GetByCodeMultiCompany(c context.Context, code string) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompany, err := a.CompanyRepository.GetByCodeMultiCompany(ctx, code)
	if err != nil {
		return nil, err
	}
	return listCompany, nil
}

func (a *CompanyUsecase) Store(c context.Context, b *models.Company) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	memberRepo := memberrepo.NewSqlMemberRepository()
	err, code := memberRepo.GenerateCode(ctx, "CO")

	if err != nil {
		return nil, err
	}
	b.Code = code
	company, err := a.CompanyRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return company, nil
}

func (a *CompanyUsecase) Update(c context.Context, b *models.Company) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	company, err := a.CompanyRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return company, nil
}

func (a *CompanyUsecase) StoreMultiCompany(c context.Context, b *models.Company) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	company, err := a.CompanyRepository.StoreMultiCompany(ctx, b)

	if err != nil {
		return nil, err
	}
	return company, nil
}

func (a *CompanyUsecase) UpdateMultiCompany(c context.Context, b *models.Company) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	company, err := a.CompanyRepository.UpdateMultiCompany(ctx, b)

	if err != nil {
		return nil, err
	}
	return company, nil
}

func (a *CompanyUsecase) UpdateLogoMultiCompany(c context.Context, id string, fileName string) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	company, err := a.CompanyRepository.UpdateLogoMultiCompany(ctx, id, fileName)

	if err != nil {
		return company, err
	}
	return company, nil
}

func (a *CompanyUsecase) Delete(c context.Context, b *models.Company) (*models.Company, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	//Validate User Or Member not referring to this company
	err, listCompanyUser := a.CompanyUserRepository.Fetch(ctx)
	if err != nil {
		return nil, err
	}
	if len(listCompanyUser) > 0 {
		return nil, fmt.Errorf("Can't delete Company because of some user using this company")
	}
	err, listMember := a.MemberRepository.Fetch(ctx, "asc", 0, 100, -1, 0, nil)
	if err != nil {
		return nil, err
	}
	if len(listMember) > 0 {
		return nil, fmt.Errorf("Can't delete Company because of some member using this company")
	}
	company, err := a.CompanyRepository.Delete(ctx, b)

	if err != nil {
		return nil, err
	}
	return company, nil
}
