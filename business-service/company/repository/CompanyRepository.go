package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/company"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlCompanyRepository struct {
	Conn *sql.DB
}

func NewSqlCompanyRepository() company.CompanyRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlCompanyRepository{conn}
}

func NewSqlCompanyRepositoryV2(Conn *sql.DB) company.CompanyRepository {
	conn := Conn
	return &sqlCompanyRepository{conn}
}

func (sj *sqlCompanyRepository) Fetch(ctx context.Context) (error, []*models.Company) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		CompanyName,
		CompanyType,
		Mnemonic
	FROM Company
	Where RecordStatus <> 0`
	result := make([]*models.Company, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Company)
		err = rows.Scan(
			&j.ID,
			&j.CompanyName,
			&j.CompanyType,
			&j.Mnemonic,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlCompanyRepository) FetchMultiCompany(ctx context.Context) (error, []*models.Company) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		CompanyName,
		CompanyType,
		Mnemonic,
		BaseURL,
		isnull(ImageLogo, '') as ImageLogo
	FROM Company
	Where RecordStatus <> 0`
	result := make([]*models.Company, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Company)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.CompanyName,
			&j.CompanyType,
			&j.Mnemonic,
			&j.BaseURL,
			&j.ImageLogo,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlCompanyRepository) GetByID(ctx context.Context, id *string) *models.Company {
	var company models.Company
	logger := service.Logger(ctx)
	query := `
	select 
		cast(ID as varchar(36)) ID, 
		CompanyName,
		CompanyType,
		Mnemonic
	from 
		Company
	where 
		ID = @p0`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&company.ID,
		&company.CompanyName,
		&company.CompanyType,
		&company.Mnemonic)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &company
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", company)),
	)
	return &company
}

func (sj *sqlCompanyRepository) Store(ctx context.Context, a *models.Company) (*models.Company, error) {
	a.ID = guid.New().StringUpper()
	companyQuery := `
		INSERT INTO Company (
			id, 
			companyName,
			currencyID,
			companyType,
			mnemonic,
			recordStatus
		) VALUES (
			@p0,
			@p1,
			'5DD09F82-2E56-42F6-B392-57D52D7F26C8',
			@p2,
			@p3,
			1
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("companyName", fmt.Sprintf("%v", a.CompanyName)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.CompanyName),
		sql.Named("p2", a.CompanyType),
		sql.Named("p3", a.Mnemonic),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyRepository) Update(ctx context.Context, a *models.Company) (*models.Company, error) {

	companyQuery := `
		Update Company 
		Set 
			CompanyName = @p2,
			CompanyType = @p3,
			Mnemonic = @p4
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("companyName", fmt.Sprintf("%v", a.CompanyName)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", a.ID),
		sql.Named("p2", a.CompanyName),
		sql.Named("p3", a.CompanyType),
		sql.Named("p4", a.Mnemonic),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlCompanyRepository) Delete(ctx context.Context, a *models.Company) (*models.Company, error) {

	companyQuery := `
		Update Company 
		Set 
			RecordStatus = 0
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyRepository) StoreMultiCompany(ctx context.Context, a *models.Company) (*models.Company, error) {
	a.ID = guid.New().StringUpper()
	companyQuery := `
		INSERT INTO Company (
			id, 
			companyName,
			currencyID,
			companyType,
			mnemonic,
			recordStatus, 
			baseurl,
			code
		) VALUES (
			@p0,
			@p1,
			'5DD09F82-2E56-42F6-B392-57D52D7F26C8',
			@p2,
			@p3,
			1,
			@p4,
			@p5
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("companyName", fmt.Sprintf("%v", a.CompanyName)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.CompanyName),
		sql.Named("p2", a.CompanyType),
		sql.Named("p3", a.Mnemonic),
		sql.Named("p4", a.BaseURL),
		sql.Named("p5", a.Code),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyRepository) UpdateMultiCompany(ctx context.Context, a *models.Company) (*models.Company, error) {

	companyQuery := `
		Update Company 
		Set 
			CompanyName = @p2,
			CompanyType = @p3,
			Mnemonic = @p4,
			BaseUrl = @p5,
			Code = @p6
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("companyName", fmt.Sprintf("%v", a.CompanyName)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", a.ID),
		sql.Named("p2", a.CompanyName),
		sql.Named("p3", a.CompanyType),
		sql.Named("p4", a.Mnemonic),
		sql.Named("p5", a.BaseURL),
		sql.Named("p6", a.Code),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyRepository) UpdateLogoMultiCompany(ctx context.Context, id string, fileName string) (bool, error) {

	companyQuery := `
		Update Company 
		Set 
			ImageLogo = @p1
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyQuery)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("filename", fmt.Sprintf("%v", fileName)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyQuery,
		sql.Named("p0", id),
		sql.Named("p1", fileName),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	return true, nil
}

func (sj *sqlCompanyRepository) GetByName(ctx context.Context, name *string) (*models.Company, error) {
	var company models.Company
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			CompanyName
		from 
			Company 
		where 
			CompanyName = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("companyName", fmt.Sprintf("%v", company.CompanyName)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", name)).Scan(
		&company.ID,
		&company.CompanyName)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", company)),
	)
	return &company, nil
}

func (sj *sqlCompanyRepository) GetByCode(ctx context.Context, code string) (*models.Company, error) {
	var company models.Company
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			CompanyName
		from 
			Company 
		where 
			Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", company.Code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&company.ID,
		&company.Code,
		&company.CompanyName)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", company)),
	)
	return &company, nil
}

func (sj *sqlCompanyRepository) GetByCodeMultiCompany(ctx context.Context, code string) (*models.Company, error) {
	var company models.Company
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			CompanyName,
			ISNULL(BaseURL,'') as BaseURL,
			ISNULL(ImageLogo,'') as ImageLogo
		from 
			Company 
		where 
			Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", company.Code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&company.ID,
		&company.Code,
		&company.CompanyName,
		&company.BaseURL,
		&company.ImageLogo,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", company)),
	)
	return &company, nil
}
