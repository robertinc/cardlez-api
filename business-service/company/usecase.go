package company

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CompanyUsecase interface {
	GetByID(ctx context.Context, id *string) *models.Company
	Update(ctx context.Context, ar *models.Company) (*models.Company, error)
	Store(context.Context, *models.Company) (*models.Company, error)
	UpdateMultiCompany(ctx context.Context, ar *models.Company) (*models.Company, error)
	UpdateLogoMultiCompany(c context.Context, id string, fileName string) (bool, error)
	StoreMultiCompany(context.Context, *models.Company) (*models.Company, error)
	Fetch(ctx context.Context) (error, []*models.Company)
	FetchMultiCompany(ctx context.Context) (error, []*models.Company)
	GetByName(ctx context.Context, name *string) (*models.Company, error)
	GetByCode(ctx context.Context, code string) (*models.Company, error)
	GetByCodeMultiCompany(ctx context.Context, code string) (*models.Company, error)
	Delete(ctx context.Context, ar *models.Company) (*models.Company, error)
}
