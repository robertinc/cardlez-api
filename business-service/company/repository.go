package company

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CompanyRepository interface {
	Fetch(ctx context.Context) (error, []*models.Company)
	FetchMultiCompany(ctx context.Context) (error, []*models.Company)
	GetByID(ctx context.Context, id *string) *models.Company
	GetByCode(ctx context.Context, code string) (*models.Company, error)
	GetByCodeMultiCompany(ctx context.Context, code string) (*models.Company, error)
	Update(ctx context.Context, company *models.Company) (*models.Company, error)
	Store(ctx context.Context, a *models.Company) (*models.Company, error)
	UpdateMultiCompany(ctx context.Context, company *models.Company) (*models.Company, error)
	UpdateLogoMultiCompany(ctx context.Context, id string, fileName string) (bool, error)
	StoreMultiCompany(ctx context.Context, a *models.Company) (*models.Company, error)
	GetByName(ctx context.Context, name *string) (*models.Company, error)
	Delete(ctx context.Context, ar *models.Company) (*models.Company, error)
}
