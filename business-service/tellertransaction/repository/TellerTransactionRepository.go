package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/tellertransaction"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlTellerTransactionRepository struct {
	Conn *sql.DB
}

func NewSqlTellerTransactionRepository() tellertransaction.TellerTransactionRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlTellerTransactionRepository{conn}
}

func NewSqlTellerTransactionRepositoryV2(Conn *sql.DB) tellertransaction.TellerTransactionRepository {
	conn := Conn
	return &sqlTellerTransactionRepository{conn}
}
func (sj *sqlTellerTransactionRepository) Fetch(ctx context.Context, id string) (error, []*models.TellerTransaction) {
	query := `
	SELECT   
		cast(t.ID as varchar(36)) as ID, 
		t.Code,
		cast(t.AccountSourceID as varchar(36)) as AccountSourceID,
		t.TransAmount,
		cast(t.TellerID as varchar(36)) as TellerID,
		t.TxnCodeDb,
		t.RecordStatus,
		vw.NamaAgen,
		cast(t.transactiondate as varchar(50)) as TransactionDate,
		vw.MemberPhone,
		vw.Pemohon,
		t.transtype,
		cast(t.dateupdate as varchar(50)) as DateUpdate
	from 
		TellerTransaction t
		LEFT JOIN VWTARIKTUNAI vw on t.id=vw.id
	where 
		MemberID=@p0 or vw.AgentID=@p0
	order by 
		t.transactiondate desc`
	result := make([]*models.TellerTransaction, 0)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.TellerTransaction)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.AccountSourceID,
			&j.TransAmount,
			&j.TellerID,
			&j.TxnCodeDb,
			&j.RecordStatus,
			&j.AgentName,
			&j.TransactionDate,
			&j.MemberPhone,
			&j.Pemohon,
			&j.TransType,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlTellerTransactionRepository) FetchTransType(ctx context.Context, id string, transType int32, setorTarik bool, recordStatus int32, dateStart string, dateFinish string) (error, []*models.TellerTransaction) {
	queryValidationTranstype := "t.transtype = @p1"
	if setorTarik == true {
		queryValidationTranstype = "(t.transtype = 1 OR t.transtype = 2)"
	}
	queryValidationID := ""
	if id != "" {
		queryValidationID = " AND (MemberID=@p0 or vw.AgentID=@p0)"
	}
	queryRecordStatus := ""
	if recordStatus != 0 {
		queryRecordStatus = " AND t.RecordStatus = @p2"
	}
	queryDateUpdateFilter := ""
	if dateStart != "" {
		queryDateUpdateFilter = " AND t.dateupdate >= @p3 AND t.dateupdate < @p4"
	}
	query := `
	SELECT   
		cast(t.ID as varchar(36)) as ID, 
		t.Code,
		cast(t.AccountSourceID as varchar(36)) as AccountSourceID,
		t.TransAmount,
		cast(t.TellerID as varchar(36)) as TellerID,
		t.TxnCodeDb,
		t.RecordStatus,
		vw.NamaAgen,
		cast(t.transactiondate as varchar(50)) as TransactionDate,
		vw.MemberPhone,
		vw.Pemohon,
		t.TransType,
		cast(t.dateupdate as varchar(50)) as DateUpdate
	from 
		TellerTransaction t
		LEFT JOIN VWTARIKTUNAI vw on t.id=vw.id
	where 
		` + queryValidationTranstype + queryValidationID + queryRecordStatus + queryDateUpdateFilter + `
	order by 
		t.transactiondate desc`
	result := make([]*models.TellerTransaction, 0)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("transtype", fmt.Sprintf("%v", transType)),
		zap.String("recordStatus", fmt.Sprintf("%v", recordStatus)),
		zap.String("datestart", fmt.Sprintf("%v", dateStart)),
		zap.String("datefinish", fmt.Sprintf("%v", dateFinish)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
		sql.Named("p1", transType),
		sql.Named("p2", recordStatus),
		sql.Named("p3", dateStart+" 00:00:00"),
		sql.Named("p4", dateFinish+" 23:59:59"),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.TellerTransaction)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.AccountSourceID,
			&j.TransAmount,
			&j.TellerID,
			&j.TxnCodeDb,
			&j.RecordStatus,
			&j.AgentName,
			&j.TransactionDate,
			&j.MemberPhone,
			&j.Pemohon,
			&j.TransType,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlTellerTransactionRepository) Store(ctx context.Context, a *models.TellerTransaction) (*models.TellerTransaction, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	query := `INSERT INTO TellerTransaction (
		id, 
		code, 
		CompanyID, 
		AccountSourceID, 
		AccountDestinationID, 
		TransType,
		TransAmount,
		TellerID,
		TxnCodeDb,
		TransactionDate,
		RefNo,
		RecordStatus,
		DateInsert,
		DateUpdate
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @transactionDate,@p9, @p10, 
	GETDATE(), GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CompanyID),
		sql.Named("p3", a.AccountSourceID),
		sql.Named("p4", a.AccountDestinationID),
		sql.Named("p5", a.TransType),
		sql.Named("p6", a.TransAmount),
		sql.Named("p7", a.TellerID),
		sql.Named("p8", &a.TxnCodeDb),
		sql.Named("transactionDate", a.TransactionDate),
		sql.Named("p9", a.RefNo),
		sql.Named("p10", a.RecordStatus),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlTellerTransactionRepository) StoreBase(ctx context.Context, a *models.TellerTransaction) (*models.TellerTransaction, error) {
	today := time.Now().Format("2006-01-02 15:04:05")
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	query := `INSERT INTO TellerTransaction (
		id, 
		code, 
		CompanyID, 
		AccountSourceID, 
		AccountDestinationID, 
		TransType,
		TransAmount,
		TellerID,
		TxnCodeDb,
		TransactionDate,
		RefNo,
		RecordStatus,
		DateInsert,
		DateUpdate,
		DateAuthor
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @transactionDate,@p9, @p10, 
		@transactionDate, @transactionDate,@today)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CompanyID),
		sql.Named("p3", a.AccountSourceID),
		sql.Named("p4", a.AccountDestinationID),
		sql.Named("p5", a.TransType),
		sql.Named("p6", a.TransAmount),
		sql.Named("p7", a.TellerID),
		sql.Named("p8", &a.TxnCodeDb),
		sql.Named("transactionDate", a.TransactionDate),
		sql.Named("p9", a.RefNo),
		sql.Named("p10", a.RecordStatus),
		sql.Named("today", today),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlTellerTransactionRepository) COAGetByProductID(ctx context.Context, productid *string) (*models.COA, error) {
	var j models.COA
	query := `
	SELECT   
		cast(ID as varchar(36)) as ID, Code, Description
	from 
		COA 
	where 
		ProductID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("productid", fmt.Sprintf("%v", productid)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", productid)).Scan(
		&j.ID,
		&j.Code,
		&j.Description,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlTellerTransactionRepository) COAGetByID(ctx context.Context, id *string) (*models.COA, error) {
	var j models.COA
	query := `
	SELECT   
	cast(ID as varchar(36)) as ID, Code, Description
	from 
		COA where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.Description,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlTellerTransactionRepository) Update(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error) {

	updateQuery := `Update TellerTransaction Set RecordStatus = @p2,DateUpdate = GETDATE() Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", b.ID),
		sql.Named("p2", b.RecordStatus),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return b, nil
}
func (sj *sqlTellerTransactionRepository) GetByID(ctx context.Context, id *string) (*models.TellerTransaction, error) {
	var j models.TellerTransaction
	query := `
		SELECT   
		cast(t.ID as varchar(36)) as ID, 
		t.Code,
		cast(t.AccountSourceID as varchar(36)) as AccountSourceID,
		t.TransAmount,
		cast(t.TellerID as varchar(36)) as TellerID,
		t.TxnCodeDb,
		t.RecordStatus,
		vw.NamaAgen,
		cast(t.transactiondate as varchar(50)) as TransactionDate,
		vw.MemberPhone,
		vw.Pemohon,
		t.TransType,
		cast(t.dateupdate as varchar(50)) as DateUpdate,
		cast(t.CompanyID as varchar(36)) as CompanyID,
		isnull(cast(t.AccountDestinationID as varchar(36)),'') as AccountDestinationID
	from 
		TellerTransaction t
		LEFT JOIN VWTARIKTUNAI vw on t.id=vw.id
	where 
		t.ID = @p0
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.AccountSourceID,
		&j.TransAmount,
		&j.TellerID,
		&j.TxnCodeDb,
		&j.RecordStatus,
		&j.AgentName,
		&j.TransactionDate,
		&j.MemberPhone,
		&j.Pemohon,
		&j.TransType,
		&j.DateUpdate,
		&j.CompanyID,
		&j.AccountDestinationID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlTellerTransactionRepository) GetByRefNo(ctx context.Context, refNo *string) (*models.TellerTransaction, error) {
	var j models.TellerTransaction
	query := `
		SELECT   
		cast(t.ID as varchar(36)) as ID, 
		t.Code,
		cast(t.AccountSourceID as varchar(36)) as AccountSourceID,
		t.TransAmount,
		cast(t.TellerID as varchar(36)) as TellerID,
		t.TxnCodeDb,
		t.RecordStatus,
		vw.NamaAgen,
		cast(t.transactiondate as varchar(50)) as TransactionDate,
		vw.MemberPhone,
		vw.Pemohon,
		t.TransType,
		cast(t.dateupdate as varchar(50)) as DateUpdate,
		cast(t.CompanyID as varchar(36)) as CompanyID
	from 
		TellerTransaction t
		LEFT JOIN VWTARIKTUNAI vw on t.id=vw.id
	where 
		t.RefNo = @p0
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("refNo", fmt.Sprintf("%v", refNo)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", refNo)).Scan(
		&j.ID,
		&j.Code,
		&j.AccountSourceID,
		&j.TransAmount,
		&j.TellerID,
		&j.TxnCodeDb,
		&j.RecordStatus,
		&j.AgentName,
		&j.TransactionDate,
		&j.MemberPhone,
		&j.Pemohon,
		&j.TransType,
		&j.DateUpdate,
		&j.CompanyID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlTellerTransactionRepository) SettleSetorTarik(ctx context.Context, b []*models.TellerTransaction, nomorTransaksiSettlement string, status int32) ([]*models.TellerTransaction, error) {

	settleLogQuery := `
		UPDATE TellerTransaction 
		SET NomorTransaksiSettlement = @NomorTransaksiSettlement,
			DateUpdate = GETDATE(),
			RecordStatus = @status
		WHERE
			ID IN([params])
	`
	paramValues := make([]interface{}, 0)
	params := make([]string, 0)
	for i, tellerTransaction := range b {
		params = append(params, "@p"+strconv.Itoa(i))
		paramValues = append(paramValues, sql.Named("p"+strconv.Itoa(i), tellerTransaction.ID))
	}
	settleLogQuery = strings.Replace(
		settleLogQuery,
		"[params]",
		strings.Join(params, ","),
		1,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", settleLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	paramValues = append(paramValues,
		sql.Named("NomorTransaksiSettlement", nomorTransaksiSettlement),
	)
	paramValues = append(paramValues,
		sql.Named("status", status),
	)
	_, err := sj.Conn.ExecContext(ctx, settleLogQuery,
		paramValues...,
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
