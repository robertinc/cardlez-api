package tellertransaction

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TellerTransactionUsecase interface {
	Store(ctx context.Context, customerAccountCode string, memberID string, amount float64, transType int32) (*models.TellerTransaction, error)
	StoreCardlez(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error)
	StoreCardlezBase(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error)
	StoreCardlezBasev2(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error)
	Update(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error)
	CancelTransaction(ctx context.Context, id string) (*models.TellerTransaction, error)
	CancelTransactionByRefNo(ctx context.Context, refNo string) (*models.TellerTransaction, error)
	AcceptTransaction(ctx context.Context, id string) (*models.TellerTransaction, error)
	AcceptTransactionBase(ctx context.Context, id string) (*models.TellerTransaction, error)
	AcceptTransactionBasev2(ctx context.Context, id string) (*models.TellerTransaction, error)
	Fetch(ctx context.Context, id string) (error, []*models.TellerTransaction)
	FetchTarikTunai(ctx context.Context, id string) (error, []*models.TellerTransaction)
	GetByID(ctx context.Context, id *string) (*models.TellerTransaction, error)
	FetchSetorTarik(ctx context.Context, id string, isSettled bool, dateStart string, dateFinish string) (error, []*models.TellerTransaction)
	SettleSetorTarik(ctx context.Context, b []*models.TellerTransaction, reffNo string) ([]*models.TellerTransaction, error)
}
