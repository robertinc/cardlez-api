package tellertransaction

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TellerTransactionRepository interface {
	Store(ctx context.Context, a *models.TellerTransaction) (*models.TellerTransaction, error)
	StoreBase(ctx context.Context, a *models.TellerTransaction) (*models.TellerTransaction, error)
	COAGetByID(ctx context.Context, id *string) (*models.COA, error)
	COAGetByProductID(ctx context.Context, productid *string) (*models.COA, error)
	Update(ctx context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error)
	GetByID(ctx context.Context, id *string) (*models.TellerTransaction, error)
	GetByRefNo(ctx context.Context, refNo *string) (*models.TellerTransaction, error)
	Fetch(ctx context.Context, id string) (error, []*models.TellerTransaction)
	FetchTransType(ctx context.Context, id string, transType int32, setorTarik bool, recordStatus int32, dateStart string, dateFinish string) (error, []*models.TellerTransaction)
	SettleSetorTarik(ctx context.Context, b []*models.TellerTransaction, nomorTransaksiSettlement string, status int32) ([]*models.TellerTransaction, error)
}
