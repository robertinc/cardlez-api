package usecase

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/beevik/guid"

	// accountrepository "gitlab.com/robertinc/cardlez-api/business-service/account/repository"
	// journalmappingrepository "gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	// memberrepository "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	cAccount "gitlab.com/robertinc/cardlez-api/business-service/account"
	cAppConfig "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	cCOA "gitlab.com/robertinc/cardlez-api/business-service/coa"
	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	tellertransaction "gitlab.com/robertinc/cardlez-api/business-service/tellertransaction"
	"gitlab.com/robertinc/cardlez-api/business-service/tellertransaction/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	service "gitlab.com/robertinc/cardlez-api/service"
)

type TellerTransactionUsecase struct {
	TellerTransactionRepository tellertransaction.TellerTransactionRepository
	contextTimeout              time.Duration
}

func NewTellerTransactionUsecase() tellertransaction.TellerTransactionUsecase {
	return &TellerTransactionUsecase{
		TellerTransactionRepository: repository.NewSqlTellerTransactionRepository(),
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func NewTellerTransactionUsecaseV2(repo tellertransaction.TellerTransactionRepository) tellertransaction.TellerTransactionUsecase {
	return &TellerTransactionUsecase{
		TellerTransactionRepository: repo,
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}
func (a *TellerTransactionUsecase) Fetch(c context.Context, id string) (error, []*models.TellerTransaction) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listTellerTransaction := a.TellerTransactionRepository.Fetch(ctx, id)
	if err != nil {
		return err, nil
	}
	return nil, listTellerTransaction
}
func (a *TellerTransactionUsecase) FetchTarikTunai(c context.Context, id string) (error, []*models.TellerTransaction) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listTellerTransaction := a.TellerTransactionRepository.FetchTransType(ctx, id, 1, false, 0, "", "")
	if err != nil {
		return err, nil
	}
	return nil, listTellerTransaction
}
func (a *TellerTransactionUsecase) StoreCardlez(c context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	if b.TransType == 2 {
		//insert journal first
		err := a.InsertJournalSetor(ctx, b, b.TransType)
		if err != nil {
			return nil, err
		}
	} else if b.TransType == 3 {
		//insert journal first
		err := a.InsertJournalSetor(ctx, b, b.TransType)
		if err != nil {
			return nil, err
		}
	}

	TellerTransaction, err := a.TellerTransactionRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}
	return TellerTransaction, nil
}
func (a *TellerTransactionUsecase) StoreCardlezBase(c context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	if b.TransType == 2 {
		//insert journal first
		err := a.InsertJournalSetorByDate(ctx, b, b.TransType)
		if err != nil {
			return nil, err
		}
	} else if b.TransType == 3 {
		configKey := "Beban Biaya Transfer Masuk"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
		if err != nil {
			return nil, err
		}
		if appConfig.ConfigValue == "1" { //beban biaya ditanggung nasabah
			//insert journal first
			err = a.InsertJournalTransferMasukNasabah(ctx, b, b.TransType)
			if err != nil {
				return nil, err
			}
		} else { // value = 2 , beban biaya ditanggung koperasi
			//insert journal first
			err = a.InsertJournalTransferMasuk(ctx, b, b.TransType)
			if err != nil {
				return nil, err
			}
		}
	}

	TellerTransaction, err := a.TellerTransactionRepository.StoreBase(ctx, b)
	if err != nil {
		return nil, err
	}
	return TellerTransaction, nil
}
func (a *TellerTransactionUsecase) StoreCardlezBasev2(c context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	if b.TransType == 2 {
		//insert journal first
		err := a.InsertJournalSetorByDate2(ctx, b, b.TransType)
		if err != nil {
			return nil, err
		}
	} else if b.TransType == 3 {
		configKey := "Beban Biaya Transfer Masuk"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
		if err != nil {
			return nil, err
		}
		if appConfig.ConfigValue == "1" { //beban biaya ditanggung nasabah
			//insert journal first
			err = a.InsertJournalTransferMasukNasabah(ctx, b, b.TransType)
			if err != nil {
				return nil, err
			}
		} else { // value = 2 , beban biaya ditanggung koperasi
			//insert journal first
			err = a.InsertJournalTransferMasuk(ctx, b, b.TransType)
			if err != nil {
				return nil, err
			}
		}
	}

	TellerTransaction, err := a.TellerTransactionRepository.StoreBase(ctx, b)
	if err != nil {
		return nil, err
	}
	return TellerTransaction, nil
}
func (a *TellerTransactionUsecase) Store(c context.Context, customerAccountCode string, memberID string, amount float64, transType int32) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	// err, code := a.TellerTransactionRepository.GenerateCode(ctx, "MBR")
	// if err != nil {
	// 	return nil, err
	// }
	// accountrepository := accountrepository.NewSqlAccountRepository()
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	// journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	// journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	account, err := accountRepo.GetByCode(ctx, &customerAccountCode)
	if err != nil {
		return nil, err
	}
	accountDestination, err := accountRepo.GetByMemberID(ctx, &memberID)
	if err != nil {
		return nil, err
	}
	// memberrepository := memberrepository.NewSqlMemberRepository()
	//member, err := memberrepository.GetByID(ctx, memberID)

	var tellerTransaction models.TellerTransaction
	if transType == 1 {
		err, code := memberRepo.GenerateCode(ctx, "TTR")
		if err != nil {
			return nil, err
		}
		tellerTransaction.ID = guid.New().StringUpper()
		tellerTransaction.CompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
		tellerTransaction.AccountSourceID = account.ID
		tellerTransaction.Code = code
		tellerTransaction.AccountDestinationID = accountDestination.ID
		tellerTransaction.TransType = transType
		tellerTransaction.RecordStatus = 1
		tellerTransaction.TransAmount = amount
		tellerTransaction.TellerID = memberID
		rand.Seed(time.Now().UnixNano())
		randomNumber := service.RandInt(10000000, 99999999)
		randomNumber2 := service.RandInt(10000000, 99999999)
		tellerTransaction.TxnCodeDb = strconv.Itoa(randomNumber) + strconv.Itoa(randomNumber2)
		t := time.Now()
		tellerTransaction.TransactionDate = t.Format("2006-01-02 15:04:05")

	} else if transType == 2 {
		err, code := memberRepo.GenerateCode(ctx, "TST")
		if err != nil {
			return nil, err
		}
		tellerTransaction.ID = guid.New().StringUpper()
		tellerTransaction.CompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
		tellerTransaction.AccountSourceID = accountDestination.ID
		tellerTransaction.Code = code
		tellerTransaction.AccountDestinationID = account.ID
		tellerTransaction.TransType = transType
		tellerTransaction.RecordStatus = 2
		tellerTransaction.TransAmount = amount
		tellerTransaction.TellerID = memberID
		rand.Seed(time.Now().UnixNano())
		randomNumber := service.RandInt(10000000, 99999999)
		randomNumber2 := service.RandInt(10000000, 99999999)
		tellerTransaction.TxnCodeDb = strconv.Itoa(randomNumber) + strconv.Itoa(randomNumber2)
		t := time.Now()
		tellerTransaction.TransactionDate = t.Format("2006-01-02 15:04:05")

		//insert journal first
		err = a.InsertJournalSetor(ctx, &tellerTransaction, transType)
		if err != nil {
			return nil, err
		}
	} else if transType == 3 {
		err, code := memberRepo.GenerateCode(ctx, "TTS")
		if err != nil {
			return nil, err
		}
		tellerTransaction.ID = guid.New().StringUpper()
		tellerTransaction.CompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
		tellerTransaction.AccountSourceID = account.ID
		tellerTransaction.Code = code
		tellerTransaction.AccountDestinationID = "00000000-0000-0000-0000-000000000000"
		tellerTransaction.TransType = transType
		tellerTransaction.RecordStatus = 2
		tellerTransaction.TransAmount = amount
		tellerTransaction.TellerID = memberID
		rand.Seed(time.Now().UnixNano())
		randomNumber := service.RandInt(10000000, 99999999)
		randomNumber2 := service.RandInt(10000000, 99999999)
		tellerTransaction.TxnCodeDb = strconv.Itoa(randomNumber) + strconv.Itoa(randomNumber2)
		t := time.Now()
		tellerTransaction.TransactionDate = t.Format("2006-01-02 15:04:05")

		//insert journal first
		err = a.InsertJournalSetor(ctx, &tellerTransaction, transType)
		if err != nil {
			return nil, err
		}
	}

	TellerTransaction, err := a.TellerTransactionRepository.Store(ctx, &tellerTransaction)
	if err != nil {
		return nil, err
	}
	return TellerTransaction, nil
}

func (a *TellerTransactionUsecase) InsertJournal(ctx context.Context, b *models.TellerTransaction) error {
	journalID := guid.New().StringUpper()

	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)
	notes := "Penarikan Dana ke Agen " + member.MemberName.String
	codeJournalMapping := "T0013"
	keyword := "COADebetTarikTunai"
	keyword2 := "COACreditTarikTunai"
	t := time.Now()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      t.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           b.TransAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: b.AccountSourceID,
	}
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: b.AccountDestinationID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}

	//jurnal biaya
	biayaAmount := 0.0
	biayaAgen := 0.0
	biayaInvelli := 0.0
	biayaPendapatan := 0.0
	journalmapping2, err := journalMappingRepo.GetByCode(ctx, "T0045")
	keywordBiayaMember := "COADebetBiayaSetorTarikTunai"
	keyword5 := "COACreditAgenSetorTarikTunai"
	keyword6 := "COACreditInvelliSetorTarikTunai"
	keyword7 := "COACreditPdptSetorTarikTunai"
	//debit rekening member
	journalDetailDebitIDBiayaMember := guid.New().StringUpper()
	journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
	if err != nil {
		return err
	}
	//rekening agen
	journalDetailCreditID5 := guid.New().StringUpper()
	journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
	if err != nil {
		return err
	}
	if journalMappingDetail5.DefaultValue != "" {
		biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaAgen
	}
	//rekening invelli
	journalDetailCreditID6 := guid.New().StringUpper()
	journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
	if err != nil {
		return err
	}
	if journalMappingDetail6.DefaultValue != "" {
		biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaInvelli
	}
	//pendapatan Setor
	journalDetailCreditID7 := guid.New().StringUpper()
	journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
	if err != nil {
		return err
	}
	if journalMappingDetail7.DefaultValue != "" {
		biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaPendapatan
	}
	if biayaAmount > 0 {
		noteBiaya := "Biaya Penarikan Tunai di Agen " + member.MemberName.String
		journalID2 := guid.New().StringUpper()
		journal2 := models.Journal{
			ID:               journalID2,
			TransactionID:    b.ID,
			PostingDate:      t.Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(noteBiaya),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping2.ID,
		}

		//jurnal detail rekening member
		COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitBiayaMember := models.JournalDetail{
			ID:              journalDetailDebitIDBiayaMember,
			JournalID:       journal2.ID,
			COAID:           COABiayaMember.ID,
			Debit:           biayaAmount,
			Credit:          0,
			Notes:           noteBiaya,
			ReferenceNumber: b.AccountSourceID,
		}
		//jurnal detail agen
		COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit5 := models.JournalDetail{
			ID:              journalDetailCreditID5,
			JournalID:       journal2.ID,
			COAID:           COA5.ID,
			Debit:           0,
			Credit:          biayaAgen,
			Notes:           noteBiaya,
			ReferenceNumber: b.AccountDestinationID,
		}
		//jurnal detail invelli
		configKeyInvelli := "AccountID Invelli"
		appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
		if err != nil {
			return err
		}
		COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit6 := models.JournalDetail{
			ID:              journalDetailCreditID6,
			JournalID:       journal2.ID,
			COAID:           COA6.ID,
			Debit:           0,
			Credit:          biayaInvelli,
			Notes:           noteBiaya,
			ReferenceNumber: appConfigInvelli.ConfigValue,
		}
		//jurnal detail pendapatan
		COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit7 := models.JournalDetail{
			ID:        journalDetailCreditID7,
			JournalID: journal2.ID,
			COAID:     COA7.ID,
			Debit:     0,
			Credit:    biayaPendapatan,
			Notes:     noteBiaya,
			//ReferenceNumber: "",
		}
		journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
		if biayaAgen > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
		}
		if biayaInvelli > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
		}
		if biayaPendapatan > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
		}
		_, err = JournalUsecase.Store(ctx, &journal2)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalByDate(ctx context.Context, b *models.TellerTransaction) error {
	journalID := guid.New().StringUpper()

	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)
	notes := "Penarikan Dana ke Agen " + member.MemberName.String
	codeJournalMapping := "T0013"
	keyword := "COADebetTarikTunai"
	keyword2 := "COACreditTarikTunai"
	//t := time.Now()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate, //t.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           b.TransAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: b.AccountSourceID,
	}
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: b.AccountDestinationID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	//jurnal biaya
	biayaAmount := 0.0
	biayaAgen := 0.0
	biayaInvelli := 0.0
	biayaPendapatan := 0.0
	journalmapping2, err := journalMappingRepo.GetByCode(ctx, "T0045")
	keywordBiayaMember := "COADebetBiayaSetorTarikTunai"
	keyword5 := "COACreditAgenSetorTarikTunai"
	keyword6 := "COACreditInvelliSetorTarikTunai"
	keyword7 := "COACreditPdptSetorTarikTunai"
	//debit rekening member
	journalDetailDebitIDBiayaMember := guid.New().StringUpper()
	journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
	if err != nil {
		return err
	}
	//rekening agen
	journalDetailCreditID5 := guid.New().StringUpper()
	journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
	if err != nil {
		return err
	}
	if journalMappingDetail5.DefaultValue != "" {
		biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaAgen
	}
	//rekening invelli
	journalDetailCreditID6 := guid.New().StringUpper()
	journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
	if err != nil {
		return err
	}
	if journalMappingDetail6.DefaultValue != "" {
		biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaInvelli
	}
	//pendapatan Setor
	journalDetailCreditID7 := guid.New().StringUpper()
	journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
	if err != nil {
		return err
	}
	if journalMappingDetail7.DefaultValue != "" {
		biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaPendapatan
	}
	if biayaAmount > 0 {
		noteBiaya := "Biaya Penarikan Tunai di Agen " + member.MemberName.String
		journalID2 := guid.New().StringUpper()
		journal2 := models.Journal{
			ID:               journalID2,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate, //t.Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(noteBiaya),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping2.ID,
		}

		//jurnal detail rekening member
		COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitBiayaMember := models.JournalDetail{
			ID:              journalDetailDebitIDBiayaMember,
			JournalID:       journal2.ID,
			COAID:           COABiayaMember.ID,
			Debit:           biayaAmount,
			Credit:          0,
			Notes:           noteBiaya,
			ReferenceNumber: b.AccountSourceID,
		}
		//jurnal detail agen
		COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit5 := models.JournalDetail{
			ID:              journalDetailCreditID5,
			JournalID:       journal2.ID,
			COAID:           COA5.ID,
			Debit:           0,
			Credit:          biayaAgen,
			Notes:           noteBiaya,
			ReferenceNumber: b.AccountDestinationID,
		}
		//jurnal detail invelli
		configKeyInvelli := "AccountID Invelli"
		appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
		if err != nil {
			return err
		}
		COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit6 := models.JournalDetail{
			ID:              journalDetailCreditID6,
			JournalID:       journal2.ID,
			COAID:           COA6.ID,
			Debit:           0,
			Credit:          biayaInvelli,
			Notes:           noteBiaya,
			ReferenceNumber: appConfigInvelli.ConfigValue,
		}
		//jurnal detail pendapatan
		COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit7 := models.JournalDetail{
			ID:        journalDetailCreditID7,
			JournalID: journal2.ID,
			COAID:     COA7.ID,
			Debit:     0,
			Credit:    biayaPendapatan,
			Notes:     noteBiaya,
			//ReferenceNumber: "",
		}
		journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
		if biayaAgen > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
		}
		if biayaInvelli > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
		}
		if biayaPendapatan > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
		}
		_, err = JournalUsecase.StoreBase(ctx, &journal2)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalByDate2(ctx context.Context, b *models.TellerTransaction) error {
	journalID := guid.New().StringUpper()

	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)
	tipemember := "Agen"
	if member.MemberType == 2 {
		tipemember = "Marketing"
	}
	notes := "Penarikan Dana ke " + tipemember + " " + member.MemberName.String
	codeJournalMapping := "T0013"
	keyword := "COADebetTarikTunai"
	keyword2 := "COACreditTarikTunai"
	//t := time.Now()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate, //t.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           b.TransAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: b.AccountSourceID,
	}
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: b.AccountDestinationID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	//jurnal biaya
	biayaAmount := 0.0
	biayaAgen := 0.0
	biayaInvelli := 0.0
	biayaPendapatan := 0.0
	keywordBiayaMember := "COADebetBiayaTarikTunai"
	keyword5 := "COACreditAgenTarikTunai"
	keyword6 := "COACreditInvelliTarikTunai"
	keyword7 := "COACreditPdptTarikTunai"
	//journalmap komisi agen
	codeJournalMappingKomisi := "T0048"
	//cek apakah marketing
	if member.MemberType == 2 {
		codeJournalMappingKomisi = "T0050"
	}
	journalmapping2, err := journalMappingRepo.GetByCode(ctx, codeJournalMappingKomisi)
	//debit rekening member
	journalDetailDebitIDBiayaMember := guid.New().StringUpper()
	journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
	if err != nil {
		return err
	}
	//rekening agen
	journalDetailCreditID5 := guid.New().StringUpper()
	journalMappingDetail5Code := ""
	if member.MemberType == 9 {
		journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
		if err != nil {
			return err
		}
		journalMappingDetail5Code = journalMappingDetail5.Code.String
		if journalMappingDetail5.DefaultValue != "" {
			biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaAgen
		}
	}
	//rekening invelli
	journalDetailCreditID6 := guid.New().StringUpper()
	journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
	if err != nil {
		return err
	}
	if journalMappingDetail6.DefaultValue != "" {
		biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaInvelli
	}
	//pendapatan Setor
	journalDetailCreditID7 := guid.New().StringUpper()
	journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
	if err != nil {
		return err
	}
	if journalMappingDetail7.DefaultValue != "" {
		biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
		biayaAmount = biayaAmount + biayaPendapatan
	}
	if biayaAmount > 0 {
		noteBiaya := "Biaya Penarikan Tunai di Agen " + member.MemberName.String
		journalID2 := guid.New().StringUpper()
		journal2 := models.Journal{
			ID:               journalID2,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate, //t.Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(noteBiaya),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping2.ID,
		}

		//jurnal detail rekening member
		COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitBiayaMember := models.JournalDetail{
			ID:              journalDetailDebitIDBiayaMember,
			JournalID:       journal2.ID,
			COAID:           COABiayaMember.ID,
			Debit:           biayaAmount,
			Credit:          0,
			Notes:           noteBiaya,
			ReferenceNumber: b.AccountSourceID,
		}

		//jurnal detail invelli
		configKeyInvelli := "AccountID Invelli"
		appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
		if err != nil {
			return err
		}
		COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit6 := models.JournalDetail{
			ID:              journalDetailCreditID6,
			JournalID:       journal2.ID,
			COAID:           COA6.ID,
			Debit:           0,
			Credit:          biayaInvelli,
			Notes:           noteBiaya,
			ReferenceNumber: appConfigInvelli.ConfigValue,
		}
		//jurnal detail pendapatan
		COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit7 := models.JournalDetail{
			ID:        journalDetailCreditID7,
			JournalID: journal2.ID,
			COAID:     COA7.ID,
			Debit:     0,
			Credit:    biayaPendapatan,
			Notes:     noteBiaya,
			//ReferenceNumber: "",
		}
		journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
		if biayaAgen > 0 {
			//jurnal detail agen
			COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5Code)
			if err != nil {
				return err
			}
			journalDetailCredit5 := models.JournalDetail{
				ID:              journalDetailCreditID5,
				JournalID:       journal2.ID,
				COAID:           COA5.ID,
				Debit:           0,
				Credit:          biayaAgen,
				Notes:           noteBiaya,
				ReferenceNumber: b.AccountDestinationID,
			}
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
		}
		if biayaInvelli > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
		}
		if biayaPendapatan > 0 {
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
		}
		_, err = JournalUsecase.StoreBase(ctx, &journal2)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalSetor(ctx context.Context, b *models.TellerTransaction, transType int32) error {
	journalID := guid.New().StringUpper()

	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)

	notes := "Setoran Tunai di Agen " + member.MemberName.String
	codeJournalMapping := "T0019"
	keyword := "COADebetSetorTunai"
	keyword2 := "COACreditSetorTunai"
	keyword3 := "COADebetSaldoTaxTopUp"
	keyword4 := "COACreditSaldoTaxTopUp"
	referrenceNumber := b.AccountDestinationID
	if transType == 3 {
		referrenceNumber = b.AccountSourceID
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		notes = "Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName
		codeJournalMapping = "T0009"
		keyword = "COADebetSaldoTopUp"
		keyword2 = "COACreditSaldoTopUp"
	}

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:        journalDetailDebitID,
		JournalID: journal.ID,
		COAID:     COA.ID,
		Debit:     b.TransAmount,
		Credit:    0,
		Notes:     notes,
	}

	if transType == 2 {
		//setoran tunai ke account agen
		journalDetailDebit.ReferenceNumber = b.AccountSourceID
	}

	//rekening member
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: referrenceNumber,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	if transType == 3 {
		configKey := "Biaya Transfer Masuk"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
		if err != nil {
			return err
		}
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalDetailDebitID3 := guid.New().StringUpper()
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitTax := models.JournalDetail{
			ID:        journalDetailDebitID3,
			JournalID: journal.ID,
			COAID:     COA3.ID,
			Debit:     FeeConvert,
			Credit:    0,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journalDetailCreditID4 := guid.New().StringUpper()
		journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
		if err != nil {
			return err
		}
		journalDetailCreditTax := models.JournalDetail{
			ID:        journalDetailCreditID4,
			JournalID: journal.ID,
			COAID:     COA4.ID,
			Debit:     0,
			Credit:    FeeConvert,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitTax)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditTax)
	}

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}

	if transType == 2 {
		//jurnal biaya
		biayaAmount := 0.0
		biayaAgen := 0.0
		biayaInvelli := 0.0
		biayaPendapatan := 0.0
		journalmapping2, err := journalMappingRepo.GetByCode(ctx, "T0045")
		keywordBiayaMember := "COADebetBiayaSetorTarikTunai"
		keyword5 := "COACreditAgenSetorTarikTunai"
		keyword6 := "COACreditInvelliSetorTarikTunai"
		keyword7 := "COACreditPdptSetorTarikTunai"
		//debit rekening member
		journalDetailDebitIDBiayaMember := guid.New().StringUpper()
		journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
		if err != nil {
			return err
		}
		//rekening agen
		journalDetailCreditID5 := guid.New().StringUpper()
		journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
		if err != nil {
			return err
		}
		if journalMappingDetail5.DefaultValue != "" {
			biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaAgen
		}
		//rekening invelli
		journalDetailCreditID6 := guid.New().StringUpper()
		journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
		if err != nil {
			return err
		}
		if journalMappingDetail6.DefaultValue != "" {
			biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaInvelli
		}
		//pendapatan Setor
		journalDetailCreditID7 := guid.New().StringUpper()
		journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
		if err != nil {
			return err
		}
		if journalMappingDetail7.DefaultValue != "" {
			biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaPendapatan
		}
		if biayaAmount > 0 {
			noteBiaya := "Biaya Setoran Tunai di Agen " + member.MemberName.String
			journalID2 := guid.New().StringUpper()
			journal2 := models.Journal{
				ID:               journalID2,
				TransactionID:    b.ID,
				PostingDate:      b.TransactionDate,
				Description:      service.NewNullString(noteBiaya),
				Code:             service.NewNullString(b.Code),
				JournalMappingID: journalmapping2.ID,
			}

			//jurnal detail rekening member
			COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
			if err != nil {
				return err
			}
			journalDetailDebitBiayaMember := models.JournalDetail{
				ID:              journalDetailDebitIDBiayaMember,
				JournalID:       journal2.ID,
				COAID:           COABiayaMember.ID,
				Debit:           biayaAmount,
				Credit:          0,
				Notes:           noteBiaya,
				ReferenceNumber: referrenceNumber,
			}
			//jurnal detail agen
			COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit5 := models.JournalDetail{
				ID:              journalDetailCreditID5,
				JournalID:       journal2.ID,
				COAID:           COA5.ID,
				Debit:           0,
				Credit:          biayaAgen,
				Notes:           noteBiaya,
				ReferenceNumber: b.AccountSourceID,
			}
			//jurnal detail invelli
			configKeyInvelli := "AccountID Invelli"
			appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
			if err != nil {
				return err
			}
			COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit6 := models.JournalDetail{
				ID:              journalDetailCreditID6,
				JournalID:       journal2.ID,
				COAID:           COA6.ID,
				Debit:           0,
				Credit:          biayaInvelli,
				Notes:           noteBiaya,
				ReferenceNumber: appConfigInvelli.ConfigValue,
			}
			//jurnal detail pendapatan
			COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit7 := models.JournalDetail{
				ID:        journalDetailCreditID7,
				JournalID: journal2.ID,
				COAID:     COA7.ID,
				Debit:     0,
				Credit:    biayaPendapatan,
				Notes:     noteBiaya,
				//ReferenceNumber: "",
			}
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
			if biayaAgen > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
			}
			if biayaInvelli > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
			}
			if biayaPendapatan > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
			}
			_, err = JournalUsecase.Store(ctx, &journal2)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalSetorByDate(ctx context.Context, b *models.TellerTransaction, transType int32) error {
	journalID := guid.New().StringUpper()

	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)

	notes := "Setoran Tunai di Agen " + member.MemberName.String
	codeJournalMapping := "T0019"
	keyword := "COADebetSetorTunai"
	keyword2 := "COACreditSetorTunai"
	keyword3 := "COADebetSaldoTaxTopUp"
	keyword4 := "COACreditSaldoTaxTopUp"
	referrenceNumber := b.AccountDestinationID
	if transType == 3 {
		referrenceNumber = b.AccountSourceID
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		notes = "Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName
		codeJournalMapping = "T0009"
		keyword = "COADebetSaldoTopUp"
		keyword2 = "COACreditSaldoTopUp"
	}

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:        journalDetailDebitID,
		JournalID: journal.ID,
		COAID:     COA.ID,
		Debit:     b.TransAmount,
		Credit:    0,
		Notes:     notes,
	}

	if transType == 2 {
		//setoran tunai ke account agen
		journalDetailDebit.ReferenceNumber = b.AccountSourceID
	}

	//rekening member
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: referrenceNumber,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	if transType == 3 {
		configKey := "Biaya Transfer Masuk"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
		if err != nil {
			return err
		}
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalDetailDebitID3 := guid.New().StringUpper()
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitTax := models.JournalDetail{
			ID:        journalDetailDebitID3,
			JournalID: journal.ID,
			COAID:     COA3.ID,
			Debit:     FeeConvert,
			Credit:    0,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journalDetailCreditID4 := guid.New().StringUpper()
		journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
		if err != nil {
			return err
		}
		journalDetailCreditTax := models.JournalDetail{
			ID:        journalDetailCreditID4,
			JournalID: journal.ID,
			COAID:     COA4.ID,
			Debit:     0,
			Credit:    FeeConvert,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitTax)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditTax)
	}

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	if transType == 2 {
		//jurnal biaya
		biayaAmount := 0.0
		biayaAgen := 0.0
		biayaInvelli := 0.0
		biayaPendapatan := 0.0
		journalmapping2, err := journalMappingRepo.GetByCode(ctx, "T0045")
		keywordBiayaMember := "COADebetBiayaSetorTarikTunai"
		keyword5 := "COACreditAgenSetorTarikTunai"
		keyword6 := "COACreditInvelliSetorTarikTunai"
		keyword7 := "COACreditPdptSetorTarikTunai"
		//debit rekening member
		journalDetailDebitIDBiayaMember := guid.New().StringUpper()
		journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
		if err != nil {
			return err
		}
		//rekening agen
		journalDetailCreditID5 := guid.New().StringUpper()
		journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
		if err != nil {
			return err
		}
		if journalMappingDetail5.DefaultValue != "" {
			biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaAgen
		}
		//rekening invelli
		journalDetailCreditID6 := guid.New().StringUpper()
		journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
		if err != nil {
			return err
		}
		if journalMappingDetail6.DefaultValue != "" {
			biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaInvelli
		}
		//pendapatan Setor
		journalDetailCreditID7 := guid.New().StringUpper()
		journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
		if err != nil {
			return err
		}
		if journalMappingDetail7.DefaultValue != "" {
			biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaPendapatan
		}
		if biayaAmount > 0 {
			noteBiaya := "Biaya Setoran Tunai di Agen " + member.MemberName.String
			journalID2 := guid.New().StringUpper()
			journal2 := models.Journal{
				ID:               journalID2,
				TransactionID:    b.ID,
				PostingDate:      b.TransactionDate,
				Description:      service.NewNullString(noteBiaya),
				Code:             service.NewNullString(b.Code),
				JournalMappingID: journalmapping2.ID,
			}

			//jurnal detail rekening member
			COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
			if err != nil {
				return err
			}
			journalDetailDebitBiayaMember := models.JournalDetail{
				ID:              journalDetailDebitIDBiayaMember,
				JournalID:       journal2.ID,
				COAID:           COABiayaMember.ID,
				Debit:           biayaAmount,
				Credit:          0,
				Notes:           noteBiaya,
				ReferenceNumber: referrenceNumber,
			}
			//jurnal detail agen
			COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit5 := models.JournalDetail{
				ID:              journalDetailCreditID5,
				JournalID:       journal2.ID,
				COAID:           COA5.ID,
				Debit:           0,
				Credit:          biayaAgen,
				Notes:           noteBiaya,
				ReferenceNumber: b.AccountSourceID,
			}
			//jurnal detail invelli
			configKeyInvelli := "AccountID Invelli"
			appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
			if err != nil {
				return err
			}
			COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit6 := models.JournalDetail{
				ID:              journalDetailCreditID6,
				JournalID:       journal2.ID,
				COAID:           COA6.ID,
				Debit:           0,
				Credit:          biayaInvelli,
				Notes:           noteBiaya,
				ReferenceNumber: appConfigInvelli.ConfigValue,
			}
			//jurnal detail pendapatan
			COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit7 := models.JournalDetail{
				ID:        journalDetailCreditID7,
				JournalID: journal2.ID,
				COAID:     COA7.ID,
				Debit:     0,
				Credit:    biayaPendapatan,
				Notes:     noteBiaya,
				//ReferenceNumber: "",
			}
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
			if biayaAgen > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
			}
			if biayaInvelli > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
			}
			if biayaPendapatan > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
			}
			_, err = JournalUsecase.StoreBase(ctx, &journal2)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalSetorByDate2(ctx context.Context, b *models.TellerTransaction, transType int32) error {
	journalID := guid.New().StringUpper()

	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	// memberrepository := memberrepository.NewSqlMemberRepository()
	member, err := memberRepo.GetByID(ctx, &b.TellerID)
	tipemember := "Agen"
	if member.MemberType == 2 {
		tipemember = "Marketing"
	}
	notes := "Setoran Tunai di " + tipemember + " " + member.MemberName.String
	codeJournalMapping := "T0019"
	keyword := "COADebetSetorTunai"
	keyword2 := "COACreditSetorTunai"
	keyword3 := "COADebetSaldoTaxTopUp"
	keyword4 := "COACreditSaldoTaxTopUp"
	referrenceNumber := b.AccountDestinationID
	if transType == 3 {
		referrenceNumber = b.AccountSourceID
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		notes = "Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName
		codeJournalMapping = "T0009"
		keyword = "COADebetSaldoTopUp"
		keyword2 = "COACreditSaldoTopUp"
	}

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:        journalDetailDebitID,
		JournalID: journal.ID,
		COAID:     COA.ID,
		Debit:     b.TransAmount,
		Credit:    0,
		Notes:     notes,
	}

	if transType == 2 {
		//setoran tunai ke account agen
		journalDetailDebit.ReferenceNumber = b.AccountSourceID
	}

	//rekening member
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: referrenceNumber,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	if transType == 3 {
		configKey := "Biaya Transfer Masuk"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
		if err != nil {
			return err
		}
		accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
		CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalDetailDebitID3 := guid.New().StringUpper()
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitTax := models.JournalDetail{
			ID:        journalDetailDebitID3,
			JournalID: journal.ID,
			COAID:     COA3.ID,
			Debit:     FeeConvert,
			Credit:    0,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journalDetailCreditID4 := guid.New().StringUpper()
		journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
		if err != nil {
			return err
		}
		journalDetailCreditTax := models.JournalDetail{
			ID:        journalDetailCreditID4,
			JournalID: journal.ID,
			COAID:     COA4.ID,
			Debit:     0,
			Credit:    FeeConvert,
			Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitTax)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditTax)
	}

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	if transType == 2 {
		//jurnal biaya
		biayaAmount := 0.0
		biayaAgen := 0.0
		biayaInvelli := 0.0
		biayaPendapatan := 0.0
		keywordBiayaMember := "COADebetBiayaSetorTunai"
		keyword5 := "COACreditAgenSetorTunai"
		keyword6 := "COACreditInvelliSetorTunai"
		keyword7 := "COACreditPdptSetorTunai"
		//journalmap komisi agen
		codeJournalMappingKomisi := "T0045"
		//cek apakah marketing
		if member.MemberType == 2 {
			codeJournalMappingKomisi = "T0049"
		}
		journalmapping2, err := journalMappingRepo.GetByCode(ctx, codeJournalMappingKomisi)

		//debit rekening member
		journalDetailDebitIDBiayaMember := guid.New().StringUpper()
		journalMappingDetailBiayaMember, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "1", &keywordBiayaMember)
		if err != nil {
			return err
		}
		//rekening agen
		journalDetailCreditID5 := guid.New().StringUpper()
		journalMappingDetail5Code := ""
		if member.MemberType == 9 {
			journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword5)
			if err != nil {
				return err
			}
			journalMappingDetail5Code = journalMappingDetail5.Code.String
			if journalMappingDetail5.DefaultValue != "" {
				biayaAgen, _ = strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
				biayaAmount = biayaAmount + biayaAgen
			}
		}
		//rekening invelli
		journalDetailCreditID6 := guid.New().StringUpper()
		journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword6)
		if err != nil {
			return err
		}
		if journalMappingDetail6.DefaultValue != "" {
			biayaInvelli, _ = strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaInvelli
		}
		//pendapatan Setor
		journalDetailCreditID7 := guid.New().StringUpper()
		journalMappingDetail7, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping2.ID, "2", &keyword7)
		if err != nil {
			return err
		}
		if journalMappingDetail7.DefaultValue != "" {
			biayaPendapatan, _ = strconv.ParseFloat(journalMappingDetail7.DefaultValue, 64)
			biayaAmount = biayaAmount + biayaPendapatan
		}
		if biayaAmount > 0 {
			noteBiaya := "Biaya Setoran Tunai di Agen " + member.MemberName.String
			journalID2 := guid.New().StringUpper()
			journal2 := models.Journal{
				ID:               journalID2,
				TransactionID:    b.ID,
				PostingDate:      b.TransactionDate,
				Description:      service.NewNullString(noteBiaya),
				Code:             service.NewNullString(b.Code),
				JournalMappingID: journalmapping2.ID,
			}

			//jurnal detail rekening member
			COABiayaMember, err := coaRepo.GetByCode(ctx, &journalMappingDetailBiayaMember.Code.String)
			if err != nil {
				return err
			}
			journalDetailDebitBiayaMember := models.JournalDetail{
				ID:              journalDetailDebitIDBiayaMember,
				JournalID:       journal2.ID,
				COAID:           COABiayaMember.ID,
				Debit:           biayaAmount,
				Credit:          0,
				Notes:           noteBiaya,
				ReferenceNumber: referrenceNumber,
			}

			//jurnal detail invelli
			configKeyInvelli := "AccountID Invelli"
			appConfigInvelli, err := appConfigRepo.GetByConfigKey(ctx, &configKeyInvelli)
			if err != nil {
				return err
			}
			COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit6 := models.JournalDetail{
				ID:              journalDetailCreditID6,
				JournalID:       journal2.ID,
				COAID:           COA6.ID,
				Debit:           0,
				Credit:          biayaInvelli,
				Notes:           noteBiaya,
				ReferenceNumber: appConfigInvelli.ConfigValue,
			}
			//jurnal detail pendapatan
			COA7, err := coaRepo.GetByCode(ctx, &journalMappingDetail7.Code.String)
			if err != nil {
				return err
			}
			journalDetailCredit7 := models.JournalDetail{
				ID:        journalDetailCreditID7,
				JournalID: journal2.ID,
				COAID:     COA7.ID,
				Debit:     0,
				Credit:    biayaPendapatan,
				Notes:     noteBiaya,
				//ReferenceNumber: "",
			}
			journal2.JournalDetails = append(journal2.JournalDetails, journalDetailDebitBiayaMember)
			if biayaAgen > 0 {
				//jurnal detail agen
				COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5Code)
				if err != nil {
					return err
				}
				journalDetailCredit5 := models.JournalDetail{
					ID:              journalDetailCreditID5,
					JournalID:       journal2.ID,
					COAID:           COA5.ID,
					Debit:           0,
					Credit:          biayaAgen,
					Notes:           noteBiaya,
					ReferenceNumber: b.AccountSourceID,
				}
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit5)
			}
			if biayaInvelli > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit6)
			}
			if biayaPendapatan > 0 {
				journal2.JournalDetails = append(journal2.JournalDetails, journalDetailCredit7)
			}
			_, err = JournalUsecase.StoreBase(ctx, &journal2)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
func (a *TellerTransactionUsecase) InsertJournalTransferMasukNasabah(ctx context.Context, b *models.TellerTransaction, transType int32) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	keyword3 := "COADebetSaldoTaxTopUp"
	keyword4 := "COACreditSaldoTaxTopUp"
	referrenceNumber := b.AccountSourceID
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
	if err != nil {
		return err
	}
	notes := "Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName
	codeJournalMapping := "T0009"
	keyword := "COADebetSaldoTopUp"
	keyword2 := "COACreditSaldoTopUp"

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           b.TransAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: "",
	}

	//rekening member
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: referrenceNumber,
	}
	configKey := "Biaya Transfer Masuk"
	appConfig, err := appConfigRepo.GetByConfigKey(ctx, &configKey)
	if err != nil {
		return err
	}

	FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
	journalDetailDebitID3 := guid.New().StringUpper()
	journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword3)
	if err != nil {
		return err
	}
	COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebitTax := models.JournalDetail{
		ID:              journalDetailDebitID3,
		JournalID:       journal.ID,
		COAID:           COA3.ID,
		Debit:           FeeConvert,
		Credit:          0,
		Notes:           "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		ReferenceNumber: referrenceNumber,
	}
	journalDetailCreditID4 := guid.New().StringUpper()
	journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword4)
	if err != nil {
		return err
	}
	COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
	if err != nil {
		return err
	}
	biayaGiro, _ := strconv.ParseFloat(journalMappingDetail4.DefaultValue, 64)
	journalDetailCreditTax := models.JournalDetail{
		ID:        journalDetailCreditID4,
		JournalID: journal.ID,
		COAID:     COA4.ID,
		Debit:     0,
		Credit:    biayaGiro,
		Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
	}
	keyword5 := "COACreditPartner"
	keyword6 := "COACreditInvelli"
	journalDetailDebitID5 := guid.New().StringUpper()
	journalMappingDetail5, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword5)
	if err != nil {
		return err
	}
	COA5, err := coaRepo.GetByCode(ctx, &journalMappingDetail5.Code.String)
	if err != nil {
		return err
	}
	pdptPartner, _ := strconv.ParseFloat(journalMappingDetail5.DefaultValue, 64)
	journalCreditPartner := models.JournalDetail{
		ID:              journalDetailDebitID5,
		JournalID:       journal.ID,
		COAID:           COA5.ID,
		Debit:           0,
		Credit:          pdptPartner,
		Notes:           "Pdpt dari Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		ReferenceNumber: "",
	}
	journalDetailCreditID6 := guid.New().StringUpper()
	journalMappingDetail6, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword6)
	if err != nil {
		return err
	}
	COA6, err := coaRepo.GetByCode(ctx, &journalMappingDetail6.Code.String)
	if err != nil {
		return err
	}
	//get AccountID Invelli
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := appConfigRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	pdptInvelli, _ := strconv.ParseFloat(journalMappingDetail6.DefaultValue, 64)
	journalCreditInvelli := models.JournalDetail{
		ID:              journalDetailCreditID6,
		JournalID:       journal.ID,
		COAID:           COA6.ID,
		Debit:           0,
		Credit:          pdptInvelli,
		Notes:           "Pdpt dari Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
		ReferenceNumber: accIDInvelli.ConfigValue,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitTax)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditTax)
	journal.JournalDetails = append(journal.JournalDetails, journalCreditPartner)
	journal.JournalDetails = append(journal.JournalDetails, journalCreditInvelli)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *TellerTransactionUsecase) InsertJournalTransferMasuk(ctx context.Context, b *models.TellerTransaction, transType int32) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	keyword3 := "COADebetBiayaTopUp"
	keyword4 := "COACreditBiayaTopUp"
	referrenceNumber := b.AccountSourceID
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	CreditAccount, err := accRepo.GetByID(ctx, &referrenceNumber)
	if err != nil {
		return err
	}
	notes := "Transfer masuk untuk " + CreditAccount.Code + " - " + CreditAccount.MemberName
	codeJournalMapping := "T0046"
	keyword := "COADebetTopUp"
	keyword2 := "COACreditTopUp"

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, codeJournalMapping)
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           b.TransAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: "",
	}

	//rekening member
	journalDetailCreditID := guid.New().StringUpper()
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: referrenceNumber,
	}

	journalDetailDebitID3 := guid.New().StringUpper()
	journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "1", &keyword3)
	if err != nil {
		return err
	}
	COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
	if err != nil {
		return err
	}
	biayaGiro, _ := strconv.ParseFloat(journalMappingDetail3.DefaultValue, 64)
	journalDetailDebitTax := models.JournalDetail{
		ID:        journalDetailDebitID3,
		JournalID: journal.ID,
		COAID:     COA3.ID,
		Debit:     biayaGiro,
		Credit:    0,
		Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
	}
	journalDetailCreditID4 := guid.New().StringUpper()
	journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword4)
	if err != nil {
		return err
	}
	COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
	if err != nil {
		return err
	}
	journalDetailCreditTax := models.JournalDetail{
		ID:        journalDetailCreditID4,
		JournalID: journal.ID,
		COAID:     COA4.ID,
		Debit:     0,
		Credit:    biayaGiro,
		Notes:     "Biaya Transfer Masuk " + CreditAccount.Code + " - " + CreditAccount.MemberName,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitTax)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditTax)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *TellerTransactionUsecase) Update(c context.Context, b *models.TellerTransaction) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	tellertransaction, err := a.TellerTransactionRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return tellertransaction, nil
}

func (a *TellerTransactionUsecase) CancelTransaction(c context.Context, id string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	configKeyMdw := "IsMiddleware"
	isMiddleware, err := appConfigRepo.GetByConfigKey(ctx, &configKeyMdw)
	if err != nil {
		return nil, err
	}
	key := "IsInvelliCore"
	isInvelliCore, err := appConfigRepo.GetByConfigKey(ctx, &key)
	if err != nil {
		return nil, err
	}
	transaction, err := a.TellerTransactionRepository.GetByID(ctx, &id)
	if err != nil {
		return nil, err
	}
	if isInvelliCore.ConfigValue == "1" && isMiddleware.ConfigValue == "1" { // jika middleware cek status account nya
		Account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
		if err != nil {
			return nil, err
		}
		if Account.RecordStatus != 2 && Account.IsPrimary == true { //jika recordstatus != 2, maka tidak bisa transaksi
			return nil, fmt.Errorf("Account belum aktif, silahkan hubungi Customer Service.")
		}
	}

	transaction.RecordStatus = 3
	TellerTransaction, err := a.TellerTransactionRepository.Update(ctx, transaction)
	if err != nil {
		return nil, err
	}
	// // accountrepository := accountrepository.NewSqlAccountRepository()
	// account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
	// if err != nil {s
	// 	return nil, err
	// }
	// //Insert To Journal
	// err = a.InsertJournalCancel(ctx, transaction, &account.ProductID)
	// if err != nil {
	// 	return nil, err
	// }
	return TellerTransaction, nil
}

func (a *TellerTransactionUsecase) CancelTransactionByRefNo(c context.Context, refNo string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	configKeyMdw := "IsMiddleware"
	isMiddleware, err := appConfigRepo.GetByConfigKey(ctx, &configKeyMdw)
	if err != nil {
		return nil, err
	}
	key := "IsInvelliCore"
	isInvelliCore, err := appConfigRepo.GetByConfigKey(ctx, &key)
	if err != nil {
		return nil, err
	}
	transaction, err := a.TellerTransactionRepository.GetByRefNo(ctx, &refNo)
	if err != nil {
		return nil, err
	}
	if isInvelliCore.ConfigValue == "1" && isMiddleware.ConfigValue == "1" { // jika middleware cek status account nya
		Account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
		if err != nil {
			return nil, err
		}
		if Account.RecordStatus != 2 && Account.IsPrimary == true { //jika recordstatus != 2, maka tidak bisa transaksi
			return nil, fmt.Errorf("Account belum aktif, silahkan hubungi Customer Service.")
		}
	}

	transaction.RecordStatus = 3
	TellerTransaction, err := a.TellerTransactionRepository.Update(ctx, transaction)
	if err != nil {
		return nil, err
	}
	// // accountrepository := accountrepository.NewSqlAccountRepository()
	// account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
	// if err != nil {s
	// 	return nil, err
	// }
	// //Insert To Journal
	// err = a.InsertJournalCancel(ctx, transaction, &account.ProductID)
	// if err != nil {
	// 	return nil, err
	// }
	return TellerTransaction, nil
}

func (a *TellerTransactionUsecase) AcceptTransaction(c context.Context, id string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	//accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	transaction, err := a.TellerTransactionRepository.GetByID(ctx, &id)
	if err != nil {
		return nil, err
	}
	//Insert To Journal
	err = a.InsertJournal(ctx, transaction)
	if err != nil {
		return nil, err
	}
	transaction.RecordStatus = 2
	TellerTransaction, err := a.TellerTransactionRepository.Update(ctx, transaction)
	if err != nil {
		return nil, err
	}
	// accountrepository := accountrepository.NewSqlAccountRepository()
	// account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
	// if err != nil {
	// 	return nil, err
	// }
	return TellerTransaction, nil
}

func (a *TellerTransactionUsecase) AcceptTransactionBase(c context.Context, id string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	//accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	transaction, err := a.TellerTransactionRepository.GetByID(ctx, &id)
	if err != nil {
		return nil, err
	}
	//Insert To Journal
	err = a.InsertJournalByDate(ctx, transaction)
	if err != nil {
		return nil, err
	}
	transaction.RecordStatus = 2
	TellerTransaction, err := a.TellerTransactionRepository.Update(ctx, transaction)
	if err != nil {
		return nil, err
	}
	// accountrepository := accountrepository.NewSqlAccountRepository()
	// account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
	// if err != nil {
	// 	return nil, err
	// }
	return TellerTransaction, nil
}
func (a *TellerTransactionUsecase) AcceptTransactionBasev2(c context.Context, id string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	configKeyMdw := "IsMiddleware"
	isMiddleware, err := appConfigRepo.GetByConfigKey(ctx, &configKeyMdw)
	if err != nil {
		return nil, err
	}
	key := "IsInvelliCore"
	isInvelliCore, err := appConfigRepo.GetByConfigKey(ctx, &key)
	if err != nil {
		return nil, err
	}
	transaction, err := a.TellerTransactionRepository.GetByID(ctx, &id)
	if err != nil {
		return nil, err
	}
	if isInvelliCore.ConfigValue == "1" && isMiddleware.ConfigValue == "1" { // jika middleware cek status account nya
		Account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
		if err != nil {
			return nil, err
		}
		if Account.RecordStatus != 2 && Account.IsPrimary == true { //jika recordstatus != 2, maka tidak bisa transaksi
			return nil, fmt.Errorf("Account belum aktif, silahkan hubungi Customer Service.")
		}
	}

	//Insert To Journal
	err = a.InsertJournalByDate2(ctx, transaction)
	if err != nil {
		return nil, err
	}
	transaction.RecordStatus = 2
	TellerTransaction, err := a.TellerTransactionRepository.Update(ctx, transaction)
	if err != nil {
		return nil, err
	}
	// accountrepository := accountrepository.NewSqlAccountRepository()
	// account, err := accountRepo.GetByID(ctx, &transaction.AccountSourceID)
	// if err != nil {
	// 	return nil, err
	// }
	return TellerTransaction, nil
}
func (a *TellerTransactionUsecase) InsertJournalCancel(ctx context.Context, b *models.TellerTransaction, productId *string) error {
	journalID := guid.New().StringUpper()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	member, err := memberRepo.GetByID(ctx, &b.TellerID)
	notes := "Pengembalian Penarikan tunai ke " + member.MemberName.String + " karena telah kardaluarsa / dibatalkan."
	t := time.Now()
	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0013")
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      t.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailID := guid.New().StringUpper()
	COA, err := a.TellerTransactionRepository.COAGetByProductID(ctx, productId)
	journalDetail := models.JournalDetail{
		ID:        journalDetailID,
		JournalID: journal.ID,
		COAID:     "42F22EF5-340F-4FE2-87CE-CAC8715ABC17",
		Debit:     b.TransAmount,
		Credit:    0,
		Notes:     notes,
		//ReferenceNumber: b.AccountSourceID,
	}
	journalDetailID2 := guid.New().StringUpper()
	//COA2, err := a.TellerTransactionRepository.COAGetByID(ctx, "42F22EF5-340F-4FE2-87CE-CAC8715ABC17")
	journalDetail2 := models.JournalDetail{
		ID:              journalDetailID2,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           0,
		Credit:          b.TransAmount,
		Notes:           notes,
		ReferenceNumber: b.AccountSourceID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetail)
	journal.JournalDetails = append(journal.JournalDetails, journalDetail2)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)

	journalID2 := guid.New().StringUpper()
	notes2 := "Pengembalian Biaya Penarikan Dana karena pembatalan."

	journalmapping2, err := journalMappingRepo.GetByCode(ctx, "T0014")
	journal2 := models.Journal{
		ID:               journalID2,
		TransactionID:    b.ID,
		PostingDate:      t.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString(notes2),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping2.ID,
	}
	journalDetailID3 := guid.New().StringUpper()
	COA2, err := a.TellerTransactionRepository.COAGetByProductID(ctx, productId)
	journalDetail3 := models.JournalDetail{
		ID:        journalDetailID3,
		JournalID: journal2.ID,
		COAID:     "70DAD419-0A3C-44E9-862D-63781ED6A87C",
		Debit:     1500,
		Credit:    0,
		Notes:     notes2,
		//ReferenceNumber: b.AccountSourceID,
	}
	journalDetailID4 := guid.New().StringUpper()
	//COA2, err := a.TellerTransactionRepository.COAGetByID(ctx, "42F22EF5-340F-4FE2-87CE-CAC8715ABC17")
	journalDetail4 := models.JournalDetail{
		ID:              journalDetailID4,
		JournalID:       journal2.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          1500,
		Notes:           notes2,
		ReferenceNumber: b.AccountSourceID,
	}
	journal2.JournalDetails = append(journal2.JournalDetails, journalDetail3)
	journal2.JournalDetails = append(journal2.JournalDetails, journalDetail4)

	//JournalUsecase := usecase.NewJournalUsecase()
	_, err = JournalUsecase.Store(ctx, &journal2)

	if err != nil {
		return err
	}
	return nil
}
func (a *TellerTransactionUsecase) GetByID(c context.Context, id *string) (*models.TellerTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	transaction, err := a.TellerTransactionRepository.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return transaction, nil
}
func (a *TellerTransactionUsecase) FetchSetorTarik(c context.Context, id string, isSettled bool, dateStart string, dateFinish string) (error, []*models.TellerTransaction) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	recordStatus := int32(2)
	if isSettled == true {
		recordStatus = 4
	}

	err, listTellerTransaction := a.TellerTransactionRepository.FetchTransType(ctx, id, 0, true, recordStatus, dateStart, dateFinish)
	if err != nil {
		return err, nil
	}
	return nil, listTellerTransaction
}
func (a *TellerTransactionUsecase) SettleSetorTarik(c context.Context, b []*models.TellerTransaction, reffNo string) ([]*models.TellerTransaction, error) {
	//TEST COMMENT
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	// memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	// reffNo := ""
	if len(b) > 0 {
		// err, reffNo := memberRepo.GenerateCode(ctx, "STLST")
		// if err != nil {
		// 	return nil, nil, err
		// }
		//Insert To Journal
		err := a.InsertJournalSettlementSetorTarik(ctx, b, reffNo)
		if err != nil {
			return nil, err
		}
		b, err = a.TellerTransactionRepository.SettleSetorTarik(ctx, b, reffNo, 4)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}

func (a *TellerTransactionUsecase) InsertJournalSettlementSetorTarik(ctx context.Context, b []*models.TellerTransaction, nomorTransaksiSettlement string) error {
	totalAmountSetor := 0.0
	totalAmountTarik := 0.0
	reffNo := nomorTransaksiSettlement

	for _, p := range b {
		if p.TransType == 1 {
			totalAmountTarik += p.TransAmount
		} else if p.TransType == 2 {
			totalAmountSetor += p.TransAmount
		}
		//totalFeeAmount += billPayment.PartnerAmount
		//totalInvelliAmount += billPayment.InvelliAmount
	}
	keyword := "COADebetSettleTarikTunai"
	keyword2 := "COACreditSettleTarikTunai"
	description := "Settlement Tarik Tunai"

	keyword3 := "COADebetSettleSetorTunai"
	keyword4 := "COACreditSettleSetorTunai"
	description2 := "Settlement Setor Tunai"

	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0020")
	if err != nil {
		return err
	}

	if totalAmountTarik != 0.0 {
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(description),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalAmountTarik,
			Credit:          0,
			Notes:           "Kewajiban Tarik Tunai - Settlement",
			ReferenceNumber: "",
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword2)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalAmountTarik,
			Notes:           "Kewajiban Tarik Tunai - Settlement",
			ReferenceNumber: "",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}
	if totalAmountSetor != 0.0 {
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(description2),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword3)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalAmountSetor,
			Credit:          0,
			Notes:           "Kewajiban Setor Tunai - Settlement",
			ReferenceNumber: "",
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalAmountSetor,
			Notes:           "Kewajiban Setor Tunai - Settlement",
			ReferenceNumber: "",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}
	return nil
}
