package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/informationclause"
	"gitlab.com/robertinc/cardlez-api/business-service/informationclause/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type InformationClauseUsecase struct {
	InformationClauseRepository informationclause.InformationClauseRepository
	contextTimeout              time.Duration
}

func NewInformationClauseUsecase() informationclause.InformationClauseUsecase {
	return &InformationClauseUsecase{
		InformationClauseRepository: repository.NewSqlInformationClauseRepository(),
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func NewInformationClauseUsecaseV2(informationClauseRepo informationclause.InformationClauseRepository) informationclause.InformationClauseUsecase {
	return &InformationClauseUsecase{
		InformationClauseRepository: informationClauseRepo,
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func (a *InformationClauseUsecase) Fetch(c context.Context) (error, []*models.InformationClause) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listInformationClause := a.InformationClauseRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listInformationClause
}
