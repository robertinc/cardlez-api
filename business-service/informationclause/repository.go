package informationclause

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type InformationClauseRepository interface {
	Fetch(ctx context.Context) (error, []*models.InformationClause)
}
