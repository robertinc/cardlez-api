package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/informationclause"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlInformationClauseRepository struct {
	Conn *sql.DB
}

func NewSqlInformationClauseRepository() informationclause.InformationClauseRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlInformationClauseRepository{conn}
}

func NewSqlInformationClauseRepositoryV2(Conn *sql.DB) informationclause.InformationClauseRepository {
	conn := Conn
	return &sqlInformationClauseRepository{conn}
}

func (sj *sqlInformationClauseRepository) Fetch(ctx context.Context) (error, []*models.InformationClause) {
	query := `
	SELECT 
	Cast(ID as varchar(36)) as ID,
	SequenceNo,
	Isnull(HeadInfo, '') as HeadInfo,
	Isnull(DetailInfo, '') as DetailInfo,
	Isnull(GroupInfo, '') as GroupInfo,
	RecordStatus,
	Isnull(UserInsert, '') as UserInsert,
	Isnull(DateInsert, '') as DateInsert,
	Isnull(UserUpdate, '') as UserUpdate,
	Isnull(DateUpdate, '') as DateUpdate
	FROM InformationClause order by SequenceNo`
	result := make([]*models.InformationClause, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.InformationClause)
		err = rows.Scan(
			&j.ID,
			&j.SequenceNo,
			&j.HeadInfo,
			&j.DetailInfo,
			&j.GroupInfo,
			&j.RecordStatus,
			&j.UserInsert,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
