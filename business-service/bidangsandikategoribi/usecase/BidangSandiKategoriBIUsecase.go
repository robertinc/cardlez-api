package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/bidangsandikategoribi"
	"gitlab.com/robertinc/cardlez-api/business-service/bidangsandikategoribi/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type BidangSandiKategoriBIUsecase struct {
	BidangSandiKategoriBIRepository bidangsandikategoribi.BidangSandiKategoriBIRepository
	contextTimeout                  time.Duration
}

func NewBidangSandiKategoriBIUsecase() bidangsandikategoribi.BidangSandiKategoriBIUsecase {
	return &BidangSandiKategoriBIUsecase{
		BidangSandiKategoriBIRepository: repository.NewSqlBidangSandiKategoriBIRepository(),
		contextTimeout:                  time.Second * time.Duration(models.Timeout()),
	}
}

func NewBidangSandiKategoriBIUsecaseV2(repo bidangsandikategoribi.BidangSandiKategoriBIRepository) bidangsandikategoribi.BidangSandiKategoriBIUsecase {
	return &BidangSandiKategoriBIUsecase{
		BidangSandiKategoriBIRepository: repo,
		contextTimeout:                  time.Second * time.Duration(models.Timeout()),
	}
}

func (a *BidangSandiKategoriBIUsecase) Fetch(c context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listBidangSandiKategoriBI := a.BidangSandiKategoriBIRepository.Fetch(ctx, BidangSandiID)
	if err != nil {
		return err, nil
	}
	return nil, listBidangSandiKategoriBI
}
func (a *BidangSandiKategoriBIUsecase) FetchView(c context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listBidangSandiKategoriBI := a.BidangSandiKategoriBIRepository.FetchView(ctx, BidangSandiID)
	if err != nil {
		return err, nil
	}
	return nil, listBidangSandiKategoriBI
}

func (a *BidangSandiKategoriBIUsecase) GetByBidangSandiKategoriID(c context.Context, BidangSandiKategoriBI *string) (error, *models.BidangSandiKategoriBI) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listBidangSandiKategoriBI := a.BidangSandiKategoriBIRepository.GetByBidangSandiKategoriID(ctx, BidangSandiKategoriBI)
	if err != nil {
		return err, nil
	}
	return nil, listBidangSandiKategoriBI
}

func (a *BidangSandiKategoriBIUsecase) GetByNoHeadKategori(ctx context.Context, NoHeadKategori *string) (error, *models.BidangSandiKategoriBI) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, BidangSandiKategoriBI := a.BidangSandiKategoriBIRepository.GetByNoHeadKategori(ctx, NoHeadKategori)
	if err != nil {
		return err, nil
	}
	return nil, BidangSandiKategoriBI
}
