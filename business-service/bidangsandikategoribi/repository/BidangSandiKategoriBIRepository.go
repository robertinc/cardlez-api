package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	"gitlab.com/robertinc/cardlez-api/business-service/bidangsandikategoribi"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlBidangSandiKategoriBIRepository struct {
	Conn *sql.DB
}

func NewSqlBidangSandiKategoriBIRepository() bidangsandikategoribi.BidangSandiKategoriBIRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlBidangSandiKategoriBIRepository{conn}
}

func NewSqlBidangSandiKategoriBIRepositoryV2(Conn *sql.DB) bidangsandikategoribi.BidangSandiKategoriBIRepository {
	conn := Conn
	return &sqlBidangSandiKategoriBIRepository{conn}
}
func (sj *sqlBidangSandiKategoriBIRepository) Fetch(ctx context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI) {
	queryCondition := ""
	if BidangSandiID != nil {
		queryCondition = " where BidangSandi_ID = @p0 "
	}
	query := `
		select 	
			isnull(cast(BidangSandiKategoriBI_ID as varchar(36)),'') as BidangSandiKategoriBI_ID,
			isnull(cast(BidangSandi_ID as varchar(36)),'') as BidangSandi_ID,
			isnull(No_Head_Kategori,'') as No_Head_Kategori,
			isnull(Kode_Kategori,'') as Kode_Kategori
		from [Master.BidangSandi.KategoriBI]
		` + queryCondition + `
		order by No_Head_Kategori
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", BidangSandiID),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.BidangSandiKategoriBI, 0)
	for rows.Next() {
		j := new(models.BidangSandiKategoriBI)
		err = rows.Scan(
			&j.BidangSandiKategoriBIID,
			&j.BidangSandiID,
			&j.NoHeadKategori,
			&j.KodeKategori,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlBidangSandiKategoriBIRepository) FetchView(ctx context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI) {
	queryCondition := ""
	if BidangSandiID != nil {
		queryCondition = " where BidangSandi_ID = @p0 "
	}
	query := `
		select 	
		isnull(cast(BidangSandiKategoriBI_ID as varchar(36)),'') as BidangSandiKategoriBI_ID,
		isnull(cast(BidangSandi_ID as varchar(36)),'') as BidangSandi_ID,
		isnull(No_Head_Kategori,'') as No_Head_Kategori,
		isnull(Kode_Kategori,'') as Kode_Kategori,
		isnull(BidangSandi_No,'') as BidangSandi_No,
		isnull(Bidang_Sandi,'') as Bidang_Sandi
		from VwMasterBidangSandiKategoriBI
		` + queryCondition + `
		order by No_Head_Kategori
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", BidangSandiID),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.BidangSandiKategoriBI, 0)
	for rows.Next() {
		j := new(models.BidangSandiKategoriBI)
		err = rows.Scan(
			&j.BidangSandiKategoriBIID,
			&j.BidangSandiID,
			&j.NoHeadKategori,
			&j.KodeKategori,
			&j.BidangSandiNo,
			&j.BidangSandi,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlBidangSandiKategoriBIRepository) GetByBidangSandiKategoriID(ctx context.Context, BidangSandiKategoriBI_ID *string) (error, *models.BidangSandiKategoriBI) {
	queryCondition := ""
	if BidangSandiKategoriBI_ID != nil {
		queryCondition = " where BidangSandiKategoriBI_ID = @p0 "
	}
	query := `
		select top(1)
		isnull(cast(BidangSandiKategoriBI_ID as varchar(36)),'') as BidangSandiKategoriBI_ID,
		isnull(cast(BidangSandi_ID as varchar(36)),'') as BidangSandi_ID,
		isnull(No_Head_Kategori,'') as No_Head_Kategori,
		isnull(Kode_Kategori,'') as Kode_Kategori,
		isnull(BidangSandi_No,'') as BidangSandi_No,
		isnull(Bidang_Sandi,'') as Bidang_Sandi
		from VwMasterBidangSandiKategoriBI
		` + queryCondition + `
		order by No_Head_Kategori
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("BidangSandiKategoriBI_ID", fmt.Sprintf("%v", BidangSandiKategoriBI_ID)),
	)
	j := new(models.BidangSandiKategoriBI)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", BidangSandiKategoriBI_ID),
	).Scan(
		&j.BidangSandiKategoriBIID,
		&j.BidangSandiID,
		&j.NoHeadKategori,
		&j.KodeKategori,
		&j.BidangSandiNo,
		&j.BidangSandi,
	)

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, j
}

func (sj *sqlBidangSandiKategoriBIRepository) GetByNoHeadKategori(ctx context.Context, NoHeadKategori *string) (error, *models.BidangSandiKategoriBI) {
	queryCondition := ""
	if NoHeadKategori != nil {
		queryCondition = " where No_Head_Kategori like @p0 "
	}
	query := `
		select top(1)
		isnull(cast(BidangSandiKategoriBI_ID as varchar(36)),'') as BidangSandiKategoriBI_ID,
		isnull(cast(BidangSandi_ID as varchar(36)),'') as BidangSandi_ID,
		isnull(No_Head_Kategori,'') as No_Head_Kategori,
		isnull(Kode_Kategori,'') as Kode_Kategori,
		isnull(BidangSandi_No,'') as BidangSandi_No,
		isnull(Bidang_Sandi,'') as Bidang_Sandi
		from VwMasterBidangSandiKategoriBI
		` + queryCondition + `
		order by No_Head_Kategori
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("NoHeadKategori", fmt.Sprintf("%v", NoHeadKategori)),
	)
	j := new(models.BidangSandiKategoriBI)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", strings.TrimSpace(*NoHeadKategori)+"%"),
	).Scan(
		&j.BidangSandiKategoriBIID,
		&j.BidangSandiID,
		&j.NoHeadKategori,
		&j.KodeKategori,
		&j.BidangSandiNo,
		&j.BidangSandi,
	)

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, j
}
