package bidangsandikategoribi

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type BidangSandiKategoriBIRepository interface {
	Fetch(ctx context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI)
	FetchView(ctx context.Context, BidangSandiID *string) (error, []*models.BidangSandiKategoriBI)
	GetByBidangSandiKategoriID(ctx context.Context, BidangSandiID *string) (error, *models.BidangSandiKategoriBI)
	GetByNoHeadKategori(ctx context.Context, NoHeadKategori *string) (error, *models.BidangSandiKategoriBI)
}
