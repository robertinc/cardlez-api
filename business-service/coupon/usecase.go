package coupon

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoCouponUseCase interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	FetchBaseCardlez(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	Store(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon)
	StoreBase(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon)
	StoreBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (error, *models.PromoCoupon)
	Update(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon)
	UpdateBase(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon)
	UpdateBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (error, *models.PromoCoupon)
	StorePromoCouponDestination(ctx context.Context, promoCouponDestination models.PromoCouponDestination) (error, *models.PromoCouponDestination)
	StorePromoCouponDestinationByDate(ctx context.Context, promoCouponDestination models.PromoCouponDestination, companyDate string) (error, *models.PromoCouponDestination)
	ClearPromoCouponDestination(ctx context.Context, promoCouponID string) (error, bool)
	FetchPromoCouponDestination(ctx context.Context, promoCouponID string) (error, []*models.PromoCouponDestination)
}
