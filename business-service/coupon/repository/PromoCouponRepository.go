package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	coupon "gitlab.com/robertinc/cardlez-api/business-service/coupon"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlPromoCouponRepository struct {
	Conn *sql.DB
}

func NewSqlPromoCouponRepository() coupon.PromoCouponRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlPromoCouponRepository{conn}
}

func NewSqlPromoCouponRepositoryV2(Conn *sql.DB) coupon.PromoCouponRepository {
	conn := Conn
	return &sqlPromoCouponRepository{conn}
}

//ini tidak dipakai, for next update query fetchbase diimplement dsini.
func (sj *sqlPromoCouponRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PromoImages,
		Isnull(PromoTitle, ''),
		PromoDesc,
		StartDate,
		EndDate,
		Cast(Isnull(LoyaltyID, '') as varchar(36)) as LoyaltyID,
		PoinAmount,
		PromoAmount,
		Isnull(IsPoin, '1')
	FROM PromoCoupon a
	`
	if search != "ALL" && search != "" {
		if search == "NOTEXPIRED" {
			query += ` where day(ENDDATE) >= day(GETDATE()) and month(ENDDATE) >= month(GETDATE()) and year(ENDDATE) >= year(GETDATE()) `
		} else {
			search = "a." + search
			query += ` where ` + search + ` like @p0`
		}
	} else {
		query += ` where 1=1`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.PromoCoupon, 0)
	for rows.Next() {
		j := new(models.PromoCoupon)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PromoImages,
			&j.PromoTitle,
			&j.PromoDesc,
			&j.StartDate,
			&j.EndDate,
			&j.LoyaltyID,
			&j.PoinAmount,
			&j.PromoAmount,
			&j.IsPoin,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlPromoCouponRepository) FetchBase(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PromoImages,
		Isnull(PromoTitle, ''),
		PromoDesc,
		StartDate,
		EndDate,
		Cast(Isnull(LoyaltyID, '') as varchar(36)) as LoyaltyID,
		PoinAmount,
		PromoAmount,
		Isnull(IsPoin, '1')
	FROM PromoCoupon a
	`
	if search != "ALL" && search != "" {
		if search == "NOTEXPIRED" {
			query += ` where CONVERT(DATE, EndDate) >= GETDATE() `
		} else {
			search = "a." + search
			query += ` where ` + search + ` like @p0`
		}
	} else {
		query += ` where 1=1`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.PromoCoupon, 0)
	for rows.Next() {
		j := new(models.PromoCoupon)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PromoImages,
			&j.PromoTitle,
			&j.PromoDesc,
			&j.StartDate,
			&j.EndDate,
			&j.LoyaltyID,
			&j.PoinAmount,
			&j.PromoAmount,
			&j.IsPoin,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlPromoCouponRepository) FetchBaseCardlez(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PromoImages,
		Isnull(PromoTitle, ''),
		PromoDesc,
		StartDate,
		EndDate,
		Cast(Isnull(LoyaltyID, '') as varchar(36)) as LoyaltyID,
		PoinAmount,
		PromoAmount,
		Isnull(IsPoin, '1'),
		Isnull(AmountIsPercentage, '0'),
		Isnull(MaxPromoAmount, 0),
		Isnull(MaxPromoUsage, 0),
		Isnull(DestinationType, 0),
		Isnull(Category, ''),
		Isnull(MinPromoAmount, 0)
	FROM PromoCoupon a
	`
	if search != "ALL" && search != "" {
		if search == "NOTEXPIRED" {
			query += ` where EndDate >= GETDATE() `
		} else {
			search = "a." + search
			query += ` where ` + search + ` like @p0`
		}
	} else {
		query += ` where 1=1`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.PromoCoupon, 0)
	for rows.Next() {
		j := new(models.PromoCoupon)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PromoImages,
			&j.PromoTitle,
			&j.PromoDesc,
			&j.StartDate,
			&j.EndDate,
			&j.LoyaltyID,
			&j.PoinAmount,
			&j.PromoAmount,
			&j.IsPoin,
			&j.AmountIsPercentage,
			&j.MaxPromoAmount,
			&j.MaxPromoUsage,
			&j.DestinationType,
			&j.Category,
			&j.MinPromoAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlPromoCouponRepository) Store(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error) {
	promoCoupon.ID = guid.New().StringUpper()
	promoCouponQuery := `
		INSERT INTO PromoCoupon (
			ID, 
			Code, 
			PromoImages,
			PromoTitle,
			PromoDesc,
			StartDate,
			EndDate,
			PoinAmount,
			PromoAmount,
			IsPoin,
			UserInsert,
			DateInsert
		) VALUES (
			@id,
			@code,
			@promoImages,
			@promoTitle,
			@promoDesc,
			@startDate,
			@endDate,
			@poinAmount,
			@promoAmount,
			@isPoin,
			@userInsert,
			GETDATE()
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("code", promoCoupon.Code),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("userInsert", ""),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) StoreBase(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error) {
	promoCoupon.ID = guid.New().StringUpper()
	promoCouponQuery := `
		INSERT INTO PromoCoupon (
			ID, 
			Code, 
			PromoImages,
			PromoTitle,
			PromoDesc,
			StartDate,
			EndDate,
			PoinAmount,
			PromoAmount,
			IsPoin,
			AmountIsPercentage,
			MaxPromoAmount,
			MaxPromoUsage,
			DestinationType,
			Category,
			MinPromoAmount,
			UserInsert,
			DateInsert
		) VALUES (
			@id,
			@code,
			@promoImages,
			@promoTitle,
			@promoDesc,
			@startDate,
			@endDate,
			@poinAmount,
			@promoAmount,
			@isPoin,
			@amountIsPercentage,
			@maxPromoAmount,
			@maxPromoUsage,
			@destinationType,
			@category,
			@minPromoAmount,
			@userInsert,
			GETDATE()
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("code", promoCoupon.Code),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("amountIsPercentage", promoCoupon.AmountIsPercentage),
		sql.Named("maxPromoAmount", promoCoupon.MaxPromoAmount),
		sql.Named("maxPromoUsage", promoCoupon.MaxPromoUsage),
		sql.Named("destinationType", promoCoupon.DestinationType),
		sql.Named("category", promoCoupon.Category),
		sql.Named("minPromoAmount", promoCoupon.MinPromoAmount),
		sql.Named("userInsert", ""),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) StoreBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (models.PromoCoupon, error) {
	promoCoupon.ID = guid.New().StringUpper()
	promoCouponQuery := `
		INSERT INTO PromoCoupon (
			ID, 
			Code, 
			PromoImages,
			PromoTitle,
			PromoDesc,
			StartDate,
			EndDate,
			PoinAmount,
			PromoAmount,
			IsPoin,
			AmountIsPercentage,
			MaxPromoAmount,
			MaxPromoUsage,
			DestinationType,
			Category,
			MinPromoAmount,
			UserInsert,
			DateInsert
		) VALUES (
			@id,
			@code,
			@promoImages,
			@promoTitle,
			@promoDesc,
			@startDate,
			@endDate,
			@poinAmount,
			@promoAmount,
			@isPoin,
			@amountIsPercentage,
			@maxPromoAmount,
			@maxPromoUsage,
			@destinationType,
			@category,
			@minPromoAmount,
			@userInsert,
			@date
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("code", promoCoupon.Code),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("amountIsPercentage", promoCoupon.AmountIsPercentage),
		sql.Named("maxPromoAmount", promoCoupon.MaxPromoAmount),
		sql.Named("maxPromoUsage", promoCoupon.MaxPromoUsage),
		sql.Named("destinationType", promoCoupon.DestinationType),
		sql.Named("category", promoCoupon.Category),
		sql.Named("minPromoAmount", promoCoupon.MinPromoAmount),
		sql.Named("userInsert", ""),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) Update(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error) {

	promoCouponQuery := `
		Update PromoCoupon 
		Set 
			PromoImages = @promoImages,
			PromoTitle = @promoTitle,
			PromoDesc = @promoDesc,
			StartDate = @startDate,
			EndDate = @endDate,
			PoinAmount = @poinAmount,
			PromoAmount = @promoAmount,
			IsPoin = @isPoin,
			UserUpdate = @userUpdate,
			DateUpdate = GETDATE()
		Where 
			ID = @id`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("userUpdate", ""),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) UpdateBase(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error) {

	promoCouponQuery := `
		Update PromoCoupon 
		Set 
			PromoImages = @promoImages,
			PromoTitle = @promoTitle,
			PromoDesc = @promoDesc,
			StartDate = @startDate,
			EndDate = @endDate,
			PoinAmount = @poinAmount,
			PromoAmount = @promoAmount,
			IsPoin = @isPoin,
			AmountIsPercentage = @amountIsPercentage,
			MaxPromoAmount = @maxPromoAmount,
			MaxPromoUsage = @maxPromoUsage,
			DestinationType = @destinationType,
			Category = @category,
			MinPromoAmount = @minPromoAmount,
			UserUpdate = @userUpdate,
			DateUpdate = GETDATE()
		Where 
			ID = @id`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("amountIsPercentage", promoCoupon.AmountIsPercentage),
		sql.Named("maxPromoAmount", promoCoupon.MaxPromoAmount),
		sql.Named("maxPromoUsage", promoCoupon.MaxPromoUsage),
		sql.Named("destinationType", promoCoupon.DestinationType),
		sql.Named("category", promoCoupon.Category),
		sql.Named("minPromoAmount", promoCoupon.MinPromoAmount),
		sql.Named("userUpdate", ""),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) UpdateBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (models.PromoCoupon, error) {

	promoCouponQuery := `
		Update PromoCoupon 
		Set 
			PromoImages = @promoImages,
			PromoTitle = @promoTitle,
			PromoDesc = @promoDesc,
			StartDate = @startDate,
			EndDate = @endDate,
			PoinAmount = @poinAmount,
			PromoAmount = @promoAmount,
			IsPoin = @isPoin,
			AmountIsPercentage = @amountIsPercentage,
			MaxPromoAmount = @maxPromoAmount,
			MaxPromoUsage = @maxPromoUsage,
			DestinationType = @destinationType,
			Category = @category,
			MinPromoAmount = @minPromoAmount,
			UserUpdate = @userUpdate,
			DateUpdate = @date
		Where 
			ID = @id`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCoupon)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponQuery,
		sql.Named("id", promoCoupon.ID),
		sql.Named("promoImages", promoCoupon.PromoImages),
		sql.Named("promoTitle", promoCoupon.PromoTitle),
		sql.Named("promoDesc", promoCoupon.PromoDesc),
		sql.Named("startDate", promoCoupon.StartDate),
		sql.Named("endDate", promoCoupon.EndDate),
		sql.Named("poinAmount", promoCoupon.PoinAmount),
		sql.Named("promoAmount", promoCoupon.PromoAmount),
		sql.Named("isPoin", promoCoupon.IsPoin),
		sql.Named("amountIsPercentage", promoCoupon.AmountIsPercentage),
		sql.Named("maxPromoAmount", promoCoupon.MaxPromoAmount),
		sql.Named("maxPromoUsage", promoCoupon.MaxPromoUsage),
		sql.Named("destinationType", promoCoupon.DestinationType),
		sql.Named("category", promoCoupon.Category),
		sql.Named("minPromoAmount", promoCoupon.MinPromoAmount),
		sql.Named("userUpdate", ""),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return models.PromoCoupon{}, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCoupon)),
	)
	return promoCoupon, nil
}
func (sj *sqlPromoCouponRepository) StorePromoCouponDestination(ctx context.Context, promoCouponDestination models.PromoCouponDestination) (error, *models.PromoCouponDestination) {
	promoCouponDestination.ID = guid.New().StringUpper()
	promoCouponDestinationQuery := `
		INSERT INTO PromoCouponDestination (
			ID, 
			PromoCouponID, 
			MemberID,
			UserInsert,
			DateInsert
		) VALUES (
			@id,
			@promoCouponID,
			@memberID,
			@userInsert,
			GETDATE()
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponDestinationQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCouponDestination)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponDestinationQuery,
		sql.Named("id", promoCouponDestination.ID),
		sql.Named("promoCouponID", promoCouponDestination.PromoCouponID),
		sql.Named("memberID", promoCouponDestination.MemberID),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCouponDestination)),
	)
	return nil, &promoCouponDestination
}
func (sj *sqlPromoCouponRepository) StorePromoCouponDestinationByDate(ctx context.Context, promoCouponDestination models.PromoCouponDestination, companyDate string) (error, *models.PromoCouponDestination) {
	promoCouponDestination.ID = guid.New().StringUpper()
	promoCouponDestinationQuery := `
		INSERT INTO PromoCouponDestination (
			ID, 
			PromoCouponID, 
			MemberID,
			UserInsert,
			DateInsert
		) VALUES (
			@id,
			@promoCouponID,
			@memberID,
			@userInsert,
			@date
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponDestinationQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCouponDestination)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponDestinationQuery,
		sql.Named("id", promoCouponDestination.ID),
		sql.Named("promoCouponID", promoCouponDestination.PromoCouponID),
		sql.Named("memberID", promoCouponDestination.MemberID),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCouponDestination)),
	)
	return nil, &promoCouponDestination
}
func (sj *sqlPromoCouponRepository) ClearPromoCouponDestination(ctx context.Context, promoCouponID string) (error, bool) {
	promoCouponDestinationQuery := `
		DELETE PromoCouponDestination where PromoCouponID = @promoCouponID
		`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", promoCouponDestinationQuery)),
		zap.String("obj", fmt.Sprintf("%v", promoCouponID)),
	)
	_, err := sj.Conn.ExecContext(ctx, promoCouponDestinationQuery,
		sql.Named("promoCouponID", promoCouponID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, false
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", promoCouponID)),
	)
	return nil, true
}
func (sj *sqlPromoCouponRepository) FetchPromoCouponDestination(ctx context.Context, promoCouponID string) (error, []*models.PromoCouponDestination) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Cast(PromoCouponID as varchar(36)) as PromoCouponID,
		Cast(MemberID as varchar(36)) as MemberID
	FROM PromoCouponDestination a
	where PromoCouponID = @promoCouponID
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", promoCouponID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("promoCouponID", promoCouponID),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.PromoCouponDestination, 0)
	for rows.Next() {
		j := new(models.PromoCouponDestination)
		err = rows.Scan(
			&j.ID,
			&j.PromoCouponID,
			&j.MemberID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
