package usecase

import (
	"context"
	"path/filepath"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/coupon"
	"gitlab.com/robertinc/cardlez-api/business-service/coupon/repository"
	memberrepo "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoCouponUsecase struct {
	PromoCouponRepository coupon.PromoCouponRepository
	contextTimeout        time.Duration
}

func NewPromoCouponUsecase() coupon.PromoCouponUseCase {
	return &PromoCouponUsecase{
		PromoCouponRepository: repository.NewSqlPromoCouponRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewPromoCouponUsecaseV2(
	promoCouponRepo coupon.PromoCouponRepository) coupon.PromoCouponUseCase {
	return &PromoCouponUsecase{
		PromoCouponRepository: promoCouponRepo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}
func (a *PromoCouponUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listPromoCoupon := a.PromoCouponRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listPromoCoupon
}
func (a *PromoCouponUsecase) FetchBase(c context.Context, search string, keyword string) (error, []*models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listPromoCoupon := a.PromoCouponRepository.FetchBase(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listPromoCoupon
}
func (a *PromoCouponUsecase) FetchBaseCardlez(c context.Context, search string, keyword string) (error, []*models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listPromoCoupon := a.PromoCouponRepository.FetchBaseCardlez(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listPromoCoupon
}

func (a *PromoCouponUsecase) Update(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	promoCoupon, err := a.PromoCouponRepository.Update(ctx, promoCoupon)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) UpdateBase(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	promoCoupon, err := a.PromoCouponRepository.UpdateBase(ctx, promoCoupon)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) UpdateBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	promoCoupon, err := a.PromoCouponRepository.UpdateBaseByDate(ctx, promoCoupon, companyDate)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) Store(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	if promoCoupon.Code == "" {
		uc := memberrepo.NewSqlMemberRepository()
		err, code := uc.GenerateCode(ctx, "PRM")
		if err != nil {
			return err, nil
		}
		promoCoupon.Code = code
	}
	promoCoupon.PromoImages = promoCoupon.Code + filepath.Ext(promoCoupon.PromoImages)
	promoCoupon, err := a.PromoCouponRepository.Store(ctx, promoCoupon)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) StoreBase(ctx context.Context, promoCoupon models.PromoCoupon) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	if promoCoupon.Code == "" {
		uc := memberrepo.NewSqlMemberRepository()
		err, code := uc.GenerateCode(ctx, "PRM")
		if err != nil {
			return err, nil
		}
		promoCoupon.Code = code
	}
	promoCoupon.PromoImages = promoCoupon.PromoImages
	promoCoupon, err := a.PromoCouponRepository.StoreBase(ctx, promoCoupon)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) StoreBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (error, *models.PromoCoupon) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	if promoCoupon.Code == "" {
		uc := memberrepo.NewSqlMemberRepository()
		err, code := uc.GenerateCode(ctx, "PRM")
		if err != nil {
			return err, nil
		}
		promoCoupon.Code = code
	}
	promoCoupon.PromoImages = promoCoupon.PromoImages
	promoCoupon, err := a.PromoCouponRepository.StoreBaseByDate(ctx, promoCoupon, companyDate)
	if err != nil {
		return err, nil
	}
	return nil, &promoCoupon
}
func (a *PromoCouponUsecase) StorePromoCouponDestination(ctx context.Context, promoCouponDestination models.PromoCouponDestination) (error, *models.PromoCouponDestination) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, pcd := a.PromoCouponRepository.StorePromoCouponDestination(ctx, promoCouponDestination)
	if err != nil {
		return err, nil
	}
	return nil, pcd
}
func (a *PromoCouponUsecase) StorePromoCouponDestinationByDate(ctx context.Context, promoCouponDestination models.PromoCouponDestination, companyDate string) (error, *models.PromoCouponDestination) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, pcd := a.PromoCouponRepository.StorePromoCouponDestinationByDate(ctx, promoCouponDestination, companyDate)
	if err != nil {
		return err, nil
	}
	return nil, pcd
}
func (a *PromoCouponUsecase) ClearPromoCouponDestination(ctx context.Context, promoCouponID string) (error, bool) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, pcd := a.PromoCouponRepository.ClearPromoCouponDestination(ctx, promoCouponID)
	if err != nil {
		return err, false
	}
	return nil, pcd
}
func (a *PromoCouponUsecase) FetchPromoCouponDestination(ctx context.Context, promoCouponID string) (error, []*models.PromoCouponDestination) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, pcds := a.PromoCouponRepository.FetchPromoCouponDestination(ctx, promoCouponID)
	if err != nil {
		return err, nil
	}
	return nil, pcds
}
