package coupon

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PromoCouponRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	FetchBaseCardlez(ctx context.Context, search string, keyword string) (error, []*models.PromoCoupon)
	Store(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error)
	StoreBase(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error)
	StoreBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (models.PromoCoupon, error)
	Update(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error)
	UpdateBase(ctx context.Context, promoCoupon models.PromoCoupon) (models.PromoCoupon, error)
	UpdateBaseByDate(ctx context.Context, promoCoupon models.PromoCoupon, companyDate string) (models.PromoCoupon, error)
	StorePromoCouponDestination(ctx context.Context, promoCouponDestination models.PromoCouponDestination) (error, *models.PromoCouponDestination)
	StorePromoCouponDestinationByDate(ctx context.Context, promoCouponDestination models.PromoCouponDestination, companyDate string) (error, *models.PromoCouponDestination)
	ClearPromoCouponDestination(ctx context.Context, promoCouponID string) (error, bool)
	FetchPromoCouponDestination(ctx context.Context, promoCouponID string) (error, []*models.PromoCouponDestination)
}
