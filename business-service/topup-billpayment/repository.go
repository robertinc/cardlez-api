package topupbillpayment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TopUpBillPaymentRepository interface {
	UpdateStatus(ctx context.Context, topupbillPayment *models.TopUpBillPayment) (bool, error)
	Store(ctx context.Context, topupbillPayment *models.TopUpBillPayment) (bool, error)
	GetHistoryPayment(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error)
	GetHistoryPaymentBase(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error)
	GetExistingPayment(ctx context.Context, debitaccountid *string) ([]*models.TopUpBillPayment, error)
	UpdateRecordStatus(ctx context.Context, code string, reffNo string, status int32) (bool, error)
	UpdateSettlementById(ctx context.Context, id string, settlementNo string, status int32) (bool, error)
	UpdateSettlementByIdNew(ctx context.Context, id string, settlementNo string, status int32, date string) (bool, error)
	UpdatePlnTokenCallback(ctx context.Context, code string, reffNo string, status int32, plnToken string) (bool, error)
	UpdatePlnToken(ctx context.Context, a *models.TopUpBillPayment) (bool, error)
}
