package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"go.uber.org/zap"

	topupbillpayment "gitlab.com/robertinc/cardlez-api/business-service/topup-billpayment"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlTopUpBillPaymentRepository struct {
	Conn *sql.DB
}

func NewSqlTopUpBillPaymentRepository() topupbillpayment.TopUpBillPaymentRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlTopUpBillPaymentRepository{conn}
}

func NewSqlTopUpBillPaymentRepositoryV2(Conn *sql.DB) topupbillpayment.TopUpBillPaymentRepository {
	conn := Conn
	return &sqlTopUpBillPaymentRepository{conn}
}

func (sj *sqlTopUpBillPaymentRepository) Store(ctx context.Context, a *models.TopUpBillPayment) (bool, error) {
	//a.ID = guid.New().StringUpper()
	TopUpBillPaymentQuery := `INSERT INTO BillPayment (
		ID
		,Code
		,DebitAccountID
		,Amount
		,FeeAmount
		,DebitDate
		,CustomerNo
		,CustomerName
		,CoreReffNo
		,BillerCode
		,RecordStatus
		,UserInsert
		,DateInsert
		,UserUpdate
		,DateUpdate
		,TransactionCode
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15 )`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", TopUpBillPaymentQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, TopUpBillPaymentQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.DebitDate),
		sql.Named("p6", a.CustomerNo),
		sql.Named("p7", a.CustomerName),
		sql.Named("p8", a.CoreReffNo),
		sql.Named("p9", a.BillerCode),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.UserInsert),
		sql.Named("p12", a.DateInsert),
		sql.Named("p13", a.UserUpdate),
		sql.Named("p14", a.DateUpdate),
		sql.Named("p15", a.TransactionCode),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
func (sj *sqlTopUpBillPaymentRepository) UpdateStatus(ctx context.Context, a *models.TopUpBillPayment) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set RecordStatus = @p1,
		DateUpdate = GETDATE()
	Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.RecordStatus),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlTopUpBillPaymentRepository) UpdateRecordStatus(ctx context.Context, code string, reffNo string, status int32) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set RecordStatus = @p1 
	Where Code = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("Code", fmt.Sprintf("%v", code)),
		zap.String("ReffNo", fmt.Sprintf("%v", reffNo)),
		zap.String("Status", fmt.Sprintf("%v", status)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", code),
		sql.Named("p1", status),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlTopUpBillPaymentRepository) UpdateSettlementById(ctx context.Context, id string, settlementNo string, status int32) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set RecordStatus = @p1 ,
		SettlementNo = @p2,
		SettlementDate = GETDATE()
	Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", id)),
		zap.String("Status", fmt.Sprintf("%v", status)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", id),
		sql.Named("p1", status),
		sql.Named("p2", settlementNo),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlTopUpBillPaymentRepository) UpdateSettlementByIdNew(ctx context.Context, id string, settlementNo string, status int32, date string) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set RecordStatus = @p1 ,
		SettlementNo = @p2,
		SettlementDate = @p3
	Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", id)),
		zap.String("Status", fmt.Sprintf("%v", status)),
		zap.String("date", fmt.Sprintf("%v", date)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", id),
		sql.Named("p1", status),
		sql.Named("p2", settlementNo),
		sql.Named("p3", date),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlTopUpBillPaymentRepository) GetHistoryPayment(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error) {

	query := `
	SELECT cast(b.ID as varchar(36)) ID
		,Isnull(b.Code,'') as Code
		,cast(DebitAccountID as varchar(36)) DebitAccountID
		,Amount
		,FeeAmount
		,DebitDate
		,Isnull(b.CustomerNo,'') as CustomerNo
		,Isnull(CustomerName,'') as CustomerName
		,Isnull(CoreReffNo,'') as CoreReffNo
		,Isnull(BillerCode,'') as BillerCode
		,b.RecordStatus
		,b.DateInsert
		FROM BillPayment b
		left join Account a on b.DebitAccountID=a.id
		left join member m on a.memberid=m.id
		inner join (select CustomerNo,max(dateinsert) as dateinsert from BillPayment
		where BillerCode='OVO' and RecordStatus=2
		group by CustomerNo) e on b.CustomerNo=e.CustomerNo and b.dateinsert=e.dateinsert
		where m.id=@memberid
		and BillerCode='OVO' and b.RecordStatus=2
		order by b.CustomerNo`
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("memberid", memberid),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	result := make([]*models.TopUpBillPayment, 0)
	for rows.Next() {
		topup := new(models.TopUpBillPayment)
		err = rows.Scan(
			&topup.ID,
			&topup.Code,
			&topup.DebitAccountID,
			&topup.Amount,
			&topup.FeeAmount,
			&topup.DebitDate,
			&topup.CustomerNo,
			&topup.CustomerName,
			&topup.CoreReffNo,
			&topup.BillerCode,
			&topup.RecordStatus,
			&topup.DateInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, topup)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)

	return result, nil
}
func (sj *sqlTopUpBillPaymentRepository) GetHistoryPaymentBase(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error) {

	query := `
	SELECT cast(b.ID as varchar(36)) ID
		,Isnull(b.Code,'') as Code
		,cast(DebitAccountID as varchar(36)) DebitAccountID
		,Amount
		,FeeAmount
		,DebitDate
		,Isnull(b.CustomerNo,'') as CustomerNo
		,Isnull(CustomerName,'') as CustomerName
		,Isnull(CoreReffNo,'') as CoreReffNo
		,CASE WHEN Isnull(BillerCode,'')='OVOSianyu' THEN 'OVO'
		ELSE Isnull(BillerCode,'') END as BillerCode
		,b.RecordStatus
		,b.DateInsert
		FROM BillPayment b
		left join Account a on b.DebitAccountID=a.id
		left join member m on a.memberid=m.id
		inner join (select CustomerNo,max(dateinsert) as dateinsert from BillPayment
		where (BillerCode='OVO' OR BillerCode='OVOSianyu' OR BillerCode='GOPAY' OR BillerCode='LINKAJA') and (RecordStatus=2 OR RecordStatus=4)
		group by CustomerNo) e on b.CustomerNo=e.CustomerNo and b.dateinsert=e.dateinsert
		where m.id=@memberid
		and (BillerCode='OVO' OR BillerCode='OVOSianyu' OR BillerCode='GOPAY' OR BillerCode='LINKAJA') and (b.RecordStatus=2 OR b.RecordStatus=4)
		order by b.CustomerNo`
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("memberid", memberid),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	result := make([]*models.TopUpBillPayment, 0)
	for rows.Next() {
		topup := new(models.TopUpBillPayment)
		err = rows.Scan(
			&topup.ID,
			&topup.Code,
			&topup.DebitAccountID,
			&topup.Amount,
			&topup.FeeAmount,
			&topup.DebitDate,
			&topup.CustomerNo,
			&topup.CustomerName,
			&topup.CoreReffNo,
			&topup.BillerCode,
			&topup.RecordStatus,
			&topup.DateInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, topup)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)

	return result, nil
}
func (sj *sqlTopUpBillPaymentRepository) GetExistingPayment(ctx context.Context, customerNo *string) ([]*models.TopUpBillPayment, error) {

	query := `
	select top 1 
	cast(ID as varchar(36)) ID
			,Isnull(Code,'') as Code
			,cast(DebitAccountID as varchar(36)) DebitAccountID
			,Amount
			,FeeAmount
			,DebitDate
			,Isnull(CustomerNo,'') as CustomerNo
			,Isnull(CustomerName,'') as CustomerName
			,Isnull(CoreReffNo,'') as CoreReffNo
			,Isnull(BillerCode,'') as BillerCode
			,RecordStatus
			,DateInsert
	from BillPayment
	where CustomerNo = @CustomerNo and BillerCode='OVO'
	order by DateInsert desc`
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("CustomerNo", customerNo),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	result := make([]*models.TopUpBillPayment, 0)
	for rows.Next() {
		topup := new(models.TopUpBillPayment)
		err = rows.Scan(
			&topup.ID,
			&topup.Code,
			&topup.DebitAccountID,
			&topup.Amount,
			&topup.FeeAmount,
			&topup.DebitDate,
			&topup.CustomerNo,
			&topup.CustomerName,
			&topup.CoreReffNo,
			&topup.BillerCode,
			&topup.RecordStatus,
			&topup.DateInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, topup)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)

	return result, nil
}

func (sj *sqlTopUpBillPaymentRepository) UpdatePlnToken(ctx context.Context, a *models.TopUpBillPayment) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set PlnToken = @p1 
	Where ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.PlnToken),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func ResultUpdateStatus(status bool) *bool {
	b := status
	return &b
}

func (sj *sqlTopUpBillPaymentRepository) UpdatePlnTokenCallback(ctx context.Context, code string, reffNo string, status int32, plnToken string) (bool, error) {

	updateQuery := `
	Update BillPayment
		Set PlnToken = @p1 
	Where Code = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("Code", fmt.Sprintf("%v", code)),
		zap.String("ReffNo", fmt.Sprintf("%v", reffNo)),
		zap.String("Status", fmt.Sprintf("%v", status)),
		zap.String("Token", fmt.Sprintf("%v", plnToken)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", code),
		sql.Named("p1", plnToken),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
