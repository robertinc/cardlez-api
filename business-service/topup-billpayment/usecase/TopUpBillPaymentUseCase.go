package usecase

import (
	"context"
	"time"

	topupbillpayment "gitlab.com/robertinc/cardlez-api/business-service/topup-billpayment"
	"gitlab.com/robertinc/cardlez-api/business-service/topup-billpayment/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type TopUpBillPaymentUsecase struct {
	TopUpBillPaymentRepository topupbillpayment.TopUpBillPaymentRepository
	contextTimeout             time.Duration
}

func NewTopUpBillPaymentUsecase() topupbillpayment.TopUpBillPaymentUsecase {
	return &TopUpBillPaymentUsecase{
		TopUpBillPaymentRepository: repository.NewSqlTopUpBillPaymentRepository(),
		contextTimeout:             time.Second * time.Duration(models.Timeout()),
	}
}

func NewTopUpBillPaymentUsecaseV2(repo topupbillpayment.TopUpBillPaymentRepository) topupbillpayment.TopUpBillPaymentUsecase {
	return &TopUpBillPaymentUsecase{
		TopUpBillPaymentRepository: repo,
		contextTimeout:             time.Second * time.Duration(models.Timeout()),
	}
}

func (a *TopUpBillPaymentUsecase) GetHistoryPayment(c context.Context, memberid *string) ([]*models.TopUpBillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listHistory, err := a.TopUpBillPaymentRepository.GetHistoryPayment(ctx, memberid)
	if err != nil {
		return nil, err
	}
	return listHistory, err
}
func (a *TopUpBillPaymentUsecase) GetHistoryPaymentBase(c context.Context, memberid *string) ([]*models.TopUpBillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listHistory, err := a.TopUpBillPaymentRepository.GetHistoryPaymentBase(ctx, memberid)
	if err != nil {
		return nil, err
	}
	return listHistory, err
}
