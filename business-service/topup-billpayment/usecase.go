package topupbillpayment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TopUpBillPaymentUsecase interface {
	GetHistoryPayment(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error)
	GetHistoryPaymentBase(ctx context.Context, memberid *string) ([]*models.TopUpBillPayment, error)
	//
}
