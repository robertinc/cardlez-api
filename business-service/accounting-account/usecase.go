package accountingaccount

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AccountingAccountUsecase interface {
	Fetch(ctx context.Context) (error, []*models.AccountingAccount)
	FetchByCOAID(ctx context.Context, COAID string) (error, *models.AccountingAccount)
}
