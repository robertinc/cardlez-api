package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	accountingaccount "gitlab.com/robertinc/cardlez-api/business-service/accounting-account"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlAccountingAccountRepository struct {
	Conn *sql.DB
}

func NewSqlAccountingAccountRepository() accountingaccount.AccountingAccountRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlAccountingAccountRepository{conn}
}

func NewSqlAccountingAccountRepositoryV2(Conn *sql.DB) accountingaccount.AccountingAccountRepository {
	conn := Conn
	return &sqlAccountingAccountRepository{conn}
}

func (sj *sqlAccountingAccountRepository) Fetch(ctx context.Context) (error, []*models.AccountingAccount) {

	logger := service.Logger(ctx)
	query := `
	select 
		Cast(aa.ID as varchar(36)) as ID, 
		c.Code, 
		c.Description, 
		Isnull(aa.OnlineBalance, 0), 
		Isnull(ac.Code, '')+'-'+Isnull(ac.AccountName, '')
	from accountingaccounts aa
		Inner Join COA c on aa.COAID = c.ID
		Left Join Account ac on ac.ID = aa.ReferenceNumber
	Order by c.Code ASC`
	result := make([]*models.AccountingAccount, 0)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.AccountingAccount)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.COAName,
			&j.Amount,
			&j.AccountName,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlAccountingAccountRepository) FetchByCOAID(ctx context.Context, COAID string) (error, *models.AccountingAccount) {
	var j models.AccountingAccount
	logger := service.Logger(ctx)
	query := `
	select TOP(1)
		Cast(aa.ID as varchar(36)) as ID, 
		c.Code, 
		c.Description, 
		Isnull(aa.OnlineBalance, 0), 
		Isnull(ac.Code, '')+'-'+Isnull(ac.AccountName, '')
	from accountingaccounts aa
		Inner Join COA c on aa.COAID = c.ID
		Left Join Account ac on ac.ID = aa.ReferenceNumber
	WHERE aa.COAID = @p0 AND aa.ReferenceNumber is NULL
	Order by c.Code ASC`

	err := sj.Conn.QueryRow(query,
		sql.Named("p0", COAID),
	).Scan(
		&j.ID,
		&j.Code,
		&j.COAName,
		&j.Amount,
		&j.AccountName,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}
