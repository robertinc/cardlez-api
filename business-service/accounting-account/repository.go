package accountingaccount

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AccountingAccountRepository interface {
	Fetch(ctx context.Context) (error, []*models.AccountingAccount)
	FetchByCOAID(ctx context.Context, COAID string) (error, *models.AccountingAccount)
}
