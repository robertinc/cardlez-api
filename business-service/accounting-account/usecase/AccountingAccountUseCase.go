package usecase

import (
	"context"
	"time"

	accountingaccount "gitlab.com/robertinc/cardlez-api/business-service/accounting-account"
	"gitlab.com/robertinc/cardlez-api/business-service/accounting-account/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type AccountingAccountUsecase struct {
	AccountingAccountRepository accountingaccount.AccountingAccountRepository
	contextTimeout              time.Duration
}

func NewAccountingAccountUsecase() accountingaccount.AccountingAccountUsecase {
	return &AccountingAccountUsecase{
		AccountingAccountRepository: repository.NewSqlAccountingAccountRepository(),
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func NewAccountingAccountUsecaseV2(accountingAccountRepo accountingaccount.AccountingAccountRepository) accountingaccount.AccountingAccountUsecase {
	return &AccountingAccountUsecase{
		AccountingAccountRepository: accountingAccountRepo,
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func (a *AccountingAccountUsecase) Fetch(c context.Context) (error, []*models.AccountingAccount) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listAccountingAccount := a.AccountingAccountRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listAccountingAccount
}

func (a *AccountingAccountUsecase) FetchByCOAID(c context.Context, COAID string) (error, *models.AccountingAccount) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accountingAccount := a.AccountingAccountRepository.FetchByCOAID(ctx, COAID)
	if err != nil {
		return err, nil
	}
	return nil, accountingAccount
}
