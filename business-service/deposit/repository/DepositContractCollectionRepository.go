package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlDepositContractCollectionRepository struct {
	Conn *sql.DB
}

func NewSqlDepositContractCollectionRepository() deposit.DepositContractCollectionRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlDepositContractCollectionRepository{conn}
}
func NewSqlDepositContractCollectionRepositoryV2(Conn *sql.DB) deposit.DepositContractCollectionRepository {
	conn := Conn
	return &sqlDepositContractCollectionRepository{conn}
}
func (sj *sqlDepositContractCollectionRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContractCollection) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Cast(DepositsContractID as varchar(36)) as DepositsContractID,
		Cast(CollectionDate as varchar(50)) as CollectionDate, 
		CollectionAmount,
		IsPaid,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(Cast(TransferDate as varchar(50)),'') as TransferDate,
		isnull(IsTransfer,0) as IsTransfer,
		isnull(Remark,'') as Remark,
		InterestAmount
	FROM dbo.DepositContractCollection 
		`
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	}
	query += ` order by CollectionDate `
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.DepositContractCollection, 0)
	for rows.Next() {
		j := new(models.DepositContractCollection)
		err = rows.Scan(
			&j.ID,
			&j.DepositsContractID,
			&j.CollectionDate,
			&j.CollectionAmount,
			&j.IsPaid,
			&j.DateInsert,
			&j.TransferDate,
			&j.IsTransfer,
			&j.Remark,
			&j.InterestAmount,
		)
		if err != nil {
			fmt.Print(err)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return nil, result
}

func (sj *sqlDepositContractCollectionRepository) Store(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error) {
	a.ID = guid.New().StringUpper()
	DepositContractCollectionQuery := `
	INSERT INTO DepositContractCollection (
	   ID
	   ,DepositsContractID
	   ,CollectionDate
	   ,CollectionAmount
	   ,IsPaid
	   ,UserInsert
	   ,DateInsert
	   ,TransferDate
	   ,IsTransfer
	   ,Remark
	   ,InterestAmount
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		GETDATE(),
		@p6,
		@p7,
		@p8,
		@p9
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositContractCollectionQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, DepositContractCollectionQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.DepositsContractID),
		sql.Named("p2", a.CollectionDate),
		sql.Named("p3", a.CollectionAmount),
		sql.Named("p4", a.IsPaid),
		sql.Named("p5", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p6", a.TransferDate),
		sql.Named("p7", a.IsTransfer),
		sql.Named("p8", a.Remark),
		sql.Named("p9", a.InterestAmount),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (sj *sqlDepositContractCollectionRepository) StoreBase(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error) {
	a.ID = guid.New().StringUpper()
	DepositContractCollectionQuery := `
	INSERT INTO DepositContractCollection (
	   ID
	   ,DepositsContractID
	   ,CollectionDate
	   ,CollectionAmount
	   ,IsPaid
	   ,UserInsert
	   ,DateInsert
	   ,TransferDate
	   ,IsTransfer
	   ,Remark
	   ,InterestAmount
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		@p10,
		@p6,
		@p7,
		@p8,
		@p9
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositContractCollectionQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, DepositContractCollectionQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.DepositsContractID),
		sql.Named("p2", a.CollectionDate),
		sql.Named("p3", a.CollectionAmount),
		sql.Named("p4", a.IsPaid),
		sql.Named("p5", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p6", a.TransferDate),
		sql.Named("p7", a.IsTransfer),
		sql.Named("p8", a.Remark),
		sql.Named("p9", a.InterestAmount),
		sql.Named("p10", a.DateInsert),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (sj *sqlDepositContractCollectionRepository) Delete(ctx context.Context, depositContractId string) error {

	Query := `
		Delete DepositContractCollection 
		Where DepositsContractID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("depositContractId", fmt.Sprintf("%v", depositContractId)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", depositContractId),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err
	}
	return nil
}

func (sj *sqlDepositContractCollectionRepository) OverWriteFromCore(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error) {
	if a.UserInsert == "" {
		a.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	DepositContractCollectionQuery := `
	INSERT INTO DepositContractCollection (
	   ID
	   ,DepositsContractID
	   ,CollectionDate
	   ,CollectionAmount
	   ,IsPaid
	   ,UserInsert
	   ,DateInsert
	   ,TransferDate
	   ,IsTransfer
	   ,Remark
	   ,InterestAmount
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		GETDATE(),
		@p6,
		@p7,
		@p8,
		@p9
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositContractCollectionQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, DepositContractCollectionQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.DepositsContractID),
		sql.Named("p2", a.CollectionDate),
		sql.Named("p3", a.CollectionAmount),
		sql.Named("p4", a.IsPaid),
		sql.Named("p5", a.UserInsert),
		sql.Named("p6", a.TransferDate),
		sql.Named("p7", a.IsTransfer),
		sql.Named("p8", a.Remark),
		sql.Named("p9", a.InterestAmount),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (sj *sqlDepositContractCollectionRepository) FetchForEOD(ctx context.Context, date time.Time) (error, []*models.DepositContractCollection) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Cast(DepositsContractID as varchar(36)) as DepositsContractID,
		Cast(CollectionDate as varchar(50)) as CollectionDate, 
		CollectionAmount,
		IsPaid,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(Cast(TransferDate as varchar(50)),'') as TransferDate,
		isnull(IsTransfer,0) as IsTransfer,
		isnull(Remark,'') as Remark,
		InterestAmount
	FROM 
		dbo.DepositContractCollection
	WHERE
		FORMAT(CollectionDate, 'yyyy-MM-dd') >= FORMAT(@p0, 'yyyy-MM-dd') AND IsPaid = 0
	order by CollectionDate 
		`
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", date))

	defer rows.Close()

	result := make([]*models.DepositContractCollection, 0)
	for rows.Next() {
		j := new(models.DepositContractCollection)
		err = rows.Scan(
			&j.ID,
			&j.DepositsContractID,
			&j.CollectionDate,
			&j.CollectionAmount,
			&j.IsPaid,
			&j.DateInsert,
			&j.TransferDate,
			&j.IsTransfer,
			&j.Remark,
			&j.InterestAmount,
		)
		if err != nil {
			fmt.Print(err)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return nil, result
}
func (sj *sqlDepositContractCollectionRepository) FetchForBreak(ctx context.Context, id string) (error, []*models.DepositContractCollection) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Cast(DepositsContractID as varchar(36)) as DepositsContractID,
		Cast(CollectionDate as varchar(50)) as CollectionDate, 
		CollectionAmount,
		IsPaid,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(Cast(TransferDate as varchar(50)),'') as TransferDate,
		isnull(IsTransfer,0) as IsTransfer,
		isnull(Remark,'') as Remark,
		InterestAmount
	FROM 
		dbo.DepositContractCollection
	WHERE
		DepositsContractID= @p0 AND IsPaid = 0
	order by CollectionDate 
		`
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id))

	defer rows.Close()

	result := make([]*models.DepositContractCollection, 0)
	for rows.Next() {
		j := new(models.DepositContractCollection)
		err = rows.Scan(
			&j.ID,
			&j.DepositsContractID,
			&j.CollectionDate,
			&j.CollectionAmount,
			&j.IsPaid,
			&j.DateInsert,
			&j.TransferDate,
			&j.IsTransfer,
			&j.Remark,
			&j.InterestAmount,
		)
		if err != nil {
			fmt.Print(err)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return nil, result
}
func (sj *sqlDepositContractCollectionRepository) UpdateForEOD(ctx context.Context, id string, amount float64) (bool, error) {
	Query := `
	Update 
		DepositContractCollection
	set 
		InterestAmount = @p1 , IsPaid = 1
	Where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("amount", fmt.Sprintf("%v", amount)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("p1", amount),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
func (sj *sqlDepositContractCollectionRepository) UpdateAmountForEOD(ctx context.Context, id string, collectAmount float64) (bool, error) {
	Query := `
	Update 
		DepositContractCollection
	set 
		CollectionAmount = @p1
	Where 
		DepositsContractID = @p0 AND IsPaid = 0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("collectAmount", fmt.Sprintf("%v", collectAmount)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("p1", collectAmount),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}

func (sj *sqlDepositContractCollectionRepository) GetByContractId(ctx context.Context, id string) (error, []*models.DepositContractCollection) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Cast(DepositsContractID as varchar(36)) as DepositsContractID,
		Cast(CollectionDate as varchar(50)) as CollectionDate, 
		CollectionAmount,
		IsPaid,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(Cast(TransferDate as varchar(50)),'') as TransferDate,
		isnull(IsTransfer,0) as IsTransfer,
		isnull(Remark,'') as Remark,
		InterestAmount
	FROM dbo.DepositContractCollection 
	WHERE
		DepositsContractID = @p0
	order by 
		CollectionDate DESC
		`
	query += ``
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id))

	defer rows.Close()

	result := make([]*models.DepositContractCollection, 0)
	for rows.Next() {
		j := new(models.DepositContractCollection)
		err = rows.Scan(
			&j.ID,
			&j.DepositsContractID,
			&j.CollectionDate,
			&j.CollectionAmount,
			&j.IsPaid,
			&j.DateInsert,
			&j.TransferDate,
			&j.IsTransfer,
			&j.Remark,
			&j.InterestAmount,
		)
		if err != nil {
			fmt.Print(err)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return nil, result
}
func (sj *sqlDepositContractCollectionRepository) UpdateIsPaidForEOD(ctx context.Context, id string) (bool, error) {
	Query := `
	Update 
		DepositContractCollection
	set 
		IsPaid = 1
	Where 
		DepositsContractID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
func (sj *sqlDepositContractCollectionRepository) UpdateStatusForBreak(ctx context.Context, id string, date time.Time) (bool, error) {
	Query := `
	Update 
		DepositContractCollection
	set 
		IsPaid = 1 Where 
		DepositsContractID = @p0 AND FORMAT(CollectionDate, 'yyyy-MM-dd') <= FORMAT(@p1, 'yyyy-MM-dd')`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("date", fmt.Sprintf("%v", date)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("p1", date),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
func (sj *sqlDepositContractCollectionRepository) UpdateInterestForBreak(ctx context.Context, id string, amount float64) (bool, error) {
	Query := `
	Update 
		DepositContractCollection
	set 
		InterestAmount = @p1
	Where 
		DepositsContractID = @p0 AND IsPaid = 0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
		zap.String("collectAmount", fmt.Sprintf("%v", amount)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("p1", amount),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
