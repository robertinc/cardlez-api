package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlDepositRepository struct {
	Conn *sql.DB
}

func NewSqlDepositRepository() deposit.DepositRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlDepositRepository{conn}
}

func NewSqlDepositRepositoryV2(Conn *sql.DB) deposit.DepositRepository {
	conn := Conn
	return &sqlDepositRepository{conn}
}
func (sj *sqlDepositRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.Deposit) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(CustomerID as varchar(36)) as CustomerID, 
		DepositsName,
		SourceFund,
		Cast(DebitAccountID as varchar(36)) as DebitAccountID,
		DestinationFund,
		InterestCapitalisation,
		Remarks,
		DepositType,
		RecordStatus,
		Cast(DateInsert as varchar(50)) as DateInsert
	FROM dbo.Deposit 
		`
	query += ` where RecordStatus = '10' `
	if search != "ALL" && search != "" {
		query += ` and ` + search + ` like @p0`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.Deposit, 0)
	for rows.Next() {
		j := new(models.Deposit)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.CustomerID,
			&j.DepositsName,
			&j.SourceFund,
			&j.DebitAccountID,
			&j.DestinationFund,
			&j.InterestCapitalisation,
			&j.Remarks,
			&j.DepositType,
			&j.RecordStatus,
			&j.DateInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlDepositRepository) FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Deposit) {
	query := `
			SELECT  
			Cast(a.ID as varchar(36)) as ID,
			a.Code,
			Cast(CustomerID as varchar(36)) as CustomerID, 
			DepositsName,
			SourceFund,
			Cast(DebitAccountID as varchar(36)) as DebitAccountID,
			DestinationFund,
			InterestCapitalisation,
			Remarks,
			DepositType,
			a.RecordStatus,
			Cast(a.DateInsert as varchar(50)) as DateInsert,
			b.MemberName as CustomerName
			FROM dbo.Deposit a 
			inner join Member b on a.CustomerID=b.ID
		`
	query += ` where a.RecordStatus != 0 `
	if search != "ALL" && search != "" {
		query += ` and a.` + search + ` like @p0`
	}
	query += ` order by a.DateInsert desc `
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.Deposit, 0)
	for rows.Next() {
		j := new(models.Deposit)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.CustomerID,
			&j.DepositsName,
			&j.SourceFund,
			&j.DebitAccountID,
			&j.DestinationFund,
			&j.InterestCapitalisation,
			&j.Remarks,
			&j.DepositType,
			&j.RecordStatus,
			&j.DateInsert,
			&j.CustomerName,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlDepositRepository) FetchNew(ctx context.Context, search string, keyword string) (error, []*models.Deposit) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(CustomerID as varchar(36)) as CustomerID, 
		DepositsName,
		SourceFund,
		Cast(DebitAccountID as varchar(36)) as DebitAccountID,
		DestinationFund,
		InterestCapitalisation,
		Remarks,
		DepositType,
		RecordStatus,
		Cast(DateInsert as varchar(50)) as DateInsert
	FROM dbo.Deposit 
		`
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.Deposit, 0)
	for rows.Next() {
		j := new(models.Deposit)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.CustomerID,
			&j.DepositsName,
			&j.SourceFund,
			&j.DebitAccountID,
			&j.DestinationFund,
			&j.InterestCapitalisation,
			&j.Remarks,
			&j.DepositType,
			&j.RecordStatus,
			&j.DateInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlDepositRepository) Store(ctx context.Context, a *models.Deposit) (*models.Deposit, error) {
	a.ID = guid.New().StringUpper()

	t := time.Now()
	a.DateInsert = t.Format("2006-01-02 15:04:05")
	a.UserInsert = ctx.Value(service.CtxUserID).(string)
	depositQuery := `
	INSERT INTO Deposit (
	   ID
      ,Code
      ,CustomerID
      ,DepositsName
      ,CurrencyID
      ,CompanyUserID
      ,SourceFund
      ,DebitAccountID
      ,DestinationFund
      ,InterestCapitalisation
      ,Remarks
      ,DepositType
      ,RecordStatus
	  ,DateInsert
	  ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3, 
		@CurrencyID, 
		@CompanyUserID, 
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12 
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CustomerID),
		sql.Named("p3", a.DepositsName),
		sql.Named("p4", a.SourceFund),
		sql.Named("p5", a.DebitAccountID),
		sql.Named("p6", a.DestinationFund),
		sql.Named("p7", a.InterestCapitalisation),
		sql.Named("p8", a.Remarks),
		sql.Named("p9", a.DepositType),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.DateInsert),
		sql.Named("p12", a.UserInsert),
		sql.Named("CurrencyID", a.CurrencyID),
		sql.Named("CompanyUserID", a.CompanyUserID),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlDepositRepository) StoreBase(ctx context.Context, a *models.Deposit) (*models.Deposit, error) {
	a.ID = guid.New().StringUpper()
	a.UserInsert = ctx.Value(service.CtxUserID).(string)
	depositQuery := `
	INSERT INTO Deposit (
	   ID
      ,Code
      ,CustomerID
      ,DepositsName
      ,CurrencyID
      ,CompanyUserID
      ,SourceFund
      ,DebitAccountID
      ,DestinationFund
      ,InterestCapitalisation
      ,Remarks
      ,DepositType
      ,RecordStatus
	  ,DateInsert
	  ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3, 
		@CurrencyID, 
		@CompanyUserID, 
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12 
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CustomerID),
		sql.Named("p3", a.DepositsName),
		sql.Named("p4", a.SourceFund),
		sql.Named("p5", a.DebitAccountID),
		sql.Named("p6", a.DestinationFund),
		sql.Named("p7", a.InterestCapitalisation),
		sql.Named("p8", a.Remarks),
		sql.Named("p9", a.DepositType),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.DateInsert),
		sql.Named("p12", a.UserInsert),
		sql.Named("CurrencyID", a.CurrencyID),
		sql.Named("CompanyUserID", a.CompanyUserID),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlDepositRepository) Update(ctx context.Context, a *models.Deposit) (*models.Deposit, error) {
	t := time.Now()
	a.DateUpdate = t.Format("2006-01-02 15:04:05")

	var param []string
	DepositQuery := `Update Deposit Set `
	if a.Code != "" {
		param = append(param, "Code = @p1")
	}
	if a.CustomerID != "" {
		param = append(param, "CustomerID = @p2")
	}
	if a.DepositsName != "" {
		param = append(param, "DepositsName = @p3")
	}
	if a.CurrencyID != "" {
		param = append(param, "CurrencyID = @p4")
	}
	if a.CompanyUserID != "" {
		param = append(param, "CompanyUserID = @p5")
	}
	if a.SourceFund != -1 {
		param = append(param, "SourceFund = @p6")
	}
	if a.DebitAccountID != "" {
		param = append(param, "DebitAccountID = @p7")
	}
	if a.DestinationFund != -1 {
		param = append(param, "DestinationFund = @p8")
	}
	if a.InterestCapitalisation != -1 {
		param = append(param, "InterestCapitalisation = @p9")
	}
	if a.Remarks != "" {
		param = append(param, "Remarks = @p10")
	}
	if a.DepositType != -1 {
		param = append(param, "DepositType = @p11")
	}
	if a.RecordStatus != -1 {
		param = append(param, "RecordStatus = @p12")
	}
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = GETDATE()")
	DepositQuery += strings.Join(param, ",")
	DepositQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, DepositQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CustomerID),
		sql.Named("p3", a.DepositsName),
		sql.Named("p4", a.CurrencyID),
		sql.Named("p5", a.CompanyUserID),
		sql.Named("p6", a.SourceFund),
		sql.Named("p7", a.DebitAccountID),
		sql.Named("p8", a.DestinationFund),
		sql.Named("p9", a.InterestCapitalisation),
		sql.Named("p10", a.Remarks),
		sql.Named("p11", a.DepositType),
		sql.Named("p12", a.RecordStatus),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlDepositRepository) UpdateBase(ctx context.Context, a *models.Deposit) (*models.Deposit, error) {
	var param []string
	DepositQuery := `Update Deposit Set `
	if a.Code != "" {
		param = append(param, "Code = @p1")
	}
	if a.CustomerID != "" {
		param = append(param, "CustomerID = @p2")
	}
	if a.DepositsName != "" {
		param = append(param, "DepositsName = @p3")
	}
	if a.CurrencyID != "" {
		param = append(param, "CurrencyID = @p4")
	}
	if a.CompanyUserID != "" {
		param = append(param, "CompanyUserID = @p5")
	}
	if a.SourceFund != -1 {
		param = append(param, "SourceFund = @p6")
	}
	if a.DebitAccountID != "" {
		param = append(param, "DebitAccountID = @p7")
	}
	if a.DestinationFund != -1 {
		param = append(param, "DestinationFund = @p8")
	}
	if a.InterestCapitalisation != -1 {
		param = append(param, "InterestCapitalisation = @p9")
	}
	if a.Remarks != "" {
		param = append(param, "Remarks = @p10")
	}
	if a.DepositType != -1 {
		param = append(param, "DepositType = @p11")
	}
	if a.RecordStatus != -1 {
		param = append(param, "RecordStatus = @p12")
	}
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = @p13")
	DepositQuery += strings.Join(param, ",")
	DepositQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, DepositQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CustomerID),
		sql.Named("p3", a.DepositsName),
		sql.Named("p4", a.CurrencyID),
		sql.Named("p5", a.CompanyUserID),
		sql.Named("p6", a.SourceFund),
		sql.Named("p7", a.DebitAccountID),
		sql.Named("p8", a.DestinationFund),
		sql.Named("p9", a.InterestCapitalisation),
		sql.Named("p10", a.Remarks),
		sql.Named("p11", a.DepositType),
		sql.Named("p12", a.RecordStatus),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p13", a.DateUpdate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlDepositRepository) Delete(ctx context.Context, id string) error {

	Query := `
		Delete Deposit 
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err
	}
	return nil
}

func (sj *sqlDepositRepository) OverWriteFromCore(ctx context.Context, a *models.Deposit) (*models.Deposit, error) {
	if a.UserInsert == "" {
		a.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	depositQuery := `
	INSERT INTO Deposit (
	   ID
      ,Code
      ,CustomerID
      ,DepositsName
      ,CurrencyID
      ,CompanyUserID
      ,SourceFund
      ,DebitAccountID
      ,DestinationFund
      ,InterestCapitalisation
      ,Remarks
      ,DepositType
      ,RecordStatus
	  ,DateInsert
	  ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3, 
		@CurrencyID, 
		@CompanyUserID, 
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12 
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.CustomerID),
		sql.Named("p3", a.DepositsName),
		sql.Named("p4", a.SourceFund),
		sql.Named("p5", a.DebitAccountID),
		sql.Named("p6", a.DestinationFund),
		sql.Named("p7", a.InterestCapitalisation),
		sql.Named("p8", a.Remarks),
		sql.Named("p9", a.DepositType),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.DateInsert),
		sql.Named("p12", a.UserInsert),
		sql.Named("CurrencyID", a.CurrencyID),
		sql.Named("CompanyUserID", a.CompanyUserID),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlDepositRepository) GetById(ctx context.Context, id string) (*models.Deposit, error) {
	var j models.Deposit
	DepositQuery := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(CustomerID as varchar(36)) as CustomerID, 
		DepositsName,
		SourceFund,
		Cast(DebitAccountID as varchar(36)) as DebitAccountID,
		DestinationFund,
		InterestCapitalisation,
		Remarks,
		DepositType,
		RecordStatus,
		Cast(DateInsert as varchar(50)) as DateInsert
	FROM 
		dbo.Deposit
	WHERE
		ID = @p0
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositQuery)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(DepositQuery,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.CustomerID,
		&j.DepositsName,
		&j.SourceFund,
		&j.DebitAccountID,
		&j.DestinationFund,
		&j.InterestCapitalisation,
		&j.Remarks,
		&j.DepositType,
		&j.RecordStatus,
		&j.DateInsert,
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return &j, nil
}
