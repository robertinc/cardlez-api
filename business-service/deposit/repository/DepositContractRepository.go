package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlDepositContractRepository struct {
	Conn *sql.DB
}

func NewSqlDepositContractRepository() deposit.DepositContractRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlDepositContractRepository{conn}
}
func NewSqlDepositContractRepositoryV2(Conn *sql.DB) deposit.DepositContractRepository {
	conn := Conn
	return &sqlDepositContractRepository{conn}
}
func (sj *sqlDepositContractRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContract) {
	query := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(DepositsID as varchar(36)) as DepositsID, 
		Cast(RetailProductID as varchar(36)) as RetailProductID,
		DepositsAmount,
		Cast(StartDate as varchar(50)) as StartDate,
		InterestPercentage,
		RolloverMethod,
		InterestPaymentDue,
		RolloverMonth,
		PostingForwardType,
		Cast(CreditAccountID as varchar(36)) as CreditAccountID,
		Remarks,
		RecordStatus,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(TypeAro,'0') as TypeAro,
		isnull(PeriodeBunga,'0') as PeriodeBunga,
		isnull(BaseRateKey,'0') as BaseRateKey
	FROM dbo.DepositContract 
		`
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	}
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))

	defer rows.Close()

	result := make([]*models.DepositContract, 0)
	for rows.Next() {
		j := new(models.DepositContract)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.DepositsID,
			&j.RetailProductID,
			&j.DepositsAmount,
			&j.StartDate,
			&j.InterestPercentage,
			&j.RolloverMethod,
			&j.InterestPaymentDue,
			&j.RolloverMonth,
			&j.PostingForwardType,
			&j.CreditAccountID,
			&j.Remarks,
			&j.RecordStatus,
			&j.DateInsert,
			&j.TypeAro,
			&j.PeriodeBunga,
			&j.BaseRateKey,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return nil, result
}
func (sj *sqlDepositContractRepository) GetById(ctx context.Context, id string) (*models.DepositContract, error) {
	var j models.DepositContract
	DepositQuery := `
	SELECT  
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(DepositsID as varchar(36)) as DepositsID, 
		Cast(RetailProductID as varchar(36)) as RetailProductID,
		DepositsAmount,
		Cast(StartDate as varchar(50)) as StartDate,
		InterestPercentage,
		RolloverMethod,
		InterestPaymentDue,
		RolloverMonth,
		PostingForwardType,
		Cast(CreditAccountID as varchar(36)) as CreditAccountID,
		Remarks,
		RecordStatus,
		Cast(DateInsert as varchar(50)) as DateInsert,
		isnull(TypeAro,'0') as TypeAro,
		isnull(PeriodeBunga,'0') as PeriodeBunga,
		isnull(BaseRateKey,'0') as BaseRateKey
	FROM dbo.DepositContract
	WHERE
		ID = @p0
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", DepositQuery)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(DepositQuery,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.DepositsID,
		&j.RetailProductID,
		&j.DepositsAmount,
		&j.StartDate,
		&j.InterestPercentage,
		&j.RolloverMethod,
		&j.InterestPaymentDue,
		&j.RolloverMonth,
		&j.PostingForwardType,
		&j.CreditAccountID,
		&j.Remarks,
		&j.RecordStatus,
		&j.DateInsert,
		&j.TypeAro,
		&j.PeriodeBunga,
		&j.BaseRateKey,
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return &j, nil
}

func (sj *sqlDepositContractRepository) Store(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error) {
	a.ID = guid.New().StringUpper()
	t := time.Now()
	a.DateInsert = t.Format("2006-01-02 15:04:05")

	depositContractQuery := `
	INSERT INTO DepositContract (
	   ID
	   ,Code
	   ,DepositsID
	   ,RetailProductID
	   ,DepositsAmount
	   ,StartDate
	   ,InterestPercentage
	   ,RolloverMethod
	   ,InterestPaymentDue
	   ,RolloverMonth
	   ,PostingForwardType
	   ,CreditAccountID
	   ,Remarks
	   ,RecordStatus
	   ,DateInsert
	   ,TypeAro
	   ,PeriodeBunga
	   ,BaseRateKey
	   ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12, 
		@p13, 
		@p14,
		@p15, 
		@p16, 
		@p17, 
		@p18
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositContractQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositContractQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DepositsID),
		sql.Named("p3", a.RetailProductID),
		sql.Named("p4", a.DepositsAmount),
		sql.Named("p5", a.StartDate),
		sql.Named("p6", a.InterestPercentage),
		sql.Named("p7", a.RolloverMethod),
		sql.Named("p8", a.InterestPaymentDue),
		sql.Named("p9", a.RolloverMonth),
		sql.Named("p10", a.PostingForwardType),
		sql.Named("p11", a.CreditAccountID),
		sql.Named("p12", a.Remarks),
		sql.Named("p13", a.RecordStatus),
		sql.Named("p14", a.DateInsert),
		sql.Named("p15", a.TypeAro),
		sql.Named("p16", a.PeriodeBunga),
		sql.Named("p17", a.BaseRateKey),
		sql.Named("p18", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}
func (sj *sqlDepositContractRepository) StoreBase(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error) {
	a.ID = guid.New().StringUpper()

	depositContractQuery := `
	INSERT INTO DepositContract (
	   ID
	   ,Code
	   ,DepositsID
	   ,RetailProductID
	   ,DepositsAmount
	   ,StartDate
	   ,InterestPercentage
	   ,RolloverMethod
	   ,InterestPaymentDue
	   ,RolloverMonth
	   ,PostingForwardType
	   ,CreditAccountID
	   ,Remarks
	   ,RecordStatus
	   ,DateInsert
	   ,TypeAro
	   ,PeriodeBunga
	   ,BaseRateKey
	   ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12, 
		@p13, 
		@p14,
		@p15, 
		@p16, 
		@p17, 
		@p18
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositContractQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositContractQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DepositsID),
		sql.Named("p3", a.RetailProductID),
		sql.Named("p4", a.DepositsAmount),
		sql.Named("p5", a.StartDate),
		sql.Named("p6", a.InterestPercentage),
		sql.Named("p7", a.RolloverMethod),
		sql.Named("p8", a.InterestPaymentDue),
		sql.Named("p9", a.RolloverMonth),
		sql.Named("p10", a.PostingForwardType),
		sql.Named("p11", a.CreditAccountID),
		sql.Named("p12", a.Remarks),
		sql.Named("p13", a.RecordStatus),
		sql.Named("p14", a.DateInsert),
		sql.Named("p15", a.TypeAro),
		sql.Named("p16", a.PeriodeBunga),
		sql.Named("p17", a.BaseRateKey),
		sql.Named("p18", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (sj *sqlDepositContractRepository) Delete(ctx context.Context, depositId string) error {

	Query := `
		Delete DepositContract 
		Where DepositsID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("depositId", fmt.Sprintf("%v", depositId)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", depositId),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err
	}
	return nil
}

func (sj *sqlDepositContractRepository) OverWriteFromCore(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error) {
	if a.UserInsert == "" {
		a.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	depositContractQuery := `
	INSERT INTO DepositContract (
	   ID
	   ,Code
	   ,DepositsID
	   ,RetailProductID
	   ,DepositsAmount
	   ,StartDate
	   ,InterestPercentage
	   ,RolloverMethod
	   ,InterestPaymentDue
	   ,RolloverMonth
	   ,PostingForwardType
	   ,CreditAccountID
	   ,Remarks
	   ,RecordStatus
	   ,DateInsert
	   ,TypeAro
	   ,PeriodeBunga
	   ,BaseRateKey
	   ,UserInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3,
		@p4,
		@p5, 
		@p6,  
		@p7,  
		@p8, 
		@p9, 
		@p10, 
		@p11,
		@p12, 
		@p13, 
		@p14,
		@p15, 
		@p16, 
		@p17, 
		@p18
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", depositContractQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, depositContractQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DepositsID),
		sql.Named("p3", a.RetailProductID),
		sql.Named("p4", a.DepositsAmount),
		sql.Named("p5", a.StartDate),
		sql.Named("p6", a.InterestPercentage),
		sql.Named("p7", a.RolloverMethod),
		sql.Named("p8", a.InterestPaymentDue),
		sql.Named("p9", a.RolloverMonth),
		sql.Named("p10", a.PostingForwardType),
		sql.Named("p11", a.CreditAccountID),
		sql.Named("p12", a.Remarks),
		sql.Named("p13", a.RecordStatus),
		sql.Named("p14", a.DateInsert),
		sql.Named("p15", a.TypeAro),
		sql.Named("p16", a.PeriodeBunga),
		sql.Named("p17", a.BaseRateKey),
		sql.Named("p18", a.UserInsert),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (sj *sqlDepositContractRepository) UpdatePenutupan(ctx context.Context, id string) (bool, error) {
	Query := `
	Update 
		DepositContract
	set 
		RolloverMethod = 2
	Where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
