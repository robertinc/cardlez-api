package deposit

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/models"
)

type DepositRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	FetchNew(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	Store(ctx context.Context, a *models.Deposit) (*models.Deposit, error)
	StoreBase(ctx context.Context, a *models.Deposit) (*models.Deposit, error)
	Update(ctx context.Context, a *models.Deposit) (*models.Deposit, error)
	UpdateBase(ctx context.Context, a *models.Deposit) (*models.Deposit, error)
	Delete(ctx context.Context, id string) error
	OverWriteFromCore(ctx context.Context, a *models.Deposit) (*models.Deposit, error)
	GetById(ctx context.Context, id string) (*models.Deposit, error)
}
type DepositContractRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContract)
	Store(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error)
	StoreBase(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error)
	Delete(ctx context.Context, id string) error
	OverWriteFromCore(ctx context.Context, a *models.DepositContract) (*models.DepositContract, error)
	UpdatePenutupan(ctx context.Context, id string) (bool, error)
	GetById(ctx context.Context, id string) (*models.DepositContract, error)
}
type DepositContractCollectionRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContractCollection)
	Store(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error)
	StoreBase(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error)
	Delete(ctx context.Context, id string) error
	OverWriteFromCore(ctx context.Context, a *models.DepositContractCollection) (*models.DepositContractCollection, error)
	FetchForEOD(ctx context.Context, date time.Time) (error, []*models.DepositContractCollection)
	UpdateForEOD(ctx context.Context, id string, amount float64) (bool, error)
	GetByContractId(ctx context.Context, id string) (error, []*models.DepositContractCollection)
	UpdateAmountForEOD(ctx context.Context, id string, amount float64) (bool, error)
	UpdateIsPaidForEOD(ctx context.Context, id string) (bool, error)
	FetchForBreak(ctx context.Context, id string) (error, []*models.DepositContractCollection)
	UpdateStatusForBreak(ctx context.Context, id string, date time.Time) (bool, error)
	UpdateInterestForBreak(ctx context.Context, id string, amount float64) (bool, error)
}
