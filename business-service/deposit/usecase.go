package deposit

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/models"
)

type DepositUsecase interface {
	Store(context.Context, *models.Deposit) (*models.Deposit, error)
	StoreBase(context.Context, *models.Deposit) (*models.Deposit, error)
	Update(context.Context, *models.Deposit) (*models.Deposit, error)
	UpdateBase(context.Context, *models.Deposit) (*models.Deposit, error)
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	FetchBase(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	FetchNew(ctx context.Context, search string, keyword string) (error, []*models.Deposit)
	OverWriteFromCore(context.Context, *models.Deposit) (*models.Deposit, error)
	Delete(ctx context.Context, id string) error
	GetById(ctx context.Context, id string) (*models.Deposit, error)
}
type DepositContractUsecase interface {
	Store(ctx context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error)
	StoreBase(ctx context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error)
	StoreBaseNew(ctx context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error)
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContract)
	OverWriteFromCore(context.Context, *models.DepositContract) (*models.DepositContract, error)
	Delete(ctx context.Context, depositId string) error
	UpdatePenutupan(ctx context.Context, id string) (bool, error)
	GetById(ctx context.Context, id string) (*models.DepositContract, error)
}
type DepositContractCollectionUsecase interface {
	Store(context.Context, *models.DepositContractCollection) (*models.DepositContractCollection, error)
	StoreBase(context.Context, *models.DepositContractCollection) (*models.DepositContractCollection, error)
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.DepositContractCollection)
	OverWriteFromCore(context.Context, *models.DepositContractCollection) (*models.DepositContractCollection, error)
	Delete(ctx context.Context, depositContractId string) error
	FetchForEOD(ctx context.Context, date time.Time) (error, []*models.DepositContractCollection)
	UpdateForEOD(ctx context.Context, id string, amount float64) (bool, error)
	GetByContractId(ctx context.Context, id string) (error, []*models.DepositContractCollection)
	UpdateAmountForEOD(ctx context.Context, id string, amount float64) (bool, error)
	UpdateIsPaidForEOD(ctx context.Context, id string) (bool, error)
	FetchForBreak(ctx context.Context, id string) (error, []*models.DepositContractCollection)
	UpdateStatusForBreak(ctx context.Context, id string, date time.Time) (bool, error)
	UpdateInterestForBreak(ctx context.Context, id string, amount float64) (bool, error)
}
