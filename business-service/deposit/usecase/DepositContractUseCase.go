package usecase

import (
	"context"
	"errors"
	"time"

	"github.com/beevik/guid"
	cAccGroup "gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition"
	uAccGroup "gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition/usecase"
	cAccount "gitlab.com/robertinc/cardlez-api/business-service/account"
	uAccount "gitlab.com/robertinc/cardlez-api/business-service/account/usecase"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit/repository"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	usecaseMember "gitlab.com/robertinc/cardlez-api/business-service/member/usecase"
	cProduct "gitlab.com/robertinc/cardlez-api/business-service/product"
	"gitlab.com/robertinc/cardlez-api/models"
)

type DepositContractUsecase struct {
	DepositContractRepository deposit.DepositContractRepository
	contextTimeout            time.Duration
}

func NewDepositContractUsecase() deposit.DepositContractUsecase {
	return &DepositContractUsecase{
		DepositContractRepository: repository.NewSqlDepositContractRepository(),
		contextTimeout:            time.Second * time.Duration(models.Timeout()),
	}
}

func NewDepositContractUsecaseV2(depositcRepo deposit.DepositContractRepository) deposit.DepositContractUsecase {
	return &DepositContractUsecase{
		DepositContractRepository: depositcRepo,
		contextTimeout:            time.Second * time.Duration(models.Timeout()),
	}
}

func (a *DepositContractUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.DepositContract) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDepositContract := a.DepositContractRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listDepositContract
}

func (a *DepositContractUsecase) GetById(c context.Context, id string) (*models.DepositContract, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	Deposit, err := a.DepositContractRepository.GetById(ctx, id)
	if err != nil {
		return nil, err
	}
	return Deposit, nil
}
func (a *DepositContractUsecase) Store(c context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}

	//Create new Account for deposito
	buMember := usecaseMember.NewMemberUsecaseV2(ctx.Value("MemberRepository").(cMember.MemberRepository), ctx.Value("MemberMailboxRepository").(cMember.MemberMailboxRepository))
	member, err := buMember.GetByID(ctx, memberid)
	if err != nil {
		return nil, err
	}
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	u := uAccount.NewAccountUsecaseV2(accountRepo)
	Account := &models.Account{
		MemberID:     *memberid,
		ProductID:    products[0].ID,
		AccountName:  member.MemberName.String,
		AccountName2: products[0].ProductName,
		IsPrimary:    false,
		CompanyID:    products[0].CompanyID,
	}
	account, err := u.Store(ctx, Account)
	if err != nil {
		return nil, err
	}

	//get interestpercentage by accgroupconditionid
	accGroupRepo := ctx.Value("AccGroupConditionDetailRepository").(cAccGroup.AccGroupConditionDetailRepository)
	useCaseAccGroup := uAccGroup.NewAccGroupConditionDetailUsecaseV2(accGroupRepo)
	err, accGroupCondition := useCaseAccGroup.GetByAccGroupID(ctx, products[0].AccGroupConditionID, b.RolloverMonth)
	if err != nil {
		return nil, err
	}

	b.RetailProductID = products[0].ID
	b.InterestPercentage = accGroupCondition.Interest_Rate //4
	b.InterestPaymentDue = false
	b.PostingForwardType = 0
	b.BaseRateKey = 0
	b.RecordStatus = 2
	b.PeriodeBunga = 0
	b.CreditAccountID = account.ID
	depositContract, err := a.DepositContractRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}

	return depositContract, nil
}
func (a *DepositContractUsecase) StoreBase(c context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}

	//Create new Account for deposito
	buMember := usecaseMember.NewMemberUsecaseV2(ctx.Value("MemberRepository").(cMember.MemberRepository), ctx.Value("MemberMailboxRepository").(cMember.MemberMailboxRepository))
	member, err := buMember.GetByID(ctx, memberid)
	if err != nil {
		return nil, err
	}
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	u := uAccount.NewAccountUsecaseV2(accountRepo)
	Account := &models.Account{
		MemberID:     *memberid,
		ProductID:    products[0].ID,
		AccountName:  member.MemberName.String,
		AccountName2: products[0].ProductName,
		IsPrimary:    false,
		CompanyID:    products[0].CompanyID,
	}
	account, err := u.Store(ctx, Account)
	if err != nil {
		return nil, err
	}

	//get interestpercentage by accgroupconditionid
	accGroupRepo := ctx.Value("AccGroupConditionDetailRepository").(cAccGroup.AccGroupConditionDetailRepository)
	useCaseAccGroup := uAccGroup.NewAccGroupConditionDetailUsecaseV2(accGroupRepo)
	err, accGroupCondition := useCaseAccGroup.GetByAccGroupID(ctx, products[0].AccGroupConditionID, b.RolloverMonth)
	if err != nil {
		return nil, err
	}

	b.RetailProductID = products[0].ID
	b.InterestPercentage = accGroupCondition.Interest_Rate //4
	b.InterestPaymentDue = false
	b.PostingForwardType = 0
	b.BaseRateKey = 0
	b.RecordStatus = 2
	b.PeriodeBunga = 0
	b.CreditAccountID = account.ID
	depositContract, err := a.DepositContractRepository.StoreBase(ctx, b)
	if err != nil {
		return nil, err
	}

	return depositContract, nil
}
func (a *DepositContractUsecase) StoreBaseNew(c context.Context, b *models.DepositContract, memberid *string) (*models.DepositContract, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}

	//get interestpercentage by accgroupconditionid
	accGroupRepo := ctx.Value("AccGroupConditionDetailRepository").(cAccGroup.AccGroupConditionDetailRepository)
	useCaseAccGroup := uAccGroup.NewAccGroupConditionDetailUsecaseV2(accGroupRepo)
	err, accGroupCondition := useCaseAccGroup.GetByAccGroupID(ctx, products[0].AccGroupConditionID, b.RolloverMonth)
	if err != nil {
		return nil, err
	}

	b.RetailProductID = products[0].ID
	b.InterestPercentage = accGroupCondition.Interest_Rate //4
	b.InterestPaymentDue = false
	b.PostingForwardType = 0
	b.BaseRateKey = 0
	b.RecordStatus = 2
	b.PeriodeBunga = 0
	b.CreditAccountID = guid.New().StringUpper()
	depositContract, err := a.DepositContractRepository.StoreBase(ctx, b)
	if err != nil {
		return nil, err
	}

	return depositContract, nil
}
func (a *DepositContractUsecase) OverWriteFromCore(c context.Context, b *models.DepositContract) (*models.DepositContract, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	depositContract, err := a.DepositContractRepository.OverWriteFromCore(ctx, b)
	if err != nil {
		return nil, err
	}

	return depositContract, nil
}
func (a *DepositContractUsecase) Delete(c context.Context, depositId string) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	err := a.DepositContractRepository.Delete(ctx, depositId)
	if err != nil {
		return err
	}
	return nil
}
func (a *DepositContractUsecase) UpdatePenutupan(c context.Context, id string) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContract, err := a.DepositContractRepository.UpdatePenutupan(ctx, id)
	if err != nil {
		return false, err
	}
	return depositContract, nil
}
