package usecase

import (
	"context"
	"errors"
	"time"

	"github.com/beevik/guid"
	cAppConfig "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	cCOA "gitlab.com/robertinc/cardlez-api/business-service/coa"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	cDepositContract "gitlab.com/robertinc/cardlez-api/business-service/deposit"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit/repository"
	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	usecaseMember "gitlab.com/robertinc/cardlez-api/business-service/member/usecase"
	cProduct "gitlab.com/robertinc/cardlez-api/business-service/product"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type DepositUsecase struct {
	DepositRepository deposit.DepositRepository
	contextTimeout    time.Duration
}

func NewDepositUsecase() deposit.DepositUsecase {
	return &DepositUsecase{
		DepositRepository: repository.NewSqlDepositRepository(),
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func NewDepositUsecaseV2(depoRepo deposit.DepositRepository) deposit.DepositUsecase {
	return &DepositUsecase{
		DepositRepository: depoRepo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *DepositUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.Deposit) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDeposit := a.DepositRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listDeposit
}
func (a *DepositUsecase) FetchBase(c context.Context, search string, keyword string) (error, []*models.Deposit) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDeposit := a.DepositRepository.FetchBase(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listDeposit
}
func (a *DepositUsecase) FetchNew(c context.Context, search string, keyword string) (error, []*models.Deposit) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDeposit := a.DepositRepository.FetchNew(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listDeposit
}

func (a *DepositUsecase) Store(c context.Context, b *models.Deposit) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	err, GenerateCode := memberRepo.GenerateCode(ctx, "DEP")
	if err != nil {
		return nil, err
	}
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}
	b.DepositsName = products[0].ProductName
	b.Code = GenerateCode
	b.CurrencyID = "5DD09F82-2E56-42F6-B392-57D52D7F26C8"
	b.CompanyUserID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.SourceFund = 1
	b.DestinationFund = 1
	b.Remarks = "PLACEMENT DEPOSITO"
	b.DepositType = 2
	b.RecordStatus = 0

	deposit, err := a.DepositRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}
	return deposit, nil
}
func (a *DepositUsecase) StoreBase(c context.Context, b *models.Deposit) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	err, GenerateCode := memberRepo.GenerateCode(ctx, "DEP")
	if err != nil {
		return nil, err
	}
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}
	b.DepositsName = products[0].ProductName
	b.Code = GenerateCode
	b.CurrencyID = "5DD09F82-2E56-42F6-B392-57D52D7F26C8"
	b.CompanyUserID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.SourceFund = 1
	b.DestinationFund = 1
	b.Remarks = "PLACEMENT DEPOSITO"
	b.DepositType = 2
	b.RecordStatus = 0

	deposit, err := a.DepositRepository.StoreBase(ctx, b)
	if err != nil {
		return nil, err
	}
	return deposit, nil
}

func (a *DepositUsecase) Update(c context.Context, b *models.Deposit) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	if b.RecordStatus == 10 {
		err := a.InsertJournalDepositoBaru(ctx, b)
		if err != nil {
			return nil, err
		}
	}
	Deposit, err := a.DepositRepository.Update(ctx, b)
	if err != nil {
		return nil, err
	}
	return Deposit, nil
}
func (a *DepositUsecase) UpdateBase(c context.Context, b *models.Deposit) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	configRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	keyCore := "IsInvelliCore"
	isInvelliCore, err := configRepo.GetByConfigKey(ctx, &keyCore)
	if err != nil {
		return nil, err
	}
	keyMdw := "IsMiddleware"
	isMiddleware, err := configRepo.GetByConfigKey(ctx, &keyMdw)
	if err != nil {
		return nil, err
	}
	if isInvelliCore.ConfigValue == "0" {
		if b.RecordStatus == 10 {
			err = a.InsertJournalBaseDepositoBaru(ctx, b)
			if err != nil {
				return nil, err
			}
		}
	} else {
		if isMiddleware.ConfigValue == "0" { //jika bukan middleware
			err = a.InsertJournalBaseDepositoBaru(ctx, b)
			if err != nil {
				return nil, err
			}
		} else {
			if b.RecordStatus == 10 {
				err = a.InsertJournalBaseDepositoBaru(ctx, b)
				if err != nil {
					return nil, err
				}
			}
		}

	}
	b.DateUpdate = b.DateInsert
	Deposit, err := a.DepositRepository.UpdateBase(ctx, b)
	if err != nil {
		return nil, err
	}
	return Deposit, nil
}
func (a *DepositUsecase) GetById(c context.Context, id string) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	Deposit, err := a.DepositRepository.GetById(ctx, id)
	if err != nil {
		return nil, err
	}
	return Deposit, nil
}
func (a *DepositUsecase) InsertJournalDepositoBaru(ctx context.Context, b *models.Deposit) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0018") //deposito baru
	if err != nil {
		return err
	}
	repodc := ctx.Value("DepositContractRepository").(cDepositContract.DepositContractRepository)
	udc := NewDepositContractUsecaseV2(repodc)
	err, depositcontracts := udc.Fetch(ctx, "DepositsID", b.ID)
	if err != nil {
		return err
	}
	if len(depositcontracts) == 0 {
		return err
	}
	buMember := usecaseMember.NewMemberUsecaseV2(ctx.Value("MemberRepository").(cMember.MemberRepository), ctx.Value("MemberMailboxRepository").(cMember.MemberMailboxRepository))
	member, err := buMember.GetByID(ctx, &b.CustomerID)
	if err != nil {
		return err
	}

	notes := "Buat Deposito Baru atas nama " + member.MemberName.String
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.DateInsert,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	keyword := "COADebetDepositoBaru"
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           depositcontracts[0].DepositsAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: b.DebitAccountID,
	}
	journalDetailCreditID := guid.New().StringUpper()
	keyword = "COACreditDepositoBaru"
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          depositcontracts[0].DepositsAmount,
		Notes:           notes,
		ReferenceNumber: depositcontracts[0].CreditAccountID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *DepositUsecase) InsertJournalBaseDepositoBaru(ctx context.Context, b *models.Deposit) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0018") //deposito baru
	if err != nil {
		return err
	}
	repodc := ctx.Value("DepositContractRepository").(cDepositContract.DepositContractRepository)
	udc := NewDepositContractUsecaseV2(repodc)
	err, depositcontracts := udc.Fetch(ctx, "DepositsID", b.ID)
	if err != nil {
		return err
	}
	if len(depositcontracts) == 0 {
		return err
	}
	buMember := usecaseMember.NewMemberUsecaseV2(ctx.Value("MemberRepository").(cMember.MemberRepository), ctx.Value("MemberMailboxRepository").(cMember.MemberMailboxRepository))
	member, err := buMember.GetByID(ctx, &b.CustomerID)
	if err != nil {
		return err
	}

	notes := "Buat Deposito Baru atas nama " + member.MemberName.String
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.DateInsert,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailDebitID := guid.New().StringUpper()
	keyword := "COADebetDepositoBaru"
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           COA.ID,
		Debit:           depositcontracts[0].DepositsAmount,
		Credit:          0,
		Notes:           notes,
		ReferenceNumber: b.DebitAccountID,
	}
	journalDetailCreditID := guid.New().StringUpper()
	keyword = "COACreditDepositoBaru"
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          depositcontracts[0].DepositsAmount,
		Notes:           notes,
		ReferenceNumber: depositcontracts[0].CreditAccountID,
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *DepositUsecase) OverWriteFromCore(c context.Context, b *models.Deposit) (*models.Deposit, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	err, products := productRepo.Fetch(ctx, "ProductType", "2")
	if err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("Error, No Retail Product Available")
	}
	b.DepositsName = products[0].ProductName
	b.CompanyUserID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	//b.RecordStatus = 0

	deposit, err := a.DepositRepository.OverWriteFromCore(ctx, b)
	if err != nil {
		return nil, err
	}
	return deposit, nil
}

func (a *DepositUsecase) Delete(c context.Context, id string) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	err := a.DepositRepository.Delete(ctx, id)
	if err != nil {
		return err
	}
	return nil
}
