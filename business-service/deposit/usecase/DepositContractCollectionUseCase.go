package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/deposit"
	"gitlab.com/robertinc/cardlez-api/business-service/deposit/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type DepositContractCollectionUsecase struct {
	DepositContractCollectionRepository deposit.DepositContractCollectionRepository
	contextTimeout                      time.Duration
}

func NewDepositContractCollectionUsecase() deposit.DepositContractCollectionUsecase {
	return &DepositContractCollectionUsecase{
		DepositContractCollectionRepository: repository.NewSqlDepositContractCollectionRepository(),
		contextTimeout:                      time.Second * time.Duration(models.Timeout()),
	}
}

func NewDepositContractCollectionUsecaseV2(depositcRepo deposit.DepositContractCollectionRepository) deposit.DepositContractCollectionUsecase {
	return &DepositContractCollectionUsecase{
		DepositContractCollectionRepository: depositcRepo,
		contextTimeout:                      time.Second * time.Duration(models.Timeout()),
	}
}

func (a *DepositContractCollectionUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.DepositContractCollection) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDepositContractCollection := a.DepositContractCollectionRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	return nil, listDepositContractCollection
}

func (a *DepositContractCollectionUsecase) Store(c context.Context, b *models.DepositContractCollection) (*models.DepositContractCollection, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	b.IsPaid = false

	DepositContractCollection, err := a.DepositContractCollectionRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return DepositContractCollection, nil
}

func (a *DepositContractCollectionUsecase) StoreBase(c context.Context, b *models.DepositContractCollection) (*models.DepositContractCollection, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	b.IsPaid = false

	DepositContractCollection, err := a.DepositContractCollectionRepository.StoreBase(ctx, b)

	if err != nil {
		return nil, err
	}
	return DepositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) OverWriteFromCore(c context.Context, b *models.DepositContractCollection) (*models.DepositContractCollection, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	DepositContractCollection, err := a.DepositContractCollectionRepository.OverWriteFromCore(ctx, b)

	if err != nil {
		return nil, err
	}
	return DepositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) Delete(c context.Context, depositContractId string) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	err := a.DepositContractCollectionRepository.Delete(ctx, depositContractId)
	if err != nil {
		return err
	}
	return nil
}
func (a *DepositContractCollectionUsecase) FetchForEOD(c context.Context, date time.Time) (error, []*models.DepositContractCollection) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDepositContractCollection := a.DepositContractCollectionRepository.FetchForEOD(ctx, date)
	if err != nil {
		return err, nil
	}
	return nil, listDepositContractCollection
}
func (a *DepositContractCollectionUsecase) UpdateForEOD(c context.Context, id string, amount float64) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContractCollection, err := a.DepositContractCollectionRepository.UpdateForEOD(ctx, id, amount)
	if err != nil {
		return false, err
	}
	return depositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) GetByContractId(c context.Context, id string) (error, []*models.DepositContractCollection) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDepositContractCollection := a.DepositContractCollectionRepository.GetByContractId(ctx, id)
	if err != nil {
		return err, nil
	}
	return nil, listDepositContractCollection
}
func (a *DepositContractCollectionUsecase) UpdateAmountForEOD(c context.Context, id string, amount float64) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContractCollection, err := a.DepositContractCollectionRepository.UpdateAmountForEOD(ctx, id, amount)
	if err != nil {
		return false, err
	}
	return depositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) UpdateIsPaidForEOD(c context.Context, id string) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContractCollection, err := a.DepositContractCollectionRepository.UpdateIsPaidForEOD(ctx, id)
	if err != nil {
		return false, err
	}
	return depositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) FetchForBreak(c context.Context, id string) (error, []*models.DepositContractCollection) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDepositContractCollection := a.DepositContractCollectionRepository.FetchForBreak(ctx, id)
	if err != nil {
		return err, nil
	}
	return nil, listDepositContractCollection
}
func (a *DepositContractCollectionUsecase) UpdateStatusForBreak(c context.Context, id string, date time.Time) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContractCollection, err := a.DepositContractCollectionRepository.UpdateStatusForBreak(ctx, id, date)
	if err != nil {
		return false, err
	}
	return depositContractCollection, nil
}
func (a *DepositContractCollectionUsecase) UpdateInterestForBreak(c context.Context, id string, amount float64) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	depositContractCollection, err := a.DepositContractCollectionRepository.UpdateInterestForBreak(ctx, id, amount)
	if err != nil {
		return false, err
	}
	return depositContractCollection, nil
}
