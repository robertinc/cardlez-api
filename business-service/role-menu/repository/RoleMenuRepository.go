package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/role-menu"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlRoleMenuRepository struct {
	Conn *sql.DB
}

func NewSqlRoleMenuRepository() rolemenu.RoleMenuRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlRoleMenuRepository{conn}
}

func NewSqlRoleMenuRepositoryV2(Conn *sql.DB) rolemenu.RoleMenuRepository {
	conn := Conn
	return &sqlRoleMenuRepository{conn}
}

func (sj *sqlRoleMenuRepository) Fetch(ctx context.Context) (error, []*models.RoleMenu) {
	query := `
	SELECT 
		Cast(rm.ID as varchar(36)) as ID,
		Cast(CompanyRoleID as varchar(36)),
		Cast(MenuID as varchar(36)),
		AccessLevel,
		cr.RoleName,
		m.MenuName,
		Cast(m.ParentMenuID as varchar(36)) as ParentMenuID
	FROM 
		RoleMenu rm
		INNER JOIN CompanyUserRole cr on rm.CompanyRoleID = cr.ID
		INNER JOIN Menu m on rm.MenuID = m.ID
		`
	result := make([]*models.RoleMenu, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.RoleMenu)
		err = rows.Scan(
			&j.ID,
			&j.CompanyRoleID,
			&j.MenuID,
			&j.AccessLevel,
			&j.Role,
			&j.Menu,
			&j.ParentMenuID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlRoleMenuRepository) GetByID(ctx context.Context, id *string) *models.RoleMenu {
	var rolemenu models.RoleMenu
	logger := service.Logger(ctx)
	query := `
	select 
		Cast(ID as varchar(36)) as ID,
		CompanyRoleID,
		MenuID,
		AccessLevel
	from 
		RoleMenu
	where 
		ID = @p0`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&rolemenu.ID,
		&rolemenu.CompanyRoleID,
		&rolemenu.MenuID,
		&rolemenu.AccessLevel,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &rolemenu
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", rolemenu)),
	)
	return &rolemenu
}
func (sj *sqlRoleMenuRepository) GetByMenuAndRole(ctx context.Context, menuID string, roleID string) (*models.RoleMenu, error) {
	var rolemenu models.RoleMenu
	logger := service.Logger(ctx)
	query := `
	select TOP 1
		Cast(ID as varchar(36)) as ID,
		Cast(CompanyRoleID as varchar(36)) as CompanyRoleID,
		Cast(MenuID as varchar(36)) as MenuID,
		AccessLevel
	from 
		RoleMenu
	where 
		menuID = @menuID
		AND CompanyRoleID = @roleID`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("menuID", fmt.Sprintf("%v", menuID)),
		zap.String("roleID", fmt.Sprintf("%v", roleID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("menuID", menuID),
		sql.Named("roleID", roleID),
	).Scan(
		&rolemenu.ID,
		&rolemenu.CompanyRoleID,
		&rolemenu.MenuID,
		&rolemenu.AccessLevel,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", rolemenu)),
	)
	return &rolemenu, nil
}
func (sj *sqlRoleMenuRepository) Store(ctx context.Context, a *models.RoleMenu) (*models.RoleMenu, error) {
	a.ID = guid.New().StringUpper()
	rolemenuQuery := `INSERT INTO RoleMenu (
		id, 
		CompanyRoleID,
		MenuID,
		AccessLevel
	) VALUES (@p0, @p1, @p2, @p3)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", rolemenuQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, rolemenuQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.CompanyRoleID),
		sql.Named("p2", a.MenuID),
		sql.Named("p3", a.AccessLevel),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlRoleMenuRepository) Update(ctx context.Context, a *models.RoleMenu) (*models.RoleMenu, error) {

	rolemenuQuery := `
		Update 
			RoleMenu 
		Set 
			CompanyRoleID = @p1, 
			MenuID = @p2,
			AccessLevel = @p3
		Where 
			ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", rolemenuQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, rolemenuQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.CompanyRoleID),
		sql.Named("p2", a.MenuID),
		sql.Named("p3", a.AccessLevel),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlRoleMenuRepository) Delete(ctx context.Context, a *models.RoleMenu) (*models.RoleMenu, error) {

	roleQuery := `
		Delete RoleMenu 
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", roleQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, roleQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
