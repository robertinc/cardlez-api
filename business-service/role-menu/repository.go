package rolemenu

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleMenuRepository interface {
	Fetch(ctx context.Context) (error, []*models.RoleMenu)
	GetByID(ctx context.Context, id *string) *models.RoleMenu
	GetByMenuAndRole(ctx context.Context, menuID string, roleID string) (*models.RoleMenu, error)
	Update(ctx context.Context, RoleMenu *models.RoleMenu) (*models.RoleMenu, error)
	Store(ctx context.Context, a *models.RoleMenu) (*models.RoleMenu, error)
	Delete(ctx context.Context, a *models.RoleMenu) (*models.RoleMenu, error)
}
