package rolemenu

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleMenuUsecase interface {
	GetByID(ctx context.Context, id *string) *models.RoleMenu
	Update(ctx context.Context, ar *models.RoleMenu) (*models.RoleMenu, error)
	Store(context.Context, *models.RoleMenu) (*models.RoleMenu, error)
	Fetch(ctx context.Context) (error, []*models.RoleMenu)
	Delete(ctx context.Context, ar *models.RoleMenu) (*models.RoleMenu, error)
}
