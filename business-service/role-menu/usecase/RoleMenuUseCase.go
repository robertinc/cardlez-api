package usecase

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/robertinc/cardlez-api/service"

	"gitlab.com/robertinc/cardlez-api/business-service/menu"
	menurepository "gitlab.com/robertinc/cardlez-api/business-service/menu/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/role-menu"
	"gitlab.com/robertinc/cardlez-api/business-service/role-menu/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type RoleMenuUsecase struct {
	RoleMenuRepository rolemenu.RoleMenuRepository
	MenuRepository     menu.MenuRepository
	contextTimeout     time.Duration
}

func NewRoleMenuUsecase() rolemenu.RoleMenuUsecase {
	return &RoleMenuUsecase{
		RoleMenuRepository: repository.NewSqlRoleMenuRepository(),
		MenuRepository:     menurepository.NewSqlMenuRepository(),
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func NewRoleMenuUsecaseV2(roleMenuRepo rolemenu.RoleMenuRepository, menuRepo menu.MenuRepository) rolemenu.RoleMenuUsecase {
	return &RoleMenuUsecase{
		RoleMenuRepository: roleMenuRepo,
		MenuRepository:     menuRepo,
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *RoleMenuUsecase) Fetch(c context.Context) (error, []*models.RoleMenu) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listRoleMenu := a.RoleMenuRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listRoleMenu
}

func (a *RoleMenuUsecase) GetByID(c context.Context, id *string) *models.RoleMenu {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listRoleMenu := a.RoleMenuRepository.GetByID(ctx, id)

	return listRoleMenu
}

func (a *RoleMenuUsecase) Store(c context.Context, b *models.RoleMenu) (*models.RoleMenu, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rolemenu, err := a.RoleMenuRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}

	menu, err := a.MenuRepository.GetByID(ctx, b.MenuID)
	if err != nil {
		return nil, err
	}
	_, err = a.RoleMenuRepository.GetByMenuAndRole(ctx, menu.ParentMenuID, b.CompanyRoleID)
	if err != nil {
		if err == sql.ErrNoRows {
			menu, err := a.MenuRepository.GetByID(ctx, rolemenu.MenuID)
			if err != nil {
				return nil, err
			}
			if menu.ParentMenuID != service.NewEmptyGuid() {
				parent := &models.RoleMenu{
					AccessLevel:   2,
					CompanyRoleID: b.CompanyRoleID,
					MenuID:        menu.ParentMenuID,
				}
				_, err := a.RoleMenuRepository.Store(ctx, parent)
				if err != nil {
					return nil, err
				}
			}
		} else {
			return nil, err
		}
	}
	return rolemenu, nil
}

func (a *RoleMenuUsecase) Update(c context.Context, b *models.RoleMenu) (*models.RoleMenu, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rolemenu, err := a.RoleMenuRepository.Update(ctx, b)
	if err != nil {
		return nil, err
	}
	menu, err := a.MenuRepository.GetByID(ctx, b.MenuID)
	if err != nil {
		return nil, err
	}
	_, err = a.RoleMenuRepository.GetByMenuAndRole(ctx, menu.ParentMenuID, b.CompanyRoleID)
	if err != nil {
		if err == sql.ErrNoRows {
			menu, err := a.MenuRepository.GetByID(ctx, rolemenu.MenuID)
			if err != nil {
				return nil, err
			}
			if menu.ParentMenuID != service.NewEmptyGuid() {
				parent := &models.RoleMenu{
					AccessLevel:   2,
					CompanyRoleID: b.CompanyRoleID,
					MenuID:        menu.ParentMenuID,
				}
				_, err := a.RoleMenuRepository.Store(ctx, parent)
				if err != nil {
					return nil, err
				}
			}
		} else {
			return nil, err
		}
	}

	return rolemenu, nil
}

func (a *RoleMenuUsecase) Delete(c context.Context, b *models.RoleMenu) (*models.RoleMenu, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	role, err := a.RoleMenuRepository.Delete(ctx, b)

	if err != nil {
		return nil, err
	}
	return role, nil
}
