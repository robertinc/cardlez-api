package payment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberPaymentRepository interface {
	GetByID(ctx context.Context, id *string) (*models.MemberPayment, error)
	Store(ctx context.Context, a *models.MemberPayment) (*models.MemberPayment, error)
	Update(ctx context.Context, a *models.MemberPayment) (*models.MemberPayment, error)
	StoreByDate(ctx context.Context, a *models.MemberPayment, companyDate string) (*models.MemberPayment, error)
	UpdateByDate(ctx context.Context, a *models.MemberPayment, companyDate string) (*models.MemberPayment, error)
}
type MemberPaymentDetailRepository interface {
	Store(ctx context.Context, a *models.MemberPaymentDetail) (*models.MemberPaymentDetail, error)
	StoreByDate(ctx context.Context, a *models.MemberPaymentDetail, companyDate string) (*models.MemberPaymentDetail, error)
	GetByMemberPaymentID(ctx context.Context, id *string) ([]*models.MemberPaymentDetail, error)
}
