package usecase

import (
	"context"
	"time"

	payment "gitlab.com/robertinc/cardlez-api/business-service/payment"
	"gitlab.com/robertinc/cardlez-api/business-service/payment/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberPaymentDetailUsecase struct {
	MemberPaymentDetailRepository payment.MemberPaymentDetailRepository
	contextTimeout                time.Duration
}

func NewMemberPaymentDetailUsecase() payment.MemberPaymentDetailUsecase {
	return &MemberPaymentDetailUsecase{
		MemberPaymentDetailRepository: repository.NewSqlMemberPaymentDetailRepository(),
		contextTimeout:                time.Second * time.Duration(models.Timeout()),
	}
}
func NewMemberPaymentDetailUsecaseV2(repo payment.MemberPaymentDetailRepository) payment.MemberPaymentDetailUsecase {
	return &MemberPaymentDetailUsecase{
		MemberPaymentDetailRepository: repo,
		contextTimeout:                time.Second * time.Duration(models.Timeout()),
	}
}
func (a *MemberPaymentDetailUsecase) GetByMemberPaymentID(c context.Context, id *string) ([]*models.MemberPaymentDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberPaymentDetail, err := a.MemberPaymentDetailRepository.GetByMemberPaymentID(ctx, id)
	if err != nil {
		return nil, err
	}
	return memberPaymentDetail, nil
}
