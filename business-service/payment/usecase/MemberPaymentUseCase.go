package usecase

import (
	"context"
	"time"

	"github.com/beevik/guid"
	// accountrepository "gitlab.com/robertinc/cardlez-api/business-service/account/repository"
	// journalmappingrepository "gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	// memberrepository "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	payment "gitlab.com/robertinc/cardlez-api/business-service/payment"
	"gitlab.com/robertinc/cardlez-api/business-service/payment/repository"

	// tellertransaction "gitlab.com/robertinc/cardlez-api/business-service/tellertransaction/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"

	cAccount "gitlab.com/robertinc/cardlez-api/business-service/account"
	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	cPayment "gitlab.com/robertinc/cardlez-api/business-service/payment"
	cTellerTransaction "gitlab.com/robertinc/cardlez-api/business-service/tellertransaction"
)

type MemberPaymentUsecase struct {
	MemberPaymentRepository payment.MemberPaymentRepository
	contextTimeout          time.Duration
}

func NewMemberPaymentUsecase() payment.MemberPaymentUsecase {
	return &MemberPaymentUsecase{
		MemberPaymentRepository: repository.NewSqlMemberPaymentRepository(),
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberPaymentUsecaseV2(repo payment.MemberPaymentRepository) payment.MemberPaymentUsecase {
	return &MemberPaymentUsecase{
		MemberPaymentRepository: repo,
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func (a *MemberPaymentUsecase) Store(c context.Context, b *models.MemberPayment) (*models.MemberPayment, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	var memberPayment models.MemberPayment
	//var productId string
	memberPayment.RecordStatus = 1
	t := time.Now()

	memberPaymentDetailRepo := ctx.Value("MemberPaymentDetailRepository").(cPayment.MemberPaymentDetailRepository)
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// accountrepository := accountrepository.NewSqlAccountRepository()
	if b.TransferType == "2" {
		accountAsal, err := accountRepo.GetByCode(ctx, &b.CreditAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.CreditAccountID = accountAsal.ID
		memberPayment.CreditDate = t.Format("2006-01-02 15:04:05")
		if b.DebitAccountID != "" {
			accountTujuan, err := accountRepo.GetByCode(ctx, &b.DebitAccountID)
			if err != nil {
				return nil, err
			}
			memberPayment.DebitAccountID = accountTujuan.ID
			//productId = accountTujuan.ProductID
			memberPayment.POSName = accountTujuan.MemberID
		} else {
			memberPayment.DebitAccountID = "00000000-0000-0000-0000-000000000000"
		}
	} else if b.TransferType == "1" {
		accountAsalCust, err := accountRepo.GetByCode(ctx, &b.DebitAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.DebitAccountID = accountAsalCust.ID
		memberPayment.DebitDate = t.Format("2006-01-02 15:04:05")
		accountTujuan, err := accountRepo.GetByCode(ctx, &b.CreditAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.CreditAccountID = accountTujuan.ID
		memberPayment.CreditDate = t.Format("2006-01-02 15:04:05")
		memberPayment.POSName = accountTujuan.MemberID
		memberPayment.RecordStatus = 2
	}

	// memberrepository := memberrepository.NewSqlMemberRepository()
	memberPayment.Code = b.Code
	memberPayment.Note = b.Note
	memberPayment.TransferType = b.TransferType
	memberPayment.Amount = b.Amount
	memberPayment.CurrencyID = "00000000-0000-0000-0000-000000000000"
	Payment, err := a.MemberPaymentRepository.Store(ctx, &memberPayment)
	if err != nil {
		return nil, err
	}
	for _, detail := range b.MemberPaymentDetail {
		detail.MemberPaymentID = Payment.ID
		//detailRepo := repository.NewSqlMemberPaymentDetailRepository()
		_, err = memberPaymentDetailRepo.Store(ctx, &detail)
		if err != nil {
			return nil, err
		}
	}

	if Payment.RecordStatus == 2 {
		err := a.InsertJournal(ctx, Payment)
		if err != nil {
			return nil, err
		}
	}
	//Insert To Journal
	//err = a.InsertJournal(ctx, Payment, &accountAsal.ProductID, productId)
	return Payment, nil
}
func (a *MemberPaymentUsecase) StoreByDate(c context.Context, b *models.MemberPayment, companyDate string) (*models.MemberPayment, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	var memberPayment models.MemberPayment
	//var productId string
	memberPayment.RecordStatus = 1

	memberPaymentDetailRepo := ctx.Value("MemberPaymentDetailRepository").(cPayment.MemberPaymentDetailRepository)
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// accountrepository := accountrepository.NewSqlAccountRepository()
	if b.TransferType == "2" {
		accountAsal, err := accountRepo.GetByCode(ctx, &b.CreditAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.CreditAccountID = accountAsal.ID
		memberPayment.CreditDate = companyDate
		if b.DebitAccountID != "" {
			accountTujuan, err := accountRepo.GetByCode(ctx, &b.DebitAccountID)
			if err != nil {
				return nil, err
			}
			memberPayment.DebitAccountID = accountTujuan.ID
			//productId = accountTujuan.ProductID
			memberPayment.POSName = accountTujuan.MemberID
		} else {
			memberPayment.DebitAccountID = "00000000-0000-0000-0000-000000000000"
		}
	} else if b.TransferType == "1" {
		accountAsalCust, err := accountRepo.GetByCode(ctx, &b.DebitAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.DebitAccountID = accountAsalCust.ID
		memberPayment.DebitDate = companyDate
		accountTujuan, err := accountRepo.GetByCode(ctx, &b.CreditAccountID)
		if err != nil {
			return nil, err
		}
		memberPayment.CreditAccountID = accountTujuan.ID
		memberPayment.CreditDate = companyDate
		memberPayment.POSName = accountTujuan.MemberID
		memberPayment.RecordStatus = 2
	}

	// memberrepository := memberrepository.NewSqlMemberRepository()
	memberPayment.Code = b.Code
	memberPayment.Note = b.Note
	memberPayment.TransferType = b.TransferType
	memberPayment.Amount = b.Amount
	memberPayment.CurrencyID = "00000000-0000-0000-0000-000000000000"
	Payment, err := a.MemberPaymentRepository.StoreByDate(ctx, &memberPayment, companyDate)
	if err != nil {
		return nil, err
	}
	for _, detail := range b.MemberPaymentDetail {
		detail.MemberPaymentID = Payment.ID
		//detailRepo := repository.NewSqlMemberPaymentDetailRepository()
		_, err = memberPaymentDetailRepo.StoreByDate(ctx, &detail, companyDate)
		if err != nil {
			return nil, err
		}
	}

	if Payment.RecordStatus == 2 {
		err := a.InsertJournalByDate(ctx, Payment)
		if err != nil {
			return nil, err
		}
	}
	//Insert To Journal
	//err = a.InsertJournal(ctx, Payment, &accountAsal.ProductID, productId)
	return Payment, nil
}

func (a *MemberPaymentUsecase) GetByID(c context.Context, b *string) (*models.MemberPayment, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	memberPayment, err := a.MemberPaymentRepository.GetByID(ctx, b)
	if err != nil {
		return nil, err
	}

	return memberPayment, nil
}

func (a *MemberPaymentUsecase) Update(c context.Context, b *models.MemberPayment) (*models.MemberPayment, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberPayment, err := a.MemberPaymentRepository.Update(ctx, b)
	if err != nil {
		return nil, err
	}
	if memberPayment.RecordStatus == 2 {
		err := a.InsertJournal(ctx, memberPayment)
		if err != nil {
			return nil, err
		}
	}
	return memberPayment, nil
}

func (a *MemberPaymentUsecase) UpdateByDate(c context.Context, b *models.MemberPayment, companyDate string) (*models.MemberPayment, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberPayment, err := a.MemberPaymentRepository.UpdateByDate(ctx, b, companyDate)
	if err != nil {
		return nil, err
	}
	if memberPayment.RecordStatus == 2 {
		err := a.InsertJournalByDate(ctx, memberPayment)
		if err != nil {
			return nil, err
		}
	}
	return memberPayment, nil
}

func (a *MemberPaymentUsecase) InsertJournal(ctx context.Context, b *models.MemberPayment) error {
	journalID := guid.New().StringUpper()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	tellerTransactionRepo := ctx.Value("TellerTransactionRepository").(cTellerTransaction.TellerTransactionRepository)
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	accountCredit, err := accountRepo.GetByID(ctx, &b.CreditAccountID)
	if err != nil {
		return err
	}
	accountDebit, err := accountRepo.GetByID(ctx, &b.DebitAccountID)
	if err != nil {
		return err
	}

	if b.TransferType == "2" {
		member, err := memberRepo.GetByID(ctx, &accountCredit.MemberID)
		if err != nil {
			return err
		}
		notes := "Minta Pembayaran untuk " + member.MemberName.String
		// if b.POSName != "" {
		// 	member, err := memberRepo.GetByID(ctx, &b.POSName)
		// 	if err != nil {
		// 		return err
		// 	}
		// 	notes = "Minta Pembayaran untuk " + member.MemberName.String
		// }

		// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0002")
		if err != nil {
			return err
		}
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.DebitDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailID := guid.New().StringUpper()
		// tellertransactionrepository := tellertransaction.NewSqlTellerTransactionRepository()
		COA, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountDebit.ProductID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		COA2, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountCredit.ProductID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}

	} else if b.TransferType == "1" {
		member, err := memberRepo.GetByID(ctx, &accountCredit.MemberID)
		if err != nil {
			return err
		}
		notes := "Kirim Pembayaran ke " + member.MemberName.String

		// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0001")
		if err != nil {
			return err
		}
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.DebitDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailID := guid.New().StringUpper()
		// tellertransactionrepository := tellertransaction.NewSqlTellerTransactionRepository()
		COA, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountDebit.ProductID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		COA2, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountCredit.ProductID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}
	return nil
}
func (a *MemberPaymentUsecase) InsertJournalByDate(ctx context.Context, b *models.MemberPayment) error {
	journalID := guid.New().StringUpper()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	tellerTransactionRepo := ctx.Value("TellerTransactionRepository").(cTellerTransaction.TellerTransactionRepository)
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	accountCredit, err := accountRepo.GetByID(ctx, &b.CreditAccountID)
	if err != nil {
		return err
	}
	accountDebit, err := accountRepo.GetByID(ctx, &b.DebitAccountID)
	if err != nil {
		return err
	}

	if b.TransferType == "2" {
		member, err := memberRepo.GetByID(ctx, &accountCredit.MemberID)
		if err != nil {
			return err
		}
		notes := "Minta Pembayaran untuk " + member.MemberName.String
		// if b.POSName != "" {
		// 	member, err := memberRepo.GetByID(ctx, &b.POSName)
		// 	if err != nil {
		// 		return err
		// 	}
		// 	notes = "Minta Pembayaran untuk " + member.MemberName.String
		// }

		// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0002")
		if err != nil {
			return err
		}
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.DebitDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailID := guid.New().StringUpper()
		// tellertransactionrepository := tellertransaction.NewSqlTellerTransactionRepository()
		COA, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountDebit.ProductID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		COA2, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountCredit.ProductID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}

	} else if b.TransferType == "1" {
		member, err := memberRepo.GetByID(ctx, &accountCredit.MemberID)
		if err != nil {
			return err
		}
		notes := "Kirim Pembayaran ke " + member.MemberName.String

		// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0001")
		if err != nil {
			return err
		}
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.DebitDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailID := guid.New().StringUpper()
		// tellertransactionrepository := tellertransaction.NewSqlTellerTransactionRepository()
		COA, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountDebit.ProductID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		COA2, err := tellerTransactionRepo.COAGetByProductID(ctx, &accountCredit.ProductID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}
	return nil
}
