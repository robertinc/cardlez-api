package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/beevik/guid"
	payment "gitlab.com/robertinc/cardlez-api/business-service/payment"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlMemberPaymentRepository struct {
	Conn *sql.DB
}

func NewSqlMemberPaymentRepository() payment.MemberPaymentRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberPaymentRepository{conn}
}

func NewSqlMemberPaymentRepositoryV2(Conn *sql.DB) payment.MemberPaymentRepository {
	conn := Conn
	return &sqlMemberPaymentRepository{conn}
}
func (sj *sqlMemberPaymentRepository) GetByID(ctx context.Context, id *string) (*models.MemberPayment, error) {
	var result models.MemberPayment
	query := `
				SELECT cast(ID as varchar(50)) as [ID]
				,[Code]
				,cast(DebitAccountID as varchar(50)) as [DebitAccountID]
				,cast(CurrencyID as varchar(50)) as [CurrencyID]
				,[Amount]
				,[DebitDate]
				,[TransferType]
				,cast(CreditAccountID as varchar(50)) as [CreditAccountID]
				,[CreditDate]
				,[Note]
				,isnull([POSTransactionRefNo],'') as [POSTransactionRefNo]
				,isnull([POSName],'') as [POSName]
				,isnull([POSTransactionDate],'') as [POSTransactionDate]
				,[RevNo]
				,[RecordStatus]
				, isnull((select phone +' - '+ membername from member a inner join account b on a.id=b.memberid where b.id=MP.DebitAccountID),'') as DebitAccountName
				, isnull((select phone +' - '+ membername from member a inner join account b on a.id=b.memberid where b.id=MP.CreditAccountID),'') as CreditAccountName
				FROM [MemberPayment] MP
				Where MP.ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query Member Payment By ID",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&result.ID,
		&result.Code,
		&result.DebitAccountID,
		&result.CurrencyID,
		&result.Amount,
		&result.DebitDate,
		&result.TransferType,
		&result.CreditAccountID,
		&result.CreditDate,
		&result.Note,
		&result.POSTransactionRefNo,
		&result.POSName,
		&result.POSTransactionDate,
		&result.RevNo,
		&result.RecordStatus,
		&result.DebitAccountName,
		&result.CreditAccountName,
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	return &result, nil
}
func (sj *sqlMemberPaymentRepository) Store(ctx context.Context, a *models.MemberPayment) (*models.MemberPayment, error) {
	a.ID = guid.New().StringUpper()
	MemberQuery := `
	INSERT INTO MemberPayment (
		id, 
		code, 
		DebitAccountID, 
		CurrencyID, 
		Amount, 
		DebitDate,
		TransferType,
		CreditAccountID,
		CreditDate,
		Note,
		RevNo,
		RecordStatus,
		DateInsert,
		UserInsert
	) VALUES (
		@p0, @p1, @p2, @p3, @p4, @debitdate, @p5, @p6, @creditdate, @p7, @p8, @p9, GETDATE(), @p10
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.CurrencyID),
		sql.Named("p4", a.Amount),
		sql.Named("debitdate", a.DebitDate),
		sql.Named("creditdate", a.CreditDate),
		sql.Named("p5", a.TransferType),
		sql.Named("p6", a.CreditAccountID),
		sql.Named("p7", a.Note),
		sql.Named("p8", 0),
		sql.Named("p9", a.RecordStatus),
		sql.Named("p10", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberPaymentRepository) StoreByDate(ctx context.Context, a *models.MemberPayment, companyDate string) (*models.MemberPayment, error) {
	a.ID = guid.New().StringUpper()
	MemberQuery := `
	INSERT INTO MemberPayment (
		id, 
		code, 
		DebitAccountID, 
		CurrencyID, 
		Amount, 
		DebitDate,
		TransferType,
		CreditAccountID,
		CreditDate,
		Note,
		RevNo,
		RecordStatus,
		DateInsert,
		UserInsert
	) VALUES (
		@p0, @p1, @p2, @p3, @p4, @debitdate, @p5, @p6, @creditdate, @p7, @p8, @p9, @p11, @p10
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.CurrencyID),
		sql.Named("p4", a.Amount),
		sql.Named("debitdate", a.DebitDate),
		sql.Named("creditdate", a.CreditDate),
		sql.Named("p5", a.TransferType),
		sql.Named("p6", a.CreditAccountID),
		sql.Named("p7", a.Note),
		sql.Named("p8", 0),
		sql.Named("p9", a.RecordStatus),
		sql.Named("p10", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p11", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberPaymentRepository) Update(ctx context.Context, a *models.MemberPayment) (*models.MemberPayment, error) {
	var param []string
	MemberPaymentQuery := `Update MemberPayment Set `
	if a.Code != "" {
		param = append(param, "Code = @p1")
	}
	if service.IsValidGuid(a.DebitAccountID) {
		param = append(param, "DebitAccountID = @p2")
	}
	if service.IsValidGuid(a.CurrencyID) {
		param = append(param, "CurrencyID = @p3")
	}
	if a.Amount != -1 {
		param = append(param, "Amount = @p4")
	}
	if a.DebitDate != "" {
		param = append(param, "DebitDate = @p5")
	}
	if a.TransferType != "" {
		param = append(param, "TransferType = @p6")
	}
	if service.IsValidGuid(a.CreditAccountID) {
		param = append(param, "CreditAccountID = @p7")
	}
	if a.CreditDate != "" {
		param = append(param, "CreditDate = @p8")
	}
	if a.Note != "" {
		param = append(param, "Note = @p9")
	}
	if a.POSTransactionRefNo != "" {
		param = append(param, "POSTransactionRefNo = @p10")
	}
	if a.POSName != "" {
		param = append(param, "POSName = @p11")
	}
	if a.POSTransactionDate != "" {
		param = append(param, "POSTransactionDate = @p12")
	}
	if a.RevNo != -1 {
		param = append(param, "RevNo = @p13")
	}
	if a.RecordStatus != -1 {
		param = append(param, "RecordStatus = @p14")
	}
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = GETDATE()")
	MemberPaymentQuery += strings.Join(param, ",")
	MemberPaymentQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberPaymentQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberPaymentQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.CurrencyID),
		sql.Named("p4", a.Amount),
		sql.Named("p5", a.DebitDate),
		sql.Named("p6", a.TransferType),
		sql.Named("p7", a.CreditAccountID),
		sql.Named("p8", a.CreditDate),
		sql.Named("p9", a.Note),
		sql.Named("p10", a.POSTransactionRefNo),
		sql.Named("p11", a.POSName),
		sql.Named("p12", a.POSTransactionDate),
		sql.Named("p13", a.RevNo),
		sql.Named("p14", a.RecordStatus),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberPaymentRepository) UpdateByDate(ctx context.Context, a *models.MemberPayment, companyDate string) (*models.MemberPayment, error) {
	var param []string
	MemberPaymentQuery := `Update MemberPayment Set `
	if a.Code != "" {
		param = append(param, "Code = @p1")
	}
	if service.IsValidGuid(a.DebitAccountID) {
		param = append(param, "DebitAccountID = @p2")
	}
	if service.IsValidGuid(a.CurrencyID) {
		param = append(param, "CurrencyID = @p3")
	}
	if a.Amount != -1 {
		param = append(param, "Amount = @p4")
	}
	if a.DebitDate != "" {
		param = append(param, "DebitDate = @p5")
	}
	if a.TransferType != "" {
		param = append(param, "TransferType = @p6")
	}
	if service.IsValidGuid(a.CreditAccountID) {
		param = append(param, "CreditAccountID = @p7")
	}
	if a.CreditDate != "" {
		param = append(param, "CreditDate = @p8")
	}
	if a.Note != "" {
		param = append(param, "Note = @p9")
	}
	if a.POSTransactionRefNo != "" {
		param = append(param, "POSTransactionRefNo = @p10")
	}
	if a.POSName != "" {
		param = append(param, "POSName = @p11")
	}
	if a.POSTransactionDate != "" {
		param = append(param, "POSTransactionDate = @p12")
	}
	if a.RevNo != -1 {
		param = append(param, "RevNo = @p13")
	}
	if a.RecordStatus != -1 {
		param = append(param, "RecordStatus = @p14")
	}
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = @date")
	MemberPaymentQuery += strings.Join(param, ",")
	MemberPaymentQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberPaymentQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberPaymentQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.CurrencyID),
		sql.Named("p4", a.Amount),
		sql.Named("p5", a.DebitDate),
		sql.Named("p6", a.TransferType),
		sql.Named("p7", a.CreditAccountID),
		sql.Named("p8", a.CreditDate),
		sql.Named("p9", a.Note),
		sql.Named("p10", a.POSTransactionRefNo),
		sql.Named("p11", a.POSName),
		sql.Named("p12", a.POSTransactionDate),
		sql.Named("p13", a.RevNo),
		sql.Named("p14", a.RecordStatus),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
