package repository

import (
	"context"
	"database/sql"
	"log"

	payment "gitlab.com/robertinc/cardlez-api/business-service/payment"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlMemberPaymentDetailRepository struct {
	Conn *sql.DB
}

func NewSqlMemberPaymentDetailRepository() payment.MemberPaymentDetailRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberPaymentDetailRepository{conn}
}
func NewSqlMemberPaymentDetailRepositoryV2(Conn *sql.DB) payment.MemberPaymentDetailRepository {
	conn := Conn
	return &sqlMemberPaymentDetailRepository{conn}
}
func (sj *sqlMemberPaymentDetailRepository) Store(ctx context.Context, a *models.MemberPaymentDetail) (*models.MemberPaymentDetail, error) {
	//a.ID = *guid.New().StringUpper()
	MemberQuery := `INSERT INTO MemberPaymentDetail (
		id, 
		MemberPaymentID, 
		Amount, 
		Note, 
		DateInsert
		) 
	VALUES (NEWID(), @p1, @p2, @p3, GETDATE())`
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberPaymentID),
		sql.Named("p2", a.Amount),
		sql.Named("p3", a.Note),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}
func (sj *sqlMemberPaymentDetailRepository) StoreByDate(ctx context.Context, a *models.MemberPaymentDetail, companyDate string) (*models.MemberPaymentDetail, error) {
	//a.ID = *guid.New().StringUpper()
	MemberQuery := `INSERT INTO MemberPaymentDetail (
		id, 
		MemberPaymentID, 
		Amount, 
		Note, 
		DateInsert
		) 
	VALUES (NEWID(), @p1, @p2, @p3, @p4)`
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberPaymentID),
		sql.Named("p2", a.Amount),
		sql.Named("p3", a.Note),
		sql.Named("p4", companyDate),
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}
func (sj *sqlMemberPaymentDetailRepository) GetByMemberPaymentID(ctx context.Context, id *string) ([]*models.MemberPaymentDetail, error) {
	//var j models.MemberPaymentDetail
	rows, err := sj.Conn.Query(`
	SELECT   
	cast(ID as varchar(36)) as ID,cast(MemberPaymentID as varchar(36)) as MemberPaymentID, Amount, Note
	from 
	MemberPaymentDetail where 
	MemberPaymentID = @p0`, sql.Named("p0", id))
	result := make([]*models.MemberPaymentDetail, 0)
	for rows.Next() {
		MemberPaymentDetail := new(models.MemberPaymentDetail)
		err = rows.Scan(
			&MemberPaymentDetail.ID,
			&MemberPaymentDetail.MemberPaymentID,
			&MemberPaymentDetail.Amount,
			&MemberPaymentDetail.Note,
		)
		if err != nil {
			return nil, err
		}
		result = append(result, MemberPaymentDetail)
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}
