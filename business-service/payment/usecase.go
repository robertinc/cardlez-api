package payment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberPaymentUsecase interface {
	GetByID(ctx context.Context, id *string) (*models.MemberPayment, error)
	Store(ctx context.Context, b *models.MemberPayment) (*models.MemberPayment, error)
	Update(ctx context.Context, b *models.MemberPayment) (*models.MemberPayment, error)
	StoreByDate(ctx context.Context, b *models.MemberPayment, companyDate string) (*models.MemberPayment, error)
	UpdateByDate(ctx context.Context, b *models.MemberPayment, companyDate string) (*models.MemberPayment, error)
}
type MemberPaymentDetailUsecase interface {
	//Store(ctx context.Context, b *models.MemberPaymentDetail) (*models.MemberPaymentDetail, error)
	GetByMemberPaymentID(ctx context.Context, id *string) ([]*models.MemberPaymentDetail, error)
}
