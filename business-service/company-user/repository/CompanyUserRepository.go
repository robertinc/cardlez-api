package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"

	companyuser "gitlab.com/robertinc/cardlez-api/business-service/company-user"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlCompanyUserRepository struct {
	Conn *sql.DB
}

func NewSqlCompanyUserRepository() companyuser.CompanyUserRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlCompanyUserRepository{conn}
}
func NewSqlCompanyUserRepositoryV2(Conn *sql.DB) companyuser.CompanyUserRepository {
	conn := Conn
	return &sqlCompanyUserRepository{conn}
}

func (sj *sqlCompanyUserRepository) Fetch(ctx context.Context) (error, []*models.CompanyUser) {

	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		UserName,
		Cast(CompanyID as varchar(36)) as CompanyID,
		Cast(CompanyUserRoleID as varchar(36)) as CompanyUserRoleID,
		UserEmail,
		ISNULL(IsLocked, '0') as IsLocked
	FROM CompanyUser
	WHERE RecordStatus <> 0
	AND ISNULL(IsSystem, '0') <> '1'
	order by DateInsert desc`
	result := make([]*models.CompanyUser, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.CompanyUser)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.UserName,
			&j.CompanyID,
			&j.CompanyUserRoleID,
			&j.UserEmail,
			&j.IsLocked,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlCompanyUserRepository) GetByID(ctx context.Context, id *string) *models.CompanyUser {
	var companyuser models.CompanyUser
	logger := service.Logger(ctx)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		UserName,
		Cast(CompanyID as varchar(36)) as CompanyID,
		Cast(CompanyUserRoleID as varchar(36)) as CompanyUserRoleID,
		UserEmail,
		ISNULL(IsLocked, '0') as IsLocked
	FROM CompanyUser
	where 
		ID = @p0
		AND ISNULL(IsSystem, '0') <> '1'`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	row := sj.Conn.QueryRow(query,
		sql.Named("p0", id))
	err := row.Scan(
		&companyuser.ID,
		&companyuser.Code,
		&companyuser.UserName,
		&companyuser.CompanyID,
		&companyuser.CompanyUserRoleID,
		&companyuser.UserEmail,
		&companyuser.IsLocked,
	)

	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &companyuser
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", companyuser)),
	)
	return &companyuser
}

func (sj *sqlCompanyUserRepository) Store(ctx context.Context, a *models.CompanyUser) (*models.CompanyUser, error) {
	a.ID = guid.New().StringUpper()
	companyuserQuery := `
		INSERT INTO CompanyUser 
		(
			id, 
			code, 
			UserName,
			CompanyID,
			CompanyUserRoleID,
			UserEmail,
			UserPass,
			RecordStatus,
			IsSystem,
			UserInsert,
			DateInsert,
			IsLocked
		) VALUES (
			@id,
			@code,
			@username,
			@companyID,
			@companyUserRoleID,
			@useremail,
			@userpass,
			1,
			'0',
			@userInsert,
			GETDATE(),
			@islocked
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyuserQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyuserQuery,
		sql.Named("id", a.ID),
		sql.Named("code", a.Code),
		sql.Named("username", a.UserName),
		sql.Named("companyID", a.CompanyID),
		sql.Named("companyUserRoleID", a.CompanyUserRoleID),
		sql.Named("useremail", a.UserEmail),
		sql.Named("userpass", a.UserPass),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("islocked", a.IsLocked),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlCompanyUserRepository) StoreByDate(ctx context.Context, a *models.CompanyUser, companyDate string) (*models.CompanyUser, error) {
	a.ID = guid.New().StringUpper()
	companyuserQuery := `
		INSERT INTO CompanyUser 
		(
			id, 
			code, 
			UserName,
			CompanyID,
			CompanyUserRoleID,
			UserEmail,
			UserPass,
			RecordStatus,
			IsSystem,
			UserInsert,
			DateInsert,
			IsLocked
		) VALUES (
			@id,
			@code,
			@username,
			@companyID,
			@companyUserRoleID,
			@useremail,
			@userpass,
			1,
			'0',
			@userInsert,
			@date,
			@islocked
		)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyuserQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyuserQuery,
		sql.Named("id", a.ID),
		sql.Named("code", a.Code),
		sql.Named("username", a.UserName),
		sql.Named("companyID", a.CompanyID),
		sql.Named("companyUserRoleID", a.CompanyUserRoleID),
		sql.Named("useremail", a.UserEmail),
		sql.Named("userpass", a.UserPass),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("islocked", a.IsLocked),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyUserRepository) Update(ctx context.Context, a *models.CompanyUser) (*models.CompanyUser, error) {
	updatePassQuery := ``
	if a.UserPass != "" {
		updatePassQuery = `,UserPass = @userpass`
	}
	companyuserQuery := `
		Update CompanyUser 
		Set 
			Username = @username,
			CompanyID = @companyID,
			CompanyUserRoleID = @companyuserroleID,
			UserEmail = @useremail,
			IsLocked = @islocked,
			UserUpdate = @userUpdate,
			DateUpdate = GETDATE()
			` + updatePassQuery + `
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyuserQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyuserQuery,
		sql.Named("p0", a.ID),
		sql.Named("username", a.UserName),
		sql.Named("companyID", a.CompanyID),
		sql.Named("companyuserroleID", a.CompanyUserRoleID),
		sql.Named("useremail", a.UserEmail),
		sql.Named("userpass", a.UserPass),
		sql.Named("islocked", a.IsLocked),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlCompanyUserRepository) UpdateByDate(ctx context.Context, a *models.CompanyUser, companyDate string) (*models.CompanyUser, error) {
	updatePassQuery := ``
	if a.UserPass != "" {
		updatePassQuery = `,UserPass = @userpass`
	}
	companyuserQuery := `
		Update CompanyUser 
		Set 
			Username = @username,
			CompanyID = @companyID,
			CompanyUserRoleID = @companyuserroleID,
			UserEmail = @useremail,
			IsLocked = @islocked,
			UserUpdate = @userUpdate,
			DateUpdate = @date
			` + updatePassQuery + `
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyuserQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyuserQuery,
		sql.Named("p0", a.ID),
		sql.Named("username", a.UserName),
		sql.Named("companyID", a.CompanyID),
		sql.Named("companyuserroleID", a.CompanyUserRoleID),
		sql.Named("useremail", a.UserEmail),
		sql.Named("userpass", a.UserPass),
		sql.Named("islocked", a.IsLocked),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCompanyUserRepository) Delete(ctx context.Context, a *models.CompanyUser) (*models.CompanyUser, error) {

	companyuserQuery := `
		Update CompanyUser 
		Set 
			RecordStatus = 0
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", companyuserQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, companyuserQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
func (sj *sqlCompanyUserRepository) GetByName(ctx context.Context, name *string) (*models.CompanyUser, error) {

	var companyuser models.CompanyUser
	query := `
		SELECT 
			Cast(ID as varchar(36)) as ID,
			Code,
			UserName,
			Cast(CompanyID as varchar(36)) as CompanyID,
			Cast(CompanyUserRoleID as varchar(36)) as CompanyUserRoleID,
			UserEmail,
			ISNULL(IsLocked, '0') as IsLocked
		FROM CompanyUser
		where 
			UserName = @p0
			AND ISNULL(IsSystem, '0') <> '1'`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("companyuserName", fmt.Sprintf("%v", companyuser.UserName)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", name)).Scan(
		&companyuser.ID,
		&companyuser.Code,
		&companyuser.UserName,
		&companyuser.CompanyID,
		&companyuser.CompanyUserRoleID,
		&companyuser.UserEmail,
		&companyuser.IsLocked)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", companyuser)),
	)
	return &companyuser, nil
}
func (sj *sqlCompanyUserRepository) GetByEmail(ctx context.Context, email *string) (*models.CompanyUser, error) {

	var companyuser models.CompanyUser
	query := `
		SELECT 
			Cast(ID as varchar(36)) as ID,
			Code,
			UserName,
			Cast(CompanyID as varchar(36)) as CompanyID,
			Cast(CompanyUserRoleID as varchar(36)) as CompanyUserRoleID,
			UserEmail,
			ISNULL(IsLocked, '0') as IsLocked
		FROM CompanyUser
		where 
		UserEmail = @p0
			AND ISNULL(IsSystem, '0') <> '1'`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("companyuserName", fmt.Sprintf("%v", companyuser.UserEmail)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", email)).Scan(
		&companyuser.ID,
		&companyuser.Code,
		&companyuser.UserName,
		&companyuser.CompanyID,
		&companyuser.CompanyUserRoleID,
		&companyuser.UserEmail,
		&companyuser.IsLocked)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", companyuser)),
	)
	return &companyuser, nil
}
