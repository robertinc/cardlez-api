package companyuser

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CompanyUserUsecase interface {
	GetByID(ctx context.Context, id *string) *models.CompanyUser
	Update(ctx context.Context, ar *models.CompanyUser) (*models.CompanyUser, error)
	UpdateByDate(ctx context.Context, ar *models.CompanyUser, companyDate string) (*models.CompanyUser, error)
	Delete(ctx context.Context, ar *models.CompanyUser) (*models.CompanyUser, error)
	Store(context.Context, *models.CompanyUser) (*models.CompanyUser, error)
	StoreByDate(ctx context.Context, a *models.CompanyUser, companyDate string) (*models.CompanyUser, error)
	Fetch(ctx context.Context) (error, []*models.CompanyUser)
	GetByName(ctx context.Context, name *string) (*models.CompanyUser, error)
	ResetPassword(ctx context.Context, id string) (*models.CompanyUser, error)
	ResetPasswordByDate(ctx context.Context, id string, companyDate string) (*models.CompanyUser, error)
	GetByEmail(ctx context.Context, email *string) (*models.CompanyUser, error)
}
