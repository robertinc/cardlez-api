package companyuser

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CompanyUserRepository interface {
	Fetch(ctx context.Context) (error, []*models.CompanyUser)
	GetByID(ctx context.Context, id *string) *models.CompanyUser
	Update(ctx context.Context, companyUser *models.CompanyUser) (*models.CompanyUser, error)
	UpdateByDate(ctx context.Context, companyUser *models.CompanyUser, companyDate string) (*models.CompanyUser, error)
	Delete(ctx context.Context, companyUser *models.CompanyUser) (*models.CompanyUser, error)
	Store(ctx context.Context, a *models.CompanyUser) (*models.CompanyUser, error)
	StoreByDate(ctx context.Context, a *models.CompanyUser, companyDate string) (*models.CompanyUser, error)
	GetByName(ctx context.Context, name *string) (*models.CompanyUser, error)
	GetByEmail(ctx context.Context, email *string) (*models.CompanyUser, error)
}
