package usecase

import (
	"context"
	"math/rand"
	"time"

	companyuser "gitlab.com/robertinc/cardlez-api/business-service/company-user"
	"gitlab.com/robertinc/cardlez-api/business-service/company-user/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"golang.org/x/crypto/bcrypt"
)

type CompanyUserUsecase struct {
	CompanyUserRepository companyuser.CompanyUserRepository
	contextTimeout        time.Duration
}

func NewCompanyUserUsecase() companyuser.CompanyUserUsecase {
	return &CompanyUserUsecase{
		CompanyUserRepository: repository.NewSqlCompanyUserRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewCompanyUserUsecaseV2(companyUserRepo companyuser.CompanyUserRepository) companyuser.CompanyUserUsecase {
	return &CompanyUserUsecase{
		CompanyUserRepository: companyUserRepo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func (a *CompanyUserUsecase) Fetch(c context.Context) (error, []*models.CompanyUser) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listCompanyUser := a.CompanyUserRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listCompanyUser
}

func (a *CompanyUserUsecase) GetByID(c context.Context, id *string) *models.CompanyUser {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompanyUser := a.CompanyUserRepository.GetByID(ctx, id)

	return listCompanyUser
}

func (a *CompanyUserUsecase) GetByName(c context.Context, name *string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCompanyUser, err := a.CompanyUserRepository.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return listCompanyUser, nil
}

func (a *CompanyUserUsecase) Store(c context.Context, b *models.CompanyUser) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rand.Seed(time.Now().UnixNano())
	tempPassword := RandStringRunes(8)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(tempPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	b.UserPass = string(hashedPassword)
	companyuser, err := a.CompanyUserRepository.Store(ctx, b)

	emailService := service.NewEmailService()
	emailMessage := `Berikut adalah password baru anda : ` + tempPassword + `. Harap segera ganti password anda setelah berhasil login ke Admin Panel.`
	emailSubject := `Login ISP-Mobile Admin Panel`
	isSendSuccess, err := emailService.SendVerificationCode(emailMessage, companyuser.UserEmail, emailSubject)
	if err != nil || isSendSuccess == false {
		return nil, err
	}
	return companyuser, nil
}

func (a *CompanyUserUsecase) StoreByDate(c context.Context, b *models.CompanyUser, companyDate string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rand.Seed(time.Now().UnixNano())
	tempPassword := RandStringRunes(8)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(tempPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	b.UserPass = string(hashedPassword)
	companyuser, err := a.CompanyUserRepository.StoreByDate(ctx, b, companyDate)

	emailService := service.NewEmailService()
	emailMessage := `Berikut adalah password baru anda : ` + tempPassword + `. Harap segera ganti password anda setelah berhasil login ke Admin Panel.`
	emailSubject := `Login Cardlez Admin Panel`
	isSendSuccess, err := emailService.SendVerificationCode(emailMessage, companyuser.UserEmail, emailSubject)
	if err != nil || isSendSuccess == false {
		return nil, err
	}
	return companyuser, nil
}

func (a *CompanyUserUsecase) Update(c context.Context, b *models.CompanyUser) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if b.UserPass != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(b.UserPass), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		b.UserPass = string(hashedPassword)
	}
	companyuser, err := a.CompanyUserRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return companyuser, nil
}
func (a *CompanyUserUsecase) UpdateByDate(c context.Context, b *models.CompanyUser, companyDate string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if b.UserPass != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(b.UserPass), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		b.UserPass = string(hashedPassword)
	}
	companyuser, err := a.CompanyUserRepository.UpdateByDate(ctx, b, companyDate)

	if err != nil {
		return nil, err
	}
	return companyuser, nil
}

func (a *CompanyUserUsecase) Delete(c context.Context, b *models.CompanyUser) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	companyuser, err := a.CompanyUserRepository.Delete(ctx, b)

	if err != nil {
		return nil, err
	}
	return companyuser, nil
}

func (a *CompanyUserUsecase) ResetPassword(c context.Context, id string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rand.Seed(time.Now().UnixNano())
	tempPassword := RandStringRunes(8)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(tempPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	companyuser := a.CompanyUserRepository.GetByID(ctx, &id)
	companyuser.UserPass = string(hashedPassword)

	companyuser, err = a.CompanyUserRepository.Update(ctx, companyuser)

	emailService := service.NewEmailService()
	emailMessage := `Reset password berhasil. Berikut adalah password baru anda : ` + tempPassword + `. Harap segera ganti password anda setelah berhasil login ke Admin Panel.`
	emailSubject := `Reset Password Berhasil`
	isSendSuccess, err := emailService.SendVerificationCode(emailMessage, companyuser.UserEmail, emailSubject)
	if err != nil || isSendSuccess == false {
		return nil, err
	}
	return companyuser, nil
}
func (a *CompanyUserUsecase) ResetPasswordByDate(c context.Context, id string, companyDate string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	rand.Seed(time.Now().UnixNano())
	tempPassword := RandStringRunes(8)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(tempPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	companyuser := a.CompanyUserRepository.GetByID(ctx, &id)
	companyuser.UserPass = string(hashedPassword)

	companyuser, err = a.CompanyUserRepository.UpdateByDate(ctx, companyuser, companyDate)

	emailService := service.NewEmailService()
	emailMessage := `Reset password berhasil. Berikut adalah password baru anda : ` + tempPassword + `. Harap segera ganti password anda setelah berhasil login ke Admin Panel.`
	emailSubject := `Reset Password Berhasil`
	isSendSuccess, err := emailService.SendVerificationCode(emailMessage, companyuser.UserEmail, emailSubject)
	if err != nil || isSendSuccess == false {
		return nil, err
	}
	return companyuser, nil
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
func (a *CompanyUserUsecase) GetByEmail(c context.Context, email *string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	companyUser, err := a.CompanyUserRepository.GetByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	return companyUser, nil
}
