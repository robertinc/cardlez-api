package permata

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PermataUsecase interface {
	FundTransferInquiry(ctx context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	FundTransfer(ctx context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	OverbookingInquiry(ctx context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	Overbooking(ctx context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
}
