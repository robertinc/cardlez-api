package usecase

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/account"
	cAppConfig "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	"gitlab.com/robertinc/cardlez-api/business-service/bank"
	bankuc "gitlab.com/robertinc/cardlez-api/business-service/bank/usecase"
	"gitlab.com/robertinc/cardlez-api/business-service/fundtransfer"
	ftUsecase "gitlab.com/robertinc/cardlez-api/business-service/fundtransfer/usecase"
	"gitlab.com/robertinc/cardlez-api/business-service/permata"
	"gitlab.com/robertinc/cardlez-api/business-service/permata/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type PermataUsecase struct {
	PermataRepository permata.PermataRepository
	contextTimeout    time.Duration
}

func NewPermataUsecase() permata.PermataUsecase {
	return &PermataUsecase{
		PermataRepository: repository.NewSqlPermataRepository(),
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func NewPermataUsecaseV2(repo permata.PermataRepository) permata.PermataUsecase {
	return &PermataUsecase{
		PermataRepository: repo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *PermataUsecase) FundTransferInquiry(c context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//insert ke FundTransferInquiry
	fundTransferInq, err := a.PermataRepository.StoreFundTransferInquiry(ctx, b)
	if err != nil {
		return nil, err
	}

	return fundTransferInq, nil
}

func (a *PermataUsecase) OverbookingInquiry(c context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	needInsert := false

	tempfundTransfer, err := a.PermataRepository.GetOverbookinInquiryById(ctx, &b.ID)
	if err != nil {
		if tempfundTransfer == nil {
			needInsert = true
		}
	}

	if needInsert {
		fundTransferInq, err := a.PermataRepository.StoreOverbookingInquiry(ctx, b)
		if err != nil {
			return nil, err
		}
		return fundTransferInq, nil
	} else {
		errNeedInsert := errors.New("Session ID has been stored before")
		return nil, errNeedInsert
	}

}

func (a *PermataUsecase) Overbooking(c context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	configKey := "RekeningDisbursementPermata"
	//appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(appconfigrepo.ApplicationConfigurationRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	rekeningPerusahaan, error := appConfigRepo.GetByConfigKey(ctx, &configKey)
	if error != nil {
		return nil, error
	}

	configKeyNamaRekening := "NamaRekeningDisbursementPermata"
	namarekeningPerusahaan, err2 := appConfigRepo.GetByConfigKey(ctx, &configKeyNamaRekening)
	if err2 != nil {
		return nil, err2
	}

	repo := ctx.Value("BankRepository").(bank.BankRepository)
	repoFundTransfer := ctx.Value("FundtransferRepository").(fundtransfer.FundTransferRepository)
	repoAccount := ctx.Value("AccountRepository").(account.AccountRepository)
	ftUc := ftUsecase.NewFundTransferUsecaseV2(repoFundTransfer)

	accountObj, errAcc := repoAccount.GetByCode(ctx, &accountNo)
	if errAcc != nil {
		return nil, errAcc
	}

	//Parameter Buat Transaksi ke Middleware
	b.AccountNoOrigin = rekeningPerusahaan.ConfigValue
	b.AccountNameOrigin = namarekeningPerusahaan.ConfigValue
	//--END Parameter Buat Transaksi ke Middleware
	toAccountName := b.AccountNameDest
	amountTransfer := b.Amount
	note := b.TrxDesc
	needInsert := false

	tempfundTransfer, err := a.PermataRepository.GetOverbookinInquiryById(ctx, &b.ID)
	if err != nil {
		if tempfundTransfer == nil {
			needInsert = true
		}
	}

	if needInsert {
		fundTransferInq, err := a.PermataRepository.StoreOverbooking(ctx, b)
		if err != nil {
			return nil, err
		}

		bu := bankuc.NewBankUsecaseV2(repo)
		bankObj := bu.GetByCode(ctx, &fundTransferInq.BankID)
		trxIsSuccess := false
		recStat, errRecStat := strconv.Atoi(fundTransferInq.RecordStatus)
		if errRecStat != nil {
			return nil, errRecStat
		}
		if recStat == 2 {
			trxIsSuccess = true
			fundTransferInq.AccountNameDest = toAccountName
		}
		fundTransferObj := &models.FundTransfer{
			ID:                     guid.New().StringUpper(),
			BankID:                 bankObj.ID,
			BankCode:               fundTransferInq.BankID,
			BankName:               fundTransferInq.BankName,
			AccountNoDestination:   fundTransferInq.AccountNoDest,
			AccountNameDestination: fundTransferInq.AccountNameDest,
			AccountCode:            accountObj.Code,
			AccountNoOrigin:        accountObj.Code,
			TransferType:           3,
			SessionIDInquiry:       fundTransferInq.ID,
			RecordStatus:           int32(recStat),
			IsSuccess:              trxIsSuccess,
			CreditAccountName:      toAccountName,
			Amount:                 amountTransfer,
			Note:                   note,
			ReffNo:                 fundTransferInq.ReffNo,
		}

		if trxIsSuccess {
			_, err_ := ftUc.InsertBase(ctx, fundTransferObj)
			if err_ != nil {
				return nil, err_
			}
		} else {
			return nil, fmt.Errorf(fundTransferInq.StatusDesc)
		}

		return fundTransferInq, nil
	} else {
		errNeedInsert := errors.New("Session ID has been stored before")
		return nil, errNeedInsert
	}
}

func (a *PermataUsecase) FundTransfer(c context.Context, accountNo string, b *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	configKey := "RekeningDisbursementPermata"
	//appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(appconfigrepo.ApplicationConfigurationRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	rekeningPerusahaan, error := appConfigRepo.GetByConfigKey(ctx, &configKey)
	if error != nil {
		return nil, error
	}

	configKeyNamaRekening := "NamaRekeningDisbursementPermata"
	namarekeningPerusahaan, err2 := appConfigRepo.GetByConfigKey(ctx, &configKeyNamaRekening)
	if err2 != nil {
		return nil, err2
	}

	repo := ctx.Value("BankRepository").(bank.BankRepository)
	repoFundTransfer := ctx.Value("FundtransferRepository").(fundtransfer.FundTransferRepository)
	repoAccount := ctx.Value("AccountRepository").(account.AccountRepository)
	ftUc := ftUsecase.NewFundTransferUsecaseV2(repoFundTransfer)

	accountObj, errAcc := repoAccount.GetByCode(ctx, &accountNo)
	if errAcc != nil {
		return nil, errAcc
	}

	//Parameter Buat Transaksi ke Middleware
	b.AccountNoOrigin = rekeningPerusahaan.ConfigValue
	b.AccountNameOrigin = namarekeningPerusahaan.ConfigValue
	//--END Parameter Buat Transaksi ke Middleware
	toAccountName := b.AccountNameDest
	amountTransfer := b.Amount
	note := b.TrxDesc
	fundTransferInq, err := a.PermataRepository.StoreFundTransfer(ctx, b)
	if err != nil {
		return nil, err
	}

	bu := bankuc.NewBankUsecaseV2(repo)
	bankObj := bu.GetByCode(ctx, &fundTransferInq.BankID)
	trxIsSuccess := false
	recStat, errRecStat := strconv.Atoi(fundTransferInq.RecordStatus)
	if errRecStat != nil {
		return nil, errRecStat
	}
	if recStat == 2 {
		trxIsSuccess = true
		fundTransferInq.AccountNameDest = toAccountName
	}

	fundTransferObj := &models.FundTransfer{
		ID:                     guid.New().StringUpper(),
		BankID:                 bankObj.ID,
		BankCode:               fundTransferInq.BankID,
		BankName:               fundTransferInq.BankName,
		AccountNoDestination:   fundTransferInq.AccountNoDest,
		AccountNameDestination: fundTransferInq.AccountNameDest,
		AccountCode:            accountObj.Code,
		AccountNoOrigin:        accountObj.Code,
		TransferType:           2,
		SessionIDInquiry:       fundTransferInq.ID,
		RecordStatus:           int32(recStat),
		IsSuccess:              trxIsSuccess,
		CreditAccountName:      toAccountName,
		Amount:                 amountTransfer,
		Note:                   note,
		//CreditAccountName:      fundTransferInq.AccountNameDest,
	}

	if trxIsSuccess {
		_, err_ := ftUc.InsertBase(ctx, fundTransferObj)
		if err_ != nil {
			return nil, err_
		}
	} else {
		return nil, fmt.Errorf(fundTransferInq.StatusDesc)
	}

	return fundTransferInq, nil
}
