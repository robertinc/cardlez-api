package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	permata "gitlab.com/robertinc/cardlez-api/business-service/permata"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlPermataRepository struct {
	Conn *sql.DB
}

func NewSqlPermataRepository() permata.PermataRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnStringV2(2))
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlPermataRepository{conn}
}

func NewSqlPermataRepositoryV2(Conn *sql.DB) permata.PermataRepository {
	conn := Conn
	return &sqlPermataRepository{conn}
}
func (sj *sqlPermataRepository) StoreOVOInquiry(ctx context.Context, a *models.PermataOVO) (*models.PermataOVO, error) {
	logger := service.Logger(ctx)
	permataOVOInqQuery := `
	INSERT INTO OVOInquiry(
		ID
		,BillName
		,NumberOfRecord
		,BillReff1
		,BillCurrency1
		,BillAmountSign1
		,BillReff2
		,BillCurrency2
		,BillAmountSign2
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@p0
      ,@p1
      ,@p2
      ,@p3
      ,@p4
      ,@p5
      ,@p6
      ,@p7
      ,@p8
      ,@p9
      ,@p10
      ,@p11
      ,GETDATE()
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataOVOInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataOVOInqQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.BillName),
		sql.Named("p2", a.NumberOfRecord),
		sql.Named("p3", a.BillReff1),
		sql.Named("p4", a.BillCurrency1),
		sql.Named("p5", a.BillAmountSign1),
		sql.Named("p6", a.BillReff2),
		sql.Named("p7", a.BillCurrency2),
		sql.Named("p8", a.BillAmountSign2),
		sql.Named("p9", a.RecordStatus),
		sql.Named("p10", a.StatusDesc),
		sql.Named("p11", ctx.Value(service.CtxUserID).(string)),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) StoreOVO(ctx context.Context, a *models.PermataOVO) (*models.PermataOVO, error) {
	logger := service.Logger(ctx)
	permataOVOInqQuery := `
	INSERT INTO OVO(
		ID
		,BillName
		,BillNumber
		,TrxAmount
		,Currency
		,UserId
		,DebAccNumber
		,DebAccName
		,DebAccCur
		,BillRefNo
		,InstCode
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@p0
      ,@p1
      ,@p2
      ,@p3
      ,@p4
      ,@p5
      ,@p6
      ,@p7
      ,@p8
      ,@p9
      ,@p10
      ,@p11
      ,@p12
      ,@p13
      ,GETDATE()
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataOVOInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataOVOInqQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.BillName),
		sql.Named("p2", a.BillNumber),
		sql.Named("p3", a.TrxAmount),
		sql.Named("p4", a.Currency),
		sql.Named("p5", a.UserId),
		sql.Named("p6", a.DebAccNumber),
		sql.Named("p7", a.DebAccName),
		sql.Named("p8", a.DebAccCur),
		sql.Named("p9", a.BillRefNo),
		sql.Named("p10", a.InstCode),
		sql.Named("p11", a.RecordStatus),
		sql.Named("p12", a.StatusDesc),
		sql.Named("p13", ctx.Value(service.CtxUserID).(string)),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) StoreFundTransferInquiry(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO FundTransferInquiry(
		ID
		,AccountNoDest
		,AccountNameDest
		,BankID
		,BankName
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@ID
      ,@RequestAccountNoDest
      ,@RequestAccountNameDest
      ,@ResponseBankID
      ,@ResponseBankName
      ,@ResponseRecordStatus
      ,@ResponseStatusDesc
      ,@User_Insert
      ,GETDATE()
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("ResponseBankID", a.BankID),
		sql.Named("ResponseBankName", a.BankName),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPermataRepository) StoreFundTransferInquiryByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO FundTransferInquiry(
		ID
		,AccountNoDest
		,AccountNameDest
		,BankID
		,BankName
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@ID
      ,@RequestAccountNoDest
      ,@RequestAccountNameDest
      ,@ResponseBankID
      ,@ResponseBankName
      ,@ResponseRecordStatus
      ,@ResponseStatusDesc
      ,@User_Insert
      ,@date
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("ResponseBankID", a.BankID),
		sql.Named("ResponseBankName", a.BankName),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("date", companyDate),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) StoreFundTransfer(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO FundTransfer(
		ID
		,AccountNoSource
		,AccountNameSource
		,AccountNoDest
		,AccountNameDest
		,AccountPhoneNoDest
		,Amount
		,Charge
		,Description
		,Description2
		,Email
		,BankID
		,BankName
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
		,SessionIDInquiry
		,ReffNo
	)VALUES(
		@ID
		,@RequestAccountNoSource
		,@RequestAccountNameSource
		,@RequestAccountNoDest
		,@RequestAccountNameDest
		,@RequestAccountPhoneNoDest
		,@RequestAmount
		,@RequestCharge
		,@RequestDesc
		,@RequestDesc2
		,@RequestEmail
		,@ResponseBankID
		,@ResponseBankName
		,@ResponseRecordStatus
		,@ResponseStatusDesc
		,@User_Insert
		,GETDATE()
		,@SessionIDInquiry
		,@ReffNo
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoSource", a.AccountNoOrigin),
		sql.Named("RequestAccountNameSource", a.AccountNameOrigin),
		sql.Named("RequestAccountPhoneNoDest", a.AccountDestPhoneNo),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("RequestAmount", a.Amount),
		sql.Named("RequestCharge", a.Charge),
		sql.Named("RequestDesc", a.TrxDesc),
		sql.Named("RequestDesc2", a.TrxDesc2),
		sql.Named("RequestEmail", a.Email),
		sql.Named("ResponseBankID", a.BankID),
		sql.Named("ResponseBankName", a.BankName),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("SessionIDInquiry", a.ID),
		sql.Named("ReffNo", a.ReffNo),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPermataRepository) StoreFundTransferByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO FundTransfer(
		ID
		,AccountNoSource
		,AccountNameSource
		,AccountNoDest
		,AccountNameDest
		,AccountPhoneNoDest
		,Amount
		,Charge
		,Description
		,Description2
		,Email
		,BankID
		,BankName
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
		,SessionIDInquiry
		,ReffNo
	)VALUES(
		@ID
		,@RequestAccountNoSource
		,@RequestAccountNameSource
		,@RequestAccountNoDest
		,@RequestAccountNameDest
		,@RequestAccountPhoneNoDest
		,@RequestAmount
		,@RequestCharge
		,@RequestDesc
		,@RequestDesc2
		,@RequestEmail
		,@ResponseBankID
		,@ResponseBankName
		,@ResponseRecordStatus
		,@ResponseStatusDesc
		,@User_Insert
		,@date
		,@SessionIDInquiry
		,@ReffNo
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoSource", a.AccountNoOrigin),
		sql.Named("RequestAccountNameSource", a.AccountNameOrigin),
		sql.Named("RequestAccountPhoneNoDest", a.AccountDestPhoneNo),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("RequestAmount", a.Amount),
		sql.Named("RequestCharge", a.Charge),
		sql.Named("RequestDesc", a.TrxDesc),
		sql.Named("RequestDesc2", a.TrxDesc2),
		sql.Named("RequestEmail", a.Email),
		sql.Named("ResponseBankID", a.BankID),
		sql.Named("ResponseBankName", a.BankName),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("SessionIDInquiry", a.ID),
		sql.Named("ReffNo", a.ReffNo),
		sql.Named("Date", companyDate),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) GetOverbookinInquiryById(ctx context.Context, id *string) (*models.PermataFundTransfer, error) {
	var result models.PermataFundTransfer
	query := `
				SELECT cast(ID as varchar(50)) as [ID]
				,[AccountNoDest]
				,[AccountNameDest]
				,[RecordStatus]
				,[StatusDesc]
				FROM OverBookingInquiry OBI
				Where OBI.ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query Overbooking Inquiry By ID",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&result.ID,
		&result.AccountNoDest,
		&result.AccountNameDest,
		&result.RecordStatus,
		&result.StatusDesc,
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	return &result, nil
}

func (sj *sqlPermataRepository) GetOverbookingById(ctx context.Context, id *string) (*models.PermataFundTransfer, error) {
	var result models.PermataFundTransfer
	query := `
				SELECT cast(ID as varchar(50)) as [ID]
				,[AccountNoDest]
				,[AccountNameDest]
				,[RecordStatus]
				,[StatusDesc]
				FROM Overbooking OBI
				Where OBI.ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query Overbooking Inquiry By ID",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&result.ID,
		&result.AccountNoDest,
		&result.AccountNameDest,
		&result.RecordStatus,
		&result.StatusDesc,
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	return &result, nil
}

func (sj *sqlPermataRepository) StoreOverbookingInquiry(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO OverbookingInquiry(
		ID
		,AccountNoDest
		,AccountNameDest
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@ID
      ,@RequestAccountNoDest
      ,@RequestAccountNameDest
      ,@ResponseRecordStatus
      ,@ResponseStatusDesc
      ,@User_Insert
      ,GETDATE()
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPermataRepository) StoreOverbookingInquiryByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}
	
	permataFundTransferInqQuery := `
	INSERT INTO OverbookingInquiry(
		ID
		,AccountNoDest
		,AccountNameDest
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		@ID
      ,@RequestAccountNoDest
      ,@RequestAccountNameDest
      ,@ResponseRecordStatus
      ,@ResponseStatusDesc
      ,@User_Insert
      ,@date
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("date", companyDate),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) StoreOverbooking(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)

	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}

	permataFundTransferInqQuery := `
	INSERT INTO Overbooking(
		ID
		,AccountNoSource
		,AccountNameSource
		,AccountNoDest
		,AccountNameDest
		,AccountPhoneNoDest
		,Amount
		,Charge
		,Description
		,Description2
		,Email
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
		,ReffNo
	)VALUES(
		@ID
		,@RequestAccountNoSource
		,@RequestAccountNameSource
		,@RequestAccountNoDest
		,@RequestAccountNameDest
		,@RequestAccountPhoneNoDest
		,@RequestAmount
		,@RequestCharge
		,@RequestDesc
		,@RequestDesc2
		,@RequestEmail
		,@ResponseRecordStatus
		,@ResponseStatusDesc
		,@User_Insert
		,GETDATE()
		,@ReffNo
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoSource", a.AccountNoOrigin),
		sql.Named("RequestAccountNameSource", a.AccountNameOrigin),
		sql.Named("RequestAccountPhoneNoDest", a.AccountDestPhoneNo),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("RequestAmount", a.Amount),
		sql.Named("RequestCharge", a.Charge),
		sql.Named("RequestDesc", a.TrxDesc),
		sql.Named("RequestDesc2", a.TrxDesc2),
		sql.Named("RequestEmail", a.Email),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("ReffNo", a.ReffNo),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPermataRepository) StoreOverbookingByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error) {
	logger := service.Logger(ctx)
	
	userInsert:=service.NewEmptyGuid()
	if ctx.Value(service.CtxUserID) != nil {
		userInsert = ctx.Value(service.CtxUserID).(string)
	}

	permataFundTransferInqQuery := `
	INSERT INTO Overbooking(
		ID
		,AccountNoSource
		,AccountNameSource
		,AccountNoDest
		,AccountNameDest
		,AccountPhoneNoDest
		,Amount
		,Charge
		,Description
		,Description2
		,Email
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
		,ReffNo
	)VALUES(
		@ID
		,@RequestAccountNoSource
		,@RequestAccountNameSource
		,@RequestAccountNoDest
		,@RequestAccountNameDest
		,@RequestAccountPhoneNoDest
		,@RequestAmount
		,@RequestCharge
		,@RequestDesc
		,@RequestDesc2
		,@RequestEmail
		,@ResponseRecordStatus
		,@ResponseStatusDesc
		,@User_Insert
		,@date
		,@ReffNo
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", permataFundTransferInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, permataFundTransferInqQuery,
		sql.Named("ID", a.ID),
		sql.Named("RequestAccountNoSource", a.AccountNoOrigin),
		sql.Named("RequestAccountNameSource", a.AccountNameOrigin),
		sql.Named("RequestAccountPhoneNoDest", a.AccountDestPhoneNo),
		sql.Named("RequestAccountNoDest", a.AccountNoDest),
		sql.Named("RequestAccountNameDest", a.AccountNameDest),
		sql.Named("RequestAmount", a.Amount),
		sql.Named("RequestCharge", a.Charge),
		sql.Named("RequestDesc", a.TrxDesc),
		sql.Named("RequestDesc2", a.TrxDesc2),
		sql.Named("RequestEmail", a.Email),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", userInsert),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("ReffNo", a.ReffNo),
		sql.Named("date", companyDate),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlPermataRepository) StoreTransactionLog(ctx context.Context, a *models.TransactionLog) (*models.TransactionLog, error) {
	logger := service.Logger(ctx)

	transactionLogInqQuery := `
	INSERT INTO TransactionLog(
		ID
		,UrlSender
		,ModuleName
		,UrlReceiver
		,Request
		,Response
		,RecordStatus
		,StatusDesc
		,UserInsert
		,DateInsert
	)VALUES(
		NEWID()
		,@RequestUrlSender
		,@RequestModuleName
		,@RequestUrlReceiver
		,@Request
		,@Response
		,@ResponseRecordStatus
		,@ResponseStatusDesc
		,@User_Insert
		,GETDATE()
	)`
	logger = service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", transactionLogInqQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err_ := sj.Conn.ExecContext(ctx, transactionLogInqQuery,
		sql.Named("RequestUrlSender", a.URLSender),
		sql.Named("RequestModuleName", a.ModuleName),
		sql.Named("RequestUrlReceiver", a.URLReceiver),
		sql.Named("Request", a.Request),
		sql.Named("Response", a.Response),
		sql.Named("ResponseRecordStatus", a.RecordStatus),
		sql.Named("ResponseStatusDesc", a.StatusDesc),
		sql.Named("User_Insert", a.UserInsert),
		sql.Named("User_Update", a.UserInsert),
	)
	if err_ != nil {
		logger.Error("Insert Error",
			zap.String("error", err_.Error()),
		)
		return nil, err_
	}
	logger.Info("Insert Success")
	return a, nil
}
