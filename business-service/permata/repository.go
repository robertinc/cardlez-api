package permata

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PermataRepository interface {
	StoreFundTransferInquiry(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	StoreFundTransferInquiryByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error)
	StoreFundTransfer(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	StoreFundTransferByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error)
	StoreOverbookingInquiry(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	StoreOverbookingInquiryByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error)
	StoreOverbooking(ctx context.Context, a *models.PermataFundTransfer) (*models.PermataFundTransfer, error)
	StoreOverbookingByDate(ctx context.Context, a *models.PermataFundTransfer, companyDate string) (*models.PermataFundTransfer, error)
	GetOverbookinInquiryById(ctx context.Context, id *string) (*models.PermataFundTransfer, error)
	GetOverbookingById(ctx context.Context, id *string) (*models.PermataFundTransfer, error)
	StoreOVOInquiry(ctx context.Context, a *models.PermataOVO) (*models.PermataOVO, error)
	StoreOVO(ctx context.Context, a *models.PermataOVO) (*models.PermataOVO, error)
	StoreTransactionLog(ctx context.Context, a *models.TransactionLog) (*models.TransactionLog, error)
}
