package applicationconfiguration

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ApplicationConfigurationRepository interface {
	GetByConfigKey(ctx context.Context, configkey *string) (*models.ApplicationConfiguration, error)
	Fetch(ctx context.Context) ([]*models.ApplicationConfiguration, error)
	Update(ctx context.Context, config *models.ApplicationConfiguration) (*models.ApplicationConfiguration, error)
}
