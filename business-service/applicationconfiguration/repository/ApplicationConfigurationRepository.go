package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlApplicationConfigurationRepository struct {
	Conn *sql.DB
}

func NewSqlApplicationConfigurationRepository() applicationconfiguration.ApplicationConfigurationRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlApplicationConfigurationRepository{conn}
}

func NewSqlApplicationConfigurationRepositoryV2(Conn *sql.DB) applicationconfiguration.ApplicationConfigurationRepository {
	conn := Conn
	return &sqlApplicationConfigurationRepository{conn}
}

func (sj *sqlApplicationConfigurationRepository) GetByConfigKey(ctx context.Context, configkey *string) (*models.ApplicationConfiguration, error) {
	var applicationconfiguration models.ApplicationConfiguration
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Configkey, 
			ConfigValue
		from 
			dbo.ApplicationConfiguration 
		where 
		 ConfigKey = @p0`
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", configkey)).Scan(
		&applicationconfiguration.ID,
		&applicationconfiguration.ConfigKey,
		&applicationconfiguration.ConfigValue)
	if err != nil {
		return nil, err
	}
	return &applicationconfiguration, nil
}

func (sj *sqlApplicationConfigurationRepository) Fetch(ctx context.Context) ([]*models.ApplicationConfiguration, error) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		ConfigKey,
		ConfigValue
	FROM dbo.ApplicationConfiguration`
	result := make([]*models.ApplicationConfiguration, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.ApplicationConfiguration)
		err = rows.Scan(
			&j.ID,
			&j.ConfigKey,
			&j.ConfigValue,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlApplicationConfigurationRepository) Update(ctx context.Context, config *models.ApplicationConfiguration) (*models.ApplicationConfiguration, error) {

	confQuery := `Update ApplicationConfiguration 
		Set ConfigValue = @p1 
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", confQuery)),
		zap.String("obj", fmt.Sprintf("%v", config)),
	)
	_, err := sj.Conn.ExecContext(ctx, confQuery,
		sql.Named("p0", config.ID),
		sql.Named("p1", config.ConfigValue),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", config)),
	)
	return config, nil
}
