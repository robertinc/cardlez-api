package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	"gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type ApplicationConfigurationUsecase struct {
	ApplicationConfigurationRepository applicationconfiguration.ApplicationConfigurationRepository
	contextTimeout                     time.Duration
}

func NewApplicationConfigurationUsecase() applicationconfiguration.ApplicationConfigurationUsecase {
	return &ApplicationConfigurationUsecase{
		ApplicationConfigurationRepository: repository.NewSqlApplicationConfigurationRepository(),
		contextTimeout:                     time.Second * time.Duration(models.Timeout()),
	}
}

func NewApplicationConfigurationUsecaseV2(
	applicationConfigRepo applicationconfiguration.ApplicationConfigurationRepository) applicationconfiguration.ApplicationConfigurationUsecase {
	return &ApplicationConfigurationUsecase{
		ApplicationConfigurationRepository: applicationConfigRepo,
		contextTimeout:                     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *ApplicationConfigurationUsecase) GetByConfigKey(c context.Context, configkey *string) (*models.ApplicationConfiguration, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	applicationconfiguration, err := a.ApplicationConfigurationRepository.GetByConfigKey(ctx, configkey)
	if err != nil {
		return nil, err
	}
	return applicationconfiguration, nil
}

func (a *ApplicationConfigurationUsecase) Fetch(c context.Context) ([]*models.ApplicationConfiguration, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listConfig, err := a.ApplicationConfigurationRepository.Fetch(ctx)
	if err != nil {
		return nil, err
	}
	return listConfig, nil
}

func (a *ApplicationConfigurationUsecase) Update(ctx context.Context, config *models.ApplicationConfiguration) (*models.ApplicationConfiguration, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	config, err := a.ApplicationConfigurationRepository.Update(ctx, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
