package billpayment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type BillPaymentUsecase interface {
	Fetch(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error)
	Update(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error)
	GetByOperatorAmount(ctx context.Context, operator string, amount float64) (*models.BillPayment, error)
	FetchNominal(ctx context.Context, category *int32) ([]*models.BillPayment, error)
	FetchNominalBaseNew(ctx context.Context, category *int32, memberType *int32) ([]*models.BillPayment, error)
	UpdateBase(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error)
	FetchBase(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error)
	FetchNominalBase(ctx context.Context, category *int32) ([]*models.BillPayment, error)
	GetByOperatorAmountBase(ctx context.Context, operator string, amount float64) (*models.BillPayment, error)
}
