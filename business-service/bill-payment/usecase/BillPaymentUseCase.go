package usecase

import (
	"context"
	"strings"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	billpayment "gitlab.com/robertinc/cardlez-api/business-service/bill-payment"
	"gitlab.com/robertinc/cardlez-api/business-service/bill-payment/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type BillPaymentUsecase struct {
	BillPaymentRepository              billpayment.BillPaymentRepository
	ApplicationConfigurationRepository applicationconfiguration.ApplicationConfigurationRepository
	contextTimeout                     time.Duration
}

func NewBillPaymentUsecase() billpayment.BillPaymentUsecase {
	return &BillPaymentUsecase{
		BillPaymentRepository: repository.NewSqlBillPaymentRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewBillPaymentUsecaseV2(billPaymentRepo billpayment.BillPaymentRepository,
	configRepo applicationconfiguration.ApplicationConfigurationRepository) billpayment.BillPaymentUsecase {
	return &BillPaymentUsecase{
		BillPaymentRepository:              billPaymentRepo,
		ApplicationConfigurationRepository: configRepo,
		contextTimeout:                     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *BillPaymentUsecase) Fetch(c context.Context, operator *string, amount *float64) ([]*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listConfig, err := a.BillPaymentRepository.Fetch(ctx, operator, amount)
	if err != nil {
		return nil, err
	}
	return listConfig, nil
}
func (a *BillPaymentUsecase) FetchBase(c context.Context, operator *string, amount *float64) ([]*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listConfig, err := a.BillPaymentRepository.FetchBase(ctx, operator, amount)
	if err != nil {
		return nil, err
	}
	return listConfig, nil
}

func (a *BillPaymentUsecase) Update(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	config, err := a.BillPaymentRepository.Update(ctx, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
func (a *BillPaymentUsecase) UpdateBase(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	config, err := a.BillPaymentRepository.UpdateBase(ctx, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func (a *BillPaymentUsecase) GetByOperatorAmount(ctx context.Context, operator string, amount float64) (*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	billpayment, err := a.BillPaymentRepository.GetByOperatorAmount(ctx, operator, amount)
	if err != nil {
		return nil, err
	}
	return billpayment, nil
}
func (a *BillPaymentUsecase) GetByOperatorAmountBase(ctx context.Context, operator string, amount float64) (*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	billpayment, err := a.BillPaymentRepository.GetByOperatorAmountBase(ctx, operator, amount)
	if err != nil {
		return nil, err
	}
	return billpayment, nil
}

func (a *BillPaymentUsecase) FetchNominal(c context.Context, category *int32) ([]*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	configKey := "PPOBKey"
	appConfig, err := a.ApplicationConfigurationRepository.GetByConfigKey(ctx, &configKey)
	if err != nil {
		return nil, err
	}
	//appConfig.ConfigValue = "Sianyu"
	listNominal, err := a.BillPaymentRepository.FetchNominal(ctx, &appConfig.ConfigValue, category)
	if err != nil {
		return nil, err
	}

	return listNominal, nil
}
func (a *BillPaymentUsecase) FetchNominalBase(c context.Context, category *int32) ([]*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	configKey := "PPOBKey"
	if *category == 20 {
		configKey = "OVOKey"
	}
	appConfig, err := a.ApplicationConfigurationRepository.GetByConfigKey(ctx, &configKey)
	if err != nil {
		return nil, err
	}
	//appConfig.ConfigValue = "Sianyu"
	listNominal, err := a.BillPaymentRepository.FetchNominalBase(ctx, &appConfig.ConfigValue, category)
	if err != nil {
		return nil, err
	}

	return listNominal, nil
}
func (a *BillPaymentUsecase) FetchNominalBaseNew(c context.Context, category *int32, memberType *int32) ([]*models.BillPayment, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	configKey := "PPOBKey"
	if *category == 20 {
		configKey = "OVOKey"
	}
	appConfig, err := a.ApplicationConfigurationRepository.GetByConfigKey(ctx, &configKey)
	if err != nil {
		return nil, err
	}
	listNominal, err := a.BillPaymentRepository.FetchNominalBaseNew(ctx, &appConfig.ConfigValue, category, memberType)
	if err != nil {
		return nil, err
	}
	for a := 0; a < len(listNominal); a++ {
		if listNominal[a].Prefix != "" {
			listOfPrefix := strings.Split(listNominal[a].Prefix, "|")
			for _, listPrefix := range listOfPrefix {
				listNominal[a].PrefixList = append(listNominal[a].PrefixList, listPrefix)
			}
		}
	}
	return listNominal, nil
}
