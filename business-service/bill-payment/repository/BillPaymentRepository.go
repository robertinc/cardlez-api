package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	billpayment "gitlab.com/robertinc/cardlez-api/business-service/bill-payment"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlBillPaymentRepository struct {
	Conn *sql.DB
}

func NewSqlBillPaymentRepository() billpayment.BillPaymentRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlBillPaymentRepository{conn}
}

func NewSqlBillPaymentRepositoryV2(Conn *sql.DB) billpayment.BillPaymentRepository {
	conn := Conn
	return &sqlBillPaymentRepository{conn}
}

func (sj *sqlBillPaymentRepository) GetByConfigKey(ctx context.Context, configkey *string) (*models.BillPayment, error) {
	var billpayment models.BillPayment
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			Description,
			AdminAmount,
			PartnerAmount,
			InvelliAmount,
			Isnull(TransactionCode, ''),
			Isnull(Operator, ''),
			Isnull(BasePrice, 0), 
			Isnull(PurchaseAmount, 0),
			Isnull(SellPrice, 0)
		from 
			dbo.MasterTransaksiBillPayment 
		where 
		 Code = @p0`
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", configkey)).Scan(
		&billpayment.ID,
		&billpayment.Code,
		&billpayment.Description,
		&billpayment.AdminAmount,
		&billpayment.PartnerAmount,
		&billpayment.InvelliAmount,
		&billpayment.TransactionCode,
		&billpayment.Operator,
		&billpayment.BasePrice,
		&billpayment.PurchaseAmount,
		&billpayment.SellPrice,
	)
	if err != nil {
		return nil, err
	}
	return &billpayment, nil
}

func (sj *sqlBillPaymentRepository) GetByID(ctx context.Context, id string) (*models.BillPayment, error) {
	var billpayment models.BillPayment
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			Description,
			AdminAmount,
			PartnerAmount,
			InvelliAmount,
			Isnull(TransactionCode, ''),
			Isnull(Operator, ''),
			Isnull(BasePrice, 0), 
			Isnull(PurchaseAmount, 0),
			Isnull(SellPrice, 0)
		from 
			dbo.MasterTransaksiBillPayment 
		where 
		 ID = @id`
	err := sj.Conn.QueryRow(query,
		sql.Named("id", id)).Scan(
		&billpayment.ID,
		&billpayment.Code,
		&billpayment.Description,
		&billpayment.AdminAmount,
		&billpayment.PartnerAmount,
		&billpayment.InvelliAmount,
		&billpayment.TransactionCode,
		&billpayment.Operator,
		&billpayment.BasePrice,
		&billpayment.PurchaseAmount,
		&billpayment.SellPrice,
	)
	if err != nil {
		return nil, err
	}
	return &billpayment, nil
}
func (sj *sqlBillPaymentRepository) GetByOperatorAmount(ctx context.Context, operator string, amount float64) (*models.BillPayment, error) {
	var billpayment models.BillPayment
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			Description,
			AdminAmount,
			PartnerAmount,
			InvelliAmount,
			Isnull(TransactionCode, ''),
			Isnull(Operator, ''),
			Isnull(BasePrice, 0), 
			Isnull(PurchaseAmount, 0),
			Isnull(SellPrice, 0)
		from 
			dbo.MasterTransaksiBillPayment 
		where 
			Operator = @operator and PurchaseAmount = @amount`

	// and billercode ini nanti
	err := sj.Conn.QueryRow(query,
		sql.Named("operator", operator),
		sql.Named("amount", amount),
	).Scan(
		&billpayment.ID,
		&billpayment.Code,
		&billpayment.Description,
		&billpayment.AdminAmount,
		&billpayment.PartnerAmount,
		&billpayment.InvelliAmount,
		&billpayment.TransactionCode,
		&billpayment.Operator,
		&billpayment.BasePrice,
		&billpayment.PurchaseAmount,
		&billpayment.SellPrice,
	)
	if err != nil {
		return nil, err
	}
	return &billpayment, nil
}
func (sj *sqlBillPaymentRepository) GetByOperatorAmountBase(ctx context.Context, operator string, amount float64) (*models.BillPayment, error) {
	var billpayment models.BillPayment
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code,
			Description,
			AdminAmount,
			PartnerAmount,
			InvelliAmount,
			Isnull(TransactionCode, ''),
			Isnull(InquiryTransactionCode, ''),
			Isnull(Operator, ''),
			Isnull(BasePrice, 0), 
			Isnull(PurchaseAmount, 0),
			Isnull(SellPrice, 0)
		from 
			dbo.MasterTransaksiBillPayment 
		where 
			Operator = @operator and PurchaseAmount = @amount`

	// and billercode ini nanti
	err := sj.Conn.QueryRow(query,
		sql.Named("operator", operator),
		sql.Named("amount", amount),
	).Scan(
		&billpayment.ID,
		&billpayment.Code,
		&billpayment.Description,
		&billpayment.AdminAmount,
		&billpayment.PartnerAmount,
		&billpayment.InvelliAmount,
		&billpayment.TransactionCode,
		&billpayment.InquiryTransactionCode,
		&billpayment.Operator,
		&billpayment.BasePrice,
		&billpayment.PurchaseAmount,
		&billpayment.SellPrice,
	)
	if err != nil {
		return nil, err
	}
	return &billpayment, nil
}
func (sj *sqlBillPaymentRepository) Fetch(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error) {
	query := `
	SELECT 
		cast(ID as varchar(36)) ID, 
		Code,
		Description,
		AdminAmount,
		PartnerAmount,
		InvelliAmount,
		Isnull(TransactionCode, ''),
		Isnull(Operator, ''),
		Isnull(BasePrice, 0), 
		Isnull(PurchaseAmount, 0),
		Isnull(SellPrice, 0)
	FROM dbo.MasterTransaksiBillPayment`
	if operator != nil && amount != nil {
		query += ` where Operator = @operator and PurchaseAmount = @amount`
	} else if operator != nil {
		query += ` where Operator = @operator`
	} else if amount != nil {
		query += ` where PurchaseAmount = @amount`
	}
	result := make([]*models.BillPayment, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("operator", operator),
		sql.Named("amount", amount))

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.BillPayment)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.AdminAmount,
			&j.PartnerAmount,
			&j.InvelliAmount,
			&j.TransactionCode,
			&j.Operator,
			&j.BasePrice,
			&j.PurchaseAmount,
			&j.SellPrice,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlBillPaymentRepository) FetchBase(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error) {
	query := `
	SELECT 
		cast(ID as varchar(36)) ID, 
		Code,
		Description,
		AdminAmount,
		PartnerAmount,
		InvelliAmount,
		Isnull(TransactionCode, ''),
		Isnull(InquiryTransactionCode, ''),
		Isnull(Operator, ''),
		Isnull(BasePrice, 0), 
		Isnull(PurchaseAmount, 0),
		Isnull(SellPrice, 0)
	FROM dbo.MasterTransaksiBillPayment`
	if operator != nil && amount != nil {
		query += ` where Operator = @operator and PurchaseAmount = @amount`
	} else if operator != nil {
		query += ` where Operator = @operator`
	} else if amount != nil {
		query += ` where PurchaseAmount = @amount`
	}
	result := make([]*models.BillPayment, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("operator", operator),
		sql.Named("amount", amount))

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.BillPayment)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.AdminAmount,
			&j.PartnerAmount,
			&j.InvelliAmount,
			&j.TransactionCode,
			&j.InquiryTransactionCode,
			&j.Operator,
			&j.BasePrice,
			&j.PurchaseAmount,
			&j.SellPrice,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlBillPaymentRepository) Update(ctx context.Context, b *models.BillPayment) (*models.BillPayment, error) {

	confQuery := `
		Update MasterTransaksiBillPayment 
		Set 
			SellPrice = @sellPrice,
			TransactionCode = @transactioncode,
			InvelliAmount = (@sellPrice - (PurchaseAmount + BasePrice)) * 0.35,
			PartnerAmount = (@sellPrice - (PurchaseAmount + BasePrice)) * 0.65
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", confQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	_, err := sj.Conn.ExecContext(ctx, confQuery,
		sql.Named("p0", b.ID),
		sql.Named("transactioncode", b.TransactionCode),
		sql.Named("sellPrice", b.SellPrice),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", b)),
	)
	return b, nil
}
func (sj *sqlBillPaymentRepository) UpdateBase(ctx context.Context, b *models.BillPayment) (*models.BillPayment, error) {

	confQuery := `
		Update MasterTransaksiBillPayment 
		Set 
			SellPrice = @sellPrice,
			TransactionCode = @transactioncode,
			InquiryTransactionCode = @inquirytransactioncode,
			InvelliAmount = (@sellPrice - (PurchaseAmount + BasePrice)) * 0.35,
			PartnerAmount = (@sellPrice - (PurchaseAmount + BasePrice)) * 0.65
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", confQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	_, err := sj.Conn.ExecContext(ctx, confQuery,
		sql.Named("p0", b.ID),
		sql.Named("transactioncode", b.TransactionCode),
		sql.Named("inquirytransactioncode", b.InquiryTransactionCode),
		sql.Named("sellPrice", b.SellPrice),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", b)),
	)
	return b, nil
}
func (sj *sqlBillPaymentRepository) FetchNominal(ctx context.Context, billerCode *string, category *int32) ([]*models.BillPayment, error) {
	query := `
	SELECT 
		cast(ID as varchar(36)) ID, 
		Code,
		Description,
		AdminAmount,
		PartnerAmount,
		InvelliAmount,
		Isnull(TransactionCode, ''),
		Isnull(Operator, ''),
		Isnull(BasePrice, 0), 
		Isnull(PurchaseAmount, 0),
		Isnull(SellPrice, 0),
		Isnull(BillerCode, '') as BillerCode,
		Isnull(Nominal, 0) as Nominal
	FROM dbo.MasterTransaksiBillPayment 
	where BillerCode = @billerCode AND Category=@category
	order by Nominal`
	// cat := 6
	// var convCategory int32
	// convCategory = int32(cat)
	// if *category == convCategory {
	// 	query = `
	// 	SELECT
	// 		cast(ID as varchar(36)) ID,
	// 		Code,
	// 		Description,
	// 		AdminAmount,
	// 		PartnerAmount,
	// 		InvelliAmount,
	// 		Isnull(TransactionCode, ''),
	// 		Isnull(Operator, ''),
	// 		Isnull(BasePrice, 0),
	// 		Isnull(PurchaseAmount, 0),
	// 		Isnull(SellPrice, 0),
	// 		Isnull(BillerCode, '') as BillerCode,
	// 		Isnull(Nominal, 0) as Nominal
	// 	FROM dbo.MasterTransaksiBillPayment
	// 	where Category=@category order by Nominal`
	// }

	result := make([]*models.BillPayment, 0)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("billerCode", billerCode),
		sql.Named("category", category))

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.BillPayment)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.AdminAmount,
			&j.PartnerAmount,
			&j.InvelliAmount,
			&j.TransactionCode,
			&j.Operator,
			&j.BasePrice,
			&j.PurchaseAmount,
			&j.SellPrice,
			&j.BillerCode,
			&j.Nominal,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlBillPaymentRepository) FetchNominalBase(ctx context.Context, billerCode *string, category *int32) ([]*models.BillPayment, error) {
	query := `
	SELECT 
		cast(ID as varchar(36)) ID, 
		Code,
		Description,
		AdminAmount,
		PartnerAmount,
		InvelliAmount,
		Isnull(TransactionCode, ''),
		Isnull(InquiryTransactionCode, ''),
		Isnull(Operator, ''),
		Isnull(BasePrice, 0), 
		Isnull(PurchaseAmount, 0),
		Isnull(SellPrice, 0),
		Isnull(BillerCode, '') as BillerCode,
		Isnull(Nominal, 0) as Nominal
	FROM dbo.MasterTransaksiBillPayment `
	cat := 9
	var convCategory int32
	convCategory = int32(cat)
	if *category == convCategory {
		query += ` where Category=@category order by Description`
	} else {
		query += ` where BillerCode = @billerCode AND Category=@category order by Nominal`
	}
	// cat := 6
	// var convCategory int32
	// convCategory = int32(cat)
	// if *category == convCategory {
	// 	query = `
	// 	SELECT
	// 		cast(ID as varchar(36)) ID,
	// 		Code,
	// 		Description,
	// 		AdminAmount,
	// 		PartnerAmount,
	// 		InvelliAmount,
	// 		Isnull(TransactionCode, ''),
	// 		Isnull(Operator, ''),
	// 		Isnull(BasePrice, 0),
	// 		Isnull(PurchaseAmount, 0),
	// 		Isnull(SellPrice, 0),
	// 		Isnull(BillerCode, '') as BillerCode,
	// 		Isnull(Nominal, 0) as Nominal
	// 	FROM dbo.MasterTransaksiBillPayment
	// 	where Category=@category order by Nominal`
	// }

	result := make([]*models.BillPayment, 0)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("billerCode", billerCode),
		sql.Named("category", category))

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.BillPayment)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.AdminAmount,
			&j.PartnerAmount,
			&j.InvelliAmount,
			&j.TransactionCode,
			&j.InquiryTransactionCode,
			&j.Operator,
			&j.BasePrice,
			&j.PurchaseAmount,
			&j.SellPrice,
			&j.BillerCode,
			&j.Nominal,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlBillPaymentRepository) FetchNominalBaseNew(ctx context.Context, billerCode *string, category *int32, memberType *int32) ([]*models.BillPayment, error) {
	query := `
	SELECT 
		cast(ID as varchar(36)) ID, 
		Code,
		Description,
		AdminAmount,
		PartnerAmount,
		InvelliAmount,
		0.0 AS AgentAmount,
		Isnull(TransactionCode, ''),
		Isnull(InquiryTransactionCode, ''),
		Isnull(Operator, ''),
		Isnull(BasePrice, 0), 
		Isnull(PurchaseAmount, 0),
		Isnull(SellPrice, 0),
		Isnull(BillerCode, '') as BillerCode,
		Isnull(Nominal, 0) as Nominal,
		Isnull(Prefix, '') as Prefix
	FROM dbo.MasterTransaksiBillPayment `
	if *memberType == 9 { //jika agent
		query = `
		SELECT 
			cast(ID as varchar(36)) ID, 
			Code,
			Description,
			AdminAmount,
			PartnerAmount,
			InvelliAmount,
			AgentAmount,
			Isnull(TransactionCode, ''),
			Isnull(InquiryTransactionCode, ''),
			Isnull(Operator, ''),
			Isnull(BasePrice, 0), 
			Isnull(PurchaseAmount, 0),
			Isnull(SellPrice, 0),
			Isnull(BillerCode, '') as BillerCode,
			Isnull(Nominal, 0) as Nominal,
			Isnull(Prefix, '') as Prefix
		FROM dbo.MasterTransaksiBillPaymentAgent `
	}
	cat := 9
	var convCategory int32
	convCategory = int32(cat)
	if *category == convCategory {
		query += ` where Category=@category order by Description`
	} else {
		query += ` where BillerCode = @billerCode AND Category=@category order by Nominal`
	}
	result := make([]*models.BillPayment, 0)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("billerCode", billerCode),
		sql.Named("category", category))

	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.BillPayment)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.Description,
			&j.AdminAmount,
			&j.PartnerAmount,
			&j.InvelliAmount,
			&j.AgentAmount,
			&j.TransactionCode,
			&j.InquiryTransactionCode,
			&j.Operator,
			&j.BasePrice,
			&j.PurchaseAmount,
			&j.SellPrice,
			&j.BillerCode,
			&j.Nominal,
			&j.Prefix,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
