package billpayment

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type BillPaymentRepository interface {
	GetByConfigKey(ctx context.Context, configkey *string) (*models.BillPayment, error)
	GetByID(ctx context.Context, id string) (*models.BillPayment, error)
	GetByOperatorAmount(ctx context.Context, operator string, amount float64) (*models.BillPayment, error)
	Fetch(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error)
	Update(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error)
	FetchNominal(ctx context.Context, billerCode *string, category *int32) ([]*models.BillPayment, error)
	FetchNominalBaseNew(ctx context.Context, billerCode *string, category *int32, memberType *int32) ([]*models.BillPayment, error)
	FetchBase(ctx context.Context, operator *string, amount *float64) ([]*models.BillPayment, error)
	FetchNominalBase(ctx context.Context, billerCode *string, category *int32) ([]*models.BillPayment, error)
	GetByOperatorAmountBase(ctx context.Context, operator string, amount float64) (*models.BillPayment, error)
	UpdateBase(ctx context.Context, config *models.BillPayment) (*models.BillPayment, error)
}
