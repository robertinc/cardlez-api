package usecase

import (
	"context"
	"time"

	menu "gitlab.com/robertinc/cardlez-api/business-service/menu"
	"gitlab.com/robertinc/cardlez-api/business-service/menu/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MenuUsecase struct {
	MenuRepository menu.MenuRepository
	contextTimeout time.Duration
}

func NewMenuUsecase() menu.MenuUsecase {
	return &MenuUsecase{
		MenuRepository: repository.NewSqlMenuRepository(),
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func NewMenuUsecaseV2(menuRepo menu.MenuRepository) menu.MenuUsecase {
	return &MenuUsecase{
		MenuRepository: menuRepo,
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func (a *MenuUsecase) Fetch(c context.Context, parentMenuID string) ([]*models.Menu, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMenu, err := a.MenuRepository.Fetch(ctx, parentMenuID)
	if err != nil {
		return nil, err
	}
	return listMenu, nil
}
