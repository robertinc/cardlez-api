package menu

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MenuRepository interface {
	Fetch(ctx context.Context, parentMenuID string) ([]*models.Menu, error)
	GetByID(ctx context.Context, id string) (*models.Menu, error)
}
