package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	menu "gitlab.com/robertinc/cardlez-api/business-service/menu"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlMenuRepository struct {
	Conn *sql.DB
}

func NewSqlMenuRepository() menu.MenuRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMenuRepository{conn}
}

func NewSqlMenuRepositoryV2(Conn *sql.DB) menu.MenuRepository {
	conn := Conn
	return &sqlMenuRepository{conn}
}
func (sj *sqlMenuRepository) Fetch(ctx context.Context, parentID string) ([]*models.Menu, error) {
	logger := service.Logger(ctx)
	query := `
	SELECT 
		CAST(m.ID as varchar(36)) as ID,
		CAST(m.ParentMenuID as varchar(36)) as ParentMenuID,
		m.MenuName,
		m.Icon,
		m.Link,
		m.Sequence
	FROM 
		Menu m
		INNER JOIN RoleMenu rm on m.ID = rm.MenuID
		INNER JOIN CompanyUser cu on cu.CompanyUserRoleID = rm.CompanyRoleID
	WHERE 
		ParentMenuID = @parentMenuID
		AND cu.ID = @companyUserID
		AND rm.AccessLevel <> 0
	order by sequence		
	`
	logger.Info("Query MENU",
		zap.String("query", query),
		zap.String("parentMenuID", parentID),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("parentMenuID", parentID),
		sql.Named("companyUserID", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Query Response Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	result := make([]*models.Menu, 0)
	defer rows.Close()
	for rows.Next() {
		j := new(models.Menu)
		err = rows.Scan(
			&j.ID,
			&j.ParentMenuID,
			&j.MenuName,
			&j.Icon,
			&j.Link,
			&j.Sequence,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlMenuRepository) GetByID(ctx context.Context, id string) (*models.Menu, error) {
	var menu models.Menu
	logger := service.Logger(ctx)
	query := `
	select 
		CAST(m.ID as varchar(36)) as ID,
		CAST(m.ParentMenuID as varchar(36)) as ParentMenuID,
		m.MenuName,
		m.Icon,
		m.Link,
		m.Sequence
	from 
		Menu m
	where 
		ID = @p0`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&menu.ID,
		&menu.ParentMenuID,
		&menu.MenuName,
		&menu.Icon,
		&menu.Link,
		&menu.Sequence,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", menu)),
	)
	return &menu, nil
}
