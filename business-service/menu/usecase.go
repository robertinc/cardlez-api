package menu

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MenuUsecase interface {
	Fetch(ctx context.Context, parentMenuID string) ([]*models.Menu, error)
}
