package accgroupcondition

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AccGroupConditionUsecase interface {
	Fetch(ctx context.Context) (error, []*models.AccGroupCondition)
	GetByID(ctx context.Context, Id string) (error, *models.AccGroupCondition)
	GetDailyLimit(ctx context.Context, accId string, transferType int64) (error, int64)
	GetDailyMaxAmountTransaction(ctx context.Context, accId string, transferType int64) (error, float64)
	FetchIDS(ctx context.Context) (error, []*models.AccGroupCondition)
	UpdateIDS(ctx context.Context, b *models.AccGroupCondition) (*models.AccGroupCondition, error)
}
type AccGroupConditionDetailUsecase interface {
	GetByAccGroupID(ctx context.Context, accId string, tenor int32) (error, *models.AccGroupConditionDetail)
	Fetch(ctx context.Context, accId string) (error, []*models.AccGroupConditionDetail)
	GetTenorIDS(ctx context.Context, accId string) (error, []*models.AccGroupConditionDetail)
	GetInterestIDS(ctx context.Context, code *string, tenor *int32, amount *float64) (error, *models.AccGroupConditionDetail)
}
