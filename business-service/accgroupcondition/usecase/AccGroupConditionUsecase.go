package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition"
	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type AccGroupConditionUsecase struct {
	AccGroupConditionRepository accgroupcondition.AccGroupConditionRepository
	contextTimeout              time.Duration
}

func NewAccGroupConditionUsecase() accgroupcondition.AccGroupConditionUsecase {
	return &AccGroupConditionUsecase{
		AccGroupConditionRepository: repository.NewSqlAccGroupConditionRepository(),
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func NewAccGroupConditionUsecaseV2(accGroupConditionRepo accgroupcondition.AccGroupConditionRepository) accgroupcondition.AccGroupConditionUsecase {
	return &AccGroupConditionUsecase{
		AccGroupConditionRepository: accGroupConditionRepo,
		contextTimeout:              time.Second * time.Duration(models.Timeout()),
	}
}

func (a *AccGroupConditionUsecase) Fetch(c context.Context) (error, []*models.AccGroupCondition) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listAccGroupCondition := a.AccGroupConditionRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listAccGroupCondition
}
func (a *AccGroupConditionUsecase) GetByID(c context.Context, id string) (error, *models.AccGroupCondition) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accGroupCondition := a.AccGroupConditionRepository.GetByID(ctx, id)
	if err != nil {
		return err, nil
	}
	return nil, accGroupCondition
}
func (a *AccGroupConditionUsecase) GetDailyLimit(c context.Context, accId string, transferType int64) (error, int64) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, dailyLimit := a.AccGroupConditionRepository.GetDailyLimit(ctx, accId, transferType)
	if err != nil {
		return err, 0
	}
	return nil, dailyLimit
}
func (a *AccGroupConditionUsecase) GetDailyMaxAmountTransaction(c context.Context, accId string, transferType int64) (error, float64) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, dailyMaxAmount := a.AccGroupConditionRepository.GetDailyMaxAmountTransaction(ctx, accId, transferType)
	if err != nil {
		return err, 0
	}
	return nil, dailyMaxAmount
}
func (a *AccGroupConditionUsecase) FetchIDS(c context.Context) (error, []*models.AccGroupCondition) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listAccGroupCondition := a.AccGroupConditionRepository.FetchIDS(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listAccGroupCondition
}
func (a *AccGroupConditionUsecase) UpdateIDS(c context.Context, b *models.AccGroupCondition) (*models.AccGroupCondition, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listAccGroupCondition, err := a.AccGroupConditionRepository.UpdateIDS(ctx, b)
	if err != nil {
		return nil, err
	}
	return listAccGroupCondition, nil
}
