package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition"
	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type AccGroupConditionDetailUsecase struct {
	AccGroupConditionDetailRepository accgroupcondition.AccGroupConditionDetailRepository
	contextTimeout                    time.Duration
}

func NewAccGroupConditionDetailUsecase() accgroupcondition.AccGroupConditionDetailUsecase {
	return &AccGroupConditionDetailUsecase{
		AccGroupConditionDetailRepository: repository.NewSqlAccGroupConditionDetailRepository(),
		contextTimeout:                    time.Second * time.Duration(models.Timeout()),
	}
}

func NewAccGroupConditionDetailUsecaseV2(accGroupConditionDetailRepo accgroupcondition.AccGroupConditionDetailRepository) accgroupcondition.AccGroupConditionDetailUsecase {
	return &AccGroupConditionDetailUsecase{
		AccGroupConditionDetailRepository: accGroupConditionDetailRepo,
		contextTimeout:                    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *AccGroupConditionDetailUsecase) GetByAccGroupID(c context.Context, accId string, tenor int32) (error, *models.AccGroupConditionDetail) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accGroupConditionDetail := a.AccGroupConditionDetailRepository.GetByAccGroupID(ctx, accId, tenor)
	if err != nil {
		return err, nil
	}
	return nil, accGroupConditionDetail
}
func (a *AccGroupConditionDetailUsecase) Fetch(c context.Context, accId string) (error, []*models.AccGroupConditionDetail) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accGroupConditionDetail := a.AccGroupConditionDetailRepository.Fetch(ctx, accId)
	if err != nil {
		return err, nil
	}
	return nil, accGroupConditionDetail
}
func (a *AccGroupConditionDetailUsecase) GetTenorIDS(c context.Context, accId string) (error, []*models.AccGroupConditionDetail) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accGroupConditionDetail := a.AccGroupConditionDetailRepository.GetTenorIDS(ctx, accId)
	if err != nil {
		return err, nil
	}
	return nil, accGroupConditionDetail
}
func (a *AccGroupConditionDetailUsecase) GetInterestIDS(c context.Context, id *string, tenor *int32, amount *float64) (error, *models.AccGroupConditionDetail) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, accGroupConditionDetail := a.AccGroupConditionDetailRepository.GetInterestIDS(ctx, id, tenor, amount)
	if err != nil {
		return err, nil
	}
	return nil, accGroupConditionDetail
}
