package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlAccGroupConditionRepository struct {
	Conn *sql.DB
}

func NewSqlAccGroupConditionRepository() accgroupcondition.AccGroupConditionRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlAccGroupConditionRepository{conn}
}

func NewSqlAccGroupConditionRepositoryV2(Conn *sql.DB) accgroupcondition.AccGroupConditionRepository {
	conn := Conn
	return &sqlAccGroupConditionRepository{conn}
}

func (sj *sqlAccGroupConditionRepository) Fetch(ctx context.Context) (error, []*models.AccGroupCondition) {
	query := `
	SELECT 
	Cast(AccGroupCondition_ID as varchar(36)) as AccGroupCondition_ID,
	Isnull(AccGroupCondition_No, '') as AccGroupCondition_No,
	Isnull(Minimum_Balance, 0) as Minimum_Balance,
	Deposit_Period,
	Isnull(Minimum_Deposit, 0) as Minimum_Deposit,
	Isnull(Maximum_Deposit, 0) as Maximum_Deposit,
	Withdrawal_Period,
	Isnull(Minimum_Withdrawal, 0) as Minimum_Withdrawal,
	Isnull(Maximum_Withdrawal, 0) as Maximum_Withdrawal,
	IsDebit_Restrict,
	Interest_Post_Period,
	Inactive_Months,
	Rev_No,
	Isnull(Rev_Remarks, '') as Rev_Remarks,
	Record_Status,
	Isnull(User_Insert, '') as User_Insert,
	Isnull(Date_Insert, '') as Date_Insert,
	Isnull(User_Author, '') as User_Author,
	Isnull(Date_Author, '') as Date_Author,
	Isnull(User_Update, '') as User_Update,
	Isnull(Date_Update, '') as Date_Update,
	Isnull(Maximum_Transfer_Antar_Nasabah, 0) as Maximum_Transfer_Antar_Nasabah,
	Isnull(Maximum_Transfer_Antar_Bank, 0) as Maximum_Transfer_Antar_Bank
	FROM [Master.AccGroupCondition]`
	result := make([]*models.AccGroupCondition, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.AccGroupCondition)
		err = rows.Scan(
			&j.AccGroupCondition_ID,
			&j.AccGroupCondition_No,
			&j.Minimum_Balance,
			&j.Deposit_Period,
			&j.Minimum_Deposit,
			&j.Maximum_Deposit,
			&j.Withdrawal_Period,
			&j.Minimum_Withdrawal,
			&j.Maximum_Withdrawal,
			&j.IsDebit_Restrict,
			&j.Interest_Post_Period,
			&j.Inactive_Months,
			&j.Rev_No,
			&j.Rev_Remarks,
			&j.Record_Status,
			&j.User_Insert,
			&j.Date_Insert,
			&j.User_Author,
			&j.Date_Author,
			&j.User_Update,
			&j.Date_Update,
			&j.Maximum_Transfer_Antar_Nasabah,
			&j.Maximum_Transfer_Antar_Bank,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlAccGroupConditionRepository) GetByID(ctx context.Context, id string) (error, *models.AccGroupCondition) {
	var j models.AccGroupCondition
	query := `
	SELECT 
	Cast(ID as varchar(36)) as ID,
	Isnull(Code, '') as Code,
	Isnull(MinimumBalance, 0) as MinimumBalance,
	DepositPeriod,
	Isnull(MinimumDeposit, 0) as MinimumDeposit,
	Isnull(MaximumDeposit, 0) as MaximumDeposit,
	WithdrawalPeriod,
	Isnull(MinimumWithdrawal, 0) as MinimumWithdrawal,
	Isnull(MaximumWithdrawal, 0) as MaximumWithdrawal,
	IsDebitRestrict,
	InterestPostPeriod,
	InactiveMonths,
	RevNo,
	Isnull(RevRemarks, '') as RevRemarks,
	RecordStatus,
	Isnull(UserInsert, '00000000-0000-0000-0000-000000000000') as UserInsert,
	Isnull(DateInsert, '') as DateInsert,
	Isnull(UserAuthor, '00000000-0000-0000-0000-000000000000') as UserAuthor,
	Isnull(DateAuthor, '') as DateAuthor,
	Isnull(UserUpdate, '00000000-0000-0000-0000-000000000000') as UserUpdate,
	Isnull(DateUpdate, '') as DateUpdate,
	Isnull(OverbookingDailyLimit, 0) as OverbookingDailyLimit,
	Isnull(TransferDailyLimit, 0) as TransferDailyLimit,
	Isnull(OverbookingMinAmount, 0) as OverbookingMinAmount,
	Isnull(TransferMinAmount, 0) as TransferMinAmount,
	Isnull(OverbookingMaxAmount, 0) as OverbookingMaxAmount,
	Isnull(TransferMaxAmount, 0) as TransferMaxAmount,
	Isnull(OverbookingMaxDailyAmountTotal, 0) as OverbookingMaxDailyAmountTotal,
	Isnull(TransferMaxDailyAmountTotal, 0) as TransferMaxDailyAmountTotal
	FROM AccGroupCondition
	where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("Id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&j.AccGroupCondition_ID,
		&j.AccGroupCondition_No,
		&j.Minimum_Balance,
		&j.Deposit_Period,
		&j.Minimum_Deposit,
		&j.Maximum_Deposit,
		&j.Withdrawal_Period,
		&j.Minimum_Withdrawal,
		&j.Maximum_Withdrawal,
		&j.IsDebit_Restrict,
		&j.Interest_Post_Period,
		&j.Inactive_Months,
		&j.Rev_No,
		&j.Rev_Remarks,
		&j.Record_Status,
		&j.User_Insert,
		&j.Date_Insert,
		&j.User_Author,
		&j.Date_Author,
		&j.User_Update,
		&j.Date_Update,
		&j.OverbookingDailyLimit,
		&j.TransferDailyLimit,
		&j.OverbookingMinAmount,
		&j.TransferMinAmount,
		&j.Maximum_Transfer_Antar_Nasabah,
		&j.Maximum_Transfer_Antar_Bank,
		&j.OverbookingMaxDailyAmountTotal,
		&j.TransferMaxDailyAmountTotal)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}

func (sj *sqlAccGroupConditionRepository) GetDailyLimit(ctx context.Context, accId string, transferType int64) (error, int64) {
	var jumlahDailyLimit int64
	t := time.Now()
	layout2 := "2006-01-02"
	layoutSQL := "2006-01-02 15:04:05"
	timeFormat := t.Format(layout2)
	currentDate, _ := time.Parse(layout2, timeFormat)
	convertCurrentDate := time.Date(currentDate.Year(), currentDate.Month(), currentDate.Day(), 0, 0, 0, 0, currentDate.Location()).Format(layoutSQL)
	query := `
	select count(DebitAccountID) from FundTransfer 
	where DebitAccountID=@p0 and TransferType=@p1 and RecordStatus=2 
	and (DebitDate between @p2 and GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("accId", fmt.Sprintf("%v", accId)),
		zap.String("transferType", fmt.Sprintf("%v", transferType)),
		zap.String("currentdate", fmt.Sprintf("%v", convertCurrentDate)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", accId),
		sql.Named("p1", transferType),
		sql.Named("p2", convertCurrentDate)).Scan(
		&jumlahDailyLimit)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, 0
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", jumlahDailyLimit)),
	)
	return nil, jumlahDailyLimit
}

func (sj *sqlAccGroupConditionRepository) GetDailyMaxAmountTransaction(ctx context.Context, accId string, transferType int64) (error, float64) {
	var jumlahMaxAmount float64
	t := time.Now()
	layout2 := "2006-01-02"
	layoutSQL := "2006-01-02 15:04:05"
	timeFormat := t.Format(layout2)
	currentDate, _ := time.Parse(layout2, timeFormat)
	convertCurrentDate := time.Date(currentDate.Year(), currentDate.Month(), currentDate.Day(), 0, 0, 0, 0, currentDate.Location()).Format(layoutSQL)
	query := ` 
	select Isnull(SUM(Amount), 0) from FundTransfer 
	where DebitAccountID=@p0 and TransferType=@p1
	and (DebitDate between @p2 and GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("accId", fmt.Sprintf("%v", accId)),
		zap.String("transferType", fmt.Sprintf("%v", transferType)),
		zap.String("currendate", fmt.Sprintf("%v", convertCurrentDate)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", accId),
		sql.Named("p1", transferType),
		sql.Named("p2", convertCurrentDate)).Scan(
		&jumlahMaxAmount)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, 0
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", jumlahMaxAmount)),
	)
	return nil, jumlahMaxAmount
}

func (sj *sqlAccGroupConditionRepository) FetchIDS(ctx context.Context) (error, []*models.AccGroupCondition) {
	query := `
	SELECT 
	Cast(ID as varchar(36)) as ID,
	Isnull(Code, '') as Code,
	Isnull(MinimumBalance, 0) as MinimumBalance,
	DepositPeriod,
	Isnull(MinimumDeposit, 0) as MinimumDeposit,
	Isnull(MaximumDeposit, 0) as MaximumDeposit,
	WithdrawalPeriod,
	Isnull(MinimumWithdrawal, 0) as MinimumWithdrawal,
	Isnull(MaximumWithdrawal, 0) as MaximumWithdrawal,
	IsDebitRestrict,
	InterestPostPeriod,
	InactiveMonths,
	RevNo,
	Isnull(RevRemarks, '') as RevRemarks,
	RecordStatus,
	Isnull(Cast(UserInsert as varchar(36)), Cast('00000000-0000-0000-0000-000000000000' as varchar(36))) as UserInsert,
	Isnull(DateInsert, '') as DateInsert,
	Isnull(Cast(UserAuthor as varchar(36)), Cast('00000000-0000-0000-0000-000000000000' as varchar(36))) as UserAuthor,
	Isnull(DateAuthor, '') as DateAuthor,
	Isnull(Cast(UserUpdate as varchar(36)),Cast('00000000-0000-0000-0000-000000000000' as varchar(36))) as UserUpdate,
	Isnull(DateUpdate, '') as DateUpdate,
	Isnull(OverbookingDailyLimit, 0) as OverbookingDailyLimit,
	Isnull(TransferDailyLimit, 0) as TransferDailyLimit,
	Isnull(OverbookingMinAmount, 0) as OverbookingMinAmount,
	Isnull(TransferMinAmount, 0) as TransferMinAmount,
	Isnull(OverbookingMaxAmount, 0) as OverbookingMaxAmount,
	Isnull(TransferMaxAmount, 0) as TransferMaxAmount,
	Isnull(OverbookingMaxDailyAmountTotal, 0) as OverbookingMaxDailyAmountTotal,
	Isnull(TransferMaxDailyAmountTotal, 0) as TransferMaxDailyAmountTotal
	FROM AccGroupCondition`
	result := make([]*models.AccGroupCondition, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.AccGroupCondition)
		err = rows.Scan(
			&j.AccGroupCondition_ID,
			&j.AccGroupCondition_No,
			&j.Minimum_Balance,
			&j.Deposit_Period,
			&j.Minimum_Deposit,
			&j.Maximum_Deposit,
			&j.Withdrawal_Period,
			&j.Minimum_Withdrawal,
			&j.Maximum_Withdrawal,
			&j.IsDebit_Restrict,
			&j.Interest_Post_Period,
			&j.Inactive_Months,
			&j.Rev_No,
			&j.Rev_Remarks,
			&j.Record_Status,
			&j.User_Insert,
			&j.Date_Insert,
			&j.User_Author,
			&j.Date_Author,
			&j.User_Update,
			&j.Date_Update,
			&j.OverbookingDailyLimit,
			&j.TransferDailyLimit,
			&j.OverbookingMinAmount,
			&j.TransferMinAmount,
			&j.Maximum_Transfer_Antar_Nasabah,
			&j.Maximum_Transfer_Antar_Bank,
			&j.OverbookingMaxDailyAmountTotal,
			&j.TransferMaxDailyAmountTotal)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlAccGroupConditionRepository) UpdateIDS(ctx context.Context, b *models.AccGroupCondition) (*models.AccGroupCondition, error) {

	query := `
		Update AccGroupCondition 
		Set 
		OverbookingDailyLimit=@p1,
		TransferDailyLimit=@p2,
		OverbookingMinAmount=@p3,
		TransferMinAmount=@p4,
		OverbookingMaxAmount=@p5,
		TransferMaxAmount=@p6,
		OverbookingMaxDailyAmountTotal=@p7,
		TransferMaxDailyAmountTotal=@p8,
		DateUpdate = GETDATE() 
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("p0", b.AccGroupCondition_ID),
		sql.Named("p1", b.OverbookingDailyLimit),
		sql.Named("p2", b.TransferDailyLimit),
		sql.Named("p3", b.OverbookingMinAmount),
		sql.Named("p4", b.TransferMinAmount),
		sql.Named("p5", b.Maximum_Transfer_Antar_Nasabah),
		sql.Named("p6", b.Maximum_Transfer_Antar_Bank),
		sql.Named("p7", b.OverbookingMaxDailyAmountTotal),
		sql.Named("p8", b.TransferMaxDailyAmountTotal),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", b)),
	)
	return b, nil
}
