package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/accgroupcondition"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlAccGroupConditionDetailRepository struct {
	Conn *sql.DB
}

func NewSqlAccGroupConditionDetailRepository() accgroupcondition.AccGroupConditionDetailRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlAccGroupConditionDetailRepository{conn}
}

func NewSqlAccGroupConditionDetailRepositoryV2(Conn *sql.DB) accgroupcondition.AccGroupConditionDetailRepository {
	conn := Conn
	return &sqlAccGroupConditionDetailRepository{conn}
}

func (sj *sqlAccGroupConditionDetailRepository) GetByAccGroupID(ctx context.Context, accId string, tenor int32) (error, *models.AccGroupConditionDetail) {
	var accGroupConditionDetail models.AccGroupConditionDetail
	query := `
	SELECT 
	Cast(AccGroupConditionDetail_ID as varchar(36)) as AccGroupConditionDetail_ID,
	Cast(AccGroupCondition_ID as varchar(36)) as AccGroupCondition_ID,
	Isnull(Minimum_Saldo, 0) as Minimum_Saldo,
	Isnull(Maximum_Saldo, 0) as Maximum_Saldo,
	Isnull(Interest_Rate, 0) as Interest_Rate,
	Isnull(User_Insert, '') as User_Insert,
	Isnull(Date_Insert, '') as Date_Insert,
	Isnull(User_Update, '') as User_Update,
	Isnull(Date_Update, '') as Date_Update
	FROM [Master.AccGroupCondition.Detail]
	where AccGroupCondition_ID = @p0 and (Minimum_Saldo<=@p1 and Maximum_Saldo>=@p1)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("accId", fmt.Sprintf("%v", accId)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", accId),
		sql.Named("p1", tenor)).Scan(
		&accGroupConditionDetail.AccGroupConditionDetail_ID,
		&accGroupConditionDetail.AccGroupCondition_ID,
		&accGroupConditionDetail.Minimum_Saldo,
		&accGroupConditionDetail.Maximum_Saldo,
		&accGroupConditionDetail.Interest_Rate,
		&accGroupConditionDetail.User_Insert,
		&accGroupConditionDetail.Date_Insert,
		&accGroupConditionDetail.User_Update,
		&accGroupConditionDetail.Date_Update)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", accGroupConditionDetail)),
	)
	return nil, &accGroupConditionDetail
}

func (sj *sqlAccGroupConditionDetailRepository) Fetch(ctx context.Context, accId string) (error, []*models.AccGroupConditionDetail) {
	query := `
	SELECT 
	Cast(AccGroupConditionDetail_ID as varchar(36)) as AccGroupConditionDetail_ID,
	Cast(AccGroupCondition_ID as varchar(36)) as AccGroupCondition_ID,
	Isnull(Minimum_Saldo, 0) as Minimum_Saldo,
	Isnull(Maximum_Saldo, 0) as Maximum_Saldo,
	Isnull(Interest_Rate, 0) as Interest_Rate,
	Isnull(User_Insert, '') as User_Insert,
	Isnull(Date_Insert, '') as Date_Insert,
	Isnull(User_Update, '') as User_Update,
	Isnull(Date_Update, '') as Date_Update
	FROM [Master.AccGroupCondition.Detail]
	where AccGroupCondition_ID = @p0`
	result := make([]*models.AccGroupConditionDetail, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", accId))
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		accGroupConditionDetail := new(models.AccGroupConditionDetail)
		err = rows.Scan(
			&accGroupConditionDetail.AccGroupConditionDetail_ID,
			&accGroupConditionDetail.AccGroupCondition_ID,
			&accGroupConditionDetail.Minimum_Saldo,
			&accGroupConditionDetail.Maximum_Saldo,
			&accGroupConditionDetail.Interest_Rate,
			&accGroupConditionDetail.User_Insert,
			&accGroupConditionDetail.Date_Insert,
			&accGroupConditionDetail.User_Update,
			&accGroupConditionDetail.Date_Update,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, accGroupConditionDetail)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAccGroupConditionDetailRepository) GetTenorIDS(ctx context.Context, accId string) (error, []*models.AccGroupConditionDetail) {
	query := `
	SELECT distinct Tenor
	FROM AccGroupConditionDetail
	where AccGroupConditionID = @p0
	order by Tenor asc`
	result := make([]*models.AccGroupConditionDetail, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", accId))
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		accGroupConditionDetail := new(models.AccGroupConditionDetail)
		err = rows.Scan(
			&accGroupConditionDetail.Tenor,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, accGroupConditionDetail)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAccGroupConditionDetailRepository) GetInterestIDS(ctx context.Context, code *string, tenor *int32, amount *float64) (error, *models.AccGroupConditionDetail) {
	var accGroupConditionDetail models.AccGroupConditionDetail
	query := `
	select a.InterestRate from AccGroupConditionDetail a
	inner join RetailProduct r 
	on a.AccGroupConditionID=r.AccGroupConditionID
	where r.Code=@p0 and (a.MinimumSaldo<=@p1 and a.MaximumSaldo>=@p1) and a.Tenor=@p2`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", code)),
		zap.String("tenor", fmt.Sprintf("%v", tenor)),
		zap.String("amount", fmt.Sprintf("%v", amount)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code),
		sql.Named("p1", amount),
		sql.Named("p2", tenor)).Scan(
		&accGroupConditionDetail.Interest_Rate)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", accGroupConditionDetail)),
	)
	return nil, &accGroupConditionDetail
}
