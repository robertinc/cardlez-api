package member

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberUsecase interface {
	Fetch(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32) (error, []*models.Member)
	FetchAll(ctx context.Context) (error, []*models.Member)
	FetchBase(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32, searchViewParam *string) (error, []*models.Member)
	Pagination(ctx context.Context, order string, direction string, first int32, count int32, memberType int32, filter string, filterValue string) (error, []*models.Member)

	Count(ctx context.Context, memberType int32, filter string, filterValue string) (error, int32)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	GetByID(ctx context.Context, id *string) (*models.Member, error)
	GetByIDBase(ctx context.Context, id *string) (*models.Member, error)
	GetByCodeBase(ctx context.Context, code *string) (*models.Member, error)
	GetByHandphone(ctx context.Context, handphone *string) (*models.Member, error)
	GetByEmail(ctx context.Context, email *string) (*models.Member, error)
	CheckPIN(ctx context.Context, id *string, pin *string) (*bool, error)
	GetCategory(ctx context.Context, id *string) *models.Category
	Update(ctx context.Context, member *models.Member) (*models.Member, error)
	UpdateBase(ctx context.Context, member *models.Member) (*models.Member, error)
	UpdateBaseByDate(ctx context.Context, member *models.Member, companyDate string) (*models.Member, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Member) (*models.Member, error)
	LockAccount(ctx context.Context, memberID *string) (bool, error)
	SendMessage(ctx context.Context, phone *string, message *string) (bool, error)
	SendVerification(ctx context.Context, phone *string, email *string) (bool, error)
	ChangePassword(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool) (bool, error)
	ChangePasswordBase(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool, isnewmember *bool) (bool, error)
	ChangePasswordBaseByDate(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool, isnewmember *bool, companyDate string) (bool, error)
	UpdateIMEI(ctx context.Context, imei string, id string) (bool, error)
	GetByIMEI(ctx context.Context, imei *string) (*models.Member, error)
	//member detail KYC
	GetMemberDetailByMemberID(ctx context.Context, memberID *string) (*models.MemberDetail, error)
	StoreMemberDetail(ctx context.Context, a *models.MemberDetail) (*models.MemberDetail, error)
	StoreMemberDetailByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error)
	StoreMemberDetailFromUploadByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error)
	UpdateMemberDetail(ctx context.Context, memberDetail *models.MemberDetail) (*models.MemberDetail, error)
	UpdateMemberDetailByDate(ctx context.Context, memberDetail *models.MemberDetail, companyDate string) (*models.MemberDetail, error)
	//member kyc untuk export ke excel
	FetchMemberKYC(ctx context.Context) (error, []*models.MemberKYC)

	RegularSendSMSSuccessConsoleWA(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
}

type MemberBranchUsecase interface {
	GetByMemberID(ctx context.Context, id *string) ([]*models.MemberBranch, error)
}

type MemberLoginUsecase interface {
	GetByMemberBranchID(ctx context.Context, id *string) ([]*models.MemberLogin, error)
}

type MemberMailboxUsecase interface {
	GetMemberMailbox(ctx context.Context, mailboxID *string) ([]*models.MemberMailbox, error)
	FetchMailbox(c context.Context) ([]*models.MemberMailbox, error)
	Update(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error)
	UpdateByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error)
	Store(context.Context, *models.MemberMailbox) (*models.MemberMailbox, error)
	StoreByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error)
	UpdateMailbox(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error)
	BulkDelete(ctx context.Context, ids []string) (bool, error)
	GetByMailboxId(ctx context.Context, memberID *string, mailboxId *string) (*models.MemberMailbox, error)
}
type MemberCouponUsecase interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.MemberCoupon)
	GetByMemberID(ctx context.Context, id *string) ([]*models.MemberCoupon, error)
	Store(context.Context, *models.MemberCoupon) (*models.MemberCoupon, error)
	StoreByDate(ctx context.Context, a *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error)
	RedeemCoupon(c context.Context, b *models.MemberCoupon, accountID string, cashBackAmount float64) (*models.MemberCoupon, error)
	RedeemCouponByDate(c context.Context, b *models.MemberCoupon, accountID string, cashBackAmount float64, companyDate string) (*models.MemberCoupon, error)
	Update(context.Context, *models.MemberCoupon) (*models.MemberCoupon, error)
}
type MemberDeviceActivityUseCase interface {
	Store(context.Context, *models.MemberDeviceActivity) (*models.MemberDeviceActivity, error)
	Fetch(ctx context.Context, memberID string) ([]*models.MemberDeviceActivity, error)
	FetchLastLogin(ctx context.Context, memberID string) (*models.MemberDeviceActivity, error)
}

type MemberLastTransactionUseCase interface {
	Fetch(ctx context.Context, memberID string) ([]*models.MemberLastTransaction, error)
}
