package usecase

import (
	"context"
	"strconv"
	"time"

	"github.com/beevik/guid"
	//couponRepository "gitlab.com/robertinc/cardlez-api/business-service/coupon/repository"
	//journalmappingrepository "gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	"gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"

	cCOA "gitlab.com/robertinc/cardlez-api/business-service/coa"
	cCoupon "gitlab.com/robertinc/cardlez-api/business-service/coupon"
	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
)

type MemberCouponUsecase struct {
	MemberCouponRepository member.MemberCouponRepository
	contextTimeout         time.Duration
}

func NewMemberCouponUsecase() member.MemberCouponUsecase {
	return &MemberCouponUsecase{
		MemberCouponRepository: repository.NewSqlMemberCouponRepository(),
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}
func NewMemberCouponUsecaseV2(repo member.MemberCouponRepository) member.MemberCouponUsecase {
	return &MemberCouponUsecase{
		MemberCouponRepository: repo,
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}
func (a *MemberCouponUsecase) Fetch(c context.Context, search string, keyword string) (error, []*models.MemberCoupon) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//promoRepo := couponRepository.NewSqlPromoCouponRepository()
	promoRepo := ctx.Value("CouponRepository").(cCoupon.PromoCouponRepository)
	var PromoCouponList []*models.PromoCoupon
	err, listMemberCoupon := a.MemberCouponRepository.Fetch(ctx, search, keyword)
	if err != nil {
		return err, nil
	}
	for _, promoCoupons := range listMemberCoupon {
		err, listPromoCoupon := promoRepo.Fetch(ctx, "ID", promoCoupons.PromoCouponID)
		if err != nil {
			return err, nil
		}
		for _, promo := range listPromoCoupon {
			listPromo := &models.PromoCoupon{
				ID:          promo.ID,
				Code:        promo.Code,
				PromoImages: promo.PromoImages,
				PromoDesc:   promo.PromoDesc,
				StartDate:   promo.StartDate,
				EndDate:     promo.EndDate,
				LoyaltyID:   promo.LoyaltyID,
				PoinAmount:  promo.PoinAmount,
				PromoAmount: promo.PromoAmount,
			}
			PromoCouponList = append(PromoCouponList, listPromo)
		}
	}

	return nil, listMemberCoupon
}
func (a *MemberCouponUsecase) GetByMemberID(c context.Context, id *string) ([]*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCoupon, err := a.MemberCouponRepository.GetByMemberID(ctx, id)
	if err != nil {
		return nil, err
	}
	return listCoupon, nil
}
func (a *MemberCouponUsecase) Store(c context.Context, b *models.MemberCoupon) (*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//memberrepository := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(member.MemberRepository)
	err, code := memberRepo.GenerateCode(ctx, "MBRPROMO")
	if err != nil {
		return nil, err
	}
	var objCoupon models.MemberCoupon
	objCoupon.Code = code
	objCoupon.MemberID = b.MemberID
	objCoupon.PromoCouponID = b.PromoCouponID
	objCoupon.PromoCode = b.PromoCode
	objCoupon.PromoAmount = b.PromoAmount
	objCoupon.ExpiryDate = b.ExpiryDate
	objCoupon.IsRedeem = false
	objCoupon.RedeemTransRefNo = ""
	t := time.Now()
	objCoupon.ExchangeDate = t.Format("2006-01-02 15:04:05")
	memberCoupon, err := a.MemberCouponRepository.Store(ctx, &objCoupon)
	if err != nil {
		return nil, err
	}
	value, err := strconv.ParseFloat(b.Code, 64)
	if err != nil {
		return nil, err
	}
	pointAmount := float64(value)
	//Insert To Journal
	err = a.InsertJournal(ctx, memberCoupon, b.ID, pointAmount)
	if err != nil {
		return nil, err
	}
	return memberCoupon, nil
}
func (a *MemberCouponUsecase) StoreByDate(c context.Context, b *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//memberrepository := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(member.MemberRepository)
	err, code := memberRepo.GenerateCode(ctx, "MBRPROMO")
	if err != nil {
		return nil, err
	}
	var objCoupon models.MemberCoupon
	objCoupon.Code = code
	objCoupon.MemberID = b.MemberID
	objCoupon.PromoCouponID = b.PromoCouponID
	objCoupon.PromoCode = b.PromoCode
	objCoupon.PromoAmount = b.PromoAmount
	objCoupon.ExpiryDate = b.ExpiryDate
	objCoupon.IsRedeem = false
	objCoupon.RedeemTransRefNo = ""

	objCoupon.ExchangeDate = companyDate
	memberCoupon, err := a.MemberCouponRepository.StoreByDate(ctx, &objCoupon, companyDate)
	if err != nil {
		return nil, err
	}
	value, err := strconv.ParseFloat(b.Code, 64)
	if err != nil {
		return nil, err
	}
	pointAmount := float64(value)
	//Insert To Journal
	err = a.InsertJournalByDate(ctx, memberCoupon, b.ID, pointAmount)
	if err != nil {
		return nil, err
	}
	return memberCoupon, nil
}
func (a *MemberCouponUsecase) InsertJournal(ctx context.Context, b *models.MemberCoupon, accountId string, pointAmount float64) error {
	journalID := guid.New().StringUpper()
	notes := "Tukar point untuk promo : " + b.Code

	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0026")
	if err != nil {
		return err
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.ExchangeDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailID,
		JournalID:       journal.ID,
		COAID:           "B9628053-1CED-4ECF-94E3-CA803323AFD2",
		Debit:           pointAmount,
		Credit:          0,
		Notes:           "Rekening Poin Member",
		ReferenceNumber: accountId,
	}
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "B9628053-1CED-4ECF-94E3-CA803323AFD2",
		Debit:           0,
		Credit:          pointAmount,
		Notes:           "Rekening Poin Merchant",
		ReferenceNumber: "0D6A9A80-35E8-4450-B9C1-48085D2F63E4",
	}
	journalDetailBiayaID := guid.New().StringUpper()
	PromoAmount := pointAmount * 1500
	journalDetailDebitBiaya := models.JournalDetail{
		ID:        journalDetailBiayaID,
		JournalID: journal.ID,
		COAID:     "1DC01E04-8E69-45DF-AF84-74A805AA4924",
		Debit:     PromoAmount,
		Credit:    0,
		Notes:     "Biaya Beban Promo",
		//ReferenceNumber: b.AccountSourceID,
	}
	journalDetailCreditBiayaID := guid.New().StringUpper()
	journalDetailCreditBiaya := models.JournalDetail{
		ID:              journalDetailCreditBiayaID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           0,
		Credit:          PromoAmount,
		Notes:           "Rekening Cardlez Hadiah.me",
		ReferenceNumber: "FCFFEE09-EA3B-4ED1-9F13-58D7A83E8E16",
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitBiaya)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditBiaya)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *MemberCouponUsecase) InsertJournalByDate(ctx context.Context, b *models.MemberCoupon, accountId string, pointAmount float64) error {
	journalID := guid.New().StringUpper()
	notes := "Tukar point untuk promo : " + b.Code

	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0026")
	if err != nil {
		return err
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.ExchangeDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailID,
		JournalID:       journal.ID,
		COAID:           "B9628053-1CED-4ECF-94E3-CA803323AFD2",
		Debit:           pointAmount,
		Credit:          0,
		Notes:           "Rekening Poin Member",
		ReferenceNumber: accountId,
	}
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "B9628053-1CED-4ECF-94E3-CA803323AFD2",
		Debit:           0,
		Credit:          pointAmount,
		Notes:           "Rekening Poin Merchant",
		ReferenceNumber: "0D6A9A80-35E8-4450-B9C1-48085D2F63E4",
	}
	journalDetailBiayaID := guid.New().StringUpper()
	PromoAmount := pointAmount * 1500
	journalDetailDebitBiaya := models.JournalDetail{
		ID:        journalDetailBiayaID,
		JournalID: journal.ID,
		COAID:     "1DC01E04-8E69-45DF-AF84-74A805AA4924",
		Debit:     PromoAmount,
		Credit:    0,
		Notes:     "Biaya Beban Promo",
		//ReferenceNumber: b.AccountSourceID,
	}
	journalDetailCreditBiayaID := guid.New().StringUpper()
	journalDetailCreditBiaya := models.JournalDetail{
		ID:              journalDetailCreditBiayaID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           0,
		Credit:          PromoAmount,
		Notes:           "Rekening Cardlez Hadiah.me",
		ReferenceNumber: "FCFFEE09-EA3B-4ED1-9F13-58D7A83E8E16",
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebitBiaya)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditBiaya)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *MemberCouponUsecase) InsertJournalRedeemCoupon(ctx context.Context, b *models.MemberCoupon, accountId string, pointAmount float64) error {
	journalID := guid.New().StringUpper()
	notes := "Redeem Promo untuk member : " + b.PromoCode

	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0026")
	if err != nil {
		return err
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.ExchangeDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailID := guid.New().StringUpper()
	keyTabungan := "COADebetRedeemCoupon"
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyTabungan)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:        journalDetailID,
		JournalID: journal.ID,
		COAID:     COA.ID,
		Debit:     pointAmount,
		Credit:    0,
		Notes:     "Beban Poin",
	}
	journalDetailCreditID := guid.New().StringUpper()
	keyTabungan2 := "COACreditRedeemCoupon"
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyTabungan2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          pointAmount,
		Notes:           "Rekening Poin Member",
		ReferenceNumber: accountId,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *MemberCouponUsecase) InsertJournalRedeemCouponByDate(ctx context.Context, b *models.MemberCoupon, accountId string, pointAmount float64) error {
	journalID := guid.New().StringUpper()
	notes := "Redeem Promo untuk member : " + b.PromoCode

	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0026")
	if err != nil {
		return err
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.ExchangeDate,
		Description:      service.NewNullString(notes),
		Code:             service.NewNullString(b.Code),
		JournalMappingID: journalmapping.ID,
	}
	journalDetailID := guid.New().StringUpper()
	keyTabungan := "COADebetRedeemCoupon"
	journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyTabungan)
	if err != nil {
		return err
	}
	COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
	if err != nil {
		return err
	}
	journalDetailDebit := models.JournalDetail{
		ID:        journalDetailID,
		JournalID: journal.ID,
		COAID:     COA.ID,
		Debit:     pointAmount,
		Credit:    0,
		Notes:     "Beban Poin",
	}
	journalDetailCreditID := guid.New().StringUpper()
	keyTabungan2 := "COACreditRedeemCoupon"
	journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyTabungan2)
	if err != nil {
		return err
	}
	COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
	if err != nil {
		return err
	}
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           COA2.ID,
		Debit:           0,
		Credit:          pointAmount,
		Notes:           "Rekening Poin Member",
		ReferenceNumber: accountId,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	//JournalUsecase := usecase.NewJournalUsecase()
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.StoreBase(ctx, &journal)
	if err != nil {
		return err
	}

	return nil
}
func (a *MemberCouponUsecase) RedeemCoupon(c context.Context, b *models.MemberCoupon, accountID string, cashBackAmount float64) (*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	//memberrepository := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(member.MemberRepository)
	err, code := memberRepo.GenerateCode(ctx, "MBRPROMO")
	if err != nil {
		return nil, err
	}
	var objCoupon models.MemberCoupon
	objCoupon.Code = code
	objCoupon.MemberID = b.MemberID
	objCoupon.PromoCouponID = b.PromoCouponID
	objCoupon.PromoCode = b.PromoCode
	objCoupon.PromoAmount = b.PromoAmount
	objCoupon.ExpiryDate = b.ExpiryDate
	objCoupon.IsRedeem = false
	objCoupon.RedeemTransRefNo = ""
	t := time.Now()
	objCoupon.ExchangeDate = t.Format("2006-01-02 15:04:05")
	d, err := a.MemberCouponRepository.Store(ctx, &objCoupon)
	if err != nil {
		return nil, err
	}

	//INSERT INTO LOCAL JOURNAL
	err = a.InsertJournalRedeemCoupon(ctx, d, accountID, cashBackAmount)
	if err != nil {
		return nil, err
	}

	//IF SUKSES is redeem true
	d.IsRedeem = true
	memberCoupon, err := a.MemberCouponRepository.Update(ctx, d)
	if err != nil {
		return nil, err
	}

	return memberCoupon, nil
}
func (a *MemberCouponUsecase) RedeemCouponByDate(c context.Context, b *models.MemberCoupon, accountID string, cashBackAmount float64, companyDate string) (*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	//memberrepository := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(member.MemberRepository)
	err, code := memberRepo.GenerateCode(ctx, "MBRPROMO")
	if err != nil {
		return nil, err
	}
	var objCoupon models.MemberCoupon
	objCoupon.Code = code
	objCoupon.MemberID = b.MemberID
	objCoupon.PromoCouponID = b.PromoCouponID
	objCoupon.PromoCode = b.PromoCode
	objCoupon.PromoAmount = b.PromoAmount
	objCoupon.ExpiryDate = b.ExpiryDate
	objCoupon.IsRedeem = false
	objCoupon.RedeemTransRefNo = ""

	objCoupon.ExchangeDate = companyDate
	d, err := a.MemberCouponRepository.StoreByDate(ctx, &objCoupon, companyDate)
	if err != nil {
		return nil, err
	}

	//INSERT INTO LOCAL JOURNAL
	err = a.InsertJournalRedeemCouponByDate(ctx, d, accountID, cashBackAmount)
	if err != nil {
		return nil, err
	}

	//IF SUKSES is redeem true
	d.IsRedeem = true
	memberCoupon, err := a.MemberCouponRepository.UpdateByDate(ctx, d, companyDate)
	if err != nil {
		return nil, err
	}

	return memberCoupon, nil
}
func (a *MemberCouponUsecase) Update(c context.Context, b *models.MemberCoupon) (*models.MemberCoupon, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	memberCoupon, err := a.MemberCouponRepository.Update(ctx, b)
	if err != nil {
		return nil, err
	}
	return memberCoupon, nil
}
