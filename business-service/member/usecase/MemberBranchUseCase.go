package usecase

import (
	"context"
	"time"

	memberbranch "gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberBranchUsecase struct {
	MemberBranchRepository memberbranch.MemberBranchRepository
	contextTimeout         time.Duration
}

func NewMemberBranchUsecase() memberbranch.MemberBranchUsecase {
	return &MemberBranchUsecase{
		MemberBranchRepository: repository.NewSqlMemberBranchRepository(),
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}
func NewMemberBranchUsecaseV2(repo memberbranch.MemberBranchRepository) memberbranch.MemberBranchUsecase {
	return &MemberBranchUsecase{
		MemberBranchRepository: repo,
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}
func (a *MemberBranchUsecase) GetByMemberID(c context.Context, id *string) ([]*models.MemberBranch, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberBranchRepository.GetByMemberID(ctx, id)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}
