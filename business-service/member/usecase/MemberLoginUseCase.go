package usecase

import (
	"context"
	"time"

	memberlogin "gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberLoginUsecase struct {
	MemberLoginRepository memberlogin.MemberLoginRepository
	contextTimeout        time.Duration
}

func NewMemberLoginUsecase() memberlogin.MemberLoginUsecase {
	return &MemberLoginUsecase{
		MemberLoginRepository: repository.NewSqlMemberLoginRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberLoginUsecaseV2(repo memberlogin.MemberLoginRepository) memberlogin.MemberLoginUsecase {
	return &MemberLoginUsecase{
		MemberLoginRepository: repo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}
func (a *MemberLoginUsecase) GetByMemberBranchID(c context.Context, id *string) ([]*models.MemberLogin, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberLoginRepository.GetByMemberBranchID(ctx, id)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}
