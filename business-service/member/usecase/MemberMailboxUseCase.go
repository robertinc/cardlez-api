package usecase

import (
	"context"
	"database/sql"
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/robertinc/cardlez-api/service"

	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	membermailbox "gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberMailboxUsecase struct {
	MemberMailboxRepository membermailbox.MemberMailboxRepository
	MailboxRepository       membermailbox.MailboxRepository
	contextTimeout          time.Duration
}

func NewMemberMailboxUsecase() membermailbox.MemberMailboxRepository {
	return &MemberMailboxUsecase{
		MemberMailboxRepository: repository.NewSqlMemberMailboxRepository(),
		MailboxRepository:       repository.NewSqlMailboxRepository(),
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberMailboxUsecaseV2(memberMailboxRepo membermailbox.MemberMailboxRepository, mailboxRepo membermailbox.MailboxRepository) membermailbox.MemberMailboxRepository {
	return &MemberMailboxUsecase{
		MemberMailboxRepository: memberMailboxRepo,
		MailboxRepository:       mailboxRepo,
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func (a *MemberMailboxUsecase) FetchMailbox(c context.Context) ([]*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	err, listMemberMailbox := a.MailboxRepository.FetchMailbox(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listMemberMailbox
}
func (a *MemberMailboxUsecase) GetMemberMailbox(c context.Context, memberId *string) ([]*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	var myListMailbox []*models.MemberMailbox
	if memberId == nil {
		listMemberMailbox, err := a.MailboxRepository.FetchMailbox(ctx)
		if err != nil {
			return nil, err
		}
		return listMemberMailbox, nil
	}
	listMemberMailbox, err := a.MemberMailboxRepository.GetMemberMailbox(ctx, memberId)
	if err != nil {
		return nil, err
	}
	for _, memberMailbox := range listMemberMailbox {
		mailbox, err := a.MailboxRepository.GetDetailMailbox(ctx, &memberMailbox.MailboxID)
		if err != nil {
			return nil, err
		}
		myListMailbox = append(myListMailbox, &models.MemberMailbox{
			ID:            memberMailbox.ID,
			MemberID:      mailbox.MemberID,
			Code:          mailbox.Code,
			MailboxID:     mailbox.ID,
			MailDate:      memberMailbox.MailDate,
			MailSubject:   mailbox.MailSubject,
			MailDesc:      mailbox.MailDesc,
			MailImages:    mailbox.MailImages,
			PublishStatus: mailbox.PublishStatus,
			MailDirection: mailbox.MailDirection,
			ReadStatus:    memberMailbox.ReadStatus,
		})
	}
	return myListMailbox, nil
}
func (a *MemberMailboxUsecase) Update(c context.Context, b *models.MemberMailbox) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//membermailboxRepo := membermailboxrepository.NewSqlMemberMailboxRepository()
	memberMailbox, err := a.MemberMailboxRepository.UpdateMailbox(ctx, b)

	if err != nil {
		return nil, err
	}
	return memberMailbox, nil
}
func (a *MemberMailboxUsecase) UpdateByDate(c context.Context, b *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//membermailboxRepo := membermailboxrepository.NewSqlMemberMailboxRepository()
	memberMailbox, err := a.MemberMailboxRepository.UpdateByDate(ctx, b, companyDate)

	if err != nil {
		return nil, err
	}
	return memberMailbox, nil
}

func (a *MemberMailboxUsecase) UpdateMailbox(c context.Context, b *models.MemberMailbox) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	mailboxResult, err := a.MailboxRepository.GetDetailMailbox(ctx, &b.MailboxID)
	if err != nil {
		return nil, err
	}
	oldMemberId := mailboxResult.MemberID
	ext := filepath.Ext(b.MailImages)
	if b.MailImages != "" {
		b.MailImages = mailboxResult.Code + ext
	} else {
		b.MailImages = mailboxResult.MailImages
	}
	memberMailbox := &models.Mailbox{
		ID:            b.MailboxID,
		MemberID:      b.MemberID,
		MailDesc:      b.MailDesc,
		MailSubject:   b.MailSubject,
		MailDirection: b.MailDirection,
		MailImages:    b.MailImages,
		PublishStatus: b.PublishStatus,
	}
	_, err = a.MailboxRepository.Update(ctx, memberMailbox)
	if err != nil {
		return nil, err
	}
	if b.MemberID != "" {
		mail, err := a.MemberMailboxRepository.GetByMailboxId(ctx, &oldMemberId, &b.MailboxID)
		if err != nil {
			if err == sql.ErrNoRows {
				return b, nil
			} else {
				return nil, err
			}
		}
		mailModel := &models.MemberMailbox{
			ID:       mail.ID,
			MemberID: b.MemberID,
		}
		_, err = a.MemberMailboxRepository.Update(ctx, mailModel)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}
func (a *MemberMailboxUsecase) Store(c context.Context, b *models.MemberMailbox) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//memberRepo := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	// memberRepo := repository.NewSqlMemberRepository()
	err, mailboxCode := memberRepo.GenerateCode(ctx, "MBX")
	if err != nil {
		return nil, err
	}
	if b.MailImages != "" {
		ext := filepath.Ext(b.MailImages)
		b.MailImages = mailboxCode + ext
	}
	if b.MemberID == "" {
		b.MemberID = service.NewEmptyGuid()
	}
	mailbox, err := a.MailboxRepository.Store(ctx, &models.Mailbox{
		Code:          mailboxCode,
		MemberID:      b.MemberID,
		MailDesc:      b.MailDesc,
		MailSubject:   b.MailSubject,
		MailDirection: b.MailDirection,
		MailImages:    b.MailImages,
		PublishStatus: b.PublishStatus,
	})
	if err != nil {
		return nil, err
	}
	var membermailbox *models.MemberMailbox
	b.MailboxID = mailbox.ID
	b.MailboxCode = mailboxCode
	if b.MailDirection == 2 {
		// blast mail
		err, members := memberRepo.Fetch(ctx, "", 0, 0, 1, 2, nil)
		if err != nil {
			return nil, err
		}
		for _, member := range members {
			b.MemberID = member.ID
			err, memberMailboxCode := memberRepo.GenerateCode(ctx, "MMX")
			if err != nil {
				return nil, err
			}
			b.Code = memberMailboxCode
			membermailbox, err = a.MemberMailboxRepository.Store(ctx, b)
			if err != nil {
				return nil, err
			}
		}
	} else if b.MailDirection == 1 {
		//personal mail
		err, memberMailboxCode := memberRepo.GenerateCode(ctx, "MMX")
		if err != nil {
			return nil, err
		}
		b.Code = memberMailboxCode
		membermailbox, err = a.MemberMailboxRepository.Store(ctx, b)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Mail Direction not valid")
	}
	return membermailbox, nil
}
func (a *MemberMailboxUsecase) StoreByDate(c context.Context, b *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	//memberRepo := repository.NewSqlMemberRepository()
	memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
	// memberRepo := repository.NewSqlMemberRepository()
	err, mailboxCode := memberRepo.GenerateCode(ctx, "MBX")
	if err != nil {
		return nil, err
	}
	if b.MailImages != "" {
		ext := filepath.Ext(b.MailImages)
		b.MailImages = mailboxCode + ext
	}
	if b.MemberID == "" {
		b.MemberID = service.NewEmptyGuid()
	}
	mailbox, err := a.MailboxRepository.StoreByDate(ctx, &models.Mailbox{
		Code:          mailboxCode,
		MemberID:      b.MemberID,
		MailDesc:      b.MailDesc,
		MailSubject:   b.MailSubject,
		MailDirection: b.MailDirection,
		MailImages:    b.MailImages,
		PublishStatus: b.PublishStatus,
	}, companyDate)
	if err != nil {
		return nil, err
	}
	var membermailbox *models.MemberMailbox
	b.MailboxID = mailbox.ID
	b.MailboxCode = mailboxCode
	if b.MailDirection == 2 {
		// blast mail
		err, members := memberRepo.Fetch(ctx, "", 0, 0, 1, 2, nil)
		if err != nil {
			return nil, err
		}
		for _, member := range members {
			b.MemberID = member.ID
			err, memberMailboxCode := memberRepo.GenerateCode(ctx, "MMX")
			if err != nil {
				return nil, err
			}
			b.Code = memberMailboxCode
			membermailbox, err = a.MemberMailboxRepository.StoreByDate(ctx, b, companyDate)
			if err != nil {
				return nil, err
			}
		}
	} else if b.MailDirection == 1 {
		//personal mail
		err, memberMailboxCode := memberRepo.GenerateCode(ctx, "MMX")
		if err != nil {
			return nil, err
		}
		b.Code = memberMailboxCode
		membermailbox, err = a.MemberMailboxRepository.StoreByDate(ctx, b, companyDate)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Mail Direction not valid")
	}
	return membermailbox, nil
}
func (a *MemberMailboxUsecase) BulkDelete(ctx context.Context, ids []string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess, err := a.MemberMailboxRepository.BulkDelete(ctx, ids)
	if err != nil {
		return false, err
	}
	return isSuccess, nil
}
func (a *MemberMailboxUsecase) GetByMailboxId(c context.Context, memberID *string, mailboxId *string) (*models.MemberMailbox, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	mail, err := a.MemberMailboxRepository.GetByMailboxId(ctx, memberID, mailboxId)
	if err != nil {
		return nil, err
	}
	return mail, nil
}
