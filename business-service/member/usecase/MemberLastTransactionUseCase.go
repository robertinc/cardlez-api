package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberLastTransactionUsecase struct {
	MemberLastTransactionRepository member.MemberLastTransactionRepository
	contextTimeout                  time.Duration
}

func NewMemberLastTransactionUsecase() member.MemberLastTransactionRepository {
	return &MemberLastTransactionUsecase{
		MemberLastTransactionRepository: repository.NewSqlMemberLastTransactionRepository(),
		contextTimeout:                  time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberLastTransactionUsecaseV2(repo member.MemberLastTransactionRepository) member.MemberLastTransactionRepository {
	return &MemberLastTransactionUsecase{
		MemberLastTransactionRepository: repo,
		contextTimeout:                  time.Second * time.Duration(models.Timeout()),
	}
}

func (a *MemberLastTransactionUsecase) Fetch(c context.Context, memberID string) ([]*models.MemberLastTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	listMemberLastTransaction, err := a.MemberLastTransactionRepository.Fetch(ctx, memberID)
	if err != nil {
		return nil, err
	}
	return listMemberLastTransaction, nil
}
