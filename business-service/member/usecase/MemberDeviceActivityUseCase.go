package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberDeviceActivityUsecase struct {
	MemberDeviceActivityRepository member.MemberDeviceActivityRepository
	contextTimeout                 time.Duration
}

func NewMemberDeviceActivityUsecase() member.MemberDeviceActivityRepository {
	return &MemberDeviceActivityUsecase{
		MemberDeviceActivityRepository: repository.NewSqlMemberDeviceActivityRepository(),
		contextTimeout:                 time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberDeviceActivityUsecaseV2(repo member.MemberDeviceActivityRepository) member.MemberDeviceActivityRepository {
	return &MemberDeviceActivityUsecase{
		MemberDeviceActivityRepository: repo,
		contextTimeout:                 time.Second * time.Duration(models.Timeout()),
	}
}
func (a *MemberDeviceActivityUsecase) Store(c context.Context, b *models.MemberDeviceActivity) (*models.MemberDeviceActivity, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	MemberDeviceActivity, err := a.MemberDeviceActivityRepository.Store(ctx, b)
	if err != nil {
		return nil, err
	}
	return MemberDeviceActivity, nil
}

func (a *MemberDeviceActivityUsecase) Fetch(c context.Context, memberID string) ([]*models.MemberDeviceActivity, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	listMemberDeviceActivity, err := a.MemberDeviceActivityRepository.Fetch(ctx, memberID)
	if err != nil {
		return nil, err
	}
	return listMemberDeviceActivity, nil
}

func (a *MemberDeviceActivityUsecase) FetchLastLogin(c context.Context, memberID string) (*models.MemberDeviceActivity, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	listMemberDeviceActivity, err := a.MemberDeviceActivityRepository.FetchLastLogin(ctx, memberID)
	if err != nil {
		return nil, err
	}
	return listMemberDeviceActivity, nil
}
