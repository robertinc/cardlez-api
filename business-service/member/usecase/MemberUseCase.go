package usecase

import (
	"context"
	"fmt"
	"net/url"
	"time"

	// accountrepository "gitlab.com/robertinc/cardlez-api/business-service/account/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/business-service/member/repository"

	// productrepository "gitlab.com/robertinc/cardlez-api/business-service/product/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	service "gitlab.com/robertinc/cardlez-api/service"
	"golang.org/x/crypto/bcrypt"

	cAccount "gitlab.com/robertinc/cardlez-api/business-service/account"
	cProduct "gitlab.com/robertinc/cardlez-api/business-service/product"
)

type MemberUsecase struct {
	MemberRepository        member.MemberRepository
	MemberMailboxRepository member.MemberMailboxRepository
	contextTimeout          time.Duration
}

func NewMemberUsecase() member.MemberUsecase {
	return &MemberUsecase{
		MemberRepository:        repository.NewSqlMemberRepository(),
		MemberMailboxRepository: repository.NewSqlMemberMailboxRepository(),
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func NewMemberUsecaseV2(
	memberRepo member.MemberRepository,
	memberMailBoxRepo member.MemberMailboxRepository) member.MemberUsecase {
	return &MemberUsecase{
		MemberRepository:        memberRepo,
		MemberMailboxRepository: memberMailBoxRepo,
		contextTimeout:          time.Second * time.Duration(models.Timeout()),
	}
}

func (a *MemberUsecase) Fetch(c context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32) (error, []*models.Member) {
	// This is for fetch 2
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listMember := a.MemberRepository.Fetch(ctx, order, first, count, memberType, recordStatus, recordStatus2)
	if err != nil {
		return err, nil
	}
	return nil, listMember
}

func (a *MemberUsecase) FetchAll(c context.Context) (error, []*models.Member) {
	// This is for fetch All Member
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listMember := a.MemberRepository.FetchAll(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listMember
}

func (a *MemberUsecase) FetchBase(c context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32, searchViewParam *string) (error, []*models.Member) {
	// This is for fetch 2
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listMember := a.MemberRepository.FetchBase(ctx, order, first, count, memberType, recordStatus, recordStatus2, searchViewParam)
	if err != nil {
		return err, nil
	}
	return nil, listMember
}
func (a *MemberUsecase) Pagination(
	c context.Context,
	order string,
	direction string,
	first int32,
	count int32,
	memberType int32,
	filter string,
	filterValue string,
) (error, []*models.Member) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listMember := a.MemberRepository.Pagination(ctx, order, direction, first, count, memberType, filter, filterValue)
	if err != nil {
		return err, nil
	}
	return nil, listMember
}
func (a *MemberUsecase) Count(
	c context.Context,
	memberType int32,
	filter string,
	filterValue string,
) (error, int32) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, countResult := a.MemberRepository.Count(ctx, memberType, filter, filterValue)
	if err != nil {
		return err, 0
	}
	return nil, countResult
}

func (a *MemberUsecase) GetByID(c context.Context, id *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberRepository.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}

func (a *MemberUsecase) GetByIDBase(c context.Context, id *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberRepository.GetByIDBase(ctx, id)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}

func (a *MemberUsecase) GetByCodeBase(c context.Context, code *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberRepository.GetByCodeBase(ctx, code)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}

func (a *MemberUsecase) GetByHandphone(c context.Context, handphone *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberRepository.GetByHandphone(ctx, handphone)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}

func (a *MemberUsecase) GetByEmail(c context.Context, email *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember, err := a.MemberRepository.GetByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	return listMember, nil
}

func (a *MemberUsecase) CheckPIN(c context.Context, id *string, pin *string) (*bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	valid, _ := a.MemberRepository.CheckPIN(ctx, id, pin)
	if *valid == true {
		a.MemberRepository.DeleteCount(ctx, *id)
		return valid, nil
	}
	count, _ := a.MemberRepository.GetCount(ctx, *id)
	if count >= 2 {
		_, err := a.MemberRepository.LockAccount(ctx, id)
		if err != nil {
			return valid, err
		}
		a.MemberRepository.DeleteCount(ctx, *id)
		a.MemberRepository.DeleteToken(ctx, *id)
		return valid, fmt.Errorf("Anda telah salah input pin sebanyak 3 kali. Akun anda diblok.")
	}
	a.MemberRepository.SetCount(ctx, *id, count+1)
	return valid, nil
}

func (a *MemberUsecase) GetCategory(c context.Context, id *string) *models.Category {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listMember := a.MemberRepository.GetCategory(ctx, id)

	return listMember
}

func (a *MemberUsecase) Store(c context.Context, Member *models.Member) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if Member.Code.String == "" {
		err, code := a.MemberRepository.GenerateCode(ctx, "MBR")
		if err != nil {
			return nil, err
		}
		Member.Code.String = code
	}
	birthDate := service.SubstringDate(Member.BirthDate)
	Member.BirthDate = birthDate
	Member, err := a.MemberRepository.Store(ctx, Member)
	if err != nil {
		return nil, err
	}
	for _, memberBranch := range Member.MemberBranches {
		memberBranch.MemberID = Member.ID
		memberBranch, err := a.MemberRepository.StoreMemberBranch(ctx, &memberBranch)
		if err != nil {
			return nil, err
		}
		if Member.MemberType == 5 || Member.MemberType == 9 {
			err = a.CreateInitialAccount(ctx, memberBranch.CompanyName.String, memberBranch.ID)
			if err != nil {
				return nil, err
			}
		}
		for _, memberLogin := range memberBranch.MemberLogins {
			memberLogin.MemberBranchID = memberBranch.ID
			_, err := a.MemberRepository.StoreMemberLogin(ctx, &memberLogin)
			if err != nil {
				return nil, err
			}
		}
	}
	if Member.MemberType == 1 {
		err = a.CreateInitialAccount(ctx, Member.MemberName.String, Member.ID)
		if err != nil {
			return nil, err
		}
	}
	return Member, nil
}

func (a *MemberUsecase) CreateInitialAccount(ctx context.Context, memberName string, memberid string) error {

	//Create 4 account
	productRepo := ctx.Value("ProductRepository").(cProduct.ProductRepository)
	// productRepo := productrepository.NewSqlProductRepository()
	var productCode = "9026C4E2-3FCB-4E2F-8EF3-6FFF41C0BE45"
	err, products := productRepo.Fetch(ctx, "ID", productCode)
	if err != nil {
		return err
	}
	err, codeAccount := a.MemberRepository.GenerateCode(ctx, products[0].PrefixCode)
	if err != nil {
		return err
	}
	//accountrepository := accountrepository.NewSqlAccountRepository()
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	account1 := &models.Account{
		Code:        codeAccount,
		AccountName: memberName,
		MemberID:    memberid,
		ProductID:   productCode,
	}
	_, err = accountRepo.Store(ctx, account1)
	if err != nil {
		return err
	}

	productCode = "4A08B36F-A36D-4C1C-B77D-668CCB082124"
	err, products = productRepo.Fetch(ctx, "ID", productCode)
	if err != nil {
		return err
	}
	err, codeAccount = a.MemberRepository.GenerateCode(ctx, products[0].PrefixCode)
	if err != nil {
		return err
	}
	account2 := &models.Account{
		Code:        codeAccount,
		AccountName: memberName,
		MemberID:    memberid,
		ProductID:   productCode,
	}
	_, err = accountRepo.Store(ctx, account2)

	productCode = "D459948F-81C1-46C8-8639-64806840B159"
	err, products = productRepo.Fetch(ctx, "ID", productCode)
	if err != nil {
		return err
	}
	err, codeAccount = a.MemberRepository.GenerateCode(ctx, products[0].PrefixCode)
	if err != nil {
		return err
	}
	account3 := &models.Account{
		Code:        codeAccount,
		AccountName: memberName,
		MemberID:    memberid,
		ProductID:   productCode,
	}
	_, err = accountRepo.Store(ctx, account3)
	if err != nil {
		return err
	}

	productCode = "B38812BF-4275-429A-953B-F6D2170B7228"
	err, products = productRepo.Fetch(ctx, "ID", productCode)
	if err != nil {
		return err
	}
	err, codeAccount = a.MemberRepository.GenerateCode(ctx, products[0].PrefixCode)
	if err != nil {
		return err
	}
	account4 := &models.Account{
		Code:        codeAccount,
		AccountName: memberName,
		MemberID:    memberid,
		ProductID:   productCode,
	}
	_, err = accountRepo.Store(ctx, account4)
	if err != nil {
		return err
	}
	return nil
}
func (a *MemberUsecase) Update(c context.Context, b *models.Member) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if b.Code.Valid == false {
		var code string
		err, code := a.MemberRepository.GenerateCode(ctx, "MBR")
		if err != nil {
			return nil, err
		}
		b.Code.String = code
	}
	oldMember, err := a.MemberRepository.GetByID(ctx, &b.ID)
	if err != nil {
		return nil, err
	}
	Member, err := a.MemberRepository.Update(ctx, b)
	if oldMember.RecordStatus == 4 && b.RecordStatus == 2 {
		smsService := service.NewSMSService()
		emailSubject := "Informasi ISP-Mobile"
		message := "Indosurya - Akun anda sudah di un-block. Terimakasih."
		a.MemberRepository.SendEmailVerification(ctx, &b.ID, &oldMember.Email.String, &message, &emailSubject)
		message = url.QueryEscape(message)
		smsService.SendVerificationCode(message, oldMember.Handphone.String)
	}
	//Insert Initial Mailbox
	if Member.RecordStatus == 9 {
		_, err := a.MemberMailboxRepository.Store(ctx, &models.MemberMailbox{
			Code:       "1ST",
			MemberID:   Member.ID,
			MailboxID:  "F0A5233F-E069-4E2C-95BD-5AB0A42405E4",
			ReadStatus: 0,
		})
		if err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}
	return Member, nil
}
func (a *MemberUsecase) UpdateBase(c context.Context, b *models.Member) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if b.Code.Valid == false {
		var code string
		err, code := a.MemberRepository.GenerateCode(ctx, "MBR")
		if err != nil {
			return nil, err
		}
		b.Code.String = code
	}

	Member, err := a.MemberRepository.UpdateBase(ctx, b)

	// oldMember, err := a.MemberRepository.GetByID(ctx, &b.ID)
	// if err != nil {
	// 	return nil, err
	// }
	// if oldMember.RecordStatus == 4 && b.RecordStatus == 2 {
	// 	smsService := service.NewSMSService()
	// 	emailSubject := "Informasi ISP-Mobile"
	// 	message := "Indosurya - Akun anda sudah di un-block. Terimakasih."
	// 	a.MemberRepository.SendEmailVerification(ctx, &oldMember.Email.String, &message, &emailSubject)
	// 	message = url.QueryEscape(message)
	// 	smsService.SendVerificationCode(message, oldMember.Handphone.String)
	// }
	//Insert Initial Mailbox
	if Member.RecordStatus == 9 {
		_, err := a.MemberMailboxRepository.Store(ctx, &models.MemberMailbox{
			Code:       "1ST",
			MemberID:   Member.ID,
			MailboxID:  "F0A5233F-E069-4E2C-95BD-5AB0A42405E4",
			ReadStatus: 0,
		})
		if err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}
	return Member, nil
}
func (a *MemberUsecase) UpdateBaseByDate(c context.Context, b *models.Member, companyDate string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if b.Code.Valid == false {
		var code string
		err, code := a.MemberRepository.GenerateCode(ctx, "MBR")
		if err != nil {
			return nil, err
		}
		b.Code.String = code
	}

	Member, err := a.MemberRepository.UpdateBaseByDate(ctx, b, companyDate)

	// oldMember, err := a.MemberRepository.GetByID(ctx, &b.ID)
	// if err != nil {
	// 	return nil, err
	// }
	// if oldMember.RecordStatus == 4 && b.RecordStatus == 2 {
	// 	smsService := service.NewSMSService()
	// 	emailSubject := "Informasi ISP-Mobile"
	// 	message := "Indosurya - Akun anda sudah di un-block. Terimakasih."
	// 	a.MemberRepository.SendEmailVerification(ctx, &oldMember.Email.String, &message, &emailSubject)
	// 	message = url.QueryEscape(message)
	// 	smsService.SendVerificationCode(message, oldMember.Handphone.String)
	// }
	//Insert Initial Mailbox
	if Member.RecordStatus == 9 {
		_, err := a.MemberMailboxRepository.StoreByDate(ctx, &models.MemberMailbox{
			Code:       "1ST",
			MemberID:   Member.ID,
			MailboxID:  "F0A5233F-E069-4E2C-95BD-5AB0A42405E4",
			ReadStatus: 0,
		}, companyDate)
		if err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}
	return Member, nil
}

func (a *MemberUsecase) LockAccount(ctx context.Context, memberID *string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	isSuccess, err := a.MemberRepository.LockAccount(ctx, memberID)
	if err != nil {
		return false, err
	}
	isSuccess, err = a.MemberRepository.DeleteToken(ctx, *memberID)
	if err != nil {
		return false, err
	}
	member, err := a.MemberRepository.GetByID(ctx, memberID)
	if err != nil {
		return false, err
	}
	smsService := service.NewSMSService()
	emailSubject := "Informasi ISP-Mobile"
	message := "Indosurya - Akun anda diblok. Silahkan hubungi Customer Service"
	a.MemberRepository.SendEmailVerification(ctx, &member.ID, &member.Email.String, &message, &emailSubject)
	message = url.QueryEscape(message)
	smsService.SendVerificationCode(message, member.Handphone.String)

	return isSuccess, nil
}

func (a *MemberUsecase) SendVerification(ctx context.Context, phone *string, email *string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	verificationCode := a.MemberRepository.GenerateVerificationCode()
	smsMessage := `Indosurya - Kode aktiv4si anda adalah %v (berlaku selama 1 x 24 jam). PENTING: Demi keamanan akun anda, mohon tidak menyebarkan kode ini dan balas pesan ini.`

	verificationEmailCode := a.MemberRepository.GenerateVerificationCode()
	message := "Indosurya - Kode aktiv4si anda adalah " + verificationEmailCode + " (berlaku selama 1 x 24 jam). PENTING: Demi keamanan akun anda, mohon tidak menyebarkan kode ini dan balas pesan ini."
	isSuccess := false
	var err error

	member, err := a.MemberRepository.GetByEmail(ctx, email)
	if err != nil {
		return false, err
	}

	if phone != nil {
		_, err := a.MemberRepository.UpdateVerificationCode(ctx, *phone, verificationCode)
		if err != nil {
			return false, err
		}
		isSuccess, err = a.MemberRepository.SendVerification(ctx, &member.ID, phone, &verificationCode, &smsMessage)
		if err != nil {
			return false, err
		}
	}
	if email != nil {
		emailSubject := "Kode Verifikasi ISP-Mobile"
		_, err := a.MemberRepository.UpdateVerificationCode(ctx, *email, verificationEmailCode)
		if err != nil {
			return false, err
		}
		isSuccess, err = a.MemberRepository.SendEmailVerification(ctx, &member.ID, email, &message, &emailSubject)
		if err != nil {
			return false, err
		}

	}

	return isSuccess, err
}

func (a *MemberUsecase) SendMessage(ctx context.Context, phone *string, message *string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess := false
	var err error

	member, err := a.MemberRepository.GetByHandphone(ctx, phone)
	if err != nil {
		return false, err
	}

	if phone != nil {
		isSuccess, err = a.MemberRepository.SendMessage(ctx, &member.ID, phone, message)
		if err != nil {
			return false, err
		}
	}
	return isSuccess, err
}

func (a *MemberUsecase) ChangePassword(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess := false
	member, err := a.MemberRepository.GetByID(ctx, &memberID)
	if err != nil {
		return isSuccess, err
	}
	if member.LoginPass != "" {
		if oldPassword == nil {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		err := bcrypt.CompareHashAndPassword([]byte(member.LoginPass), []byte(*oldPassword))
		if err != nil {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		isSuccess, err = a.MemberRepository.ChangePassword(ctx, &memberID, &newPassword, isactivation)
		if isSuccess == false {
			return false, err
		}
	} else {
		isSuccess, err = a.MemberRepository.ChangePassword(ctx, &memberID, &newPassword, isactivation)
		if isSuccess == false {
			return false, err
		}
	}

	return isSuccess, err
}
func (a *MemberUsecase) ChangePasswordBase(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool, isnewmember *bool) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess := false
	member, err := a.MemberRepository.GetByID(ctx, &memberID)
	if err != nil {
		return isSuccess, err
	}
	if member.LoginPass != "" && oldPassword != nil {
		if *oldPassword == "" {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		err := bcrypt.CompareHashAndPassword([]byte(member.LoginPass), []byte(*oldPassword))
		if err != nil {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		isSuccess, err = a.MemberRepository.ChangePasswordBase(ctx, &memberID, &newPassword, isactivation, isnewmember)
		if isSuccess == false {
			return false, err
		}
	} else {
		isSuccess, err = a.MemberRepository.ChangePasswordBase(ctx, &memberID, &newPassword, isactivation, isnewmember)
		if isSuccess == false {
			return false, err
		}
	}

	return isSuccess, err
}
func (a *MemberUsecase) ChangePasswordBaseByDate(ctx context.Context, memberID string, oldPassword *string, newPassword string, isactivation *bool, isnewmember *bool, companyDate string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess := false
	member, err := a.MemberRepository.GetByID(ctx, &memberID)
	if err != nil {
		return isSuccess, err
	}
	if member.LoginPass != "" && oldPassword != nil {
		if *oldPassword == "" {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		err := bcrypt.CompareHashAndPassword([]byte(member.LoginPass), []byte(*oldPassword))
		if err != nil {
			return false, fmt.Errorf("Password Lama Salah.")
		}
		isSuccess, err = a.MemberRepository.ChangePasswordBaseByDate(ctx, &memberID, &newPassword, isactivation, isnewmember, companyDate)
		if isSuccess == false {
			return false, err
		}
	} else {
		isSuccess, err = a.MemberRepository.ChangePasswordBaseByDate(ctx, &memberID, &newPassword, isactivation, isnewmember, companyDate)
		if isSuccess == false {
			return false, err
		}
	}

	return isSuccess, err
}
func (a *MemberUsecase) UpdateIMEI(ctx context.Context, imei string, memberID string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	isSuccess, err := a.MemberRepository.UpdateIMEI(ctx, &imei, memberID)
	if err != nil {
		return isSuccess, err
	}
	return isSuccess, nil
}
func (a *MemberUsecase) GetByIMEI(c context.Context, imei *string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	member, err := a.MemberRepository.GetByIMEI(ctx, imei)
	if err != nil {
		return nil, err
	}
	return member, nil
}

func (a *MemberUsecase) GetMemberDetailByMemberID(c context.Context, memberID *string) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.GetMemberDetailByMemberID(ctx, memberID)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}

func (a *MemberUsecase) StoreMemberDetail(c context.Context, MemberDetail *models.MemberDetail) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.StoreMemberDetail(ctx, MemberDetail)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}
func (a *MemberUsecase) StoreMemberDetailByDate(c context.Context, MemberDetail *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.StoreMemberDetailByDate(ctx, MemberDetail, companyDate)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}
func (a *MemberUsecase) StoreMemberDetailFromUploadByDate(c context.Context, MemberDetail *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.StoreMemberDetailFromUploadByDate(ctx, MemberDetail, companyDate)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}

func (a *MemberUsecase) UpdateMemberDetail(c context.Context, MemberDetail *models.MemberDetail) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.UpdateMemberDetail(ctx, MemberDetail)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}
func (a *MemberUsecase) UpdateMemberDetailByDate(c context.Context, MemberDetail *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	memberDetail, err := a.MemberRepository.UpdateMemberDetailByDate(ctx, MemberDetail, companyDate)
	if err != nil {
		return nil, err
	}

	return memberDetail, nil
}

func (a *MemberUsecase) FetchMemberKYC(c context.Context) (error, []*models.MemberKYC) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listMemberKYC := a.MemberRepository.FetchMemberKYC(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listMemberKYC
}

func (a *MemberUsecase) RegularSendSMSSuccessConsoleWA(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	isSuccess, err := a.MemberRepository.RegularSendSMSSuccessConsoleWA(ctx, memberId, phone, smsMessage)
	if err != nil {
		return isSuccess, err
	}
	return isSuccess, nil
}
