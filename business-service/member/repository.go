package member

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type MemberRepository interface {
	Fetch(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32) (error, []*models.Member)
	FetchAll(ctx context.Context) (error, []*models.Member)
	FetchBase(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32, searchViewParam *string) (error, []*models.Member)
	Pagination(ctx context.Context, order string, direction string, first int32, count int32, memberType int32, filter string, filterValue string) (error, []*models.Member)
	Count(ctx context.Context, memberType int32, filter string, filterValue string) (error, int32)
	GenerateCode(ctx context.Context, prefix string) (error, string)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	GetByID(ctx context.Context, id *string) (*models.Member, error)
	GetByIDBase(ctx context.Context, id *string) (*models.Member, error)
	GetByCodeBase(ctx context.Context, code *string) (*models.Member, error)
	GetByHandphone(ctx context.Context, handphone *string) (*models.Member, error)
	GetByHandphoneBase(ctx context.Context, handphone *string) (*models.Member, error)
	GetByEmail(ctx context.Context, email *string) (*models.Member, error)
	GetByCode(ctx context.Context, code *string) (*models.Member, error)
	GetMemberDetailByMemberID(ctx context.Context, memberID *string) (*models.MemberDetail, error)
	CheckPIN(ctx context.Context, id *string, pin *string) (*bool, error)
	GetCategory(ctx context.Context, id *string) *models.Category
	Update(ctx context.Context, member *models.Member) (*models.Member, error)
	UpdateBase(ctx context.Context, member *models.Member) (*models.Member, error)
	UpdateBaseByDate(ctx context.Context, member *models.Member, companyDate string) (*models.Member, error)
	UpdateMemberDetail(ctx context.Context, a *models.MemberDetail) (*models.MemberDetail, error)
	UpdateMemberDetailByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Member) (*models.Member, error)
	StoreBase(ctx context.Context, a *models.Member) (*models.Member, error)
	StoreBaseByDate(ctx context.Context, a *models.Member, companyDate string) (*models.Member, error)
	StoreMemberBranch(ctx context.Context, a *models.MemberBranch) (*models.MemberBranch, error)
	StoreMemberLogin(ctx context.Context, a *models.MemberLogin) (*models.MemberLogin, error)
	StoreMemberDetail(ctx context.Context, a *models.MemberDetail) (*models.MemberDetail, error)
	StoreMemberDetailByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error)
	StoreMemberDetailFromUploadByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error)

	UpdateVerificationCode(ctx context.Context, key string, verificationCode string) (bool, error)
	LockAccount(ctx context.Context, memberID *string) (bool, error)
	SendVerification(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error)
	SendVerificationConsole(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error)
	SendMessage(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	SendMessageConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	RegularSendVerification(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error)
	RegularSendVerificationConsole(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error)
	RegularSendVerificationConsoleWA(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error)
	ChangePassword(ctx context.Context, password *string, memberID *string, isactivation *bool) (bool, error)
	ChangePasswordBase(ctx context.Context, password *string, memberID *string, isactivation *bool, isnewmember *bool) (bool, error)
	ChangePasswordBaseByDate(ctx context.Context, password *string, memberID *string, isactivation *bool, isnewmember *bool, companyDate string) (bool, error)
	GenerateVerificationCode() string
	CheckEmailAndPhone(ctx context.Context, member models.Member) (bool, error)
	CheckIdentityNumberRegistered(ctx context.Context, member models.Member) (bool, error)
	CheckIdentityNumberDataRegistered(ctx context.Context, member models.Member) (bool, error)
	CheckEmailPeruri(ctx context.Context, member models.Member) (bool, error)
	SendEmailVerification(ctx context.Context, memberId *string, email *string, emailMessage *string, emailSubject *string) (bool, error)
	SendEmailVerificationWithCC(ctx context.Context, memberId *string, email *string, emailMessage *string, emailSubject *string, cc *string, bcc *string) (bool, error)
	GetVerificationCode(ctx context.Context, key string, verificationCode string, tokenType string) (bool, error)
	CheckEmail(ctx context.Context, email *string) (bool, error)
	UpdateForForgotPassword(ctx context.Context, password string, email *string) (bool, error)
	DeleteCount(ctx context.Context, key string) (bool, error)
	SetCount(ctx context.Context, key string, count int) (bool, error)
	GetCount(ctx context.Context, key string) (int, error)
	DeleteToken(ctx context.Context, key string) (bool, error)
	SendSMSSuccess(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	SendSMSSuccessConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	RegularSendSMSSuccess(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	RegularSendSMSSuccessConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	RegularSendSMSSuccessConsoleWA(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error)
	SendSMSNexmo(ctx context.Context, memberId *string, phone *string, smsMessage *string, apikey *string, apisecret *string) (bool, error)
	UpdateIMEI(ctx context.Context, imei *string, id string) (bool, error)
	GetByIMEI(ctx context.Context, imei *string) (*models.Member, error)
	GetByPhone(ctx context.Context, handphone *string) (*models.Member, error)
	FetchMemberKYC(ctx context.Context) (error, []*models.MemberKYC)
	UpdateForSignedDocument(ctx context.Context, fileName *string, id *string) (bool, error)
}

type MemberBranchRepository interface {
	GetByMemberID(ctx context.Context, id *string) ([]*models.MemberBranch, error)
}

type MemberLoginRepository interface {
	GetByMemberBranchID(ctx context.Context, id *string) ([]*models.MemberLogin, error)
}

type MemberMailboxRepository interface {
	GetMemberMailbox(ctx context.Context, memberID *string) ([]*models.MemberMailbox, error)
	Store(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error)
	StoreByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error)
	UpdateMailbox(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error)
	Update(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error)
	UpdateByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error)
	BulkDelete(ctx context.Context, ids []string) (bool, error)
	GetByMailboxId(ctx context.Context, memberID *string, mailboxId *string) (*models.MemberMailbox, error)
}
type MailboxRepository interface {
	GetDetailMailbox(ctx context.Context, mailboxID *string) (*models.Mailbox, error)
	FetchMailbox(c context.Context) ([]*models.MemberMailbox, error)
	Store(ctx context.Context, a *models.Mailbox) (*models.Mailbox, error)
	StoreByDate(ctx context.Context, a *models.Mailbox, companyDate string) (*models.Mailbox, error)
	Update(ctx context.Context, a *models.Mailbox) (*models.Mailbox, error)
}
type MemberCouponRepository interface {
	Fetch(ctx context.Context, search string, keyword string) (error, []*models.MemberCoupon)
	GetByMemberID(ctx context.Context, id *string) ([]*models.MemberCoupon, error)
	Store(ctx context.Context, a *models.MemberCoupon) (*models.MemberCoupon, error)
	StoreByDate(ctx context.Context, a *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error)
	Update(ctx context.Context, a *models.MemberCoupon) (*models.MemberCoupon, error)
	UpdateByDate(ctx context.Context, a *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error)
}
type MemberDeviceActivityRepository interface {
	Store(context.Context, *models.MemberDeviceActivity) (*models.MemberDeviceActivity, error)
	Fetch(ctx context.Context, memberID string) ([]*models.MemberDeviceActivity, error)
	FetchLastLogin(ctx context.Context, memberID string) (*models.MemberDeviceActivity, error)
}
type MemberLastTransactionRepository interface {
	Fetch(ctx context.Context, memberID string) ([]*models.MemberLastTransaction, error)
}
