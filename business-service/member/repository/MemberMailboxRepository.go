package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	membermailbox "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlMemberMailboxRepository struct {
	Conn *sql.DB
}

func NewSqlMemberMailboxRepository() membermailbox.MemberMailboxRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberMailboxRepository{conn}
}
func NewSqlMemberMailboxRepositoryV2(Conn *sql.DB) membermailbox.MemberMailboxRepository {
	conn := Conn
	return &sqlMemberMailboxRepository{conn}
}
func (sj *sqlMemberMailboxRepository) GetMemberMailbox(ctx context.Context, memberID *string) ([]*models.MemberMailbox, error) {

	result := make([]*models.MemberMailbox, 0)
	query := `
	select 
		cast(a.ID as varchar(36)) ID,
		cast(a.MemberID as varchar(36)) MemberID,
		cast(a.MailboxID as varchar(36)) MailboxID,
		Isnull(a.Code,'') as Code,
		Isnull(a.ReadStatus,0) as ReadStatus,
		m.Code as MailboxCode,
		a.DateInsert
	from 
		dbo.MemberMailbox a
		Inner Join Mailbox m on a.MailboxID = m.ID
	where 
		a.MemberID = @p0 and m.PublishStatus <> 0
	Order By a.DateInsert desc`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("memberId", fmt.Sprintf("%v", memberID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", memberID))

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		MemberMailbox := new(models.MemberMailbox)
		err = rows.Scan(
			&MemberMailbox.ID,
			&MemberMailbox.MemberID,
			&MemberMailbox.MailboxID,
			&MemberMailbox.Code,
			&MemberMailbox.ReadStatus,
			&MemberMailbox.MailboxCode,
			&MemberMailbox.MailDate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, MemberMailbox)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlMemberMailboxRepository) Store(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error) {

	a.ID = guid.New().StringUpper()
	bankQuery := `
		INSERT INTO MemberMailbox (
			id, 
			code, 
			memberid,
			mailboxid,
			readstatus,
			userinsert,
			dateinsert,
			userupdate,
			dateupdate
		) VALUES (
			@p0, 
			@p1, 
			@p2,
			@p3, 
			@p4, 
			@p5,
			GETDATE(), 
			@p6,
			GETDATE()
		)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberID),
		sql.Named("p3", a.MailboxID),
		sql.Named("p4", a.ReadStatus),
		sql.Named("p5", "Admin"),
		sql.Named("p6", "Admin"),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberMailboxRepository) StoreByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error) {

	a.ID = guid.New().StringUpper()
	bankQuery := `
		INSERT INTO MemberMailbox (
			id, 
			code, 
			memberid,
			mailboxid,
			readstatus,
			userinsert,
			dateinsert,
			userupdate,
			dateupdate
		) VALUES (
			@p0, 
			@p1, 
			@p2,
			@p3, 
			@p4, 
			@p5,
			@date, 
			@p6,
			@date
		)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberID),
		sql.Named("p3", a.MailboxID),
		sql.Named("p4", a.ReadStatus),
		sql.Named("p5", "Admin"),
		sql.Named("p6", "Admin"),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberMailboxRepository) UpdateMailbox(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error) {

	updateQuery := `
		Update MemberMailbox
		Set ReadStatus = 1
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberMailboxRepository) Update(ctx context.Context, a *models.MemberMailbox) (*models.MemberMailbox, error) {
	updateQuery := `
		Update MemberMailbox
		Set MemberID = @p1,
		DateUpdate = GETDATE()
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberMailboxRepository) UpdateByDate(ctx context.Context, a *models.MemberMailbox, companyDate string) (*models.MemberMailbox, error) {
	updateQuery := `
		Update MemberMailbox
		Set MemberID = @p1,
		DateUpdate = @date
		Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberMailboxRepository) BulkDelete(ctx context.Context, ids []string) (bool, error) {

	updateQuery := `
		Delete from MemberMailbox
		Where ID = @id`
	txn, err := sj.Conn.Begin()
	if err != nil {
		return false, err
	}

	stmt, err := txn.Prepare(updateQuery)
	if err != nil {
		log.Fatal(err.Error())
	}

	for i := 0; i < len(ids); i++ {
		_, err = stmt.Exec(
			sql.Named("id", ids[i]),
		)
		if err != nil {
			return false, err
		}
	}

	err = stmt.Close()
	if err != nil {
		return false, err
	}

	err = txn.Commit()
	if err != nil {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberMailboxRepository) GetByMailboxId(ctx context.Context, memberID *string, mailboxId *string) (*models.MemberMailbox, error) {
	var a models.MemberMailbox
	query := `
	select 
		cast(ID as varchar(36)) ID,
		cast(MemberID as varchar(36)) MemberID,
		cast(MailboxID as varchar(36)) MailboxID,
		Isnull(Code,'') as Code,
		Isnull(ReadStatus,0) as ReadStatus,
		DateInsert
	from 
		dbo.MemberMailbox
	where 
		MemberID = @p0 and MailboxID=@p1`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
		zap.String("mailboxId", fmt.Sprintf("%v", mailboxId)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", memberID),
		sql.Named("p1", mailboxId)).Scan(
		&a.ID,
		&a.MemberID,
		&a.MailboxID,
		&a.Code,
		&a.ReadStatus,
		&a.MailDate,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return &a, nil
}
