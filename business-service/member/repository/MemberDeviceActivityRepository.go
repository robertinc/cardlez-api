package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	"go.uber.org/zap"

	mda "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlMemberDeviceActivityRepository struct {
	Conn *sql.DB
}

func NewSqlMemberDeviceActivityRepository() mda.MemberDeviceActivityRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberDeviceActivityRepository{conn}
}
func NewSqlMemberDeviceActivityRepositoryV2(Conn *sql.DB) mda.MemberDeviceActivityRepository {
	conn := Conn
	return &sqlMemberDeviceActivityRepository{conn}
}
func (sj *sqlMemberDeviceActivityRepository) Store(ctx context.Context, a *models.MemberDeviceActivity) (*models.MemberDeviceActivity, error) {
	a.ID = guid.New().StringUpper()
	query := `
	Insert INTO MemberDeviceActivity (
		ID, 
		MemberID, 
		ActivityType, 
		ActivityDate, 
		ActivityRemarks, 
		UserInsert, 
		DateInsert,
		Imei
	) VALUES (
		@id,
		@memberid,
		@activityType,
		@activityDate,
		@activityRemarks,
		@userInsert,
		GetDate(),
		@imei
	)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("id", a.ID),
		sql.Named("memberid", a.MemberID),
		sql.Named("activityType", a.ActivityType),
		sql.Named("activityDate", a.ActivityDate),
		sql.Named("activityRemarks", a.ActivityRemarks),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("imei", a.Imei),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMemberDeviceActivityRepository) Fetch(ctx context.Context, memberID string) ([]*models.MemberDeviceActivity, error) {
	//var resultCoupon models.MemberPromoResult
	query := `
	SELECT TOP 50
		Cast(ID as varchar(36)) as ID,
		ActivityType,
		ActivityDate,
		ActivityRemarks
	FROM MemberDeviceActivity
	Where MemberID = @id
	Order By ActivityDate Desc
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("MemberID", fmt.Sprintf("%v", memberID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("id", memberID),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	//resultCoupon.MemberCouponx = make([]*models.MemberCoupon, 0)
	result := make([]*models.MemberDeviceActivity, 0)
	for rows.Next() {
		j := new(models.MemberDeviceActivity)
		err = rows.Scan(
			&j.ID,
			&j.ActivityType,
			&j.ActivityDate,
			&j.ActivityRemarks,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	//resultCoupon.MemberCouponx = result

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlMemberDeviceActivityRepository) FetchLastLogin(ctx context.Context, memberID string) (*models.MemberDeviceActivity, error) {
	//var resultCoupon models.MemberPromoResult
	var result models.MemberDeviceActivity
	query := `
	SELECT TOP 1
		Cast(ID as varchar(36)) as ID,
		ActivityType,
		ActivityDate,
		ActivityRemarks
	FROM MemberDeviceActivity
	Where MemberID = @id
	Order By ActivityDate Desc
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("MemberID", fmt.Sprintf("%v", memberID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("id", memberID),
	).Scan(
		&result.ID,
		&result.ActivityType,
		&result.ActivityDate,
		&result.ActivityRemarks,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	//
	return &result, nil
}
