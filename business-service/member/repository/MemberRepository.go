package repository

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/url"
	"regexp"
	"strings"
	"time"

	redis "github.com/go-redis/redis/v8"
	"golang.org/x/crypto/bcrypt"

	"go.uber.org/zap"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlMemberRepository struct {
	Conn   *sql.DB
	Redis  *redis.Client
	Redis2 *redis.Client
	Redis0 *redis.Client
}

func NewSqlMemberRepository() member.MemberRepository {
	//conn := service.GetConnectionDB()
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	redis := service.BuildConnRedis(1)
	redis2 := service.BuildConnRedis(2)
	redis0 := service.BuildConnRedis(0)
	return &sqlMemberRepository{conn, redis, redis2, redis0}
}

func NewSqlMemberRepositoryV2(Conn *sql.DB) member.MemberRepository {
	redis := service.BuildConnRedis(1)
	redis2 := service.BuildConnRedis(2)
	redis0 := service.BuildConnRedis(0)
	conn := Conn
	return &sqlMemberRepository{conn, redis, redis2, redis0}
}

func (sj *sqlMemberRepository) Fetch(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32) (error, []*models.Member) {

	whereQuery := ""
	if memberType != -1 {
		whereQuery = `WHERE MemberType = @p3`
		if recordStatus2 != nil {
			whereQuery += ` AND (RecordStatus = @p4 or RecordStatus = @p5)`
		} else {
			if recordStatus != 0 {
				//if 2 show 2,3
				if recordStatus == 2 {
					whereQuery += ` AND RecordStatus in (2,3)`
				} else {
					whereQuery += ` AND RecordStatus = @p4`
				}
			}
		}
	} else {
		if recordStatus2 != nil {
			whereQuery += ` AND (RecordStatus = @p4 or RecordStatus = @p5)`
		} else {
			if recordStatus != 0 {
				//if 2 show 2,3
				if recordStatus == 2 {
					whereQuery += ` AND RecordStatus in (2,3)`
				} else {
					whereQuery += ` AND RecordStatus = @p4`
				}
			}
		}
	}
	result := make([]*models.Member, 0)
	query := `
		SELECT 
			Cast(m.ID as varchar(36)) as ID,
			Isnull(Code,'') as Code,
			Isnull(MemberName,'') as MemberName,
			Isnull(EmailAddress,'') as EmailAddress,
			Isnull(LoginPIN,'') as LoginPIN,
			Isnull(Phone, '') as Phone,
			Isnull(HandPhone, '') as Handphone,
			Isnull(MemberType, 0) as MemberType,
			Isnull(Gender, '') as Gender,
			Isnull(Nationality, '') as Nationality,
			Isnull(MaritalStatus, '') as MaritalStatus,
			Isnull(VirtualAccountNo, '') as VirtualAccountNo,
			Isnull(RecordStatus, 0) as RecordStatus,
			cast(BankID as varchar(36)) as BankID,
			IsNULL(cast(CategoryID as varchar(36)), '') as CategoryID,
			ISNULL(c.Name, '') as CategoryName,
			ISNULL(m.ProfileImage, '') as ProfileImage,
			ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
			ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
			ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
			ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
			ISNULL(CONVERT(VARCHAR(10), m.DateAuthor, 103) + ' '  + convert(VARCHAR(8), m.DateAuthor, 14) ,'') as DateAuthor,
			ISNULL(Cast(m.UserAuthor as varchar(36)), '') as UserAuthor,
			ISNULL(Cast(m.Imei as varchar(100)), '') as Imei,
			ISNULL(m.IdentityNumber, '') as IdentityNumber,
			ISNULL(m.IdentityUpload, '') as IdentityUpload,
			ISNULL(m.NPWPNumber, '') as NPWPNumber,
			ISNULL(m.NPWPUpload, '') as NPWPUpload,
			ISNULL(CONVERT(VARCHAR(10), m.BirthDate, 103) ,'') as BirthDate,
			ISNULL(m.BirthPlace, '') as BirthPlace,
			ISNULL(m.MotherMaiden, '') as MotherMaiden,
			ISNULL(m.AlamatRumah, '') as AlamatRumah
		FROM dbo.Member m
			LEFT JOIN dbo.Category c on m.CategoryID = c.ID
			` + whereQuery + `
		ORDER BY 
			Case When @p2 = 'ID' then Cast(m.ID as varchar(36))
				When @p2 = 'Code' then Code
				When @p2 = 'MemberName' then MemberName End`
	// OFFSET @p0 ROWS FETCH NEXT @p1 ROWS ONLY;`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("first", fmt.Sprintf("%v", first)),
		zap.String("count", fmt.Sprintf("%v", count)),
		zap.String("memberType", fmt.Sprintf("%v", memberType)),
		zap.String("recordStatus", fmt.Sprintf("%v", recordStatus)),
		zap.String("recordStatus2", fmt.Sprintf("%v", recordStatus2)),
	)
	rows, err := sj.Conn.Query(query,
		// sql.Named("p0", first),
		// sql.Named("p1", count),
		sql.Named("p2", order),
		sql.Named("p3", memberType),
		sql.Named("p4", recordStatus),
		sql.Named("p5", recordStatus2),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Member)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberName,
			&j.Email,
			&j.LoginPIN,
			&j.Phone,
			&j.Handphone,
			&j.MemberType,
			&j.Gender,
			&j.Nationality,
			&j.MaritalStatus,
			&j.VirtualAccountNo,
			&j.RecordStatus,
			&j.BankID,
			&j.Category.ID,
			&j.Category.Name,
			&j.ProfileImage,
			&j.DateInsert,
			&j.DateUpdate,
			&j.UserInsert,
			&j.UserUpdate,
			&j.DateAuthor,
			&j.UserAuthor,
			&j.Imei,
			&j.IdentityNumber,
			&j.IdentityUpload,
			&j.NoNPWP,
			&j.NPWPUpload,
			&j.BirthDate,
			&j.BirthPlace,
			&j.MotherMaiden,
			&j.Address,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	//
	return nil, result
}

func (sj *sqlMemberRepository) FetchAll(ctx context.Context) (error, []*models.Member) {

	result := make([]*models.Member, 0)
	query := `
		SELECT 
			Cast(m.ID as varchar(36)) as ID,
			Isnull(Code,'') as Code,
			Isnull(MemberName,'') as MemberName,
			Isnull(EmailAddress,'') as EmailAddress,
			Isnull(LoginPIN,'') as LoginPIN,
			Isnull(Phone, '') as Phone,
			Isnull(HandPhone, '') as Handphone,
			Isnull(MemberType, 0) as MemberType,
			Isnull(Gender, '') as Gender,
			Isnull(Nationality, '') as Nationality,
			Isnull(MaritalStatus, '') as MaritalStatus,
			Isnull(VirtualAccountNo, '') as VirtualAccountNo,
			Isnull(RecordStatus, 0) as RecordStatus,
			cast(BankID as varchar(36)) as BankID,
			IsNULL(cast(CategoryID as varchar(36)), '') as CategoryID,
			ISNULL(c.Name, '') as CategoryName,
			ISNULL(m.ProfileImage, '') as ProfileImage,
			ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
			ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
			ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
			ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
			ISNULL(CONVERT(VARCHAR(10), m.DateAuthor, 103) + ' '  + convert(VARCHAR(8), m.DateAuthor, 14) ,'') as DateAuthor,
			ISNULL(Cast(m.UserAuthor as varchar(36)), '') as UserAuthor,
			ISNULL(Cast(m.Imei as varchar(100)), '') as Imei,
			ISNULL(m.IdentityNumber, '') as IdentityNumber,
			ISNULL(m.IdentityUpload, '') as IdentityUpload,
			ISNULL(m.NPWPNumber, '') as NPWPNumber,
			ISNULL(m.NPWPUpload, '') as NPWPUpload,
			ISNULL(CONVERT(VARCHAR(10), m.BirthDate, 103) + ' '  + convert(VARCHAR(8), m.BirthDate, 14) ,'') as BirthDate,
			ISNULL(m.BirthPlace, '') as BirthPlace,
			ISNULL(m.MotherMaiden, '') as MotherMaiden,
			ISNULL(m.AlamatRumah, '') as AlamatRumah
		FROM dbo.Member m
			LEFT JOIN dbo.Category c on m.CategoryID = c.ID
		ORDER BY MemberName`
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Member)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberName,
			&j.Email,
			&j.LoginPIN,
			&j.Phone,
			&j.Handphone,
			&j.MemberType,
			&j.Gender,
			&j.Nationality,
			&j.MaritalStatus,
			&j.VirtualAccountNo,
			&j.RecordStatus,
			&j.BankID,
			&j.Category.ID,
			&j.Category.Name,
			&j.ProfileImage,
			&j.DateInsert,
			&j.DateUpdate,
			&j.UserInsert,
			&j.UserUpdate,
			&j.DateAuthor,
			&j.UserAuthor,
			&j.Imei,
			&j.IdentityNumber,
			&j.IdentityUpload,
			&j.NoNPWP,
			&j.NPWPUpload,
			&j.BirthDate,
			&j.BirthPlace,
			&j.MotherMaiden,
			&j.Address,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	//
	return nil, result
}

func (sj *sqlMemberRepository) FetchBase(ctx context.Context, order string, first int32, count int32, memberType int32, recordStatus int32, recordStatus2 *int32, searchViewParam *string) (error, []*models.Member) {

	whereQuery := ""
	if memberType != -1 {
		whereQuery = `WHERE MemberType = @p3`
		if recordStatus2 != nil {
			//if 8 show 8,10
			if recordStatus == 8 {
				whereQuery += ` AND (RecordStatus in (8,10) or RecordStatus = @p5)`
			} else {
				whereQuery += ` AND (RecordStatus = @p4 or RecordStatus = @p5)`
			}
		} else {
			if recordStatus != 0 {
				//if 2 show 1,2,3,4,5,9,11,12
				if recordStatus == 2 {
					whereQuery += ` AND RecordStatus in (1,2,3,4,5,9,11,12)`
				} else {
					whereQuery += ` AND RecordStatus = @p4`
				}
			}
		}
		if searchViewParam != nil && *searchViewParam != "" {
			whereQuery += ` AND (MemberName like @p6 or Handphone like @p6)`
		}
	} else {
		if recordStatus2 != nil {
			//if 8 show 8,10
			if recordStatus == 8 {
				whereQuery += ` AND (RecordStatus in (8,10) or RecordStatus = @p5)`
			} else {
				whereQuery += ` AND (RecordStatus = @p4 or RecordStatus = @p5)`
			}
		} else {
			if recordStatus != 0 {
				//if 2 show 1,2,3,4,5,9,11,12
				if recordStatus == 2 {
					whereQuery += ` AND RecordStatus in (1,2,3,4,5,9,11,12)`
				} else {
					whereQuery += ` AND RecordStatus = @p4`
				}
			}
		}
		if searchViewParam != nil && *searchViewParam != "" {
			whereQuery += ` AND (MemberName like @p6 or Handphone like @p6)`
		}
	}
	result := make([]*models.Member, 0)
	query := `
		SELECT 
			Cast(m.ID as varchar(36)) as ID,
			Isnull(Code,'') as Code,
			Isnull(MemberName,'') as MemberName,
			Isnull(EmailAddress,'') as EmailAddress,
			Isnull(LoginPIN,'') as LoginPIN,
			Isnull(Phone, '') as Phone,
			Isnull(HandPhone, '') as Handphone,
			Isnull(MemberType, 0) as MemberType,
			Isnull(Gender, '') as Gender,
			Isnull(Nationality, '') as Nationality,
			Isnull(MaritalStatus, '') as MaritalStatus,
			Isnull(VirtualAccountNo, '') as VirtualAccountNo,
			Isnull(RecordStatus, 0) as RecordStatus,
			cast(BankID as varchar(36)) as BankID,
			IsNULL(cast(CategoryID as varchar(36)), '') as CategoryID,
			ISNULL(c.Name, '') as CategoryName,
			ISNULL(m.ProfileImage, '') as ProfileImage,
			ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
			ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
			ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
			ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
			ISNULL(CONVERT(VARCHAR(10), m.DateAuthor, 103) + ' '  + convert(VARCHAR(8), m.DateAuthor, 14) ,'') as DateAuthor,
			ISNULL(Cast(m.UserAuthor as varchar(36)), '') as UserAuthor,
			ISNULL(Cast(m.Imei as varchar(100)), '') as Imei,
			ISNULL(Cast(m.PlayerID as varchar(100)), '') as PlayerID,
			ISNULL(BankAccountName, '') as BankAccountName,
			ISNULL(BankAccountNo, '') as BankAccountNo,
			ISNULL(BankCode, '') as BankCode
		FROM dbo.Member m
			LEFT JOIN dbo.Category c on m.CategoryID = c.ID
			` + whereQuery + `
		ORDER BY 
			Case When @p2 = 'ID' then Cast(m.ID as varchar(36))
				When @p2 = 'Code' then Code
				When @p2 = 'MemberName' then MemberName End`
	// OFFSET @p0 ROWS FETCH NEXT @p1 ROWS ONLY;`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("first", fmt.Sprintf("%v", first)),
		zap.String("count", fmt.Sprintf("%v", count)),
		zap.String("memberType", fmt.Sprintf("%v", memberType)),
		zap.String("recordStatus", fmt.Sprintf("%v", recordStatus)),
		zap.String("recordStatus2", fmt.Sprintf("%v", recordStatus2)),
		zap.String("searchViewParam", fmt.Sprintf("%v", searchViewParam)),
	)
	rows, err := sj.Conn.Query(query,
		// sql.Named("p0", first),
		// sql.Named("p1", count),
		sql.Named("p2", order),
		sql.Named("p3", memberType),
		sql.Named("p4", recordStatus),
		sql.Named("p5", recordStatus2),
		sql.Named("p6", "%"+*searchViewParam+"%"),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Member)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberName,
			&j.Email,
			&j.LoginPIN,
			&j.Phone,
			&j.Handphone,
			&j.MemberType,
			&j.Gender,
			&j.Nationality,
			&j.MaritalStatus,
			&j.VirtualAccountNo,
			&j.RecordStatus,
			&j.BankID,
			&j.Category.ID,
			&j.Category.Name,
			&j.ProfileImage,
			&j.DateInsert,
			&j.DateUpdate,
			&j.UserInsert,
			&j.UserUpdate,
			&j.DateAuthor,
			&j.UserAuthor,
			&j.Imei,
			&j.PlayerID,
			&j.BankAccountName,
			&j.BankAccountNo,
			&j.BankCode,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	//
	return nil, result
}

func (sj *sqlMemberRepository) Pagination(
	ctx context.Context,
	order string,
	direction string,
	first int32,
	count int32,
	memberType int32,
	filter string,
	filterValue string,
) (error, []*models.Member) {
	whereQuery := ""
	if memberType != -1 {
		whereQuery = `WHERE MemberType = @p3 `
		if filter != "" {
			whereQuery += `AND ` + filter + ` = @p6`
		}
	} else {
		if filter != "" {
			whereQuery = `WHERE ` + filter + ` = @p6`
		}
	}
	orderByQuery := ""
	if order != "" {
		orderByQuery += ` ORDER BY ` + order
		if direction != "" {
			orderByQuery += ` ` + direction
		}
	}

	result := make([]*models.Member, 0)
	rows, err := sj.Conn.Query(`
        SELECT 
			Cast(m.ID as varchar(36)) as ID,
			Isnull(Code,'') as Code,
			Isnull(MemberName,'') as MemberName,
			Isnull(EmailAddress,'') as EmailAddress,
			Isnull(LoginPIN,'') as LoginPIN,
			Isnull(Phone, '') as Phone,
			Isnull(HandPhone, '') as Handphone,
			Isnull(MemberType, 0) as MemberType,
			Isnull(Gender, '') as Gender,
			Isnull(Nationality, '') as Nationality,
			Isnull(MaritalStatus, '') as MaritalStatus,
			Isnull(VirtualAccountNo, '') as VirtualAccountNo,
			Isnull(RecordStatus, 0) as RecordStatus,
			cast(BankID as varchar(36)) as BankID,
			IsNULL(cast(CategoryID as varchar(36)), '') as CategoryID,
			ISNULL(c.Name, '') as CategoryName,
			Isnull(m.BirthPlace,'') as BirthPlace,
			Isnull(m.BirthDate,'') as BirthDate,
			Isnull(m.MotherMaiden,'') as MotherMaiden,
			Isnull(m.IdentityNumber,'') as IdentityNumber,
			ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
			ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
			ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
			ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate
			
		FROM dbo.Member m
			LEFT JOIN dbo.Category c on m.CategoryID = c.ID
			`+whereQuery+`
			`+orderByQuery+`
		OFFSET @p0 ROWS FETCH NEXT @p1 ROWS ONLY;`,
		sql.Named("p0", first),
		sql.Named("p1", count),
		sql.Named("p3", memberType),
		sql.Named("p6", filterValue),
	)
	if err != nil {
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Member)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberName,
			&j.Email,
			&j.LoginPIN,
			&j.Phone,
			&j.Handphone,
			&j.MemberType,
			&j.Gender,
			&j.Nationality,
			&j.MaritalStatus,
			&j.VirtualAccountNo,
			&j.RecordStatus,
			&j.BankID,
			&j.CategoryID,
			&j.Category.Name,
			&j.BirthPlace,
			&j.BirthDate,
			&j.MotherMaiden,
			&j.IdentityNumber,
			&j.DateInsert,
			&j.DateUpdate,
			&j.UserInsert,
			&j.UserUpdate,
		)
		if err != nil {
			return err, nil
		}
		j.Category.ID = j.CategoryID.String
		result = append(result, j)
	}
	if err != nil {
		return err, nil
	}
	return nil, result
}
func (sj *sqlMemberRepository) Count(
	ctx context.Context,
	memberType int32,
	filter string,
	filterValue string,
) (error, int32) {

	whereQuery := ""
	if memberType != -1 {
		whereQuery = `WHERE MemberType = @p3 `
		if filter != "" {
			whereQuery += `AND ` + filter + ` = @p6`
		}
	} else {
		if filter != "" {
			whereQuery = `WHERE ` + filter + ` = @p6`
		}
	}
	var countResult int32
	err := sj.Conn.QueryRow(`
        SELECT COUNT(1)
		FROM dbo.Member m
			LEFT JOIN dbo.Category c on m.CategoryID = c.ID
			`+whereQuery+``,
		sql.Named("p3", memberType),
		sql.Named("p6", filterValue),
	).Scan(
		&countResult,
	)
	if err != nil {
		return err, 0
	}
	return nil, countResult
}
func (sj *sqlMemberRepository) GenerateCode(
	ctx context.Context,
	prefix string,
) (error, string) {
	var result string
	query := `
		EXEC dbo.usp_GetRunningNumber
		@prefix = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("prefix", fmt.Sprintf("%v", prefix)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", prefix),
	).Scan(
		&result,
	)
	if err != nil {
		logger.Error("SP error",
			zap.String("error", err.Error()),
		)
		return err, ""
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlMemberRepository) CheckEmailAndPhone(
	ctx context.Context,
	member models.Member,
) (bool, error) {
	var result int32
	query := `
		Select Count(1) From Member Where ( EmailAddress = @p0 Or Handphone = @p1 ) and RecordStatus != 5`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", member)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", member.Email),
		sql.Named("p1", member.Handphone),
	).Scan(
		&result,
	)
	if err != nil {
		logger.Error("SP error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	if result > 0 {
		return false, nil
	}
	return true, nil
}
func (sj *sqlMemberRepository) CheckIdentityNumberRegistered(
	ctx context.Context,
	member models.Member,
) (bool, error) {
	var result int32
	query := `
		Select Count(1) From Member Where IdentityType = @p0 and IdentityNumber = @p1 and RecordStatus != 5`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", member)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", member.IdentityType),
		sql.Named("p1", member.IdentityNumber),
	).Scan(
		&result,
	)
	if err != nil {
		logger.Error("SP error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	if result > 0 {
		return false, nil
	}
	return true, nil
}
func (sj *sqlMemberRepository) CheckIdentityNumberDataRegistered(
	ctx context.Context,
	member models.Member,
) (bool, error) {
	var result int32
	query := `
		Select Count(1) From Member Where MemberName = @p0 and BirthDate = @p1 and RecordStatus != 5`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", member)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", member.MemberName),
		sql.Named("p1", member.BirthDate),
	).Scan(
		&result,
	)
	if err != nil {
		logger.Error("SP error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	if result > 0 {
		return false, nil
	}
	return true, nil
}
func (sj *sqlMemberRepository) CheckEmailPeruri(
	ctx context.Context,
	member models.Member,
) (bool, error) {
	var result int32
	query := `
		Select Count(1) From Member Where PeruriEmailAddress = @p0 `
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", member)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", member.PeruriEmailAddress),
	).Scan(
		&result,
	)
	if err != nil {
		logger.Error("SP error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	if result > 0 {
		return false, nil
	}
	return true, nil
}
func (sj *sqlMemberRepository) GetByID(ctx context.Context, id *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
		ISNULL(m.IdentityNumber, '') as IdentityNumber,
		ISNULL(m.IdentityUpload, '') as IdentityUpload,
		ISNULL(m.NPWPNumber, '') as NPWPNumber,
		ISNULL(m.NPWPUpload, '') as NPWPUpload
	from 
		dbo.Member m
	where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
		&Member.IdentityNumber,
		&Member.IdentityUpload,
		&Member.NoNPWP,
		&Member.NPWPUpload,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}
func (sj *sqlMemberRepository) GetByIDBase(ctx context.Context, id *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(cast(BirthDate as varchar(100)),'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
		ISNULL(NoPasPor,'') as NoPasPor,
		ISNULL(NoAkteLahir,'') as NoAkteLahir,
		ISNULL(SIUP,'') as SIUP,
		ISNULL(TDP,'') as TDP,
		ISNULL(Agama,'') as Agama,
		ISNULL(Akte,'') as Akte,
		ISNULL(JumlahTagungan,'') as JumlahTagungan,
		ISNULL(PendapatanPerbulan,'') as PendapatanPerbulan,
		ISNULL(NamaPasangan,'') as NamaPasangan,
		ISNULL(NoIdentitasPasangan,'') as NoIdentitasPasangan,
		ISNULL(RTRW,'') as RTRW,
		ISNULL(Kelurahan,'') as Kelurahan,
		ISNULL(KodePos,'') as KodePos,
		ISNULL(MaritalStatus,0) as MaritalStatus,
		ISNULL(Kecamatan,'') as Kecamatan,
		ISNULL(NoNPWP,'') as NoNPWP,
		ISNULL(AlamatRumah2,'') as AlamatRumah2,
		ISNULL(AlamatTempatKerja1,'') as AlamatTempatKerja1,
		ISNULL(AlamatTempatKerja2,'') as AlamatTempatKerja2,
		ISNULL(KecamatanTempatKerja,'') as KecamatanTempatKerja,
		ISNULL(FileNameFotoKTP,'') as FileNameFotoKTP,
		ISNULL(FileNameFotoSelfie,'') as FileNameFotoSelfie,
		ISNULL(IdentityType, 0) as IdentityType,
		ISNULL(NoInduk, '') as NoInduk,
		ISNULL(Gender, 0) as Gender,
		ISNULL(BankAccountName,'') as BankAccountName,
		ISNULL(BankAccountNo,'') as BankAccountNo,
		ISNULL(BankCode,'') as BankCode,
		ISNULL(Grade,'') as Grade,
		Isnull(LoginPIN,'') as LoginPIN,
		Isnull(ReferralMemberID,'') as ReferralMemberID
	from 
		dbo.Member m
	where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
		&Member.NoPaspor,
		&Member.NoAkteLahir,
		&Member.SIUP,
		&Member.TDP,
		&Member.Agama,
		&Member.Akte,
		&Member.JumlahTagungan,
		&Member.PendapatanPerbulan,
		&Member.NamaPasangan,
		&Member.NoIdentitasPasangan,
		&Member.RTRW,
		&Member.Kelurahan,
		&Member.KodePos,
		&Member.MaritalStatus,
		&Member.Kecamatan,
		&Member.NoNPWP,
		&Member.AlamatRumah2,
		&Member.AlamatTempatKerja1,
		&Member.AlamatTempatKerja2,
		&Member.KecamatanTempatKerja,
		&Member.FileNameFotoKTP,
		&Member.FileNameFotoSelfie,
		&Member.IdentityType,
		&Member.NoInduk,
		&Member.Gender,
		&Member.BankAccountName,
		&Member.BankAccountNo,
		&Member.BankCode,
		&Member.Grade,
		&Member.LoginPIN,
		&Member.ReferralMemberID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}

func (sj *sqlMemberRepository) GetMemberDetailByMemberID(ctx context.Context, memberID *string) (*models.MemberDetail, error) {
	var MemberDetail models.MemberDetail
	query := `
	select 
		cast(ID as varchar(36)) ID,
		cast(MemberID as varchar(36)) MemberID,
		Isnull(NamaLengkapTanpaSingkatan,'') as NamaLengkapTanpaSingkatan,
		Isnull(StatusPendidikan,'') as StatusPendidikan,
		Isnull(NamaIbuKandung,'') as NamaIbuKandung,
		Isnull(NamaKontakDarurat,'') as NamaKontakDarurat,
		Isnull(NomorKontakDarurat,'') as NomorKontakDarurat,
		Isnull(HubunganKontakDarurat,'') as HubunganKontakDarurat,
		Isnull(Provinsi,'') as Provinsi,
		Isnull(Kabupaten,'') as Kabupaten,
		Isnull(Kecamatan,'') as Kecamatan,
		Isnull(Kelurahan,'') as Kelurahan,
		Isnull(KodePos,'') as KodePos,
		Isnull(Phone,'') as Phone,
		Isnull(ProvinsiTempatKerja,'') as ProvinsiTempatKerja,
		Isnull(KabupatenTempatKerja,'') as KabupatenTempatKerja,
		Isnull(KecamatanTempatKerja,'') as KecamatanTempatKerja,
		Isnull(KelurahanTempatKerja,'') as KelurahanTempatKerja,
		Isnull(KodePosTempatKerja,'') as KodePosTempatKerja,
		Isnull(KodePekerjaan,'') as KodePekerjaan,
		Isnull(NamaTempatKerja,'') as NamaTempatKerja,
		Isnull(KodeBidangUsaha,'') as KodeBidangUsaha,
		Isnull(PenghasilanKotorPerTahun,0) as PenghasilanKotorPerTahun,
		Isnull(KodeSumberPenghasilan,'') as KodeSumberPenghasilan,
		Isnull(MaritalStatus,0)as MaritalStatus,
		Isnull(JumlahTanggungan,0) as JumlahTanggungan,
		Isnull(NoIdentitasPasangan,'') as NoIdentitasPasangan,
		Isnull(NamaPasangan,'') as NamaPasangan,
		ISNULL(CONVERT(VARCHAR(10), TanggalLahirPasangan, 126) ,'') as TanggalLahirPasangan,
		Isnull(PisahHarta,0) as PisahHarta,
		Isnull(AreaID,'') as AreaID,
		Isnull(AreaIDTempatKerja,'') as AreaIDTempatKerja,
		Isnull(b.No_Head_Kategori,'') as StatusPendidikanSTR,
		Isnull(c.Province_Name,'') as ProvinsiSTR,
		Isnull(d.District_Name,'') as KabupatenSTR,
		Isnull(e.Province_Name,'') as ProvinsiTempatKerjaSTR,
		Isnull(f.District_Name,'') as KabupatenTempatKerjaSTR
	from 
		dbo.MemberDetail a
		left join VwMasterBidangSandiKategoriBI b on
		CASE WHEN a.StatusPendidikan='' OR a.StatusPendidikan is null then '00000000-0000-0000-0000-000000000000' else a.statuspendidikan end = b.BidangSandiKategoriBI_ID
		left join [Master.Province] c on a.Provinsi = c.Province_ID
		left join [Master.District] d on a.Kabupaten = d.District_ID
		left join [Master.Province] e on 
		CASE WHEN a.ProvinsiTempatKerja='' OR a.ProvinsiTempatKerja is null then '00000000-0000-0000-0000-000000000000' else a.ProvinsiTempatKerja end = e.Province_ID
		left join [Master.District] f on 
		CASE WHEN a.KabupatenTempatKerja='' OR a.KabupatenTempatKerja is null then '00000000-0000-0000-0000-000000000000' else a.KabupatenTempatKerja end = f.District_ID
	where 
		MemberID = @p0`
	// query := `
	// select
	// 	cast(ID as varchar(36)) ID,
	// 	cast(MemberID as varchar(36)) MemberID,
	// 	Isnull(NamaLengkapTanpaSingkatan,'') as NamaLengkapTanpaSingkatan,
	// 	Isnull(StatusPendidikan,'') as StatusPendidikan,
	// 	Isnull(NamaIbuKandung,'') as NamaIbuKandung,
	// 	Isnull(NamaKontakDarurat,'') as NamaKontakDarurat,
	// 	Isnull(NomorKontakDarurat,'') as NomorKontakDarurat,
	// 	Isnull(HubunganKontakDarurat,'') as HubunganKontakDarurat,
	// 	Isnull(c.Province_Name,'') as Provinsi,
	// 	Isnull(d.District_Name,'') as Kabupaten,
	// 	Isnull(Kecamatan,'') as Kecamatan,
	// 	Isnull(Kelurahan,'') as Kelurahan,
	// 	Isnull(KodePos,'') as KodePos,
	// 	Isnull(Phone,'') as Phone,
	// 	Isnull(e.Province_Name,'') as ProvinsiTempatKerja,
	// 	Isnull(f.District_Name,'') as KabupatenTempatKerja,
	// 	Isnull(KecamatanTempatKerja,'') as KecamatanTempatKerja,
	// 	Isnull(KelurahanTempatKerja,'') as KelurahanTempatKerja,
	// 	Isnull(KodePosTempatKerja,'') as KodePosTempatKerja,
	// 	case when Isnull(KodePekerjaan,'') = 'BE3BF0E1-E842-4360-8B53-0028736E54E5' then 'Pekerja Informal'
	// 	when Isnull(KodePekerjaan,'') = '58045180-392C-4686-BD56-039761CF0091' then 'Pendidikan'
	// 	when Isnull(KodePekerjaan,'') = '45980B34-B9C2-447D-B3E8-03B70F18AB44' then 'Pegawai Pemerintahan/ Lembaga Negara'
	// 	when Isnull(KodePekerjaan,'') = '20A0A824-716E-4533-A3D4-10F1C328F948' then 'Komputer'
	// 	when Isnull(KodePekerjaan,'') = 'CCAE0138-F70F-4F4F-98DC-125F375890FF' then 'Tenaga Medis'
	// 	when Isnull(KodePekerjaan,'') = '5B6EAFCC-5887-465A-8F1A-172CC0B522C3' then 'Petani'
	// 	when Isnull(KodePekerjaan,'') = 'D5002CF1-B26F-4272-99D3-23A5C08B4236' then 'Pejabat Negara/ Penyelenggara Negara'
	// 	when Isnull(KodePekerjaan,'') = 'A1A06410-8D7D-4A6B-B4EA-25C100366F1E' then 'Peternak'
	// 	when Isnull(KodePekerjaan,'') = '1FB8EB28-86BE-4156-91AD-25DBA238E32F' then 'Marketing'
	// 	when Isnull(KodePekerjaan,'') = '8A17BB6B-C81E-4BBD-A8AD-29BAB4C5B551' then 'Pialang / Broker'
	// 	when Isnull(KodePekerjaan,'') = '0E43214A-C1D7-4695-9A38-3758FEE23F73' then 'Transportasi Darat'
	// 	when Isnull(KodePekerjaan,'') = '0D15514D-ABCD-4C41-B98A-3AD038C68EC7' then 'Customer Service'
	// 	when Isnull(KodePekerjaan,'') = 'DE01612D-0D8F-4635-B2E2-3C666D02BCB5' then 'Desainer'
	// 	when Isnull(KodePekerjaan,'') = '3EA597F0-9276-4EB5-B051-44B24F6827BE' then 'Buruh'
	// 	when Isnull(KodePekerjaan,'') = '4450125F-080D-4DCD-AE44-46A7F12FAE70' then 'Engineering'
	// 	when Isnull(KodePekerjaan,'') = 'E9EF5BAC-6B0E-40AA-A0F3-48FE325DFE60' then 'Eksekutif'
	// 	when Isnull(KodePekerjaan,'') = '12F272DE-8AA9-4A54-AD53-4B1D286261E1' then 'Transportasi Laut'
	// 	when Isnull(KodePekerjaan,'') = 'B0AE3F2F-2F40-4846-8595-4D1C6D1914E1' then 'Pengamanan'
	// 	when Isnull(KodePekerjaan,'') = '7576E1C1-F425-4E50-A583-54A3A615ACFA' then 'Pensiunan'
	// 	when Isnull(KodePekerjaan,'') = 'DE5754DF-FECF-41A4-9804-5805372E5880' then 'Polisi'
	// 	when Isnull(KodePekerjaan,'') = '4463F87B-314B-4B0B-B541-612F42A3BF11' then 'Lain-lain'
	// 	when Isnull(KodePekerjaan,'') = '8E3DCFE7-C5DB-4DA6-AB3B-62EC32F530B2' then 'Transportasi Udara'
	// 	when Isnull(KodePekerjaan,'') = '26A66F64-8981-4C91-9A89-782CD590452B' then 'Militer'
	// 	when Isnull(KodePekerjaan,'') = 'C25EDEB7-F90E-470A-BC1E-8488378A4A29' then 'Dokter'
	// 	when Isnull(KodePekerjaan,'') = '2B4E2D1A-3BBC-44FC-ACF9-857DFB8704CA' then 'Ibu Rumah Tangga'
	// 	when Isnull(KodePekerjaan,'') = '180366CA-1D96-431A-9370-88CE0A3B818F' then 'Wiraswasta'
	// 	when Isnull(KodePekerjaan,'') = '4DE2D2B1-07DF-4CBD-A4FF-95A2813C902A' then 'Administrasi Umum'
	// 	when Isnull(KodePekerjaan,'') = '1B5DCE00-EE4B-479E-AA29-990043EC2113' then 'Hukum'
	// 	when Isnull(KodePekerjaan,'') = '98CFBFB9-F474-4676-B6F9-AE612B273F6F' then 'Arsitek'
	// 	when Isnull(KodePekerjaan,'') = '96F5F13E-CAB6-435D-A491-B40B08F350DB' then 'Pelajar/Mahasiswa'
	// 	when Isnull(KodePekerjaan,'') = 'D61B813D-B890-436F-87AD-BC93C3AD8A22' then 'Akunting/Keuangan'
	// 	when Isnull(KodePekerjaan,'') = 'D06688FD-B23B-43F6-BB3E-C3B2B5934734' then 'Nelayan'
	// 	when Isnull(KodePekerjaan,'') = '9DD5DC22-350A-46D1-ABEA-C5ECB78705DF' then 'Pekerja Seni'
	// 	when Isnull(KodePekerjaan,'') = '59D16268-E4A8-48DF-AC26-E3503721AC54' then 'Perhotelan & Restoran'
	// 	when Isnull(KodePekerjaan,'') = '63E42F4C-7DA5-4795-85E7-E3E61E2A4F51' then 'Peneliti'
	// 	when Isnull(KodePekerjaan,'') = 'C2981D05-687E-4EB0-843F-F1CA887040D6' then 'Pertukangan & Pengrajin'
	// 	when Isnull(KodePekerjaan,'') = 'BF624F81-215D-46FF-B5D3-F1CACD1A03D1' then 'Distributor'
	// 	when Isnull(KodePekerjaan,'') = '0D39DAF1-C365-490D-A906-F63E25087C02' then 'Konsultan'
	// 	else '' end as KodePekerjaan,
	// 	Isnull(NamaTempatKerja,'') as NamaTempatKerja,
	// 	Isnull(b.No_Head_Kategori,'') as KodeBidangUsaha,
	// 	Isnull(PenghasilanKotorPerTahun,0) as PenghasilanKotorPerTahun,
	// 	case when Isnull(KodeSumberPenghasilan,'') = '4C4C1204-514B-42AF-B47C-3E92B4EA05E5' then 'Gaji'
	// 	when Isnull(KodeSumberPenghasilan,'') = '8BD33B99-49B8-465E-9DFD-6479367D28A4' then 'Usaha'
	// 	when Isnull(KodeSumberPenghasilan,'') = '7D4F7659-7305-4499-BDDB-278DC0C3D48F' then 'Lainnya' else '' end as KodeSumberPenghasilan,
	// 	case when Isnull(MaritalStatus,0) = '5' then 'Lajang'
	// 	when Isnull(MaritalStatus,0) = '3' then 'Menikah'
	// 	when Isnull(MaritalStatus,0) = '1' then 'De Facto'
	// 	when Isnull(MaritalStatus,0) = '2' then 'Cerai'
	// 	when Isnull(MaritalStatus,0) = '4' then 'Terpisah'
	// 	when Isnull(MaritalStatus,0) = '6' then 'Duda/Janda' else '' end as MaritalStatus,
	// 	Isnull(JumlahTanggungan,0) as JumlahTanggungan,
	// 	Isnull(NoIdentitasPasangan,'') as NoIdentitasPasangan,
	// 	Isnull(NamaPasangan,'') as NamaPasangan,
	// 	ISNULL(CONVERT(VARCHAR(10), TanggalLahirPasangan, 103) ,'') as TanggalLahirPasangan,
	// 	case when Isnull(PisahHarta,0) = 0 then 'Tidak' else 'Ya' end as PisahHarta,
	// 	Isnull(AreaID,'') as AreaID,
	// 	Isnull(AreaIDTempatKerja,'') as AreaIDTempatKerja
	// from
	// 	dbo.MemberDetail a
	// 	left join [Master.BidangSandi.KategoriBI] b on a.KodeBidangUsaha = b.BidangSandiKategoriBI_ID
	// 	left join [Master.Province] c on a.Provinsi = c.Province_ID
	// 	left join [Master.District] d on a.Kabupaten = d.District_ID
	// 	left join [Master.Province] e on a.ProvinsiTempatKerja = e.Province_ID
	// 	left join [Master.District] f on a.KabupatenTempatKerja = f.District_ID
	// where
	// 	MemberID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", memberID)).Scan(
		&MemberDetail.ID,
		&MemberDetail.MemberID,
		&MemberDetail.NamaLengkapTanpaSingkatan,
		&MemberDetail.StatusPendidikan,
		&MemberDetail.NamaIbuKandung,
		&MemberDetail.NamaKontakDarurat,
		&MemberDetail.NomorKontakDarurat,
		&MemberDetail.HubunganKontakDarurat,
		&MemberDetail.Provinsi,
		&MemberDetail.Kabupaten,
		&MemberDetail.Kecamatan,
		&MemberDetail.Kelurahan,
		&MemberDetail.KodePos,
		&MemberDetail.Phone,
		&MemberDetail.ProvinsiTempatKerja,
		&MemberDetail.KabupatenTempatKerja,
		&MemberDetail.KecamatanTempatKerja,
		&MemberDetail.KelurahanTempatKerja,
		&MemberDetail.KodePosTempatKerja,
		&MemberDetail.KodePekerjaan,
		&MemberDetail.NamaTempatKerja,
		&MemberDetail.KodeBidangUsaha,
		&MemberDetail.PenghasilanKotorPerTahun,
		&MemberDetail.KodeSumberPenghasilan,
		&MemberDetail.MaritalStatus,
		&MemberDetail.JumlahTanggungan,
		&MemberDetail.NoIdentitasPasangan,
		&MemberDetail.NamaPasangan,
		&MemberDetail.TanggalLahirPasangan,
		&MemberDetail.PisahHarta,
		&MemberDetail.AreaID,
		&MemberDetail.AreaIDTempatKerja,
		&MemberDetail.StatusPendidikanSTR,
		&MemberDetail.ProvinsiSTR,
		&MemberDetail.KabupatenSTR,
		&MemberDetail.ProvinsiTempatKerjaSTR,
		&MemberDetail.KabupatenTempatKerjaSTR,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", MemberDetail)),
	)
	return &MemberDetail, nil
}

func (sj *sqlMemberRepository) GetByCodeBase(ctx context.Context, code *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(cast(BirthDate as varchar(100)),'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
		ISNULL(NoPasPor,'') as NoPasPor,
		ISNULL(NoAkteLahir,'') as NoAkteLahir,
		ISNULL(SIUP,'') as SIUP,
		ISNULL(TDP,'') as TDP,
		ISNULL(Agama,'') as Agama,
		ISNULL(Akte,'') as Akte,
		ISNULL(JumlahTagungan,'') as JumlahTagungan,
		ISNULL(PendapatanPerbulan,'') as PendapatanPerbulan,
		ISNULL(NamaPasangan,'') as NamaPasangan,
		ISNULL(NoIdentitasPasangan,'') as NoIdentitasPasangan,
		ISNULL(RTRW,'') as RTRW,
		ISNULL(Kelurahan,'') as Kelurahan,
		ISNULL(KodePos,'') as KodePos,
		ISNULL(MaritalStatus,0) as MaritalStatus,
		ISNULL(Kecamatan,'') as Kecamatan,
		ISNULL(NoNPWP,'') as NoNPWP,
		ISNULL(AlamatRumah2,'') as AlamatRumah2,
		ISNULL(AlamatTempatKerja1,'') as AlamatTempatKerja1,
		ISNULL(AlamatTempatKerja2,'') as AlamatTempatKerja2,
		ISNULL(KecamatanTempatKerja,'') as KecamatanTempatKerja,
		ISNULL(FileNameFotoKTP,'') as FileNameFotoKTP,
		ISNULL(FileNameFotoSelfie,'') as FileNameFotoSelfie,
		ISNULL(IdentityType, 0) as IdentityType,
		ISNULL(NoInduk, '') as NoInduk,
		ISNULL(Gender, 0) as Gender,
		ISNULL(BankAccountName,'') as BankAccountName,
		ISNULL(BankAccountNo,'') as BankAccountNo,
		ISNULL(BankCode,'') as BankCode,
		ISNULL(Grade,'') as Grade,
		Isnull(LoginPIN,'') as LoginPIN,
		ISNULL(ReferralMemberID,'') as ReferralMemberID
	from 
		dbo.Member m
	where 
		Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
		&Member.NoPaspor,
		&Member.NoAkteLahir,
		&Member.SIUP,
		&Member.TDP,
		&Member.Agama,
		&Member.Akte,
		&Member.JumlahTagungan,
		&Member.PendapatanPerbulan,
		&Member.NamaPasangan,
		&Member.NoIdentitasPasangan,
		&Member.RTRW,
		&Member.Kelurahan,
		&Member.KodePos,
		&Member.MaritalStatus,
		&Member.Kecamatan,
		&Member.NoNPWP,
		&Member.AlamatRumah2,
		&Member.AlamatTempatKerja1,
		&Member.AlamatTempatKerja2,
		&Member.KecamatanTempatKerja,
		&Member.FileNameFotoKTP,
		&Member.FileNameFotoSelfie,
		&Member.IdentityType,
		&Member.NoInduk,
		&Member.Gender,
		&Member.BankAccountName,
		&Member.BankAccountNo,
		&Member.BankCode,
		&Member.Grade,
		&Member.LoginPIN,
		&Member.ReferralMemberID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}
func (sj *sqlMemberRepository) GetByHandphone(ctx context.Context, handphone *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate
	from 
		dbo.Member m
	where 
		Handphone = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("handphone", fmt.Sprintf("%v", handphone)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", handphone)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}
func (sj *sqlMemberRepository) GetByHandphoneBase(ctx context.Context, handphone *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		Isnull(Imei,'') as Imei,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate
	from 
		dbo.Member m
	where 
		Handphone = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("handphone", fmt.Sprintf("%v", handphone)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", handphone)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.Imei,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}
func (sj *sqlMemberRepository) GetByEmail(ctx context.Context, email *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate
	from 
		dbo.Member m
	where 
		EmailAddress = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("email", fmt.Sprintf("%v", email)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", email)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}

func (sj *sqlMemberRepository) GetByCode(ctx context.Context, code *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as Code,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate
	from 
		dbo.Member m
	where 
		Code = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}

func (sj *sqlMemberRepository) CheckPIN(ctx context.Context, id *string, pin *string) (*bool, error) {
	var valid = false
	j := new(models.Member)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		LoginPIN
	FROM 
		dbo.Member 
	where 
		id = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return &valid, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(
			&j.ID,
			&j.LoginPIN,
		)
		valid = true
	}
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &valid, err
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", valid)),
	)

	err = bcrypt.CompareHashAndPassword([]byte(j.LoginPIN.String), []byte(*pin))
	if err != nil {
		valid = false
		return &valid, fmt.Errorf("Password Salah.")
	}
	return &valid, nil
}

func (sj *sqlMemberRepository) GetCategory(ctx context.Context, id *string) *models.Category {
	var Member models.Member
	err := sj.Conn.QueryRow(`
        select 
			cast(ID as varchar(36)) ID,
			Name
        from 
			dbo.Category 
		where 
			ID = @p0`, sql.Named("p0", id)).Scan(
		&Member.Category.ID,
		&Member.Category.Name,
	)
	if err != nil {
		log.Fatal(err)
	}
	return &Member.Category
}

func (sj *sqlMemberRepository) StoreBase(ctx context.Context, a *models.Member) (*models.Member, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	var err error
	// hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
	// if err != nil {
	// 	return nil, err
	// }
	// a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
	a.UserInsert = a.ID
	if ctx.Value(service.CtxUserID) != nil {
		a.UserInsert = ctx.Value(service.CtxUserID).(string)
	}

	MemberQuery := `INSERT INTO Member (
		id, 
		code, 
		MemberName, 
		EmailAddress, 
		LoginPIN, 
		MemberType,
		Gender,
		Nationality,
		MaritalStatus,
		ChildrenCount,
		BankID,
		PendidikanTerakhir,
		RecordStatus,
		IdentityType,
		IsLocked,
		Phone,
		Handphone,
		VirtualAccountNo,
		ProfileImage,
		CategoryID,
		IdentityNumber,
		AlamatRumah,
		BirthPlace,
		BirthDate,
		MotherMaiden,
		GoldStatus,
		IdentityUpload,
		NPWPUpload,
		VerificationCodeEmail,
		FotoKTPDiriUpload,
		UserInsert,
		DateInsert,
		NoPaspor,
		NoAkteLahir,
		SIUP,
		Akte,
		TDP,
		NoNPWP,
		NoInduk,
		AlamatRumah2,
		AlamatTempatKerja1,
		AlamatTempatKerja2,
		PropinsiTempatKerja,
		KabupatenTempatKerja,
		KecamatanTempatKerja,
		KelurahanTempatKerja,
		KodePosTempatKerja,
		FileNameFotoKTP,
		FileNameFotoSelfie,
		Provinsi,
		Kota,
		Kecamatan,
		Kelurahan,
		KodePos,
		BankAccountName,
		BankAccountNo,
		BankCode,
		Grade
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, 
		@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25,
		@p26,@p27,@p28,@p30, @userInsert, GETDATE(),@p31,@p32,@p33,@p34,@p35,@p36,
		@p37,@p38,@p39,@p40,@p41,@p42,@p43,@p44,@p45,@p46,@p47,@p49,@p50,@p51,@p52,@p53,@p54,@p55,@p56,@p57)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code.String),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.Gender),
		sql.Named("p7", a.Nationality),
		sql.Named("p8", a.MaritalStatus),
		sql.Named("p9", 0),
		sql.Named("p10", a.BankID),
		sql.Named("p11", 1),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.IdentityType),
		sql.Named("p14", a.IsLocked),
		sql.Named("p15", a.Phone),
		sql.Named("p16", a.Handphone),
		sql.Named("p17", a.VirtualAccountNo),
		sql.Named("p18", a.ProfileImage),
		sql.Named("p19", a.CategoryID),
		sql.Named("p20", a.IdentityNumber.String),
		sql.Named("p21", a.Address),
		sql.Named("p22", a.BirthPlace),
		sql.Named("p23", a.BirthDate),
		sql.Named("p24", a.MotherMaiden),
		sql.Named("p25", a.GoldStatus),
		sql.Named("p26", a.IdentityUpload),
		sql.Named("p27", a.NPWPUpload),
		sql.Named("p28", a.VerificationCodeEmail),
		sql.Named("p30", a.FotoKTPDiriUpload),
		sql.Named("userInsert", a.UserInsert),
		sql.Named("p31", a.NoPaspor),
		sql.Named("p32", a.NoAkteLahir),
		sql.Named("p33", a.SIUP),
		sql.Named("p34", a.Akte),
		sql.Named("p35", a.TDP),
		sql.Named("p36", a.NoNPWP),
		sql.Named("p37", a.NoInduk),
		sql.Named("p38", a.AlamatRumah2),
		sql.Named("p39", a.AlamatTempatKerja1),
		sql.Named("p40", a.AlamatTempatKerja2),
		sql.Named("p41", a.PropinsiTempatKerja),
		sql.Named("p42", a.KabupatenTempatKerja),
		sql.Named("p43", a.KecamatanTempatKerja),
		sql.Named("p44", a.KelurahanTempatKerja),
		sql.Named("p45", a.KodePosTempatKerja),
		sql.Named("p46", a.FileNameFotoKTP),
		sql.Named("p47", a.FileNameFotoSelfie),
		sql.Named("p49", a.Province),
		sql.Named("p50", a.City),
		sql.Named("p51", a.Kecamatan),
		sql.Named("p52", a.Kelurahan),
		sql.Named("p53", a.KodePos),
		sql.Named("p54", a.BankAccountName),
		sql.Named("p55", a.BankAccountNo),
		sql.Named("p56", a.BankCode),
		sql.Named("p57", a.Grade),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberRepository) StoreBaseByDate(ctx context.Context, a *models.Member, companyDate string) (*models.Member, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	var err error
	// hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
	// if err != nil {
	// 	return nil, err
	// }
	// a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
	a.UserInsert = a.ID
	if ctx.Value(service.CtxUserID) != nil {
		a.UserInsert = ctx.Value(service.CtxUserID).(string)
	}

	MemberQuery := `INSERT INTO Member (
		id, 
		code, 
		MemberName, 
		EmailAddress, 
		LoginPIN, 
		MemberType,
		Gender,
		Nationality,
		MaritalStatus,
		ChildrenCount,
		BankID,
		PendidikanTerakhir,
		RecordStatus,
		IdentityType,
		IsLocked,
		Phone,
		Handphone,
		VirtualAccountNo,
		ProfileImage,
		CategoryID,
		IdentityNumber,
		AlamatRumah,
		BirthPlace,
		BirthDate,
		MotherMaiden,
		GoldStatus,
		IdentityUpload,
		NPWPUpload,
		VerificationCodeEmail,
		FotoKTPDiriUpload,
		UserInsert,
		DateInsert,
		NoPaspor,
		NoAkteLahir,
		SIUP,
		Akte,
		TDP,
		NoNPWP,
		NoInduk,
		AlamatRumah2,
		AlamatTempatKerja1,
		AlamatTempatKerja2,
		PropinsiTempatKerja,
		KabupatenTempatKerja,
		KecamatanTempatKerja,
		KelurahanTempatKerja,
		KodePosTempatKerja,
		FileNameFotoKTP,
		FileNameFotoSelfie,
		Provinsi,
		Kota,
		Kecamatan,
		Kelurahan,
		KodePos,
		BankAccountName,
		BankAccountNo,
		BankCode,
		Grade,
		IsUpload
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, 
		@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25,
		@p26,@p27,@p28,@p30, @userInsert, @date,@p31,@p32,@p33,@p34,@p35,@p36,
		@p37,@p38,@p39,@p40,@p41,@p42,@p43,@p44,@p45,@p46,@p47,@p49,@p50,@p51,@p52,@p53,@p54,@p55,@p56,@p57,@p58)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code.String),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.Gender),
		sql.Named("p7", a.Nationality),
		sql.Named("p8", a.MaritalStatus),
		sql.Named("p9", 0),
		sql.Named("p10", a.BankID),
		sql.Named("p11", 1),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.IdentityType),
		sql.Named("p14", a.IsLocked),
		sql.Named("p15", a.Phone),
		sql.Named("p16", a.Handphone),
		sql.Named("p17", a.VirtualAccountNo),
		sql.Named("p18", a.ProfileImage),
		sql.Named("p19", a.CategoryID),
		sql.Named("p20", a.IdentityNumber.String),
		sql.Named("p21", a.Address),
		sql.Named("p22", a.BirthPlace),
		sql.Named("p23", a.BirthDate),
		sql.Named("p24", a.MotherMaiden),
		sql.Named("p25", a.GoldStatus),
		sql.Named("p26", a.IdentityUpload),
		sql.Named("p27", a.NPWPUpload),
		sql.Named("p28", a.VerificationCodeEmail),
		sql.Named("p30", a.FotoKTPDiriUpload),
		sql.Named("userInsert", a.UserInsert),
		sql.Named("p31", a.NoPaspor),
		sql.Named("p32", a.NoAkteLahir),
		sql.Named("p33", a.SIUP),
		sql.Named("p34", a.Akte),
		sql.Named("p35", a.TDP),
		sql.Named("p36", a.NoNPWP),
		sql.Named("p37", a.NoInduk),
		sql.Named("p38", a.AlamatRumah2),
		sql.Named("p39", a.AlamatTempatKerja1),
		sql.Named("p40", a.AlamatTempatKerja2),
		sql.Named("p41", a.PropinsiTempatKerja),
		sql.Named("p42", a.KabupatenTempatKerja),
		sql.Named("p43", a.KecamatanTempatKerja),
		sql.Named("p44", a.KelurahanTempatKerja),
		sql.Named("p45", a.KodePosTempatKerja),
		sql.Named("p46", a.FileNameFotoKTP),
		sql.Named("p47", a.FileNameFotoSelfie),
		sql.Named("p49", a.Province),
		sql.Named("p50", a.City),
		sql.Named("p51", a.Kecamatan),
		sql.Named("p52", a.Kelurahan),
		sql.Named("p53", a.KodePos),
		sql.Named("p54", a.BankAccountName),
		sql.Named("p55", a.BankAccountNo),
		sql.Named("p56", a.BankCode),
		sql.Named("p57", a.Grade),
		sql.Named("p58", a.IsUpload),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberRepository) StoreMemberDetail(ctx context.Context, a *models.MemberDetail) (*models.MemberDetail, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	var err error

	//Backup karena untuk default param saat insert dikosongin
	// MemberDetailQuery := `INSERT INTO MemberDetail (
	// 	[ID]
	// 	,[MemberID]
	// 	,[NamaLengkapTanpaSingkatan]
	// 	,[StatusPendidikan]
	// 	,[NamaIbuKandung]
	// 	,[NamaKontakDarurat]
	// 	,[NomorKontakDarurat]
	// 	,[HubunganKontakDarurat]
	// 	,[Provinsi]
	// 	,[Kabupaten]
	// 	,[Kecamatan]
	// 	,[Kelurahan]
	// 	,[Kodepos]
	// 	,[Phone]
	// 	,[ProvinsiTempatKerja]
	// 	,[KabupatenTempatKerja]
	// 	,[KecamatanTempatKerja]
	// 	,[KelurahanTempatKerja]
	// 	,[KodePosTempatKerja]
	// 	,[KodePekerjaan]
	// 	,[NamaTempatKerja]
	// 	,[KodeBidangUsaha]
	// 	,[PenghasilanKotorPerTahun]
	// 	,[KodeSumberPenghasilan]
	// 	,[MaritalStatus]
	// 	,[JumlahTanggungan]
	// 	,[NoIdentitasPasangan]
	// 	,[NamaPasangan]
	// 	,[TanggalLahirPasangan]
	// 	,[PisahHarta]
	// 	,[AreaID]
	// 	,[AreaIDTempatKerja]
	// 	,[UserInsert]
	// 	,[DateInsert]
	// 	,[UserUpdate]
	// 	,[DateUpdate]
	// 	)
	// VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10,
	// 		@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20,
	// 		@p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28, @p29, @p30,
	// 		@p31, @userInsert, GETDATE(), null, null)`
	// logger := service.Logger(ctx)
	// logger.Info("Query",
	// 	zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
	// 	zap.String("obj", fmt.Sprintf("%v", a)),
	// )
	// _, err = sj.Conn.ExecContext(ctx, MemberDetailQuery,
	// 	sql.Named("p0", a.ID),
	// 	sql.Named("p1", a.MemberID),
	// 	sql.Named("p2", a.NamaLengkapTanpaSingkatan),
	// 	sql.Named("p3", a.StatusPendidikan),
	// 	sql.Named("p4", a.NamaIbuKandung),
	// 	sql.Named("p5", a.NamaKontakDarurat),
	// 	sql.Named("p6", a.NomorKontakDarurat),
	// 	sql.Named("p7", a.HubunganKontakDarurat),
	// 	sql.Named("p8", a.Provinsi),
	// 	sql.Named("p9", a.Kabupaten),
	// 	sql.Named("p10", a.Kecamatan),
	// 	sql.Named("p11", a.Kelurahan),
	// 	sql.Named("p12", a.KodePos),
	// 	sql.Named("p13", a.Phone),
	// 	sql.Named("p14", a.ProvinsiTempatKerja),
	// 	sql.Named("p15", a.KabupatenTempatKerja),
	// 	sql.Named("p16", a.KecamatanTempatKerja),
	// 	sql.Named("p17", a.KelurahanTempatKerja),
	// 	sql.Named("p18", a.KodePosTempatKerja),
	// 	sql.Named("p19", a.KodePekerjaan),
	// 	sql.Named("p20", a.NamaTempatKerja),
	// 	sql.Named("p21", a.KodeBidangUsaha),
	// 	sql.Named("p22", a.PenghasilanKotorPerTahun),
	// 	sql.Named("p23", a.KodeSumberPenghasilan),
	// 	sql.Named("p24", a.MaritalStatus),
	// 	sql.Named("p25", a.JumlahTanggungan),
	// 	sql.Named("p26", a.NoIdentitasPasangan),
	// 	sql.Named("p27", a.NamaPasangan),
	// 	sql.Named("p28", a.TanggalLahirPasangan),
	// 	sql.Named("p29", a.PisahHarta),
	// 	sql.Named("p30", a.AreaID),
	// 	sql.Named("p31", a.AreaIDTempatKerja),
	// 	sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
	// )
	MemberDetailQuery := `INSERT INTO MemberDetail (
		[ID]
		,[MemberID]
		,[NamaLengkapTanpaSingkatan]
		,[StatusPendidikan]
		,[NamaIbuKandung]
		,[NamaKontakDarurat]
		,[NomorKontakDarurat]
		,[HubunganKontakDarurat]
		,[Provinsi]
		,[Kabupaten]
		,[Kecamatan]
		,[Kelurahan]
		,[Kodepos]
		,[Phone]
		,[PenghasilanKotorPerTahun]
		,[KodeSumberPenghasilan]
		,[MaritalStatus]
		,[JumlahTanggungan]
		,[NoIdentitasPasangan]
		,[NamaPasangan]
		,[PisahHarta]
		,[AreaID]
		,[AreaIDTempatKerja]
		,[UserInsert]
		,[DateInsert]
		,[UserUpdate]
		,[DateUpdate]
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10,
			@p11, @p12, @p13, @p22, @p23, @p24, @p25, @p26, @p27, @p29, @p30,
			@p31, @userInsert, getdate(), null, null)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberDetailQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.NamaLengkapTanpaSingkatan),
		sql.Named("p3", a.StatusPendidikan),
		sql.Named("p4", a.NamaIbuKandung),
		sql.Named("p5", a.NamaKontakDarurat),
		sql.Named("p6", a.NomorKontakDarurat),
		sql.Named("p7", a.HubunganKontakDarurat),
		sql.Named("p8", a.Provinsi),
		sql.Named("p9", a.Kabupaten),
		sql.Named("p10", a.Kecamatan),
		sql.Named("p11", a.Kelurahan),
		sql.Named("p12", a.KodePos),
		sql.Named("p13", a.Phone),
		//sql.Named("p14", a.ProvinsiTempatKerja),
		//sql.Named("p15", a.KabupatenTempatKerja),
		//sql.Named("p16", a.KecamatanTempatKerja),
		//sql.Named("p17", a.KelurahanTempatKerja),
		//sql.Named("p18", a.KodePosTempatKerja),
		//sql.Named("p19", a.KodePekerjaan),
		//sql.Named("p20", a.NamaTempatKerja),
		//sql.Named("p21", a.KodeBidangUsaha),
		sql.Named("p22", a.PenghasilanKotorPerTahun),
		sql.Named("p23", a.KodeSumberPenghasilan),
		sql.Named("p24", a.MaritalStatus),
		sql.Named("p25", a.JumlahTanggungan),
		sql.Named("p26", a.NoIdentitasPasangan),
		sql.Named("p27", a.NamaPasangan),
		//sql.Named("p28", a.TanggalLahirPasangan),
		sql.Named("p29", a.PisahHarta),
		sql.Named("p30", a.AreaID),
		sql.Named("p31", a.AreaIDTempatKerja),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMemberRepository) StoreMemberDetailFromUploadByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	var err error

	MemberDetailQuery := `INSERT INTO MemberDetail (
		[ID]
		,[MemberID]
		,[NamaLengkapTanpaSingkatan]
		,[StatusPendidikan]
		,[NamaIbuKandung]
		,[NamaKontakDarurat]
		,[NomorKontakDarurat]
		,[HubunganKontakDarurat]
		,[Provinsi]
		,[Kabupaten]
		,[Kecamatan]
		,[Kelurahan]
		,[Kodepos]
		,[Phone]
		,[ProvinsiTempatKerja]
		,[KabupatenTempatKerja]
		,[KecamatanTempatKerja]
		,[KelurahanTempatKerja]
		,[KodePosTempatKerja]
		,[KodePekerjaan]
		,[NamaTempatKerja]
		,[KodeBidangUsaha]
		,[PenghasilanKotorPerTahun]
		,[KodeSumberPenghasilan]
		,[MaritalStatus]
		,[JumlahTanggungan]
		,[NoIdentitasPasangan]
		,[NamaPasangan]
		,[TanggalLahirPasangan]
		,[PisahHarta]
		,[AreaID]
		,[AreaIDTempatKerja]
		,[UserInsert]
		,[DateInsert]
		,[UserUpdate]
		,[DateUpdate]
		)
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10,
			@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20,
			@p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28, @p29, @p30,
			@p31, @userInsert, @date, null, null)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberDetailQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.NamaLengkapTanpaSingkatan),
		sql.Named("p3", a.StatusPendidikan),
		sql.Named("p4", a.NamaIbuKandung),
		sql.Named("p5", a.NamaKontakDarurat),
		sql.Named("p6", a.NomorKontakDarurat),
		sql.Named("p7", a.HubunganKontakDarurat),
		sql.Named("p8", a.Provinsi),
		sql.Named("p9", a.Kabupaten),
		sql.Named("p10", a.Kecamatan),
		sql.Named("p11", a.Kelurahan),
		sql.Named("p12", a.KodePos),
		sql.Named("p13", a.Phone),
		sql.Named("p14", a.ProvinsiTempatKerja),
		sql.Named("p15", a.KabupatenTempatKerja),
		sql.Named("p16", a.KecamatanTempatKerja),
		sql.Named("p17", a.KelurahanTempatKerja),
		sql.Named("p18", a.KodePosTempatKerja),
		sql.Named("p19", a.KodePekerjaan),
		sql.Named("p20", a.NamaTempatKerja),
		sql.Named("p21", a.KodeBidangUsaha),
		sql.Named("p22", a.PenghasilanKotorPerTahun),
		sql.Named("p23", a.KodeSumberPenghasilan),
		sql.Named("p24", a.MaritalStatus),
		sql.Named("p25", a.JumlahTanggungan),
		sql.Named("p26", a.NoIdentitasPasangan),
		sql.Named("p27", a.NamaPasangan),
		sql.Named("p28", a.TanggalLahirPasangan),
		sql.Named("p29", a.PisahHarta),
		sql.Named("p30", a.AreaID),
		sql.Named("p31", a.AreaIDTempatKerja),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)

	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMemberRepository) StoreMemberDetailByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {
	if a.ID == "" {
		a.ID = guid.New().StringUpper()
	}
	var err error

	//Backup karena untuk default param saat insert dikosongin
	// MemberDetailQuery := `INSERT INTO MemberDetail (
	// 	[ID]
	// 	,[MemberID]
	// 	,[NamaLengkapTanpaSingkatan]
	// 	,[StatusPendidikan]
	// 	,[NamaIbuKandung]
	// 	,[NamaKontakDarurat]
	// 	,[NomorKontakDarurat]
	// 	,[HubunganKontakDarurat]
	// 	,[Provinsi]
	// 	,[Kabupaten]
	// 	,[Kecamatan]
	// 	,[Kelurahan]
	// 	,[Kodepos]
	// 	,[Phone]
	// 	,[ProvinsiTempatKerja]
	// 	,[KabupatenTempatKerja]
	// 	,[KecamatanTempatKerja]
	// 	,[KelurahanTempatKerja]
	// 	,[KodePosTempatKerja]
	// 	,[KodePekerjaan]
	// 	,[NamaTempatKerja]
	// 	,[KodeBidangUsaha]
	// 	,[PenghasilanKotorPerTahun]
	// 	,[KodeSumberPenghasilan]
	// 	,[MaritalStatus]
	// 	,[JumlahTanggungan]
	// 	,[NoIdentitasPasangan]
	// 	,[NamaPasangan]
	// 	,[TanggalLahirPasangan]
	// 	,[PisahHarta]
	// 	,[AreaID]
	// 	,[AreaIDTempatKerja]
	// 	,[UserInsert]
	// 	,[DateInsert]
	// 	,[UserUpdate]
	// 	,[DateUpdate]
	// 	)
	// VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10,
	// 		@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20,
	// 		@p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28, @p29, @p30,
	// 		@p31, @userInsert, @date, null, null)`
	// logger := service.Logger(ctx)
	// logger.Info("Query",
	// 	zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
	// 	zap.String("obj", fmt.Sprintf("%v", a)),
	// )
	// _, err = sj.Conn.ExecContext(ctx, MemberDetailQuery,
	// 	sql.Named("p0", a.ID),
	// 	sql.Named("p1", a.MemberID),
	// 	sql.Named("p2", a.NamaLengkapTanpaSingkatan),
	// 	sql.Named("p3", a.StatusPendidikan),
	// 	sql.Named("p4", a.NamaIbuKandung),
	// 	sql.Named("p5", a.NamaKontakDarurat),
	// 	sql.Named("p6", a.NomorKontakDarurat),
	// 	sql.Named("p7", a.HubunganKontakDarurat),
	// 	sql.Named("p8", a.Provinsi),
	// 	sql.Named("p9", a.Kabupaten),
	// 	sql.Named("p10", a.Kecamatan),
	// 	sql.Named("p11", a.Kelurahan),
	// 	sql.Named("p12", a.KodePos),
	// 	sql.Named("p13", a.Phone),
	// 	sql.Named("p14", a.ProvinsiTempatKerja),
	// 	sql.Named("p15", a.KabupatenTempatKerja),
	// 	sql.Named("p16", a.KecamatanTempatKerja),
	// 	sql.Named("p17", a.KelurahanTempatKerja),
	// 	sql.Named("p18", a.KodePosTempatKerja),
	// 	sql.Named("p19", a.KodePekerjaan),
	// 	sql.Named("p20", a.NamaTempatKerja),
	// 	sql.Named("p21", a.KodeBidangUsaha),
	// 	sql.Named("p22", a.PenghasilanKotorPerTahun),
	// 	sql.Named("p23", a.KodeSumberPenghasilan),
	// 	sql.Named("p24", a.MaritalStatus),
	// 	sql.Named("p25", a.JumlahTanggungan),
	// 	sql.Named("p26", a.NoIdentitasPasangan),
	// 	sql.Named("p27", a.NamaPasangan),
	// 	sql.Named("p28", a.TanggalLahirPasangan),
	// 	sql.Named("p29", a.PisahHarta),
	// 	sql.Named("p30", a.AreaID),
	// 	sql.Named("p31", a.AreaIDTempatKerja),
	// 	sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
	// 	sql.Named("date", companyDate),
	// )

	MemberDetailQuery := `INSERT INTO MemberDetail (
		[ID]
		,[MemberID]
		,[NamaLengkapTanpaSingkatan]
		,[StatusPendidikan]
		,[NamaIbuKandung]
		,[NamaKontakDarurat]
		,[NomorKontakDarurat]
		,[HubunganKontakDarurat]
		,[Provinsi]
		,[Kabupaten]
		,[Kecamatan]
		,[Kelurahan]
		,[Kodepos]
		,[Phone]
		,[PenghasilanKotorPerTahun]
		,[KodeSumberPenghasilan]
		,[MaritalStatus]
		,[JumlahTanggungan]
		,[NoIdentitasPasangan]
		,[NamaPasangan]
		,[PisahHarta]
		,[AreaID]
		,[AreaIDTempatKerja]
		,[UserInsert]
		,[DateInsert]
		,[UserUpdate]
		,[DateUpdate]
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10,
			@p11, @p12, @p13, @p22, @p23, @p24, @p25, @p26, @p27, @p29, @p30,
			@p31, @userInsert, @date, null, null)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberDetailQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.NamaLengkapTanpaSingkatan),
		sql.Named("p3", a.StatusPendidikan),
		sql.Named("p4", a.NamaIbuKandung),
		sql.Named("p5", a.NamaKontakDarurat),
		sql.Named("p6", a.NomorKontakDarurat),
		sql.Named("p7", a.HubunganKontakDarurat),
		sql.Named("p8", a.Provinsi),
		sql.Named("p9", a.Kabupaten),
		sql.Named("p10", a.Kecamatan),
		sql.Named("p11", a.Kelurahan),
		sql.Named("p12", a.KodePos),
		sql.Named("p13", a.Phone),
		//sql.Named("p14", a.ProvinsiTempatKerja),
		//sql.Named("p15", a.KabupatenTempatKerja),
		//sql.Named("p16", a.KecamatanTempatKerja),
		//sql.Named("p17", a.KelurahanTempatKerja),
		//sql.Named("p18", a.KodePosTempatKerja),
		//sql.Named("p19", a.KodePekerjaan),
		//sql.Named("p20", a.NamaTempatKerja),
		//sql.Named("p21", a.KodeBidangUsaha),
		sql.Named("p22", a.PenghasilanKotorPerTahun),
		sql.Named("p23", a.KodeSumberPenghasilan),
		sql.Named("p24", a.MaritalStatus),
		sql.Named("p25", a.JumlahTanggungan),
		sql.Named("p26", a.NoIdentitasPasangan),
		sql.Named("p27", a.NamaPasangan),
		//sql.Named("p28", a.TanggalLahirPasangan),
		sql.Named("p29", a.PisahHarta),
		sql.Named("p30", a.AreaID),
		sql.Named("p31", a.AreaIDTempatKerja),
		sql.Named("userInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMemberRepository) Store(ctx context.Context, a *models.Member) (*models.Member, error) {
	a.ID = guid.New().StringUpper()
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
	a.UserInsert = ctx.Value(service.CtxUserID).(string)
	MemberQuery := `INSERT INTO Member (
		id, 
		code, 
		MemberName, 
		EmailAddress, 
		LoginPIN, 
		MemberType,
		Gender,
		Nationality,
		MaritalStatus,
		ChildrenCount,
		BankID,
		PendidikanTerakhir,
		RecordStatus,
		IdentityType,
		IsLocked,
		Phone,
		Handphone,
		VirtualAccountNo,
		ProfileImage,
		CategoryID,
		IdentityNumber,
		AlamatRumah,
		BirthPlace,
		BirthDate,
		MotherMaiden,
		GoldStatus,
		IdentityUpload,
		NPWPUpload,
		NPWPNumber,
		VerificationCodeEmail,
		FotoKTPDiriUpload,
		UserInsert,
		DateInsert
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, 
		@p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25,
		@p26,@p27,@p28,@p29,@p30, @userInsert, GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code.String),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.Gender),
		sql.Named("p7", a.Nationality),
		sql.Named("p8", a.MaritalStatus),
		sql.Named("p9", 0),
		sql.Named("p10", a.BankID),
		sql.Named("p11", 1),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.IdentityType),
		sql.Named("p14", a.IsLocked),
		sql.Named("p15", a.Phone),
		sql.Named("p16", a.Handphone),
		sql.Named("p17", a.VirtualAccountNo),
		sql.Named("p18", a.ProfileImage),
		sql.Named("p19", a.CategoryID),
		sql.Named("p20", a.IdentityNumber.String),
		sql.Named("p21", a.Address),
		sql.Named("p22", a.BirthPlace),
		sql.Named("p23", a.BirthDate),
		sql.Named("p24", a.MotherMaiden),
		sql.Named("p25", a.GoldStatus),
		sql.Named("p26", a.IdentityUpload),
		sql.Named("p27", a.NPWPUpload),
		sql.Named("p28", a.NoNPWP),
		sql.Named("p29", a.VerificationCodeEmail),
		sql.Named("p30", a.FotoKTPDiriUpload),
		sql.Named("userInsert", a.UserInsert),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMemberRepository) StoreMemberBranch(ctx context.Context, a *models.MemberBranch) (*models.MemberBranch, error) {
	a.ID = guid.New().StringUpper()
	MemberQuery := `INSERT INTO MemberBranch (
		ID, 
		MemberID,
		BusinessType,
		CompanyName,
		BankID,
		CompanyAddress,
		Province,
		City
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.BusinessType),
		sql.Named("p3", a.CompanyName),
		sql.Named("p4", a.BankID),
		sql.Named("p5", a.CompanyAddress),
		sql.Named("p6", a.Province),
		sql.Named("p7", a.City),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlMemberRepository) StoreMemberLogin(ctx context.Context, a *models.MemberLogin) (*models.MemberLogin, error) {
	a.ID = guid.New().StringUpper()
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPass), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	a.LoginPass = string(hashedPassword)
	MemberQuery := `INSERT INTO MemberLogin (
		ID, 
		MemberBranchID,
		EmailAddress,
		LoginPass,
		RecordStatus
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberBranchID),
		sql.Named("p2", a.EmailAddress),
		sql.Named("p3", a.LoginPass),
		sql.Named("p4", a.RecordStatus),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberRepository) Update(ctx context.Context, a *models.Member) (*models.Member, error) {
	var param []string
	MemberQuery := `Update Member Set `
	if a.Code.Valid == true {
		param = append(param, "Code = @p1")
	}
	if a.MemberName.Valid == true {
		param = append(param, "MemberName = @p2")
	}
	if a.Email.Valid == true {
		param = append(param, "EmailAddress = @p3")
	}
	if a.LoginPIN.Valid == true {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
		param = append(param, "LoginPIN = @p4")
	}
	if a.MemberType != -1 {
		param = append(param, "MemberType = @p5")
	}
	if a.IdentityType != -1 {
		param = append(param, "IdentityType = @p6")
	}
	if a.Gender != -1 {
		param = append(param, "Gender = @p7")
	}
	if a.Nationality != -1 {
		param = append(param, "Nationality = @p8")
	}
	if a.MaritalStatus != -1 {
		param = append(param, "MaritalStatus = @p9")
	}
	if a.RecordStatus != -1 {
		param = append(param, "RecordStatus = @p10")
	}
	if a.Phone.Valid {
		param = append(param, "Phone = @p11")
	}
	if a.Handphone.Valid {
		param = append(param, "Handphone = @p12")
	}
	if a.VirtualAccountNo.Valid {
		param = append(param, "VirtualAccountNo = @p13")
	}
	if service.IsValidGuid(a.BankID) {
		param = append(param, "BankID = @p14")
	}
	if a.ProfileImage.Valid {
		param = append(param, "ProfileImage = @p15")
	}
	if service.IsValidGuid(a.CategoryID.String) {
		param = append(param, "CategoryID = @p16")
	}
	if service.IsValidGuid(a.PlayerID.String) {
		param = append(param, "PlayerID = @p17")
	}
	if a.IdentityNumber.Valid {
		param = append(param, "IdentityNumber = @p18")
	}
	if a.Address.Valid {
		param = append(param, "AlamatRumah = @p19")
	}
	if a.City.Valid {
		param = append(param, "Kota = @p20")
	}
	if a.Province.Valid {
		param = append(param, "Provinsi = @p21")
	}
	if a.GoldStatus != -1 {
		param = append(param, "GoldStatus = @p22")
	}
	if a.IsLocked.Valid {
		param = append(param, "IsLocked = @p23")
	}
	if a.NoNPWP.Valid {
		param = append(param, "NPWPNumber = @p24")
	}
	if a.MotherMaiden != "" {
		param = append(param, "MotherMaiden = @p25")
	}
	if a.RecordStatus == 9 {
		param = append(param, "UserAuthor = @userAuthor")
		param = append(param, "DateAuthor = GETDATE()")
	}
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	if ctx.Value(service.CtxIsAdmin).(bool) {
		param = append(param, "UserUpdate = @userUpdate")
		param = append(param, "DateUpdate = GETDATE()")
	}
	MemberQuery += strings.Join(param, ",")
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	a.UserUpdate = ctx.Value(service.CtxUserID).(string)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.IdentityType),
		sql.Named("p7", a.Gender),
		sql.Named("p8", a.Nationality),
		sql.Named("p9", a.MaritalStatus),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.Phone),
		sql.Named("p12", a.Handphone),
		sql.Named("p13", a.VirtualAccountNo),
		sql.Named("p14", a.BankID),
		sql.Named("p15", a.ProfileImage),
		sql.Named("p16", a.CategoryID),
		sql.Named("p17", a.PlayerID),
		sql.Named("p18", a.IdentityNumber),
		sql.Named("p19", a.Address),
		sql.Named("p20", a.City),
		sql.Named("p21", a.Province),
		sql.Named("p22", a.GoldStatus),
		sql.Named("p23", a.IsLocked),
		sql.Named("p24", a.NoNPWP),
		sql.Named("p25", a.MotherMaiden),
		sql.Named("userUpdate", a.UserUpdate),
		sql.Named("userAuthor", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlMemberRepository) UpdateBase(ctx context.Context, a *models.Member) (*models.Member, error) {
	var param []string
	MemberQuery := `Update Member Set `
	if a.Code.Valid == true {
		param = append(param, "Code = @p1")
	}
	if a.MemberName.Valid == true {
		param = append(param, "MemberName = @p2")
	}
	if a.Email.Valid == true {
		param = append(param, "EmailAddress = @p3")
	}
	if a.LoginPIN.Valid == true {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
		param = append(param, "LoginPIN = @p4")
	}
	if a.MemberType != -1 && a.MemberType != 0 {
		param = append(param, "MemberType = @p5")
	}
	if a.IdentityType != -1 && a.IdentityType != 0 {
		param = append(param, "IdentityType = @p6")
	}
	if a.Gender != -1 && a.Gender != 0 {
		param = append(param, "Gender = @p7")
	}
	if a.Nationality != -1 && a.Nationality != 0 {
		param = append(param, "Nationality = @p8")
	}
	if a.MaritalStatus != -1 && a.MaritalStatus != 0 {
		param = append(param, "MaritalStatus = @p9")
	}
	if a.RecordStatus != -1 && a.RecordStatus != 0 {
		param = append(param, "RecordStatus = @p10")
	}
	if a.Phone.Valid {
		param = append(param, "Phone = @p11")
	}
	if a.Handphone.Valid {
		param = append(param, "Handphone = @p12")
	}
	if a.VirtualAccountNo.Valid {
		param = append(param, "VirtualAccountNo = @p13")
	}
	if service.IsValidGuid(a.BankID) {
		param = append(param, "BankID = @p14")
	}
	if a.ProfileImage.Valid {
		param = append(param, "ProfileImage = @p15")
	}
	if service.IsValidGuid(a.CategoryID.String) {
		param = append(param, "CategoryID = @p16")
	}
	if service.IsValidGuid(a.PlayerID.String) {
		param = append(param, "PlayerID = @p17")
	}
	if a.IdentityNumber.Valid {
		param = append(param, "IdentityNumber = @p18")
	}
	if a.Address.Valid {
		param = append(param, "AlamatRumah = @p19")
	}
	if a.City.Valid {
		param = append(param, "Kota = @p20")
	}
	if a.Province.Valid {
		param = append(param, "Provinsi = @p21")
	}
	if a.GoldStatus != -1 && a.GoldStatus != 0 {
		param = append(param, "GoldStatus = @p22")
	}
	if a.IsLocked.Valid {
		param = append(param, "IsLocked = @p23")
	}
	if a.NoPaspor.Valid {
		param = append(param, "NoPaspor = @p24")
	}
	if a.NoAkteLahir.Valid {
		param = append(param, "NoAkteLahir = @p25")
	}
	if a.SIUP.Valid {
		param = append(param, "SIUP = @p26")
	}
	if a.TDP.Valid {
		param = append(param, "TDP = @p27")
	}
	if a.Agama.Valid {
		param = append(param, "Agama = @p28")
	}
	if a.Akte.Valid {
		param = append(param, "Akte = @p29")
	}
	if a.JumlahTagungan.Valid {
		param = append(param, "JumlahTagungan = @p30")
	}
	if a.PendapatanPerbulan.Valid {
		param = append(param, "PendapatanPerbulan = @p31")
	}
	if a.NamaPasangan.Valid {
		param = append(param, "NamaPasangan = @p32")
	}
	if a.NoIdentitasPasangan.Valid {
		param = append(param, "NoIdentitasPasangan = @p33")
	}
	if a.RTRW.Valid {
		param = append(param, "RTRW = @p34")
	}
	if a.Kelurahan.Valid {
		param = append(param, "Kelurahan = @p35")
	}
	if a.KodePos.Valid {
		param = append(param, "KodePos = @p36")
	}
	if a.MotherMaiden != "" {
		param = append(param, "MotherMaiden = @p37")
	}
	if a.Kecamatan.Valid {
		param = append(param, "Kecamatan = @p38")
	}
	if a.NoNPWP.Valid {
		param = append(param, "NoNPWP = @p39")
	}
	if a.FileNameFotoKTP.Valid {
		param = append(param, "FileNameFotoKTP = @p40")
	}
	if a.FileNameFotoSelfie.Valid {
		param = append(param, "FileNameFotoSelfie = @p41")
	}
	if a.AlamatRumah2.Valid {
		param = append(param, "AlamatRumah2 = @p42")
	}
	if a.AlamatTempatKerja1.Valid {
		param = append(param, "AlamatTempatKerja1 = @p43")
	}
	if a.AlamatTempatKerja2.Valid {
		param = append(param, "AlamatTempatKerja2 = @p44")
	}
	if a.NoInduk.Valid {
		param = append(param, "NoInduk = @p45")
	}
	if a.BankAccountName.Valid {
		param = append(param, "BankAccountName = @p46")
	}
	if a.BankAccountNo.Valid {
		param = append(param, "BankAccountNo = @p47")
	}
	if a.BankCode.Valid {
		param = append(param, "BankCode = @p48")
	}
	if a.ReferralMemberID.Valid {
		param = append(param, "ReferralMemberID = @p49")
	}
	if a.PeruriEmailAddress.Valid {
		param = append(param, "PeruriEmailAddress = @p50")
	}
	if a.IsNeedVerificationPeruri.Valid {
		param = append(param, "IsNeedVerificationPeruri = @p51")
	}
	// if a.RecordStatus == 9 {
	// 	param = append(param, "UserAuthor = @userAuthor")
	// 	param = append(param, "DateAuthor = GETDATE()")
	// }
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	if ctx.Value(service.CtxIsAdmin).(bool) {
		param = append(param, "UserUpdate = @userUpdate")
		param = append(param, "DateUpdate = GETDATE()")
	}
	MemberQuery += strings.Join(param, ",")
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	a.UserUpdate = ctx.Value(service.CtxUserID).(string)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.IdentityType),
		sql.Named("p7", a.Gender),
		sql.Named("p8", a.Nationality),
		sql.Named("p9", a.MaritalStatus),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.Phone),
		sql.Named("p12", a.Handphone),
		sql.Named("p13", a.VirtualAccountNo),
		sql.Named("p14", a.BankID),
		sql.Named("p15", a.ProfileImage),
		sql.Named("p16", a.CategoryID),
		sql.Named("p17", a.PlayerID),
		sql.Named("p18", a.IdentityNumber),
		sql.Named("p19", a.Address),
		sql.Named("p20", a.City),
		sql.Named("p21", a.Province),
		sql.Named("p22", a.GoldStatus),
		sql.Named("p23", a.IsLocked),
		sql.Named("userUpdate", a.UserUpdate),
		sql.Named("userAuthor", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p24", a.NoPaspor),
		sql.Named("p25", a.NoAkteLahir),
		sql.Named("p26", a.SIUP),
		sql.Named("p27", a.TDP),
		sql.Named("p28", a.Agama),
		sql.Named("p29", a.Akte),
		sql.Named("p30", a.JumlahTagungan),
		sql.Named("p31", a.PendapatanPerbulan),
		sql.Named("p32", a.NamaPasangan),
		sql.Named("p33", a.NoIdentitasPasangan),
		sql.Named("p34", a.RTRW),
		sql.Named("p35", a.Kelurahan),
		sql.Named("p36", a.KodePos),
		sql.Named("p37", a.MotherMaiden),
		sql.Named("p38", a.Kecamatan),
		sql.Named("p39", a.NoNPWP),
		sql.Named("p40", a.FileNameFotoKTP),
		sql.Named("p41", a.FileNameFotoSelfie),
		sql.Named("p42", a.AlamatRumah2),
		sql.Named("p43", a.AlamatTempatKerja1),
		sql.Named("p44", a.AlamatTempatKerja2),
		sql.Named("p45", a.NoInduk),
		sql.Named("p46", a.BankAccountName),
		sql.Named("p47", a.BankAccountNo),
		sql.Named("p48", a.BankCode),
		sql.Named("p49", a.ReferralMemberID),
		sql.Named("p50", a.PeruriEmailAddress),
		sql.Named("p51", a.IsNeedVerificationPeruri),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberRepository) UpdateBaseByDate(ctx context.Context, a *models.Member, companyDate string) (*models.Member, error) {
	var param []string
	MemberQuery := `Update Member Set `
	if a.Code.Valid == true {
		param = append(param, "Code = @p1")
	}
	if a.MemberName.Valid == true {
		param = append(param, "MemberName = @p2")
	}
	if a.Email.Valid == true {
		param = append(param, "EmailAddress = @p3")
	}
	if a.LoginPIN.Valid == true {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.LoginPIN.String), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		a.LoginPIN = sql.NullString{String: string(hashedPassword), Valid: true}
		param = append(param, "LoginPIN = @p4")
	}
	if a.MemberType != -1 && a.MemberType != 0 {
		param = append(param, "MemberType = @p5")
	}
	if a.IdentityType != -1 && a.IdentityType != 0 {
		param = append(param, "IdentityType = @p6")
	}
	if a.Gender != -1 && a.Gender != 0 {
		param = append(param, "Gender = @p7")
	}
	if a.Nationality != -1 && a.Nationality != 0 {
		param = append(param, "Nationality = @p8")
	}
	if a.MaritalStatus != -1 && a.MaritalStatus != 0 {
		param = append(param, "MaritalStatus = @p9")
	}
	if a.RecordStatus != -1 && a.RecordStatus != 0 {
		param = append(param, "RecordStatus = @p10")
	}
	if a.Phone.Valid {
		param = append(param, "Phone = @p11")
	}
	if a.Handphone.Valid {
		param = append(param, "Handphone = @p12")
	}
	if a.VirtualAccountNo.Valid {
		param = append(param, "VirtualAccountNo = @p13")
	}
	if service.IsValidGuid(a.BankID) {
		param = append(param, "BankID = @p14")
	}
	if a.ProfileImage.Valid {
		param = append(param, "ProfileImage = @p15")
	}
	if service.IsValidGuid(a.CategoryID.String) {
		param = append(param, "CategoryID = @p16")
	}
	if service.IsValidGuid(a.PlayerID.String) {
		param = append(param, "PlayerID = @p17")
	}
	if a.IdentityNumber.Valid {
		param = append(param, "IdentityNumber = @p18")
	}
	if a.Address.Valid {
		param = append(param, "AlamatRumah = @p19")
	}
	if a.City.Valid {
		param = append(param, "Kota = @p20")
	}
	if a.Province.Valid {
		param = append(param, "Provinsi = @p21")
	}
	if a.GoldStatus != -1 && a.GoldStatus != 0 {
		param = append(param, "GoldStatus = @p22")
	}
	if a.IsLocked.Valid {
		param = append(param, "IsLocked = @p23")
	}
	if a.NoPaspor.Valid {
		param = append(param, "NoPaspor = @p24")
	}
	if a.NoAkteLahir.Valid {
		param = append(param, "NoAkteLahir = @p25")
	}
	if a.SIUP.Valid {
		param = append(param, "SIUP = @p26")
	}
	if a.TDP.Valid {
		param = append(param, "TDP = @p27")
	}
	if a.Agama.Valid {
		param = append(param, "Agama = @p28")
	}
	if a.Akte.Valid {
		param = append(param, "Akte = @p29")
	}
	if a.JumlahTagungan.Valid {
		param = append(param, "JumlahTagungan = @p30")
	}
	if a.PendapatanPerbulan.Valid {
		param = append(param, "PendapatanPerbulan = @p31")
	}
	if a.NamaPasangan.Valid {
		param = append(param, "NamaPasangan = @p32")
	}
	if a.NoIdentitasPasangan.Valid {
		param = append(param, "NoIdentitasPasangan = @p33")
	}
	if a.RTRW.Valid {
		param = append(param, "RTRW = @p34")
	}
	if a.Kelurahan.Valid {
		param = append(param, "Kelurahan = @p35")
	}
	if a.KodePos.Valid {
		param = append(param, "KodePos = @p36")
	}
	if a.MotherMaiden != "" {
		param = append(param, "MotherMaiden = @p37")
	}
	if a.Kecamatan.Valid {
		param = append(param, "Kecamatan = @p38")
	}
	if a.NoNPWP.Valid {
		param = append(param, "NoNPWP = @p39")
	}
	if a.FileNameFotoKTP.Valid {
		param = append(param, "FileNameFotoKTP = @p40")
	}
	if a.FileNameFotoSelfie.Valid {
		param = append(param, "FileNameFotoSelfie = @p41")
	}
	if a.AlamatRumah2.Valid {
		param = append(param, "AlamatRumah2 = @p42")
	}
	if a.AlamatTempatKerja1.Valid {
		param = append(param, "AlamatTempatKerja1 = @p43")
	}
	if a.AlamatTempatKerja2.Valid {
		param = append(param, "AlamatTempatKerja2 = @p44")
	}
	if a.NoInduk.Valid {
		param = append(param, "NoInduk = @p45")
	}
	if a.BankAccountName.Valid {
		param = append(param, "BankAccountName = @p46")
	}
	if a.BankAccountNo.Valid {
		param = append(param, "BankAccountNo = @p47")
	}
	if a.BankCode.Valid {
		param = append(param, "BankCode = @p48")
	}
	if a.ReferralMemberID.Valid {
		param = append(param, "ReferralMemberID = @p49")
	}
	if a.PeruriEmailAddress.Valid {
		param = append(param, "PeruriEmailAddress = @p50")
	}
	if a.IsNeedVerificationPeruri.Valid {
		param = append(param, "IsNeedVerificationPeruri = @p51")
	}
	// if a.RecordStatus == 9 {
	// 	param = append(param, "UserAuthor = @userAuthor")
	// 	param = append(param, "DateAuthor = GETDATE()")
	// }
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	if ctx.Value(service.CtxIsAdmin).(bool) {
		param = append(param, "UserUpdate = @userUpdate")
		param = append(param, "DateUpdate = @date")
	}
	MemberQuery += strings.Join(param, ",")
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	a.UserUpdate = ctx.Value(service.CtxUserID).(string)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberName),
		sql.Named("p3", a.Email),
		sql.Named("p4", a.LoginPIN),
		sql.Named("p5", a.MemberType),
		sql.Named("p6", a.IdentityType),
		sql.Named("p7", a.Gender),
		sql.Named("p8", a.Nationality),
		sql.Named("p9", a.MaritalStatus),
		sql.Named("p10", a.RecordStatus),
		sql.Named("p11", a.Phone),
		sql.Named("p12", a.Handphone),
		sql.Named("p13", a.VirtualAccountNo),
		sql.Named("p14", a.BankID),
		sql.Named("p15", a.ProfileImage),
		sql.Named("p16", a.CategoryID),
		sql.Named("p17", a.PlayerID),
		sql.Named("p18", a.IdentityNumber),
		sql.Named("p19", a.Address),
		sql.Named("p20", a.City),
		sql.Named("p21", a.Province),
		sql.Named("p22", a.GoldStatus),
		sql.Named("p23", a.IsLocked),
		sql.Named("userUpdate", a.UserUpdate),
		sql.Named("userAuthor", ctx.Value(service.CtxUserID).(string)),
		sql.Named("p24", a.NoPaspor),
		sql.Named("p25", a.NoAkteLahir),
		sql.Named("p26", a.SIUP),
		sql.Named("p27", a.TDP),
		sql.Named("p28", a.Agama),
		sql.Named("p29", a.Akte),
		sql.Named("p30", a.JumlahTagungan),
		sql.Named("p31", a.PendapatanPerbulan),
		sql.Named("p32", a.NamaPasangan),
		sql.Named("p33", a.NoIdentitasPasangan),
		sql.Named("p34", a.RTRW),
		sql.Named("p35", a.Kelurahan),
		sql.Named("p36", a.KodePos),
		sql.Named("p37", a.MotherMaiden),
		sql.Named("p38", a.Kecamatan),
		sql.Named("p39", a.NoNPWP),
		sql.Named("p40", a.FileNameFotoKTP),
		sql.Named("p41", a.FileNameFotoSelfie),
		sql.Named("p42", a.AlamatRumah2),
		sql.Named("p43", a.AlamatTempatKerja1),
		sql.Named("p44", a.AlamatTempatKerja2),
		sql.Named("p45", a.NoInduk),
		sql.Named("p46", a.BankAccountName),
		sql.Named("p47", a.BankAccountNo),
		sql.Named("p48", a.BankCode),
		sql.Named("p49", a.ReferralMemberID),
		sql.Named("p50", a.PeruriEmailAddress),
		sql.Named("p51", a.IsNeedVerificationPeruri),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlMemberRepository) UpdateForSignedDocument(ctx context.Context, fileName *string, id *string) (bool, error) {
	MemberQuery := `Update Member Set IdentityUpload = @fileName, DateUpdate = GETDATE() Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("filename", fmt.Sprintf("%v", fileName)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)

	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", id),
		sql.Named("fileName", fileName),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlMemberRepository) UpdateMemberDetail(ctx context.Context, a *models.MemberDetail) (*models.MemberDetail, error) {
	var param []string
	MemberDetailQuery := `Update MemberDetail Set `
	if a.NamaLengkapTanpaSingkatan != "" {
		param = append(param, "NamaLengkapTanpaSingkatan = @p2")
	}
	if a.StatusPendidikan != "" {
		param = append(param, "StatusPendidikan = @p3")
	}
	if a.NamaIbuKandung != "" {
		param = append(param, "NamaIbuKandung = @p4")
	}
	if a.NamaKontakDarurat != "" {
		param = append(param, "NamaKontakDarurat = @p5")
	}
	if a.NomorKontakDarurat != "" {
		param = append(param, "NomorKontakDarurat = @p6")
	}
	if a.HubunganKontakDarurat != "" {
		param = append(param, "HubunganKontakDarurat = @p7")
	}
	if a.Provinsi != "" {
		param = append(param, "Provinsi = @p8")
	}
	if a.Kabupaten != "" {
		param = append(param, "Kabupaten = @p9")
	}
	if a.Kecamatan != "" {
		param = append(param, "Kecamatan = @p10")
	}
	if a.Kelurahan != "" {
		param = append(param, "Kelurahan = @p11")
	}
	if a.KodePos != "" {
		param = append(param, "KodePos = @p12")
	}
	if a.Phone != "" {
		param = append(param, "Phone = @p13")
	}
	if a.ProvinsiTempatKerja != "" {
		param = append(param, "ProvinsiTempatKerja = @p14")
	}
	if a.KabupatenTempatKerja != "" {
		param = append(param, "KabupatenTempatKerja = @p15")
	}
	if a.KecamatanTempatKerja != "" {
		param = append(param, "KecamatanTempatKerja = @p16")
	}
	if a.KelurahanTempatKerja != "" {
		param = append(param, "KelurahanTempatKerja = @p17")
	}
	if a.KodePosTempatKerja != "" {
		param = append(param, "KodePosTempatKerja = @p18")
	}
	if a.KodePekerjaan != "" {
		param = append(param, "KodePekerjaan = @p19")
	}
	if a.NamaTempatKerja != "" {
		param = append(param, "NamaTempatKerja = @p20")
	}
	if a.KodeBidangUsaha != "" {
		param = append(param, "KodeBidangUsaha = @p21")
	}
	if a.PenghasilanKotorPerTahun != "" {
		param = append(param, "PenghasilanKotorPerTahun = @p22")
	}
	if a.KodeSumberPenghasilan != "" {
		param = append(param, "KodeSumberPenghasilan = @p23")
	}
	if a.MaritalStatus != "" {
		param = append(param, "MaritalStatus = @p24")
	}
	if a.JumlahTanggungan != "" {
		param = append(param, "JumlahTanggungan = @p25")
	}
	if a.NoIdentitasPasangan != "" {
		param = append(param, "NoIdentitasPasangan = @p26")
	}
	if a.NamaPasangan != "" {
		param = append(param, "NamaPasangan = @p27")
	}
	if a.TanggalLahirPasangan != "" {
		param = append(param, "TanggalLahirPasangan = @p28")
	}
	if a.PisahHarta != "" {
		param = append(param, "PisahHarta = @p29")
	}
	if a.AreaID != "" {
		param = append(param, "AreaID = @p30")
	}
	if a.AreaIDTempatKerja != "" {
		param = append(param, "AreaIDTempatKerja = @p31")
	}
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	//if ctx.Value(service.CtxUserID) != nil {
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = GETDATE()")
	//}
	MemberDetailQuery += strings.Join(param, ",")
	MemberDetailQuery += ` Where MemberID = @p1`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberDetailQuery,
		//sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.NamaLengkapTanpaSingkatan),
		sql.Named("p3", a.StatusPendidikan),
		sql.Named("p4", a.NamaIbuKandung),
		sql.Named("p5", a.NamaKontakDarurat),
		sql.Named("p6", a.NomorKontakDarurat),
		sql.Named("p7", a.HubunganKontakDarurat),
		sql.Named("p8", a.Provinsi),
		sql.Named("p9", a.Kabupaten),
		sql.Named("p10", a.Kecamatan),
		sql.Named("p11", a.Kelurahan),
		sql.Named("p12", a.KodePos),
		sql.Named("p13", a.Phone),
		sql.Named("p14", a.ProvinsiTempatKerja),
		sql.Named("p15", a.KabupatenTempatKerja),
		sql.Named("p16", a.KecamatanTempatKerja),
		sql.Named("p17", a.KelurahanTempatKerja),
		sql.Named("p18", a.KodePosTempatKerja),
		sql.Named("p19", a.KodePekerjaan),
		sql.Named("p20", a.NamaTempatKerja),
		sql.Named("p21", a.KodeBidangUsaha),
		sql.Named("p22", a.PenghasilanKotorPerTahun),
		sql.Named("p23", a.KodeSumberPenghasilan),
		sql.Named("p24", a.MaritalStatus),
		sql.Named("p25", a.JumlahTanggungan),
		sql.Named("p26", a.NoIdentitasPasangan),
		sql.Named("p27", a.NamaPasangan),
		sql.Named("p28", a.TanggalLahirPasangan),
		sql.Named("p29", a.PisahHarta),
		sql.Named("p30", a.AreaID),
		sql.Named("p31", a.AreaIDTempatKerja),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberRepository) UpdateMemberDetailByDate(ctx context.Context, a *models.MemberDetail, companyDate string) (*models.MemberDetail, error) {
	var param []string
	MemberDetailQuery := `Update MemberDetail Set `
	if a.NamaLengkapTanpaSingkatan != "" {
		param = append(param, "NamaLengkapTanpaSingkatan = @p2")
	}
	if a.StatusPendidikan != "" {
		param = append(param, "StatusPendidikan = @p3")
	}
	if a.NamaIbuKandung != "" {
		param = append(param, "NamaIbuKandung = @p4")
	}
	if a.NamaKontakDarurat != "" {
		param = append(param, "NamaKontakDarurat = @p5")
	}
	if a.NomorKontakDarurat != "" {
		param = append(param, "NomorKontakDarurat = @p6")
	}
	if a.HubunganKontakDarurat != "" {
		param = append(param, "HubunganKontakDarurat = @p7")
	}
	if a.Provinsi != "" {
		param = append(param, "Provinsi = @p8")
	}
	if a.Kabupaten != "" {
		param = append(param, "Kabupaten = @p9")
	}
	if a.Kecamatan != "" {
		param = append(param, "Kecamatan = @p10")
	}
	if a.Kelurahan != "" {
		param = append(param, "Kelurahan = @p11")
	}
	if a.KodePos != "" {
		param = append(param, "KodePos = @p12")
	}
	if a.Phone != "" {
		param = append(param, "Phone = @p13")
	}
	if a.ProvinsiTempatKerja != "" {
		param = append(param, "ProvinsiTempatKerja = @p14")
	}
	if a.KabupatenTempatKerja != "" {
		param = append(param, "KabupatenTempatKerja = @p15")
	}
	if a.KecamatanTempatKerja != "" {
		param = append(param, "KecamatanTempatKerja = @p16")
	}
	if a.KelurahanTempatKerja != "" {
		param = append(param, "KelurahanTempatKerja = @p17")
	}
	if a.KodePosTempatKerja != "" {
		param = append(param, "KodePosTempatKerja = @p18")
	}
	if a.KodePekerjaan != "" {
		param = append(param, "KodePekerjaan = @p19")
	}
	if a.NamaTempatKerja != "" {
		param = append(param, "NamaTempatKerja = @p20")
	}
	if a.KodeBidangUsaha != "" {
		param = append(param, "KodeBidangUsaha = @p21")
	}
	if a.PenghasilanKotorPerTahun != "" {
		param = append(param, "PenghasilanKotorPerTahun = @p22")
	}
	if a.KodeSumberPenghasilan != "" {
		param = append(param, "KodeSumberPenghasilan = @p23")
	}
	if a.MaritalStatus != "" {
		param = append(param, "MaritalStatus = @p24")
	}
	if a.JumlahTanggungan != "" {
		param = append(param, "JumlahTanggungan = @p25")
	}
	if a.NoIdentitasPasangan != "" {
		param = append(param, "NoIdentitasPasangan = @p26")
	}
	if a.NamaPasangan != "" {
		param = append(param, "NamaPasangan = @p27")
	}
	if a.TanggalLahirPasangan != "" {
		param = append(param, "TanggalLahirPasangan = @p28")
	}
	if a.PisahHarta != "" {
		param = append(param, "PisahHarta = @p29")
	}
	if a.AreaID != "" {
		param = append(param, "AreaID = @p30")
	}
	if a.AreaIDTempatKerja != "" {
		param = append(param, "AreaIDTempatKerja = @p31")
	}
	if len(param) == 0 {
		return nil, fmt.Errorf("No Parameter to update")
	}
	//if ctx.Value(service.CtxUserID) != nil {
	param = append(param, "UserUpdate = @userUpdate")
	param = append(param, "DateUpdate = @date")
	//}
	MemberDetailQuery += strings.Join(param, ",")
	MemberDetailQuery += ` Where MemberID = @p1`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberDetailQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberDetailQuery,
		//sql.Named("p0", a.ID),
		sql.Named("p1", a.MemberID),
		sql.Named("p2", a.NamaLengkapTanpaSingkatan),
		sql.Named("p3", a.StatusPendidikan),
		sql.Named("p4", a.NamaIbuKandung),
		sql.Named("p5", a.NamaKontakDarurat),
		sql.Named("p6", a.NomorKontakDarurat),
		sql.Named("p7", a.HubunganKontakDarurat),
		sql.Named("p8", a.Provinsi),
		sql.Named("p9", a.Kabupaten),
		sql.Named("p10", a.Kecamatan),
		sql.Named("p11", a.Kelurahan),
		sql.Named("p12", a.KodePos),
		sql.Named("p13", a.Phone),
		sql.Named("p14", a.ProvinsiTempatKerja),
		sql.Named("p15", a.KabupatenTempatKerja),
		sql.Named("p16", a.KecamatanTempatKerja),
		sql.Named("p17", a.KelurahanTempatKerja),
		sql.Named("p18", a.KodePosTempatKerja),
		sql.Named("p19", a.KodePekerjaan),
		sql.Named("p20", a.NamaTempatKerja),
		sql.Named("p21", a.KodeBidangUsaha),
		sql.Named("p22", a.PenghasilanKotorPerTahun),
		sql.Named("p23", a.KodeSumberPenghasilan),
		sql.Named("p24", a.MaritalStatus),
		sql.Named("p25", a.JumlahTanggungan),
		sql.Named("p26", a.NoIdentitasPasangan),
		sql.Named("p27", a.NamaPasangan),
		sql.Named("p28", a.TanggalLahirPasangan),
		sql.Named("p29", a.PisahHarta),
		sql.Named("p30", a.AreaID),
		sql.Named("p31", a.AreaIDTempatKerja),
		sql.Named("userUpdate", ctx.Value(service.CtxUserID).(string)),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}

func (sj *sqlMemberRepository) LockAccount(ctx context.Context, memberID *string) (bool, error) {
	MemberQuery := `Update Member Set 
		IsLocked = 1,
		RecordStatus = 4
	Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", memberID),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) SendVerification(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage, *verificationCode)
	messageLog := fmt.Sprintf(*smsMessage, *verificationCode)
	message = url.QueryEscape(message)

	payload := map[string]interface{}{
		"sender":           "Zenziva-Reguler",
		"smsMessage":       smsMessage,
		"phone":            phone,
		"verificationCode": verificationCode,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), &messageLog, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendOTP(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendVerificationConsole(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage, *verificationCode)
	messageLog := fmt.Sprintf(*smsMessage, *verificationCode)

	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":           "Zenziva-Reguler",
		"smsMessage":       smsMessage,
		"phone":            phone,
		"verificationCode": verificationCode,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), &messageLog, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendOTPConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendMessage(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage)
	messageLog := fmt.Sprintf(*smsMessage)
	message = url.QueryEscape(message)

	payload := map[string]interface{}{
		"sender":     "Zenziva-Reguler",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), &messageLog, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendOTP(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendMessageConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage)
	messageLog := fmt.Sprintf(*smsMessage)

	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":     "Zenziva-Reguler",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), &messageLog, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendOTPConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) RegularSendVerification(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage, *verificationCode)
	message = url.QueryEscape(message)
	payload := map[string]interface{}{
		"sender":           "Zenziva-Reguler",
		"smsMessage":       smsMessage,
		"phone":            phone,
		"verificationCode": verificationCode,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}
	isSendSuccess, err := smsService.RegularSendVerificationCode(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}
	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) RegularSendVerificationConsole(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage, *verificationCode)

	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":           "Zenziva-Reguler",
		"smsMessage":       smsMessage,
		"phone":            phone,
		"verificationCode": verificationCode,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}
	isSendSuccess, err := smsService.RegularSendVerificationCodeConsole(message, *phone)
	//isSendSuccess, err := smsService.RegularSendOTP(*verificationCode, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}
	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) RegularSendVerificationConsoleWA(ctx context.Context, memberId *string, phone *string, verificationCode *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()
	message := fmt.Sprintf(*smsMessage, *verificationCode)

	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":           "Zenziva-Reguler",
		"smsMessage":       smsMessage,
		"phone":            phone,
		"verificationCode": verificationCode,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}
	isSendSuccess, err := smsService.RegularSendWhatsAppConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}
	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) SendSMSNexmo(ctx context.Context, memberId *string, phone *string, smsMessage *string, apikey *string, apisecret *string) (bool, error) {
	smsService := service.NewSMSService()

	payload := map[string]interface{}{
		"sender":     "Nexmo",
		"smsMessage": smsMessage,
		"phone":      phone,
		"apikey":     apikey,
		"apisecret":  apisecret,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, resp, err := smsService.SendVerificationCodeNexmo(*smsMessage, *phone, *apikey, *apisecret)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, resp, "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, resp, "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendEmailVerification(ctx context.Context, memberId *string, email *string, emailMessage *string, emailSubject *string) (bool, error) {
	emailService := service.NewEmailService()

	payload := map[string]interface{}{
		"emailMessage": emailMessage,
		"email":        email,
		"emailSubject": emailSubject,
		"cc":           "",
		"bcc":          "",
	}
	payloadStr, err := json.Marshal(payload)

	emailId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, emailId, memberId, string(payloadStr), emailMessage, 2, email)
	if isInsertLog == false {
		return false, err
	}

	//*email = "riyan10dec2@gmail.com"
	isSendSuccess, err := emailService.SendVerificationCode(*emailMessage, *email, *emailSubject)
	if isSendSuccess == false {
		isUpdateLog, errlog := sj.UpdateSMSEmailLog(ctx, emailId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, errlog
		}
		return false, err
	}

	isUpdateLog, errlog := sj.UpdateSMSEmailLog(ctx, emailId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, errlog
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendEmailVerificationWithCC(ctx context.Context, memberId *string, email *string, emailMessage *string, emailSubject *string, cc *string, bcc *string) (bool, error) {
	emailService := service.NewEmailService()

	payload := map[string]interface{}{
		"emailMessage": emailMessage,
		"email":        email,
		"emailSubject": emailSubject,
		"cc":           cc,
		"bcc":          bcc,
	}
	payloadStr, err := json.Marshal(payload)

	emailId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, emailId, memberId, string(payloadStr), emailMessage, 2, email)
	if isInsertLog == false {
		return false, err
	}

	//*email = "riyan10dec2@gmail.com"
	isSendSuccess, err := emailService.SendVerificationCodeWithCC(*emailMessage, *email, *emailSubject, *cc, *bcc)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, emailId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, emailId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}

	return true, nil
}
func (sj *sqlMemberRepository) SendSMSSuccess(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()

	message := url.QueryEscape(*smsMessage)

	payload := map[string]interface{}{
		"sender":     "Regular-Zenziva",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendVerificationCode(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) SendSMSSuccessConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()

	message := *smsMessage
	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":     "Regular-Zenziva",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.SendVerificationCodeConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) RegularSendSMSSuccess(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()

	message := url.QueryEscape(*smsMessage)

	payload := map[string]interface{}{
		"sender":     "Regular-Zenziva",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.RegularSendVerificationCode(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) RegularSendSMSSuccessConsole(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()

	message := *smsMessage
	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":     "Regular-Zenziva",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.RegularSendVerificationCodeConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) RegularSendSMSSuccessConsoleWA(ctx context.Context, memberId *string, phone *string, smsMessage *string) (bool, error) {
	smsService := service.NewSMSService()

	message := *smsMessage
	regex, err := regexp.Compile("\n")
	if err != nil {
		return false, err
	}
	message = regex.ReplaceAllString(message, "")

	payload := map[string]interface{}{
		"sender":     "Regular-Zenziva",
		"smsMessage": smsMessage,
		"phone":      phone,
	}
	payloadStr, err := json.Marshal(payload)

	smsId := guid.New().StringUpper()

	isInsertLog, err := sj.InsertSMSEmailLog(ctx, smsId, memberId, string(payloadStr), smsMessage, 1, phone)
	if isInsertLog == false {
		return false, err
	}

	isSendSuccess, err := smsService.RegularSendWhatsAppConsole(message, *phone)
	if isSendSuccess == false {
		isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, err.Error(), "500")
		if isUpdateLog == false {
			return false, err
		}
		return false, err
	}

	isUpdateLog, err := sj.UpdateSMSEmailLog(ctx, smsId, memberId, "Delivered", "00")
	if isUpdateLog == false {
		return false, err
	}
	return true, nil
}
func (sj *sqlMemberRepository) UpdateVerificationCode(ctx context.Context, key string, verificationCode string) (bool, error) {
	//Change Redis Here
	//err := sj.Redis.Set(key, verificationCode, time.Hour*24).Err()
	err := sj.Redis.Set(ctx, key, verificationCode, time.Hour*24).Err()
	if err != nil {
		logger := service.Logger(ctx)
		logger.Error("Set Redis Error",
			zap.String("error", err.Error()),
		)
		return false, fmt.Errorf("Gagal menyimpan token. Silahkan coba kembali.")
	}
	return true, nil
}
func (sj *sqlMemberRepository) GetVerificationCode(ctx context.Context, key string, verificationCode string, tokenType string) (bool, error) {
	//Change Redis Here
	//code, err := sj.Redis.Get(key).Result()
	code, err := sj.Redis.Get(ctx, key).Result()
	if err != nil {
		return false, fmt.Errorf("Token sudah expired (tidak berlaku)")
	}
	if verificationCode != code {
		if tokenType == "sms" {
			return false, fmt.Errorf("Token SMS Salah")
		} else {
			return false, fmt.Errorf("Token Email Salah")
		}
	}
	return true, nil
}
func (sj *sqlMemberRepository) GetCount(ctx context.Context, key string) (int, error) {
	//Change Redis Here
	//countCMD := sj.Redis2.Get(key)
	countCMD := sj.Redis2.Get(ctx, key)
	count, err := countCMD.Int()
	if err != nil {
		return 0, err
	}
	return count, nil
}
func (sj *sqlMemberRepository) DeleteCount(ctx context.Context, key string) (bool, error) {
	//Change Redis Here
	//sj.Redis2.Del(key).Result()
	sj.Redis2.Del(ctx, key).Result()
	return true, nil
}
func (sj *sqlMemberRepository) DeleteToken(ctx context.Context, key string) (bool, error) {
	//Change Redis Here
	//sj.Redis0.Del(key).Result()
	sj.Redis0.Del(ctx, key).Result()
	return true, nil
}
func (sj *sqlMemberRepository) SetCount(ctx context.Context, key string, count int) (bool, error) {
	//Change Redis Here
	//sj.Redis2.Set(key, count, -1)
	sj.Redis2.Set(ctx, key, count, -1)
	return true, nil
}

func (sj *sqlMemberRepository) GenerateVerificationCode() string {
	var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	max := 4
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b)
}

func (sj *sqlMemberRepository) ChangePassword(ctx context.Context, memberID *string, password *string, isactivation *bool) (bool, error) {
	MemberQuery := `Update Member Set LoginPass = @p1`
	if *isactivation != false {
		MemberQuery += ` ,RecordStatus = 2, DateUpdate=GETDATE(), DateActivatedByMember=GETDATE(),UserUpdate=@p0`
	}
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
		//zap.String("password", fmt.Sprintf("%v", password)),
	)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(*password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}
	*password = string(hashedPassword)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", memberID),
		sql.Named("p1", password),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) ChangePasswordBase(ctx context.Context, memberID *string, password *string, isactivation *bool, isnewmember *bool) (bool, error) {
	MemberQuery := `Update Member Set LoginPass = @p1`
	if isactivation != nil && *isactivation == true {
		MemberQuery += ` ,RecordStatus = 11, DateUpdate=GETDATE(), UserUpdate=@p0`
	}
	if isnewmember != nil && *isnewmember == true {
		MemberQuery += ` ,RecordStatus = 12, DateUpdate=GETDATE(), UserUpdate=@p0`
	}
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
		//zap.String("password", fmt.Sprintf("%v", password)),
	)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(*password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}
	*password = string(hashedPassword)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", memberID),
		sql.Named("p1", password),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) ChangePasswordBaseByDate(ctx context.Context, memberID *string, password *string, isactivation *bool, isnewmember *bool, companyDate string) (bool, error) {
	MemberQuery := `Update Member Set LoginPass = @p1`
	if isactivation != nil && *isactivation == true {
		MemberQuery += ` ,RecordStatus = 11, DateUpdate=@date, UserUpdate=@p0`
	}
	if isnewmember != nil && *isnewmember == true {
		MemberQuery += ` ,RecordStatus = 12, DateUpdate=@date, UserUpdate=@p0`
	}
	MemberQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("memberID", fmt.Sprintf("%v", memberID)),
		//zap.String("password", fmt.Sprintf("%v", password)),
	)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(*password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}
	*password = string(hashedPassword)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", memberID),
		sql.Named("p1", password),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) CheckEmail(ctx context.Context, email *string) (bool, error) {
	var valid = false
	j := new(models.Member)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID
	FROM 
		dbo.Member 
	where 
	EmailAddress = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("email", fmt.Sprintf("%v", email)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", email),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return valid, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(
			&j.ID,
		)
		valid = true
	}
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return valid, err
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", valid)),
	)
	return valid, nil
}
func (sj *sqlMemberRepository) UpdateForForgotPassword(ctx context.Context, password string, email *string) (bool, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}
	password = string(hashedPassword)
	// MemberQuery := `Update Member Set RecordStatus = @p1`
	MemberQuery := `Update Member Set`
	if password != "" {
		// MemberQuery += ` ,LoginPass = @p2`
		MemberQuery += ` LoginPass = @p2`
	}
	MemberQuery += ` Where EmailAddress = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
		zap.String("email", fmt.Sprintf("%v", email)),
		zap.String("password", fmt.Sprintf("%v", password)),
	)
	_, err = sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", email),
		sql.Named("p2", password),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) UpdateIMEI(ctx context.Context, imei *string, id string) (bool, error) {
	MemberQuery := `Update Member Set Imei = null Where ID =@p2`
	if imei != nil {
		MemberQuery = `Update Member Set Imei = @p1 Where ID =@p2`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p1", imei),
		sql.Named("p2", id),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlMemberRepository) GetByIMEI(ctx context.Context, imei *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
		Isnull(Imei,'') as Imei
	from 
		dbo.Member m
	where 
	Imei = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", imei)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
		&Member.Imei,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}
func (sj *sqlMemberRepository) GetByPhone(ctx context.Context, handphone *string) (*models.Member, error) {
	var Member models.Member
	query := `
	select 
		cast(ID as varchar(36)) ID,
		Isnull(Code,'') as Code,
		Isnull(MemberName,'') as MemberName,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(Phone,'') as Phone,
		IsNULL(cast(BankID as varchar(36)), '') as BankID,
		Isnull(Handphone,'') as Handphone,
		Isnull(VirtualAccountNo,'') as VirtualAccountNo,
		RecordStatus,
		Isnull(ProfileImage,'') as ProfileImage,
		Isnull(MemberType,'') as MemberType,
		Isnull(cast(CategoryID as varchar(36)),'') as CategoryID,
		Isnull(IdentityNumber,'') as IdentityNumber,
		Isnull(AlamatRumah,'') as Address,
		Isnull(Kota,'') as City,
		Isnull(Provinsi,'') as Province,
		Isnull(GoldStatus,0) as GoldStatus,
		Isnull(BirthPlace,'') as BirthPlace,
		Isnull(BirthDate,'') as BirthDate,
		Isnull(MotherMaiden,'') as MotherMaiden,
		Isnull(LoginPass,'') as LoginPass,
		cast(PlayerID as varchar(36)) as PlayerID,
		ISNULL(CONVERT(VARCHAR(10), m.DateInsert, 103) + ' '  + convert(VARCHAR(8), m.DateInsert, 14) ,'') as DateInsert,
		ISNULL(CONVERT(VARCHAR(10), m.DateUpdate, 103) + ' '  + convert(VARCHAR(8), m.DateUpdate, 14) ,'') as DateUpdate,
		ISNULL(Cast(m.UserInsert as varchar(36)), '') as UserInsert,
		ISNULL(Cast(m.UserUpdate as varchar(36)), '') as UserUpdate,
		Isnull(Imei,'') as Imei
	from 
		dbo.Member m
	where 
	Handphone = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", handphone)).Scan(
		&Member.ID,
		&Member.Code,
		&Member.MemberName,
		&Member.Email,
		&Member.Phone,
		&Member.BankID,
		&Member.Handphone,
		&Member.VirtualAccountNo,
		&Member.RecordStatus,
		&Member.ProfileImage,
		&Member.MemberType,
		&Member.CategoryID,
		&Member.IdentityNumber,
		&Member.Address,
		&Member.City,
		&Member.Province,
		&Member.GoldStatus,
		&Member.BirthPlace,
		&Member.BirthDate,
		&Member.MotherMaiden,
		&Member.LoginPass,
		&Member.PlayerID,
		&Member.DateInsert,
		&Member.DateUpdate,
		&Member.UserInsert,
		&Member.UserUpdate,
		&Member.Imei,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Member)),
	)
	return &Member, nil
}

func (sj *sqlMemberRepository) InsertSMSEmailLog(ctx context.Context, smsId string, memberId *string, requestbody string, message *string, tipe int32, sender *string) (bool, error) {
	MemberQuery := `INSERT INTO SMSEmailSenderLog (
		ID, 
		RequestBody,
		TransactionDate,
		Message,
		ResponseMessage,
		Type,
		Status,
		Sender,
		UserInsert,
		DateInsert
		) 
	VALUES (@p0, @p1, GETDATE(), @p2, NULL, @p3, NULL, @p5, @p4, GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", smsId),
		sql.Named("p1", requestbody),
		sql.Named("p2", message),
		sql.Named("p3", tipe),
		sql.Named("p4", memberId),
		sql.Named("p5", sender),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}

func (sj *sqlMemberRepository) UpdateSMSEmailLog(ctx context.Context, messageId string, memberId *string, responseMessage string, status string) (bool, error) {
	MemberQuery := `Update SMSEmailSenderLog 
	Set
		ResponseMessage = @p1,
		Status = @p2,
		UserUpdate = @p3,
		DateUpdate = GETDATE() 
	Where
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberQuery)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberQuery,
		sql.Named("p0", messageId),
		sql.Named("p1", responseMessage),
		sql.Named("p2", status),
		sql.Named("p3", memberId),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlMemberRepository) FetchMemberKYC(ctx context.Context) (error, []*models.MemberKYC) {

	result := make([]*models.MemberKYC, 0)
	query := `
		select
		Isnull(m.Code,'') as Code,
		Isnull(m.NoInduk,'') as NoInduk,
		Isnull(m.MemberName,'') as MemberName,
		Isnull(m.EmailAddress,'') as EmailAddress,
		Isnull(m.Handphone,'') as Handphone,
		--Isnull(m.IdentityType,0) as IdentityType,
		case when Isnull(m.IdentityType,0) = '1' then 'KTP'
		when Isnull(m.IdentityType,0) = '2' then 'Paspor'
		when Isnull(m.IdentityType,0) = '3' then 'SIM'
		when Isnull(m.IdentityType,0) = '4' then 'SIUP'
		when Isnull(m.IdentityType,0) = '5' then 'TDP'
		when Isnull(m.IdentityType,0) = '6' then 'SIUP' else '' end as IdentityType,
		Isnull(m.IdentityNumber,'') as IdentityNumber,
		Isnull(m.NoNPWP,'') as NoNPWP,
		--Isnull(m.Gender,0) as Gender,
		case when Isnull(m.Gender,0) = '1' then 'Pria'
		when Isnull(m.Gender,0) = '2' then 'Wanita' else '' end as Gender,
		Isnull(m.BirthPlace,'') as BirthPlace,
		Isnull(cast(m.BirthDate as nvarchar(10)),'') as BirthDate,
		case when Isnull(m.MemberType,0) = '1' then 'Customer'
		when Isnull(m.MemberType,0) = '9' then 'Agent' else '' end as MemberType,
		Isnull(bank.Code,'') as BankCode,
		Isnull(bank.BankName,'') as BankName,
		Isnull(m.BankAccountName,'') as BankAccountName,
		Isnull(m.BankAccountNo,'') as BankAccountNo,
		--Isnull(m.RecordStatus,0) as RecordStatus,
		case when Isnull(m.RecordStatus,'') = '1' then 'Pendaftaran Baru'
		when Isnull(m.RecordStatus,'') = '2' then 'Aktif'
		when Isnull(m.RecordStatus,'') = '3' then 'Lupa Password'
		when Isnull(m.RecordStatus,'') = '4' then 'Blokir'
		when Isnull(m.RecordStatus,'') = '5' then 'Ditolak'
		when Isnull(m.RecordStatus,'') = '8' then 'Menunggu Approval (Aktivasi)'
		when Isnull(m.RecordStatus,'') = '9' then 'Menunggu Verifikasi oleh Member'
		when Isnull(m.RecordStatus,'') = '10' then 'Menunggu Approval (Registrasi)'
		when Isnull(m.RecordStatus,'') = '11' then 'Member Melengkapi Data (Aktivasi)'
		when Isnull(m.RecordStatus,'') = '12' then 'Member Melengkapi Data (Registrasi)' else '' end as RecordStatus,
		Isnull(cast(m.DateInsert as nvarchar(10)),'') as TanggalTerdaftar,
		Isnull(m.Grade,'') as Grade,
		Isnull(a.NamaLengkapTanpaSingkatan,'') as NamaLengkapTanpaSingkatan,
		--Isnull(a.StatusPendidikan,'') as StatusPendidikan,
		case when Isnull(a.StatusPendidikan,'') = 'E0EEC25F-29E6-4420-ACF4-04D28C84EBD3' then 'SLTA'
		when Isnull(a.StatusPendidikan,'') = 'C63BF988-4354-48F1-A6E5-503EEEFFC933' then 'D3'
		when Isnull(a.StatusPendidikan,'') = '5E8CB79C-9C75-42D3-A090-CA3EF48E290B' then 'S1'
		when Isnull(a.StatusPendidikan,'') = '0A400AD6-FE90-4D6A-A86C-396DF67477DF' then 'S2'
		when Isnull(a.StatusPendidikan,'') = 'E0EEC25F-29E6-4420-ACF4-04D28C84EBD0' then 'S3'
		when Isnull(a.StatusPendidikan,'') = 'EF2684E7-0ECB-468B-B59C-7C9DBA096BEB' then 'Lainnya' else '' end as StatusPendidikan,
		Isnull(a.NamaIbuKandung,'') as NamaIbuKandung,
		Isnull(a.NamaKontakDarurat,'') as NamaKontakDarurat,
		Isnull(a.NomorKontakDarurat,'') as NomorKontakDarurat,
		Isnull(a.HubunganKontakDarurat,'') as HubunganKontakDarurat,
		Isnull(m.AlamatRumah,'') as AlamatRumah,
		Isnull(m.AlamatRumah2,'') as AlamatRumah2,
		Isnull(c.Province_Name,'') as Provinsi,
		Isnull(d.District_Name,'') as Kabupaten,
		Isnull(a.Kecamatan,'') as Kecamatan,
		Isnull(a.Kelurahan,'') as Kelurahan,
		Isnull(a.KodePos,'') as KodePos,
		Isnull(a.Phone,'') as Phone,
		Isnull(m.AlamatTempatKerja1,'') as AlamatTempatKerja1,
		Isnull(m.AlamatTempatKerja2,'') as AlamatTempatKerja2,
		Isnull(e.Province_Name,'') as ProvinsiTempatKerja,
		Isnull(f.District_Name,'') as KabupatenTempatKerja,
		Isnull(a.KecamatanTempatKerja,'') as KecamatanTempatKerja,
		Isnull(a.KelurahanTempatKerja,'') as KelurahanTempatKerja,
		Isnull(a.KodePosTempatKerja,'') as KodePosTempatKerja,
		case when Isnull(KodePekerjaan,'') = 'BE3BF0E1-E842-4360-8B53-0028736E54E5' then 'Pekerja Informal'
		when Isnull(KodePekerjaan,'') = '58045180-392C-4686-BD56-039761CF0091' then 'Pendidikan'
		when Isnull(KodePekerjaan,'') = '45980B34-B9C2-447D-B3E8-03B70F18AB44' then 'Pegawai Pemerintahan/ Lembaga Negara'
		when Isnull(KodePekerjaan,'') = '20A0A824-716E-4533-A3D4-10F1C328F948' then 'Komputer'
		when Isnull(KodePekerjaan,'') = 'CCAE0138-F70F-4F4F-98DC-125F375890FF' then 'Tenaga Medis'
		when Isnull(KodePekerjaan,'') = '5B6EAFCC-5887-465A-8F1A-172CC0B522C3' then 'Petani'
		when Isnull(KodePekerjaan,'') = 'D5002CF1-B26F-4272-99D3-23A5C08B4236' then 'Pejabat Negara/ Penyelenggara Negara'
		when Isnull(KodePekerjaan,'') = 'A1A06410-8D7D-4A6B-B4EA-25C100366F1E' then 'Peternak'
		when Isnull(KodePekerjaan,'') = '1FB8EB28-86BE-4156-91AD-25DBA238E32F' then 'Marketing'
		when Isnull(KodePekerjaan,'') = '8A17BB6B-C81E-4BBD-A8AD-29BAB4C5B551' then 'Pialang / Broker'
		when Isnull(KodePekerjaan,'') = '0E43214A-C1D7-4695-9A38-3758FEE23F73' then 'Transportasi Darat'
		when Isnull(KodePekerjaan,'') = '0D15514D-ABCD-4C41-B98A-3AD038C68EC7' then 'Customer Service'
		when Isnull(KodePekerjaan,'') = 'DE01612D-0D8F-4635-B2E2-3C666D02BCB5' then 'Desainer'
		when Isnull(KodePekerjaan,'') = '3EA597F0-9276-4EB5-B051-44B24F6827BE' then 'Buruh'
		when Isnull(KodePekerjaan,'') = '4450125F-080D-4DCD-AE44-46A7F12FAE70' then 'Engineering'
		when Isnull(KodePekerjaan,'') = 'E9EF5BAC-6B0E-40AA-A0F3-48FE325DFE60' then 'Eksekutif'
		when Isnull(KodePekerjaan,'') = '12F272DE-8AA9-4A54-AD53-4B1D286261E1' then 'Transportasi Laut'
		when Isnull(KodePekerjaan,'') = 'B0AE3F2F-2F40-4846-8595-4D1C6D1914E1' then 'Pengamanan'
		when Isnull(KodePekerjaan,'') = '7576E1C1-F425-4E50-A583-54A3A615ACFA' then 'Pensiunan'
		when Isnull(KodePekerjaan,'') = 'DE5754DF-FECF-41A4-9804-5805372E5880' then 'Polisi'
		when Isnull(KodePekerjaan,'') = '4463F87B-314B-4B0B-B541-612F42A3BF11' then 'Lain-lain'
		when Isnull(KodePekerjaan,'') = '8E3DCFE7-C5DB-4DA6-AB3B-62EC32F530B2' then 'Transportasi Udara'
		when Isnull(KodePekerjaan,'') = '26A66F64-8981-4C91-9A89-782CD590452B' then 'Militer'
		when Isnull(KodePekerjaan,'') = 'C25EDEB7-F90E-470A-BC1E-8488378A4A29' then 'Dokter'
		when Isnull(KodePekerjaan,'') = '2B4E2D1A-3BBC-44FC-ACF9-857DFB8704CA' then 'Ibu Rumah Tangga'
		when Isnull(KodePekerjaan,'') = '180366CA-1D96-431A-9370-88CE0A3B818F' then 'Wiraswasta'
		when Isnull(KodePekerjaan,'') = '4DE2D2B1-07DF-4CBD-A4FF-95A2813C902A' then 'Administrasi Umum'
		when Isnull(KodePekerjaan,'') = '1B5DCE00-EE4B-479E-AA29-990043EC2113' then 'Hukum'
		when Isnull(KodePekerjaan,'') = '98CFBFB9-F474-4676-B6F9-AE612B273F6F' then 'Arsitek'
		when Isnull(KodePekerjaan,'') = '96F5F13E-CAB6-435D-A491-B40B08F350DB' then 'Pelajar/Mahasiswa'
		when Isnull(KodePekerjaan,'') = 'D61B813D-B890-436F-87AD-BC93C3AD8A22' then 'Akunting/Keuangan'
		when Isnull(KodePekerjaan,'') = 'D06688FD-B23B-43F6-BB3E-C3B2B5934734' then 'Nelayan'
		when Isnull(KodePekerjaan,'') = '9DD5DC22-350A-46D1-ABEA-C5ECB78705DF' then 'Pekerja Seni'
		when Isnull(KodePekerjaan,'') = '59D16268-E4A8-48DF-AC26-E3503721AC54' then 'Perhotelan & Restoran'
		when Isnull(KodePekerjaan,'') = '63E42F4C-7DA5-4795-85E7-E3E61E2A4F51' then 'Peneliti'
		when Isnull(KodePekerjaan,'') = 'C2981D05-687E-4EB0-843F-F1CA887040D6' then 'Pertukangan & Pengrajin'
		when Isnull(KodePekerjaan,'') = 'BF624F81-215D-46FF-B5D3-F1CACD1A03D1' then 'Distributor'
		when Isnull(KodePekerjaan,'') = '0D39DAF1-C365-490D-A906-F63E25087C02' then 'Konsultan'
		else '' end as KodePekerjaan,
		Isnull(NamaTempatKerja,'') as NamaTempatKerja,
		Isnull(b.No_Head_Kategori,'') as KodeBidangUsaha,
		Isnull(PenghasilanKotorPerTahun,0) as PenghasilanKotorPerTahun,
		case when Isnull(KodeSumberPenghasilan,'') = '4C4C1204-514B-42AF-B47C-3E92B4EA05E5' then 'Gaji'
		when Isnull(KodeSumberPenghasilan,'') = '8BD33B99-49B8-465E-9DFD-6479367D28A4' then 'Usaha'
		when Isnull(KodeSumberPenghasilan,'') = '7D4F7659-7305-4499-BDDB-278DC0C3D48F' then 'Lainnya' else '' end as KodeSumberPenghasilan,
		case when Isnull(a.MaritalStatus,0) = '5' then 'Lajang'
		when Isnull(a.MaritalStatus,0) = '3' then 'Menikah'
		when Isnull(a.MaritalStatus,0) = '1' then 'De Facto'
		when Isnull(a.MaritalStatus,0) = '2' then 'Cerai'
		when Isnull(a.MaritalStatus,0) = '4' then 'Terpisah'
		when Isnull(a.MaritalStatus,0) = '6' then 'Duda/Janda' else '' end as MaritalStatus,
		Isnull(JumlahTanggungan,0) as JumlahTanggungan,
		Isnull(a.NoIdentitasPasangan,'') as NoIdentitasPasangan,
		Isnull(a.NamaPasangan,'') as NamaPasangan,
		ISNULL(CONVERT(VARCHAR(10), TanggalLahirPasangan) ,'') as TanggalLahirPasangan,
		case when Isnull(PisahHarta,0) = 0 then 'Tidak' else 'Ya' end as PisahHarta,
		cast(m.ID as varchar(36)) as MemberID
		from
		member m 
		left join dbo.MemberDetail a on m.id=a.MemberID
		left join Bank bank on m.BankID = bank.ID
		left join [Master.BidangSandi.KategoriBI] b on a.KodeBidangUsaha = b.BidangSandiKategoriBI_ID
		left join [Master.Province] c on a.Provinsi = c.Province_ID
		left join [Master.District] d on a.Kabupaten = d.District_ID
		left join [Master.Province] e on a.ProvinsiTempatKerja = e.Province_ID
		left join [Master.District] f on a.KabupatenTempatKerja = f.District_ID`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(
		query,
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.MemberKYC)
		err = rows.Scan(
			&j.Code,
			&j.NoInduk,
			&j.MemberName,
			&j.EmailAddress,
			&j.Handphone,
			&j.IdentityType,
			&j.IdentityNumber,
			&j.NoNPWP,
			&j.Gender,
			&j.BirthPlace,
			&j.BirthDate,
			&j.MemberType,
			&j.BankCode,
			&j.BankName,
			&j.BankAccountName,
			&j.BankAccountNo,
			&j.RecordStatus,
			&j.TanggalTerdaftar,
			&j.Grade,
			&j.NamaLengkapTanpaSingkatan,
			&j.StatusPendidikan,
			&j.NamaIbuKandung,
			&j.NamaKontakDarurat,
			&j.NomorKontakDarurat,
			&j.HubunganKontakDarurat,
			&j.AlamatRumah,
			&j.AlamatRumah2,
			&j.Provinsi,
			&j.Kabupaten,
			&j.Kecamatan,
			&j.Kelurahan,
			&j.KodePos,
			&j.Phone,
			&j.AlamatTempatKerja1,
			&j.AlamatTempatKerja2,
			&j.ProvinsiTempatKerja,
			&j.KabupatenTempatKerja,
			&j.KecamatanTempatKerja,
			&j.KelurahanTempatKerja,
			&j.KodePosTempatKerja,
			&j.KodePekerjaan,
			&j.NamaTempatKerja,
			&j.KodeBidangUsaha,
			&j.PenghasilanKotorPerTahun,
			&j.KodeSumberPenghasilan,
			&j.MaritalStatus,
			&j.JumlahTanggungan,
			&j.NoIdentitasPasangan,
			&j.NamaPasangan,
			&j.TanggalLahirPasangan,
			&j.PisahHarta,
			&j.MemberID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
