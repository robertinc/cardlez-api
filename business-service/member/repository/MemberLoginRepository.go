package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	memberlogin "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlMemberLoginRepository struct {
	Conn *sql.DB
}

func NewSqlMemberLoginRepository() memberlogin.MemberLoginRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberLoginRepository{conn}
}

func NewSqlMemberLoginRepositoryV2(Conn *sql.DB) memberlogin.MemberLoginRepository {
	conn := Conn
	return &sqlMemberLoginRepository{conn}
}

func (sj *sqlMemberLoginRepository) GetByMemberBranchID(ctx context.Context, id *string) ([]*models.MemberLogin, error) {
	query := `
	select 
		cast(ID as varchar(36)) ID,
		cast(MemberBranchID as varchar(36)) MemberBranchID,
		Isnull(EmailAddress,'') as EmailAddress,
		Isnull(LoginPass,'') as LoginPass,
		Isnull(Phone,'') as Phone,
		Isnull(RecordStatus,0) as RecordStatus
	from 
		dbo.MemberLogin 
	where 
		MemberBranchID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id))

	result := make([]*models.MemberLogin, 0)
	for rows.Next() {
		MemberLogin := new(models.MemberLogin)
		err = rows.Scan(
			&MemberLogin.ID,
			&MemberLogin.MemberBranchID,
			&MemberLogin.EmailAddress,
			&MemberLogin.LoginPass,
			&MemberLogin.Phone,
			&MemberLogin.RecordStatus,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, MemberLogin)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
