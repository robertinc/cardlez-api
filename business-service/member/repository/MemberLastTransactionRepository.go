package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"go.uber.org/zap"

	mda "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlMemberLastTransactionRepository struct {
	Conn *sql.DB
}

func NewSqlMemberLastTransactionRepository() mda.MemberLastTransactionRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberLastTransactionRepository{conn}
}

func NewSqlMemberLastTransactionRepositoryV2(Conn *sql.DB) mda.MemberLastTransactionRepository {
	conn := Conn
	return &sqlMemberLastTransactionRepository{conn}
}

func (sj *sqlMemberLastTransactionRepository) Fetch(ctx context.Context, memberID string) ([]*models.MemberLastTransaction, error) {
	//var resultCoupon models.MemberPromoResult
	query := `
	
		select TOP 50
			isnull(jd.DateInsert, '') as DateInsert, 
			Case When jd.Debet = 0 then jd.Credit
				else jd.Debet * -1 end as Amount,
			Notes
		from 
			Member m
			Inner Join Account a on m.ID = a.MemberID
			Inner Join JournalDetail jd on jd.ReferenceNumber = cast(a.ID as varchar(36))
		where
			m.id = @id
		order by jd.DateInsert desc
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("MemberID", fmt.Sprintf("%v", memberID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("id", memberID),
	) 
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	//resultCoupon.MemberCouponx = make([]*models.MemberCoupon, 0)
	result := make([]*models.MemberLastTransaction, 0)
	for rows.Next() {
		j := new(models.MemberLastTransaction)
		err = rows.Scan(
			&j.DateInsert,
			&j.Amount,
			&j.Notes,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	//resultCoupon.MemberCouponx = result

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
