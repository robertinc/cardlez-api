package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	mailbox "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlMailboxRepository struct {
	Conn *sql.DB
}

func NewSqlMailboxRepository() mailbox.MailboxRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMailboxRepository{conn}
}
func NewSqlMailboxRepositoryV2(Conn *sql.DB) mailbox.MailboxRepository {
	conn := Conn
	return &sqlMailboxRepository{conn}
}
func (sj *sqlMailboxRepository) GetDetailMailbox(ctx context.Context, mailboxID *string) (*models.Mailbox, error) {
	query := `
	select 
		cast(ID as varchar(36)) ID,
		cast(MemberID as varchar(36)) MemberID,
		Isnull(Code,'') as Code,
		Isnull(MailDate,'') as MailDate,
		Isnull(MailSubject,'') as MailSubject,
		Isnull(MailDesc,'') as MailDesc,
		Isnull(MailImages,'') as MailImages,
		Isnull(PublishStatus,0) as PublishStatus,
		Isnull(MailDirection,0) as MailDirection
	from 
		dbo.Mailbox 
	where 
	ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("mailboxId", fmt.Sprintf("%v", mailboxID)),
	)
	var mailbox models.Mailbox
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", mailboxID)).Scan(
		&mailbox.ID,
		&mailbox.MemberID,
		&mailbox.Code,
		&mailbox.MailDate,
		&mailbox.MailSubject,
		&mailbox.MailDesc,
		&mailbox.MailImages,
		&mailbox.PublishStatus,
		&mailbox.MailDirection,
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", mailbox)),
	)
	return &mailbox, nil
}

func (sj *sqlMailboxRepository) Store(ctx context.Context, a *models.Mailbox) (*models.Mailbox, error) {
	a.ID = guid.New().StringUpper()
	mailboxQuery := `
		INSERT INTO Mailbox (
			ID, 
			Code, 
			MailDate,
			MailDirection,
			MemberID,
			MailSubject,
			MailDesc,
			MailImages,
			PublishStatus,
			userinsert,
			dateinsert,
			userupdate,
			dateupdate
		) VALUES (
			@id, 
			@code, 
			GETDATE(),
			@maildirection, 
			@memberid, 
			@mailsubject,
			@maildesc,
			@mailimages,
			@publishstatus,
			@p5,
			GETDATE(), 
			@p6,
			GETDATE()
		)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", mailboxQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, mailboxQuery,
		sql.Named("id", a.ID),
		sql.Named("code", a.Code),
		sql.Named("maildirection", a.MailDirection),
		sql.Named("memberid", a.MemberID),
		sql.Named("mailsubject", a.MailSubject),
		sql.Named("maildesc", a.MailDesc),
		sql.Named("mailimages", a.MailImages),
		sql.Named("publishstatus", a.PublishStatus),
		sql.Named("p5", "Admin"),
		sql.Named("p6", "Admin"),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMailboxRepository) StoreByDate(ctx context.Context, a *models.Mailbox, companyDate string) (*models.Mailbox, error) {
	a.ID = guid.New().StringUpper()
	mailboxQuery := `
		INSERT INTO Mailbox (
			ID, 
			Code, 
			MailDate,
			MailDirection,
			MemberID,
			MailSubject,
			MailDesc,
			MailImages,
			PublishStatus,
			userinsert,
			dateinsert,
			userupdate,
			dateupdate
		) VALUES (
			@id, 
			@code, 
			@date,
			@maildirection, 
			@memberid, 
			@mailsubject,
			@maildesc,
			@mailimages,
			@publishstatus,
			@p5,
			@date, 
			@p6,
			@date
		)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", mailboxQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, mailboxQuery,
		sql.Named("id", a.ID),
		sql.Named("code", a.Code),
		sql.Named("maildirection", a.MailDirection),
		sql.Named("memberid", a.MemberID),
		sql.Named("mailsubject", a.MailSubject),
		sql.Named("maildesc", a.MailDesc),
		sql.Named("mailimages", a.MailImages),
		sql.Named("publishstatus", a.PublishStatus),
		sql.Named("p5", "Admin"),
		sql.Named("p6", "Admin"),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlMailboxRepository) FetchMailbox(ctx context.Context) ([]*models.MemberMailbox, error) {

	result := make([]*models.MemberMailbox, 0)
	query := `
	SELECT
		Cast(ID as varchar(36)),
		Code,
		MailDate,
		MailDirection,
		Cast(Isnull(MemberID, '00000000-0000-0000-0000-000000000000') as varchar(36)),
		Isnull(MailSubject, ''),
		Isnull(MailDesc, ''),
		Isnull(MailImages, ''),
		PublishStatus
	FROM
		Mailbox
	WHERE
		PublishStatus <> 0
	Order By DateInsert Desc
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.MemberMailbox)
		err = rows.Scan(
			&j.MailboxID,
			&j.Code,
			&j.MailDate,
			&j.MailDirection,
			&j.MemberID,
			&j.MailSubject,
			&j.MailDesc,
			&j.MailImages,
			&j.PublishStatus,
		)
		j.ID = j.MailboxID
		j.MailboxCode = j.Code
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlMailboxRepository) Update(ctx context.Context, a *models.Mailbox) (*models.Mailbox, error) {

	mailboxQuery := `
		UPDATE Mailbox 
		SET
			MailDirection = @maildirection,
			MemberID = @memberid,
			MailSubject = @mailsubject,
			MailDesc = @maildesc,
			MailImages = @mailimages,
			PublishStatus = @publishstatus,
			DateUpdate = GETDATE(),
			UserUpdate = @p5
		WHERE
			ID = @id
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", mailboxQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, mailboxQuery,
		sql.Named("id", a.ID),
		sql.Named("maildirection", a.MailDirection),
		sql.Named("memberid", a.MemberID),
		sql.Named("mailsubject", a.MailSubject),
		sql.Named("maildesc", a.MailDesc),
		sql.Named("mailimages", a.MailImages),
		sql.Named("publishstatus", a.PublishStatus),
		sql.Named("p5", "Admin"),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
