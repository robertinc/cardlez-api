package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/beevik/guid"
	"go.uber.org/zap"

	coupon "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlMemberCouponRepository struct {
	Conn *sql.DB
}

func NewSqlMemberCouponRepository() coupon.MemberCouponRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberCouponRepository{conn}
}

func NewSqlMemberCouponRepositoryV2(Conn *sql.DB) coupon.MemberCouponRepository {
	conn := Conn
	return &sqlMemberCouponRepository{conn}
}
func (sj *sqlMemberCouponRepository) Fetch(ctx context.Context, search string, keyword string) (error, []*models.MemberCoupon) {
	//var resultCoupon models.MemberPromoResult
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		Cast(MemberID as varchar(36)) as MemberID,
		Cast(PromoCouponID as varchar(36)) as PromoCouponID,
		PromoCode,
		PromoAmount,
		ExchangeDate,
		ExpiryDate,
		IsRedeem,
		RedeemDate, 
		RedeemTransRefNo
	FROM MemberCoupon
	`
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	} else {
		query += ` where 1=1`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	//resultCoupon.MemberCouponx = make([]*models.MemberCoupon, 0)
	result := make([]*models.MemberCoupon, 0)
	for rows.Next() {
		j := new(models.MemberCoupon)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberID,
			&j.PromoCouponID,
			&j.PromoCode,
			&j.PromoAmount,
			&j.ExchangeDate,
			&j.ExpiryDate,
			&j.IsRedeem,
			&j.RedeemDate,
			&j.RedeemTransRefNo,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	//resultCoupon.MemberCouponx = result

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlMemberCouponRepository) GetByMemberID(ctx context.Context, id *string) ([]*models.MemberCoupon, error) {

	query := `
		SELECT 
			Cast(ID as varchar(36)) as ID,
			Code,
			Cast(MemberID as varchar(36)) as MemberID,
			Cast(PromoCouponID as varchar(36)) as PromoCouponID,
			PromoCode,
			PromoAmount,
			ExchangeDate,
			ExpiryDate,
			IsRedeem,
			RedeemDate, 
			RedeemTransRefNo
		FROM 
			MemberCoupon
		where 
			MemberID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	result := make([]*models.MemberCoupon, 0)
	for rows.Next() {
		j := new(models.MemberCoupon)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.MemberID,
			&j.PromoCouponID,
			&j.PromoCode,
			&j.PromoAmount,
			&j.ExchangeDate,
			&j.ExpiryDate,
			&j.IsRedeem,
			&j.RedeemDate,
			&j.RedeemTransRefNo,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlMemberCouponRepository) Store(ctx context.Context, a *models.MemberCoupon) (*models.MemberCoupon, error) {
	a.ID = guid.New().StringUpper()
	query := `
	INSERT INTO MemberCoupon (
		ID, 
		Code, 
		MemberID, 
		PromoCouponID, 
		PromoCode, 
		PromoAmount,
		ExchangeDate,
		ExpiryDate,
		IsRedeem,
		RedeemDate,
		RedeemTransRefNo,
		DateInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3, 
		@p4, 
		@p5,
		@exchangeDate, 
		@p6,
		@p7,
		GETDATE(),
		@p8,
		GETDATE()
	)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberID),
		sql.Named("p3", a.PromoCouponID),
		sql.Named("p4", a.PromoCode),
		sql.Named("p5", a.PromoAmount),
		sql.Named("exchangeDate", a.ExchangeDate),
		sql.Named("p6", a.ExpiryDate),
		sql.Named("p7", a.IsRedeem),
		sql.Named("p8", a.RedeemTransRefNo),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberCouponRepository) StoreByDate(ctx context.Context, a *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error) {
	a.ID = guid.New().StringUpper()
	query := `
	INSERT INTO MemberCoupon (
		ID, 
		Code, 
		MemberID, 
		PromoCouponID, 
		PromoCode, 
		PromoAmount,
		ExchangeDate,
		ExpiryDate,
		IsRedeem,
		RedeemDate,
		RedeemTransRefNo,
		DateInsert
	) 
	VALUES (
		@p0, 
		@p1, 
		@p2, 
		@p3, 
		@p4, 
		@p5,
		@exchangeDate, 
		@p6,
		@p7,
		@date,
		@p8,
		@date
	)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, query,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.MemberID),
		sql.Named("p3", a.PromoCouponID),
		sql.Named("p4", a.PromoCode),
		sql.Named("p5", a.PromoAmount),
		sql.Named("exchangeDate", a.ExchangeDate),
		sql.Named("p6", a.ExpiryDate),
		sql.Named("p7", a.IsRedeem),
		sql.Named("p8", a.RedeemTransRefNo),
		sql.Named("date", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlMemberCouponRepository) Update(ctx context.Context, a *models.MemberCoupon) (*models.MemberCoupon, error) {
	var param []string
	MemberCouponQuery := `Update MemberCoupon Set IsRedeem = @p1, `
	// if a.IsRedeem.Valid == true {
	// 	param = append(param, "IsRedeem = @p1")
	// }
	// if len(param) == 0 {
	// 	return nil, fmt.Errorf("No Parameter to update")
	// }
	param = append(param, "UserUpdate = @userupdate")
	param = append(param, "DateUpdate = getdate()")
	MemberCouponQuery += strings.Join(param, ",")
	MemberCouponQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberCouponQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.IsRedeem),
		sql.Named("userupdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlMemberCouponRepository) UpdateByDate(ctx context.Context, a *models.MemberCoupon, companyDate string) (*models.MemberCoupon, error) {
	var param []string
	MemberCouponQuery := `Update MemberCoupon Set IsRedeem = @p1, `
	// if a.IsRedeem.Valid == true {
	// 	param = append(param, "IsRedeem = @p1")
	// }
	// if len(param) == 0 {
	// 	return nil, fmt.Errorf("No Parameter to update")
	// }
	param = append(param, "UserUpdate = @userupdate")
	param = append(param, "DateUpdate = @date")
	MemberCouponQuery += strings.Join(param, ",")
	MemberCouponQuery += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", MemberCouponQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, MemberCouponQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.IsRedeem),
		sql.Named("date", companyDate),
		sql.Named("userupdate", ctx.Value(service.CtxUserID).(string)),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
