package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	memberbranch "gitlab.com/robertinc/cardlez-api/business-service/member"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlMemberBranchRepository struct {
	Conn *sql.DB
}

func NewSqlMemberBranchRepository() memberbranch.MemberBranchRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlMemberBranchRepository{conn}
}

func NewSqlMemberBranchRepositoryV2(Conn *sql.DB) memberbranch.MemberBranchRepository {
	conn := Conn
	return &sqlMemberBranchRepository{conn}
}

func (sj *sqlMemberBranchRepository) GetByMemberID(ctx context.Context, id *string) ([]*models.MemberBranch, error) {

	query := `
	select 
		cast(ID as varchar(36)) ID,
		cast(MemberID as varchar(36)) MemberID,
		cast(BankID as varchar(36)) BankID,
		Isnull(CompanyName,'') as CompanyName,
		Isnull(BusinessType,0) as BusinessType
	from 
		dbo.MemberBranch 
	where 
		MemberID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id)) 
	if err != nil {
		logger.Error("Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}

	result := make([]*models.MemberBranch, 0)
	for rows.Next() {
		MemberBranch := new(models.MemberBranch)
		err = rows.Scan(
			&MemberBranch.ID,
			&MemberBranch.MemberID,
			&MemberBranch.BankID,
			&MemberBranch.CompanyName,
			&MemberBranch.BusinessType,
		)
		if err != nil {
			logger.Error("Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, MemberBranch)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
