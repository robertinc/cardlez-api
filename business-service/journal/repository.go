package journal

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/models"
)

type JournalRepository interface {
	Fetch(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	FetchBase(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	FetchBaseWithHour(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	GetByID(ctx context.Context, id *string) []*models.Journal
	GetByTransactionID(ctx context.Context, id *string) []*models.Journal
	GetByCode(ctx context.Context, code *string) []*models.Journal
	// Update(ctx context.Context, journal *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Journal) (*models.Journal, error)
	StoreBase(ctx context.Context, a *models.Journal) (*models.Journal, error)
	UpdateForBreak(ctx context.Context, notes string, amount float64, date time.Time) (bool, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
type JournalMappingRepository interface {
	GetByCode(ctx context.Context, code string) (*models.JournalMapping, error)
	GetJournalMappingDetail(ctx context.Context, journalMappingId string, position string, key *string) (*models.JournalMapping, error)
	GetJournalMappingDetailBase(ctx context.Context, journalMappingId string, position string, key *string) (*models.JournalMapping, error)
}
