package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/journal"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlJournalRepository struct {
	Conn *sql.DB
}

func NewSqlJournalRepository() journal.JournalRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlJournalRepository{conn}
}

func NewSqlJournalRepositoryV2(Conn *sql.DB) journal.JournalRepository {
	conn := Conn
	return &sqlJournalRepository{conn}
}
func (sj *sqlJournalRepository) Fetch(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	result := make([]*models.Journal, 0)
	query := `
		SELECT 
			Cast(ID as varchar(36)) as ID,
			Code,
			CONVERT(VARCHAR(10), postingDate, 103) + ' '  + convert(VARCHAR(8), postingDate, 14) as postingdate, 
			Deskripsi,
			DebitAmount,
			CreditAmount,
			COANo,
			DeskripsiCOA
		FROM dbo.VwJournalTransaksi
		WHERE CAST(PostingDate AS DATE) Between CAST(@p0 as date) and cast(@p1 as date)
		ORDER BY PostingDate,Code ASC `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd))
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlJournalRepository) FetchBase(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	result := make([]*models.Journal, 0)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		CONVERT(VARCHAR(10), postingDate, 103) + ' '  + convert(VARCHAR(8), postingDate, 14) as postingdate, 
		Deskripsi,
		DebitAmount,
		CreditAmount,
		COANo,
		DeskripsiCOA,
		ReferenceNumber,
		AccountNo,
		AccountName,
		isnull((
			select a.Balance from (
			--opening
			select a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,
			case when year(a.dateInsert)=year(cast(@p0 as date)) and month(a.dateInsert)=month(cast(@p0 as date)) and day(a.dateInsert)=day(cast(@p0 as date))
			then a.OpeningBalance else a.ClosingBalance end as Balance
			from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p0 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=isnull(b.ReferenceNumber,'')
			inner join COA c on a.COAID=c.ID
			) a 
			where  a.Code = vw.COANO and isnull(a.ReferenceNumber,'')=vw.referencenumber
		),0) as SaldoAwal,
		isnull((
			select a.ClosingBalance from (
			--closing
			select a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,a.ClosingBalance from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p1 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=isnull(b.ReferenceNumber,'')
			inner join COA c on a.COAID=c.ID
			) a 
			where  a.Code = vw.COANO and isnull(a.ReferenceNumber,'')=vw.referencenumber
		),0) as SaldoAkhir
	FROM dbo.VwJournalTransaksi vw
	WHERE CAST(PostingDate AS DATE) Between CAST(@p0 as date) and cast(@p1 as date)
	ORDER BY PostingDate,Code,CreditAmount ASC `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd))
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
			&j.ReferenceNumber,
			&j.AccountNo,
			&j.AccountName,
			&j.SaldoAwal,
			&j.SaldoAkhir,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlJournalRepository) FetchBaseWithHour(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	result := make([]*models.Journal, 0)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		CONVERT(VARCHAR(10), postingDate, 103) + ' '  + convert(VARCHAR(8), postingDate, 14) as postingdate, 
		Deskripsi,
		DebitAmount,
		CreditAmount,
		COANo,
		DeskripsiCOA,
		ReferenceNumber,
		AccountNo,
		AccountName,
		Cast(JournalDetailID as varchar(36)) as JournalDetailID,
		isnull((
			select a.Balance from (
			--opening
			select a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,
			case when year(a.dateInsert)=year(cast(@p0 as date)) and month(a.dateInsert)=month(cast(@p0 as date)) and day(a.dateInsert)=day(cast(@p0 as date))
			then a.OpeningBalance else a.ClosingBalance end as Balance
			from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p0 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=isnull(b.ReferenceNumber,'')
			inner join COA c on a.COAID=c.ID
			) a 
			where  a.Code = vw.COANO and isnull(a.ReferenceNumber,'')=vw.referencenumber
		),0) as SaldoAwal,
		isnull((
			select a.ClosingBalance from (
			--closing
			select a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,a.ClosingBalance from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p1 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=isnull(b.ReferenceNumber,'')
			inner join COA c on a.COAID=c.ID
			) a 
			where  a.Code = vw.COANO and isnull(a.ReferenceNumber,'')=vw.referencenumber
		),0) as SaldoAkhir
	FROM dbo.VwJournalTransaksi vw
	WHERE CAST(PostingDate AS datetime) Between CAST(@p0 as datetime) and cast(@p1 as datetime)
	ORDER BY PostingDate,Code,CreditAmount ASC `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd))
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
			&j.ReferenceNumber,
			&j.AccountNo,
			&j.AccountName,
			&j.JournalDetailID,
			&j.SaldoAwal,
			&j.SaldoAkhir,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlJournalRepository) GetByID(ctx context.Context, id *string) []*models.Journal {
	result := make([]*models.Journal, 0)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PostingDate,
		Deskripsi,
		DebitAmount,
		CreditAmount,
		COANo,
		DeskripsiCOA
	FROM dbo.VwJournalTransaksi
	Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)

	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil
		}
		result = append(result, j)
	}
	return result
}

func (sj *sqlJournalRepository) GetByTransactionID(ctx context.Context, id *string) []*models.Journal {
	result := make([]*models.Journal, 0)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PostingDate,
		Deskripsi,
		DebitAmount,
		CreditAmount,
		COANo,
		DeskripsiCOA
	FROM dbo.VwJournalTransaksi
	Where TransactionID = @p0
	Order By PostingDate Desc`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)

	rows, err := sj.Conn.Query(query,
		sql.Named("p0", id),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil
		}
		result = append(result, j)
	}
	return result
}

func (sj *sqlJournalRepository) GetByCode(ctx context.Context, code *string) []*models.Journal {
	result := make([]*models.Journal, 0)
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		PostingDate,
		Deskripsi,
		DebitAmount,
		CreditAmount,
		COANo,
		DeskripsiCOA
	FROM dbo.VwJournalTransaksi
	Where Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", code)),
	)

	rows, err := sj.Conn.Query(query,
		sql.Named("p0", code),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Journal)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
			&j.COA,
			&j.COADescription,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil
		}
		result = append(result, j)
	}
	return result
}

func (sj *sqlJournalRepository) Store(ctx context.Context, a *models.Journal) (*models.Journal, error) {
	a.ID = guid.New().StringUpper()
	if guid.IsGuid(a.JournalMappingID) == false {
		a.JournalMappingID = service.NewEmptyGuid()
	}
	dcQuery := `
	INSERT INTO Journal
	(
		ID,
		TransactionID,
		PostingDate,
		Notes,
		Code,
		JournalMappingID,
		DateInsert
	) VALUES (
		@p4,
		@p0,
		@p1,
		@p2,
		@p3,
		@p5,
		GETDATE()
	)
	`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", dcQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.TransactionID),
		sql.Named("p1", a.PostingDate),
		sql.Named("p2", a.Description),
		sql.Named("p3", a.Code),
		sql.Named("p4", a.ID),
		sql.Named("p5", a.JournalMappingID),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		trx.Rollback()
		return nil, err
	}
	// Insert Journal Detail
	for _, jd := range a.JournalDetails {
		jdQuery := `
			INSERT INTO JournalDetail (
				ID, 
				JournalID,
				COAID,
				Debet,
				Credit,
				Notes,
				ReferenceNumber,
				DateInsert
			) VALUES (
				@p6,
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				GetDate()
			)
		`
		logger.Info("Query",
			zap.String("query", fmt.Sprintf("%v", dcQuery)),
			zap.String("obj", fmt.Sprintf("%v", a)),
		)
		_, err = sj.Conn.ExecContext(ctx, jdQuery,
			sql.Named("p6", jd.ID),
			sql.Named("p0", a.ID),
			sql.Named("p1", jd.COAID),
			sql.Named("p2", jd.Debit),
			sql.Named("p3", jd.Credit),
			sql.Named("p4", jd.Notes),
			sql.Named("p5", jd.ReferenceNumber),
		)
		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}
		//Insert to AccountingAccount and AccountingBalance
		_, err = sj.StoreAccounting(ctx, &jd, a)
		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}

	}
	trx.Commit()
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlJournalRepository) StoreAccounting(ctx context.Context, jd *models.JournalDetail, a *models.Journal) (*models.JournalDetail, error) {

	logger := service.Logger(ctx)
	logger.Info("Insert to AccountingAccounts and AccountingBalance")
	// Insert Accounting Account
	accountingBalanceQuery := `

			DECLARE @@COACategory int = (select COACategory from [COA] where ID = @COAID);
			DECLARE @@DebetAmount decimal = @debit;
			DECLARE @@DateJournal datetime = @DateJournal;

			IF
			(
				select ISNULL(count(ID),0) as IsExist
				from [AccountingBalance]
				where 
					COAID = @COAID
					and IsNull(ReferenceNumber,'') = IsNull(@AccID, '')
					and DAY(DateInsert) = DAY(@@DateJournal)
					and MONTH(DateInsert) = MONTH(@@DateJournal)
					and YEAR(DateInsert) = YEAR(@@DateJournal)
			) = 0
			BEGIN
				INSERT INTO [dbo].[AccountingBalance](
					ID
					,COAID
					,ReferenceNumber
					,OpeningBalance
					,DebitMovement
					,CreditMovement
					,ClosingBalance
					,UserInsert
					,DateInsert
					,UserUpdate
					,DateUpdate
					)
				VALUES (
					NEWID()
					,@COAID
					,@AccID
					,ISNULL((select top 1 ISNULL(closingbalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0)
					,@debit
					,@credit
					,(case when @@DebetAmount != 0 then  
						CASE 
						WHEN @@COACategory = 1 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 2 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 5 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 6 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						END
						when @@DebetAmount = 0 then  
						CASE
						WHEN @@COACategory = 1 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 2 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 5 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 6 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						end
					END) 
					,NULL
					,GETDATE()
					,NULL
					,GETDATE()
				)
			END
			ELSE
			BEGIN
				UPDATE [dbo].[AccountingBalance] SET            
				COAID = @COAID
				, ReferenceNumber = @AccID         
				, DebitMovement =  DebitMovement + @debit
				, CreditMovement =  CreditMovement + @credit
				, ClosingBalance = 
					(case 
						when @@DebetAmount != 0 then
						CASE 
							WHEN @@COACategory = 1 THEN ClosingBalance + @amount
							WHEN @@COACategory = 2 THEN ClosingBalance - @amount
							WHEN @@COACategory = 5 THEN ClosingBalance - @amount
							WHEN @@COACategory = 6 THEN ClosingBalance + @amount
						END
						when @@DebetAmount = 0 then
						CASE 
							WHEN @@COACategory = 1 THEN ClosingBalance - @amount
							WHEN @@COACategory = 2 THEN ClosingBalance + @amount
							WHEN @@COACategory = 5 THEN ClosingBalance + @amount
							WHEN @@COACategory = 6 THEN ClosingBalance - @amount
						END
					end) 
				, UserUpdate =  NULL
				, DateUpdate = GETDATE()
			WHERE ID = (select top 1 ID
			from [AccountingBalance]
			where 
			COAID = @COAID
			and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
			and DAY(DateInsert) = DAY(@@DateJournal)
			and MONTH(DateInsert) = MONTH(@@DateJournal)
			and YEAR(DateInsert) = YEAR(@@DateJournal)
			)
			END;


			declare @@accno varchar(1000) = IsNull(
				(
					select top 1 Code
					From Account
					Where ID = @AccID
				)
			,0)

			declare @@balance decimal = ISNULL(
				(
					select top 1 ClosingBalance 
					from [AccountingBalance]  
					where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
					order by DateInsert desc
				),0)
			IF (
				(
					select ISNULL(count(ID),0)
					from [AccountingAccounts]
					where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
				) = 0
			)
			BEGIN 
				Insert Into AccountingAccounts
					(ID, COAID, ReferenceNumber, OnlineBalance, WorkingBalance,
					DateInsert, AccountNumber)
				values(
					NEWID(), @COAID, @AccID, @@balance, @@balance,
					getdate(), @@accno
				)
			END 
			ELSE
			BEGIN
				update AccountingAccounts
				set 
					OnlineBalance =  @@balance,
					WorkingBalance = @@balance,
					DateUpdate = GetDate()
				where COAID = @COAID and IsNull(ReferenceNumber,'') = IsNull(@AccID, '')
			END
		`
	amount := jd.Debit
	if jd.Debit == 0 {
		amount = jd.Credit
	}
	var refNumber sql.NullString
	if jd.ReferenceNumber == "" {
		refNumber = sql.NullString{}
	} else {
		refNumber = sql.NullString{
			String: jd.ReferenceNumber,
			Valid:  true,
		}
	}
	_, err := sj.Conn.ExecContext(ctx, accountingBalanceQuery,
		sql.Named("COAID", jd.COAID),
		sql.Named("AccID", refNumber),
		sql.Named("debit", jd.Debit),
		sql.Named("credit", jd.Credit),
		sql.Named("amount", amount),
		sql.Named("DateJournal", a.PostingDate),
	)

	if err != nil {
		return nil, err
	}

	return jd, nil
}

func (sj *sqlJournalRepository) StoreBase(ctx context.Context, a *models.Journal) (*models.Journal, error) {
	a.ID = guid.New().StringUpper()
	if guid.IsGuid(a.JournalMappingID) == false {
		a.JournalMappingID = service.NewEmptyGuid()
	}
	layout := "2006-01-02 15:04:05"
	t := time.Now()
	compDateConvert, err := time.Parse(layout, a.PostingDate)
	if err != nil {
		return nil, err
	}
	companyDate := time.Date(compDateConvert.Year(), compDateConvert.Month(), compDateConvert.Day(), t.Hour(), t.Minute(), t.Second(), 0, compDateConvert.Location()).Format(layout)

	dcQuery := `
	INSERT INTO Journal
	(
		ID,
		TransactionID,
		PostingDate,
		Notes,
		Code,
		JournalMappingID,
		DateInsert
	) VALUES (
		@p4,
		@p0,
		@p1,
		@p2,
		@p3,
		@p5,
		@p1
	)
	`
	trx, err := sj.Conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", dcQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err = sj.Conn.ExecContext(ctx, dcQuery,
		sql.Named("p0", a.TransactionID),
		sql.Named("p1", companyDate),
		sql.Named("p2", a.Description),
		sql.Named("p3", a.Code),
		sql.Named("p4", a.ID),
		sql.Named("p5", a.JournalMappingID),
	)

	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		trx.Rollback()
		return nil, err
	}
	// Insert Journal Detail
	for _, jd := range a.JournalDetails {
		jdQuery := `
			INSERT INTO JournalDetail (
				ID, 
				JournalID,
				COAID,
				Debet,
				Credit,
				Notes,
				ReferenceNumber,
				DateInsert
			) VALUES (
				@p6,
				@p0,
				@p1,
				@p2,
				@p3,
				@p4,
				@p5,
				@p7
			)
		`
		logger.Info("Query",
			zap.String("query", fmt.Sprintf("%v", dcQuery)),
			zap.String("obj", fmt.Sprintf("%v", a)),
		)
		_, err = sj.Conn.ExecContext(ctx, jdQuery,
			sql.Named("p6", jd.ID),
			sql.Named("p0", a.ID),
			sql.Named("p1", jd.COAID),
			sql.Named("p2", jd.Debit),
			sql.Named("p3", jd.Credit),
			sql.Named("p4", jd.Notes),
			sql.Named("p5", jd.ReferenceNumber),
			sql.Named("p7", companyDate),
		)
		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}
		//Insert to AccountingAccount and AccountingBalance
		_, err = sj.StoreAccountingBase(ctx, &jd, a)
		if err != nil {
			logger.Error("Query Error",
				zap.String("error", err.Error()),
			)
			trx.Rollback()
			return nil, err
		}

	}
	trx.Commit()
	logger.Info("Insert Success")
	return a, nil
}

func (sj *sqlJournalRepository) StoreAccountingBase(ctx context.Context, jd *models.JournalDetail, a *models.Journal) (*models.JournalDetail, error) {

	logger := service.Logger(ctx)
	logger.Info("Insert to AccountingAccounts and AccountingBalance")
	layout := "2006-01-02 15:04:05"
	t := time.Now()
	compDateConvert, err := time.Parse(layout, a.PostingDate)
	if err != nil {
		return nil, err
	}
	companyDate := time.Date(compDateConvert.Year(), compDateConvert.Month(), compDateConvert.Day(), t.Hour(), t.Minute(), t.Second(), 0, compDateConvert.Location()).Format(layout)

	// Insert Accounting Account
	accountingBalanceQuery := `

			DECLARE @@COACategory int = (select COACategory from [COA] where ID = @COAID);
			DECLARE @@DebetAmount decimal = @debit;
			DECLARE @@DateJournal datetime = @DateJournal;

			IF
			(
				select ISNULL(count(ID),0) as IsExist
				from [AccountingBalance]
				where 
					COAID = @COAID
					and IsNull(ReferenceNumber,'') = IsNull(@AccID, '')
					and DAY(DateInsert) = DAY(@@DateJournal)
					and MONTH(DateInsert) = MONTH(@@DateJournal)
					and YEAR(DateInsert) = YEAR(@@DateJournal)
			) = 0
			BEGIN
				INSERT INTO [dbo].[AccountingBalance](
					ID
					,COAID
					,ReferenceNumber
					,OpeningBalance
					,DebitMovement
					,CreditMovement
					,ClosingBalance
					,UserInsert
					,DateInsert
					,UserUpdate
					,DateUpdate
					)
				VALUES (
					NEWID()
					,@COAID
					,@AccID
					,ISNULL((select top 1 ISNULL(closingbalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0)
					,@debit
					,@credit
					,(case when @@DebetAmount != 0 then  
						CASE 
						WHEN @@COACategory = 1 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 2 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 5 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 6 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						END
						when @@DebetAmount = 0 then  
						CASE
						WHEN @@COACategory = 1 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						WHEN @@COACategory = 2 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 5 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) + @amount)
						WHEN @@COACategory = 6 THEN (ISNULL((select top 1 ISNULL(closingBalance,0) as OpeningBalance from [AccountingBalance] where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '') order by DateInsert desc),0) - @amount)
						end
					END) 
					,NULL
					,@CompanyDate
					,NULL
					,@CompanyDate
				)
			END
			ELSE
			BEGIN
				UPDATE [dbo].[AccountingBalance] SET            
				COAID = @COAID
				, ReferenceNumber = @AccID         
				, DebitMovement =  DebitMovement + @debit
				, CreditMovement =  CreditMovement + @credit
				, ClosingBalance = 
					(case 
						when @@DebetAmount != 0 then
						CASE 
							WHEN @@COACategory = 1 THEN ClosingBalance + @amount
							WHEN @@COACategory = 2 THEN ClosingBalance - @amount
							WHEN @@COACategory = 5 THEN ClosingBalance - @amount
							WHEN @@COACategory = 6 THEN ClosingBalance + @amount
						END
						when @@DebetAmount = 0 then
						CASE 
							WHEN @@COACategory = 1 THEN ClosingBalance - @amount
							WHEN @@COACategory = 2 THEN ClosingBalance + @amount
							WHEN @@COACategory = 5 THEN ClosingBalance + @amount
							WHEN @@COACategory = 6 THEN ClosingBalance - @amount
						END
					end) 
				, UserUpdate =  NULL
				, DateUpdate = @CompanyDate
			WHERE ID = (select top 1 ID
			from [AccountingBalance]
			where 
			COAID = @COAID
			and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
			and DAY(DateInsert) = DAY(@@DateJournal)
			and MONTH(DateInsert) = MONTH(@@DateJournal)
			and YEAR(DateInsert) = YEAR(@@DateJournal)
			)
			END;


			declare @@accno varchar(1000) = IsNull(
				(
					select top 1 Code
					From Account
					Where ID = @AccID
				)
			,0)

			declare @@balance decimal = ISNULL(
				(
					select top 1 ClosingBalance 
					from [AccountingBalance]  
					where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
					order by DateInsert desc
				),0)
			IF (
				(
					select ISNULL(count(ID),0)
					from [AccountingAccounts]
					where COAID = @COAID and IsNull(ReferenceNumber, '') = IsNull(@AccID, '')
				) = 0
			)
			BEGIN 
				Insert Into AccountingAccounts
					(ID, COAID, ReferenceNumber, OnlineBalance, WorkingBalance,
					DateInsert, AccountNumber)
				values(
					NEWID(), @COAID, @AccID, @@balance, @@balance,
					@CompanyDate, @@accno
				)
			END 
			ELSE
			BEGIN
				update AccountingAccounts
				set 
					OnlineBalance =  @@balance,
					WorkingBalance = @@balance,
					DateUpdate = @CompanyDate
				where COAID = @COAID and IsNull(ReferenceNumber,'') = IsNull(@AccID, '')
			END
		`
	amount := jd.Debit
	if jd.Debit == 0 {
		amount = jd.Credit
	}
	var refNumber sql.NullString
	if jd.ReferenceNumber == "" {
		refNumber = sql.NullString{}
	} else {
		refNumber = sql.NullString{
			String: jd.ReferenceNumber,
			Valid:  true,
		}
	}
	_, err = sj.Conn.ExecContext(ctx, accountingBalanceQuery,
		sql.Named("COAID", jd.COAID),
		sql.Named("AccID", refNumber),
		sql.Named("debit", jd.Debit),
		sql.Named("credit", jd.Credit),
		sql.Named("amount", amount),
		sql.Named("DateJournal", companyDate),
		sql.Named("CompanyDate", companyDate),
	)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", accountingBalanceQuery)),
		zap.String("companyDate", fmt.Sprintf("%v", companyDate)),
	)
	if err != nil {
		return nil, err //fmt.Errorf(a.PostingDate)
	}

	return jd, nil
}

func (sj *sqlJournalRepository) UpdateForBreak(ctx context.Context, notes string, amount float64, date time.Time) (bool, error) {
	t := time.Now()
	layout := "2006-01-02 15:04:05"
	companyDate := time.Date(date.Year(), date.Month(), date.Day(), t.Hour(), t.Minute(), t.Second(), 0, date.Location()).Format(layout)

	Query := `
	update 
		JournalDetail 
	set 
		Debet=@p1,DateUpdate=@p3 
	where 
		COAID='82B8332C-03FF-4587-BDF1-8B51EFEDAC15' AND Notes=@p0 AND FORMAT(DateInsert, 'yyyy-MM-dd')=FORMAT(@p2, 'yyyy-MM-dd')
	update 
		JournalDetail 
	set Credit=@p1,DateUpdate=@p3 
	where 
		COAID='17F8644F-12EC-4DB4-A8D2-62389A567D81' AND Notes=@p0 AND FORMAT(DateInsert, 'yyyy-MM-dd')=FORMAT(@p2, 'yyyy-MM-dd')
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("notes", fmt.Sprintf("%v", notes)),
		zap.String("accrualAmount", fmt.Sprintf("%v", amount)),
		zap.String("date", fmt.Sprintf("%v", date)),
		zap.String("companydate", fmt.Sprintf("%v", companyDate)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", notes),
		sql.Named("p1", amount),
		sql.Named("p2", date),
		sql.Named("p3", companyDate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
