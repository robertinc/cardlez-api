package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/journal"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlJournalMappingRepository struct {
	Conn *sql.DB
}

func NewSqlJournalMappingRepository() journal.JournalMappingRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlJournalMappingRepository{conn}
}

func NewSqlJournalMappingRepositoryV2(Conn *sql.DB) journal.JournalMappingRepository {
	conn := Conn
	return &sqlJournalMappingRepository{conn}
}
func (sj *sqlJournalMappingRepository) GetByCode(ctx context.Context, code string) (*models.JournalMapping, error) {
	var j models.JournalMapping
	query := `
	SELECT   
		Cast(ID as varchar(36)) as ID,
		Code,
		TransactionName,
		Remarks
	FROM 
		dbo.JournalMapping 
	where 
		Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", j.ID)),
		zap.String("code", fmt.Sprintf("%v", j.Code)),
		zap.String("transactionName", fmt.Sprintf("%v", j.TransactionName)),
		zap.String("remarks", fmt.Sprintf("%v", j.Remarks)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&j.ID,
		&j.Code,
		&j.TransactionName,
		&j.Remarks,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlJournalMappingRepository) GetJournalMappingDetail(ctx context.Context, journalMappingId string, position string, key *string) (*models.JournalMapping, error) {
	var j models.JournalMapping
	query := `
	SELECT   
		Cast(ID as varchar(36)) as ID,
		COANo
	FROM 
		dbo.JournalMappingDetail 
	where 
	JournalMappingID = @p0 and Position = @p1`
	if key != nil {
		query += ` and KeyName like @p2`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", j.ID)),
		zap.String("coano", fmt.Sprintf("%v", j.Code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", journalMappingId),
		sql.Named("p1", position),
		sql.Named("p2", "%"+*key+"%")).Scan(
		&j.ID,
		&j.Code,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlJournalMappingRepository) GetJournalMappingDetailBase(ctx context.Context, journalMappingId string, position string, key *string) (*models.JournalMapping, error) {
	var j models.JournalMapping
	query := `
	SELECT   
		Cast(ID as varchar(36)) as ID,
		COANo,
		ISNULL(DefaultValue, '') as DefaultValue
	FROM 
		dbo.JournalMappingDetail 
	where 
	JournalMappingID = @p0 and Position = @p1`
	if key != nil {
		query += ` and KeyName = @p2`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", j.ID)),
		zap.String("coano", fmt.Sprintf("%v", j.Code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", journalMappingId),
		sql.Named("p1", position),
		sql.Named("p2", key)).Scan(
		//sql.Named("p2", "%"+*key+"%")).Scan(
		&j.ID,
		&j.Code,
		&j.DefaultValue,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
