package journal

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/models"
)

type JournalUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, string, error)
	GetByID(ctx context.Context, id *string) []*models.Journal
	GetByTransactionID(ctx context.Context, id *string) []*models.Journal
	GetByCode(ctx context.Context, code *string) []*models.Journal
	// Update(ctx context.Context, ar *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(context.Context, *models.Journal) (*models.Journal, error)
	StoreBase(context.Context, *models.Journal) (*models.Journal, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	FetchBase(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	FetchBaseWithHour(ctx context.Context, periodStart string, periodEnd string) ([]*models.Journal, error)
	UpdateForBreak(ctx context.Context, notes string, amount float64, date time.Time) (bool, error)
}
type JournalMappingUsecase interface {
	GetByCode(ctx context.Context, code string) (*models.JournalMapping, error)
}
