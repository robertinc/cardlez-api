package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/journal"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type JournalUsecase struct {
	JournalRepository journal.JournalRepository
	contextTimeout    time.Duration
}

func NewJournalUsecase() journal.JournalUsecase {
	return &JournalUsecase{
		JournalRepository: repository.NewSqlJournalRepository(),
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func NewJournalUsecaseV2(repo journal.JournalRepository) journal.JournalUsecase {
	return &JournalUsecase{
		JournalRepository: repo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func (a *JournalUsecase) Fetch(c context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal, err := a.JournalRepository.Fetch(ctx, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listJournal, nil
}

func (a *JournalUsecase) FetchBase(c context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal, err := a.JournalRepository.FetchBase(ctx, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listJournal, nil
}

func (a *JournalUsecase) FetchBaseWithHour(c context.Context, periodStart string, periodEnd string) ([]*models.Journal, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal, err := a.JournalRepository.FetchBaseWithHour(ctx, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listJournal, nil
}

func (a *JournalUsecase) GetByID(c context.Context, id *string) []*models.Journal {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal := a.JournalRepository.GetByID(ctx, id)

	return listJournal
}
func (a *JournalUsecase) GetByTransactionID(c context.Context, id *string) []*models.Journal {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal := a.JournalRepository.GetByTransactionID(ctx, id)

	return listJournal
}
func (a *JournalUsecase) GetByCode(c context.Context, code *string) []*models.Journal {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listJournal := a.JournalRepository.GetByCode(ctx, code)

	return listJournal
}
func (a *JournalUsecase) Store(c context.Context, b *models.Journal) (*models.Journal, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	j, err := a.JournalRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return j, nil
}
func (a *JournalUsecase) StoreBase(c context.Context, b *models.Journal) (*models.Journal, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	j, err := a.JournalRepository.StoreBase(ctx, b)

	if err != nil {
		return nil, err
	}
	return j, nil
}
func (a *JournalUsecase) UpdateForBreak(c context.Context, notes string, amount float64, date time.Time) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	j, err := a.JournalRepository.UpdateForBreak(ctx, notes, amount, date)
	if err != nil {
		return false, err
	}
	return j, nil
}
