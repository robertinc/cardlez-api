package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/journal"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type JournalMappingUsecase struct {
	JournalMappingRepository journal.JournalMappingRepository
	contextTimeout           time.Duration
}

func NewJournalMappingUsecase() journal.JournalMappingUsecase {
	return &JournalMappingUsecase{
		JournalMappingRepository: repository.NewSqlJournalMappingRepository(),
		contextTimeout:           time.Second * time.Duration(models.Timeout()),
	}
}

func NewJournalMappingUsecaseV2(journalMappingRepo journal.JournalMappingRepository) journal.JournalMappingUsecase {
	return &JournalMappingUsecase{
		JournalMappingRepository: journalMappingRepo,
		contextTimeout:           time.Second * time.Duration(models.Timeout()),
	}
}

func (a *JournalMappingUsecase) GetByCode(c context.Context, code string) (*models.JournalMapping, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	journalmapping, err := a.JournalMappingRepository.GetByCode(ctx, code)

	if err != nil {
		return nil, err
	}
	return journalmapping, nil
}
