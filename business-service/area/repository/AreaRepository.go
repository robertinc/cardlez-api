package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/area"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlAreaRepository struct {
	Conn *sql.DB
}

func NewSqlAreaRepository() area.AreaRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlAreaRepository{conn}
}

func NewSqlAreaRepositoryV2(Conn *sql.DB) area.AreaRepository {
	conn := Conn
	return &sqlAreaRepository{conn}
}
func (sj *sqlAreaRepository) FetchKota(ctx context.Context, DistrictID string) (error, []*models.Area) {
	query := `
		select 	
			distinct Kota
		from Area
		where District_ID = @p0
		order by Kota
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", DistrictID),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Area, 0)
	for rows.Next() {
		j := new(models.Area)
		err = rows.Scan(
			&j.Kota,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAreaRepository) FetchKecamatan(ctx context.Context, DistrictID string) (error, []*models.Area) {
	query := `
		select 	
			distinct Kecamatan
		from vwArea
		where District_ID = @p0
		order by Kecamatan
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", DistrictID),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Area, 0)
	for rows.Next() {
		j := new(models.Area)
		err = rows.Scan(
			&j.Kecamatan,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAreaRepository) FetchKelurahan(ctx context.Context, Kecamatan string) (error, []*models.Area) {
	query := `
		select 	
			distinct Kelurahan,
			Kodepos
		from vwArea
		where Kecamatan = @p0
		order by Kelurahan
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", Kecamatan),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Area, 0)
	for rows.Next() {
		j := new(models.Area)
		err = rows.Scan(
			&j.Kelurahan,
			&j.KodePos,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAreaRepository) GetKelurahan(ctx context.Context, Kecamatan string, DistrictID string) (error, []*models.Area) {
	query := `
		select 	
			distinct Kelurahan,
			Kodepos
		from vwArea
		where Kecamatan = @p0 and District_ID = @p1
		order by Kelurahan
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", Kecamatan),
		sql.Named("p1", DistrictID),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Area, 0)
	for rows.Next() {
		j := new(models.Area)
		err = rows.Scan(
			&j.Kelurahan,
			&j.KodePos,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAreaRepository) FetchKodePos(ctx context.Context, Kelurahan string) (error, []*models.Area) {
	query := `
		select 	
			distinct KodePos
		from Area
		where Kelurahan = @p0
		order by Kodepos
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", Kelurahan),
	)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Area, 0)
	for rows.Next() {
		j := new(models.Area)
		err = rows.Scan(
			&j.KodePos,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlAreaRepository) FetchAreaID(ctx context.Context, KodePos string, Kelurahan string, Kecamatan string) (error, *models.Area) {
	var Area models.Area
	query := `
		select 	
			 isnull(cast(Area_ID as varchar(36)),'') as Area_ID
		from Area
		where KodePos = @p0 and Kelurahan = @p1 and Kecamatan = @p2
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", KodePos),
		sql.Named("p1", Kelurahan),
		sql.Named("p2", Kecamatan),
	).Scan(
		&Area.Area_ID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Area)),
	)
	return nil, &Area
}
