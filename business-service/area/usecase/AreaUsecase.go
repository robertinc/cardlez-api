package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/area"
	"gitlab.com/robertinc/cardlez-api/business-service/area/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type AreaUsecase struct {
	AreaRepository area.AreaRepository
	contextTimeout time.Duration
}

func NewAreaUsecase() area.AreaUsecase {
	return &AreaUsecase{
		AreaRepository: repository.NewSqlAreaRepository(),
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func NewAreaUsecaseV2(repo area.AreaRepository) area.AreaUsecase {
	return &AreaUsecase{
		AreaRepository: repo,
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func (a *AreaUsecase) FetchKota(c context.Context, DistrictID string) (error, []*models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listArea := a.AreaRepository.FetchKota(ctx, DistrictID)
	if err != nil {
		return err, nil
	}
	return nil, listArea
}
func (a *AreaUsecase) FetchKecamatan(c context.Context, DistrictID string) (error, []*models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listArea := a.AreaRepository.FetchKecamatan(ctx, DistrictID)
	if err != nil {
		return err, nil
	}
	return nil, listArea
}
func (a *AreaUsecase) FetchKelurahan(c context.Context, Kecamatan string) (error, []*models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listArea := a.AreaRepository.FetchKelurahan(ctx, Kecamatan)
	if err != nil {
		return err, nil
	}
	return nil, listArea
}
func (a *AreaUsecase) GetKelurahan(c context.Context, Kecamatan string, DistrictID string) (error, []*models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listArea := a.AreaRepository.GetKelurahan(ctx, Kecamatan, DistrictID)
	if err != nil {
		return err, nil
	}
	return nil, listArea
}
func (a *AreaUsecase) FetchKodePos(c context.Context, Kelurahan string) (error, []*models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listArea := a.AreaRepository.FetchKodePos(ctx, Kelurahan)
	if err != nil {
		return err, nil
	}
	return nil, listArea
}
func (a *AreaUsecase) FetchAreaID(c context.Context, KodePos string, Kelurahan string, Kecamatan string) (error, *models.Area) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, Area := a.AreaRepository.FetchAreaID(ctx, KodePos,Kelurahan,Kecamatan)
	if err != nil {
		return err, nil
	}
	return nil, Area
}
