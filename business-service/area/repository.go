package area

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type AreaRepository interface {
	FetchKota(ctx context.Context, DistrictID string) (error, []*models.Area)
	FetchKecamatan(ctx context.Context, DistrictID string) (error, []*models.Area)
	FetchKelurahan(ctx context.Context, Kecamatan string) (error, []*models.Area)
	GetKelurahan(ctx context.Context, Kecamatan string, DistrictID string) (error, []*models.Area)
	FetchKodePos(ctx context.Context, Kelurahan string) (error, []*models.Area)
	FetchAreaID(ctx context.Context, KodePos string, Kelurahan string, Kecamatan string) (error, *models.Area)
}
