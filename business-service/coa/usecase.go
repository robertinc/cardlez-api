package coa

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type COAUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.COA, string, error)
	//GetByID(ctx context.Context, id *string) *models.COA
	// Update(ctx context.Context, ar *models.COA) (*models.COA, error)
	// GetByTitle(ctx context.Context, title string) (*models.COA, error)
	// Store(context.Context, *models.COA) (*models.COA, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, search string, keyword string) ([]*models.COA, error)
}
