package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/coa"
	"gitlab.com/robertinc/cardlez-api/business-service/coa/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type COAUsecase struct {
	COARepository  coa.COARepository
	contextTimeout time.Duration
}

func NewCOAUsecase() coa.COAUsecase {
	return &COAUsecase{
		COARepository:  repository.NewSqlCOARepository(),
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func NewCOAUsecaseV2(coaRepo coa.COARepository) coa.COAUsecase {
	return &COAUsecase{
		COARepository:  coaRepo,
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func (a *COAUsecase) Fetch(c context.Context, search string, keyword string) ([]*models.COA, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCOA, err := a.COARepository.Fetch(ctx, search, keyword)
	if err != nil {
		return nil, err
	}
	return listCOA, nil
}
