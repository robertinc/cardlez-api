package coa

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type COARepository interface {
	Fetch(ctx context.Context, search string, keyword string) ([]*models.COA, error)
	GetByID(ctx context.Context, id *string) (*models.COA, error)
	GetByProductID(ctx context.Context, productid *string) (*models.COA, error)
	GetByCode(ctx context.Context, code *string) (*models.COA, error)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	// GetByID(ctx context.Context, id *string) *models.Journal
	// Update(ctx context.Context, journal *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	// Store(ctx context.Context, a *models.Journal) (int64, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
