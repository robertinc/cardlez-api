package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/coa"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlCOARepository struct {
	Conn *sql.DB
}

func NewSqlCOARepository() coa.COARepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlCOARepository{conn}
}

func NewSqlCOARepositoryV2(Conn *sql.DB) coa.COARepository {
	conn := Conn
	return &sqlCOARepository{conn}
}
func (sj *sqlCOARepository) Fetch(ctx context.Context, search string, keyword string) ([]*models.COA, error) {
	query := `
	SELECT 
		IsNull(Cast(ID as varchar(36)),'') as ID,
		IsNull(Cast(ParentCOAID as varchar(36)), '') as ParentCOAID,
		IsNull(Parent_COA_Name, '') as ParentCOAName,
		Code,
		Description,
		COACategory
	FROM dbo.vwCOA`
	if search != "ALL" && search != "" {
		query += ` where ` + search + ` like @p0`
	}
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("keyword", keyword),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"))
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.COA, 0)
	for rows.Next() {
		j := new(models.COA)
		err = rows.Scan(
			&j.ID,
			&j.ParentCOAID,
			&j.ParentCOAName,
			&j.Code,
			&j.Description,
			&j.COACategory,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlCOARepository) GetByProductID(ctx context.Context, productid *string) (*models.COA, error) {
	var j models.COA
	query := `
	SELECT   
		cast(ID as varchar(36)) as ID, Code, Description
	from 
		COA 
	where 
		ProductID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("id", j.ID),
		zap.String("code", j.Code),
		zap.String("description", j.Description),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", productid)).Scan(
		&j.ID,
		&j.Code,
		&j.Description,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlCOARepository) GetByID(ctx context.Context, id *string) (*models.COA, error) {
	var j models.COA
	query := `
		SELECT   
			cast(ID as varchar(36)) as ID, Code, Description
		from 
			COA 
		where 
			ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("id", j.ID),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.Code,
		&j.Description,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlCOARepository) GetByCode(ctx context.Context, code *string) (*models.COA, error) {
	var j models.COA
	query := `
	SELECT   
		cast(ID as varchar(36)) as ID, Code, Description
	from 
		COA 
	where 
	Code = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("id", j.ID),
		zap.String("code", j.Code),
		zap.String("description", j.Description),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&j.ID,
		&j.Code,
		&j.Description,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
