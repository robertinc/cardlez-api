package bank

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type BankRepository interface {
	Fetch(ctx context.Context, order string, first int32, count int32) (error, []*models.Bank)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	GetByID(ctx context.Context, id *string) *models.Bank
	Update(ctx context.Context, bank *models.Bank) (*models.Bank, error)
	Delete(ctx context.Context, bank *models.Bank) (bool, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Bank) (*models.Bank, error)
	GetByName(ctx context.Context, name *string) (*models.Bank, error)
	GetByCode(ctx context.Context, code *string) (*models.Bank, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
