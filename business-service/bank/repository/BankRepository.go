package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"

	"gitlab.com/robertinc/cardlez-api/business-service/bank"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlBankRepository struct {
	Conn *sql.DB
}

func NewSqlBankRepository() bank.BankRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlBankRepository{conn}
}

func NewSqlBankRepositoryV2(Conn *sql.DB) bank.BankRepository {
	conn := Conn
	return &sqlBankRepository{conn}
}

func (sj *sqlBankRepository) Fetch(ctx context.Context, order string, first int32, count int32) (error, []*models.Bank) {
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		Code,
		BankName,
		Isnull(TransferCode,'') as TransferCode
	FROM dbo.Bank
	ORDER BY 
	Case When @p2 = 'ID' then Cast(ID as varchar(36))
		 When @p2 = 'Code' then Code
		 When @p2 = 'BankName' then BankName
		 When @p2 = 'TransferCode' then TransferCode End
	OFFSET @p0 ROWS FETCH NEXT @p1 ROWS ONLY;`
	result := make([]*models.Bank, 0)

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("first", fmt.Sprintf("%v", first)),
		zap.String("count", fmt.Sprintf("%v", count)),
		zap.String("order", fmt.Sprintf("%v", order)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", first),
		sql.Named("p1", count),
		sql.Named("p2", order),
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Bank)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.BankName,
			&j.TransferCode,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlBankRepository) GetByID(ctx context.Context, id *string) *models.Bank {
	var bank models.Bank
	logger := service.Logger(ctx)
	query := `
	select 
		cast(ID as varchar(36)) ID, 
		Code, 
		BankName,
		Isnull(TransferCode,'') as TransferCode
	from 
		dbo.Bank 
	where 
		ID = @p0`

	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	row := sj.Conn.QueryRow(query,
		sql.Named("p0", id))
	err := row.Scan(
		&bank.ID,
		&bank.Code,
		&bank.BankName,
		&bank.TransferCode)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return &bank
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", bank)),
	)
	return &bank
}

func (sj *sqlBankRepository) Store(ctx context.Context, a *models.Bank) (*models.Bank, error) {
	a.ID = guid.New().StringUpper()
	bankQuery := `INSERT INTO bank (id, code, bankName,transferCode) VALUES (@p0, @p1, @p2,@p3)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
		zap.String("bankName", fmt.Sprintf("%v", a.BankName)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.BankName),
		sql.Named("p3", a.TransferCode),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlBankRepository) Update(ctx context.Context, a *models.Bank) (*models.Bank, error) {

	bankQuery := `Update Bank Set Code = @p1, BankName = @p2, TransferCode = @p3 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("code", fmt.Sprintf("%v", a.Code)),
		zap.String("bankName", fmt.Sprintf("%v", a.BankName)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.BankName),
		sql.Named("p2", a.TransferCode),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlBankRepository) Delete(ctx context.Context, a *models.Bank) (bool, error) {

	bankQuery := `
		delete from bank Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", bankQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
	)
	_, err := sj.Conn.ExecContext(ctx, bankQuery,
		sql.Named("p0", a.ID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return true, nil
}

func (sj *sqlBankRepository) GetByName(ctx context.Context, name *string) (*models.Bank, error) {
	var bank models.Bank
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code, 
			BankName,
			Isnull(TransferCode,'') as TransferCode
		from 
			dbo.Bank 
		where 
			BankName = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("bankName", fmt.Sprintf("%v", bank.BankName)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", name)).Scan(
		&bank.ID,
		&bank.Code,
		&bank.BankName,
		&bank.TransferCode)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", bank)),
	)
	return &bank, nil
}

func (sj *sqlBankRepository) GetByCode(ctx context.Context, code *string) (*models.Bank, error) {
	var bank models.Bank
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Code, 
			BankName,
			isnull(TransferCode,'') as TransferCode
		from 
			dbo.Bank 
		where 
			Code = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("cpde", fmt.Sprintf("%v", code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code)).Scan(
		&bank.ID,
		&bank.Code,
		&bank.BankName,
		&bank.TransferCode)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", bank)),
	)
	return &bank, nil
}
