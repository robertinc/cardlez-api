package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/bank"
	"gitlab.com/robertinc/cardlez-api/business-service/bank/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type BankUsecase struct {
	BankRepository bank.BankRepository
	contextTimeout time.Duration
}

func NewBankUsecase() bank.BankUsecase {
	return &BankUsecase{
		BankRepository: repository.NewSqlBankRepository(),
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func NewBankUsecaseV2(bankRepo bank.BankRepository) bank.BankUsecase {
	return &BankUsecase{
		BankRepository: bankRepo,
		contextTimeout: time.Second * time.Duration(models.Timeout()),
	}
}

func (a *BankUsecase) Fetch(c context.Context, order string, first int32, count int32) (error, []*models.Bank) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listBank := a.BankRepository.Fetch(ctx, order, first, count)
	if err != nil {
		return err, nil
	}
	return nil, listBank
}

func (a *BankUsecase) GetByID(c context.Context, id *string) *models.Bank {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listBank := a.BankRepository.GetByID(ctx, id)

	return listBank
}

func (a *BankUsecase) GetByCode(c context.Context, code *string) *models.Bank {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listBank, err := a.BankRepository.GetByCode(ctx, code)
	if err != nil {
		return nil
	}

	return listBank
}

func (a *BankUsecase) GetByName(c context.Context, name *string) (*models.Bank, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listBank, err := a.BankRepository.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return listBank, nil
}

func (a *BankUsecase) Store(c context.Context, b *models.Bank) (*models.Bank, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	bank, err := a.BankRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return bank, nil
}

func (a *BankUsecase) Update(c context.Context, b *models.Bank) (*models.Bank, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	bank, err := a.BankRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return bank, nil
}

func (a *BankUsecase) Delete(c context.Context, b *models.Bank) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	bank, err := a.BankRepository.Delete(ctx, b)

	if err != nil {
		return false, err
	}
	return bank, nil
}
