package bank

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type BankUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Bank, string, error)
	GetByID(ctx context.Context, id *string) *models.Bank
	GetByCode(ctx context.Context, code *string) *models.Bank
	Update(ctx context.Context, ar *models.Bank) (*models.Bank, error)
	Delete(ctx context.Context, ar *models.Bank) (bool, error)
	// GetByTitle(ctx context.Context, title string) (*models.Bank, error)
	Store(context.Context, *models.Bank) (*models.Bank, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, order string, first int32, count int32) (error, []*models.Bank)
	GetByName(ctx context.Context, name *string) (*models.Bank, error)
}
