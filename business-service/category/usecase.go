package category

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CategoryUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Category, string, error)
	GetByID(ctx context.Context, id *string) *models.Category
	Update(ctx context.Context, ar *models.Category) (*models.Category, error)
	// GetByTitle(ctx context.Context, title string) (*models.Category, error)
	Store(context.Context, *models.Category) (*models.Category, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context) (error, []*models.Category)
}
