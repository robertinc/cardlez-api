package category

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type CategoryRepository interface {
	Fetch(ctx context.Context) (error, []*models.Category)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	GetByID(ctx context.Context, id *string) *models.Category
	Update(ctx context.Context, category *models.Category) (*models.Category, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	Store(ctx context.Context, a *models.Category) (*models.Category, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
