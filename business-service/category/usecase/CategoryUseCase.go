package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/category"
	"gitlab.com/robertinc/cardlez-api/business-service/category/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type CategoryUsecase struct {
	CategoryRepository category.CategoryRepository
	contextTimeout     time.Duration
}

func NewCategoryUsecase() category.CategoryUsecase {
	return &CategoryUsecase{
		CategoryRepository: repository.NewSqlCategoryRepository(),
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func NewCategoryUsecaseV2(categoryRepo category.CategoryRepository) category.CategoryUsecase {
	return &CategoryUsecase{
		CategoryRepository: categoryRepo,
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *CategoryUsecase) Fetch(c context.Context) (error, []*models.Category) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listCategory := a.CategoryRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listCategory
}

func (a *CategoryUsecase) GetByID(c context.Context, id *string) *models.Category {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listCategory := a.CategoryRepository.GetByID(ctx, id)

	return listCategory
}

func (a *CategoryUsecase) Store(c context.Context, b *models.Category) (*models.Category, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	category, err := a.CategoryRepository.Store(ctx, b)

	if err != nil {
		return nil, err
	}
	return category, nil
}

func (a *CategoryUsecase) Update(c context.Context, b *models.Category) (*models.Category, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	category, err := a.CategoryRepository.Update(ctx, b)

	if err != nil {
		return nil, err
	}
	return category, nil
}
