package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/beevik/guid"
	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/category"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlCategoryRepository struct {
	Conn *sql.DB
}

func NewSqlCategoryRepository() category.CategoryRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlCategoryRepository{conn}
}

func NewSqlCategoryRepositoryV2(Conn *sql.DB) category.CategoryRepository {
	conn := Conn
	return &sqlCategoryRepository{conn}
}

func (sj *sqlCategoryRepository) Fetch(ctx context.Context) (error, []*models.Category) {

	result := make([]*models.Category, 0)
	query := `
		SELECT 
			Cast(ID as varchar(36)) as ID,
			Name
		FROM dbo.Category`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query) 
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.Category)
		err = rows.Scan(
			&j.ID,
			&j.Name,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlCategoryRepository) GetByID(ctx context.Context, id *string) *models.Category {
	var category models.Category
	query := `
		select 
			cast(ID as varchar(36)) ID, 
			Name
		from 
			dbo.Category 
		where 
			ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&category.ID,
		&category.Name,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return &category
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", category)),
	)
	return &category
}

func (sj *sqlCategoryRepository) Store(ctx context.Context, a *models.Category) (*models.Category, error) {
	a.ID = guid.New().StringUpper()
	categoryQuery := `INSERT INTO category (id, Name) VALUES (@p0, @p1)`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", categoryQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("name", fmt.Sprintf("%v", a.Name)),
	)
	_, err := sj.Conn.ExecContext(ctx, categoryQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Name),
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}

func (sj *sqlCategoryRepository) Update(ctx context.Context, a *models.Category) (*models.Category, error) {

	categoryQuery := `Update Category Set Name = @p2 Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", categoryQuery)),
		zap.String("id", fmt.Sprintf("%v", a.ID)),
		zap.String("name", fmt.Sprintf("%v", a.Name)),
	)
	_, err := sj.Conn.ExecContext(ctx, categoryQuery,
		sql.Named("p0", a.ID),
		sql.Named("p2", a.Name),
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", a)),
	)
	return a, nil
}
