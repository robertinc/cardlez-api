package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	cAppConfig "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	"gitlab.com/robertinc/cardlez-api/business-service/report"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlReportRepository struct {
	Conn *sql.DB
}

func NewSqlReportRepository() report.ReportRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlReportRepository{conn}
}

func NewSqlReportRepositoryV2(Conn *sql.DB) report.ReportRepository {
	conn := Conn
	return &sqlReportRepository{conn}
}
func (sj *sqlReportRepository) FetchPaymentProvince(ctx context.Context) (error, []*models.ReportPaymentProvince) {
	query := `
		select 
			Sum(mp.Amount), isnull(mb.Province, '') as Province
		from
			MemberPayment mp
			Inner Join Account a on mp.CreditAccountID = a.ID
			Inner Join MemberBranch mb on a.MemberID = mb.ID
			Inner Join Member m2 on m2.ID = mb.MemberID
			Inner Join Category c2 on m2.CategoryID = c2.ID
		Group By 
			mb.Province
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.ReportPaymentProvince, 0)
	for rows.Next() {
		j := new(models.ReportPaymentProvince)
		err = rows.Scan(
			&j.Amount,
			&j.Province,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlReportRepository) FetchPaymentCity(ctx context.Context, province string) (error, []*models.ReportPaymentCity) {
	query := `
	select 
		Sum(mp.Amount), Isnull(mb.City, '')
	from
		MemberPayment mp
		Left Join Account a on mp.CreditAccountID = a.ID
		Left Join MemberBranch mb on a.MemberID = mb.ID
		Left Join Member m2 on m2.ID = mb.MemberID
		Left Join Category c2 on m2.CategoryID = c2.ID
	Where Province = @Province
	Group By 
		mb.City
		`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("province", fmt.Sprintf("%v", province)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("Province", province))
	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.ReportPaymentCity, 0)
	for rows.Next() {
		j := new(models.ReportPaymentCity)
		err = rows.Scan(
			&j.Amount,
			&j.City,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlReportRepository) FetchPaymentArea(ctx context.Context, city string) (error, []*models.ReportPaymentArea) {
	query := `
	select 
		Sum(mp.Amount), Isnull(ar.Area, 'Others'), Isnull(cast(mb.AreaID as varchar(36)), '')
	from
		MemberPayment mp
		Left Join Account a on mp.CreditAccountID = a.ID
		Left Join MemberBranch mb on a.MemberID = mb.ID
		Left Join Area ar on mb.AreaID = ar.ID
	Where City = @City
	Group By 
		ar.Area, mb.AreaID
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("city", fmt.Sprintf("%v", city)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("City", city))

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.ReportPaymentArea, 0)
	for rows.Next() {
		j := new(models.ReportPaymentArea)
		err = rows.Scan(
			&j.Amount,
			&j.Area,
			&j.ID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlReportRepository) FetchDetailCategory(ctx context.Context, area string) (error, []*models.ReportCategory) {
	query := `
	select 
		Sum(mp.Amount), Isnull(c2.Name, '')
	from
		MemberPayment mp
		Left Join Account a on mp.CreditAccountID = a.ID
		Left Join MemberBranch mb on a.MemberID = mb.ID
		Left Join Member m2 on m2.ID = mb.MemberID
		Left Join Category c2 on m2.CategoryID = c2.ID
	Where mb.AreaID = @Area
	Group By 
		c2.Name
		`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("area", fmt.Sprintf("%v", area)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("Area", area))

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.ReportCategory, 0)
	for rows.Next() {
		j := new(models.ReportCategory)
		err = rows.Scan(
			&j.Amount,
			&j.Category,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlReportRepository) FetchMerchantCategory(ctx context.Context, province string) (error, []*models.ReportCategory) {
	query := `
	select 
		Sum(mp.Amount), Isnull(mb.City, '')
	from
		MemberPayment mp
		Left Join Account a on mp.CreditAccountID = a.ID
		Left Join MemberBranch mb on a.MemberID = mb.ID
		Left Join Member m2 on m2.ID = mb.MemberID
		Left Join Category c2 on m2.CategoryID = c2.ID
	Where Province = @Province
	Group By 
		mb.City
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("province", fmt.Sprintf("%v", province)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("Province", province))

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.ReportCategory, 0)
	for rows.Next() {
		j := new(models.ReportCategory)
		err = rows.Scan(
			&j.Amount,
			&j.Category,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlReportRepository) FetchMessageLog(ctx context.Context, startDate *string, endDate *string) (error, []*models.MessageLog) {
	query := `
	select 
		cast(ID as varchar(36)) ID,
		ISNULL(CONVERT(VARCHAR(10), TransactionDate, 103) + ' '  + convert(VARCHAR(8), TransactionDate, 14) ,'') as TransactionDate,
		ISNULL(Message,'') as Message,
		ISNULL(Status,'') as Status,
		ISNULL(Sender,'') as Sender,
		CASE 
			WHEN Type = 1 THEN 'SMS'
			WHEN Type = 2 THEN 'EMAIL'
			ELSE ''
		END AS Tipe,
		cast(UserInsert as varchar(36)) UserInsert
	from
		SMSEmailSenderLog
		`

	if startDate != nil && endDate != nil {
		query = query + " where DateInsert >= '" + *startDate + " 00:00:00' and DateInsert <= '" + *endDate + " 23:59:59'"
	}

	query = query + " order by DateInsert desc"

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("startDate", fmt.Sprintf("%v", startDate)),
		zap.String("endDate", fmt.Sprintf("%v", endDate)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", startDate),
		sql.Named("p1", endDate))
	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.MessageLog, 0)
	for rows.Next() {
		j := new(models.MessageLog)
		err = rows.Scan(
			&j.ID,
			&j.TransactionDate,
			&j.Message,
			&j.Status,
			&j.Sender,
			&j.Tipe,
			&j.UserInsert,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlReportRepository) GetMessageLogById(ctx context.Context, id *string) (error, *models.MessageLog) {
	var j models.MessageLog
	query := `
	select 
		cast(ID as varchar(36)) ID,
		ISNULL(CONVERT(VARCHAR(10), TransactionDate, 103) + ' '  + convert(VARCHAR(8), TransactionDate, 14) ,'') as TransactionDate,
		ISNULL(Message,'') as Message,
		ISNULL(Status,'') as Status,
		ISNULL(Sender,'') as Sender,
		CASE 
			WHEN Type = 1 THEN 'SMS'
			WHEN Type = 2 THEN 'EMAIL'
			ELSE ''
		END AS Tipe,
		cast(UserInsert as varchar(36)) UserInsert
	from
		SMSEmailSenderLog
	where
		ID = @p0
		`

	query = query + " order by DateInsert desc"

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("ID", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&j.ID,
		&j.TransactionDate,
		&j.Message,
		&j.Status,
		&j.Sender,
		&j.Tipe,
		&j.UserInsert,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}
func (sj *sqlReportRepository) FetchFinalJournalReport(ctx context.Context, date string) (error, []*models.FinalJournalReport) {
	query := `
		select isnull(b.Code,'') as CoaNo
		,isnull(b.Description,'') as CoaName
		,isnull(c.Code,'') as NoRekening
		,isnull(c.AccountName,'') as NamaRekening
		,isnull(a.DebitMovement,0) as DebitMovement
		,isnull(a.CreditMovement,0) as CreditMovement
		,case when (a.DebitMovement - a.CreditMovement) > 0 then (a.DebitMovement - a.CreditMovement) else 0 end as SelisihJournalDebet
		,case when (a.CreditMovement - a.DebitMovement) > 0 then (a.CreditMovement - a.DebitMovement) else 0 end as SelisihJournalCredit
		from AccountingBalance a
		inner join COA b on a.COAID = b.ID
		left join Account c on a.ReferenceNumber = c.ID
		where a.DateInsert>='` + date + ` 00:00:00' and a.DateInsert<='` + date + ` 23:59:59'
		union all
		select ''
		,''
		,''
		,'Total'
		,isnull(sum(a.DebitMovement),0)
		,isnull(sum(a.CreditMovement),0)
		,isnull(sum(case when (a.DebitMovement - a.CreditMovement) > 0 then (a.DebitMovement - a.CreditMovement) else 0 end),0) as SelisihJournalDebet
		,isnull(sum(case when (a.CreditMovement - a.DebitMovement) > 0 then (a.CreditMovement - a.DebitMovement) else 0 end),0) as SelisihJournalCredit
		from AccountingBalance a
		inner join COA b on a.COAID = b.ID
		left join Account c on a.ReferenceNumber = c.ID
		where a.DateInsert>='` + date + ` 00:00:00' and a.DateInsert<='` + date + ` 23:59:59'
		`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("date", fmt.Sprintf("%v", date)),
	)
	rows, err := sj.Conn.Query(query)
	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.FinalJournalReport, 0)
	for rows.Next() {
		j := new(models.FinalJournalReport)
		err = rows.Scan(
			&j.CoaNo,
			&j.CoaName,
			&j.NoRekening,
			&j.NamaRekening,
			&j.DebitMovement,
			&j.CreditMovement,
			&j.SelisihDebitMovement,
			&j.SelisihCreditMovement,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlReportRepository) GetReportWithdrawal(ctx context.Context) (error, *models.ReportWithdrawal) {
	configKey := "AccountID Invelli"
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)

	invAccID, error := appConfigRepo.GetByConfigKey(ctx, &configKey)
	if error != nil {
		return error, nil
	}
	var j models.ReportWithdrawal
	query := `
		select 
		isnull((select sum(OnlineBalance) from AccountingAccounts a inner join coa b on a.COAID=b.ID where b.Code='100.02.08.01' or b.Code='100.02.01.05.01'),0) as SaldoSimpananMobile
		, isnull((select sum(OnlineBalance) from AccountingAccounts a inner join coa b on a.COAID=b.ID where b.Code='100.01.02.04'),0) as SaldoVAPermata
		, isnull((select sum(OnlineBalance) from AccountingAccounts a inner join COA b on a.COAID=b.ID where b.Code='100.02.30.05' or b.Code='100.02.30.04'),0) as SaldoRekeningBiller	
		, isnull((select sum(OnlineBalance) from AccountingAccounts a inner join coa b on a.COAID=b.ID where (b.Code='100.02.08.01' or b.Code='100.02.01.05.01') and a.ReferenceNumber=@p0),0) as SaldoRekeningInvelli
		`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", invAccID.ConfigValue)).Scan(
		&j.SaldoSimpananMobile,
		&j.SaldoVAPermata,
		&j.SaldoRekeningBiller,
		&j.SaldoRekeningInvelli,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}
