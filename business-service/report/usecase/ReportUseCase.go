package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/report"
	"gitlab.com/robertinc/cardlez-api/business-service/report/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type ReportUsecase struct {
	ReportRepository report.ReportRepository
	contextTimeout   time.Duration
}

func NewReportUsecase() report.ReportUsecase {
	return &ReportUsecase{
		ReportRepository: repository.NewSqlReportRepository(),
		contextTimeout:   time.Second * time.Duration(models.Timeout()),
	}
}

func NewReportUsecaseV2(repo report.ReportRepository) report.ReportUsecase {
	return &ReportUsecase{
		ReportRepository: repo,
		contextTimeout:   time.Second * time.Duration(models.Timeout()),
	}
}

func (a *ReportUsecase) FetchPaymentProvince(c context.Context) (error, []*models.ReportPaymentProvince) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listReport := a.ReportRepository.FetchPaymentProvince(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listReport
}

func (a *ReportUsecase) FetchPaymentCity(c context.Context, province string) (error, []*models.ReportPaymentCity) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listReport := a.ReportRepository.FetchPaymentCity(ctx, province)
	if err != nil {
		return err, nil
	}
	return nil, listReport
}
func (a *ReportUsecase) FetchPaymentArea(ctx context.Context, city string) (error, []*models.ReportPaymentArea) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listReport := a.ReportRepository.FetchPaymentArea(ctx, city)
	if err != nil {
		return err, nil
	}
	return nil, listReport
}
func (a *ReportUsecase) FetchMerchantCategory(ctx context.Context, city string) (error, []*models.ReportCategory) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listReport := a.ReportRepository.FetchMerchantCategory(ctx, city)
	if err != nil {
		return err, nil
	}
	return nil, listReport
}
func (a *ReportUsecase) FetchDetailCategory(ctx context.Context, area string) (error, []*models.ReportCategory) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listReport := a.ReportRepository.FetchDetailCategory(ctx, area)
	if err != nil {
		return err, nil
	}
	return nil, listReport
}

func (a *ReportUsecase) FetchMessageLog(ctx context.Context, startDate *string, endDate *string) (error, []*models.MessageLog) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listMessageLog := a.ReportRepository.FetchMessageLog(ctx, startDate, endDate)
	if err != nil {
		return err, nil
	}
	return nil, listMessageLog
}

func (a *ReportUsecase) GetMessageLogById(ctx context.Context, id *string) (error, *models.MessageLog) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listMessageLog := a.ReportRepository.GetMessageLogById(ctx, id)
	if err != nil {
		return err, nil
	}
	return nil, listMessageLog
}
func (a *ReportUsecase) FetchFinalJournalReport(ctx context.Context, date string) (error, []*models.FinalJournalReport) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, listFinalJournals := a.ReportRepository.FetchFinalJournalReport(ctx, date)
	if err != nil {
		return err, nil
	}
	return nil, listFinalJournals
}
func (a *ReportUsecase) GetReportWithdrawal(ctx context.Context) (error, *models.ReportWithdrawal) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, reportWithdrawal := a.ReportRepository.GetReportWithdrawal(ctx)
	if err != nil {
		return err, nil
	}

	return nil, reportWithdrawal
}
