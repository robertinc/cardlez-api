package report

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ReportRepository interface {
	FetchPaymentProvince(ctx context.Context) (error, []*models.ReportPaymentProvince)
	FetchPaymentCity(ctx context.Context, province string) (error, []*models.ReportPaymentCity)
	FetchPaymentArea(ctx context.Context, city string) (error, []*models.ReportPaymentArea)
	FetchMerchantCategory(ctx context.Context, city string) (error, []*models.ReportCategory)
	FetchDetailCategory(ctx context.Context, area string) (error, []*models.ReportCategory)
	FetchMessageLog(ctx context.Context, startDate *string, endDate *string) (error, []*models.MessageLog)
	GetMessageLogById(ctx context.Context, id *string) (error, *models.MessageLog)
	FetchFinalJournalReport(ctx context.Context, date string) (error, []*models.FinalJournalReport)
	GetReportWithdrawal(ctx context.Context) (error, *models.ReportWithdrawal)
}
