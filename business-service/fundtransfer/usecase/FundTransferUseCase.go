package usecase

import (
	"context"
	"database/sql"
	"math/rand"
	"strconv"
	"time"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/fundtransfer/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	"gitlab.com/robertinc/cardlez-api/service"

	//accountrepository "gitlab.com/robertinc/cardlez-api/business-service/account/repository"
	// appconfigrepository "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration/repository"
	// bankrepository "gitlab.com/robertinc/cardlez-api/business-service/bank/repository"
	// coarepository "gitlab.com/robertinc/cardlez-api/business-service/coa/repository"
	// journalmappingrepository "gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	//memberrepository "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	cAccount "gitlab.com/robertinc/cardlez-api/business-service/account"
	cAppConfig "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	cBank "gitlab.com/robertinc/cardlez-api/business-service/bank"
	cCOA "gitlab.com/robertinc/cardlez-api/business-service/coa"
	cFundTransfer "gitlab.com/robertinc/cardlez-api/business-service/fundtransfer"
	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
	cMember "gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/models"
)

type FundTransferUsecase struct {
	FundTransferRepository cFundTransfer.FundTransferRepository
	contextTimeout         time.Duration
}

func NewFundTransferUsecase() cFundTransfer.FundTransferUseCase {
	return &FundTransferUsecase{
		FundTransferRepository: repository.NewSqlFundTransferRepository(),
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}

func NewFundTransferUsecaseV2(repo cFundTransfer.FundTransferRepository) cFundTransfer.FundTransferUseCase {
	return &FundTransferUsecase{
		FundTransferRepository: repo,
		contextTimeout:         time.Second * time.Duration(models.Timeout()),
	}
}

func (a *FundTransferUsecase) Fetch(c context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listFundtransfer := a.FundTransferRepository.Fetch(ctx, search, keyword, recent, transferType, startDate, endDate)
	if err != nil {
		return err, nil
	}
	return nil, listFundtransfer
}
func (a *FundTransferUsecase) FetchBase(c context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listFundtransfer := a.FundTransferRepository.FetchBase(ctx, search, keyword, recent, transferType, startDate, endDate)
	if err != nil {
		return err, nil
	}
	return nil, listFundtransfer
}
func (a *FundTransferUsecase) SettleTransfer(c context.Context, b []*models.FundTransfer, reffNo string) ([]*models.FundTransfer, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if len(b) > 0 {
		//Insert To Journal
		err := a.InsertJournalSettleTransfer(ctx, b, reffNo)
		if err != nil {
			return nil, err
		}
		b, err = a.FundTransferRepository.SettleTransfer(ctx, b, reffNo, 4)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}
func (a *FundTransferUsecase) SettleTransferByDate(c context.Context, b []*models.FundTransfer, reffNo string, companyDate string) ([]*models.FundTransfer, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if len(b) > 0 {
		//Insert To Journal
		err := a.InsertJournalSettleTransferByDate(ctx, b, reffNo, companyDate)
		if err != nil {
			return nil, err
		}
		b, err = a.FundTransferRepository.SettleTransferByDate(ctx, b, reffNo, 4, companyDate)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}
func (a *FundTransferUsecase) InsertJournalSettleTransfer(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string) error {
	configRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	keyCore := "Biaya Transfer"
	biayaTransfer, err := configRepo.GetByConfigKey(ctx, &keyCore)
	if err != nil {
		return err
	}
	biayaConvert, err := strconv.ParseFloat(biayaTransfer.ConfigValue, 64)
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := configRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0023")
	if err != nil {
		return err
	}
	keyword2 := "COACreditSettlementTransferBankLain"
	journalMappingDetailEscrow, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	escrowConvert, err := strconv.ParseFloat(journalMappingDetailEscrow.DefaultValue, 64)
	if err != nil {
		return err
	}
	reffNo := nomorTransaksiSettlement
	totalEscrow := 0.0
	bagiHasil := biayaConvert / 2
	bagiHasil2 := (biayaConvert - escrowConvert) / 2
	totalBagiHasil := 0.0
	totalKWS := 0.0
	totalBagiHasilOVB := 0.0
	totalKWSOVB := 0.0
	totalAmountTransferBank := 0.0
	totalAmountOverbooking := 0.0
	for _, p := range b {
		if p.TransferType == 2 {
			totalAmountTransferBank += p.Amount
			totalEscrow = totalEscrow + escrowConvert
			totalBagiHasil = totalBagiHasil + bagiHasil2
			totalKWS = totalKWS + biayaConvert
		} else if p.TransferType == 3 {
			totalAmountOverbooking += p.Amount
			totalBagiHasilOVB = totalBagiHasilOVB + bagiHasil
			totalKWSOVB = totalKWSOVB + biayaConvert
		}

	}
	if totalAmountTransferBank != 0.0 {
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0023")
		if err != nil {
			return err
		}
		keyword := "COADebetKWSSettlementTransferBankLain"
		keyword2 := "COACreditSettlementTransferBankLain"
		keyword3 := "COACreditPendapatanSettlementTransferBankLain"
		keyword4 := "COACreditRekSettlementTransferBankLain"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
		if err != nil {
			return err
		}
		description := "Settlement Transfer ke Bank Lain"
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(description),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalKWS,
			Credit:          0,
			Notes:           "Kewajiban Segera Administrasi Transfer Permata VA",
			ReferenceNumber: "",
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalEscrow,
			Notes:           "Escrow-Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID2 := guid.New().StringUpper()
		journalDetailCredit2 := models.JournalDetail{
			ID:              journalDetailCreditID2,
			JournalID:       journal.ID,
			COAID:           COA3.ID,
			Debit:           0,
			Credit:          totalBagiHasil,
			Notes:           "Pendapatan Administrasi Transfer Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID3 := guid.New().StringUpper()
		journalDetailCredit3 := models.JournalDetail{
			ID:              journalDetailCreditID3,
			JournalID:       journal.ID,
			COAID:           COA4.ID,
			Debit:           0,
			Credit:          totalBagiHasil,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit2)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit3)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}
	if totalAmountOverbooking != 0.0 {
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0024")
		if err != nil {
			return err
		}
		keyword := "COADebetKWSSettlementTransferPermata"
		keyword2 := "COACreditPendapatanSettlementTransferPermata"
		keyword3 := "COACreditRekSettlementTransferPermata"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		description := "Settlement Transfer ke Bank Permata (Overbooking)"
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString(description),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalKWSOVB,
			Credit:          0,
			Notes:           "Kewajiban Segera Administrasi Transfer Permata VA",
			ReferenceNumber: "",
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalBagiHasilOVB,
			Notes:           "Pendapatan Administrasi Transfer Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID2 := guid.New().StringUpper()
		journalDetailCredit2 := models.JournalDetail{
			ID:              journalDetailCreditID2,
			JournalID:       journal.ID,
			COAID:           COA3.ID,
			Debit:           0,
			Credit:          totalBagiHasilOVB,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005", //4F584C08-4832-40CA-A08E-99E005452AAB
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit2)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}
	return nil
}
func (a *FundTransferUsecase) InsertJournalSettleTransferByDate(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string, companyDate string) error {
	configRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)

	keyCore := "Biaya Transfer"
	biayaTransfer, err := configRepo.GetByConfigKey(ctx, &keyCore)
	if err != nil {
		return err
	}
	biayaConvert, err := strconv.ParseFloat(biayaTransfer.ConfigValue, 64)
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := configRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0023")
	if err != nil {
		return err
	}
	keyword2 := "COACreditSettlementTransferBankLain"
	journalMappingDetailEscrow, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
	if err != nil {
		return err
	}
	escrowConvert, err := strconv.ParseFloat(journalMappingDetailEscrow.DefaultValue, 64)
	if err != nil {
		return err
	}
	reffNo := nomorTransaksiSettlement
	totalEscrow := 0.0
	bagiHasil := biayaConvert / 2
	bagiHasil2 := (biayaConvert - escrowConvert) / 2
	totalBagiHasil := 0.0
	totalKWS := 0.0
	totalBagiHasilOVB := 0.0
	totalKWSOVB := 0.0
	totalAmountTransferBank := 0.0
	totalAmountOverbooking := 0.0
	for _, p := range b {
		if p.TransferType == 2 {
			totalAmountTransferBank += p.Amount
			totalEscrow = totalEscrow + escrowConvert
			totalBagiHasil = totalBagiHasil + bagiHasil2
			totalKWS = totalKWS + biayaConvert
		} else if p.TransferType == 3 {
			totalAmountOverbooking += p.Amount
			totalBagiHasilOVB = totalBagiHasilOVB + bagiHasil
			totalKWSOVB = totalKWSOVB + biayaConvert
		}

	}
	if totalAmountTransferBank != 0.0 {
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0023")
		if err != nil {
			return err
		}
		keyword := "COADebetKWSSettlementTransferBankLain"
		keyword2 := "COACreditSettlementTransferBankLain"
		keyword3 := "COACreditPendapatanSettlementTransferBankLain"
		keyword4 := "COACreditRekSettlementTransferBankLain"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail4, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword4)
		if err != nil {
			return err
		}
		COA4, err := coaRepo.GetByCode(ctx, &journalMappingDetail4.Code.String)
		if err != nil {
			return err
		}
		description := "Settlement Transfer ke Bank Lain"
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      companyDate,
			Description:      service.NewNullString(description),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalKWS,
			Credit:          0,
			Notes:           "Kewajiban Segera Administrasi Transfer Permata VA",
			ReferenceNumber: "",
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalEscrow,
			Notes:           "Escrow-Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID2 := guid.New().StringUpper()
		journalDetailCredit2 := models.JournalDetail{
			ID:              journalDetailCreditID2,
			JournalID:       journal.ID,
			COAID:           COA3.ID,
			Debit:           0,
			Credit:          totalBagiHasil,
			Notes:           "Pendapatan Administrasi Transfer Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID3 := guid.New().StringUpper()
		journalDetailCredit3 := models.JournalDetail{
			ID:              journalDetailCreditID3,
			JournalID:       journal.ID,
			COAID:           COA4.ID,
			Debit:           0,
			Credit:          totalBagiHasil,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit2)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit3)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}
	if totalAmountOverbooking != 0.0 {
		journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0024")
		if err != nil {
			return err
		}
		keyword := "COADebetKWSSettlementTransferPermata"
		keyword2 := "COACreditPendapatanSettlementTransferPermata"
		keyword3 := "COACreditRekSettlementTransferPermata"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword2)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalMappingDetail3, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmapping.ID, "2", &keyword3)
		if err != nil {
			return err
		}
		COA3, err := coaRepo.GetByCode(ctx, &journalMappingDetail3.Code.String)
		if err != nil {
			return err
		}
		description := "Settlement Transfer ke Bank Permata (Overbooking)"
		journalID := guid.New().StringUpper()
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      companyDate,
			Description:      service.NewNullString(description),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           totalKWSOVB,
			Credit:          0,
			Notes:           "Kewajiban Segera Administrasi Transfer Permata VA",
			ReferenceNumber: "",
		}
		journalDetailCreditID := guid.New().StringUpper()
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          totalBagiHasilOVB,
			Notes:           "Pendapatan Administrasi Transfer Permata",
			ReferenceNumber: "",
		}
		journalDetailCreditID2 := guid.New().StringUpper()
		journalDetailCredit2 := models.JournalDetail{
			ID:              journalDetailCreditID2,
			JournalID:       journal.ID,
			COAID:           COA3.ID,
			Debit:           0,
			Credit:          totalBagiHasilOVB,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005", //4F584C08-4832-40CA-A08E-99E005452AAB
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit2)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}
	return nil
}
func (a *FundTransferUsecase) HistoryTransfer(c context.Context, memberid string, recent bool) (error, []*models.FundTransfer) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	var transferHistory, listTransfers []*models.FundTransfer
	var creditAccountIds []string

	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	//accountrepository := accountrepository.NewSqlAccountRepository()

	err, accounts := accountRepo.Fetch(ctx, "MemberID", memberid, nil)
	if err != nil {
		return err, nil
	}
	for _, transfer := range accounts {
		err, listTransfer := a.FundTransferRepository.HistoryTransfer(ctx, transfer.ID)
		if err != nil {
			return err, nil
		}
		listTransfers = append(listTransfers, listTransfer...)
	}

	for _, accountDestinationTransfer := range listTransfers {
		account, err := accountRepo.GetByID(ctx, &accountDestinationTransfer.CreditAccountID)
		if err != nil {
			return err, nil
		}

		if recent == true {
			hasInsert := false
			for _, creditAccountId := range creditAccountIds {
				if accountDestinationTransfer.CreditAccountID == creditAccountId {
					hasInsert = true
					break
				}
			}
			if hasInsert == true {
				continue
			}
			creditAccountIds = append(creditAccountIds, accountDestinationTransfer.CreditAccountID)
		}
		history := &models.FundTransfer{
			ID:                accountDestinationTransfer.ID,
			Code:              accountDestinationTransfer.Code,
			TransferDirection: accountDestinationTransfer.TransferDirection,
			DebitCompanyID:    accountDestinationTransfer.DebitCompanyID,
			CreditCompanyID:   accountDestinationTransfer.CreditCompanyID,
			DebitAccountID:    accountDestinationTransfer.DebitAccountID,
			CurrencyID:        accountDestinationTransfer.CurrencyID,
			Amount:            accountDestinationTransfer.Amount,
			DebitDate:         accountDestinationTransfer.DebitDate,
			BankCode:          accountDestinationTransfer.BankCode,
			BankAccountName:   accountDestinationTransfer.BankAccountName,
			TransferType:      accountDestinationTransfer.TransferType,
			CreditAccountID:   accountDestinationTransfer.CreditAccountID,
			CreditAccountName: accountDestinationTransfer.CreditAccountName,
			CreditDate:        accountDestinationTransfer.CreditDate,
			Note:              accountDestinationTransfer.Note,
			RevNo:             accountDestinationTransfer.RevNo,
			RecordStatus:      accountDestinationTransfer.RecordStatus,
			IsTab:             accountDestinationTransfer.IsTab,
			Remarks:           accountDestinationTransfer.Remarks,
			IsTabCredit:       accountDestinationTransfer.IsTabCredit,
			MemberID:          accountDestinationTransfer.MemberID,
			BankID:            accountDestinationTransfer.BankID,
			BankName:          accountDestinationTransfer.BankName,
			MemberName:        account.MemberName,
			Handphone:         account.Phone,
			AccountCode:       account.Code,
		}
		transferHistory = append(transferHistory, history)

	}
	return nil, transferHistory
}

func (a *FundTransferUsecase) InsertBase(c context.Context, b *models.FundTransfer) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	fundTransferRepo := ctx.Value("FundtransferRepository").(cFundTransfer.FundTransferRepository)

	needInsertToFundTransfer := true
	// Check Jika Pernah Inquiry, jika belum maka baru insert ke Table
	err, FundTransferBefore := fundTransferRepo.GetFundTransferBySessionID(ctx, b.SessionIDInquiry)
	if err == nil {
		//ada data lama tidak perlu create row baru
		needInsertToFundTransfer = false
	}

	insertJournal := false
	fundTransfer := &models.FundTransfer{}
	if b.TransferType == 1 {
		fundTransfer, err = fundTransferRepo.InsertBase(ctx, b)
		if err != nil {
			return nil, err
		}
		insertJournal = true
	} else {
		if needInsertToFundTransfer == true {
			fundTransfer, err = fundTransferRepo.InsertBase(ctx, b)
			if err != nil {
				return nil, err
			}
			insertJournal = true
		} else {
			fundTransfer = FundTransferBefore
		}
	}

	//Insert To Journal
	if insertJournal == true {
		err = a.InsertJournal(ctx, fundTransfer)
		if err != nil {
			return nil, err
		}
	}

	return fundTransfer, nil
}
func (a *FundTransferUsecase) InsertBaseByDate(c context.Context, b *models.FundTransfer, companyDate string) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	fundTransferRepo := ctx.Value("FundtransferRepository").(cFundTransfer.FundTransferRepository)

	needInsertToFundTransfer := true
	// Check Jika Pernah Inquiry, jika belum maka baru insert ke Table
	err, FundTransferBefore := fundTransferRepo.GetFundTransferBySessionID(ctx, b.SessionIDInquiry)
	if err == nil {
		//ada data lama tidak perlu create row baru
		needInsertToFundTransfer = false
	}

	insertJournal := false
	fundTransfer := &models.FundTransfer{}
	if b.TransferType == 1 {
		fundTransfer, err = fundTransferRepo.InsertBaseByDate(ctx, b, companyDate)
		if err != nil {
			return nil, err
		}
		insertJournal = true
	} else {
		if needInsertToFundTransfer == true {
			fundTransfer, err = fundTransferRepo.InsertBaseByDate(ctx, b, companyDate)
			if err != nil {
				return nil, err
			}
			insertJournal = true
		} else {
			fundTransfer = FundTransferBefore
		}
	}

	//Insert To Journal
	if insertJournal == true {
		err = a.InsertJournalByDate(ctx, fundTransfer)
		if err != nil {
			return nil, err
		}
	}

	return fundTransfer, nil
}

func (a *FundTransferUsecase) InsertBasePermata(c context.Context, b *models.FundTransfer) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// accountrepository := accountrepository.NewSqlAccountRepository()
	accountorigin, err := accountRepo.GetByCode(ctx, &b.AccountNoOrigin)
	if err != nil {
		return nil, err
	}
	b.ProductIDOrigin = accountorigin.ProductID

	if b.Code == "" {
		memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
		// memberrepository := memberrepository.NewSqlMemberRepository()
		err, b.Code = memberRepo.GenerateCode(ctx, "FTF")
		if err != nil {
			return nil, err
		}
	}
	b.ID = guid.New().StringUpper()
	b.DebitAccountID = accountorigin.ID
	b.DebitCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CreditCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CurrencyID = service.NewEmptyGuid()
	b.Remarks = sql.NullString{}
	b.CreditAccountID = service.NewEmptyGuid()
	if b.TransferType == 1 {
		if b.AccountNoDestination != "" {
			accountdestination, err := accountRepo.GetByCode(ctx, &b.AccountNoDestination)
			if err != nil {
				if err == sql.ErrNoRows {
					b.ProductIDDestination = ""
					b.CreditAccountID = ""
					b.NonMember = true
				} else {
					return nil, err
				}
			} else {
				b.ProductIDDestination = accountdestination.ProductID
				b.CreditAccountID = accountdestination.ID
				b.NonMember = false
			}
		} else {
			b.ProductIDDestination = ""
			b.NonMember = true
			rand.Seed(time.Now().UnixNano())
			token := service.RandInt(10000000, 99999999)
			token2 := service.RandInt(10000000, 99999999)
			b.Remarks = service.NewNullString(strconv.Itoa(token) + strconv.Itoa(token2))
		}
	}
	fundTransferRepo := ctx.Value("FundtransferRepository").(cFundTransfer.FundTransferRepository)

	needInsertToFundTransfer := true
	// Check Jika Pernah Inquiry, jika belum maka baru insert ke Table
	err, FundTransferBefore := fundTransferRepo.GetFundTransferBySessionID(ctx, b.SessionIDInquiry)
	if err == nil {
		//ada data lama tidak perlu create row baru
		needInsertToFundTransfer = false
	}

	insertJournal := false
	fundTransfer := &models.FundTransfer{}
	if b.TransferType == 1 {
		fundTransfer, err = fundTransferRepo.InsertBase(ctx, b)
		if err != nil {
			return nil, err
		}
		insertJournal = true
	} else {
		if needInsertToFundTransfer == true {
			fundTransfer, err = fundTransferRepo.InsertBase(ctx, b)
			if err != nil {
				return nil, err
			}
			insertJournal = true
		} else {
			fundTransfer = FundTransferBefore
		}
	}

	//Insert To Journal
	if insertJournal == true {
		err = a.InsertJournalPermata(ctx, fundTransfer)
		if err != nil {
			return nil, err
		}
	}

	return fundTransfer, nil
}
func (a *FundTransferUsecase) InsertBasePermataByDate(c context.Context, b *models.FundTransfer, companyDate string) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// accountrepository := accountrepository.NewSqlAccountRepository()
	accountorigin, err := accountRepo.GetByCode(ctx, &b.AccountNoOrigin)
	if err != nil {
		return nil, err
	}
	b.ProductIDOrigin = accountorigin.ProductID

	if b.Code == "" {
		memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
		// memberrepository := memberrepository.NewSqlMemberRepository()
		err, b.Code = memberRepo.GenerateCode(ctx, "FTF")
		if err != nil {
			return nil, err
		}
	}

	b.ID = guid.New().StringUpper()
	b.DebitAccountID = accountorigin.ID
	b.TransactionDate = companyDate
	b.DebitCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CreditCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CurrencyID = service.NewEmptyGuid()
	b.Remarks = sql.NullString{}
	b.CreditAccountID = service.NewEmptyGuid()
	if b.TransferType == 1 {
		b.FeeAmount = 0.0
		if b.AccountNoDestination != "" {
			accountdestination, err := accountRepo.GetByCode(ctx, &b.AccountNoDestination)
			if err != nil {
				if err == sql.ErrNoRows {
					b.ProductIDDestination = ""
					b.CreditAccountID = ""
					b.NonMember = true
				} else {
					return nil, err
				}
			} else {
				b.ProductIDDestination = accountdestination.ProductID
				b.CreditAccountID = accountdestination.ID
				b.NonMember = false
			}
		} else {
			b.ProductIDDestination = ""
			b.NonMember = true
			rand.Seed(time.Now().UnixNano())
			token := service.RandInt(10000000, 99999999)
			token2 := service.RandInt(10000000, 99999999)
			b.Remarks = service.NewNullString(strconv.Itoa(token) + strconv.Itoa(token2))
		}
	} else {
		appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return nil, err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		b.FeeAmount = FeeConvert
	}
	fundTransferRepo := ctx.Value("FundtransferRepository").(cFundTransfer.FundTransferRepository)

	needInsertToFundTransfer := true
	// Check Jika Pernah Inquiry, jika belum maka baru insert ke Table
	err, FundTransferBefore := fundTransferRepo.GetFundTransferBySessionID(ctx, b.SessionIDInquiry)
	if err == nil {
		//ada data lama tidak perlu create row baru
		needInsertToFundTransfer = false
	}

	insertJournal := false
	fundTransfer := &models.FundTransfer{}
	if b.TransferType == 1 {
		fundTransfer, err = fundTransferRepo.InsertBaseByDate(ctx, b, companyDate)
		if err != nil {
			return nil, err
		}
		insertJournal = true
	} else {
		if needInsertToFundTransfer == true {
			fundTransfer, err = fundTransferRepo.InsertBaseByDate(ctx, b, companyDate)
			if err != nil {
				return nil, err
			}
			insertJournal = true
		} else {
			fundTransfer = FundTransferBefore
		}
	}

	//Insert To Journal
	if insertJournal == true {
		err = a.InsertJournalPermataByDate(ctx, fundTransfer)
		if err != nil {
			return nil, err
		}
	}

	return fundTransfer, nil
}

type TransferBankData struct {
	FromAccount   string
	ToAccount     string
	ToBankId      string
	ToBankName    string
	Amount        float64
	ChargeTo      float64
	TrxDesc       string
	TrxDesc2      string
	BenefEmail    string
	BenefAcctName string
	BenefPhoneNo  string
	FromAcctName  string
	DatiII        string
	TkiFlag       string
}

func (a *FundTransferUsecase) GetById(c context.Context, id string) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, fundtransfer := a.FundTransferRepository.GetById(ctx, id)
	if err != nil {
		return nil, err
	}
	return fundtransfer, nil
}
func (a *FundTransferUsecase) Insert(c context.Context, b *models.FundTransfer) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	accountRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// accountrepository := accountrepository.NewSqlAccountRepository()
	accountorigin, err := accountRepo.GetByCode(ctx, &b.AccountNoOrigin)
	if err != nil {
		return nil, err
	}
	b.ProductIDOrigin = accountorigin.ProductID

	if b.Code == "" {
		memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
		// memberrepository := memberrepository.NewSqlMemberRepository()
		err, b.Code = memberRepo.GenerateCode(ctx, "FTF")
		if err != nil {
			return nil, err
		}
	}
	b.ID = guid.New().StringUpper()
	b.DebitAccountID = accountorigin.ID
	b.DebitCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CreditCompanyID = "15C55F75-85AD-4431-B1A6-DFBC7516925F"
	b.CurrencyID = service.NewEmptyGuid()
	b.Remarks = sql.NullString{}
	b.CreditAccountID = service.NewEmptyGuid()
	if b.TransferType == 1 {
		if b.AccountNoDestination != "" {
			accountdestination, err := accountRepo.GetByCode(ctx, &b.AccountNoDestination)
			if err != nil {
				if err == sql.ErrNoRows {
					b.ProductIDDestination = ""
					//b.CreditAccountID = ""
					b.NonMember = true
				} else {
					return nil, err
				}
			} else {
				b.ProductIDDestination = accountdestination.ProductID
				b.CreditAccountID = accountdestination.ID
				b.NonMember = false
			}
		} else {
			b.ProductIDDestination = ""
			b.NonMember = true
			rand.Seed(time.Now().UnixNano())
			token := service.RandInt(10000000, 99999999)
			token2 := service.RandInt(10000000, 99999999)
			b.Remarks = service.NewNullString(strconv.Itoa(token) + strconv.Itoa(token2))
		}
	}
	// fundtransferrepository := repository.NewSqlFundTransferRepository()
	fundTransferRepo := ctx.Value("FundtransferRepository").(cFundTransfer.FundTransferRepository)
	fundTransfer, err := fundTransferRepo.Insert(ctx, b)
	if err != nil {
		return nil, err
	}

	//Insert To Journal
	err = a.InsertJournal(ctx, fundTransfer)
	if err != nil {
		return nil, err
	}
	return fundTransfer, nil
}

func (a *FundTransferUsecase) InsertJournal(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}

	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		CreditAccount, err := accRepo.GetByID(ctx, &b.CreditAccountID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           "Transfer ke Teman atas nama " + CreditAccount.MemberName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           "Transfer dari Teman atas nama " + DebitAccount.MemberName,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0004") //Bank Transfer
		if err != nil {
			return err
		}

		//bankrepository := bankrepository.NewSqlBankRepository()
		bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		bank, err := bankRepo.GetByCode(ctx, &b.BankCode)
		if err != nil {
			return err
		}
		notes = "Transfer Ke " + bank.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     0,
			Credit:    b.Amount,
			Notes:     notes,
			//ReferenceNumber: nil,
		}
		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           FeeConvert,
			Credit:          0,
			Notes:           "Biaya Transfer ke " + bank.BankName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalBiayaCreditID := guid.New().StringUpper()
		keyword = "COACreditBiayaTransferBank"
		journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailBiayaCredit := models.JournalDetail{
			ID:        journalBiayaCreditID,
			JournalID: journal.ID,
			COAID:     COABiayaCredit.ID,
			Debit:     0,
			Credit:    FeeConvert,
			Notes:     "Biaya Transfer ke " + bank.BankName,
			//ReferenceNumber: nil,
		}

		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *FundTransferUsecase) InsertJournalByDate(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)
	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}

	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		CreditAccount, err := accRepo.GetByID(ctx, &b.CreditAccountID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           "Transfer ke Teman atas nama " + CreditAccount.MemberName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           "Transfer dari Teman atas nama " + DebitAccount.MemberName,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0004") //Bank Transfer
		if err != nil {
			return err
		}

		//bankrepository := bankrepository.NewSqlBankRepository()
		bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		bank, err := bankRepo.GetByCode(ctx, &b.BankCode)
		if err != nil {
			return err
		}
		notes = "Transfer Ke " + bank.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     0,
			Credit:    b.Amount,
			Notes:     notes,
			//ReferenceNumber: nil,
		}
		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           FeeConvert,
			Credit:          0,
			Notes:           "Biaya Transfer ke " + bank.BankName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalBiayaCreditID := guid.New().StringUpper()
		keyword = "COACreditBiayaTransferBank"
		journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailBiayaCredit := models.JournalDetail{
			ID:        journalBiayaCreditID,
			JournalID: journal.ID,
			COAID:     COABiayaCredit.ID,
			Debit:     0,
			Credit:    FeeConvert,
			Notes:     "Biaya Transfer ke " + bank.BankName,
			//ReferenceNumber: nil,
		}

		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *FundTransferUsecase) InsertJournalPermata(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := appConfigRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0021") //Bank Transfer Permata
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		//bankrepository := bankrepository.NewSqlBankRepository()
		// bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		// bank := bankRepo.GetByID(ctx, &b.BankCode)
		notes = "Transfer Ke " + b.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     0,
			Credit:    b.Amount,
			Notes:     notes,
			//ReferenceNumber: nil,
		}

		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)

		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           FeeConvert,
			Credit:          0,
			Notes:           "Biaya Transfer ke " + b.BankName,
			ReferenceNumber: b.DebitAccountID,
		}

		if b.TransferType == 2 {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0023") //tf bank lain
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferBankLain"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     0,
				Credit:    biayaGiro,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferBankLain"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     0,
				Credit:    (FeeConvert - biayaGiro) / 2,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferBankLain"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           0,
				Credit:          (FeeConvert - biayaGiro) / 2,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		} else {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0024") //tf antar permata
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferPermata"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     0,
				Credit:    biayaGiro,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferPermata"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     0,
				Credit:    (FeeConvert - biayaGiro) / 2,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferPermata"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           0,
				Credit:          (FeeConvert - biayaGiro) / 2,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		}

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *FundTransferUsecase) InsertJournalPermataByDate(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := appConfigRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0021") //Bank Transfer Permata
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		isTfUseAgent := false
		isOvbUseAgent := false
		TfAgentFeeConvert := 0.0
		OvbAgentFeeConvert := 0.0
		memberRepo := ctx.Value("MemberRepository").(cMember.MemberRepository)
		member, err := memberRepo.GetByID(ctx, &DebitAccount.MemberID)
		if err != nil {
			return err
		}
		if member.MemberType == 9 {
			isTfUseAgent = true
			isOvbUseAgent = true
			tfAgentKey := "Transfer Agent Amount"
			TfAgentFee, err := appConfigRepo.GetByConfigKey(ctx, &tfAgentKey)
			if err != nil {
				return err
			}
			TfAgentFeeConvert, _ = strconv.ParseFloat(TfAgentFee.ConfigValue, 64)
			if TfAgentFeeConvert == 0.0 {
				isTfUseAgent = false
			}
			ovbAgentKey := "Transfer Agent Amount"
			OvbAgentFee, err := appConfigRepo.GetByConfigKey(ctx, &ovbAgentKey)
			if err != nil {
				return err
			}
			OvbAgentFeeConvert, _ = strconv.ParseFloat(OvbAgentFee.ConfigValue, 64)
			if OvbAgentFeeConvert == 0.0 {
				isOvbUseAgent = false
			}
		}

		//bankrepository := bankrepository.NewSqlBankRepository()
		// bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		// bank := bankRepo.GetByID(ctx, &b.BankCode)
		notes = "Transfer Ke " + b.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     0,
			Credit:    b.Amount,
			Notes:     notes,
			//ReferenceNumber: nil,
		}

		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)

		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           FeeConvert,
			Credit:          0,
			Notes:           "Biaya Transfer ke " + b.BankName,
			ReferenceNumber: b.DebitAccountID,
		}

		if b.TransferType == 2 {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0023") //tf bank lain
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferBankLain"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     0,
				Credit:    biayaGiro,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferBankLain"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     0,
				Credit:    (FeeConvert - biayaGiro - TfAgentFeeConvert) / 2,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferBankLain"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           0,
				Credit:          (FeeConvert - biayaGiro - TfAgentFeeConvert) / 2,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			if isTfUseAgent {
				journalDetailBiayaCredit4 := models.JournalDetail{
					ID:              guid.New().StringUpper(),
					JournalID:       journal.ID,
					COAID:           COABiayaCredit3.ID,
					Debit:           0,
					Credit:          TfAgentFeeConvert,
					Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
					ReferenceNumber: b.DebitAccountID,
				}
				journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit4)
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		} else {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0024") //tf antar permata
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferPermata"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     0,
				Credit:    biayaGiro,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferPermata"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     0,
				Credit:    (FeeConvert - biayaGiro - OvbAgentFeeConvert) / 2,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferPermata"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           0,
				Credit:          (FeeConvert - biayaGiro - OvbAgentFeeConvert) / 2,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			if isOvbUseAgent {
				journalDetailBiayaCredit4 := models.JournalDetail{
					ID:              guid.New().StringUpper(),
					JournalID:       journal.ID,
					COAID:           COABiayaCredit3.ID,
					Debit:           0,
					Credit:          OvbAgentFeeConvert,
					Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
					ReferenceNumber: b.DebitAccountID,
				}
				journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit4)
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		}

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *FundTransferUsecase) InsertJournalReversePermata(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := appConfigRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0021") //Bank Transfer Permata
		if err != nil {
			return err
		}
		//bankrepository := bankrepository.NewSqlBankRepository()
		// bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		// bank := bankRepo.GetByID(ctx, &b.BankCode)
		notes = "Reversal Transfer Ke " + b.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     b.Amount,
			Credit:    0,
			Notes:     notes,
			//ReferenceNumber: nil,
		}

		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)

		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           0,
			Credit:          FeeConvert,
			Notes:           "Biaya Transfer ke " + b.BankName,
			ReferenceNumber: b.DebitAccountID,
		}

		if b.TransferType == 2 {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0023") //tf bank lain
			if err != nil {
				return err
			}
			DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferBankLain"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     biayaGiro,
				Credit:    0,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferBankLain"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     (FeeConvert - biayaGiro) / 2,
				Credit:    0,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferBankLain"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           (FeeConvert - biayaGiro) / 2,
				Credit:          0,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		} else {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0024") //tf antar permata
			if err != nil {
				return err
			}
			DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferPermata"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     biayaGiro,
				Credit:    0,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferPermata"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     (FeeConvert - biayaGiro) / 2,
				Credit:    0,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferPermata"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           (FeeConvert - biayaGiro) / 2,
				Credit:          0,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		}

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *FundTransferUsecase) InsertJournalReversePermataByDate(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}
	keyAccInvelli := "AccountID Invelli"
	accIDInvelli, err := appConfigRepo.GetByConfigKey(ctx, &keyAccInvelli)
	if err != nil {
		return err
	}
	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           notes,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0021") //Bank Transfer Permata
		if err != nil {
			return err
		}
		//bankrepository := bankrepository.NewSqlBankRepository()
		// bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		// bank := bankRepo.GetByID(ctx, &b.BankCode)
		notes = "Reversal Transfer Ke " + b.BankName + " - " + b.AccountNoDestination + " - " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     b.Amount,
			Credit:    0,
			Notes:     notes,
			//ReferenceNumber: nil,
		}

		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)

		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           0,
			Credit:          FeeConvert,
			Notes:           "Biaya Transfer ke " + b.BankName,
			ReferenceNumber: b.DebitAccountID,
		}

		if b.TransferType == 2 {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0023") //tf bank lain
			if err != nil {
				return err
			}
			DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferBankLain"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     biayaGiro,
				Credit:    0,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //"Bank Giro Permata VA",
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferBankLain"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     (FeeConvert - biayaGiro) / 2,
				Credit:    0,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferBankLain"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           (FeeConvert - biayaGiro) / 2,
				Credit:          0,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		} else {
			journalmappingbiaya, err := journalMappingRepo.GetByCode(ctx, "T0024") //tf antar permata
			if err != nil {
				return err
			}
			DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
			if err != nil {
				return err
			}
			journalBiayaCreditID := guid.New().StringUpper()
			keyword = "COACreditSettlementTransferPermata"
			journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetailBase(ctx, journalmappingbiaya.ID, "2", &keyword)
			if err != nil {
				return err
			}
			biayaGiro, _ := strconv.ParseFloat(journalMappingBiayaDetail2.DefaultValue, 64)
			COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit := models.JournalDetail{
				ID:        journalBiayaCreditID,
				JournalID: journal.ID,
				COAID:     COABiayaCredit.ID,
				Debit:     biayaGiro,
				Credit:    0,
				Notes:     "Biaya Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName, //,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID2 := guid.New().StringUpper()
			keywordbiaya2 := "COACreditPendapatanSettlementTransferPermata"
			journalMappingBiayaDetail3, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya2)
			if err != nil {
				return err
			}
			COABiayaCredit2, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail3.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit2 := models.JournalDetail{
				ID:        journalBiayaCreditID2,
				JournalID: journal.ID,
				COAID:     COABiayaCredit2.ID,
				Debit:     (FeeConvert - biayaGiro) / 2,
				Credit:    0,
				Notes:     "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				//ReferenceNumber: nil,
			}
			journalBiayaCreditID3 := guid.New().StringUpper()
			keywordbiaya3 := "COACreditRekSettlementTransferPermata"
			journalMappingBiayaDetail4, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmappingbiaya.ID, "2", &keywordbiaya3)
			if err != nil {
				return err
			}
			COABiayaCredit3, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail4.Code.String)
			if err != nil {
				return err
			}
			journalDetailBiayaCredit3 := models.JournalDetail{
				ID:              journalBiayaCreditID3,
				JournalID:       journal.ID,
				COAID:           COABiayaCredit3.ID,
				Debit:           (FeeConvert - biayaGiro) / 2,
				Credit:          0,
				Notes:           "Pendapatan Trf " + DebitAccount.MemberName + " ke " + b.BankName + " - " + b.CreditAccountName,
				ReferenceNumber: accIDInvelli.ConfigValue, //"9BCF1B63-9FA8-4F58-A6D3-8DF6F97D6005",
			}
			journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit2)
			journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit3)
		}

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *FundTransferUsecase) InsertJournalReverse(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}

	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		CreditAccount, err := accRepo.GetByID(ctx, &b.CreditAccountID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           "Transfer ke Teman atas nama " + CreditAccount.MemberName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           "Transfer dari Teman atas nama " + DebitAccount.MemberName,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0004") //Bank Transfer
		if err != nil {
			return err
		}

		// bankrepository := bankrepository.NewSqlBankRepository()
		bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		bank, _ := bankRepo.GetByCode(ctx, &b.BankCode)
		notes = "Transfer Ke Bank " + bank.BankName + "-" + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     b.Amount,
			Credit:    0,
			Notes:     notes,
			//ReferenceNumber: nil,
		}
		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           0,
			Credit:          FeeConvert,
			Notes:           "Biaya Transfer ke " + bank.BankName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalBiayaCreditID := guid.New().StringUpper()
		keyword = "COACreditBiayaTransferBank"
		journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailBiayaCredit := models.JournalDetail{
			ID:        journalBiayaCreditID,
			JournalID: journal.ID,
			COAID:     COABiayaCredit.ID,
			Debit:     FeeConvert,
			Credit:    0,
			Notes:     "Biaya Transfer ke " + bank.BankName,
			//ReferenceNumber: nil,
		}

		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)

		// JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)

		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}
func (a *FundTransferUsecase) InsertJournalReverseByDate(ctx context.Context, b *models.FundTransfer) error {
	journalID := guid.New().StringUpper()

	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	// coarepository := coarepository.NewSqlCOARepository()
	// appconfigrepository := appconfigrepository.NewSqlApplicationConfigurationRepository()
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	coaRepo := ctx.Value("COARepository").(cCOA.COARepository)
	appConfigRepo := ctx.Value("ApplicationConfigurationRepository").(cAppConfig.ApplicationConfigurationRepository)
	accRepo := ctx.Value("AccountRepository").(cAccount.AccountRepository)

	journalmapping, err := journalMappingRepo.GetByCode(ctx, "T0003") //registered member
	if err != nil {
		return err
	}

	notes := ""
	if b.TransferType == 1 {
		notes = "Transfer ke Teman atas nama " + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferTeman"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		CreditAccount, err := accRepo.GetByID(ctx, &b.CreditAccountID)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           "Transfer ke Teman atas nama " + CreditAccount.MemberName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransaksiTeman"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		DebitAccount, err := accRepo.GetByID(ctx, &b.DebitAccountID)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:              journalDetailCreditID,
			JournalID:       journal.ID,
			COAID:           COA2.ID,
			Debit:           b.Amount,
			Credit:          0,
			Notes:           "Transfer dari Teman atas nama " + DebitAccount.MemberName,
			ReferenceNumber: b.CreditAccountID,
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

		//JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	} else {
		journalmapping, err = journalMappingRepo.GetByCode(ctx, "T0004") //Bank Transfer
		if err != nil {
			return err
		}

		// bankrepository := bankrepository.NewSqlBankRepository()
		bankRepo := ctx.Value("BankRepository").(cBank.BankRepository)
		bank, _ := bankRepo.GetByCode(ctx, &b.BankCode)
		notes = "Transfer Ke Bank " + bank.BankName + "-" + b.CreditAccountName
		journal := models.Journal{
			ID:               journalID,
			TransactionID:    b.ID,
			PostingDate:      b.TransactionDate,
			Description:      service.NewNullString(notes),
			Code:             service.NewNullString(b.Code),
			JournalMappingID: journalmapping.ID,
		}
		journalDetailDebitID := guid.New().StringUpper()
		keyword := "COADebetTransferBank"
		journalMappingDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COA, err := coaRepo.GetByCode(ctx, &journalMappingDetail.Code.String)
		if err != nil {
			return err
		}
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailDebitID,
			JournalID:       journal.ID,
			COAID:           COA.ID,
			Debit:           0,
			Credit:          b.Amount,
			Notes:           notes,
			ReferenceNumber: b.DebitAccountID,
		}
		journalDetailCreditID := guid.New().StringUpper()
		keyword = "COACreditTransferBank"
		journalMappingDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COA2, err := coaRepo.GetByCode(ctx, &journalMappingDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailCredit := models.JournalDetail{
			ID:        journalDetailCreditID,
			JournalID: journal.ID,
			COAID:     COA2.ID,
			Debit:     b.Amount,
			Credit:    0,
			Notes:     notes,
			//ReferenceNumber: nil,
		}
		confifKey := "Biaya Transfer"
		appConfig, err := appConfigRepo.GetByConfigKey(ctx, &confifKey)
		if err != nil {
			return err
		}
		FeeConvert, _ := strconv.ParseFloat(appConfig.ConfigValue, 64)
		journalBiayaDebitID := guid.New().StringUpper()
		keyword = "COADebetBiayaTransferBank"
		journalMappingBiayaDetail, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "1", &keyword)
		if err != nil {
			return err
		}
		COABiayaDebet, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail.Code.String)
		if err != nil {
			return err
		}

		journalDetailBiayaDebit := models.JournalDetail{
			ID:              journalBiayaDebitID,
			JournalID:       journal.ID,
			COAID:           COABiayaDebet.ID,
			Debit:           0,
			Credit:          FeeConvert,
			Notes:           "Biaya Transfer ke " + bank.BankName,
			ReferenceNumber: b.DebitAccountID,
		}
		journalBiayaCreditID := guid.New().StringUpper()
		keyword = "COACreditBiayaTransferBank"
		journalMappingBiayaDetail2, err := journalMappingRepo.GetJournalMappingDetail(ctx, journalmapping.ID, "2", &keyword)
		if err != nil {
			return err
		}
		COABiayaCredit, err := coaRepo.GetByCode(ctx, &journalMappingBiayaDetail2.Code.String)
		if err != nil {
			return err
		}
		journalDetailBiayaCredit := models.JournalDetail{
			ID:        journalBiayaCreditID,
			JournalID: journal.ID,
			COAID:     COABiayaCredit.ID,
			Debit:     FeeConvert,
			Credit:    0,
			Notes:     "Biaya Transfer ke " + bank.BankName,
			//ReferenceNumber: nil,
		}

		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailBiayaCredit)

		// JournalUsecase := usecase.NewJournalUsecase()
		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)

		_, err = JournalUsecase.StoreBase(ctx, &journal)
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *FundTransferUsecase) GetByReffNo(c context.Context, reffNo string) (*models.FundTransfer, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	fundtransfer, err := a.FundTransferRepository.GetByReffNo(ctx, reffNo)
	if err != nil {
		return nil, err
	}
	return fundtransfer, err
}
func (a *FundTransferUsecase) UpdateRecordStatus(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	status, err := a.FundTransferRepository.UpdateRecordStatus(ctx, code, reffNo, recordStatus)
	if err != nil {
		return false, err
	}
	fundtransfer, err := a.FundTransferRepository.GetByCode(ctx, code)
	if err != nil {
		return false, err
	}
	if recordStatus == 0 {
		//Insert Journal reverse
		err = a.InsertJournalReverse(ctx, fundtransfer)
		if err != nil {
			return false, err
		}
	}
	return status, nil
}
func (a *FundTransferUsecase) UpdateRecordStatusByDate(ctx context.Context, code string, reffNo string, recordStatus int, companyDate string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	status, err := a.FundTransferRepository.UpdateRecordStatus(ctx, code, reffNo, recordStatus)
	if err != nil {
		return false, err
	}
	fundtransfer, err := a.FundTransferRepository.GetByCode(ctx, code)
	if err != nil {
		return false, err
	}
	if recordStatus == 0 {
		//Insert Journal reverse
		err = a.InsertJournalReverseByDate(ctx, fundtransfer)
		if err != nil {
			return false, err
		}
	}
	return status, nil
}
func (a *FundTransferUsecase) UpdateTransferStatus(ctx context.Context, code string, reffNo string, recordStatus int, bankTransactionID string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	status, err := a.FundTransferRepository.UpdateTransferStatus(ctx, code, reffNo, recordStatus, bankTransactionID)
	if err != nil {
		return false, err
	}
	fundtransfer, err := a.FundTransferRepository.GetByCode(ctx, code)
	if err != nil {
		return false, err
	}
	if recordStatus == 0 {
		//Insert Journal reverse
		err = a.InsertJournalReverse(ctx, fundtransfer)
		if err != nil {
			return false, err
		}
	}
	return status, nil
}
func (a *FundTransferUsecase) UpdateRecordStatusPermata(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	status, err := a.FundTransferRepository.UpdateRecordStatus(ctx, code, reffNo, recordStatus)
	if err != nil {
		return false, err
	}
	fundtransfer, err := a.FundTransferRepository.GetByCode(ctx, code)
	if err != nil {
		return false, err
	}
	if recordStatus == 0 {
		//Insert Journal reverse
		err = a.InsertJournalReversePermata(ctx, fundtransfer)
		if err != nil {
			return false, err
		}
	}
	return status, nil
}
func (a *FundTransferUsecase) UpdateRecordStatusPermataByDate(ctx context.Context, code string, reffNo string, recordStatus int, companyDate string) (bool, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	status, err := a.FundTransferRepository.UpdateRecordStatus(ctx, code, reffNo, recordStatus)
	if err != nil {
		return false, err
	}
	fundtransfer, err := a.FundTransferRepository.GetByCode(ctx, code)
	if err != nil {
		return false, err
	}
	if recordStatus == 0 {
		//Insert Journal reverse
		err = a.InsertJournalReversePermataByDate(ctx, fundtransfer)
		if err != nil {
			return false, err
		}
	}
	return status, nil
}
func (a *FundTransferUsecase) MostTransferList(c context.Context, memberId string, transferType string) (error, []*models.FundTransfer) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listFundtransfer := a.FundTransferRepository.MostTransferList(ctx, memberId, transferType)
	if err != nil {
		return err, nil
	}
	return nil, listFundtransfer
}
