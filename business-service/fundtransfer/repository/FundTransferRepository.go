package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/fundtransfer"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlFundTransferRepository struct {
	Conn *sql.DB
}

func NewSqlFundTransferRepository() fundtransfer.FundTransferRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlFundTransferRepository{conn}
}

func NewSqlFundTransferRepositoryV2(Conn *sql.DB) fundtransfer.FundTransferRepository {
	conn := Conn
	return &sqlFundTransferRepository{conn}
}

func (sj *sqlFundTransferRepository) Fetch(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer) {
	feeAmountQuery := `,0 AS FeeAmount`
	if transferType != "" {
		feeAmountQuery = `,CASE WHEN a.TransferType=1 then 0 ELSE 
        ISNULL((SELECT TOP(1)jd.Debet FROM JournalDetail jd INNER JOIN Journal j on jd.JournalID = j.ID WHERE j.TransactionID = a.ID AND jd.Notes = 'Biaya Transfer' AND jd.Debet > 0 ORDER by jd.Debet),0)
		END AS FeeAmount`
	}
	query := `
	SELECT Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'00000000-0000-0000-0000-000000000000') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,case when a.CreditAccountID=''  then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(Cast(c.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as MemberID
		,isnull((select Handphone from Member hm inner join account ha on hm.id=ha.MemberID and ha.id=
		(select case when a.CreditAccountID=''  then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID)),'') as HandphoneTujuan
		,isnull(Cast(d.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as BankID
		,isnull(d.BankName,'') as BankName
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '') as CreditAccountNo
		,isnull(a.BankTransactionID,'') as BankTransactionID
		` + feeAmountQuery + `
	FROM [dbo].[FundTransfer] a 
	left join Account b on a.DebitAccountID=b.ID
	left join Member c on b.MemberID=c.ID
	left join Bank d on a.bankcode=d.code
	`
	if recent == true {
		queryTransferType := ""
		if transferType != "" {
			trfType := strings.Split(transferType, ",")
			fieldCondition := ``
			if transferType == "1" {
				if search != "ALL" && search != "" {
					if search == "MEMBERID" {
						queryTransferType = "where TransferType =@transferType and m.ID like @p0"
					}
				} else {
					queryTransferType = "where TransferType =@transferType"
				}
			} else {
				for index, element := range trfType {
					if fieldCondition == `` {
						fieldCondition = fieldCondition + element
					} else if index == len(trfType)-1 {
						fieldCondition = fieldCondition + "," + element
					} else {
						fieldCondition = fieldCondition + "," + element
					}
				}
				if search != "ALL" && search != "" {
					if search == "MEMBERID" {
						queryTransferType = `where TransferType IN (` + fieldCondition + `)  and m.ID like @p0`
					}
				} else {
					queryTransferType = `where TransferType IN (` + fieldCondition + `)`
				}

			}
		}
		query += `
		inner join (select f.CreditAccountNo,max(f.dateinsert) as dateinsert from FundTransfer f
		left join Account a on f.DebitAccountID=a.ID
		left join Member m on a.MemberID=m.ID
		` + queryTransferType + `
		group by CreditAccountNo) e on a.CreditAccountNo=e.CreditAccountNo and a.dateinsert=e.dateinsert
		`
	}

	if search != "ALL" && search != "" {
		if search == "MEMBERID" {
			search = "c.ID"
		} else {
			search = "a." + search
		}
		query += ` where ` + search + ` like @p0`

	} else {
		query += ` where 1=1`
	}
	if transferType != "" {
		trfType := strings.Split(transferType, ",")
		fieldCondition := ``
		if transferType == "1" {
			query += ` and TransferType =` + transferType
		} else {
			for index, element := range trfType {
				if fieldCondition == `` {
					fieldCondition = fieldCondition + element
				} else if index == len(trfType)-1 {
					fieldCondition = fieldCondition + "," + element
				} else {
					fieldCondition = fieldCondition + "," + element
				}
			}
			query += ` and TransferType IN (` + fieldCondition + `)`
		}
	}
	if startDate != nil && endDate != nil {
		layoutSQL := "2006-01-02 15:04:05"
		layoutDate := "2006-01-02"
		dateEndTime, _ := time.Parse(layoutDate, *endDate)
		*endDate = time.Date(dateEndTime.Year(), dateEndTime.Month(), dateEndTime.Day(), 23, 59, 59, 0, dateEndTime.Location()).Format(layoutSQL)
		query += ` and DebitDate BETWEEN @startDate AND @endDate`
	}

	query += ` order by a.CreditAccountName`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
		zap.String("startDate", fmt.Sprintf("%v", startDate)),
		zap.String("endDate", fmt.Sprintf("%v", endDate)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
		sql.Named("startDate", startDate),
		sql.Named("endDate", endDate),
		sql.Named("transferType", transferType),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.FundTransfer, 0)
	for rows.Next() {
		j := new(models.FundTransfer)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.TransferDirection,
			&j.DebitCompanyID,
			&j.CreditCompanyID,
			&j.DebitAccountID,
			&j.CurrencyID,
			&j.Amount,
			&j.DebitDate,
			&j.BankCode,
			&j.BankAccountName,
			&j.TransferType,
			&j.CreditAccountID,
			&j.CreditAccountName,
			&j.CreditDate,
			&j.Note,
			&j.RevNo,
			&j.RecordStatus,
			&j.IsTab,
			&j.Remarks,
			&j.IsTabCredit,
			&j.MemberID,
			&j.HandphoneTujuan,
			&j.BankID,
			&j.BankName,
			&j.ReffNo,
			&j.AccountNoDestination,
			&j.BankTransactionID,
			&j.FeeAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlFundTransferRepository) FetchBase(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer) {
	query := `
	SELECT Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'00000000-0000-0000-0000-000000000000') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,isnull(Cast(d.Code as varchar(36)),'') as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,case when a.CreditAccountID=''  then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(Cast(c.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as MemberID
		,isnull((select Handphone from Member hm inner join account ha on hm.id=ha.MemberID and ha.id=
		(select case when a.CreditAccountID=''  then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID)),'') as HandphoneTujuan
		,isnull(Cast(d.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as BankID
		,isnull(d.BankName,'') as BankName
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '') as CreditAccountNo
		,ISNULL(a.NomorTransaksiSettlement, '') as NomorTransaksiSettlement
		,ISNULL(CONVERT(VARCHAR(10), a.DateUpdate, 103) + ' '  + convert(VARCHAR(8), a.DateUpdate, 14),'') as DateUpdate
	FROM [dbo].[FundTransfer] a 
	left join Account b on a.DebitAccountID=b.ID
	left join Member c on b.MemberID=c.ID
	left join Bank d on a.bankcode=d.code
	`
	if recent == true {
		queryTransferType := ""
		if transferType != "" {
			trfType := strings.Split(transferType, ",")
			fieldCondition := ``
			if transferType == "1" {
				if search != "ALL" && search != "" {
					if search == "MEMBERID" {
						queryTransferType = "where TransferType =@transferType and m.ID like @p0"
					}
				} else {
					queryTransferType = "where TransferType =@transferType"
				}
			} else {
				for index, element := range trfType {
					if fieldCondition == `` {
						fieldCondition = fieldCondition + element
					} else if index == len(trfType)-1 {
						fieldCondition = fieldCondition + "," + element
					} else {
						fieldCondition = fieldCondition + "," + element
					}
				}
				if search != "ALL" && search != "" {
					if search == "MEMBERID" {
						queryTransferType = `where TransferType IN (` + fieldCondition + `)  and m.ID like @p0`
					}
				} else {
					queryTransferType = `where TransferType IN (` + fieldCondition + `)`
				}

			}
		}
		query += `
		inner join (select f.CreditAccountNo,max(f.dateinsert) as dateinsert from FundTransfer f
		left join Account a on f.DebitAccountID=a.ID
		left join Member m on a.MemberID=m.ID
		` + queryTransferType + `
		group by CreditAccountNo) e on a.CreditAccountNo=e.CreditAccountNo and a.dateinsert=e.dateinsert
		`
	}

	if search != "ALL" && search != "" {
		if search == "MEMBERID" {
			search = "c.ID"
		} else {
			search = "a." + search
		}
		query += ` where ` + search + ` like @p0`

	} else {
		query += ` where 1=1`
	}
	if transferType != "" {
		trfType := strings.Split(transferType, ",")
		fieldCondition := ``
		if transferType == "1" {
			query += ` and TransferType =` + transferType
		} else {
			for index, element := range trfType {
				if fieldCondition == `` {
					fieldCondition = fieldCondition + element
				} else if index == len(trfType)-1 {
					fieldCondition = fieldCondition + "," + element
				} else {
					fieldCondition = fieldCondition + "," + element
				}
			}
			query += ` and TransferType IN (` + fieldCondition + `)`
		}
	}
	if startDate != nil && endDate != nil {
		layoutSQL := "2006-01-02 15:04:05"
		layoutDate := "2006-01-02"
		dateEndTime, _ := time.Parse(layoutDate, *endDate)
		*endDate = time.Date(dateEndTime.Year(), dateEndTime.Month(), dateEndTime.Day(), 23, 59, 59, 0, dateEndTime.Location()).Format(layoutSQL)
		query += ` and DebitDate BETWEEN @startDate AND @endDate`
	}

	query += ` order by a.CreditAccountName`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("keyword", fmt.Sprintf("%v", keyword)),
		zap.String("startDate", fmt.Sprintf("%v", startDate)),
		zap.String("endDate", fmt.Sprintf("%v", endDate)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", "%"+keyword+"%"),
		sql.Named("startDate", startDate),
		sql.Named("endDate", endDate),
		sql.Named("transferType", transferType),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.FundTransfer, 0)
	for rows.Next() {
		j := new(models.FundTransfer)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.TransferDirection,
			&j.DebitCompanyID,
			&j.CreditCompanyID,
			&j.DebitAccountID,
			&j.CurrencyID,
			&j.Amount,
			&j.DebitDate,
			&j.BankCode,
			&j.BankAccountName,
			&j.TransferType,
			&j.CreditAccountID,
			&j.CreditAccountName,
			&j.CreditDate,
			&j.Note,
			&j.RevNo,
			&j.RecordStatus,
			&j.IsTab,
			&j.Remarks,
			&j.IsTabCredit,
			&j.MemberID,
			&j.HandphoneTujuan,
			&j.BankID,
			&j.BankName,
			&j.ReffNo,
			&j.AccountNoDestination,
			&j.NomorTransaksiSettlement,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}
func (sj *sqlFundTransferRepository) HistoryTransfer(ctx context.Context, debitAccountId string) (error, []*models.FundTransfer) {
	query := `
	SELECT 
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.DebitAccountID=@p0 
		and TransferType = 1 
		AND Remarks is null 
	order by 
		DateInsert desc
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("debitAccountId", fmt.Sprintf("%v", debitAccountId)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", debitAccountId),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.FundTransfer, 0)
	for rows.Next() {
		j := new(models.FundTransfer)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.TransferDirection,
			&j.DebitCompanyID,
			&j.CreditCompanyID,
			&j.DebitAccountID,
			&j.CurrencyID,
			&j.Amount,
			&j.DebitDate,
			&j.BankCode,
			&j.BankAccountName,
			&j.TransferType,
			&j.CreditAccountID,
			&j.CreditAccountName,
			&j.CreditDate,
			&j.Note,
			&j.RevNo,
			&j.RecordStatus,
			&j.IsTab,
			&j.Remarks,
			&j.IsTabCredit,
			&j.ReffNo,
			&j.AccountNoDestination,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlFundTransferRepository) GetById(ctx context.Context, id string) (error, *models.FundTransfer) {
	var j models.FundTransfer
	query := `
	SELECT 
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.ID=@p0
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id),
	).Scan(
		&j.ID,
		&j.Code,
		&j.TransferDirection,
		&j.DebitCompanyID,
		&j.CreditCompanyID,
		&j.DebitAccountID,
		&j.CurrencyID,
		&j.Amount,
		&j.DebitDate,
		&j.BankCode,
		&j.BankAccountName,
		&j.TransferType,
		&j.CreditAccountID,
		&j.CreditAccountName,
		&j.CreditDate,
		&j.Note,
		&j.RevNo,
		&j.RecordStatus,
		&j.IsTab,
		&j.Remarks,
		&j.IsTabCredit,
		&j.ReffNo,
		&j.AccountNoDestination,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}

func (sj *sqlFundTransferRepository) GetByReffNo(ctx context.Context, reffNo string) (*models.FundTransfer, error) {
	var j models.FundTransfer
	query := `
	SELECT TOP 1
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.reffNo=@p0
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("reffNo", fmt.Sprintf("%v", reffNo)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", reffNo),
	).Scan(
		&j.ID,
		&j.Code,
		&j.TransferDirection,
		&j.DebitCompanyID,
		&j.CreditCompanyID,
		&j.DebitAccountID,
		&j.CurrencyID,
		&j.Amount,
		&j.DebitDate,
		&j.BankCode,
		&j.BankAccountName,
		&j.TransferType,
		&j.CreditAccountID,
		&j.CreditAccountName,
		&j.CreditDate,
		&j.Note,
		&j.RevNo,
		&j.RecordStatus,
		&j.IsTab,
		&j.Remarks,
		&j.IsTabCredit,
		&j.ReffNo,
		&j.AccountNoDestination,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}

func (sj *sqlFundTransferRepository) GetBySessionId(ctx context.Context, id string) (error, *models.FundTransfer) {
	var j models.FundTransfer
	query := `
	SELECT 
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.SessionIDInquiry=@p0
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id),
	).Scan(
		&j.ID,
		&j.Code,
		&j.TransferDirection,
		&j.DebitCompanyID,
		&j.CreditCompanyID,
		&j.DebitAccountID,
		&j.CurrencyID,
		&j.Amount,
		&j.DebitDate,
		&j.BankCode,
		&j.BankAccountName,
		&j.TransferType,
		&j.CreditAccountID,
		&j.CreditAccountName,
		&j.CreditDate,
		&j.Note,
		&j.RevNo,
		&j.RecordStatus,
		&j.IsTab,
		&j.Remarks,
		&j.IsTabCredit,
		&j.ReffNo,
		&j.AccountNoDestination,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}

func (sj *sqlFundTransferRepository) GetByCode(ctx context.Context, code string) (*models.FundTransfer, error) {
	var j models.FundTransfer
	query := `
	SELECT TOP 1
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,ISNULL(a.BankCode, '') as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.Code=@p0
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("code", fmt.Sprintf("%v", code)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", code),
	).Scan(
		&j.ID,
		&j.Code,
		&j.TransferDirection,
		&j.DebitCompanyID,
		&j.CreditCompanyID,
		&j.DebitAccountID,
		&j.CurrencyID,
		&j.Amount,
		&j.DebitDate,
		&j.BankCode,
		&j.BankAccountName,
		&j.TransferType,
		&j.CreditAccountID,
		&j.CreditAccountName,
		&j.CreditDate,
		&j.Note,
		&j.RevNo,
		&j.RecordStatus,
		&j.IsTab,
		&j.Remarks,
		&j.IsTabCredit,
		&j.ReffNo,
		&j.AccountNoDestination,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	t := time.Now()
	j.TransactionDate = t.Format("2006-01-02 15:04:05")
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
func (sj *sqlFundTransferRepository) Insert(ctx context.Context, a *models.FundTransfer) (*models.FundTransfer, error) {
	insertQuery := `
		Insert Into FundTransfer
		Values(
			@p0id,
			@p0,
			'',
			@p1,
			@p2,
			@p3,
			@p4,
			@p5,
			getdate(),
			@p6,
			@p7,
			@p8,
			@p9,
			@p10,
			getdate(),
			@p11,
			0,
			@p12,
			'',
			@p13,
			'',
			null,
			getdate(),
			null,null,null,null,
			@p14,
			null,
			@p16
		)
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", insertQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, insertQuery,
		sql.Named("p0id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.DebitCompanyID),
		sql.Named("p2", a.CreditCompanyID),
		sql.Named("p3", a.DebitAccountID),
		sql.Named("p4", a.CurrencyID),
		sql.Named("p5", a.Amount),
		sql.Named("p6", a.BankCode),
		sql.Named("p7", a.BankAccountName),
		sql.Named("p8", a.TransferType),
		sql.Named("p9", a.CreditAccountID),
		sql.Named("p10", a.CreditAccountName),
		sql.Named("p11", a.Note),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.Remarks),
		sql.Named("p14", a.ReffNo),
		sql.Named("p16", a.AccountNoDestination),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	t := time.Now()

	a.IsSuccess = true
	a.Description = "Transaction Success"
	a.TransactionDate = t.Format("2006-01-02 15:04:05")
	a.Token = a.Remarks

	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlFundTransferRepository) InsertBase(ctx context.Context, a *models.FundTransfer) (*models.FundTransfer, error) {
	insertQuery := `
		Insert Into FundTransfer
		(ID,
		Code, 
		TransferDirection, 
		DebitCompanyID, 
		CreditCompanyID, 
		DebitAccountID, 
		CurrencyID, 
		Amount, 
		DebitDate, 
		BankCode, 
		BankAccountName, 
		TransferType, 
		CreditAccountID, 
		CreditAccountName, 
		CreditDate, 
		Note, 
		RevNo, 
		RecordStatus, 
		IsTab, 
		Remarks, 
		IsTabCredit, 
		UserInsert, 
		DateInsert, 
		UserAuthor, 
		DateAuthor, 
		UserUpdate, 
		DateUpdate, 
		ReffNo, 
		BankTransactionID, 
		CreditAccountNo, 
		SessionIDInquiry)
		Values(
			@p0id,
			@p0,
			'',
			@p1,
			@p2,
			@p3,
			@p4,
			@p5,
			getdate(),
			@p6,
			@p7,
			@p8,
			@p9,
			@p10,
			getdate(),
			@p11,
			0,
			@p12,
			'',
			@p13,
			'',
			null,
			getdate(),
			null,
			null,
			null,
			null,
			@p14,
			@p15,
			@p16,
			@p17
		)
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", insertQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, insertQuery,
		sql.Named("p0id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.DebitCompanyID),
		sql.Named("p2", a.CreditCompanyID),
		sql.Named("p3", a.DebitAccountID),
		sql.Named("p4", a.CurrencyID),
		sql.Named("p5", a.Amount),
		sql.Named("p6", a.BankCode),
		sql.Named("p7", a.BankAccountName),
		sql.Named("p8", a.TransferType),
		sql.Named("p9", a.CreditAccountID),
		sql.Named("p10", a.CreditAccountName),
		sql.Named("p11", a.Note),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.Remarks),
		sql.Named("p14", a.ReffNo),
		sql.Named("p15", a.BankTransactionID),
		sql.Named("p16", a.AccountNoDestination),
		sql.Named("p17", a.SessionIDInquiry),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	t := time.Now()

	a.IsSuccess = true
	a.Description = "Transaction Success"
	a.TransactionDate = t.Format("2006-01-02 15:04:05")
	a.Token = a.Remarks

	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlFundTransferRepository) InsertBaseByDate(ctx context.Context, a *models.FundTransfer, companyDate string) (*models.FundTransfer, error) {
	today := time.Now().Format("2006-01-02 15:04:05")
	insertQuery := `
		Insert Into FundTransfer
		(ID,
		Code, 
		TransferDirection, 
		DebitCompanyID, 
		CreditCompanyID, 
		DebitAccountID, 
		CurrencyID, 
		Amount, 
		DebitDate, 
		BankCode, 
		BankAccountName, 
		TransferType, 
		CreditAccountID, 
		CreditAccountName, 
		CreditDate, 
		Note, 
		RevNo, 
		RecordStatus, 
		IsTab, 
		Remarks, 
		IsTabCredit, 
		UserInsert, 
		DateInsert, 
		UserAuthor, 
		DateAuthor, 
		UserUpdate, 
		DateUpdate, 
		ReffNo, 
		BankTransactionID, 
		CreditAccountNo, 
		SessionIDInquiry, 
		Biaya)
		Values(
			@p0id,
			@p0,
			'',
			@p1,
			@p2,
			@p3,
			@p4,
			@p5,
			@date,
			@p6,
			@p7,
			@p8,
			@p9,
			@p10,
			@date,
			@p11,
			0,
			@p12,
			'',
			@p13,
			'',
			null,
			@date,
			null,
			@today,
			null,
			null,
			@p14,
			@p15,
			@p16,
			@p17,
			@biaya
		)
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", insertQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, insertQuery,
		sql.Named("p0id", a.ID),
		sql.Named("p0", a.Code),
		sql.Named("p1", a.DebitCompanyID),
		sql.Named("p2", a.CreditCompanyID),
		sql.Named("p3", a.DebitAccountID),
		sql.Named("p4", a.CurrencyID),
		sql.Named("p5", a.Amount),
		sql.Named("p6", a.BankCode),
		sql.Named("p7", a.BankAccountName),
		sql.Named("p8", a.TransferType),
		sql.Named("p9", a.CreditAccountID),
		sql.Named("p10", a.CreditAccountName),
		sql.Named("p11", a.Note),
		sql.Named("p12", a.RecordStatus),
		sql.Named("p13", a.Remarks),
		sql.Named("p14", a.ReffNo),
		sql.Named("p15", a.BankTransactionID),
		sql.Named("p16", a.AccountNoDestination),
		sql.Named("p17", a.SessionIDInquiry),
		sql.Named("date", companyDate),
		sql.Named("biaya", a.FeeAmount),
		sql.Named("today", today),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}

	a.IsSuccess = true
	a.Description = "Transaction Success"
	a.TransactionDate = companyDate
	a.Token = a.Remarks

	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlFundTransferRepository) UpdateRecordStatus(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error) {
	query := ""
	if reffNo != "" {
		query += ` ,ReffNo = @reffno `
	}
	updateQuery := `
			Update FundTransfer 
			Set 
				RecordStatus = @recordStatus` + query + `
			Where 
				Code = @code`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("code", fmt.Sprintf("%v", code)),
		zap.String("reffno", fmt.Sprintf("%v", reffNo)),
		zap.String("recordStatus", fmt.Sprintf("%v", recordStatus)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("recordStatus", recordStatus),
		sql.Named("reffno", reffNo),
		sql.Named("code", code),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", "update success"),
	)
	return true, nil

}
func (sj *sqlFundTransferRepository) UpdateTransferStatus(ctx context.Context, code string, reffNo string, recordStatus int, bankTransactionID string) (bool, error) {
	query := ""
	if reffNo != "" {
		query += ` ,ReffNo = @reffno `
	}
	updateQuery := `
			Update FundTransfer 
			Set 
				BankTransactionID =@bankTransactionID,
				RecordStatus = @recordStatus` + query + `
			Where 
				Code = @code`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("code", fmt.Sprintf("%v", code)),
		zap.String("reffno", fmt.Sprintf("%v", reffNo)),
		zap.String("recordStatus", fmt.Sprintf("%v", recordStatus)),
		zap.String("bankTransactionID", fmt.Sprintf("%v", bankTransactionID)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("recordStatus", recordStatus),
		sql.Named("reffno", reffNo),
		sql.Named("code", code),
		sql.Named("bankTransactionID", bankTransactionID),
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Result",
		zap.String("res", "update success"),
	)
	return true, nil

}
func (sj *sqlFundTransferRepository) MostTransferList(ctx context.Context, memberId string, transferType string) (error, []*models.FundTransfer) {
	condition := ""
	trfType := strings.Split(transferType, ",")
	fieldCondition := ``
	if transferType == "1" {
		condition = ` TransferType = @transferType`
	} else {
		for index, element := range trfType {
			if fieldCondition == `` {
				fieldCondition = fieldCondition + element
			} else if index == len(trfType)-1 {
				fieldCondition = fieldCondition + "," + element
			} else {
				fieldCondition = fieldCondition + "," + element
			}
		}
		condition = ` TransferType IN (` + fieldCondition + `)`
	}
	query := `
	SELECT top(3) Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'00000000-0000-0000-0000-000000000000') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'00000000-0000-0000-0000-000000000000') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(a.BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,case when CreditAccountID='' then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(Cast(c.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as MemberID
		,isnull((select Handphone from Member hm inner join account ha on hm.id=ha.MemberID and ha.id=(select case when a.CreditAccountID=''  then  '00000000-0000-0000-0000-000000000000'
		else isnull(Cast(a.CreditAccountID as varchar(36)),'00000000-0000-0000-0000-000000000000') end as CreditAccountID)),'') as HandphoneTujuan
		,isnull(Cast(d.ID as varchar(36)),'00000000-0000-0000-0000-000000000000') as BankID
		,isnull(d.BankName,'') as BankName
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '')
	FROM [dbo].[FundTransfer] a 
	left join Account b on a.DebitAccountID=b.ID
	left join Member c on b.MemberID=c.ID
	left join Bank d on a.bankcode=d.code
	inner join (select f.CreditAccountNo,COUNT(f.CreditAccountNo) as Jumlah,max(f.dateinsert) as dateinsert from FundTransfer f
	left join Account a on f.DebitAccountID=a.ID
	left join Member m on a.MemberID=m.ID
	where ` + condition + `
	and m.ID=@p0 group by CreditAccountNo) e on a.CreditAccountNo=e.CreditAccountNo and a.dateinsert=e.dateinsert
	where c.ID=@p0 and ` + condition + `
	order by e.Jumlah desc `
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("memberId", fmt.Sprintf("%v", memberId)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", memberId),
		sql.Named("transferType", transferType),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()

	result := make([]*models.FundTransfer, 0)
	for rows.Next() {
		j := new(models.FundTransfer)
		err = rows.Scan(
			&j.ID,
			&j.Code,
			&j.TransferDirection,
			&j.DebitCompanyID,
			&j.CreditCompanyID,
			&j.DebitAccountID,
			&j.CurrencyID,
			&j.Amount,
			&j.DebitDate,
			&j.BankCode,
			&j.BankAccountName,
			&j.TransferType,
			&j.CreditAccountID,
			&j.CreditAccountName,
			&j.CreditDate,
			&j.Note,
			&j.RevNo,
			&j.RecordStatus,
			&j.IsTab,
			&j.Remarks,
			&j.IsTabCredit,
			&j.MemberID,
			&j.HandphoneTujuan,
			&j.BankID,
			&j.BankName,
			&j.ReffNo,
			&j.AccountNoDestination,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

// func (sj *sqlFundTransferRepository) BaseTransferOnline(ctx context.Context, sourceAccountNo string, destinationAccountNo string, amount float64, desc string, bankCode string, bankTransactionID string, destinationAccountName string, reffNo string) (models.FundTransferResponse, error) {
// 	logger := service.Logger(ctx)
// 	logger.Info("Generating RefNo")
// 	TraceNo, err := sj.GenerateRefNo(ctx)
// 	if err != nil {
// 		logger.Error("Generating Error",
// 			zap.String("error", err.Error()),
// 		)
// 		return models.FundTransferResponse{}, err
// 	}
// 	logger.Info("Generating Success")
// 	payload := map[string]interface{}{
// 		"reff_no":                  reffNo,
// 		"source_account_no":        strings.TrimSpace(sourceAccountNo),
// 		"beneficiary_account_no":   strings.TrimSpace(destinationAccountNo),
// 		"beneficiary_bank_code":    bankCode,
// 		"amount":                   fmt.Sprintf("%.2f", amount),
// 		"description":              desc,
// 		"beneficiary_account_name": destinationAccountName,
// 		"bank_transaction_id":      bankTransactionID,
// 	}

// 	logger.Info("Call cardlez Transfer",
// 		zap.String("url", "/(externalURL-API)/v1/transfers/online"),
// 	)
// 	orderBody := []string{
// 		"reff_no",
// 		"source_account_no",
// 		"beneficiary_account_no",
// 		"beneficiary_bank_code",
// 		"amount",
// 		"description",
// 		"beneficiary_account_name",
// 		"bank_transaction_id"}
// 	resp, err := helper.RequestPost("/(externalURL-API)/v1/transfers/online", payload, orderBody, TraceNo)

// 	if err != nil {
// 		logger.Error("Call cardlez API Error",
// 			zap.String("error", err.Error()),
// 		)
// 		return models.FundTransferResponse{}, err
// 	}
// 	logger.Info("Read Response")
// 	bodyBytes, err := ioutil.ReadAll(resp.Body)
// 	if err != nil {
// 		logger.Error("Read Response Error",
// 			zap.String("error", err.Error()),
// 		)
// 		return models.FundTransferResponse{}, err
// 	}
// 	response := models.FundTransferResponse{}
// 	logger.Info("Unmarshall Response")
// 	err = json.Unmarshal(bodyBytes, &response)
// 	if err != nil {
// 		logger.Error("Unmarshall Response Error",
// 			zap.String("error", err.Error()),
// 		)
// 		return models.FundTransferResponse{}, err
// 	}
// 	logger.Info("Response result",
// 		zap.String("res", fmt.Sprintf("%v", string(bodyBytes))),
// 	)
// 	return response, err
// }

func (sj *sqlFundTransferRepository) GenerateRefNo(ctx context.Context) (string, error) {

	var result string
	err := sj.Conn.QueryRow(`
		EXEC dbo.usp_GetMerchantRefNo_cardlez
		@prefix = @p0`,
		sql.Named("p0", ""),
	).Scan(
		&result,
	)
	if err != nil {
		return "", err
	}
	return result, nil
}
func (sj *sqlFundTransferRepository) GetFundTransferBySessionID(ctx context.Context, sessionID string) (error, *models.FundTransfer) {
	var j models.FundTransfer
	query := `
	SELECT 
		Cast(a.ID as varchar(36)) as ID
		,a.[Code]
		,isnull([TransferDirection],0) as TransferDirection
		,isnull(Cast(DebitCompanyID as varchar(36)),'') as DebitCompanyID
		,isnull(Cast(CreditCompanyID as varchar(36)),'') as CreditCompanyID
		,isnull(Cast(DebitAccountID as varchar(36)),'') as DebitAccountID
		,isnull(Cast(a.[CurrencyID] as varchar(36)),'') as  CurrencyID
		,[Amount]
		,CONVERT(VARCHAR(10), DebitDate, 103) + ' '  + convert(VARCHAR(8), DebitDate, 14) as DebitDate
		,Cast(BankCode as varchar(36)) as BankCode
		,isnull(a.[BankAccountName],'') as BankAccountName
		,[TransferType]
		,isnull(Cast(CreditAccountID as varchar(36)),'') as CreditAccountID
		,a.[CreditAccountName]
		,CONVERT(VARCHAR(10), [CreditDate], 103) + ' '  + convert(VARCHAR(8), [CreditDate], 14) as CreditDate
		,isnull(Note,'')  as Note
		,isnull(a.[RevNo],0) as RevNo
		,a.[RecordStatus]
		,isnull(a.IsTab,0) as IsTab
		,isnull(a.Remarks,'') as Remarks
		,isnull(a.IsTabCredit,0) as IsTabCredit
		,isnull(a.ReffNo,'') as ReffNo
		,ISNULL(a.CreditAccountNo, '') as AccountNoDestination
	FROM 
		[dbo].[FundTransfer] a 
	where 
		a.SessionIDInquiry=@p0
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("sessionID", fmt.Sprintf("%v", sessionID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", sessionID),
	).Scan(
		&j.ID,
		&j.Code,
		&j.TransferDirection,
		&j.DebitCompanyID,
		&j.CreditCompanyID,
		&j.DebitAccountID,
		&j.CurrencyID,
		&j.Amount,
		&j.DebitDate,
		&j.BankCode,
		&j.BankAccountName,
		&j.TransferType,
		&j.CreditAccountID,
		&j.CreditAccountName,
		&j.CreditDate,
		&j.Note,
		&j.RevNo,
		&j.RecordStatus,
		&j.IsTab,
		&j.Remarks,
		&j.IsTabCredit,
		&j.ReffNo,
		&j.AccountNoDestination,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}
func (sj *sqlFundTransferRepository) SettleTransfer(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string, status int32) ([]*models.FundTransfer, error) {

	settleLogQuery := `
		UPDATE FundTransfer 
		SET NomorTransaksiSettlement = @NomorTransaksiSettlement,
			DateUpdate = GETDATE(),
			RecordStatus = @status
		WHERE
			ID IN([params])
	`
	paramValues := make([]interface{}, 0)
	params := make([]string, 0)
	for i, fundtransfer := range b {
		params = append(params, "@p"+strconv.Itoa(i))
		paramValues = append(paramValues, sql.Named("p"+strconv.Itoa(i), fundtransfer.ID))
	}
	settleLogQuery = strings.Replace(
		settleLogQuery,
		"[params]",
		strings.Join(params, ","),
		1,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", settleLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	paramValues = append(paramValues,
		sql.Named("NomorTransaksiSettlement", nomorTransaksiSettlement),
	)
	paramValues = append(paramValues,
		sql.Named("status", status),
	)
	_, err := sj.Conn.ExecContext(ctx, settleLogQuery,
		paramValues...,
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
func (sj *sqlFundTransferRepository) SettleTransferByDate(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string, status int32, companyDate string) ([]*models.FundTransfer, error) {

	settleLogQuery := `
		UPDATE FundTransfer 
		SET NomorTransaksiSettlement = @NomorTransaksiSettlement,
			DateUpdate = @date,
			RecordStatus = @status
		WHERE
			ID IN([params])
	`
	paramValues := make([]interface{}, 0)
	params := make([]string, 0)
	for i, fundtransfer := range b {
		params = append(params, "@p"+strconv.Itoa(i))
		paramValues = append(paramValues, sql.Named("p"+strconv.Itoa(i), fundtransfer.ID))
	}
	settleLogQuery = strings.Replace(
		settleLogQuery,
		"[params]",
		strings.Join(params, ","),
		1,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", settleLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	paramValues = append(paramValues,
		sql.Named("NomorTransaksiSettlement", nomorTransaksiSettlement),
	)
	paramValues = append(paramValues,
		sql.Named("status", status),
	)
	paramValues = append(paramValues,
		sql.Named("date", companyDate),
	)
	_, err := sj.Conn.ExecContext(ctx, settleLogQuery,
		paramValues...,
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
