package fundtransfer

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type FundTransferRepository interface {
	Fetch(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer)
	FetchBase(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer)
	InsertBase(ctx context.Context, fundtransfer *models.FundTransfer) (*models.FundTransfer, error)
	InsertBaseByDate(ctx context.Context, fundtransfer *models.FundTransfer, companyDate string) (*models.FundTransfer, error)
	Insert(ctx context.Context, fundtransfer *models.FundTransfer) (*models.FundTransfer, error)
	HistoryTransfer(ctx context.Context, debitAccountId string) (error, []*models.FundTransfer)
	GetById(ctx context.Context, id string) (error, *models.FundTransfer)
	GetByReffNo(ctx context.Context, reffNo string) (*models.FundTransfer, error)
	UpdateRecordStatus(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error)
	MostTransferList(ctx context.Context, memberId string, transferType string) (error, []*models.FundTransfer)
	GetByCode(ctx context.Context, code string) (*models.FundTransfer, error)

	GetFundTransferBySessionID(ctx context.Context, sessionID string) (error, *models.FundTransfer)
	SettleTransfer(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string, status int32) ([]*models.FundTransfer, error)
	SettleTransferByDate(ctx context.Context, b []*models.FundTransfer, nomorTransaksiSettlement string, status int32, companyDate string) ([]*models.FundTransfer, error)
	UpdateTransferStatus(ctx context.Context, code string, reffNo string, recordStatus int, bankTransactionID string) (bool, error)
}
