package fundtransfer

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type FundTransferUseCase interface {
	Fetch(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer)
	FetchBase(ctx context.Context, search string, keyword string, recent bool, transferType string, startDate *string, endDate *string) (error, []*models.FundTransfer)
	InsertBase(ctx context.Context, fundtransfer *models.FundTransfer) (*models.FundTransfer, error)
	InsertBaseByDate(ctx context.Context, fundtransfer *models.FundTransfer, companyDate string) (*models.FundTransfer, error)
	InsertBasePermata(ctx context.Context, fundtransfer *models.FundTransfer) (*models.FundTransfer, error)
	InsertBasePermataByDate(ctx context.Context, fundtransfer *models.FundTransfer, companyDate string) (*models.FundTransfer, error)
	Insert(ctx context.Context, fundtransfer *models.FundTransfer) (*models.FundTransfer, error)
	InsertJournal(ctx context.Context, fundtransfer *models.FundTransfer) error
	InsertJournalByDate(ctx context.Context, fundtransfer *models.FundTransfer) error
	InsertJournalPermata(ctx context.Context, fundtransfer *models.FundTransfer) error
	InsertJournalPermataByDate(ctx context.Context, fundtransfer *models.FundTransfer) error
	HistoryTransfer(ctx context.Context, memberid string, recent bool) (error, []*models.FundTransfer)
	GetByReffNo(ctx context.Context, reffNo string) (*models.FundTransfer, error)
	UpdateRecordStatus(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error)
	UpdateRecordStatusByDate(ctx context.Context, code string, reffNo string, recordStatus int, companyDate string) (bool, error)
	UpdateRecordStatusPermata(ctx context.Context, code string, reffNo string, recordStatus int) (bool, error)
	UpdateRecordStatusPermataByDate(ctx context.Context, code string, reffNo string, recordStatus int, companyDate string) (bool, error)
	MostTransferList(ctx context.Context, memberId string, transferType string) (error, []*models.FundTransfer)
	InsertJournalReverse(ctx context.Context, fundtransfer *models.FundTransfer) error
	InsertJournalReversePermata(ctx context.Context, fundtransfer *models.FundTransfer) error
	InsertJournalReversePermataByDate(ctx context.Context, fundtransfer *models.FundTransfer) error
	SettleTransfer(ctx context.Context, b []*models.FundTransfer, reffNo string) ([]*models.FundTransfer, error)
	SettleTransferByDate(ctx context.Context, b []*models.FundTransfer, reffNo string, companyDate string) ([]*models.FundTransfer, error)
	GetById(ctx context.Context, id string) (*models.FundTransfer, error)
	UpdateTransferStatus(ctx context.Context, code string, reffNo string, recordStatus int, bankTransactionID string) (bool, error)
}
