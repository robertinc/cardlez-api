package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/province"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlProvinceRepository struct {
	Conn *sql.DB
}

func NewSqlProvinceRepository() province.ProvinceRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlProvinceRepository{conn}
}

func NewSqlProvinceRepositoryV2(Conn *sql.DB) province.ProvinceRepository {
	conn := Conn
	return &sqlProvinceRepository{conn}
}
func (sj *sqlProvinceRepository) Fetch(ctx context.Context) (error, []*models.Province) {
	query := `
	select 	
			cast(province_id as varchar(36)) as province_id,
			isnull(province_no,'') as province_no,
			isnull(Province_Name,'') as Province_Name
		from [Master.Province]
		order by Province_Name
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query)

	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.Province, 0)
	for rows.Next() {
		j := new(models.Province)
		err = rows.Scan(
			&j.Province_ID,
			&j.Province_No,
			&j.Province_Name,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlProvinceRepository) GetByName(ctx context.Context, provinceName string) (error, *models.Province) {
	var Province models.Province
	query := `
	select 	top(1)
			cast(province_id as varchar(36)) as province_id,
			isnull(province_no,'') as province_no,
			isnull(Province_Name,'') as Province_Name
		from VwProvince
	where 
		Province_Name = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("provinceName", fmt.Sprintf("%v", provinceName)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", provinceName)).Scan(
		&Province.Province_ID,
		&Province.Province_No,
		&Province.Province_Name,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", Province)),
	)
	return nil, &Province
}
