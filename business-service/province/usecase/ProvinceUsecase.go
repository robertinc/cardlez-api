package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/province"
	"gitlab.com/robertinc/cardlez-api/business-service/province/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type ProvinceUsecase struct {
	ProvinceRepository province.ProvinceRepository
	contextTimeout     time.Duration
}

func NewProvinceUsecase() province.ProvinceUsecase {
	return &ProvinceUsecase{
		ProvinceRepository: repository.NewSqlProvinceRepository(),
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func NewProvinceUsecaseV2(repo province.ProvinceRepository) province.ProvinceUsecase {
	return &ProvinceUsecase{
		ProvinceRepository: repo,
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *ProvinceUsecase) Fetch(c context.Context) (error, []*models.Province) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listProvince := a.ProvinceRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, listProvince
}
func (a *ProvinceUsecase) GetByName(c context.Context, provinceName string) (error, *models.Province) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, Province := a.ProvinceRepository.GetByName(ctx, provinceName)
	if err != nil {
		return err, nil
	}
	return nil, Province
}
