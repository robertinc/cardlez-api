package province

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ProvinceUsecase interface {
	Fetch(ctx context.Context) (error, []*models.Province)
	GetByName(ctx context.Context, provinceName string) (error, *models.Province)
}
