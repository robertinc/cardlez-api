package ppob

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PPOBRepository interface {
	Fetch(c context.Context, status *int32) ([]*models.PPOB, error)
	Store(ctx context.Context, a *models.PPOB, tableName string) (*models.PPOB, error)
	UpdateStatus(ctx context.Context, a *models.PPOB, tableName string, status int32) (*models.PPOB, error)
	CallPPOB(ctx context.Context, a *models.PPOB, route string) (*models.PPOB, error)
	CheckStatusPPOB(ctx context.Context, a *models.PPOB, route string) (*models.PPOB, error)
	StorePLNPrepaidInquiry(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error)
	CallPLNPrepaidInquiry(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error)
	StorePLNPrepaid(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error)
	UpdateStatusPLNPrepaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error)
	CallPLNPrepaid(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error)
	StorePLNPostpaidInquiry(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error)
	CallPLNPostpaidInquiry(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error)
	StorePLNPostpaid(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error)
	UpdateStatusPLNPostpaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error)
	CallPLNPostpaid(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error)
	SettlePPOB(c context.Context, b []*models.PPOB, operator string, nomorTransaksiSettlement string, status int32) ([]*models.PPOB, error)
	SettlePPOBPLN(c context.Context, b []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string, status int32) ([]*models.PLNPrepaid, error)
	GetByID(ctx context.Context, id string, operator string) (*models.PPOB, error)
	GetByIDPLNPrepaid(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error)
	GetByIDPLNPostpaid(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error)
}
