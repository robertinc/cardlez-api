package ppob

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type PPOBUsecase interface {
	Fetch(c context.Context, status *int32) ([]*models.PPOB, error)
	Store(ctx context.Context, b *models.PPOB, operator string) (*models.PPOB, error)
	CheckStatusPPOB(ctx context.Context, a *models.PPOB, route string) (*models.PPOB, error)
	GetFeeAmount(ctx context.Context, amount float64, operator string) (*models.BillPayment, error)
	StorePLNPrepaidInquiry(ctx context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error)
	StorePLNPrepaid(ctx context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error)
	StorePLNPostpaidInquiry(ctx context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error)
	StorePLNPostpaid(ctx context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error)
	SettlePPOB(c context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string) ([]*models.PPOB, error)
	SettleToRejectPPOB(c context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string) ([]*models.PPOB, error)
	RejectToSettlePPOB(c context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string) ([]*models.PPOB, error)
	ReversePPOB(c context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string) ([]*models.PPOB, error)
	GetByID(ctx context.Context, id string, operator string) (*models.PPOB, error)
	GetByIDPLN(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error)
	UpdateStatus(ctx context.Context, a *models.PPOB, tableName string, status int32) (*models.PPOB, error)
	UpdateStatusPLNPostpaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error)
	UpdateStatusPLNPrepaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error)
	InsertJournal(ctx context.Context, b models.PPOB, journalCode string, operator string) error 
}
