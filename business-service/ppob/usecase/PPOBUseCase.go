package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration"
	billpayment "gitlab.com/robertinc/cardlez-api/business-service/bill-payment"
	"gitlab.com/robertinc/cardlez-api/business-service/member"

	"github.com/beevik/guid"
	"gitlab.com/robertinc/cardlez-api/business-service/account"
	accountrepository "gitlab.com/robertinc/cardlez-api/business-service/account/repository"
	applicationconfigurationrepository "gitlab.com/robertinc/cardlez-api/business-service/applicationconfiguration/repository"
	billpaymentrepository "gitlab.com/robertinc/cardlez-api/business-service/bill-payment/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/coa"
	coarepository "gitlab.com/robertinc/cardlez-api/business-service/coa/repository"

	// journalmappingrepository "gitlab.com/robertinc/cardlez-api/business-service/journal/repository"
	"gitlab.com/robertinc/cardlez-api/business-service/journal/usecase"
	memberrepository "gitlab.com/robertinc/cardlez-api/business-service/member/repository"
	ppob "gitlab.com/robertinc/cardlez-api/business-service/ppob"
	"gitlab.com/robertinc/cardlez-api/business-service/ppob/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"

	cJournal "gitlab.com/robertinc/cardlez-api/business-service/journal"
)

type PPOBUsecase struct {
	PPOBRepository                     ppob.PPOBRepository
	COARepository                      coa.COARepository
	MemberRepository                   member.MemberRepository
	AccountRepository                  account.AccountRepository
	BillPaymentRepository              billpayment.BillPaymentRepository
	ApplicationConfigurationRepository applicationconfiguration.ApplicationConfigurationRepository
	contextTimeout                     time.Duration
}

func NewPPOBUsecase() ppob.PPOBUsecase {
	return &PPOBUsecase{
		PPOBRepository:                     repository.NewSqlPPOBRepository(),
		MemberRepository:                   memberrepository.NewSqlMemberRepository(),
		COARepository:                      coarepository.NewSqlCOARepository(),
		AccountRepository:                  accountrepository.NewSqlAccountRepository(),
		BillPaymentRepository:              billpaymentrepository.NewSqlBillPaymentRepository(),
		ApplicationConfigurationRepository: applicationconfigurationrepository.NewSqlApplicationConfigurationRepository(),
		contextTimeout:                     time.Second * time.Duration(models.Timeout()),
	}
}
func NewPPOBUsecaseV2(
	ppobRepo ppob.PPOBRepository,
	memberRepo member.MemberRepository,
	coaRepo coa.COARepository,
	accountRepo account.AccountRepository,
	billPRepo billpayment.BillPaymentRepository,
	appConfigRepo applicationconfiguration.ApplicationConfigurationRepository) ppob.PPOBUsecase {
	return &PPOBUsecase{
		PPOBRepository:                     ppobRepo,
		MemberRepository:                   memberRepo,
		COARepository:                      coaRepo,
		AccountRepository:                  accountRepo,
		BillPaymentRepository:              billPRepo,
		ApplicationConfigurationRepository: appConfigRepo,
		contextTimeout:                     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *PPOBUsecase) Store(c context.Context, b *models.PPOB, operator string) (*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	tableName := ""
	route := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		route = "/telkomselsimpatiprepaid"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		route = "/xlbebasprepaid"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		route = "/indosatimthreeprepaid"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		route = "/threeprepaid"
		break
	}
	// billPayment, err := a.GetFeeAmount(ctx, b.RequestAmount, operator)
	// b.AdminAmount = billPayment.AdminAmount
	// b.InvelliAmount = billPayment.InvelliAmount
	// b.PartnerAmount = billPayment.PartnerAmount
	// b.TransactionDate = time.Now()

	// b, err = a.PPOBRepository.Store(ctx, b, tableName)
	// if err != nil {
	// 	return nil, err
	// }

	b, err := a.PPOBRepository.CallPPOB(ctx, b, route)
	if err != nil || (b.StatusCode != "00" && b.StatusCode != "68") {
		a.PPOBRepository.UpdateStatus(ctx, b, tableName, 0)
		return nil, err
	}

	if b.StatusCode == "68" {
		// if b.StatusCode == "00" { //for testing
		//Pending
		b, err = a.PPOBRepository.UpdateStatus(ctx, b, tableName, 1)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("Pending")
	} else if b.StatusCode == "00" {
		b, err = a.PPOBRepository.UpdateStatus(ctx, b, tableName, 2)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournal(ctx, *b, "T0010", operator)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Request Failed")
	}
	return b, nil
}
func (a *PPOBUsecase) StorePLNPrepaidInquiry(c context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	route := "/plnprepaidinquiry"

	b, err := a.PPOBRepository.CallPLNPrepaidInquiry(ctx, b, route)
	if err != nil {
		return nil, err
	}

	b, err = a.PPOBRepository.StorePLNPrepaidInquiry(ctx, b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (a *PPOBUsecase) StorePLNPrepaid(c context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	route := "/plnprepaid"

	//billPayment, err := a.GetFeeAmount(ctx, b.RequestAmount, "PLNPREPAID")
	// b.AdminAmount = billPayment.AdminAmount
	// b.InvelliAmount = billPayment.InvelliAmount
	// b.PartnerAmount = billPayment.PartnerAmount
	// b.RequestAmount = b.RequestAmount + b.AdminAmount
	// b.TransactionDate = time.Now()

	// b, err = a.PPOBRepository.StorePLNPrepaid(ctx, b)
	// if err != nil {
	// 	return nil, err
	// }
	b, err := a.PPOBRepository.CallPLNPrepaid(ctx, b, route)
	if err != nil {
		a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, b, 0)
		return nil, err
	}
	b, err = a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, b, 1)

	var ppob models.PPOB
	ppob.ID = b.ID
	ppob.TransactionDate = b.TransactionDate
	ppob.ResponseReference = b.NomorTransaksi
	ppob.RequestAccountNo = b.RequestAccountNo
	ppob.RequestAmount = b.RequestAmount - b.AdminAmount
	ppob.AdminAmount = b.AdminAmount
	if err != nil || (b.StatusCode != "00" && b.StatusCode != "68") {
		a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, b, 0)
		return nil, err
	}

	if b.StatusCode == "68" {
		// if b.StatusCode == "00" { //for testing
		//Pending
		b, err = a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, b, 1)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("Pending")
	} else if b.StatusCode == "00" {
		b, err = a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, b, 2)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournal(ctx, ppob, "T0011", "Pulsa Listrik")
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Request Failed")
	}
	return b, nil
}
func (a *PPOBUsecase) StorePLNPostpaidInquiry(c context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	route := "/plnpostpaidinquiry"

	b, err := a.PPOBRepository.CallPLNPostpaidInquiry(ctx, b, route)
	if err != nil {
		return nil, err
	}

	b, err = a.PPOBRepository.StorePLNPostpaidInquiry(ctx, b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (a *PPOBUsecase) StorePLNPostpaid(c context.Context, b *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	route := "/plnpostpaid"

	// billPayment, err := a.BillPaymentRepository.GetByID(ctx, "7D524FF8-CF0E-4FAF-A7DB-6B3A1B043F1B")
	// if err != nil {
	// 	return nil, err
	// }
	// b.AdminAmount = billPayment.AdminAmount
	// b.InvelliAmount = billPayment.InvelliAmount
	// b.PartnerAmount = billPayment.PartnerAmount
	// b.RequestAmount = b.ResponseBillAmount
	// b.RequestCustomerNumber = b.ResponsePLNCustomerNo
	// b, err = a.PPOBRepository.StorePLNPostpaid(ctx, b)
	// if err != nil {
	// 	return nil, err
	// }

	b, err := a.PPOBRepository.CallPLNPostpaid(ctx, b, route)
	if err != nil {
		a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, b, 0)
		return nil, err
	}
	b, err = a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, b, 1)
	if err != nil {
		return nil, err
	}
	var ppob models.PPOB
	ppob.ID = b.ID
	ppob.TransactionDate = time.Now()
	ppob.ResponseReference = b.NomorTransaksi
	ppob.RequestAccountNo = b.RequestAccountNo
	ppob.RequestAmount = b.RequestAmount
	ppob.AdminAmount = b.AdminAmount

	if err != nil || (b.StatusCode != "00" && b.StatusCode != "68") {
		a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, b, 0)
		return nil, err
	}

	if b.StatusCode == "68" {
		// if b.StatusCode == "00" { //for testing
		//Pending
		b, err = a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, b, 1)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("Pending")
	} else if b.StatusCode == "00" {
		b, err = a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, b, 2)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournal(ctx, ppob, "T0012", "Bayar Listrik")
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Request Failed")
	}
	return b, nil
}
func (a *PPOBUsecase) GetFeeAmount(ctx context.Context, amount float64, operator string) (*models.BillPayment, error) {
	billPayment, err := a.BillPaymentRepository.GetByOperatorAmount(ctx, operator, amount)
	if err != nil {
		return nil, err
	}
	return billPayment, nil
}
func (a *PPOBUsecase) SettlePPOB(ctx context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string) ([]*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	}
	if len(b) > 0 {
		//Pulsa
		b, err := a.PPOBRepository.SettlePPOB(ctx, b, tableName, nomorTransaksiSettlement, 4)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournalSettlement(ctx, b, "T0017", operator)
	}

	if len(plns) > 0 {
		//PLN
		pln, err := a.PPOBRepository.SettlePPOBPLN(ctx, plns, tableName, nomorTransaksiSettlement, 4)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournalSettlementPLN(ctx, pln, "T0017", operator)
	}
	return b, nil
}
func (a *PPOBUsecase) SettleToRejectPPOB(ctx context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string) ([]*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, nomorTransaksiSettlement := a.MemberRepository.GenerateCode(ctx, "STR")
	if err != nil {
		return nil, err
	}
	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	}
	if len(b) > 0 {
		//Pulsa
		b, err := a.PPOBRepository.SettlePPOB(ctx, b, tableName, nomorTransaksiSettlement, 5)
		if err != nil {
			return nil, err
		}
		// Reverse Settlement Journal
		err = a.InsertJournalReverseSettlement(ctx, b, "T0017", operator)
		//Insert To Journal
		for _, ppob := range b {
			//Insert To Journal
			ppob, err := a.PPOBRepository.GetByID(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			billPayment, err := a.GetFeeAmount(ctx, ppob.RequestAmount, operator)
			if err != nil {
				return nil, err
			}
			ppob.AdminAmount = billPayment.AdminAmount
			ppob.InvelliAmount = billPayment.InvelliAmount
			ppob.PartnerAmount = billPayment.PartnerAmount
			ppob.TransactionDate = time.Now()
			err = a.InsertJournalReverse(ctx, *ppob, "T0017", operator)

		}
	}

	if len(plns) > 0 {
		//PLN
		_, err := a.PPOBRepository.SettlePPOBPLN(ctx, plns, tableName, nomorTransaksiSettlement, 5)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		err = a.InsertJournalReverseSettlementPLN(ctx, plns, "T0017", operator)

		for _, ppob := range plns {
			//Insert To Journal
			ppob, err := a.GetByIDPLN(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			err = a.InsertJournalReversePLN(ctx, *ppob, "T0017", operator)
			if err != nil {
				return nil, err
			}
		}
	}
	return b, nil
}
func (a *PPOBUsecase) RejectToSettlePPOB(ctx context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string) ([]*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	err, nomorTransaksiSettlement := a.MemberRepository.GenerateCode(ctx, "RTS")
	if err != nil {
		return nil, err
	}
	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	}
	if len(b) > 0 {
		//Pulsa
		b, err := a.PPOBRepository.SettlePPOB(ctx, b, tableName, nomorTransaksiSettlement, 4)
		if err != nil {
			return nil, err
		}
		//Insert To Journal
		for _, ppob := range b {
			// Reverse Settlement Journal
			ppob, err := a.PPOBRepository.GetByID(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			err = a.InsertJournalReverseReject(ctx, *ppob, "T0017", operator)
			if err != nil {
				return nil, err
			}
		}
		err = a.InsertJournalSettlement(ctx, b, "T0017", operator)
		if err != nil {
			return nil, err
		}
	}

	if len(plns) > 0 {
		//PLN
		_, err := a.PPOBRepository.SettlePPOBPLN(ctx, plns, tableName, nomorTransaksiSettlement, 4)
		if err != nil {
			return nil, err
		}

		for _, ppob := range plns {
			//Insert To Journal
			ppob, err := a.GetByIDPLN(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			err = a.InsertJournalReverseRejectPLN(ctx, *ppob, "T0017", operator)
			if err != nil {
				return nil, err
			}
		}
		//Insert To Journal
		err = a.InsertJournalSettlementPLN(ctx, plns, "T0017", operator)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}
func (a *PPOBUsecase) ReversePPOB(ctx context.Context, b []*models.PPOB, plns []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string) ([]*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	}
	if len(b) > 0 {
		//Pulsa
		ppobs, err := a.PPOBRepository.SettlePPOB(ctx, b, tableName, nomorTransaksiSettlement, 5)
		if err != nil {
			return nil, err
		}
		for _, ppob := range ppobs {
			//Insert To Journal
			ppob, err := a.PPOBRepository.GetByID(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			billPayment, err := a.GetFeeAmount(ctx, ppob.RequestAmount, operator)
			if err != nil {
				return nil, err
			}
			ppob.AdminAmount = billPayment.AdminAmount
			ppob.InvelliAmount = billPayment.InvelliAmount
			ppob.PartnerAmount = billPayment.PartnerAmount
			ppob.TransactionDate = time.Now()
			err = a.InsertJournalReverse(ctx, *ppob, "T0017", operator)
		}
	}

	if len(plns) > 0 {
		// PLN
		plns, err := a.PPOBRepository.SettlePPOBPLN(ctx, plns, tableName, nomorTransaksiSettlement, 5)
		if err != nil {
			return nil, err
		}
		for _, ppob := range plns {
			//Insert To Journal
			ppob, err := a.GetByIDPLN(ctx, ppob.ID, tableName)
			if err != nil {
				return nil, err
			}
			err = a.InsertJournalReversePLN(ctx, *ppob, "T0017", operator)
			if err != nil {
				return nil, err
			}
		}
	}
	return b, nil
}
func (a *PPOBUsecase) GetByID(ctx context.Context, id string, operator string) (*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	b, err := a.PPOBRepository.GetByID(ctx, id, operator)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (a *PPOBUsecase) GetByIDPLN(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()
	var b *models.PLNPrepaid
	var err error
	if operator == "PLNPostpaidLog" {
		b, err = a.PPOBRepository.GetByIDPLNPostpaid(ctx, id, operator)
		if err != nil {
			return nil, err
		}
	}
	if operator == "PLNPrepaidLog" {
		b, err = a.PPOBRepository.GetByIDPLNPrepaid(ctx, id, operator)
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}
func (a *PPOBUsecase) InsertJournal(ctx context.Context, b models.PPOB, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	// journalmappingrepository := journalmappingrepository.NewSqlJournalMappingRepository()
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("Beli " + operator),
		Code:             service.NewNullString(b.ResponseReference),
		JournalMappingID: journalmapping.ID,
	}
	account, err := a.AccountRepository.GetByCode(ctx, &b.RequestAccountNo)
	if err != nil {
		return err
	}
	sellPrice := 0.0
	if journalCode == "T0012" && operator == "Bayar Listrik" {
		operator = "PLNPOSTPAID"
		billPayment, err := a.BillPaymentRepository.GetByID(ctx, "7D524FF8-CF0E-4FAF-A7DB-6B3A1B043F1B")
		if err != nil {
			return err
		}
		sellPrice = billPayment.SellPrice
	} else {
		if journalCode == "T0011" && operator == "Pulsa Listrik" {
			operator = "PLNPREPAID"
		}
		billPayment, err := a.GetFeeAmount(ctx, b.RequestAmount, operator)
		if err != nil {
			return err
		}
		sellPrice = billPayment.SellPrice
	}
	journalDetailID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           sellPrice,
		Credit:          0,
		Notes:           "Beli " + operator, //"Rekening Tabungan",
		ReferenceNumber: account.ID,
	}

	//Credit Kewajiban Sementara
	journalDetailCreditKWSID := guid.New().StringUpper()
	journalDetailCreditKWS := models.JournalDetail{
		ID:              journalDetailCreditKWSID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           0,
		Credit:          sellPrice,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReverseSettlement(ctx context.Context, b []*models.PPOB, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	}
	reffNo := ""
	for _, p := range b {
		ppob, err := a.PPOBRepository.GetByID(ctx, p.ID, tableName)
		if err != nil {
			return err
		}
		billPayment, err := a.GetFeeAmount(ctx, ppob.RequestAmount, operator)
		if err != nil {
			return err
		}
		reffNo = ppob.NomorTransaksiSettlement

		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString("Reverse Settlement PPOB " + operator),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}

		//Credit KWS
		journalDetailID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
			Debit:           0,
			Credit:          billPayment.SellPrice,
			Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
			ReferenceNumber: "",
		}

		//Debit KWS Prismalink
		journalDetailCreditKWSID := guid.New().StringUpper()
		journalDetailCreditKWS := models.JournalDetail{
			ID:              journalDetailCreditKWSID,
			JournalID:       journal.ID,
			COAID:           "874CB58C-2123-48FA-8C74-4FC4C219D440",
			Debit:           billPayment.SellPrice - billPayment.PartnerAmount - billPayment.InvelliAmount,
			Credit:          0,
			Notes:           "Kewajiban Segera - Prismalink",
			ReferenceNumber: "",
		}

		//Debit Pendapatan Invelli
		journalDetailCreditInvelliID := guid.New().StringUpper()
		journalDetailCreditInvelli := models.JournalDetail{
			ID:              journalDetailCreditInvelliID,
			JournalID:       journal.ID,
			COAID:           "294F21F6-94A0-4DF4-A208-3FFA76A12D1B",
			Debit:           billPayment.InvelliAmount,
			Credit:          0,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: "",
		}

		//Debit Bagi Hasil ISP
		journalDetailCreditISPID := guid.New().StringUpper()
		journalDetailCreditISP := models.JournalDetail{
			ID:              journalDetailCreditISPID,
			JournalID:       journal.ID,
			COAID:           "B6113005-CAC3-4152-87D3-13761757C435",
			Debit:           billPayment.PartnerAmount,
			Credit:          0,
			Notes:           "Bagi Hasil ISP",
			ReferenceNumber: "",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditInvelli)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditISP)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}

	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReverseSettlementPLN(ctx context.Context, b []*models.PLNPrepaid, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	}
	reffNo := ""
	for _, p := range b {
		var ppob *models.PLNPrepaid
		enumAmount := 0.0
		if operator == "PLNPREPAID" {
			ppob, err = a.PPOBRepository.GetByIDPLNPrepaid(ctx, p.ID, tableName)
			enumAmount = ppob.RequestAmount - ppob.AdminAmount
		} else {
			ppob, err = a.PPOBRepository.GetByIDPLNPostpaid(ctx, p.ID, tableName)
			enumAmount = ppob.RequestAmount
		}
		if err != nil {
			return err
		}
		billPayment, err := a.GetFeeAmount(ctx, enumAmount, operator)
		if err != nil {
			return err
		}
		reffNo = ppob.NomorTransaksiSettlement

		journal := models.Journal{
			ID:               journalID,
			TransactionID:    service.NewEmptyGuid(),
			PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
			Description:      service.NewNullString("Reverse Settlement PPOB " + operator),
			Code:             service.NewNullString(reffNo),
			JournalMappingID: journalmapping.ID,
		}

		//Credit KWS
		journalDetailID := guid.New().StringUpper()
		journalDetailDebit := models.JournalDetail{
			ID:              journalDetailID,
			JournalID:       journal.ID,
			COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
			Debit:           0,
			Credit:          billPayment.SellPrice,
			Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
			ReferenceNumber: "",
		}

		//Debit KWS Prismalink
		journalDetailCreditKWSID := guid.New().StringUpper()
		journalDetailCreditKWS := models.JournalDetail{
			ID:              journalDetailCreditKWSID,
			JournalID:       journal.ID,
			COAID:           "874CB58C-2123-48FA-8C74-4FC4C219D440",
			Debit:           billPayment.SellPrice - billPayment.PartnerAmount - billPayment.InvelliAmount,
			Credit:          0,
			Notes:           "Kewajiban Segera - Prismalink",
			ReferenceNumber: "",
		}

		//Debit Pendapatan Invelli
		journalDetailCreditInvelliID := guid.New().StringUpper()
		journalDetailCreditInvelli := models.JournalDetail{
			ID:              journalDetailCreditInvelliID,
			JournalID:       journal.ID,
			COAID:           "294F21F6-94A0-4DF4-A208-3FFA76A12D1B",
			Debit:           billPayment.InvelliAmount,
			Credit:          0,
			Notes:           "Pendapatan Invelli",
			ReferenceNumber: "",
		}

		//Debit Bagi Hasil ISP
		journalDetailCreditISPID := guid.New().StringUpper()
		journalDetailCreditISP := models.JournalDetail{
			ID:              journalDetailCreditISPID,
			JournalID:       journal.ID,
			COAID:           "B6113005-CAC3-4152-87D3-13761757C435",
			Debit:           billPayment.PartnerAmount,
			Credit:          0,
			Notes:           "Bagi Hasil ISP",
			ReferenceNumber: "",
		}
		journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditInvelli)
		journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditISP)

		JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
		_, err = JournalUsecase.Store(ctx, &journal)
		if err != nil {
			return err
		}

	}
	return nil
}
func (a *PPOBUsecase) InsertJournalSettlement(ctx context.Context, b []*models.PPOB, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	}

	totalAmount := 0.0
	totalFeeAmount := 0.0
	totalInvelliAmount := 0.0
	reffNo := ""
	for _, p := range b {
		ppob, err := a.PPOBRepository.GetByID(ctx, p.ID, tableName)
		if err != nil {
			return err
		}
		billPayment, err := a.GetFeeAmount(ctx, ppob.RequestAmount, operator)
		if err != nil {
			return err
		}
		totalAmount += (billPayment.SellPrice)
		totalFeeAmount += billPayment.PartnerAmount
		totalInvelliAmount += billPayment.InvelliAmount
		reffNo = ppob.NomorTransaksiSettlement
	}

	journal := models.Journal{
		ID:               journalID,
		TransactionID:    service.NewEmptyGuid(),
		PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("Settlement PPOB " + operator),
		Code:             service.NewNullString(reffNo),
		JournalMappingID: journalmapping.ID,
	}

	//Debit KWS
	journalDetailID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           totalAmount,
		Credit:          0,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit KWS Prismalink
	journalDetailCreditKWSID := guid.New().StringUpper()
	journalDetailCreditKWS := models.JournalDetail{
		ID:              journalDetailCreditKWSID,
		JournalID:       journal.ID,
		COAID:           "874CB58C-2123-48FA-8C74-4FC4C219D440",
		Debit:           0,
		Credit:          totalAmount - totalFeeAmount - totalInvelliAmount,
		Notes:           "Kewajiban Segera - Prismalink",
		ReferenceNumber: "",
	}

	//Credit Pendapatan Invelli
	journalDetailCreditInvelliID := guid.New().StringUpper()
	journalDetailCreditInvelli := models.JournalDetail{
		ID:              journalDetailCreditInvelliID,
		JournalID:       journal.ID,
		COAID:           "294F21F6-94A0-4DF4-A208-3FFA76A12D1B",
		Debit:           0,
		Credit:          totalInvelliAmount,
		Notes:           "Pendapatan Invelli",
		ReferenceNumber: "",
	}

	//Credit Bagi Hasil ISP
	journalDetailCreditISPID := guid.New().StringUpper()
	journalDetailCreditISP := models.JournalDetail{
		ID:              journalDetailCreditISPID,
		JournalID:       journal.ID,
		COAID:           "B6113005-CAC3-4152-87D3-13761757C435",
		Debit:           0,
		Credit:          totalFeeAmount,
		Notes:           "Bagi Hasil ISP",
		ReferenceNumber: "",
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditInvelli)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditISP)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalSettlementPLN(ctx context.Context, b []*models.PLNPrepaid, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	tableName := ""
	switch operator {
	case "SIMPATI":
		tableName = "TelkomselSimpatiPrepaidLog"
		break
	case "XL":
		tableName = "XLBebasPrepaidLog"
		break
	case "INDOSAT":
		tableName = "IndosatIM3PrepaidLog"
		break
	case "THREE":
		tableName = "ThreePrepaidLog"
		break
	case "PLNPREPAID":
		tableName = "PLNPrepaidLog"
		break
	case "PLNPOSTPAID":
		tableName = "PLNPostpaidLog"
		break
	}

	totalAmount := 0.0
	totalFeeAmount := 0.0
	totalInvelliAmount := 0.0
	reffNo := ""
	for _, p := range b {
		ppob, err := a.GetByIDPLN(ctx, p.ID, tableName)
		if err != nil {
			return err
		}
		enumAmount := ppob.RequestAmount
		if operator == "PLNPREPAID" {
			enumAmount = ppob.RequestAmount - ppob.AdminAmount
		}
		billPayment, err := a.GetFeeAmount(ctx, enumAmount, operator)
		if err != nil {
			return err
		}

		debitAmount := (ppob.RequestAmount + billPayment.BasePrice + billPayment.AdminAmount)
		if operator == "PLNPREPAID" {
			debitAmount = billPayment.SellPrice
		}
		totalAmount += debitAmount
		totalFeeAmount += billPayment.PartnerAmount
		totalInvelliAmount += billPayment.InvelliAmount
		reffNo = ppob.NomorTransaksiSettlement
	}

	journal := models.Journal{
		ID:               journalID,
		TransactionID:    service.NewEmptyGuid(),
		PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("Settlement PPOB " + operator),
		Code:             service.NewNullString(reffNo),
		JournalMappingID: journalmapping.ID,
	}

	//Debit KWS
	journalDetailID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           totalAmount,
		Credit:          0,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit KWS Prismalink
	journalDetailCreditKWSID := guid.New().StringUpper()
	journalDetailCreditKWS := models.JournalDetail{
		ID:              journalDetailCreditKWSID,
		JournalID:       journal.ID,
		COAID:           "874CB58C-2123-48FA-8C74-4FC4C219D440",
		Debit:           0,
		Credit:          totalAmount - totalFeeAmount - totalInvelliAmount,
		Notes:           "Kewajiban Segera - Prismalink",
		ReferenceNumber: "",
	}

	//Credit Pendapatan Invelli
	journalDetailCreditInvelliID := guid.New().StringUpper()
	journalDetailCreditInvelli := models.JournalDetail{
		ID:              journalDetailCreditInvelliID,
		JournalID:       journal.ID,
		COAID:           "294F21F6-94A0-4DF4-A208-3FFA76A12D1B",
		Debit:           0,
		Credit:          totalInvelliAmount,
		Notes:           "Pendapatan Invelli",
		ReferenceNumber: "",
	}

	//Credit Bagi Hasil ISP
	journalDetailCreditISPID := guid.New().StringUpper()
	journalDetailCreditISP := models.JournalDetail{
		ID:              journalDetailCreditISPID,
		JournalID:       journal.ID,
		COAID:           "B6113005-CAC3-4152-87D3-13761757C435",
		Debit:           0,
		Credit:          totalFeeAmount,
		Notes:           "Bagi Hasil ISP",
		ReferenceNumber: "",
	}
	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditInvelli)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditISP)

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReverse(ctx context.Context, b models.PPOB, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	oldJournals := JournalUsecase.GetByTransactionID(ctx, &b.ID)
	debitAmount := 0.0
	creditAmount := 0.0
	for _, oldJournal := range oldJournals {
		if oldJournal.DebitAmount > 0 {
			debitAmount = oldJournal.DebitAmount
		} else if oldJournal.CreditAmount > 0 {
			creditAmount = oldJournal.CreditAmount
		}
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("REJECT Beli " + operator),
		Code:             service.NewNullString(b.NomorTransaksiSettlement),
		JournalMappingID: journalmapping.ID,
	}
	account, err := a.AccountRepository.GetByCode(ctx, &b.RequestAccountNo)
	if err != nil {
		return err
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           creditAmount,
		Credit:          0,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit Kewajiban Sementara
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           0,
		Credit:          debitAmount,
		Notes:           "REJECT Beli " + operator, //"Rekening Tabungan",
		ReferenceNumber: account.ID,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReversePLN(ctx context.Context, b models.PLNPrepaid, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	oldJournals := JournalUsecase.GetByTransactionID(ctx, &b.ID)
	debitAmount := 0.0
	creditAmount := 0.0
	for _, oldJournal := range oldJournals {
		if oldJournal.DebitAmount > 0 {
			debitAmount = oldJournal.DebitAmount
		} else if oldJournal.CreditAmount > 0 {
			creditAmount = oldJournal.CreditAmount
		}
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("REJECT Beli " + operator),
		Code:             service.NewNullString(b.NomorTransaksiSettlement),
		JournalMappingID: journalmapping.ID,
	}
	account, err := a.AccountRepository.GetByCode(ctx, &b.RequestAccountNo)
	if err != nil {
		return err
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           debitAmount,
		Credit:          0,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit Kewajiban Sementara
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           0,
		Credit:          creditAmount,
		Notes:           "REJECT Beli " + operator, //"Rekening Tabungan",
		ReferenceNumber: account.ID,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReverseReject(ctx context.Context, b models.PPOB, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}
	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	oldJournals := JournalUsecase.GetByTransactionID(ctx, &b.ID)
	debitAmount := 0.0
	creditAmount := 0.0
	for _, oldJournal := range oldJournals {
		if oldJournal.DebitAmount > 0 {
			debitAmount = oldJournal.DebitAmount
		} else if oldJournal.CreditAmount > 0 {
			creditAmount = oldJournal.CreditAmount
		}
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      b.TransactionDate.Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("REJECT Beli " + operator),
		Code:             service.NewNullString(b.ResponseReference),
		JournalMappingID: journalmapping.ID,
	}
	account, err := a.AccountRepository.GetByCode(ctx, &b.RequestAccountNo)
	if err != nil {
		return err
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           0,
		Credit:          creditAmount,
		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit Kewajiban Sementara
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           debitAmount,
		Credit:          0,
		Notes:           "REJECT Beli " + operator, //"Rekening Tabungan",
		ReferenceNumber: account.ID,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)
	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) InsertJournalReverseRejectPLN(ctx context.Context, b models.PLNPrepaid, journalCode string, operator string) error {
	journalID := guid.New().StringUpper()
	//notes := ""
	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
	if err != nil {
		return err
	}

	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
	oldJournals := JournalUsecase.GetByTransactionID(ctx, &b.ID)
	debitAmount := 0.0
	creditAmount := 0.0
	for _, oldJournal := range oldJournals {
		if oldJournal.DebitAmount > 0 {
			debitAmount = oldJournal.DebitAmount
		} else if oldJournal.CreditAmount > 0 {
			creditAmount = oldJournal.CreditAmount
		}
	}
	journal := models.Journal{
		ID:               journalID,
		TransactionID:    b.ID,
		PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
		Description:      service.NewNullString("REJECT Beli " + operator),
		Code:             service.NewNullString(b.NomorTransaksiSettlement),
		JournalMappingID: journalmapping.ID,
	}
	account, err := a.AccountRepository.GetByCode(ctx, &b.RequestAccountNo)
	if err != nil {
		return err
	}
	journalDetailDebitID := guid.New().StringUpper()
	journalDetailDebit := models.JournalDetail{
		ID:              journalDetailDebitID,
		JournalID:       journal.ID,
		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
		Debit:           debitAmount,
		Credit:          0,
		Notes:           "REJECT Beli " + operator, //"Kewajiban Segera - Titipan Transaksi Mobile",
		ReferenceNumber: "",
	}

	//Credit Kewajiban Sementara
	journalDetailCreditID := guid.New().StringUpper()
	journalDetailCredit := models.JournalDetail{
		ID:              journalDetailCreditID,
		JournalID:       journal.ID,
		COAID:           "0A3EDE5B-0D36-4B7F-9453-143A8C08E26B",
		Debit:           0,
		Credit:          creditAmount,
		Notes:           "Rekening Tabungan",
		ReferenceNumber: account.ID,
	}

	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
	journal.JournalDetails = append(journal.JournalDetails, journalDetailCredit)

	_, err = JournalUsecase.Store(ctx, &journal)
	if err != nil {
		return err
	}
	return nil
}
func (a *PPOBUsecase) UpdateStatus(ctx context.Context, ppob *models.PPOB, tableName string, status int32) (*models.PPOB, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	b, err := a.PPOBRepository.UpdateStatus(ctx, ppob, tableName, status)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (a *PPOBUsecase) UpdateStatusPLNPostpaid(ctx context.Context, pln *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	b, err := a.PPOBRepository.UpdateStatusPLNPrepaid(ctx, pln, status)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (a *PPOBUsecase) UpdateStatusPLNPrepaid(ctx context.Context, pln *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	b, err := a.PPOBRepository.UpdateStatusPLNPostpaid(ctx, pln, status)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (a *PPOBUsecase) CheckStatusPPOB(ctx context.Context, ppob *models.PPOB, route string) (*models.PPOB, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	ppob, err := a.PPOBRepository.CheckStatusPPOB(ctx, ppob, route)
	if err != nil {
		return nil, err
	}
	return ppob, nil
}
func (a *PPOBUsecase) Fetch(c context.Context, status *int32) ([]*models.PPOB, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listPPOB, err := a.PPOBRepository.Fetch(ctx, status)
	if err != nil {
		return nil, err
	}
	return listPPOB, nil
}
