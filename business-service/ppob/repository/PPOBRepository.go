package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/beevik/guid"
	ppob "gitlab.com/robertinc/cardlez-api/business-service/ppob"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlPPOBRepository struct {
	Conn *sql.DB
}

func NewSqlPPOBRepository() ppob.PPOBRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlPPOBRepository{conn}
}

func NewSqlPPOBRepositoryV2(Conn *sql.DB) ppob.PPOBRepository {
	conn := Conn
	return &sqlPPOBRepository{conn}
}
func (sj *sqlPPOBRepository) Store(ctx context.Context, a *models.PPOB, tableName string) (*models.PPOB, error) {
	a.ID = guid.New().StringUpper()
	ppobLogQuery := `
	INSERT INTO ` + tableName + ` (
		ID, 
		TransactionDate, 
		RequestAccountNo ,
		RequestPhoneNumber ,
		RequestAmount ,
		ResponsePhoneNumber ,
		ResponseAmount ,
		ResponseReference ,
		ResponseRawMessage ,
		AdminAmount ,
		PartnerAmount ,
		InvelliAmount ,
		StatusCode ,
		CompanyId ,
		MessagePrinter ,
		TransStatus ,
		User_Insert ,
		Date_Insert ,
		User_Update ,
		Date_Update ,
		User_Authorize ,
		Date_Authorize,
		NomorTransaksi
	) VALUES (
		@ID,
		@TransactionDate, 
		@RequestAccountNo ,
		@RequestPhoneNumber ,
		@RequestAmount ,
		@ResponsePhoneNumber ,
		@ResponseAmount ,
		@ResponseReference ,
		@ResponseRawMessage ,
		@AdminAmount ,
		@PartnerAmount ,
		@InvelliAmount ,
		@StatusCode ,
		@CompanyID ,
		@MessagePrinter ,
		@TransStatus ,
		@UserInsert ,
		GETDATE() ,
		@UserUpdate ,
		GETDATE() ,
		@UserAuthorize ,
		GETDATE(),
		@NomorTransaksi
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransactionDate", a.TransactionDate),
		sql.Named("RequestAccountNo", a.RequestAccountNo),
		sql.Named("RequestPhoneNumber", a.RequestPhoneNumber),
		sql.Named("RequestAmount", a.RequestAmount),
		sql.Named("ResponsePhoneNumber", a.ResponsePhoneNumber),
		sql.Named("ResponseAmount", a.ResponseAmount),
		sql.Named("ResponseReference", a.ResponseReference),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
		sql.Named("AdminAmount", a.AdminAmount),
		sql.Named("PartnerAmount", a.PartnerAmount),
		sql.Named("InvelliAmount", a.InvelliAmount),
		sql.Named("StatusCode", a.StatusCode),
		sql.Named("CompanyID", "15C55F75-85AD-4431-B1A6-DFBC7516925F"),
		sql.Named("MessagePrinter", a.MessagePrinter),
		sql.Named("TransStatus", 1),
		sql.Named("UserInsert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("UserUpdate", service.NewEmptyGuid()),
		sql.Named("UserAuthorize", service.NewEmptyGuid()),
		sql.Named("NomorTransaksi", a.NomorTransaksi),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPPOBRepository) UpdateStatus(ctx context.Context, a *models.PPOB, tableName string, status int32) (*models.PPOB, error) {
	ppobLogQuery := `
	UPDATE ` + tableName + ` 
	SET 
		TransStatus = @status,
		StatusCode = @statuscode,
		ResponsePhoneNumber = @responsePhoneNumber,
		ResponseAmount = @responseAmount,
		ResponseReference = @responseReference,
		ResponseRawMessage = @responseRawMessage
	WHERE ID = @ID
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("status", status),
		sql.Named("statuscode", a.StatusCode),
		sql.Named("responsePhoneNumber", a.ResponsePhoneNumber),
		sql.Named("responseAmount", a.ResponseAmount),
		sql.Named("responseReference", a.ResponseReference),
		sql.Named("responseRawMessage", a.ResponseRawMessage),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlPPOBRepository) CallPPOB(ctx context.Context, a *models.PPOB, route string) (*models.PPOB, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]interface{}{
		"AccountNumber": a.RequestAccountNo,
		"BillNumber":    a.RequestPhoneNumber,
		"PaymentAmount": a.RequestAmount,
	}
	ppob, err := ppobService.SendPulsa(route, payload)
	a.TransactionDate = ppob.TransactionDate
	a.ResponseAmount = ppob.ResponseAmount
	a.ResponsePhoneNumber = ppob.ResponsePhoneNumber
	a.StatusCode = ppob.StatusCode
	a.ResponseRawMessage = ppob.ResponseRawMessage
	a.ResponseAmount = ppob.ResponseAmount
	a.ResponseReference = ppob.ResponseReference
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return a, err
	}
	return a, nil
}
func (sj *sqlPPOBRepository) CheckStatusPPOB(ctx context.Context, a *models.PPOB, route string) (*models.PPOB, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]string{
		"RefNo":   a.ResponseReference,
		"Tanggal": a.TransactionDate.Format("02-01-2006"),
	}
	ppob, err := ppobService.CheckPPOB(route, payload)
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	a.TransactionDate = ppob.TransactionDate
	a.ResponseAmount = ppob.ResponseAmount
	a.ResponsePhoneNumber = ppob.ResponsePhoneNumber
	a.StatusCode = ppob.StatusCode
	a.ResponseRawMessage = ppob.ResponseRawMessage
	a.ResponseAmount = ppob.ResponseAmount
	a.ResponseReference = ppob.ResponseReference

	return a, nil
}
func (sj *sqlPPOBRepository) StorePLNPrepaidInquiry(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	a.ID = guid.New().StringUpper()
	ppobLogQuery := `
	INSERT INTO PLNPrepaidInquiryLog (
		ID
      ,TransactionDate
      ,RequestAccountNo
      ,RequestCustomerNumber
      ,ResponseMeterNo
      ,ResponsePLNCustomerNo
      ,ResponseCustomerName
      ,ResponseType
      ,ResponseTarif
      ,ResponseFiller
      ,ResponseType2
      ,ResponseAdminFee
      ,ResponseRawMessage
      ,CompanyId
      ,User_Insert
      ,Date_Insert
      ,User_Update
	  ,Date_Update
	) VALUES (
		@ID
      ,GETDATE()
      ,@RequestAccountNo
      ,@RequestCustomerNumber
      ,@ResponseMeterNo
      ,@ResponsePLNCustomerNo
      ,@ResponseCustomerName
      ,@ResponseType
      ,@ResponseTarif
      ,@ResponseFiller
      ,@ResponseType2
      ,@ResponseAdminFee
      ,@ResponseRawMessage
      ,@CompanyId
      ,@User_Insert
      ,GETDATE()
      ,@User_Update
	  ,GETDATE()
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransactionDate", a.TransactionDate),
		sql.Named("RequestAccountNo", a.RequestAccountNo),
		sql.Named("RequestCustomerNumber", a.RequestCustomerNumber),
		sql.Named("ResponseMeterNo", a.ResponseMeterNo),
		sql.Named("ResponsePLNCustomerNo", a.ResponsePLNCustomerNo),
		sql.Named("ResponseCustomerName", a.ResponseCustomerName),
		sql.Named("ResponseType", a.ResponseType),
		sql.Named("ResponseTarif", a.ResponseTarif),
		sql.Named("ResponseFiller", a.ResponseFiller),
		sql.Named("ResponseType2", a.ResponseType2),
		sql.Named("ResponseAdminFee", a.ResponseAdminFee),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
		sql.Named("CompanyID", "15C55F75-85AD-4431-B1A6-DFBC7516925F"),
		sql.Named("User_Insert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("User_Update", service.NewEmptyGuid()),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPPOBRepository) CallPLNPrepaidInquiry(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]interface{}{
		"BillNumber": a.RequestCustomerNumber,
	}
	inquiry, err := ppobService.SendPLN(route, payload)
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	a.ResponseMeterNo = inquiry.ResponseMeterNo
	a.ResponsePLNCustomerNo = inquiry.ResponsePLNCustomerNo
	a.ResponseCustomerName = inquiry.ResponseCustomerName
	a.ResponseType = inquiry.ResponseType
	a.ResponseTarif = inquiry.ResponseTarif
	a.ResponseFiller = inquiry.ResponseFiller
	a.ResponseType2 = inquiry.ResponseType2
	a.ResponseAdminFee = inquiry.ResponseAdminFee
	a.ResponseRawMessage = inquiry.ResponseRawMessage

	return a, nil
}
func (sj *sqlPPOBRepository) StorePLNPrepaid(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	a.ID = guid.New().StringUpper()
	ppobLogQuery := `
	INSERT INTO PLNPrepaidLog (
		ID 
	  ,TransactionDate
      ,NomorTransaksi
      ,RequestAccountNo
      ,RequestCustomerNumber
      ,RequestAmount
      ,ResponseMeterNo
      ,ResponsePLNCustomerNo
      ,ResponseCustomerName
      ,ResponseTarif
      ,ResponseAdminFee
      ,ResponseTotalBayar
      ,ResponseKodeToken
      ,ResponseDate
      ,ResponseKWH
      ,ResponseRawMessage
      ,AdminAmount
      ,PartnerAmount
      ,InvelliAmount
      ,StatusCode
      ,CompanyId
      ,MessagePrinter
      ,TransStatus
      ,User_Insert
      ,Date_Insert
      ,User_Update
      ,Date_Update
      ,User_Authorize
      ,Date_Authorize
	  ,NomorTransaksiSettlement
	  ,ResponseReference
	) VALUES (
		@ID
	  ,@TransactionDate
      ,@NomorTransaksi
      ,@RequestAccountNo
      ,@RequestCustomerNumber
      ,@RequestAmount
      ,@ResponseMeterNo
      ,@ResponsePLNCustomerNo
      ,@ResponseCustomerName
      ,@ResponseTarif
      ,@ResponseAdminFee
      ,@ResponseTotalBayar
      ,@ResponseKodeToken
      ,@ResponseDate
      ,@ResponseKWH
      ,@ResponseRawMessage
      ,@AdminAmount
      ,@PartnerAmount
      ,@InvelliAmount
      ,@StatusCode
      ,@CompanyId
      ,@MessagePrinter
      ,@TransStatus
      ,@User_Insert
      ,GETDATE() 
      ,@User_Update
      ,GETDATE() 
      ,@User_Authorize
	  ,GETDATE() 
	  ,@NomorTransaksiSettlement
	  ,@ResponseReference
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransactionDate", a.TransactionDate),
		sql.Named("NomorTransaksi", a.NomorTransaksi),
		sql.Named("RequestAccountNo", a.RequestAccountNo),
		sql.Named("RequestCustomerNumber", a.RequestCustomerNumber),
		sql.Named("RequestAmount", a.RequestAmount),
		sql.Named("ResponseMeterNo", a.ResponseMeterNo),
		sql.Named("ResponsePLNCustomerNo", a.ResponsePLNCustomerNo),
		sql.Named("ResponseCustomerName", a.ResponseCustomerName),
		sql.Named("ResponseTarif", a.ResponseTarif),
		sql.Named("ResponseAdminFee", a.ResponseAdminFee),
		sql.Named("ResponseTotalBayar", a.ResponseTotalBayar),
		sql.Named("ResponseKodeToken", a.ResponseKodeToken),
		sql.Named("ResponseDate", a.ResponseDate),
		sql.Named("ResponseKWH", a.ResponseKWH),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
		sql.Named("AdminAmount", a.AdminAmount),
		sql.Named("PartnerAmount", a.PartnerAmount),
		sql.Named("InvelliAmount", a.InvelliAmount),
		sql.Named("StatusCode", a.StatusCode),
		sql.Named("CompanyID", "15C55F75-85AD-4431-B1A6-DFBC7516925F"),
		sql.Named("MessagePrinter", ""),
		sql.Named("TransStatus", 1),
		sql.Named("User_Insert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("User_Authorize", service.NewEmptyGuid()),
		sql.Named("NomorTransaksiSettlement", a.NomorTransaksiSettlement),
		sql.Named("ResponseReference", a.ResponseReference),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPPOBRepository) UpdateStatusPLNPrepaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error) {
	ppobLogQuery := `
	UPDATE PLNPrepaidLog 
	SET TransStatus = @TransStatus,
		StatusCode = @statuscode,
		ResponseMeterNo = @ResponseMeterNo,
		ResponsePLNCustomerNo = @ResponsePLNCustomerNo,
		ResponseCustomerName = @ResponseCustomerName,
		ResponseTarif = @ResponseTarif,
		ResponseAdminFee = @ResponseAdminFee,
		ResponseTotalBayar = @ResponseTotalBayar,
		ResponseKodeToken = @ResponseKodeToken,
		ResponseDate = @ResponseDate,
		ResponseKWH = @ResponseKWH,
		ResponseRawMessage = @ResponseRawMessage
	WHERE ID = @ID`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransStatus", status),
		sql.Named("statuscode", a.StatusCode),
		sql.Named("ResponseMeterNo", a.ResponseMeterNo),
		sql.Named("ResponsePLNCustomerNo", a.ResponsePLNCustomerNo),
		sql.Named("ResponseCustomerName", a.ResponseCustomerName),
		sql.Named("ResponseTarif", a.ResponseTarif),
		sql.Named("ResponseAdminFee", a.ResponseAdminFee),
		sql.Named("ResponseTotalBayar", a.ResponseTotalBayar),
		sql.Named("ResponseKodeToken", a.ResponseKodeToken),
		sql.Named("ResponseDate", a.ResponseDate),
		sql.Named("ResponseKWH", a.ResponseKWH),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlPPOBRepository) CallPLNPrepaid(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]interface{}{
		"AccountNumber": a.RequestAccountNo,
		"BillNumber":    a.RequestCustomerNumber,
		"PaymentAmount": a.RequestAmount,
	}
	plnPrepaid, err := ppobService.SendPLN(route, payload)
	a.ResponseMeterNo = plnPrepaid.ResponseMeterNo
	a.ResponsePLNCustomerNo = plnPrepaid.ResponsePLNCustomerNo
	a.ResponseCustomerName = plnPrepaid.ResponseCustomerName
	a.ResponseTarif = plnPrepaid.ResponseTarif
	a.ResponseAdminFee = plnPrepaid.ResponseAdminFee
	a.ResponseTotalBayar = plnPrepaid.ResponseTotalBayar
	a.ResponseKodeToken = plnPrepaid.ResponseKodeToken
	a.ResponseDate = plnPrepaid.ResponseDate
	a.ResponseKWH = plnPrepaid.ResponseKWH
	a.ResponseRawMessage = plnPrepaid.ResponseRawMessage
	a.StatusCode = plnPrepaid.StatusCode
	a.ResponseReference = plnPrepaid.ResponseReference
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return a, err
	}
	return a, nil
}
func (sj *sqlPPOBRepository) StorePLNPostpaidInquiry(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	a.ID = guid.New().StringUpper()
	ppobLogQuery := `
	INSERT INTO PLNPostpaidInquiryLog (
		ID
		,TransactionDate
		,RequestAccountNo
		,RequestCustomerNumber
		,ResponsePLNCustomerNo
		,ResponseCustomerName
		,ResponseBillPeriod
		,ResponseNumberOfBill
		,ResponseAdminFee
		,ResponseUsage
		,ResponseTrxType
		,ResponseBillAmount
		,ResponseRawMessage
		,CompanyId
		,User_Insert
		,Date_Insert
		,User_Update
		,Date_Update
	) VALUES (
		@ID
		,GETDATE()
		,@RequestAccountNo
		,@RequestCustomerNumber
		,@ResponsePLNCustomerNo
		,@ResponseCustomerName
		,@ResponseBillPeriod
		,@ResponseNumberOfBill
		,@ResponseAdminFee
		,@ResponseUsage
		,@ResponseTrxType
		,@ResponseBillAmount
		,@ResponseRawMessage
		,@CompanyId
		,@User_Insert
		,GETDATE()
		,@User_Update
		,GETDATE()
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransactionDate", a.TransactionDate),
		sql.Named("RequestAccountNo", a.RequestAccountNo),
		sql.Named("RequestCustomerNumber", a.RequestCustomerNumber),
		sql.Named("ResponsePLNCustomerNo", a.ResponsePLNCustomerNo),
		sql.Named("ResponseCustomerName", a.ResponseCustomerName),
		sql.Named("ResponseBillPeriod", a.ResponseBillPeriod),
		sql.Named("ResponseNumberOfBill", a.ResponseNumberOfBill),
		sql.Named("ResponseAdminFee", a.ResponseAdminFee),
		sql.Named("ResponseUsage", a.ResponseUsage),
		sql.Named("ResponseTrxType", a.ResponseTrxType),
		sql.Named("ResponseBillAmount", a.ResponseBillAmount),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
		sql.Named("CompanyID", "15C55F75-85AD-4431-B1A6-DFBC7516925F"),
		sql.Named("User_Insert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("User_Update", service.NewEmptyGuid()),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPPOBRepository) CallPLNPostpaidInquiry(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]interface{}{
		"BillNumber":    a.RequestCustomerNumber,
		"AccountNumber": a.RequestAccountNo,
	}
	inquiry, err := ppobService.SendPLN(route, payload)
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	a.ResponsePLNCustomerNo = inquiry.ResponsePLNCustomerNo
	a.ResponseCustomerName = inquiry.ResponseCustomerName
	a.ResponseBillPeriod = inquiry.ResponseBillPeriod
	a.ResponseNumberOfBill = inquiry.ResponseNumberOfBill
	a.ResponseUsage = inquiry.ResponseUsage
	a.ResponseTrxType = inquiry.ResponseTrxType
	a.ResponseBillAmount = inquiry.ResponseBillAmount
	a.PostingDate = inquiry.PostingDate
	a.StrukNo = inquiry.StrukNo
	a.ResponseAdminFee = inquiry.ResponseAdminFee
	a.ResponseRawMessage = inquiry.ResponseRawMessage

	return a, nil
}
func (sj *sqlPPOBRepository) StorePLNPostpaid(ctx context.Context, a *models.PLNPrepaid) (*models.PLNPrepaid, error) {
	a.ID = guid.New().StringUpper()
	ppobLogQuery := `
	INSERT INTO PLNPostpaidLog (
		ID 
		,TransactionDate
		,NomorTransaksi
		,RequestAccountNo
		,RequestCustomerNumber
		,RequestAmount
		,ResponsePLNCustomerNo
		,ResponseCustomerName
		,ResponseBillPeriod
		,ResponseNumberOfBill
		,ResponseAdminFee
		,ResponseUsage
		,ResponseTrxType
		,ResponseBillAmount
		,ResponseRawMessage
		,AdminAmount
		,PartnerAmount
		,InvelliAmount
		,StatusCode
		,CompanyId
		,MessagePrinter
		,TransStatus
		,User_Insert
		,Date_Insert
		,User_Update
		,Date_Update
		,User_Authorize
		,Date_Authorize
		,NomorTransaksiSettlement
		,ResponseReference
	) VALUES (
		@ID
		,GETDATE()
		,@NomorTransaksi
		,@RequestAccountNo
		,@RequestCustomerNumber
		,@RequestAmount
		,@ResponsePLNCustomerNo
		,@ResponseCustomerName
		,@ResponseBillPeriod
		,@ResponseNumberOfBill
		,@ResponseAdminFee
		,@ResponseUsage
		,@ResponseTrxType
		,@ResponseBillAmount
		,@ResponseRawMessage
		,@AdminAmount
		,@PartnerAmount
		,@InvelliAmount
		,@StatusCode
		,@CompanyId
		,@MessagePrinter
		,@TransStatus
		,@User_Insert
		,GETDATE()
		,@User_Update
		,GETDATE()
		,@User_Authorize
		,GETDATE()
		,@NomorTransaksiSettlement
		,@ResponseReference
	)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransactionDate", a.ResponseDate),
		sql.Named("NomorTransaksi", a.NomorTransaksi),
		sql.Named("RequestAccountNo", a.RequestAccountNo),
		sql.Named("RequestCustomerNumber", a.RequestCustomerNumber),
		sql.Named("RequestAmount", a.RequestAmount),
		sql.Named("ResponsePLNCustomerNo", a.ResponsePLNCustomerNo),
		sql.Named("ResponseCustomerName", a.ResponseCustomerName),
		sql.Named("ResponseBillPeriod", a.ResponseBillPeriod),
		sql.Named("ResponseNumberOfBill", a.ResponseNumberOfBill),
		sql.Named("ResponseAdminFee", a.ResponseAdminFee),
		sql.Named("ResponseUsage", a.ResponseUsage),
		sql.Named("ResponseTrxType", a.ResponseTrxType),
		sql.Named("ResponseBillAmount", a.ResponseBillAmount),
		sql.Named("ResponseRawMessage", a.ResponseRawMessage),
		sql.Named("AdminAmount", a.AdminAmount),
		sql.Named("PartnerAmount", a.PartnerAmount),
		sql.Named("InvelliAmount", a.InvelliAmount),
		sql.Named("StatusCode", a.StatusCode),
		sql.Named("CompanyID", "15C55F75-85AD-4431-B1A6-DFBC7516925F"),
		sql.Named("MessagePrinter", ""),
		sql.Named("TransStatus", 1),
		sql.Named("User_Insert", ctx.Value(service.CtxUserID).(string)),
		sql.Named("User_Update", service.NewEmptyGuid()),
		sql.Named("User_Authorize", service.NewEmptyGuid()),
		sql.Named("NomorTransaksiSettlement", a.NomorTransaksiSettlement),
		sql.Named("ResponseReference", a.ResponseReference),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return a, nil
}
func (sj *sqlPPOBRepository) UpdateStatusPLNPostpaid(ctx context.Context, a *models.PLNPrepaid, status int32) (*models.PLNPrepaid, error) {
	ppobLogQuery := `
	UPDATE PLNPostpaidLog 
	SET TransStatus = @TransStatus,
		StatusCode = @statuscode,
		ResponsePLNCustomerNo = @responsePLNCustNo,
		ResponseCustomerName = @responseCustName,
		ResponseBillPeriod = @responseBillPeriod,
		ResponseNumberOfBill = @responseNumberOfBill,
		ResponseAdminFee = @responseAdminFee,
		ResponseUsage = @responseUsage,
		ResponseTrxType = @responseTrxType,
		ResponseBillAmount = @responseBillAmount,
		ResponseRawMessage = @responseRawMessage
	WHERE ID = @ID`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		sql.Named("ID", a.ID),
		sql.Named("TransStatus", status),
		sql.Named("statuscode", a.StatusCode),
		sql.Named("responsePLNCustNo", a.ResponsePLNCustomerNo),
		sql.Named("responseCustName", a.ResponseCustomerName),
		sql.Named("responseBillPeriod", a.ResponseBillPeriod),
		sql.Named("responseNumberOfBill", a.ResponseNumberOfBill),
		sql.Named("responseAdminFee", a.ResponseAdminFee),
		sql.Named("responseUsage", a.ResponseUsage),
		sql.Named("responseTrxType", a.ResponseTrxType),
		sql.Named("responseBillAmount", a.ResponseBillAmount),
		sql.Named("responseRawMessage", a.ResponseRawMessage),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return a, nil
}
func (sj *sqlPPOBRepository) CallPLNPostpaid(ctx context.Context, a *models.PLNPrepaid, route string) (*models.PLNPrepaid, error) {
	ppobService := service.NewPPOBService()
	logger := service.Logger(ctx)
	payload := map[string]interface{}{
		"AccountNumber": a.RequestAccountNo,
		"BillNumber":    a.ResponsePLNCustomerNo,
		"PaymentAmount": a.ResponseBillAmount,
	}
	plnPostpaid, err := ppobService.SendPLN(route, payload)
	a.ResponsePLNCustomerNo = plnPostpaid.ResponsePLNCustomerNo
	a.ResponseCustomerName = plnPostpaid.ResponseCustomerName
	a.ResponseBillPeriod = plnPostpaid.ResponseBillPeriod
	a.ResponseNumberOfBill = plnPostpaid.ResponseNumberOfBill
	a.ResponseUsage = plnPostpaid.ResponseUsage
	a.ResponseTrxType = plnPostpaid.ResponseTrxType
	a.ResponseBillAmount = plnPostpaid.ResponseBillAmount
	a.PostingDate = plnPostpaid.PostingDate
	a.StrukNo = plnPostpaid.StrukNo
	a.ResponseAdminFee = plnPostpaid.ResponseAdminFee
	a.ResponseRawMessage = plnPostpaid.ResponseRawMessage
	a.ResponseReference = plnPostpaid.ResponseReference
	a.StatusCode = plnPostpaid.StatusCode
	if err != nil {
		logger.Error("Request Error",
			zap.String("error", err.Error()),
		)
		return a, err
	}
	return a, nil
}
func (sj *sqlPPOBRepository) SettlePPOB(ctx context.Context, b []*models.PPOB, operator string, nomorTransaksiSettlement string, status int32) ([]*models.PPOB, error) {

	ppobLogQuery := `
		UPDATE ` + operator + ` 
		SET 
			TransStatus = @status
			,NomorTransaksiSettlement = @NomorTransaksiSettlement
			,Date_Update = GETDATE()
		WHERE
			ID IN([params])
	`
	paramValues := make([]interface{}, 0)
	params := make([]string, 0)
	for i, ppob := range b {
		params = append(params, "@p"+strconv.Itoa(i))
		paramValues = append(paramValues, sql.Named("p"+strconv.Itoa(i), ppob.ID))
	}
	ppobLogQuery = strings.Replace(
		ppobLogQuery,
		"[params]",
		strings.Join(params, ","),
		1,
	)
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	paramValues = append(paramValues,
		sql.Named("NomorTransaksiSettlement", nomorTransaksiSettlement),
	)
	paramValues = append(paramValues,
		sql.Named("status", status),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		paramValues...,
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
func (sj *sqlPPOBRepository) SettlePPOBPLN(ctx context.Context, b []*models.PLNPrepaid, operator string, nomorTransaksiSettlement string, status int32) ([]*models.PLNPrepaid, error) {

	ppobLogQuery := `
		UPDATE ` + operator + ` 
		SET 
			TransStatus = @status
			,NomorTransaksiSettlement = @NomorTransaksiSettlement
			,Date_Update = GETDATE()
		WHERE
			ID IN([params])
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ppobLogQuery)),
		zap.String("obj", fmt.Sprintf("%v", b)),
	)
	paramValues := make([]interface{}, 0)
	params := make([]string, 0)
	for i, ppob := range b {
		params = append(params, "@p"+strconv.Itoa(i))
		paramValues = append(paramValues, sql.Named("p"+strconv.Itoa(i), ppob.ID))
	}
	ppobLogQuery = strings.Replace(
		ppobLogQuery,
		"[params]",
		strings.Join(params, ","),
		1,
	)
	paramValues = append(paramValues,
		sql.Named("NomorTransaksiSettlement", nomorTransaksiSettlement),
	)
	paramValues = append(paramValues,
		sql.Named("status", status),
	)
	_, err := sj.Conn.ExecContext(ctx, ppobLogQuery,
		paramValues...,
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return b, nil
}
func (sj *sqlPPOBRepository) GetByID(ctx context.Context, id string, operator string) (*models.PPOB, error) {
	var ppob models.PPOB
	query := `
	SELECT
		Cast(ID as varchar(36)) as ID,
		TransactionDate, 
		RequestAccountNo ,
		RequestPhoneNumber ,
		RequestAmount ,
		ResponsePhoneNumber ,
		ResponseAmount ,
		ResponseReference ,
		ResponseRawMessage ,
		AdminAmount ,
		PartnerAmount ,
		InvelliAmount ,
		StatusCode ,
		CompanyId ,
		MessagePrinter ,
		TransStatus ,
		User_Insert ,
		Date_Insert ,
		User_Update ,
		Date_Update ,
		User_Authorize ,
		Date_Authorize ,
		ISNULL(NomorTransaksi, ''),
		ISNULL(NomorTransaksiSettlement, '')
	from 
		` + operator + ` 
	where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&ppob.ID,
		&ppob.TransactionDate,
		&ppob.RequestAccountNo,
		&ppob.RequestPhoneNumber,
		&ppob.RequestAmount,
		&ppob.ResponsePhoneNumber,
		&ppob.ResponseAmount,
		&ppob.ResponseReference,
		&ppob.ResponseRawMessage,
		&ppob.AdminAmount,
		&ppob.PartnerAmount,
		&ppob.InvelliAmount,
		&ppob.StatusCode,
		&ppob.CompanyID,
		&ppob.MessagePrinter,
		&ppob.TransStatus,
		&ppob.UserInsert,
		&ppob.DateInsert,
		&ppob.UserUpdate,
		&ppob.DateUpdate,
		&ppob.UserAuthorize,
		&ppob.DateAuthorize,
		&ppob.NomorTransaksi,
		&ppob.NomorTransaksiSettlement,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", ppob)),
	)
	return &ppob, nil
}
func (sj *sqlPPOBRepository) GetByIDPLNPrepaid(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error) {
	var ppob models.PLNPrepaid
	query := `
	SELECT
		Cast(ID as varchar(36)) as ID
		,TransactionDate
		,NomorTransaksi
		,RequestAccountNo
		,RequestCustomerNumber
		,RequestAmount
		,ResponseMeterNo
		,ResponsePLNCustomerNo
		,ResponseCustomerName
		,ResponseTarif
		,ResponseAdminFee
		,ResponseTotalBayar
		,ResponseKodeToken
		,ResponseDate
		,ResponseKWH
		,ResponseRawMessage
		,AdminAmount
		,PartnerAmount
		,InvelliAmount
		,StatusCode
		,CompanyId
		,MessagePrinter
		,TransStatus
		,User_Insert
		,Date_Insert
		,User_Update
		,Date_Update
		,User_Authorize
		,Date_Authorize
		,ISNULL(NomorTransaksiSettlement, '')
	from 
		` + operator + ` 
	where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&ppob.ID,
		&ppob.TransactionDate,
		&ppob.NomorTransaksi,
		&ppob.RequestAccountNo,
		&ppob.RequestCustomerNumber,
		&ppob.RequestAmount,
		&ppob.ResponseMeterNo,
		&ppob.ResponsePLNCustomerNo,
		&ppob.ResponseCustomerName,
		&ppob.ResponseTarif,
		&ppob.ResponseAdminFee,
		&ppob.ResponseTotalBayar,
		&ppob.ResponseKodeToken,
		&ppob.ResponseDate,
		&ppob.ResponseKWH,
		&ppob.ResponseRawMessage,
		&ppob.AdminAmount,
		&ppob.PartnerAmount,
		&ppob.InvelliAmount,
		&ppob.StatusCode,
		&ppob.CompanyID,
		&ppob.MessagePrinter,
		&ppob.TransStatus,
		&ppob.UserInsert,
		&ppob.DateInsert,
		&ppob.UserUpdate,
		&ppob.DateUpdate,
		&ppob.UserAuthorize,
		&ppob.DateAuthorize,
		&ppob.NomorTransaksiSettlement,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", ppob)),
	)
	return &ppob, nil
}
func (sj *sqlPPOBRepository) GetByIDPLNPostpaid(ctx context.Context, id string, operator string) (*models.PLNPrepaid, error) {
	var ppob models.PLNPrepaid
	query := `
	SELECT
		Cast(ID as varchar(36)) as ID
		,TransactionDate
		,NomorTransaksi
		,RequestAccountNo
		,RequestCustomerNumber
		,RequestAmount
		,ResponsePLNCustomerNo
		,ResponseCustomerName
		,ResponseBillPeriod
		,ResponseNumberOfBill
		,ResponseAdminFee
		,ResponseUsage
		,ResponseTrxType
		,ResponseBillAmount
		,ResponseRawMessage
		,AdminAmount
		,PartnerAmount
		,InvelliAmount
		,StatusCode
		,CompanyId
		,MessagePrinter
		,TransStatus
		,User_Insert
		,Date_Insert
		,User_Update
		,Date_Update
		,User_Authorize
		,Date_Authorize
		,ISNULL(NomorTransaksiSettlement, '')
		,ResponseReference
	from 
		` + operator + ` 
	where 
		ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", id)).Scan(
		&ppob.ID,
		&ppob.TransactionDate,
		&ppob.NomorTransaksi,
		&ppob.RequestAccountNo,
		&ppob.RequestCustomerNumber,
		&ppob.RequestAmount,
		&ppob.ResponsePLNCustomerNo,
		&ppob.ResponseCustomerName,
		&ppob.ResponseBillPeriod,
		&ppob.ResponseNumberOfBill,
		&ppob.ResponseAdminFee,
		&ppob.ResponseUsage,
		&ppob.ResponseTrxType,
		&ppob.ResponseBillAmount,
		&ppob.ResponseRawMessage,
		&ppob.AdminAmount,
		&ppob.PartnerAmount,
		&ppob.InvelliAmount,
		&ppob.StatusCode,
		&ppob.CompanyID,
		&ppob.MessagePrinter,
		&ppob.TransStatus,
		&ppob.UserInsert,
		&ppob.DateInsert,
		&ppob.UserUpdate,
		&ppob.DateUpdate,
		&ppob.UserAuthorize,
		&ppob.DateAuthorize,
		&ppob.NomorTransaksiSettlement,
		&ppob.ResponseReference,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", ppob)),
	)
	return &ppob, nil
}
func (sj *sqlPPOBRepository) Fetch(ctx context.Context, status *int32) ([]*models.PPOB, error) {
	filterStatus := ``
	filters := make([]string, 0)
	filterTable := ``
	if status != nil {
		filters = append(filters, `TransStatus = @status`)
	}
	if len(filters) > 0 {
		filterStatus = ` WHERE ` + strings.Join(filters, " AND ")
	}
	query := `
        SELECT 
			ID,
			MemberName, 
			TransactionNo, 
			ResponseReference, 
			TransactionDate
        FROM (
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID, 
				'SIMPATI' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				telkomselsimpatiprepaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code 
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = t.RequestAmount and bp.Operator = 'SIMPATI'
			` + filterTable + `
			UNION ALL
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID, 
				'XL' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				xlbebasprepaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code 
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = t.RequestAmount and bp.Operator = 'XL'
			` + filterTable + `
			UNION ALL 
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID, 
				'INDOSAT' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				indosatim3prepaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code                 
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = t.RequestAmount and bp.Operator = 'INDOSAT'
			` + filterTable + `
			UNION ALL 
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID,
				'THREE' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				threeprepaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code 
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = t.RequestAmount and bp.Operator = 'THREE'
			` + filterTable + `
			UNION ALL 
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID,
				'PLNPREPAID' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				plnprepaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code 
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = (t.RequestAmount - t.AdminAmount)
					and bp.Operator = 'PLNPREPAID'

			` + filterTable + `
			UNION ALL 
			SELECT 
				Cast(t.id AS VARCHAR(36)) AS ID,
				'PLNPOSTPAID' AS MemberName, 
				ISNULL(NomorTransaksi, '') AS TransactionNo, 
				ISNULL(ResponseReference, '') as ResponseReference,
				TransactionDate,
				TransStatus
			FROM   
				plnpostpaidlog t 
				INNER JOIN account acc ON t.requestaccountno = acc.code
				INNER JOIN MasterTransaksiBillPayment bp ON bp.PurchaseAmount = t.RequestAmount and bp.Operator = 'PLNPOSTPAID'
			` + filterTable + `
        ) Notification ` + filterStatus + `
	`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("status", status),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.PPOB, 0)
	for rows.Next() {
		j := new(models.PPOB)
		err = rows.Scan(
			&j.ID,
			&j.Operator,
			&j.NomorTransaksi,
			&j.ResponseReference,
			&j.TransactionDate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}

	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
