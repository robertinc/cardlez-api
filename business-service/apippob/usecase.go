package apippob

import (
	"context"
)

type ApiPPOBUsecase interface {
	UpdateApiPPOBStatus(ctx context.Context, billPaymentID string, recordStatus int) (bool, error)
	//InsertJournalSettlement(ctx context.Context, journalCode string, b *models.ApiPPOBBillPayment, c *modelsext.MasterTransaksiBillPaymentResponse) error

}
