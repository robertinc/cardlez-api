package apippob

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type ApiPPOBRepository interface {
	UpdateBillPayment(ctx context.Context, apippobbillPayment *models.ApiPPOBBillPayment) (bool, error)
	UpdateBillPaymentNew(ctx context.Context, apippobbillPayment *models.ApiPPOBBillPayment) (bool, error)
	UpdateBillPaymentRecordStatusBySessionID(ctx context.Context, sessionID string, data *models.ApiPPOBBillPayment) (bool, error)
	UpdateBillPaymentStatus(ctx context.Context, data *models.ApiPPOBBillPayment) (bool, error)
	Store(ctx context.Context, apippobbillPayment *models.ApiPPOBBillPayment) (bool, error)
	StoreByDate(ctx context.Context, apippobbillPayment *models.ApiPPOBBillPayment) (bool, error)

	GetBillPaymentBySessionID(ctx context.Context, sessionID string) (bool, error)
	GetBillPaymentBySessionIDPayment(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error)
	GetBillPaymentBySessionIDInquiry(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error)
	GetBillPaymentByID(ctx context.Context, billPaymentID string) (*models.ApiPPOBBillPayment, error)
	GetBillPaymentBySessionIDInquiryOrPayment(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error)
}
