package usecase

import (
	"context"
	"encoding/json"
	"time"

	apippob "gitlab.com/robertinc/cardlez-api/business-service/apippob"
	"gitlab.com/robertinc/cardlez-api/business-service/member"
	"gitlab.com/robertinc/cardlez-api/models"
	//"github.com/shopspring/decimal"
	//"github.com/leekchan/accounting"
)

type ApiPPOBUseCase struct {
	MemberRepository  member.MemberRepository
	ApiPPOBRepository apippob.ApiPPOBRepository
	contextTimeout    time.Duration
}

func NewApiPPOBUsecaseV2(memberRepo member.MemberRepository, apippobrepo apippob.ApiPPOBRepository) apippob.ApiPPOBUsecase {
	return &ApiPPOBUseCase{
		MemberRepository:  memberRepo,
		ApiPPOBRepository: apippobrepo,
		contextTimeout:    time.Second * time.Duration(models.Timeout()),
	}
}

func CreateDataSubmit(a DataSubmitBuilder) string {
	// for creating data submit based on transaction code
	s := []rune(a.Code)
	isPLN := false

	if string(s[0:3]) == "PLN" {
		isPLN = true
	} else {
		isPLN = false
	}

	var data = ""
	if a.TC == "INQ" {
		var out []byte
		var err error

		if isPLN {
			o := &PLNDataSubmit{a.BillNumber, a.ReffNo}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		} else {
			o := &InqDataSubmit{a.PhoneNumber, a.ReffNo}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		}

		data = string(out)
	} else if a.TC == "PAY" && a.InqReferenceNumber != "" {
		// Pay with inquiry mandatory
		var out []byte
		var err error

		if isPLN {
			o := &PayPLNDataSubmit{a.BillNumber, a.ReffNo, a.Amount}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		} else {
			o := &PayDataSubmit{a.PhoneNumber, a.InqReferenceNumber, a.ReffNo}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		}

		data = string(out)
	} else {
		// Pay only
		var out []byte
		var err error

		if isPLN {
			o := &PayPLNDataSubmit{a.BillNumber, a.ReffNo, a.Amount}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		} else {
			o := &PayOnlyDataSubmit{a.PhoneNumber, a.ReffNo}
			out, err = json.Marshal(o)
			if err != nil {
				return ""
			}
		}

		data = string(out)
	}

	return data
}

type DataSubmitBuilder struct {
	TC                 string
	TransactionCode    string
	Code               string
	PhoneNumber        string
	BillNumber         string
	ReffNo             string
	InqReferenceNumber string
	Amount             float64
}

func (a *ApiPPOBUseCase) UpdateApiPPOBStatus(ctx context.Context, billPaymentID string, recordStatus int) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	aB, err := a.ApiPPOBRepository.GetBillPaymentByID(ctx, billPaymentID)
	if err != nil {
		return false, err
	}

	_, errUpdate := a.ApiPPOBRepository.UpdateBillPaymentStatus(ctx, aB)
	if errUpdate != nil {
		return false, errUpdate
	}

	return true, nil
}

// func (a *ApiPPOBUseCase) InsertJournalSettlement(ctx context.Context, journalCode string, b *models.ApiPPOBBillPayment, c *modelsext.MasterTransaksiBillPaymentResponse) error {
// 	journalID := guid.New().StringUpper()
// 	//notes := ""
// 	journalRepo := ctx.Value("JournalRepository").(cJournal.JournalRepository)
// 	journalMappingRepo := ctx.Value("JournalMappingRepository").(cJournal.JournalMappingRepository)
// 	journalmapping, err := journalMappingRepo.GetByCode(ctx, journalCode)
// 	if err != nil {
// 		return err
// 	}
// 	totalAmount := c.SellPrice
// 	totalFeeAmount := c.PartnerAmount
// 	totalInvelliAmount := c.InvelliAmount
// 	reffNo := b.Code

// 	journal := models.Journal{
// 		ID:               journalID,
// 		TransactionID:    service.NewEmptyGuid(),
// 		PostingDate:      time.Now().Format("2006-01-02 15:04:05"),
// 		Description:      service.NewNullString("Settlement PPOB " + c.Operator),
// 		Code:             service.NewNullString(reffNo),
// 		JournalMappingID: journalmapping.ID,
// 	}

// 	//Debit KWS
// 	journalDetailID := guid.New().StringUpper()
// 	journalDetailDebit := models.JournalDetail{
// 		ID:              journalDetailID,
// 		JournalID:       journal.ID,
// 		COAID:           "FA7B042B-3365-4F18-9835-74D956D5968F",
// 		Debit:           totalAmount,
// 		Credit:          0,
// 		Notes:           "Kewajiban Segera - Titipan Transaksi Mobile",
// 		ReferenceNumber: "",
// 	}

// 	//Credit KWS Prismalink
// 	journalDetailCreditKWSID := guid.New().StringUpper()
// 	journalDetailCreditKWS := models.JournalDetail{
// 		ID:              journalDetailCreditKWSID,
// 		JournalID:       journal.ID,
// 		COAID:           "874CB58C-2123-48FA-8C74-4FC4C219D440",
// 		Debit:           0,
// 		Credit:          totalAmount - totalFeeAmount - totalInvelliAmount,
// 		Notes:           "Kewajiban Segera - Prismalink",
// 		ReferenceNumber: "",
// 	}

// 	//Credit Pendapatan Invelli
// 	journalDetailCreditInvelliID := guid.New().StringUpper()
// 	journalDetailCreditInvelli := models.JournalDetail{
// 		ID:              journalDetailCreditInvelliID,
// 		JournalID:       journal.ID,
// 		COAID:           "294F21F6-94A0-4DF4-A208-3FFA76A12D1B",
// 		Debit:           0,
// 		Credit:          totalInvelliAmount,
// 		Notes:           "Pendapatan Invelli",
// 		ReferenceNumber: "",
// 	}

// 	//Credit Bagi Hasil ISP
// 	journalDetailCreditISPID := guid.New().StringUpper()
// 	journalDetailCreditISP := models.JournalDetail{
// 		ID:              journalDetailCreditISPID,
// 		JournalID:       journal.ID,
// 		COAID:           "B6113005-CAC3-4152-87D3-13761757C435",
// 		Debit:           0,
// 		Credit:          totalFeeAmount,
// 		Notes:           "Bagi Hasil ISP",
// 		ReferenceNumber: "",
// 	}
// 	journal.JournalDetails = append(journal.JournalDetails, journalDetailDebit)
// 	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditKWS)
// 	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditInvelli)
// 	journal.JournalDetails = append(journal.JournalDetails, journalDetailCreditISP)

// 	JournalUsecase := usecase.NewJournalUsecaseV2(journalRepo)
// 	_, err = JournalUsecase.Store(ctx, &journal)
// 	if err != nil {
// 		return err
// 	}
// 	return nil

// }

type InqDataSubmit struct {
	PhoneNumber         string
	UserReferenceNumber string
}

type PayDataSubmit struct {
	PhoneNumber         string
	InqReferenceNumber  string
	UserReferenceNumber string
}
type PayOnlyDataSubmit struct {
	PhoneNumber         string
	UserReferenceNumber string
}

type PLNDataSubmit struct {
	BillNumber          string
	UserReferenceNumber string
}

type PayPLNDataSubmit struct {
	BillNumber          string
	UserReferenceNumber string
	Amount              float64
}
