package repository

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	//	"strconv"

	ApiPPOB "gitlab.com/robertinc/cardlez-api/business-service/apippob"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlApiPPOBRepository struct {
	Conn *sql.DB
}

func NewSqlApiPPOBRepositoryV2(Conn *sql.DB) ApiPPOB.ApiPPOBRepository {
	conn := Conn
	return &sqlApiPPOBRepository{conn}
}
func (sj *sqlApiPPOBRepository) Store(ctx context.Context, a *models.ApiPPOBBillPayment) (bool, error) {
	//a.ID = guid.New().StringUpper()
	ApiPPOBBillPaymentQuery := `INSERT INTO BillPayment (
		ID
		,Code
		,DebitAccountID
		,Amount
		,FeeAmount
		,DebitDate
		,CustomerNo
		,CustomerName
		,CoreReffNo
		,BillerCode
		,SessionIDInquiry
		,SessionIDPayment
		,InqReferenceNumber
		,TransactionCode
		,InquiryTransactionCode
		,PLNCustomerNo
		,PLNCustomerName
		,BillPeriod
		,Usage
		,TrxType
		,MeterNo
		,PaymentDate
		,KWH
		,Message
		,RecordStatus
		,UserInsert
		,DateInsert
		,UserUpdate
		,DateUpdate
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17,@p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28 )`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ApiPPOBBillPaymentQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ApiPPOBBillPaymentQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.DebitDate),
		sql.Named("p6", a.CustomerNo),
		sql.Named("p7", a.CustomerName),
		sql.Named("p8", a.CoreReffNo),
		sql.Named("p9", a.BillerCode),
		sql.Named("p10", a.SessionIDInquiry),
		sql.Named("p11", a.SessionIDPayment),
		sql.Named("p12", a.InqReferenceNumber),
		sql.Named("p13", a.TransactionCode),
		sql.Named("p14", a.InquiryTransactionCode),

		sql.Named("p15", a.PLNCustomerNo),
		sql.Named("p16", a.PLNCustomerName),
		sql.Named("p17", a.BillPeriod),
		sql.Named("p18", a.Usage),
		sql.Named("p19", a.TrxType),
		sql.Named("p20", a.MeterNo),
		sql.Named("p21", a.PaymentDate),
		sql.Named("p22", a.KWH),
		sql.Named("p23", a.Message),
		sql.Named("p24", a.RecordStatus),
		sql.Named("p25", a.UserInsert),
		sql.Named("p26", a.DateInsert),
		sql.Named("p27", a.UserUpdate),
		sql.Named("p28", a.DateUpdate),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}

func (sj *sqlApiPPOBRepository) StoreByDate(ctx context.Context, a *models.ApiPPOBBillPayment) (bool, error) {
	//a.ID = guid.New().StringUpper()
	today := time.Now().Format("2006-01-02 15:04:05")
	ApiPPOBBillPaymentQuery := `INSERT INTO BillPayment (
		ID
		,Code
		,DebitAccountID
		,Amount
		,FeeAmount
		,DebitDate
		,CustomerNo
		,CustomerName
		,CoreReffNo
		,BillerCode
		,SessionIDInquiry
		,SessionIDPayment
		,InqReferenceNumber
		,TransactionCode
		,InquiryTransactionCode
		,PLNCustomerNo
		,PLNCustomerName
		,BillPeriod
		,Usage
		,TrxType
		,MeterNo
		,PaymentDate
		,KWH
		,Message
		,RecordStatus
		,UserInsert
		,DateInsert
		,UserUpdate
		,DateUpdate
		,DateAuthor
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17,@p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28, @p29 )`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", ApiPPOBBillPaymentQuery)),
		zap.String("obj", fmt.Sprintf("%v", a)),
	)
	_, err := sj.Conn.ExecContext(ctx, ApiPPOBBillPaymentQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.Code),
		sql.Named("p2", a.DebitAccountID),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.DebitDate),
		sql.Named("p6", a.CustomerNo),
		sql.Named("p7", a.CustomerName),
		sql.Named("p8", a.CoreReffNo),
		sql.Named("p9", a.BillerCode),
		sql.Named("p10", a.SessionIDInquiry),
		sql.Named("p11", a.SessionIDPayment),
		sql.Named("p12", a.InqReferenceNumber),
		sql.Named("p13", a.TransactionCode),
		sql.Named("p14", a.InquiryTransactionCode),

		sql.Named("p15", a.PLNCustomerNo),
		sql.Named("p16", a.PLNCustomerName),
		sql.Named("p17", a.BillPeriod),
		sql.Named("p18", a.Usage),
		sql.Named("p19", a.TrxType),
		sql.Named("p20", a.MeterNo),
		sql.Named("p21", a.PaymentDate),
		sql.Named("p22", a.KWH),
		sql.Named("p23", a.Message),
		sql.Named("p24", a.RecordStatus),
		sql.Named("p25", a.UserInsert),
		sql.Named("p26", a.DateInsert),
		sql.Named("p27", a.UserUpdate),
		sql.Named("p28", a.DateUpdate),
		sql.Named("p29", today),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, nil
}
func (sj *sqlApiPPOBRepository) UpdateBillPayment(ctx context.Context, a *models.ApiPPOBBillPayment) (bool, error) {

	updateQuery := `
	Update 
		BillPayment 
	Set 
		RecordStatus = @p1, 
		InqReferenceNumber = @p2, 
		Amount = @p3, 
		FeeAmount = @p4,
		PLNCustomerNo = @p5,
		PLNCustomerName = @p6,
		BillPeriod = @p7,
		Usage = @p8,
		TrxType = @p9,
		MeterNo = @p10,
		PaymentDate = @p11,
		KWH = @p12,
		PlnToken = @p13,
		Message = @p14,
		UserUpdate =@p15,
		DateUpdate = @p16,
		SessionIDPayment = @p17
	Where ID = @p0 `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", a.ID)),
		zap.String("InqReferenceNumber", fmt.Sprintf("%v", a.InqReferenceNumber)),
	)

	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.RecordStatus),
		sql.Named("p2", a.InqReferenceNumber),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.PLNCustomerNo),
		sql.Named("p6", a.PLNCustomerName),
		sql.Named("p7", a.BillPeriod),
		sql.Named("p8", a.Usage),
		sql.Named("p9", a.TrxType),
		sql.Named("p10", a.MeterNo),
		sql.Named("p11", a.PaymentDate),
		sql.Named("p12", a.KWH),
		sql.Named("p13", a.PlnToken.String),
		sql.Named("p14", a.Message),
		sql.Named("p15", a.UserUpdate),
		sql.Named("p16", a.DateUpdate),
		sql.Named("p17", a.SessionIDPayment),
	)

	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlApiPPOBRepository) UpdateBillPaymentNew(ctx context.Context, a *models.ApiPPOBBillPayment) (bool, error) {

	updateQuery := `
	Update 
		BillPayment 
	Set 
		RecordStatus = @p1, 
		InqReferenceNumber = @p2, 
		Amount = @p3, 
		FeeAmount = @p4,
		PLNCustomerNo = @p5,
		PLNCustomerName = @p6,
		BillPeriod = @p7,
		Usage = @p8,
		TrxType = @p9,
		MeterNo = @p10,
		PaymentDate = @p11,
		KWH = @p12,
		PlnToken = @p13,
		Message = @p14,
		UserUpdate =@p15,
		DateUpdate = @p16,
		SessionIDPayment = @p17,
		TransactionCode = @p18
	Where ID = @p0 `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", a.ID)),
		zap.String("InqReferenceNumber", fmt.Sprintf("%v", a.InqReferenceNumber)),
	)

	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.RecordStatus),
		sql.Named("p2", a.InqReferenceNumber),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.PLNCustomerNo),
		sql.Named("p6", a.PLNCustomerName),
		sql.Named("p7", a.BillPeriod),
		sql.Named("p8", a.Usage),
		sql.Named("p9", a.TrxType),
		sql.Named("p10", a.MeterNo),
		sql.Named("p11", a.PaymentDate),
		sql.Named("p12", a.KWH),
		sql.Named("p13", a.PlnToken.String),
		sql.Named("p14", a.Message),
		sql.Named("p15", a.UserUpdate),
		sql.Named("p16", a.DateUpdate),
		sql.Named("p17", a.SessionIDPayment),
		sql.Named("p18", a.TransactionCode),
	)

	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlApiPPOBRepository) UpdateBillPaymentStatus(ctx context.Context, a *models.ApiPPOBBillPayment) (bool, error) {

	updateQuery := `
	Update 
		BillPayment 
	Set 
		RecordStatus = @p1, 
		DateUpdate = GETDATE()
	Where ID = @p0 `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", a.ID)),
	)

	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.RecordStatus),
	)

	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}

func (sj *sqlApiPPOBRepository) UpdateBillPaymentRecordStatusBySessionID(ctx context.Context, sessionID string, a *models.ApiPPOBBillPayment) (bool, error) {

	updateQuery := `
	Update 
		BillPayment 
	Set 
		RecordStatus = @p1, 
		InqReferenceNumber = @p2, 
		Amount = @p3, 
		FeeAmount = @p4,
		PLNCustomerNo = @p5,
		PLNCustomerName = @p6,
		BillPeriod = @p7,
		Usage = @p8,
		TrxType = @p9,
		MeterNo = @p10,
		PaymentDate = @p11,
		KWH = @p12,
		PlnToken = @p13,
		Message = @p14
	Where ID = @p0 `

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", updateQuery)),
		zap.String("ID", fmt.Sprintf("%v", sessionID)),
	)
	_, err := sj.Conn.ExecContext(ctx, updateQuery,
		sql.Named("p0", a.ID),
		sql.Named("p1", a.RecordStatus),
		sql.Named("p2", a.InqReferenceNumber),
		sql.Named("p3", a.Amount),
		sql.Named("p4", a.FeeAmount),
		sql.Named("p5", a.PLNCustomerNo),
		sql.Named("p6", a.PLNCustomerName),
		sql.Named("p7", a.BillPeriod),
		sql.Named("p8", a.Usage),
		sql.Named("p9", a.TrxType),
		sql.Named("p10", a.MeterNo),
		sql.Named("p11", a.PaymentDate),
		sql.Named("p12", a.KWH),
		sql.Named("p13", a.PlnToken.String),
		sql.Named("p14", a.Message),
	)
	if err != nil {
		logger.Error("UpdateBillPaymentRecordStatusBySessionID Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("UpdateBillPaymentRecordStatusBySessionID Success")
	return true, nil
}

func (sj *sqlApiPPOBRepository) GetBillPaymentBySessionID(ctx context.Context, sessionID string) (bool, error) {
	logger := service.Logger(ctx)
	//var queryResult bool
	var Result int

	logger.Info("Query BillPayment by Session ID")
	err := sj.Conn.QueryRow(`
	SELECT   
		count(1) as Result
	FROM dbo.BillPayment m where SessionIDInquiry = @sessionID `, sql.Named("sessionID", sessionID)).Scan(
		&Result,
	)
	if err != nil {
		logger.Error("Query BillPayment by SessionID Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}

	if Result > 0 {
		return false, nil
	}

	return true, nil
}

func (sj *sqlApiPPOBRepository) GetBillPaymentBySessionIDPayment(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error) {
	logger := service.Logger(ctx)
	//var queryResult bool
	var m models.ApiPPOBBillPayment

	logger.Info("Query BillPayment by Session ID")
	err := sj.Conn.QueryRow(`
	SELECT   
		cast(ID as varchar(36)) as ID,
		Code,
		cast(DebitAccountID as varchar(36)) as DebitAccountID,
		Amount,
		FeeAmount,
		DebitDate,
		CustomerNo,
		CustomerName,
		CoreReffNo,
		BillerCode,
		PlnToken,
		cast(SessionIDInquiry as varchar(36)) as SessionIDInquiry,
		cast(SessionIDPayment as varchar(36)) as SessionIDPayment,
		InqReferenceNumber,
		TransactionCode,
		InquiryTransactionCode,
		PLNCustomerNo,
		PLNCustomerName,
		BillPeriod,
		Usage,
		TrxType,
		MeterNo,
		PaymentDate,
		KWH,
		RecordStatus
	FROM dbo.BillPayment m where SessionIDPayment = @sessionID `,
		sql.Named("sessionID", sessionID)).Scan(
		&m.ID,
		&m.Code,
		&m.DebitAccountID,
		&m.Amount,
		&m.FeeAmount,
		&m.DebitDate,
		&m.CustomerNo,
		&m.CustomerName,
		&m.CoreReffNo,
		&m.BillerCode,
		&m.PlnToken,
		&m.SessionIDInquiry,
		&m.SessionIDPayment,
		&m.InqReferenceNumber,
		&m.TransactionCode,
		&m.InquiryTransactionCode,
		&m.PLNCustomerNo,
		&m.PLNCustomerName,
		&m.BillPeriod,
		&m.Usage,
		&m.TrxType,
		&m.MeterNo,
		&m.PaymentDate,
		&m.KWH,
		&m.RecordStatus,
	)
	if err != nil {
		logger.Error("Query BillPayment by SessionID Error",
			zap.String("error", err.Error()),
		)
		return &models.ApiPPOBBillPayment{}, err
	}
	return &m, nil
}
func (sj *sqlApiPPOBRepository) GetBillPaymentBySessionIDInquiryOrPayment(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error) {
	logger := service.Logger(ctx)
	//var queryResult bool
	var m models.ApiPPOBBillPayment

	logger.Info("Query BillPayment by Session ID")
	err := sj.Conn.QueryRow(`
	SELECT   
		cast(ID as varchar(36)) as ID,
		Code,
		cast(DebitAccountID as varchar(36)) as DebitAccountID,
		Amount,
		FeeAmount,
		DebitDate,
		CustomerNo,
		CustomerName,
		CoreReffNo,
		BillerCode,
		PlnToken,
		cast(SessionIDInquiry as varchar(36)) as SessionIDInquiry,
		cast(SessionIDPayment as varchar(36)) as SessionIDPayment,
		InqReferenceNumber,
		TransactionCode,
		InquiryTransactionCode,
		PLNCustomerNo,
		PLNCustomerName,
		BillPeriod,
		Usage,
		TrxType,
		MeterNo,
		PaymentDate,
		KWH,
		RecordStatus
	FROM dbo.BillPayment m where SessionIDPayment = @sessionID or SessionIDInquiry = @sessionID `,
		sql.Named("sessionID", sessionID)).Scan(
		&m.ID,
		&m.Code,
		&m.DebitAccountID,
		&m.Amount,
		&m.FeeAmount,
		&m.DebitDate,
		&m.CustomerNo,
		&m.CustomerName,
		&m.CoreReffNo,
		&m.BillerCode,
		&m.PlnToken,
		&m.SessionIDInquiry,
		&m.SessionIDPayment,
		&m.InqReferenceNumber,
		&m.TransactionCode,
		&m.InquiryTransactionCode,
		&m.PLNCustomerNo,
		&m.PLNCustomerName,
		&m.BillPeriod,
		&m.Usage,
		&m.TrxType,
		&m.MeterNo,
		&m.PaymentDate,
		&m.KWH,
		&m.RecordStatus,
	)
	if err != nil {
		logger.Error("Query BillPayment by SessionID Error",
			zap.String("error", err.Error()),
		)
		return &models.ApiPPOBBillPayment{}, err
	}
	return &m, nil
}

func (sj *sqlApiPPOBRepository) GetBillPaymentBySessionIDInquiry(ctx context.Context, sessionID string) (*models.ApiPPOBBillPayment, error) {
	logger := service.Logger(ctx)
	//var queryResult bool
	var m models.ApiPPOBBillPayment

	logger.Info("Query BillPayment by Session ID")
	err := sj.Conn.QueryRow(`
	SELECT TOP 1   
		cast(ID as varchar(36)) as ID,
		Code,
		cast(DebitAccountID as varchar(36)) as DebitAccountID,
		Amount,
		FeeAmount,
		DebitDate,
		CustomerNo,
		CustomerName,
		CoreReffNo,
		BillerCode,
		PlnToken,
		cast(SessionIDInquiry as varchar(36)) as SessionIDInquiry,
		cast(SessionIDPayment as varchar(36)) as SessionIDPayment,
		InqReferenceNumber,
		TransactionCode,
		InquiryTransactionCode,
		PLNCustomerNo,
		PLNCustomerName,
		BillPeriod,
		Usage,
		TrxType,
		MeterNo,
		PaymentDate,
		KWH,
		RecordStatus
	FROM dbo.BillPayment m where SessionIDInquiry = @sessionID ORDER BY DateInsert ASC`,
		sql.Named("sessionID", sessionID)).Scan(
		&m.ID,
		&m.Code,
		&m.DebitAccountID,
		&m.Amount,
		&m.FeeAmount,
		&m.DebitDate,
		&m.CustomerNo,
		&m.CustomerName,
		&m.CoreReffNo,
		&m.BillerCode,
		&m.PlnToken,
		&m.SessionIDInquiry,
		&m.SessionIDPayment,
		&m.InqReferenceNumber,
		&m.TransactionCode,
		&m.InquiryTransactionCode,
		&m.PLNCustomerNo,
		&m.PLNCustomerName,
		&m.BillPeriod,
		&m.Usage,
		&m.TrxType,
		&m.MeterNo,
		&m.PaymentDate,
		&m.KWH,
		&m.RecordStatus,
	)
	if err != nil {
		logger.Error("Query BillPayment by SessionID Error",
			zap.String("error", err.Error()),
		)
		return &models.ApiPPOBBillPayment{}, err
	}
	return &m, nil
}

func (sj *sqlApiPPOBRepository) GetBillPaymentByID(ctx context.Context, billPaymentID string) (*models.ApiPPOBBillPayment, error) {
	logger := service.Logger(ctx)
	//var queryResult bool
	var m models.ApiPPOBBillPayment

	logger.Info("Query BillPayment by Session ID")
	err := sj.Conn.QueryRow(`
	SELECT   
		cast(ID as varchar(36)) as ID,
		Code,
		cast(DebitAccountID as varchar(36)) as DebitAccountID,
		Amount,
		FeeAmount,
		DebitDate,
		CustomerNo,
		CustomerName,
		CoreReffNo,
		BillerCode,
		PlnToken,
		cast(SessionIDInquiry as varchar(36)) as SessionIDInquiry,
		cast(SessionIDPayment as varchar(36)) as SessionIDPayment,
		InqReferenceNumber,
		TransactionCode,
		InquiryTransactionCode,
		PLNCustomerNo,
		PLNCustomerName,
		BillPeriod,
		Usage,
		TrxType,
		MeterNo,
		COALESCE(PaymentDate,'') as PaymentDate,
		KWH,
		RecordStatus
	FROM dbo.BillPayment m where ID = @billPaymentID `,
		sql.Named("billPaymentID", billPaymentID)).Scan(
		&m.ID,
		&m.Code,
		&m.DebitAccountID,
		&m.Amount,
		&m.FeeAmount,
		&m.DebitDate,
		&m.CustomerNo,
		&m.CustomerName,
		&m.CoreReffNo,
		&m.BillerCode,
		&m.PlnToken,
		&m.SessionIDInquiry,
		&m.SessionIDPayment,
		&m.InqReferenceNumber,
		&m.TransactionCode,
		&m.InquiryTransactionCode,
		&m.PLNCustomerNo,
		&m.PLNCustomerName,
		&m.BillPeriod,
		&m.Usage,
		&m.TrxType,
		&m.MeterNo,
		&m.PaymentDate,
		&m.KWH,
		&m.RecordStatus,
	)
	if err != nil {
		logger.Error("Query BillPayment by ID Error",
			zap.String("error", err.Error()),
		)
		return &models.ApiPPOBBillPayment{}, err
	}
	return &m, nil
}
