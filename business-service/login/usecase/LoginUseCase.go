package usecase

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/login"
	"gitlab.com/robertinc/cardlez-api/business-service/login/repository"
	"gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"golang.org/x/crypto/bcrypt"
)

type LoginUsecase struct {
	LoginRepository login.LoginRepository
	contextTimeout  time.Duration
}

func NewLoginUsecase() login.LoginUsecase {
	return &LoginUsecase{
		LoginRepository: repository.NewSqlLoginRepository(),
		contextTimeout:  time.Second * time.Duration(models.Timeout()),
	}
}

func NewLoginUsecaseV2(loginRepo login.LoginRepository) login.LoginUsecase {
	return &LoginUsecase{
		LoginRepository: loginRepo,
		contextTimeout:  time.Second * time.Duration(models.Timeout()),
	}
}

func (a *LoginUsecase) AdminLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin := a.LoginRepository.AdminLogin(ctx, userEmail, password)

	err := bcrypt.CompareHashAndPassword([]byte(listLogin.UserPass), []byte(password))
	if err != nil {
		return nil
	}
	return listLogin
}
func (a *LoginUsecase) SystemLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin := a.LoginRepository.SystemLogin(ctx, userEmail, password)

	err := bcrypt.CompareHashAndPassword([]byte(listLogin.UserPass), []byte(password))
	if err != nil {
		return nil
	}
	return listLogin
}

func (a *LoginUsecase) MemberLoginBase(ctx context.Context, username string, password string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin, err := a.LoginRepository.MemberLogin(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("Maaf Username dan Password yang anda masukan tidak valid / terdaftar.")
	}

	err = bcrypt.CompareHashAndPassword([]byte(listLogin.UserPass), []byte(password))
	if err != nil {
		return nil, fmt.Errorf("Maaf Username dan Password yang anda masukan tidak valid / terdaftar.")
	}
	if listLogin.RecordStatus == 1 || listLogin.RecordStatus == 2 || listLogin.RecordStatus == 6 || listLogin.RecordStatus == 8 || listLogin.RecordStatus == 10 || listLogin.RecordStatus == 11 || listLogin.RecordStatus == 12 {
		return listLogin, err
	} else if listLogin.RecordStatus == 4 {
		return nil, fmt.Errorf("Maaf akun anda di blokir, harap hubungi Customer Service")
	}
	return nil, fmt.Errorf("Maaf akun anda belum aktif, harap hubungi Customer Service")
}

func (a *LoginUsecase) MemberLogin(ctx context.Context, username string, password string) (*models.CompanyUser, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin, err := a.LoginRepository.MemberLogin(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("Maaf Username dan Password yang anda masukan tidak valid / terdaftar.")
	}

	err = bcrypt.CompareHashAndPassword([]byte(listLogin.UserPass), []byte(password))
	if err != nil {
		return nil, fmt.Errorf("Maaf Username dan Password yang anda masukan tidak valid / terdaftar.")
	}
	if listLogin.RecordStatus == 2 {
		return listLogin, err
	} else if listLogin.RecordStatus == 4 {
		return nil, fmt.Errorf("Maaf akun anda di blokir, harap hubungi Customer Service")
	}
	return nil, fmt.Errorf("Maaf akun anda belum aktif, harap hubungi Customer Service")
}

func (a *LoginUsecase) GetMemberByPhone(ctx context.Context, handphone string) (*models.Member, error) {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin, err := a.LoginRepository.GetMemberByPhone(ctx, handphone)

	return listLogin, err
}

func (a *LoginUsecase) AdminFindByEmail(ctx context.Context, userEmail string) *models.CompanyUser {

	ctx, cancel := context.WithTimeout(ctx, a.contextTimeout)
	defer cancel()

	listLogin := a.LoginRepository.AdminFindByEmail(ctx, userEmail)

	return listLogin
}
func (u *LoginUsecase) ComparePassword(ctx context.Context, userCredentials *models.CompanyUser) (*models.CompanyUser, error) {
	user := u.AdminFindByEmail(ctx, userCredentials.UserEmail)
	if user == nil {
		return nil, errors.New(service.UnauthorizedAccess)
	}
	if result := user.ComparePassword(userCredentials.UserPass); !result {
		return nil, errors.New(service.UnauthorizedAccess)
	}
	return user, nil
}

func (u *LoginUsecase) DoAuditLog(ctx context.Context, log *models.AuditLog) (bool, error) {

	auditLog, err := u.LoginRepository.DoAuditLog(ctx, log)
	if err != nil {
		return false, err
	}
	return auditLog, err
}

func (u *LoginUsecase) GetAuditLog(c context.Context, startDate *string, endDate *string) ([]*models.AuditLog, error) {

	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	listAuditLog, err := u.LoginRepository.GetAuditLog(ctx, startDate, endDate)
	if err != nil {
		return nil, err
	}
	return listAuditLog, nil
}

func (u *LoginUsecase) GetChangeDeviceLog(c context.Context, memberId *string, modulName *string, date *string) (*models.AuditLog, error) {

	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	changeDeviceLog, err := u.LoginRepository.GetChangeDeviceLog(ctx, memberId, modulName, date)
	if err != nil {
		return nil, err
	}
	return changeDeviceLog, nil
}
