package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/login"
	models "gitlab.com/robertinc/cardlez-api/models"
)

type sqlLoginRepository struct {
	Conn *sql.DB
}

func NewSqlLoginRepository() login.LoginRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlLoginRepository{conn}
}
func NewSqlLoginRepositoryV2(Conn *sql.DB) login.LoginRepository {
	conn := Conn
	return &sqlLoginRepository{conn}
}
func (sj *sqlLoginRepository) AdminLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser {
	var login models.CompanyUser
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		UserName,
		UserEmail,
		UserPass
	FROM dbo.CompanyUser
	WHERE UserEmail = @p0 AND UserPass = @p1 AND ISNULL(IsSystem, '0') <> '1'`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("userEmail", fmt.Sprintf("%v", userEmail)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", userEmail),
	).Scan(
		&login.ID,
		&login.UserName,
		&login.UserEmail,
		&login.UserPass,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", login)),
	)
	return &login
}

func (sj *sqlLoginRepository) SystemLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser {

	var login models.CompanyUser
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		UserName,
		UserEmail,
		UserPass
	FROM dbo.CompanyUser
	WHERE UserEmail = @p0 AND ISNULL(IsSystem, '0') = '1'`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("userEmail", fmt.Sprintf("%v", userEmail)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", userEmail),
	).Scan(
		&login.ID,
		&login.UserName,
		&login.UserEmail,
		&login.UserPass,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", login)),
	)
	//
	return &login
}
func (sj *sqlLoginRepository) MemberLogin(ctx context.Context, username string) (*models.CompanyUser, error) {

	var login models.CompanyUser
	query := `
	SELECT TOP 1
		Cast(ID as varchar(36)) as ID,
		IsNull(EmailAddress, ''),
		Handphone,
		RecordStatus,
		IsNull(LoginPass, ''),
		IsNull(Imei, '')
	FROM dbo.Member
	WHERE 
		Handphone = @p0 and RecordStatus != 5`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("username", fmt.Sprintf("%v", username)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", username),
	).Scan(
		&login.ID,
		&login.UserEmail,
		&login.UserName,
		&login.RecordStatus,
		&login.UserPass,
		&login.Imei,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", login)),
	)
	//
	return &login, nil
}
func (sj *sqlLoginRepository) GetMemberByPhone(ctx context.Context, handphone string) (*models.Member, error) {

	var login models.Member
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		EmailAddress,
		Handphone, RecordStatus,
		Isnull(VerificationExpiry, cast('1900-1-1' as datetime2))
	FROM dbo.Member
	WHERE Handphone = @p0 and RecordStatus != 5`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("handphone", fmt.Sprintf("%v", handphone)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", handphone),
	).Scan(
		&login.ID,
		&login.Email,
		&login.Handphone,
		&login.RecordStatus,
		&login.VerificationExpiry,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", login)),
	)
	return &login, nil
}
func (sj *sqlLoginRepository) AdminFindByEmail(ctx context.Context, userEmail string) *models.CompanyUser {
	var login models.CompanyUser
	query := `
	SELECT 
		Cast(ID as varchar(36)) as ID,
		UserName,
		UserEmail,
		UserPass,
		Cast(CompanyID as varchar(36)),
		Cast(CompanyUserRoleID as varchar(36)),
		ISNULL(IsLocked, '0') as IsLocked
	FROM dbo.CompanyUser
	WHERE UserEmail = @p0 and RecordStatus=1
		AND ISNULL(IsSystem, '0') <> '1'`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("userEmail", fmt.Sprintf("%v", userEmail)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", userEmail),
	).Scan(
		&login.ID,
		&login.UserName,
		&login.UserEmail,
		&login.UserPass,
		&login.CompanyID,
		&login.CompanyUserRoleID,
		&login.IsLocked,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", login)),
	)

	return &login
}

func (sj *sqlLoginRepository) DoAuditLog(ctx context.Context, log *models.AuditLog) (bool, error) {
	auditLogQuery := `INSERT INTO AuditLog (
		ID
		,UserID
		,Token
		,ModuleName
		,UserInsert
		,DateInsert
		,Remarks 
		) 
	VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", auditLogQuery)),
	)
	_, err := sj.Conn.ExecContext(ctx, auditLogQuery,
		sql.Named("p0", log.ID),
		sql.Named("p1", log.UserID),
		sql.Named("p2", log.Token),
		sql.Named("p3", log.ModuleName),
		sql.Named("p4", log.UserInsert),
		sql.Named("p5", log.DateInsert),
		sql.Named("p6", log.Remarks),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Insert Success")
	return true, err
}

func (sj *sqlLoginRepository) GetAuditLog(ctx context.Context, startDate *string, endDate *string) ([]*models.AuditLog, error) {
	query := `
	SELECT Cast(ID as varchar(36)) as [ID]
      ,Cast(UserID as varchar(36)) as UserID
	  ,isnull(isnull((select Username from companyuser where id = a.userid),(select membername from Member where id = a.userid)),'') as UserIDName
      ,isnull([Token],'') as Token
      ,isnull([ModuleName],'') as ModuleName
      ,isnull(convert(varchar(max),[UserInsert]),'') as UserInsert
	  ,isnull(isnull((select Username from companyuser where id = a.UserInsert),(select membername from Member where id = a.UserInsert)),'') as UserInsertName
      ,isnull(convert(varchar(max),[DateInsert]),'') as [DateInsert]
      ,isnull(convert(varchar(max),[UserUpdate]),'') as [UserUpdate]
      ,isnull(convert(varchar(max),[DateUpdate]),'') as [DateUpdate]
      ,isnull([Remarks],'') as [Remarks]
  	FROM [dbo].[AuditLog] a
	
	`
	if startDate != nil && endDate != nil {
		query = query + " where DateInsert > '" + *startDate + " 00:00:00' and DateInsert < '" + *endDate + " 23:59:59'"
	}

	query = query + " order by DateInsert desc"
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Audit Log"),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.AuditLog, 0)
	for rows.Next() {
		j := new(models.AuditLog)
		err = rows.Scan(
			&j.ID,
			&j.UserID,
			&j.UserIDName,
			&j.Token,
			&j.ModuleName,
			&j.UserInsert,
			&j.UserInsertName,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
			&j.Remarks,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlLoginRepository) GetChangeDeviceLog(ctx context.Context, memberId *string, modulName *string, date *string) (*models.AuditLog, error) {
	var j models.AuditLog
	query := `
	SELECT top (1) [ID]
      ,isnull(convert(varchar(max),UserID),'') as UserID
	  ,isnull(isnull((select Username from companyuser where id = a.userid),(select membername from Member where id = a.userid)),'') as UserIDName
      ,isnull([Token],'') as Token
      ,isnull([ModuleName],'') as ModuleName
      ,isnull(convert(varchar(max),[UserInsert]),'') as UserInsert
	  ,isnull(isnull((select Username from companyuser where id = a.UserInsert),(select membername from Member where id = a.UserInsert)),'') as UserInsertName
      ,isnull(convert(varchar(max),[DateInsert]),'') as [DateInsert]
      ,isnull(convert(varchar(max),[UserUpdate]),'') as [UserUpdate]
      ,isnull(convert(varchar(max),[DateUpdate]),'') as [DateUpdate]
      ,isnull([Remarks],'') as [Remarks]
  	FROM [dbo].[AuditLog] a
	where UserID=@p0 and ModuleName=@p1 and dateinsert like @p2 
	order by DateInsert desc`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Change Device Log"),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", memberId),
		sql.Named("p1", modulName),
		sql.Named("p2", "%"+*date+"%"),
	).Scan(
		&j.ID,
		&j.UserID,
		&j.UserIDName,
		&j.Token,
		&j.ModuleName,
		&j.UserInsert,
		&j.UserInsertName,
		&j.DateInsert,
		&j.UserUpdate,
		&j.DateUpdate,
		&j.Remarks,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return &j, nil
}
