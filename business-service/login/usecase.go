package login

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type LoginUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, string, error)
	AdminLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser
	SystemLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser
	AdminFindByEmail(ctx context.Context, userEmail string) *models.CompanyUser
	ComparePassword(ctx context.Context, userCredentials *models.CompanyUser) (*models.CompanyUser, error)
	MemberLoginBase(ctx context.Context, username string, password string) (*models.CompanyUser, error)
	MemberLogin(ctx context.Context, username string, password string) (*models.CompanyUser, error)
	GetMemberByPhone(ctx context.Context, handphone string) (*models.Member, error)
	DoAuditLog(ctx context.Context, log *models.AuditLog) (bool, error)
	GetAuditLog(ctx context.Context, startDate *string, endDate *string) ([]*models.AuditLog, error)
	GetChangeDeviceLog(ctx context.Context, memberId *string, modulName *string, date *string) (*models.AuditLog, error)
	// Update(ctx context.Context, ar *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	// Store(context.Context, *models.Journal) (*models.Journal, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	//Fetch(ctx context.Context) []*models.Journal
}
