package login

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type LoginRepository interface {
	//Fetch(ctx context.Context) []*models.Journal
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	AdminLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser
	SystemLogin(ctx context.Context, userEmail string, password string) *models.CompanyUser
	AdminFindByEmail(ctx context.Context, userEmail string) *models.CompanyUser
	MemberLogin(ctx context.Context, email string) (*models.CompanyUser, error)
	GetMemberByPhone(ctx context.Context, handphone string) (*models.Member, error)
	DoAuditLog(ctx context.Context, log *models.AuditLog) (bool, error)
	GetAuditLog(ctx context.Context, startDate *string, endDate *string) ([]*models.AuditLog, error)
	GetChangeDeviceLog(ctx context.Context, memberId *string, modulName *string, date *string) (*models.AuditLog, error)
	// Update(ctx context.Context, journal *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	// Store(ctx context.Context, a *models.Journal) (int64, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
