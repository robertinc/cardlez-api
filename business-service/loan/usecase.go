package loan

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type LoanProductUsecase interface {
	Fetch(ctx context.Context) (error, *[]*models.LoanProduct)
	GetByID(ctx context.Context, LoanProductID string) (error, *models.LoanProduct)
}
