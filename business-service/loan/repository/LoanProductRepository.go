package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"go.uber.org/zap"

	"gitlab.com/robertinc/cardlez-api/business-service/loan"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
)

type sqlLoanProductRepository struct {
	Conn *sql.DB
}

func NewSqlLoanProductRepository() loan.LoanProductRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlLoanProductRepository{conn}
}

func NewSqlLoanProductRepositoryV2(Conn *sql.DB) loan.LoanProductRepository {
	conn := Conn
	return &sqlLoanProductRepository{conn}
}

func (sj *sqlLoanProductRepository) Fetch(ctx context.Context) (error, *[]*models.LoanProduct) {
	query := `
	SELECT Cast(LoanProduct_ID as varchar(36)) as LoanProduct_ID
      ,Isnull(LoanProduct_No, '') as LoanProduct_No
      ,Isnull(Product_Name, '') as Product_Name
      ,Isnull(Interest_Calc_Type, 0) as Interest_Calc_Type
      ,Isnull(Cast(InterestDayBasis_ID as varchar(36)), '') as InterestDayBasis_ID
      ,Isnull(Interest_Percentage, 0) as Interest_Percentage 
      ,Isnull(Calc_Base, 0) as Calc_Base 
      ,Isnull(Scheme_Type, 0) as Scheme_Type
      ,Isnull(Schedule_Type, 0) as Schedule_Type 
      ,Isnull(Collection_Type, 0) as Collection_Type
      ,Isnull(Rounding_Type, 0) as Rounding_Type 
      ,Isnull(Penalty_Termination_Amount, 0) as Penalty_Termination_Amount
      ,Isnull(min_penalty_termination, 0) as min_penalty_termination 
      ,Isnull(Penalty_Amount, 0) as Penalty_Amount 
      ,Isnull(Min_Loan_Amount, 0) as Min_Loan_Amount 
      ,Isnull(Record_Status, 0) as Record_Status 
      ,Isnull(Cast(CompanyBranch_ID as varchar(36)), '') as CompanyBranch_ID
	  FROM [Master.LoanProduct]
	  order by Product_Name
	`
	result := make([]*models.LoanProduct, 0)
	logger := service.Logger(ctx)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		j := new(models.LoanProduct)
		err = rows.Scan(
			&j.LoanProductID,
			&j.LoanProductNo,
			&j.ProductName,
			&j.InterestCalcType,
			&j.InterestDayBasisID,
			&j.InterestPercentage,
			&j.CalcBase,
			&j.SchemeType,
			&j.ScheduleType,
			&j.CollectionType,
			&j.RoundingType,
			&j.PenaltyTerminationAmount,
			&j.MinPenaltyTermination,
			&j.PenaltyAmount,
			&j.MinLoanAmount,
			&j.RecordStatus,
			&j.CompanyBranchID,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, &result
}

func (sj *sqlLoanProductRepository) GetByID(ctx context.Context, LoanProductID string) (error, *models.LoanProduct) {
	var loanProduct models.LoanProduct
	query := `
	SELECT Cast(LoanProduct_ID as varchar(36)) as LoanProduct_ID
      ,Isnull(LoanProduct_No, '') as LoanProduct_No
      ,Isnull(Product_Name, '') as Product_Name
      ,Isnull(Interest_Calc_Type, 0) as Interest_Calc_Type
      ,Isnull(Cast(InterestDayBasis_ID as varchar(36)), '') as InterestDayBasis_ID
      ,Isnull(Interest_Percentage, 0) as Interest_Percentage 
      ,Isnull(Calc_Base, 0) as Calc_Base 
      ,Isnull(Scheme_Type, 0) as Scheme_Type
      ,Isnull(Schedule_Type, 0) as Schedule_Type 
      ,Isnull(Collection_Type, 0) as Collection_Type
      ,Isnull(Rounding_Type, 0) as Rounding_Type 
      ,Isnull(Penalty_Termination_Amount, 0) as Penalty_Termination_Amount
      ,Isnull(min_penalty_termination, 0) as min_penalty_termination 
      ,Isnull(Penalty_Amount, 0) as Penalty_Amount 
      ,Isnull(Min_Loan_Amount, 0) as Min_Loan_Amount 
      ,Isnull(Record_Status, 0) as Record_Status 
      ,Isnull(Cast(CompanyBranch_ID as varchar(36)), '') as CompanyBranch_ID
	  FROM [Master.LoanProduct]
	where LoanProduct_ID = @p0 
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
		zap.String("loanproductid", fmt.Sprintf("%v", LoanProductID)),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", LoanProductID)).Scan(
		&loanProduct.LoanProductID,
		&loanProduct.LoanProductNo,
		&loanProduct.ProductName,
		&loanProduct.InterestCalcType,
		&loanProduct.InterestDayBasisID,
		&loanProduct.InterestPercentage,
		&loanProduct.CalcBase,
		&loanProduct.SchemeType,
		&loanProduct.ScheduleType,
		&loanProduct.CollectionType,
		&loanProduct.RoundingType,
		&loanProduct.PenaltyTerminationAmount,
		&loanProduct.MinPenaltyTermination,
		&loanProduct.PenaltyAmount,
		&loanProduct.MinLoanAmount,
		&loanProduct.RecordStatus,
		&loanProduct.CompanyBranchID,
	)
	if err != nil {
		logger.Error("Scan Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Result",
		zap.String("res", fmt.Sprintf("%v", loanProduct)),
	)
	return nil, &loanProduct
}
