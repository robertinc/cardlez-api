package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/loan"
	"gitlab.com/robertinc/cardlez-api/business-service/loan/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type LoanProductUsecase struct {
	LoanProductRepository loan.LoanProductRepository
	contextTimeout        time.Duration
}

func NewLoanProductUsecase() loan.LoanProductUsecase {
	return &LoanProductUsecase{
		LoanProductRepository: repository.NewSqlLoanProductRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewLoanProductUsecaseV2(LoanProductRepo loan.LoanProductRepository) loan.LoanProductUsecase {
	return &LoanProductUsecase{
		LoanProductRepository: LoanProductRepo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func (a *LoanProductUsecase) Fetch(c context.Context) (error, *[]*models.LoanProduct) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, LoanProducts := a.LoanProductRepository.Fetch(ctx)
	if err != nil {
		return err, nil
	}
	return nil, LoanProducts
}

func (a *LoanProductUsecase) GetByID(c context.Context, LoanProductID string) (error, *models.LoanProduct) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, loanProduct := a.LoanProductRepository.GetByID(ctx, LoanProductID)
	if err != nil {
		return err, nil
	}
	return nil, loanProduct
}
