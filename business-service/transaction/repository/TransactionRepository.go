package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	"gitlab.com/robertinc/cardlez-api/business-service/transaction"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"

	"github.com/beevik/guid"
)

type sqlTransactionRepository struct {
	Conn *sql.DB
}

func NewSqlTransactionRepository() transaction.TransactionRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlTransactionRepository{conn}
}

func NewSqlTransactionRepositoryV2(Conn *sql.DB) transaction.TransactionRepository {
	conn := Conn
	return &sqlTransactionRepository{conn}
}

func (sj *sqlTransactionRepository) Fetch(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {
	query := `
	DROP TABLE IF EXISTS tab
	CREATE TABLE tab
	(
		JournalID uniqueidentifier,
		Code varchar(50),
		DebitAmount decimal,
		MemberName varchar(255),
		AccountNo varchar(255),
		CreditAmount decimal,
		[Description] varchar(max),
		Tipe_Tabungan varchar(50),
		PostingDate varchar(500),
		BICode varchar(50),
		MemberId uniqueidentifier,
		AccountId uniqueidentifier,
		DateInsert datetime,
		Expr1 varchar(50),
		amount decimal,
		TotalAmount decimal
	);

	INSERT INTO tab EXEC sp_rekeningKoran
	@p0,
	@p1,
	@p2

	select
		Cast(JournalID as varchar(36)) as JournalID,
		PostingDate,
		Description,
		case when DebitAmount > 0 then DebitAmount * -1 else CreditAmount end as amount,
		TotalAmount
	from tab
	drop table tab
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Transaction"),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
		zap.String("accountID", fmt.Sprintf("%v", accountID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd),
		sql.Named("p2", accountID),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.Transaction, 0)
	for rows.Next() {
		j := new(models.Transaction)
		err = rows.Scan(
			&j.JournalID,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		j.PostingDateCurrency = j.PostingDate
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlTransactionRepository) FetchBase(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {
	query := `
	DROP TABLE IF EXISTS tab
	CREATE TABLE tab
	(
		JournalID uniqueidentifier, 
		Code varchar(50), 
		DebitAmount decimal (18, 2), 
		MemberName varchar(255), 
		AccountNo varchar(255), 
		CreditAmount decimal (18, 2), 
		[Description] varchar(max), 
		Tipe_Tabungan varchar(50), 
		PostingDate varchar(500), 
		BICode varchar(50), 
		MemberId uniqueidentifier,
		AccountId uniqueidentifier, 
		DateInsert datetime, 
		Expr1 varchar(50),
		amount decimal (18, 2),
		TotalAmount decimal (18, 2)
	);

	INSERT INTO tab EXEC sp_rekeningKoran
	@p0,
	@p1,
	@p2

	select 
		Cast(JournalID as varchar(36)) as JournalID,
		PostingDate,
		Description,
		DebitAmount,
		CreditAmount
	from tab
	drop table tab
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Transaction"),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
		zap.String("accountID", fmt.Sprintf("%v", accountID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd),
		sql.Named("p2", accountID),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.Transaction, 0)
	for rows.Next() {
		j := new(models.Transaction)
		err = rows.Scan(
			&j.JournalID,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		j.PostingDateCurrency = j.PostingDate
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlTransactionRepository) FetchBase2(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {
	query := `
		DROP TABLE IF EXISTS tab
		CREATE TABLE tab
		(
			JournalID uniqueidentifier, 
			Code varchar(50), 
			DebitAmount decimal (18, 2), 
			MemberName varchar(255), 
			AccountNo varchar(255), 
			CreditAmount decimal (18, 2), 
			[Description] varchar(max), 
			Tipe_Tabungan varchar(50), 
			PostingDate varchar(500), 
			BICode varchar(50), 
			MemberId uniqueidentifier,
			AccountId uniqueidentifier, 
			DateInsert datetime, 
			Expr1 varchar(50),
			amount decimal (18, 2),
			TotalAmount decimal (18, 2)
		);

		INSERT INTO tab EXEC sp_rekeningKoran
		@p0,
		@p1,
		@p2

		select '' as JournalID,'' as PostingDate,'Saldo Awal' as Description, 0 as DebitAmount,
		isnull((
			select a.Balance from (
			--opening
			select TOP(1) a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,
			case when year(a.dateInsert)=year(cast(@p0 as date)) and month(a.dateInsert)=month(cast(@p0 as date)) and day(a.dateInsert)=day(cast(@p0 as date))
			then a.OpeningBalance else a.ClosingBalance end as Balance
			from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p0 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=@p2
			inner join COA c on a.COAID=c.ID
			ORDER BY a.DateInsert DESC
			) a 
		),0) as CreditAmount
		UNION ALL
		select 
			Cast(JournalID as varchar(36)) as JournalID,
			ISNULL(CONVERT(VARCHAR(50), PostingDate),'') as PostingDate,
			Description,
			DebitAmount,
			CreditAmount
		from tab
		UNION ALL
		select '','','Saldo Akhir', 0,
		isnull((
			select a.ClosingBalance from (
			--opening
			select TOP(1) a.COAID,c.Code,a.ReferenceNumber,a.DateInsert,a.ClosingBalance
			from AccountingBalance a 
			inner join (
			select distinct COAID,ReferenceNumber, max(DateInsert) as dateinsert from AccountingBalance
			where DateInsert < dateadd(day,1,cast(@p1 as date))
			group by COAid,ReferenceNumber) b
			on a.COAID=b.COAID and a.DateInsert=b.dateinsert and isnull(a.ReferenceNumber,'')=@p2
			inner join COA c on a.COAID=c.ID
			ORDER BY a.DateInsert DESC
			) a 
		),0) as CreditAmount
		drop table tab
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Transaction"),
		zap.String("periodStart", fmt.Sprintf("%v", periodStart)),
		zap.String("periodEnd", fmt.Sprintf("%v", periodEnd)),
		zap.String("accountID", fmt.Sprintf("%v", accountID)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", periodStart),
		sql.Named("p1", periodEnd),
		sql.Named("p2", accountID),
	)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.Transaction, 0)
	for rows.Next() {
		j := new(models.Transaction)
		err = rows.Scan(
			&j.JournalID,
			&j.PostingDate,
			&j.Description,
			&j.DebitAmount,
			&j.CreditAmount,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		j.PostingDateCurrency = j.PostingDate
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}

func (sj *sqlTransactionRepository) SyncMutationTransactionFetch(ctx context.Context) ([]*models.SyncMutationTransaction, error) {
	query := `
	Select 
		cast(ID as varchar(50)) as ID,
		isnull(cast(DateStart as varchar(50)),'') as DateStart,
		isnull(cast(DateFinish as varchar(50)),'') as DateFinish,
		Type,
		Status,
		isnull(cast(UserInsert as varchar(50)),'') as UserInsert,
		isnull(cast(DateInsert as varchar(50)),'') as DateInsert,
		isnull(cast(UserUpdate as varchar(50)),'') as UserUpdate,
		isnull(cast(DateUpdate as varchar(50)),'') as DateUpdate
	from SyncMutationTransaction
	order by DateInsert desc
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Sync Mutation Transaction"),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.SyncMutationTransaction, 0)
	for rows.Next() {
		j := new(models.SyncMutationTransaction)
		err = rows.Scan(
			&j.ID,
			&j.DateStart,
			&j.DateFinish,
			&j.Type,
			&j.Status,
			&j.UserInsert,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlTransactionRepository) SyncMutationTransactionFetch2(ctx context.Context, ByDateRequest bool) ([]*models.SyncMutationTransaction, error) {
	queryCondition := "where Type not in (5,6) "
	if ByDateRequest {
		queryCondition = "where Type in (5,6) "
	}
	query := `
	Select 
		cast(ID as varchar(50)) as ID,
		isnull(cast(DateStart as varchar(50)),'') as DateStart,
		isnull(cast(DateFinish as varchar(50)),'') as DateFinish,
		Type,
		Status,
		isnull(cast(UserInsert as varchar(50)),'') as UserInsert,
		isnull(cast(DateInsert as varchar(50)),'') as DateInsert,
		isnull(cast(UserUpdate as varchar(50)),'') as UserUpdate,
		isnull(cast(DateUpdate as varchar(50)),'') as DateUpdate,
		isnull(cast(StartDateRequest as varchar(50)),'') as StartDateRequest,
		isnull(cast(FinishDateRequest as varchar(50)),'') as FinishDateRequest
	from SyncMutationTransaction
	` + queryCondition + `
	order by DateInsert desc
	`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", "Fetch Sync Mutation Transaction"),
	)
	rows, err := sj.Conn.Query(query)
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.SyncMutationTransaction, 0)
	for rows.Next() {
		j := new(models.SyncMutationTransaction)
		err = rows.Scan(
			&j.ID,
			&j.DateStart,
			&j.DateFinish,
			&j.Type,
			&j.Status,
			&j.UserInsert,
			&j.DateInsert,
			&j.UserUpdate,
			&j.DateUpdate,
			&j.StartDateRequest,
			&j.FinishDateRequest,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return nil, err
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return result, nil
}
func (sj *sqlTransactionRepository) SyncMutationTransactionStore(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {
	obj.ID = guid.New().StringUpper()
	obj.UserInsert = "00000000-0000-0000-0000-000000000000"
	if ctx.Value(service.CtxUserID) != nil {
		obj.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	Query := `INSERT INTO SyncMutationTransaction (
		ID,
		DateStart,
		Type,
		Status,
		UserInsert,
		DateInsert
		) 
	VALUES (@p0, GETDATE(), @p2, 1, @userInsert, GETDATE())`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("obj", fmt.Sprintf("%v", obj)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", obj.ID),
		sql.Named("p2", obj.Type),
		sql.Named("userInsert", obj.UserInsert),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return obj, nil
}
func (sj *sqlTransactionRepository) SyncMutationTransactionStoreWithDate(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {
	obj.ID = guid.New().StringUpper()
	obj.UserInsert = "00000000-0000-0000-0000-000000000000"
	if ctx.Value(service.CtxUserID) != nil {
		obj.UserInsert = ctx.Value(service.CtxUserID).(string)
	}
	Query := `INSERT INTO SyncMutationTransaction (
		ID,
		DateStart,
		Type,
		Status,
		UserInsert,
		DateInsert,
		StartDateRequest,
		FinishDateRequest
		) 
	VALUES (@p0, GETDATE(), @p2, 1, @userInsert, GETDATE(), @p3, @p4)`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("obj", fmt.Sprintf("%v", obj)),
	)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", obj.ID),
		sql.Named("p2", obj.Type),
		sql.Named("p3", obj.StartDateRequest),
		sql.Named("p4", obj.FinishDateRequest),
		sql.Named("userInsert", obj.UserInsert),
	)
	if err != nil {
		logger.Error("Insert Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Insert Success")
	return obj, nil
}
func (sj *sqlTransactionRepository) SyncMutationTransactionUpdate(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {
	var param []string
	Query := `Update SyncMutationTransaction Set `
	if obj.DateStart != "" {
		param = append(param, "DateStart = @p1")
	}
	if obj.DateFinish != "" {
		param = append(param, "DateFinish = @p2")
	}
	if obj.Type != -1 {
		param = append(param, "Type = @p3")
	}
	if obj.Status != -1 {
		param = append(param, "Status = @p4")
	}
	if ctx.Value(service.CtxIsAdmin).(bool) {
		param = append(param, "UserUpdate = @userUpdate")
		param = append(param, "DateUpdate = GETDATE()")
	} else {
		param = append(param, "UserUpdate = UserInsert")
		param = append(param, "DateUpdate = GETDATE()")
	}

	Query += strings.Join(param, ",")
	Query += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("obj", fmt.Sprintf("%v", obj)),
	)
	obj.UserUpdate = ctx.Value(service.CtxUserID).(string)
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", obj.ID),
		sql.Named("p1", obj.DateStart),
		sql.Named("p2", obj.DateFinish),
		sql.Named("p3", obj.Type),
		sql.Named("p4", obj.Status),
		sql.Named("userUpdate", obj.UserUpdate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return nil, err
	}
	logger.Info("Update Success")
	return obj, nil
}

func (sj *sqlTransactionRepository) SyncMutationTransactionFinish(ctx context.Context, id string) (bool, error) {

	var param []string
	Query := `Update SyncMutationTransaction Set 
	DateFinish = GETDATE(),
	Status = 2,
	DateUpdate = GETDATE(),
	`
	param = append(param, "UserUpdate = @userUpdate")

	Query += strings.Join(param, ",")
	Query += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	UserUpdate := "00000000-0000-0000-0000-000000000000"
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("userUpdate", UserUpdate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
func (sj *sqlTransactionRepository) SyncMutationTransactionFailed(ctx context.Context, id string) (bool, error) {
	var param []string
	Query := `Update SyncMutationTransaction Set 
	DateFinish = GETDATE(),
	Status = 3,
	DateUpdate = GETDATE(),
	`
	param = append(param, "UserUpdate = @userUpdate")

	Query += strings.Join(param, ",")
	Query += ` Where ID = @p0`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", Query)),
		zap.String("id", fmt.Sprintf("%v", id)),
	)
	UserUpdate := "00000000-0000-0000-0000-000000000000"
	if ctx.Value(service.CtxUserID) != nil {
		UserUpdate = ctx.Value(service.CtxUserID).(string)
	}
	_, err := sj.Conn.ExecContext(ctx, Query,
		sql.Named("p0", id),
		sql.Named("userUpdate", UserUpdate),
	)
	if err != nil {
		logger.Error("Update Error",
			zap.String("error", err.Error()),
		)
		return false, err
	}
	logger.Info("Update Success")
	return true, nil
}
