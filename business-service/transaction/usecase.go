package transaction

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TransactionUsecase interface {
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.COA, string, error)
	//GetByID(ctx context.Context, id *string) *models.COA
	// Update(ctx context.Context, ar *models.COA) (*models.COA, error)
	// GetByTitle(ctx context.Context, title string) (*models.COA, error)
	// Store(context.Context, *models.COA) (*models.COA, error)
	// Delete(ctx context.Context, id int64) (bool, error)
	Fetch(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	FetchBase(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	FetchBase2(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	SyncMutationTransactionFetch(ctx context.Context) ([]*models.SyncMutationTransaction, error)
	SyncMutationTransactionFetch2(ctx context.Context, ByDateRequest bool) ([]*models.SyncMutationTransaction, error)
	SyncMutationTransactionStore(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionStore2(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionUpdate(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionFinish(ctx context.Context, id string) (bool, error)
	SyncMutationTransactionFailed(ctx context.Context, id string) (bool, error)
}
