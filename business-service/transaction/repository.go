package transaction

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type TransactionRepository interface {
	Fetch(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	FetchBase(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	FetchBase2(ctx context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error)
	SyncMutationTransactionFetch(ctx context.Context) ([]*models.SyncMutationTransaction, error)
	SyncMutationTransactionFetch2(ctx context.Context, ByDateRequest bool) ([]*models.SyncMutationTransaction, error)
	SyncMutationTransactionStore(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionStoreWithDate(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionUpdate(ctx context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error)
	SyncMutationTransactionFinish(ctx context.Context, id string) (bool, error)
	SyncMutationTransactionFailed(ctx context.Context, id string) (bool, error)
	// Fetch(ctx context.Context, cursor string, num int64) ([]*models.Journal, error)
	// GetByID(ctx context.Context, id *string) *models.Journal
	// Update(ctx context.Context, journal *models.Journal) (*models.Journal, error)
	// GetByTitle(ctx context.Context, title string) (*models.Journal, error)
	// Store(ctx context.Context, a *models.Journal) (int64, error)
	// Delete(ctx context.Context, id int64) (bool, error)
}
