package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/transaction"
	"gitlab.com/robertinc/cardlez-api/business-service/transaction/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type TransactionUsecase struct {
	TransactionRepository transaction.TransactionRepository
	contextTimeout        time.Duration
}

func NewTransactionUsecase() transaction.TransactionUsecase {
	return &TransactionUsecase{
		TransactionRepository: repository.NewSqlTransactionRepository(),
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func NewTransactionUsecaseV2(repo transaction.TransactionRepository) transaction.TransactionUsecase {
	return &TransactionUsecase{
		TransactionRepository: repo,
		contextTimeout:        time.Second * time.Duration(models.Timeout()),
	}
}

func (a *TransactionUsecase) Fetch(c context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listTransaction, err := a.TransactionRepository.Fetch(ctx, accountID, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listTransaction, nil
}
func (a *TransactionUsecase) FetchBase(c context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listTransaction, err := a.TransactionRepository.FetchBase(ctx, accountID, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listTransaction, nil
}

func (a *TransactionUsecase) FetchBase2(c context.Context, accountID string, periodStart string, periodEnd string) ([]*models.Transaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listTransaction, err := a.TransactionRepository.FetchBase2(ctx, accountID, periodStart, periodEnd)
	if err != nil {
		return nil, err
	}
	return listTransaction, nil
}

func (a *TransactionUsecase) SyncMutationTransactionFetch(c context.Context) ([]*models.SyncMutationTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listSyncTransaction, err := a.TransactionRepository.SyncMutationTransactionFetch(ctx)
	if err != nil {
		return nil, err
	}
	return listSyncTransaction, nil
}
func (a *TransactionUsecase) SyncMutationTransactionFetch2(c context.Context, ByDateRequest bool) ([]*models.SyncMutationTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listSyncTransaction, err := a.TransactionRepository.SyncMutationTransactionFetch2(ctx, ByDateRequest)
	if err != nil {
		return nil, err
	}
	return listSyncTransaction, nil
}
func (a *TransactionUsecase) SyncMutationTransactionStore(c context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionStore(ctx, obj)
	if err != nil {
		return nil, err
	}
	return SyncTransaction, nil
}
func (a *TransactionUsecase) SyncMutationTransactionStore2(c context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	if obj.StartDateRequest != "" {
		SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionStoreWithDate(ctx, obj)
		if err != nil {
			return nil, err
		}
		return SyncTransaction, nil
	} else {
		SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionStore(ctx, obj)
		if err != nil {
			return nil, err
		}
		return SyncTransaction, nil
	}
}
func (a *TransactionUsecase) SyncMutationTransactionUpdate(c context.Context, obj *models.SyncMutationTransaction) (*models.SyncMutationTransaction, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionUpdate(ctx, obj)
	if err != nil {
		return nil, err
	}
	return SyncTransaction, nil
}
func (a *TransactionUsecase) SyncMutationTransactionFinish(c context.Context, id string) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionFinish(ctx, id)
	if err != nil {
		return false, err
	}
	return SyncTransaction, nil
}
func (a *TransactionUsecase) SyncMutationTransactionFailed(c context.Context, id string) (bool, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	SyncTransaction, err := a.TransactionRepository.SyncMutationTransactionFailed(ctx, id)
	if err != nil {
		return false, err
	}
	return SyncTransaction, nil
}
