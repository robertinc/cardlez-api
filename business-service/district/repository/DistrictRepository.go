package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/robertinc/cardlez-api/business-service/district"
	models "gitlab.com/robertinc/cardlez-api/models"
	"gitlab.com/robertinc/cardlez-api/service"
	"go.uber.org/zap"
)

type sqlDistrictRepository struct {
	Conn *sql.DB
}

func NewSqlDistrictRepository() district.DistrictRepository {
	conn, err := sql.Open("sqlserver", models.BuildConnString())
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	return &sqlDistrictRepository{conn}
}

func NewSqlDistrictRepositoryV2(Conn *sql.DB) district.DistrictRepository {
	conn := Conn
	return &sqlDistrictRepository{conn}
}
func (sj *sqlDistrictRepository) Fetch(ctx context.Context, Province_ID *string) (error, []*models.District) {
	queryWhere := ""
	if Province_ID != nil && *Province_ID != "" {
		queryWhere += " where province_id = @p0 "
	}
	query := `
	select 
		cast(District_ID as varchar(36)) as District_ID,
		isnull(District_No,'') as District_No,
		isnull(District_Name,'') as District_Name,
		isnull(LBU_Code,'') as LBU_Code,
		cast(province_id as varchar(36)) as province_id,
		isnull(Postal_Code_Prefix,'') as Postal_Code_Prefix
	from VwDistrict
	 ` + queryWhere + `
	order by District_Name
		`
	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", fmt.Sprintf("%v", query)),
	)
	rows, err := sj.Conn.Query(query,
		sql.Named("p0", Province_ID),
	)
	defer rows.Close()
	if err != nil {
		logger.Error("Query Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}

	result := make([]*models.District, 0)
	for rows.Next() {
		j := new(models.District)
		err = rows.Scan(
			&j.District_ID,
			&j.District_No,
			&j.District_Name,
			&j.LBU_Code,
			&j.Province_ID,
			&j.Postal_Code_Prefix,
		)
		if err != nil {
			logger.Error("Scan Error",
				zap.String("error", err.Error()),
			)
			return err, nil
		}
		result = append(result, j)
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", result)),
	)
	return nil, result
}

func (sj *sqlDistrictRepository) GetByID(ctx context.Context, District_ID string) (error, *models.District) {
	var j models.District
	query := `
	select 
		cast(District_ID as varchar(36)) as District_ID,
		isnull(District_No,'') as District_No,
		isnull(District_Name,'') as District_Name,
		isnull(LBU_Code,'') as LBU_Code,
		cast(province_id as varchar(36)) as province_id,
		isnull(Postal_Code_Prefix,'') as Postal_Code_Prefix
	from 
		[Master.District]
	where 
		District_ID = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("District_ID", District_ID),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", District_ID)).Scan(
		&j.District_ID,
		&j.District_No,
		&j.District_Name,
		&j.LBU_Code,
		&j.Province_ID,
		&j.Postal_Code_Prefix,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}

func (sj *sqlDistrictRepository) GetByName(ctx context.Context, districtName string) (error, *models.District) {
	var j models.District
	query := `
	select top(1)
		cast(District_ID as varchar(36)) as District_ID,
		isnull(District_No,'') as District_No,
		isnull(District_Name,'') as District_Name,
		isnull(LBU_Code,'') as LBU_Code,
		cast(province_id as varchar(36)) as province_id,
		isnull(Postal_Code_Prefix,'') as Postal_Code_Prefix
	from 
		[Master.District]
	where 
		District_Name = @p0`

	logger := service.Logger(ctx)
	logger.Info("Query",
		zap.String("query", query),
		zap.String("districtName", districtName),
	)
	err := sj.Conn.QueryRow(query,
		sql.Named("p0", districtName)).Scan(
		&j.District_ID,
		&j.District_No,
		&j.District_Name,
		&j.LBU_Code,
		&j.Province_ID,
		&j.Postal_Code_Prefix,
	)
	if err != nil {
		logger.Error("Select Error",
			zap.String("error", err.Error()),
		)
		return err, nil
	}
	logger.Info("Response result",
		zap.String("res", fmt.Sprintf("%v", j)),
	)
	return nil, &j
}
