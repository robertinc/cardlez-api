package district

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type DistrictUsecase interface {
	Fetch(ctx context.Context, Province_ID *string) (error, []*models.District)
	GetByID(ctx context.Context, District_ID string) (error, *models.District)
	GetByName(ctx context.Context, districtName string) (error, *models.District)
}
