package usecase

import (
	"context"
	"time"

	"gitlab.com/robertinc/cardlez-api/business-service/district"
	"gitlab.com/robertinc/cardlez-api/business-service/district/repository"
	"gitlab.com/robertinc/cardlez-api/models"
)

type DistrictUsecase struct {
	DistrictRepository district.DistrictRepository
	contextTimeout     time.Duration
}

func NewDistrictUsecase() district.DistrictUsecase {
	return &DistrictUsecase{
		DistrictRepository: repository.NewSqlDistrictRepository(),
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func NewDistrictUsecaseV2(repo district.DistrictRepository) district.DistrictUsecase {
	return &DistrictUsecase{
		DistrictRepository: repo,
		contextTimeout:     time.Second * time.Duration(models.Timeout()),
	}
}

func (a *DistrictUsecase) Fetch(c context.Context, Province_ID *string) (error, []*models.District) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, listDistrict := a.DistrictRepository.Fetch(ctx, Province_ID)
	if err != nil {
		return err, nil
	}
	return nil, listDistrict
}

func (a *DistrictUsecase) GetByID(c context.Context, District_ID string) (error, *models.District) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, District := a.DistrictRepository.GetByID(ctx, District_ID)
	if err != nil {
		return err, nil
	}
	return nil, District
}

func (a *DistrictUsecase) GetByName(c context.Context, districtName string) (error, *models.District) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err, District := a.DistrictRepository.GetByName(ctx, districtName)
	if err != nil {
		return err, nil
	}
	return nil, District
}
