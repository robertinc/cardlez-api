package district

import (
	"context"

	"gitlab.com/robertinc/cardlez-api/models"
)

type DistrictRepository interface {
	Fetch(ctx context.Context, Province_ID *string) (error, []*models.District)
	GetByID(ctx context.Context, District_ID string) (error, *models.District)
	GetByName(c context.Context, districtName string) (error, *models.District)
}
